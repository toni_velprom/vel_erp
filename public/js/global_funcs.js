
parent
async function cit_send_status( 

data,
 
arg_dep_from,
arg_from,
 
arg_dep_to, 
arg_to, 

arg_source_status, // status na koji odgovaram
arg_new_status_tip, // ovo je objekt koji mogu dobiti sa NPR cit_deep( find_item( window.admin_conf.status_conf, `IS1635256284641925`, `sifra`) ); 
arg_komentar,

DB_model, // ovo je string koji govori mongo DB u koji collection treba spremiti ili update-ati status
DB_model_id, // samo _id od gore navedenih objekata
DB_object, // ovo može biti cijeli objekt od partnera, producta ili sirov
 
arg_kalk, // ako je samo _id od kalulacije i koristi se u statusima koji su zapravo radni nalozi
arg_proces, // ovo je zapravo proces.row_sifra od procesa u gornjoj kalkulaciji na koji se odnosi ovaj radni nalog / status
 
arg_kupac,
 
arg_docs_arr,
 
arg_dead_hours,
arg_dead_date,
 
arg_start,
arg_end,
 
 
) {
  
  
  if ( !arg_new_status_tip ) {
    popup_warn(`Nedostaje TIP statusa !!!`);
    return;
  };
  
  if ( !window.cit_local_list?.status_conf ) {
    popup_warn(`Nedostaje lista svih tipova statusa<br>OSVJEŽI stranicu i probaj ponovo!!!`);
    return;
  };
  
  
  var branch = null;
  var branch_parent = null;
  
;
  
  // uzimam samo elem parent objec i elem parent jer mi to jedino treba !!!!
  if ( arg_source_status ) {
    
    branch = await ajax_complete_branch({
      elem_parent_object: arg_source_status.elem_parent_object,
      elem_parent: arg_source_status.elem_parent
    });
    
    if ( branch?.length > 0 ) {
      
      // uzmi cijeli status sa svim propertijima koji sam dobio od brancha !!!!!!!!!
      // uzmi cijeli status sa svim propertijima koji sam dobio od brancha !!!!!!!!!
      arg_source_status = find_item( branch, status.sifra, `sifra` );
      
      $.each(branch, function(branch_ind, branch_status)  {
        if ( branch_status.elem_parent == null ) {
          branch_parent = cit_deep(branch_status);
        };
      });
      
    };
   
    
  };
  
  
  var status_place = check_status_place(window.cit_local_list.status_conf, arg_new_status_tip, branch_parent );
  
  var is_next_status = status_place.next;
  var is_finish_status = status_place.finish;
  var has_children = status_place.has_children;
  
  // ako ovo NIJE posve novi korijen onda provjeri 
  // da li je status tip ispravan tj da li spada u granu od parenta !!!
  if ( branch_parent && arg_source_status ) {
    
    // ako ovaj tip ne pripada NITI U NEXT LISTU NITI U FINISH LISTU 
    if ( !status_place.next_match && !status_place.finish_match ) {
      popup_warn(`Status koji šaljete nije uopće u ovoj grani!!!`);
      return;
    };
    
  
  };
  
  
  // ako NISAM dao izvorni status a status tip koji samm dao je za next ili finish onda javi grešku !!!
  if ( !arg_source_status && (is_next_status || is_finish_status) ) {
    popup_warn(`Pošto niste dali izvorni status, STATUS TIP mora biti neki od korijenskih statusa !!!`);
    return;
  };

  

  // ako NISAM dao izvorni status a status tip koji samm dao je za next ili finish onda javi grešku !!!
  if ( !arg_source_status && (is_next_status || is_finish_status) ) {
    popup_warn(`Pošto niste dali izvorni status, STATUS TIP mora biti neki od korijenskih statusa !!!`);
    return;
  };

  
  
  var elem_parent = null;
  var elem_parent_object = null;  
  
  if ( arg_source_status ) {
    // ako sam dao izvodni satus on može biti već child ali može biti i parent
    elem_parent = arg_source_status.elem_parent ? arg_source_status.elem_parent : arg_source_status.sifra;
    elem_parent_object = arg_source_status.elem_parent_object ? arg_source_status.elem_parent_object : arg_source_status;
  };
  
  
  if ( !arg_to || !arg_dep_to ) {
    popup_warn(`Nedostaje podatak prema kome šalješ status!!!`);
    return;
  };
  
  
  if ( !arg_from || !arg_dep_from ) {
    popup_warn(`Nedostaje podatak tko šalje status!!!`);
    return;
  };
  
  
  // samo kreiram objekt zato je donja func očekuje objekt ALI TREBA SAMO status_tip prop
  var new_status_data = { status_tip: arg_new_status_tip };

  var reply_work_result = write_reply_for_or_work_done( new_status_data, branch || data.statuses, arg_source_status.status_tip.sifra );
  
  
  var proj_sifra = arg_source_status.proj_sifra || null;
  var item_sifra = arg_source_status.item_sifra || null;
  var variant = arg_source_status.variant || null;
  
  var rok_isporuke = arg_source_status.rok_isporuke || null;
  var potrebno_dana_proiz = arg_source_status.potrebno_dana_proiz || null;
  var potrebno_dana_isporuka = arg_source_status.potrebno_dana_isporuka || null;
  var rok_proiz = arg_source_status.rok_proiz || null;
  var rok_za_def = arg_source_status.rok_za_def || null;
  
  var new_status_sifra = `status` + cit_rand();
  
  
  var product_model_id = null;
  var partner_model_id = null;
  var sirov_model_id = null;
  

  
  
  
  var link = null;
  if ( DB_model == "Partner") {
    
    link = `#partner/${ DB_model_id || DB_object?._id }/status/${new_status_sifra}`;
    partner_model_id = DB_object?._id || null;
  };
  
  
  
  if ( DB_model == "Product" ) {
    
    product_model_id = DB_object?._id || null;
    
    if ( !proj_sifra ) proj_sifra = DB_object?.proj_sifra || null;
    if ( !item_sifra ) item_sifra = DB_object.item_sifra || null;
    if ( !variant ) variant = DB_object.variant || null;
    
    if ( !rok_isporuke ) rok_isporuke = DB_object?.rok_isporuke || null;
    if ( !potrebno_dana_proiz ) potrebno_dana_proiz = DB_object?.potrebno_dana_proiz || null;
    if ( !potrebno_dana_isporuka ) potrebno_dana_isporuka = DB_object?.potrebno_dana_isporuka || null;
    if ( !rok_proiz ) rok_proiz = DB_object?.rok_proiz || null;
    if ( !rok_za_def ) rok_za_def = DB_object?.rok_za_def || null;

    
  };
  
  
  if ( DB_model == "Product" && !arg_kalk && !arg_proces ) {
    
    link = `#project/${ proj_sifra || null }/item/${item_sifra || null }/variant/${variant || null }/status/${new_status_sifra}`;
    product_model_id = DB_object?._id || null;
  };
  
  
  if ( DB_model == "Product" && arg_kalk && !arg_proces ) {
    
    link = `#project/${ proj_sifra || null }/item/${item_sifra || null }/variant/${variant || null }/kalk/${arg_kalk.kalk_sifra || null}/proces/null`;
    product_model_id = DB_object?._id || null;
  };
  
  
  if ( DB_model == "Product" && arg_kalk && arg_proces ) {
    link = `#project/${ proj_sifra || null }/item/${item_sifra || null }/variant/${variant || null }/kalk/${arg_kalk.kalk_sifra || null}/proces/${arg_proces.row_sifra}`;
    product_model_id = DB_object?._id || null;
  };
  
  
  if ( DB_model == "Product" && window.location.hash.indexOf(`#production/`) > -1 ) {
    
    link = `#production/${ new_status_sifra }/proces/${ arg_source_status.proces_id || null }`;
    product_model_id = DB_object?._id || null;
    
  };
  

  
  if ( DB_model == "Sirov") {
    link = `#sirov/${ DB_model_id || DB_object?._id }/status/${new_status_sifra}`;
    sirov_model_id = DB_object?._id || null;
  };
  
  
  /*
  ------------------------------------------------------------------------
  PRIMJER DOCSA U STATUS
  ------------------------------------------------------------------------
    [
      {
        "sifra": "doc16644556286795244",
        "time": 1664455619189.0,
        "link": "<a href=\"/docs/2022/09/1664455619189_10-toni_kutlic-offer_dobav.pdf\" target=\"_blank\" onClick=\"event.stopPropagation();\" >1664455619189_10-toni_kutlic-offer_dobav.pdf</a>"
        }
    ],
  
  
  */
  
  
  var status_template = {
    
    
    "full_product_name" : arg_source_status.full_product_name || null,
    "product_id" : arg_source_status.product_id || null,
    product_tip: DB_object?.tip?.sifra || null,
    
    /* ovo  mi govori u kojem modulu je status spremljen tj koji je parent module od statusa */
    product_model_id: product_model_id,
    partner_model_id: partner_model_id,
    sirov_model_id: sirov_model_id,
    
    
    "link" : link,
    
    "kupac_id" : arg_source_status.kupac_id || arg_kupac?._id || null,
    "kupac_naziv" : arg_source_status.kupac_naziv || arg_kupac?.naziv || null,
    
    "proj_sifra" : proj_sifra,
    "item_sifra" : item_sifra,
    "variant" : variant,
    
    "sifra" : new_status_sifra,
    "status" : new_status_sifra,
    
    "elem_parent" : elem_parent,
    "elem_parent_object" : elem_parent_object,
    
    
    "time" : Date.now(),
    
    
    "work_times" : null,
    "worker_error_users" : [],
    
    "docs" : arg_docs_arr || null,
    
    
    "rn_files" : [],
    
    "est_deadline_hours" : arg_dead_hours || null,
    "est_deadline_date" : arg_dead_date || null,
    
    
    "start" : arg_start || null,
    "end" : arg_end || null,
    
    "old_status_tip" : arg_source_status.status_tip,
    
    "status_tip" : arg_new_status_tip,
    status_tip_flat: JSON.stringify(arg_new_status_tip || null),
    
    
    "dep_from" : arg_dep_from,
    "from" : arg_from,
    
    "dep_to": arg_dep_to,
    "to" : arg_to,
    
    "old_komentar" : arg_source_status.komentar || null,
    "komentar" : arg_komentar,
    
    "seen" : null,
    "reply_for" : null,
    
    "responded" : true,
    
    "done_for_status" : null,
    "work_finished" : null,
        
    "sirov" : null,
    "order_sirov_count" : null,
    
    "records" : [],
    
    "rok_isporuke" : rok_isporuke,
    "potrebno_dana_proiz" : potrebno_dana_proiz,
    "potrebno_dana_isporuka" : potrebno_dana_isporuka,
    "rok_proiz" : rok_proiz,
    "rok_za_def" : rok_za_def,
    
    
    "branch_done" : false,
    

    rn_files: null,
    rn_files_flat: null,
    rn_alat: null,
    rn_alat_flat: null,

    
    kalk: arg_source_status.kalk || null,
    proces_id: arg_source_status.proces_id || null,
    
    prio:  null,

    rn_droped: null,
    rn_extra: null,

    rn_finished: null,
    rn_verif: null,


    
    
};
  
  
  
  
  
};



function wait_for(condition, callbackFunction, timeout, expired_callback, interval_time, arg_data) {
  
  if ( !interval_time ) interval_time = 30;

  var new_wait_for_object = {
    timeStamp: Date.now(),
    condition: condition
  };

  if ( !window.wait_interval_array ) window.wait_interval_array = [];
  window.wait_interval_array.push(new_wait_for_object);
  
  var interval_id = setInterval(function() {
      check_condition(interval_id, condition, callbackFunction, timeout, expired_callback, arg_data);
  }, interval_time);
  
  var wait_arr_length = window.wait_interval_array.length;
  // ovo je wait objekt kojeg sam upravo napravio
  var this_wait_obj = window.wait_interval_array[wait_arr_length - 1]; 
  // dodaj tom zadnjem wait objekt id on intervala
  this_wait_obj.id = interval_id;
  
  window.wait_interval_array[wait_arr_length - 1] = {
    ...this_wait_obj, 
    callbackFunction,
    timeout,
    expired_callback,
    interval_time
  };
  
  return interval_id;

};

function check_condition(interval_id, condition, callbackFunction, timeout, expired_callback, arg_data) {
  
  var waiting_time = 50000;
  if (typeof timeout !== 'undefined' && timeout !== 'inf') {
    waiting_time = Number(timeout);
  };

  var resolved_condition = null;

  /*
  function Date(n){
    return ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"][n%7 || 0];
  }
  function runCodeWithDateFunction(obj){
      return Function('"use strict";return ( function(Date){ return Date(5) } )')()(Date);
  }
  console.log( runCodeWithDateFunction("function(Date){ return Date(5) }") )
  */

  if ($.type(condition) == "function") resolved_condition = condition();
  if ($.type(condition) == "string") resolved_condition = eval(condition);

  if (resolved_condition !== true) {

    /* -------------- if timeout is 'inf' then dont do this check at all --------------  */
    // effectively this means that listener is infinite !!!!!!
    if (timeout == 'inf') {

      /* 
      ------------- 
      DONT DO ANY CHECKS IS TIME EXPIRED ---> BECAUSE LISTENER IS INFINITE
      ----------------
      */
    } else {
      // console.log('waiting for condition: ' + condition + '    to be true.....');
      // just a pointers to delete later
      var remove_this_index = null;
      // iterate array of interval object ids
      $.each(window.wait_interval_array, function (index, interval_object) {
        // when I find right id
        if (parseInt(interval_object.id) === parseInt(interval_id)) {
          // check timestamp to see is expired
          if (Date.now() - Number(interval_object.timeStamp) > waiting_time) {
            // if difference is greater then defined timeout/waiting time
            // remove this interval
            clearInterval(interval_id);
            // save this index for later delete
            remove_this_index = index;

            // console.log('condition  ' + condition + ' DID NOT fulfil IN ' + waiting_time / 1000 + 'sec .. aborting .....');

            if (typeof expired_callback !== 'undefined' && expired_callback !== null ) {
              expired_callback(arg_data);
            };
          };
          
        };
      });

      if (remove_this_index !== null) {
        window.wait_interval_array.splice(remove_this_index, 1);
      };

      remove_this_index = null;

    }; // END OF if timeout is NOT 'inf' and it is NOT undefined

  } 
  else {

    // IF CONDITION IS TRUE !!!!!!!!!1

    if (timeout == 'inf') {

      // if timeout is 'inf' just run callback function 
      // BUT DONT STOP THE LISTENER !!!!!!!!

      callbackFunction(arg_data);
    } else {

      var remove_this_index = null;

      $.each(window.wait_interval_array, function (index, interval_object) {

        if (parseInt(interval_object.id) === parseInt(interval_id)) {
          clearInterval(interval_id);
          // save this index to later remove
          remove_this_index = index;
          callbackFunction(arg_data);
        };
      });

      if (remove_this_index !== null) {
        window.wait_interval_array.splice(remove_this_index, 1);
      };

      remove_this_index = null;

    }; // end if timeout is NOT 'inf'

  }; // END IF IF CONDITION IS TRUE

};



function get_comp_data( arg_elem, arg_parent_selector, this_module ) {
  
  var this_comp_id = arg_elem.closest( arg_parent_selector || '.cit_comp' )[0].id;
  var data = this_module.cit_data[this_comp_id];
  var rand_id =  this_module.cit_data[this_comp_id].rand_id;
  
  return data;
  
};

/*
VAŽNO !!!!!
OVO JE LISTENER KOJI PROVJERAVA DA LI POSTOJI KOMPONENTA U HTML
AKO JE NEMA OBRIŠI SVE LISTENERE ZA TU KOMPONENTU
*/

function global_interval_listener() {
    
  setInterval(function() {
    remove_comp_listeners_if_comp_not_in_DOM();
  }, 100 );
  
};
global_interval_listener();

function remove_comp_listeners_if_comp_not_in_DOM() {

  var wait_arr_length = window.wait_interval_array.length;
  var ind;

  for (ind=wait_arr_length-1; ind >= 0; ind--) {
    var wait_obj = window.wait_interval_array[ind];
    if ( 
      wait_obj.component_id /* ako listener objek ima component_id !!!! */
      &&
      $('#'+wait_obj.component_id).length == 0 /* ako taj component_id_vise ne postoji u DOM-u */
      ) {
      console.log('BRIŠEM LISTENER ZA '  + wait_obj.component_id + ' JER VIŠE NIJE U HTML-u');
      clearInterval( window.wait_interval_array[ind].id );
      window.wait_interval_array.splice(ind, 1);
    };
  };
  
};

function cit_required( valid, data, exeptions ) {
  
  // ako nisam dao exeptions onda neka returna isti required koji dobije NIŠTA NE MIJENJA 
  if ( !exeptions ) exeptions = function ( valid_key, valid_obj, data ) { return valid_obj.required }; 
  
  var obavezni_podaci = "";
  var missing_counter = 0;
  
  $(`.cit_input`).removeClass(`missing_data`);
  
  $.each(valid, function(valid_key, valid_obj) {
    

    if (

      /* ovo znači da NE SMJE BITI FALSY ali da može biti nula */ 
      ( exeptions( valid_key, valid_obj, data ) && !data[valid_key] && data[valid_key] !== 0 )

      ||

      /* ovo znači da ako je required i ako je array mora biti barem jedan item u arrayu */
      ( exeptions( valid_key, valid_obj, data ) && data[valid_key] && $.isArray( data[valid_key] ) && data[valid_key].length == 0 )

    ) {
      missing_counter += 1;
      $(`#`+data.rand_id+`_`+valid_key).addClass(`missing_data`);
      obavezni_podaci += valid_obj.label + `<br>`;
    };



    
    
    
  });
  
  return {
    counter: missing_counter,
    podaci: obavezni_podaci,
  };
  
  
};

function obj_prop_or_arr_item(curr_prop) {

  var curr_type = 'obj_prop';
  if ( $.isNumeric(curr_prop) == true  ) curr_type = 'arr_item';
  return curr_type;

};

function get_input_data(elem, parent_object, arg_rand_id, child) {
  
  var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id; 
  var data = parent_object; // parent_object.cit_data[this_comp_id];
  
  var rand_id = arg_rand_id || data.rand_id;
  var prop = elem.id.replace( rand_id+'_', '');
  return data[prop];
  
};

function format_number_input( arg_new_value, elem, arg_decimals, arg_old_value) {
  
  var text_value = "";
  var decimals = 0;
  
  
  if ( !elem ) {
    console.error(`Nedostaje input element za format number funkciju !!!`);
    return;
  };
  
  // $(elem).val( arg_new_value || "" );
  
  var new_value = ( !arg_new_value && arg_new_value !== 0 ) ? null : cit_number( arg_new_value );
  
  if ( arg_decimals ) decimals = arg_decimals;
  
  // ako fali koliko je decimala onda probaj procitati iz data attr !!!
  if ( !arg_decimals && elem ) {
    var data_decimals = $(elem).attr("data-decimals");
    if ( data_decimals && data_decimals != "null" ) decimals = Number(data_decimals);
  };
  
  
  if ( $(elem).hasClass('number') ) {
    
    // pretvori od hrvatskog formata u američki format
    
    if ( $(elem).val() !== "" && $.isNumeric(new_value) == false ) {
      
      popup_warn(`Vrijednost mora biti brojčana!`, function() {
        
        // AKO NE POSTOJI OLD VALUE ARGUMENT
        if ( !arg_old_value && arg_old_value !== 0 ) {
          
          $(elem).val("");
          
        } else {
        
          $(elem).val( cit_format( arg_old_value, decimals ) || "");
          $(elem).trigger('focus');
          $(elem).select();
        }
        
      });
      
      return;
      
    };
    
  }; // kraj ako je input type number
  
  // ako JE FALSY A NIJE NULA
  if ( !new_value && new_value !== 0 ) new_value = null;
  
  
  if ( new_value == null ) {
    
    if ( elem ) $(elem).val("");
    if ( elem ) $(elem).attr("data-number", "null");
    
  } else {
    
    if ( elem ) {
      
      $(elem).attr("data-number", new_value);
      
      text_value = cit_format(new_value, decimals || 0);
      
      // ako JESAM dao DOM element ONDA UPIŠI U TAJ INPUT !!!!
      $(elem).val(text_value);
      
      // isto tako upiši i koliko decimala sada ima
      $(elem).attr( "data-decimals", ""+decimals );
      
    } else {
      // ako nisam dao DOM element
      text_value = cit_format(new_value, decimals || 0);
    };
    
  };

  return text_value;
  
};

function set_input_data(elem, parent_object, arg_rand_id, dont_update ) {
  
  if ( !current_input_id ) return;
  
  // var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id; 
  
  // if ( !this_comp_id ) return;
  
  var data = parent_object; // parent_object.cit_data[this_comp_id];
  
  var rand_id = arg_rand_id || data.rand_id;
  
  var old_value = get_input_data(elem, parent_object, rand_id);
  
  console.log(old_value);
  
  var new_value = $(elem).val();
  
  if ( !new_value && new_value !== 0 ) new_value = null;
  
  if ( $(elem).hasClass('number') ) {
    
    // ako ima razlomak u sebi ----------> na primjer ako želim upisati 1/2 ili 1/15 i slično 
    if ( new_value !== null && new_value.indexOf(`/`) > -1 ) {
      
      var brojnik = cit_number( new_value.split(`/`)[0] );
      var nazivnik = cit_number( new_value.split(`/`)[1] );
      
      if ( brojnik !== null && nazivnik !== null ) {
        
        if ( nazivnik === 0 ) {
          popup_warn( `Nazivnik je NULA !!!!` );
          $(elem).val(``);
          new_value = ``;
          return;
        };
        
        
        new_value = brojnik/nazivnik;
        
        
      } else {
        
        popup_warn(` Ili BROJNIK ili NAZIVNIK nisu ispravni brojevi !!!!` );
        $(elem).val(``);
        new_value = ``;
        
        return;
        
        
      };
      
      
    } 
    else if ( new_value !== null && new_value.indexOf(`*`) > -1 ) {
      
      var faktor_1 = cit_number( new_value.split(`*`)[0] );
      var faktor_2 = cit_number( new_value.split(`*`)[1] );
      
      if ( faktor_1 !== null && faktor_2 !== null ) {

        new_value = faktor_1 * faktor_2;


      }
      else {
        
        popup_warn(` JEDAN ILI OBA FAKTORA nisu ispravni brojevi !!!!` );
        $(elem).val(``);
        new_value = ``;
        return;
        
      };
        
      
    } 
    else if ( new_value !== null && new_value.indexOf(`-`) > -1 ) {
      
      var num_1 = cit_number( new_value.split(`-`)[0] );
      var num_2 = cit_number( new_value.split(`-`)[1] );
      
      if ( num_1 !== null && num_2 !== null ) {

        new_value = num_1 - num_2;


      }
      else {
        
        popup_warn(` JEDAN ILI OBA BROJA nisu ispravni !!!!` );
        $(elem).val(``);
        new_value = ``;
        return;
        
      };
        
      
    } 
    else if ( new_value !== null && new_value.indexOf(`+`) > -1 ) {
      
      var num_1 = cit_number( new_value.split(`+`)[0] );
      var num_2 = cit_number( new_value.split(`+`)[1] );
      
      if ( num_1 !== null && num_2 !== null ) {

        new_value = num_1 + num_2;


      }
      else {
        
        popup_warn(` JEDAN ILI OBA BROJA nisu ispravni !!!!` );
        $(elem).val(``);
        new_value = ``;
        return;
        
      };
        
      
    } 
    else {
      // ako nema znak / u sebi !!!!!
      new_value = cit_number( $(elem).val() );
      
    };
    
    var new_text_value = format_number_input(new_value, elem, null, old_value );
    
    console.log(new_text_value);
    
    // ako NE trebam u ovoj funkciji direktno radit update top propertija
    // onda samo vrati novi value
    if ( dont_update ) return new_value;
    
  }; // kraj ako je input type number
  
  
  var prop = elem.id.replace( rand_id+'_', '');
  
  
  if ( new_value === "" || typeof new_value === 'undefined' ) new_value = null;
  
  // ako mu nisam zabranio da napravi update
  if ( !dont_update ) {
    // ako je objekt i samo ako postoji taj property u objektu
    if ( $.isPlainObject(data) && data.hasOwnProperty(prop) ) data[prop] = new_value;
    // ako je array prop mora biti number kao npr [0]
    if ( $.isArray(data) ) {
      prop = Number(prop);
      if ( $.isNumeric(prop) ) data[prop] = new_value;
    };
    
  };
  
  
  console.log(data);
  
  // console.log( new_value );
  // console.log( data );
  
  return new_value;
  
};

function cit_place_component(parent_element, component_html, placement) {
  
    if ( !placement ) parent_element.html(component_html);
    if ( placement == 'prepend' ) parent_element.prepend(component_html);
    if ( placement == 'append' ) parent_element.append(component_html);
    if ( placement == 'after' ) parent_element.after(component_html);
    if ( placement == 'before' ) parent_element.before(component_html);
};

function get_cit_module(url, load_css) {
  
  
  var prom = $.Deferred();
  
  var url_module_id = url.replace(/\//g, '_');
  url_module_id = url_module_id.replace(/\./g, '_');
  
  var css_module_id = url_module_id.replace(/_js/gi, '_css');
  var css_url = url.replace('.js', '.css');

  if ( load_css == 'load_css' && $('#' + css_module_id).length == 0 ) {
    var css_link =
    `<link type="text/css" rel="stylesheet" id="${css_module_id}" href="${css_url}">`;
    $("head").append(css_link);
  };
  
  var token = "";
  
  if (
    typeof window.cit_user !== 'undefined' && 
    window.cit_user !== null               &&
    window.cit_user.token
  ) {
    token = window.cit_user.token;
  };
  
  // ako ne postoji ovaj customm id znači da ta scrip nije učitana
  if ( $(`#${url_module_id}`).length == 0 ) {

    $.ajax({
        headers: {
          'X-Auth-Token': token
        },
        type: "GET",
        url: url,
        dataType: "script",
        cache: true,
      })
      .done(function (module) {
      
      var json_response = null;
      
      try {
        json_response = JSON.parse(module);
      }
      catch(err) {
        
        
      };
      
      
      if ( json_response && json_response.success == false ) {
        
        if (json_response.msg) popup_error(json_response.msg);
        prom.resolve(null);
        return;
        
      };
      
      
      var module_code =
`
<script id="${url_module_id}" type="text/javascript">

(function () {
window['${url}'] = {};

var module_url = '${url}';
var this_module = window['${url}'];
${module}

$.each(module_object, function(key, item) {
  window['${url}'][key] = item;
});

}());

//# sourceURL=${url}
</script>
`;

        $('body').append(module_code);
        
        // prilikom prvog loadanja pokreni sve funkcije unutar scripts propertija
        wait_for( `typeof  window["${url}"] !== "undefined"`, function() {
          if ( window[url].scripts ) window[url].scripts();
          prom.resolve(window[url]);
        }, 50 * 1000);

      })
      .fail(function (err) {
        console.error("GREŠKA PRI UČITAVANJU MAIN MODULA !!!!!");
        if (err.msg) popup_error(err.msg);
        prom.resolve(err);
      });

  }
  else {
    if ( window[url].scripts ) window[url].scripts();
    prom.resolve(window[url]);
  };
    
  return prom;
  
};

function load_css(url) {
  
  var url_module_id = url.replace(/\//g, '_');
  url_module_id = url_module_id.replace(/\./g, '_');
  
  var css_module_id = url_module_id.replace(/_js/gi, '_css');
  var css_url = url.replace('.js', '.css');

  if ( $('#' + css_module_id).length == 0 ) {
    var css_link =
    `<link rel="stylesheet" id="${css_module_id}" href="${css_url}">`;
    $("head").append(css_link);
  };  
  
};

function load_lib(path, module) {
  if ( window[path] ) {
    console.error(`MODULE WITH PATH ${path} ALREADY EXISTS !!!!!!`);
  } else {
    window[path] = module;
  };
  
};

var STORE = {
  
  curr_chain_errors: [],

  make: function(store_name, init_store_data) {
    
    var cit_store = window.__glb_ct_str_data__;
    var all_history =  window.__glb_a_his__;
    
    if (!store_name) {
      console.error('OBAVEZNO UPISATI IME STORE-a !!!');
      return;
    };

    if (!init_store_data) {
      console.error('OBAVEZNO UPISATI INIT DATA STORE-a !!!');
      return;
    };
    
    var store_name_exists = false;
    $.each(cit_store, function (key_name, init_store_data) {
      if (key_name == store_name) store_name_exists = true;
    });

    if ( store_name_exists == true ) {
      console.error(`IME ${store_name} VEĆ POSTOJI !!!`);
      return;
    };

    if (!cit_store[store_name]) cit_store[store_name] = init_store_data;

    if (!all_history[store_name]) all_history[store_name] = [];
    
    return true;
  },

  history: function(store_name) {
    
    var cit_store = window.__glb_ct_str_data__;
    var all_history =  window.__glb_a_his__;
    
    if (!store_name) {
      console.error('OBAVEZNO UPISATI IME STORE-a !!!');
      return;
    };
    
    // deep copy od historija
    var cit_store_history_copy = JSON.parse( JSON.stringify( all_history[store_name] ) );
    // deep copy od trenutnog stanja
    var cit_store_copy = JSON.parse( JSON.stringify( cit_store[store_name] ) );
    
    // dodaj kopiju trenutnog stanja u history 
    cit_store_history_copy.push(cit_store_copy);
    
    return cit_store_history_copy;
  },
    
  find: function(store_name, prop_chain, is_update) {
    
    
    
    // resetiram na početu da li je našao taj property path
    var find_success = false;
    var prop_type = null;
    
    var cit_store = window.__glb_ct_str_data__;
    var all_history =  window.__glb_a_his__;
    
    if ( store_name == 'all_stores' ) {
      var all_stores_copy_string = JSON.stringify( cit_store );
      var all_stores_copy = JSON.parse( all_stores_copy_string );
      return all_stores_copy;
    };
    
    
    if ( !prop_chain ) {
      console.error('OBAVEZNO UPISATI TERGET PROPERTY (prop_chain) !!!');
      return;
    };
    
    if ( store_name == 'all_stores' || !prop_chain ) return;
      
    if (!store_name) {
      console.error('OBAVEZNO UPISATI IME STORE-a !!!');
      return;
    };
    
    
    if ( !cit_store[store_name] && cit_store[store_name] !== null ) {
      
      // console.error('NE POSTOJI OVAJ STORE !!!');
      
      return null;
      
    };
    
    
    if (prop_chain == 'all') {
      
      var store_copy = null;
      if ( cit_store[store_name] ) {
        var store_copy_string = JSON.stringify( cit_store[store_name] );
        store_copy = JSON.parse( store_copy_string );
      };
      return store_copy;
    };

    // na početku mi je found data cijeli store
    var found_data = cit_store[store_name];
    // na početku mi je current chain samo store name jer je to početni property
    var curr_chain = store_name;
    
    var curr_prop_copy = null;
    
    // ako je argument za prop chain all onda sam već gore vratio cijeli store objekt i ne trebam ići dalje
    if (prop_chain == 'all') return;

    var prop_dots = prop_chain.replace(/\[/g,'.' );
    var prop_dots = prop_dots.replace(/\]/g,'' );
    // dobijem 'mama.tata.0.arr.2'
    var prop_dots = prop_dots.replace(/\.\.\./g,'.' );
    var prop_dots = prop_dots.replace(/\.\./g,'.' );
    // brišem višak točaka (ako ih ima) i dobijem
    // 'mama.tata.0.arr.2'
    var prop_dots = prop_dots.split('.');
    
    
    // ponekad se dogodi na postoje na kraju i na početku itemi koji su prazni stringovi !!!!
    //  OBRIŠI IH !!!
    var last_prop_dots_index = prop_dots.length - 1;
    // ako je zadnji item prazan string
    if ( prop_dots[last_prop_dots_index] == '' ) prop_dots.splice(last_prop_dots_index, 1);
    // ako je prvi item prazan string
    if ( prop_dots[0] == '' ) prop_dots.splice(0, 1);

    var this_store = this;
    
    // sada loop po svim itemima u arrayu propertija
    $.each(prop_dots, function(prop_ind, curr_prop) {

      if ( found_data && curr_prop !== '' ) {
        // ako je nekada prije prijavio grešku da ovaj prop ne postoji
        // A SADA POSTOJI !!!
        // onda ga obriši iz error arraja !!!!!!
        var chain_error_index = $.inArray(curr_chain, this_store.curr_chain_errors);
        if ( chain_error_index > -1 ) this_store.curr_chain_errors.splice(chain_error_index, 1);

        prop_type = obj_prop_or_arr_item(curr_prop);

        // ako je ovo array onda ce curr prop biti broj npr arr[2]
        if ( prop_type == 'arr_item' ) curr_prop = Number(curr_prop);
        // kopiram prop koji je sada trenutno
        curr_prop_copy = curr_prop;
        // dodajem na dosadašnji chain ovaj trenutni prop
        // (na početku je chain samo ime STORA)
        curr_chain += ('.'+curr_prop);
        
        // ako je zadnji propery onda je uspješno pronašao property !!!!!!!!!
        if ( prop_ind == prop_dots.length - 1 ) {

          find_success = true;
          
        } 
        else {
          // ako nije zadnji property u string chainu onda 
          // pretvory found prop u njegov child object
          // tj pomakni se jednu kariku u chainu 
          // NAPOMINJEM - NA POČETKU JE FOUND DATA SAMO STORE[ime_stora]
          found_data = found_data[curr_prop];

        };
        
        // kraj ako trenutni property chain postoji  
        // i ako sam property string nije ""
      } 
      else {
        if ( $.inArray(curr_chain, this_store.curr_chain_errors) == -1 ) {
          console.error(curr_chain + ' NE POSTOJI !!!!! -----> AKO TAJ PROPERTY VIŠE NEĆE BITI PRISUTAN OBRIŠITE LISTENER ZA NJEGA !!!');
          this_store.curr_chain_errors.push(curr_chain);
        };
      };

    }); // kraj loopa po svim stringovima unutar string chain-a
    
    if ( find_success == true ) find_success = found_data[curr_prop_copy];
    
    if ( is_update && find_success !== false ) {
      
      return {
        find_success: find_success,
        found_data: found_data,
        curr_prop: curr_prop_copy,
        prop_type: prop_type,
      }
      
    } else if ( is_update && find_success === false ) {
      return false;
      
    } else if ( !is_update && find_success ) {
      find_success = JSON.parse( JSON.stringify( find_success ) );
      return find_success;
    };

  },
  
  update: function(store_name, prop_chain, new_data, desc, action_type ) {
    
    var cit_store = window.__glb_ct_str_data__;
    var all_history =  window.__glb_a_his__;
    
    var update_success = false;
    
    if (!store_name) {
      console.error('OBAVEZNO UPISATI IME STORE-a !!!');
      return;
    };
    
    if ( !prop_chain ) {
      console.error('OBAVEZNO UPISATI TERGET PROPERTY (prop_chain) !!!');
      return;
    }; 
    
    
    if ( typeof new_data == 'undefined' && action_type !== 'DELETE' ) {
      console.error('OBAVEZNO UPISATI NEW DATA ZA UPDATE CIT STORE-a !!!');
      return;
    };  


    if (!desc) {
      console.error('OBAVEZNO UPISATI DESC ZA UPDATE CIT STORE-a !!!');
      return;
    };
    
    var data = this.find( store_name, prop_chain, true);
    
    if ( !data ) return;
    
    var found_data = data.found_data;
    var curr_prop = data.curr_prop;
    var prop_type = data.prop_type;
    
    
    desc = (action_type == 'DELETE' ? 'DELETE ' : '') + prop_chain +  ' >>>>> ' + desc;

    console.log(`%c CHAIN: ${prop_chain} STORE UPDATE DESC: ${desc} `, "background-color: green; color: white;"); 
    
    var cit_store_copy = JSON.stringify(cit_store[store_name]);
    all_history[store_name].push( JSON.parse(cit_store_copy) );

    // ako array od historja ima više od 10 itema onda izbaci prvi tj. najstariji
    if ( all_history[store_name].length > 10 ) all_history[store_name].shift();
    
    
    // ako property VEĆ POSTOJI !!!
    if ( typeof found_data[curr_prop] !== 'undefined' ) {

      // ako nema action type to znači da ga tertiram kao EDIT ili novi upis !!!!!

      if ( !action_type ) {
        found_data[curr_prop] = new_data;
        update_success = true;
        cit_store[store_name].desc = desc;
      };

      if ( action_type == 'DELETE' ) {

        if ( prop_type == 'arr_item' ) {
          // delete array item
          found_data.splice(curr_prop, 1);
          update_success = true;
          cit_store[store_name].desc = desc;
        } 
        else if ( prop_type == 'obj_prop' ) {

          // delete object property
          delete found_data[curr_prop];
          update_success = true;
          cit_store[store_name].desc = desc;
        };

      }; // kraj ako je DELETE


    // kraj ako ovaj property postoji  
    } 
    else {

      // ako property ne postoji !!!!!
      if ( prop_type == 'arr_item' ) {

        if ( curr_prop !== found_data.length ) {
          // ako NE POSTOJI !!! current property
          // nije bitno jel action type EDIT ILI DELETE
          // ako je user u string chainu naveo krivi index array-a
          // javit ću mu grešku bez obzira na action type


          console.error(
            'KRIVI INDEX OD ARRAYA - OVAJ ARRAY IMA '
            + (found_data.length-1)
            + ' ITEMA, STOGA SLJEDEĆI INDEX TREBA BITI '
            + found_data.length + ' A SADE JE ' + curr_prop
          );
          console.error('OVO JE PROPERTY CHAIN: ' + prop_chain);

        } else {
          // ako je user upisao sljedeći index po redu tj kako treba
          // onda samo push data to array
          if ( action_type !== 'DELETE' ) {
            found_data.push(new_data);
            update_success = true;
            cit_store[store_name].desc = desc;

          } else {
            console.error(
              `
              NE MOGU OBRISATI ITEM S INDEXOM ${curr_prop}
              JER TAJ INDEX NE POSTOJI ----> 
              ARRAY IMA MAX INDEX ${found_data.length-1} :(
              `
            );
          }; // kraj ako je/nije DELETE ITEMA U ARRAJU

          // kraj provjere jel ovo sljedeći index u arraju 
          // npr array ima 3 itema i indexe od 0 do 2 i sada user upiše index 3 tj 4. član arraja  
        };

      // kraj ako je array 
      } 
      else if ( prop_type == 'obj_prop' ) {

        if ( !action_type ) {
          found_data[curr_prop] = new_data;
          update_success = true;
          cit_store[store_name].desc = desc;
        };

        if ( action_type == 'DELETE' ) {
          console.error(`PROPERTY ${prop_chain} NE POSTOJI I NE MOGU GA OBRISTAI !!!`);
        };

      }; // kraj ako je property

    }; // kraj ako property  za update ne postoji

    if ( update_success == true ) update_success = found_data[curr_prop];
    
    return update_success;
    
    
  },
  
  
}; // END OF  STORE

function compare_functions( func_1, func_2 ) {

  var func_are_same = false;

  var func_1_string = String(func_1);
  var func_2_string = String(func_2);

  var func_1_start = func_1_string.indexOf('{') || 0;
  var func_2_start = func_2_string.indexOf('{') || 0;

  var func_1_body = func_1_string.substring(func_1_start);
  var func_2_body = func_2_string.substring(func_2_start);

  if ( func_1_body == func_2_body ) func_are_same = true;
  
  return func_are_same;

};

function remove_store_listener(store_name, prop_chain, cb) {
  
  if ( !store_name ) {
    console.error('NEDOSTAJE STORE NAME !!!');
    return;
  };

  
  if ( !prop_chain ) {
    console.error('NEDOSTAJE PROP CHAIN !!!');
    return;
  };
  
  if ( !cb ) {
    console.error('NEDOSTAJE CALLBACK FUNC !!!');
    return;
  };
  
  // obriši listener ako već postoji !!!!!
  var wait_arr_length = window.wait_interval_array.length;
  
  for (ind=wait_arr_length-1; ind >= 0; ind-- ) {
    var wait_obj = window.wait_interval_array[ind];
    if (
      (wait_obj.prop_chain && wait_obj.prop_chain == prop_chain)
      &&
      (wait_obj.store_name && wait_obj.store_name == store_name)
      &&
      compare_functions( wait_obj.callbackFunction, cb ) == true
      ) {
      clearInterval( window.wait_interval_array[ind].id );
      window.wait_interval_array.splice(ind, 1);
    };
    
  }; // end of for loop
};


function store_listener(store_name, prop_chain, callback, component_id ) {

  var old_data = '';

  try {
    old_data = JSON.stringify( STORE.find(store_name, prop_chain) ); 
  } catch (error) {
    console.error(error);
    old_data = 'error';
  };

  if ( old_data == 'error' ) return;
    
  function check_data_changed() {

    var state_change = false;
    var data_now = '';
    try {
      data_now = JSON.stringify( STORE.find(store_name, prop_chain) ); 
    } catch (error) {
      // console.error(error);
    };

    if ( old_data !== data_now ) state_change = true;

    old_data = data_now;

    return state_change;
    
  };


  // obriši listener ako već postoji !!!!!
  remove_store_listener( store_name, prop_chain, callback );
  
  var wait_id = wait_for(check_data_changed, callback, 'inf');
  
  $.each(window.wait_interval_array, function( ind, wait_obj ) {
    // pronađi ovaj listener koji si upravo stvorio i dodaj mu ove propertije:  
    if ( wait_obj.id == wait_id ) {
      window.wait_interval_array[ind].prop_chain = prop_chain;
      window.wait_interval_array[ind].store_name = store_name;
      window.wait_interval_array[ind].component_id = component_id;
    };
    
  });
  
  return wait_id;

};

/*
----------------------------------------------------------
FUNKCIJE ZA DROP LIST KOMPONENTU
----------------------------------------------------------
*/


function toggle_global_progress_bar(show, duration) {
  
  if (show) {
    
    
    $('#cit_global_precentage').removeAttr(`style`);
    
    if (duration) {
      $('#cit_global_precentage').css(`transition`, `width ${duration}s ease`);
    };
    
    
    $('#cit_global_progress_bar').addClass('cit_show');
    
  };
  
  if (!show) $('#cit_global_progress_bar').removeClass('cit_show');
}

function remove_sort_arrows( parent_box ) {

  // u tablici statusa NE SMJEM SORTIRATI
  // stoga obriši strelice gore / dolje
  parent_box.find(`.header_column`).find(`.fa`).each(function() {
    $(this).remove();
  });

  // remove click evente
  parent_box.find(`.header_column`).each(function() {
    $(this).off(`click`);
  });
  
};

function remove_list_buttons( parent_box ) {
  
  parent_box.find(`.result_row_edit_btn`).each(function() {
    $(this).off(`click`);
    $(this).remove();
  });

  parent_box.find(`.result_row_reply_btn`).each(function() {
    $(this).remove();
    $(this).off(`click`);
  });
  
  parent_box.find(`.result_row_seen_btn`).each(function() {
    $(this).remove();
    $(this).off(`click`);
  });
  
};


var button_cols = [ 
  `button_reply`,
  `button_edit`,
  `button_delete`,
  `button_seen`,
];

function sort_cit_result( array, props_sort) {

  var sort_rules = [];

  // vrti po arrayu
  $.each( props_sort, function( sort_index, sort_obj ) {
    // vrti po keys --- > ionako svaki objekt ima samo jedan key !!!! :)
    $.each(sort_obj, function( sort_key, sort_direction ) {
      if ( sort_direction == 1 ) sort_rules.push(sort_key);
      if ( sort_direction == -1 ) sort_rules.push(`!`+sort_key);
    });
  });


  var sorted_array = multisort(array, sort_rules);
  
  return sorted_array;
  
};


function create_result_header(search_object, props) {
  
  delete search_object.__v;
  
  var count_cit_hide = props.hide_cols ? props.hide_cols.length : 0;
  // broj stupaca je zapravo ukupan broj propertija u objektu minus propertiji koje treba sakriti
  var number_of_columns = Object.keys(search_object).length - count_cit_hide;

  
  if ( props.show_cols ) {
    number_of_columns = props.show_cols.length;
  };
  
  
  if ( !props.show_cols ) {
    if ( props.button_reply ) number_of_columns += 1;
    if ( props.button_edit ) number_of_columns += 1;
    if ( props.button_delete ) number_of_columns += 1;
    if ( props.button_seen ) number_of_columns += 1;
  };

  
  
  var col_widths_sum = 0;
  if ( props.col_widths ) {
    $.each(props.col_widths, function(w_ind, width) {
      col_widths_sum += Number(width);
    });
  };
  
  
  var columns = '';
  var col_count = 0;
  
  var cit_random = cit_rand();
  
  function run_col_creation(key, value) {
    
    var curr_time = Date.now();
    
    var style_hide = '';
    
    if ( 
      $.inArray( key, button_cols ) == -1 && // ako nije neki gumb
      props.hide_cols &&
      $.inArray( key, props.hide_cols ) > -1 
    )  { style_hide = 'display: none;'; }
    
    if ( 
      $.inArray( key, button_cols ) == -1 && // ako nije neki gumb
      props.show_cols ) {
      // ako je key u show arrayu
      if ( $.inArray( key, props.show_cols ) > -1 ) style_hide = '';
      // ako key nije u arrayu
      if ( $.inArray( key, props.show_cols ) == -1 ) style_hide = 'display: none;';
    };
    
    if (
      $.inArray( key, button_cols ) == -1 && // ako nije neki gumb
      props.col_widths && 
      props.col_widths[col_count] == 0
    ) { style_hide = 'display: none;'; };

    
    
    var sirina_columne = `calc(${ props.col_widths ? props.col_widths[col_count]/col_widths_sum*100 : (100/number_of_columns)}% + 1px)`;
    // var sirina_columne = (props.col_widths && props.col_widths[col_count]) ? props.col_widths[col_count] : 1; 
    
    if ( style_hide == 'display: none;' ) sirina_columne = 0;
    
    var property_index = null;
    if ( props.show_cols && props.custom_headers ) {
      $.each(props.show_cols, function(pr_ind, propery_name) {
        if ( key == propery_name ) property_index = pr_ind;
      });
    };
    
    var sort_class = '';
    
    if ( props.sort ) {
      // promjenio sa da sort bude array umjesto objekt jer kada ga šaljem s ajaxom ako je objekt
      // moguće je da promjeni redosljed od keys što ne valja jer sort mora biti po točnom redu
      // ali ako šaljem array objekata onda ajax neće promjeniti redosljed
      
      // vrti po arrayu
      $.each( props.sort, function( sort_index, sort_obj ) {
        // loop po keys --- > ionako svaki objekt ima samo jedan key !!!! :)
        $.each(sort_obj, function( sort_key, sort_direction ) {
          if ( sort_key == key ) {
            if (sort_direction == 1) sort_class = 'cit_asc';
            if (sort_direction == -1) sort_class = 'cit_desc';
          };
        });
      });
      
      
    };
    
    
    var header_col_content = `&nbsp;`;
    /* ako nije neki gumb */
    if ( $.inArray( key, button_cols ) == -1 ) {
      header_col_content = props.custom_headers ? props.custom_headers[property_index] : key.replace(/_/g, " ").toUpperCase();
    };
    
    
    var header_col_arrows = 
`
<i class="fa fa-caret-up"></i>
<i class="fa fa-caret-down"></i>
`;
    
    // AKO JESTE NEKI BUTTON !!!!
    if ( $.inArray( key, button_cols ) > -1 ) {
      header_col_arrows = "";
    };
    
    // ako namjerno želim sakriti sor strelice
    if (props.hide_sort_arrows == true ) {
      header_col_arrows = "";
    };
    
    // prije nego postaviš imena propertija kao naslove u stupcima replace sve underscore znakove sa space
    columns += 
    `
    <div  id="${ cit_random + '_' + key}" class="search_result_column header_column ${sort_class}" 
          style="width: ${sirina_columne}; ${style_hide}">

      ${ header_col_content }

      ${ header_col_arrows }

    </div>

    `;
    
    // if ( sirina_columne > 0 ) 
    
    col_count += 1;
 
  };
  
  /*
  if ( props.return && Object.keys(props.return).length > 0 ) {
    
    // ako sam definirao specifična polja za return
    $.each(props.return, function(return_key, return_val) {  
      $.each(search_object, function(item_key, item_value) {
        if ( item_key == return_key && item_key !== "_id" ) run_col_creation(item_key, item_value);
      }); 
    });  
    
  } else
    
  */  
    
  if ( props.show_cols && props.show_cols.length > 0 ) {
    
    $.each(props.show_cols, function(show_ind, show_col_key) {  
      $.each(search_object, function(item_key, item_value) {
        if ( item_key == show_col_key && item_key !== "_id" ) run_col_creation(item_key, item_value);
      }); 
    }); 
    
  } else {
  
    $.each(search_object, function(item_key, item_value) {
      if (item_key !== "_id" ) run_col_creation(item_key, item_value);
    });
    
  };
  
  return columns;
  
  
}; // kraj rezult header


function create_result_row(search_object, props, is_sum_row ) {

  delete search_object.__v;
  
  var count_cit_hide = props.hide_cols ? props.hide_cols.length : 0;
  // broj stupaca je zapravo ukupan broj propertija u objektu minus propertiji koje treba sakriti
  var number_of_columns = Object.keys(search_object).length - count_cit_hide;

  if ( props.show_cols ) {
    number_of_columns = props.show_cols.length;
  };
    
  if ( !props.show_cols ) {
    if ( props.button_reply ) number_of_columns += 1;
    if ( props.button_edit ) number_of_columns += 1;
    if ( props.button_delete ) number_of_columns += 1;
    if ( props.button_seen ) number_of_columns += 1;
  };
  
  
  var columns = '';
  var col_count = 0;
  
  var col_widths_sum = 0;
  if ( props.col_widths ) {
    $.each(props.col_widths, function(w_ind, width) {
      col_widths_sum += Number(width);
    });
  };
  
  
  function run_col_creation(key, value) {
    
    // VAŽNO !!!!!
    // KADA JE PROPERTY OZNAČEN KAO HIDDEN ONDA IPAK IZGENERIRAM ĆELIJU ALI JE SAKRIJEM
    // TJ BUDE DISPLAY NONE !!!!!!
    var style_hide = '';
    
    if ( 
      $.inArray( key, button_cols ) == -1  // ako  neki gumb
      && 
      props.hide_cols // ako postoji hide cols
      &&
      $.inArray( key, props.hide_cols ) > -1 // i ako je ovaj key u hide cols
    ) {
      style_hide = 'display: none;';
    };
    
    if (
      $.inArray( key, button_cols ) == -1 // ako  nije neki gumb
      && 
      props.show_cols 
    ) {
      // ako je key u show arrayu
      if ( $.inArray( key, props.show_cols ) > -1 ) style_hide = '';
      // ako key nije u arrayu
      if ( $.inArray( key, props.show_cols ) == -1 ) style_hide = 'display: none;';
    };
    
    if ( 
      $.inArray( key, button_cols ) == -1 // ako nije neki gumb
      && 
      props.col_widths 
      && 
      props.col_widths[col_count] == 0 
    ) {
      style_hide = 'display: none;';
    };
    
    var sirina_columne = `calc(${ props.col_widths ? (props.col_widths[col_count]/col_widths_sum*100) : (100/number_of_columns)}% + 1px)`;
    
    // var sirina_columne = (props.col_widths && props.col_widths[col_count]) ? props.col_widths[col_count] : 1;
    
    if ( style_hide == 'display: none;' ) sirina_columne = 0;
    
    // ----------------- AKO JE VALUE ARRAY KAO NA PRIMJER car_plates UNUTAR USER OBJEKATA -----------------
    // ----------------- AKO JE VALUE ARRAY KAO NA PRIMJER car_plates UNUTAR USER OBJEKATA -----------------
    
    if ( $.isArray(value) ) {
      
      if ( key == 'car_plates') {
        
        
      };
      
      
    };

    // ako je value string tipa i ako u sebi sadrži .png ili .jpg
    if (
      $.inArray( key, button_cols ) == -1 && // ako  neki gumb
      $.isArray(value) == false && 
      String(value).indexOf(`target="_blank"`) == -1 && // ovo znači da moram biti sigiran na možda nisam napravio a tag već unaprijed
      ( String(value).indexOf('.png') > -1 || String(value).indexOf('.jpg') > -1 )
    ) {
      // onda ga tretiraj kao sliku !!!! tj wrepaj ga u img tag
      value = `<img src="/img/${value}" class="img-responsive cit_search_result_image">`;
    };
    
    
    // ako je key buttons
    // onda je value zapravo array buttona koje treba prikazati
    
    if ( key == `button_reply` ) {
      value = `<div class="result_row_reply_btn"><i class="fas fa-reply"></i></div>`;
      if ( search_object.hide_reply ) value = "";
    };    
    
    
    if ( key == `button_edit` ) {
      value = `<div class="result_row_edit_btn"><i class="fas fa-pencil-alt"></i></div>`;
      if ( search_object.hide_edit ) value = "";
    };
    
    if ( key == `button_delete` ) {
      value = `<div class="result_row_delete_btn"><i class="fas fa-trash-alt"></i></div>`;
      if ( search_object.hide_delete) value = "";
    };
    
    if ( key == `button_seen` ) {
      value = `<div class="result_row_seen_btn"><i class="fas fa-eye"></i></div>`;
      if ( search_object.hide_seen) value = "";
    };
    
    

    var format_style = "";
    // ako postoji property format_cols i ako je broj
    // to znači da taj property treba formatirati kao broj s tim brojem decimala
    if ( 
      props.format_cols &&
      props.format_cols.hasOwnProperty(key) &&
      typeof props.format_cols[key] == "number"
    ) {
      // align u desno da ako je broj
      format_style = `justify-content: flex-end;`;
      var decimals = props.format_cols[key];
      value = $.isNumeric(value) ? cit_format(Number(value), decimals ) : "";
    };

    // ako postoji property format_cols i ako je DATE
    // to znači da taj property treba formatirati kao DATE
    if ( props.format_cols && props.format_cols[key] && props.format_cols[key] == "date") {
      format_style = `justify-content: center;`;
      value = $.isNumeric(value) ? cit_dt( Number(value) ).date : "";
    };

    // ako postoji property format_cols i ako je TIME
    // to znači da taj property treba formatirati kao TIME
    if ( props.format_cols && props.format_cols[key] && props.format_cols[key] == "time") {
      format_style = `justify-content: center;`;
      value = $.isNumeric(value) ? cit_dt( Number(value) ).time : "";
    };
    
    
    // ako postoji property format_cols i ako je DATE_TIME
    // to znači da taj property treba formatirati kao DATE_TIME
    if ( props.format_cols && props.format_cols[key] && props.format_cols[key] == "date_time") {
      
      format_style = `justify-content: center;`;
      
      if ( $.isNumeric(value) ) {
        var date_time_obj = cit_dt( Number(value) );
        value = date_time_obj.date + " " + date_time_obj.time;
      } else {
        value = "";
      };
      
    };
    
    if ( props.format_cols && props.format_cols[key] && props.format_cols[key] == "center" ) {
      format_style = `justify-content: center;`;
    };
    
    if ( props.format_cols && props.format_cols[key] && props.format_cols[key] == "right" ) {
      format_style = `justify-content: right;`;
    };
    
    if ( props.format_cols && props.format_cols[key] && props.format_cols[key] == "left" ) {
      format_style = `justify-content: left;`;
    };
    
    
    
    
    var cell_value = (value !== null && typeof value !== "undefined" ) ? value : "";
    // if ( typeof cell_value == "string" ) cell_value = esc_html(cell_value);
    
    columns += 
    `
    <div  class="search_result_column ${key}" style="width: ${sirina_columne}; ${style_hide} ${format_style}">
      ${ cell_value }
    </div>

    `;
    
    

    col_count += 1;    
    
  };
  
  /*
  if ( props.return && Object.keys(props.return).length > 0 ) {
    
    $.each(props.return, function(return_key, return_val) {  
      $.each(search_object, function(item_key, item_value) {
        if ( item_key == return_key && item_key !== "_id" ) run_col_creation(item_key, item_value);
      }); 
    }); 
    
  } 
  else
  */  
    
  if ( props.show_cols && props.show_cols.length > 0 ) {
    
    $.each(props.show_cols, function(show_ind, show_col_key) {  
      $.each(search_object, function(item_key, item_value) {
        if ( item_key == show_col_key && item_key !== "_id" ) run_col_creation(item_key, item_value);
      }); 
    }); 
    
  } 
  else {
  
    $.each(search_object, function(item_key, item_value) {
      if ( item_key !== "_id" ) run_col_creation(item_key, item_value);
    });
    
  };

  return columns;
};


function create_cit_result_list( result_array, props, input_id ) {
  
  
  if ( props.button_reply ) {
    $.each( result_array, function( result_index, result ) {
      result_array[result_index].button_reply = true;
    });
  };
  
  
  if ( props.button_edit ) {
    $.each( result_array, function( result_index, result ) {
      result_array[result_index].button_edit = true;
    });
  };
  
  if ( props.button_delete ) {
    $.each( result_array, function( result_index, result ) {
      result_array[result_index].button_delete = true;
    });
  };
  
  
  if ( props.button_seen ) {
    $.each( result_array, function( result_index, result ) {
      result_array[result_index].button_seen = true;
    });
  };
  
  
  var final_result = '';
  
  if ( result_array.length > 0 ) {

    // POŠTO SAM NAKNADNO DODAVAO POLJA U BAZI
    // sada imam nejednak broj polja u različitim db dokumnetima
    // zato tražim dokument sa maksimalnim brojem polja 
    // i to uzimam kao referencu za polja
    var max_property_count = 0;
    var index_of_max_property_object = null;

    $.each(result_array, function(search_arr_index, search_object) {

      if ( Object.keys(search_object).length > max_property_count ) {
        max_property_count = Object.keys(search_object).length;
        index_of_max_property_object = search_arr_index;
      };

    });

    var result_rows_HTML = '';

    var list_class = props.list_class ? props.list_class : ''; 
    
    
    var header_row_HTML = 
    `
    <div class="search_result_row header ${list_class}">
      ${create_result_header(result_array[index_of_max_property_object], props)}
    </div>
    `;
    
    
    var sum_row = {};

    $.each(result_array, function(search_arr_index, search_object) {
      
      var cit_random = cit_rand();

      // usporedi sve propertije od trenutnog objekta sa objektom koji ima najviše propertija

      $.each(result_array[index_of_max_property_object], function(max_obj_key, max_obj_value) {

        // VAŽNO !!!!!! - ako trenutni objekt NEMA neki od propertija koji ima referentni objekt
        // VAŽNO !!!!!! - ako trenutni objekt NEMA neki od propertija koji ima referentni objekt
        // VAŽNO !!!!!! - ako trenutni objekt NEMA neki od propertija koji ima referentni objekt

        // onda ga kreiraj i dodaj mu vrijednost null !!!!!
        if ( typeof search_object[max_obj_key] == 'undefined' ) {
          search_object[max_obj_key] = null;
        };
        
        // popuni sum row objekt sa svim propsima od objketa koji ima najviše propsa
        sum_row[max_obj_key] = null;

      });
      
      
      
      if ( props.sums ) {
    
        $.each(result_array, function(res_index, res_object) {
          
          $.each(res_object, function(res_key, res_value) {
            
            // samo ako se ovaj key nalazi u arraju od props.sums
            if ( props.sums.indexOf(res_key) > -1 ) sum_row[res_key] += cit_number(res_value) || 0;
             
          });
          
        });

      };
      
      

      var sifra_ili_id_class = '';
      if ( search_object._id ) sifra_ili_id_class = '_id';
      
      if ( 
        !sifra_ili_id_class 
        && 
        (search_object.sifra || search_object.sifra == 0)
      ) sifra_ili_id_class = 'sifra';


      // pogledaj jel ima id  
      var row_unique_id = search_object._id ? search_object._id : '';
      // ako nema id onda gledaj jel ima sifru
      if ( !row_unique_id ) row_unique_id = (search_object.sifra || search_object.sifra == 0 || search_object.sifra == "0") ? search_object.sifra : '';
      // ako nema niti sifru onda kreiraj uniq id od vremena i od indexa ovog loopa
      if ( !row_unique_id ) row_unique_id = Date.now() + '_' + search_arr_index;
      
      
      var result_row_HTML = 
          `
          <div  class="search_result_row ${list_class} ${sifra_ili_id_class}" 
                id="${ cit_random + '_list_item_' + row_unique_id }"
                data-id="${ search_object._id || search_object.status_id || search_object.sifra }"
                >

            ${create_result_row(search_object, props)}

          </div>

          `;
      
          result_rows_HTML += result_row_HTML;
      
      
      
      
    }); 
    // kraj loopa po array koji sam dobio
    
    
    var sum_row_HTML = ``;
    
    if ( props.sums ) {
      
      var sum_row_HTML = 
`
<div  class="search_result_row ${list_class} sum_row" id="${ cit_rand() + "_sum_row" }" data-id="${ "_________" }">
  ${create_result_row(sum_row, props)}
</div>
`;
    };
    
    
    var page_row_HTML = ``;
    
    
    // ----------------------------------  PAGE NAVIGATION ----------------------------------
    // ----------------------------------  PAGE NAVIGATION ----------------------------------
    
    if ( props.get_count ) {
      
      page_row_HTML = 
`
<div  class="page_row" id="${ cit_rand() + "_page_row" }" data-id="${ "_________" }">
 
  <div class="page_num_arrows_box">
   
    <div class="page_arrow_prev"><i class="fas fa-chevron-circle-left"></i></div>
    
    <div class="page_num_box">
      <div class="page_num_curr">${ props.page || 1 }</div>
      <div class="page_num_slash">/</div>
      <div class="page_num_max">${ (props.max_pages || props.max_pages === 0) ? (props.max_pages + ` (${props.max_count})`) : "???" }</div>
    </div>
    
    <div class="page_arrow_next"><i class="fas fa-chevron-circle-right"></i></div>
    
  </div>
  
</div>
`;
      
      
      
   };
    
    // ----------------------------------  PAGE NAVIGATION ----------------------------------
    
    // ----------------------------------  PAGE NAVIGATION ----------------------------------
    
    
    // <div class="cit_table_parent" >
    // </div>

    final_result = 

`
  ${page_row_HTML}
  ${sum_row_HTML}
  ${header_row_HTML}
  <div class="search_result_table cit_scrollbar scrollbar-macosx">
    ${result_rows_HTML}
  </div>
`;
    
    
  } else {
  
    // ----------------------------- ako je result array prazan !!!!
    final_result = '<div id="nema_rezultata">Nije pronađen niti jedan zapis u bazi</div>'
  
  }; 

  
  var place_function = props.position || 'html';
  
  var default_parent = '#search_autocomplete_container';
  
  
  if ( props.parent == "return_html" ) {
    
    return final_result;
  };

  
  
  if ( props.parent == "return_html" ) return;
  
  
  
  $( props.parent || default_parent )[place_function](final_result);
  
  
  var current_input = input_id ? $('#'+input_id)[0] : null;
  
  // registriraj event klikanja na headere od tablice 
  $(`.cit_result_table .header_column`).off('click');
  $(`.cit_result_table .header_column`).on('click', header_click_sort);
  
  // ako nisam naveo parent onda je search_autocomplete_container
  
  if ( !props.parent ) {

    if ( input_id ) pozicioniraj_drop_listu(current_input);
    
    
    $('#search_autocomplete_container').data('cit_result', result_array );
    $('#search_autocomplete_container').data('cit_props', props );
    $('#search_autocomplete_container').data('cit_input_id', input_id );
    
    
    $('#search_autocomplete_container .result_row_reply_btn').off('click');
    $('#search_autocomplete_container .result_row_reply_btn').on('click', props.button_reply);
    

    $('#search_autocomplete_container .result_row_edit_btn').off('click');
    $('#search_autocomplete_container .result_row_edit_btn').on('click', props.button_edit);

    $('#search_autocomplete_container .result_row_delete_btn').off('click');
    $('#search_autocomplete_container .result_row_delete_btn').on('click', props.button_delete);

    $('#search_autocomplete_container .result_row_seen_btn').off('click');
    $('#search_autocomplete_container .result_row_seen_btn').on('click', props.button_seen);

    
    
    $('#search_autocomplete_container').css({
      'opacity': '1',
      'pointer-events': 'all'
    });

  } 
  else {
    
    // ako unutar propsa postoji definirani Parent za ovu tablicu 
    
    $(props.parent).data('cit_result', result_array );
    $(props.parent).data('cit_props', props );
    $(props.parent).data('cit_input_id', input_id );

    
        
    $(`${props.parent} .result_row_reply_btn`).off('click');
    $(`${props.parent} .result_row_reply_btn`).on('click', props.button_reply);
    

    $(`${props.parent} .result_row_edit_btn`).off('click');
    $(`${props.parent} .result_row_edit_btn`).on('click', props.button_edit);

    $(`${props.parent} .result_row_delete_btn`).off('click');
    $(`${props.parent} .result_row_delete_btn`).on('click', props.button_delete);

    $(`${props.parent} .result_row_seen_btn`).off('click');
    $(`${props.parent} .result_row_seen_btn`).on('click', props.button_seen);
    
    
    
    $(`${props.parent} .page_arrow_prev`).off('click');
    $(`${props.parent} .page_arrow_prev`).on('click', cit_pagination);
   
    $(`${props.parent} .page_arrow_next`).off('click');
    $(`${props.parent} .page_arrow_next`).on('click', cit_pagination);
   
    
    
   
    
  };
  
  
}; // kraj create result list



function cit_pagination() {
  
  
  var cit_result_table = $(this).closest(`.result_table_inside_gui`);
  
  var page_num_curr = cit_result_table.find(`.page_num_curr`);
  
  /*
  
  <div class="page_num_curr">5</div>
  <div class="page_num_slash">/</div>
  <div class="page_num_max">28</div>
  
  */
  var props = cit_result_table.data('cit_props');
  var input_id = cit_result_table.data('cit_input_id');
  var search_string = input_id ? $(`#` + input_id).val() : null;
    
  console.log( this );
  console.log( props );
  
  if ( !props.page ) props.page = 1;
  
  
  var run_search = false;
  
  if ( $(this).hasClass(`page_arrow_prev`) ) {
    
    if ( props.page > 1 ) {
      run_search = true;
      props.page -= 1;
      page_num_curr.text(props.page);
    };
    
  }
  else {
    
    if ( props.page + 1 <= props.max_pages ) {
      run_search = true;
      props.page += 1;
      page_num_curr.text(props.page);
    };
    
  };
  
  
  // ponovo upiši update od propsa sa novim page-om
  // ponovo upiši update od propsa sa novim page-om
  if ( cit_result_table ) {
    cit_result_table.data('cit_props', props);
  };
  
  
  if ( !run_search ) return;
  
  
  search_database(search_string, props).then(
    function( remote_result ) { 
      create_cit_result_list( remote_result.docs, props, input_id || null ); 
      
    },
    function( error ) { 
      console.error(`SEARCH DATABASE ERROR ZA:\n${props.desc}`)
      console.error( error );
    }
  );

  
};


function search_database( trazi_string, props, parallel ) {

  
  
  if(props?.desc) console.log(`---------- DB SEARCH ------------: ` + props.desc );
  
  // inače je current input uvijek polje na koje je user kliknuo ili na njemu traži nešto
  var curr_input = $('#' + current_input_id );
  
  // ALI !!!!!!! postoje slučajevi kada generiram tablicu automatski i user nije niti na jednom input polju
  // tada je parent neki objekt koji nije input polje
  
  if ( props.parent && props.parent !== '#search_autocomplete_container' ) {
    
    // onda tretiram parent element kao da je input polje !!!!!
    // i kasnije spremam we result u DOM data property od tog parenta
    curr_input = $( props.parent );
  };
  
  return new Promise(function(resolve, reject) {

    var throttle_time = 300;

    // ako je show on click i ako nije ništa upisano u input !!!!
    // to znači da želim da se lista samo pokaže kao da je klasična drop lista ili select option polje
    if ( props.show_on_click == true && trazi_string == null ) throttle_time = 0;

    if ( parallel == 'parallel' ) throttle_time = 0;

    // ako je request za park places ne treba user !!!!!
    if (!cit_user) {
      console.log(`Ne mogu izvršiti request: ` + props.desc + ` jer niste ulogirani !!!`);
      reject('NISTE ULOGIRANI U APLIKACIJU !!!');
    };

    if ( !props.url ) {
      reject('NEMA URL-a !!!');
      return;
    };
    
    // SORT -----> VAŽNO !!!
    // promjenio sa da sort bude array umjesto objekt jer kada ga šaljem s ajaxom ako je objekt
    // moguće je da promjeni redosljed od keys što ne valja jer sort mora biti po točnom redu
    // ali ako šaljem array objekata onda ajax neće promjeniti redosljed

    
    function filter_to_string(func) {
      var filter_func_string = func.toString();
      var just_func_body = filter_func_string.substring( filter_func_string.indexOf("{")+1, filter_func_string.length-1 );
      return just_func_body;
    };
    
    
    var converted_filter = null
    
    if (props.filter) {
      
      if ( typeof props.filter == "function" ) {
        converted_filter = filter_to_string(props.filter);
      };
      
      if ( $.isArray(props.filter) ) {
        converted_filter = [];
        $.each( props.filter, function( f_ind, filter_func ) {
          if ( typeof filter_func == "function" ) converted_filter.push( filter_to_string(filter_func) );
          if ( typeof filter_func == "string" ) converted_filter.push( filter_func );
        });
      };
      
    };
    
    var query_object = props.query || {};
    
    // ako je query funcija onda je pokreni tako da generira potrebni objekt
    if ( props.query && typeof props.query == "function" ) {
      query_object = props.query();
    };

    // ovo sam počeo koristit kod req za primke u modulu sirovina  / skladište
    if ( query_object == `abort_req` ) {
      resolve([]);
      return;
    }
    
    
    if ( window.cit_find_query && Object.keys(window.cit_find_query).length > 0 ) {
      query_object = { ...query_object, ...window.cit_find_query };
    };
    
    var search_body = {
      trazi_string: ( trazi_string || null ),
      query: query_object,
      find_in: (props.find_in || null ),
      return: (props.return || {} ),
      sort: ( props.sort || null ),
      filter: converted_filter,
      limit: (props.limit || 100 ),
      page: (props.page || 1 ),
      get_count: (props.get_count || null),
      
    };

    if ( !window.cit_xhr ) window.cit_xhr = {};
    if ( !window.cit_xhr[props.desc] ) window.cit_xhr[props.desc] = {};

    // ako ima arg parallel to znači da ga ne smijem abortati !!!!!
    // ako nije parallel onda ABORT ako je prošlo manje vremena od throttle time
    if ( 
      parallel !== 'parallel'
      &&
      Date.now() - ( window.cit_xhr[props.desc].time || 0 ) < throttle_time
    ) {
      clearTimeout( window.cit_xhr[props.desc].id );
    };

    window.cit_xhr[props.desc].time = Date.now();

    // slow down ajax request firing to throttle time
    window.cit_xhr[props.desc].id = setTimeout(function () {

      var req = window.cit_xhr[ props.desc ].ajax;
      if (req && req.readyState != 4) {
          req.abort();
      };

      toggle_global_progress_bar(true);


      window.cit_xhr[props.desc].ajax = 
      $.ajax({
        headers: {
            'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
        },
        type: "POST",
        cache: false,
        url: props.url,
        data: JSON.stringify(search_body),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done(function (result_array) {

        toggle_global_progress_bar(false);


        if ( result_array.success == false ) {

          if ( result_array.msg ) {
            
            popup_error(result_array.msg);
            
          };
          
          console.error("GREŠKA U PREUZIMANJU PODATAKA !");

          if ( result_array.error ) console.log( result_array.error );
          
          resolve([]);
          
        } else if ( result_array.msg && result_array.msg.indexOf('ENDPOINT NOT ACTIVE') > -1 ) {

          resolve([]);
          console.error("GREŠKA U PREUZIMANJU PODATAKA !");

        } else {
          
          if ( props.sort ) result_array = sort_cit_result(result_array, props.sort);
          
          curr_input.data('cit_result', result_array);
          current_input_results = result_array;
          resolve( result_array);

        };

      })
      .fail(function (error) {


        curr_input.data('cit_result', null);
        current_input_results = null;

        toggle_global_progress_bar(false);
        if ( error.statusText && error.statusText !== 'abort' ) console.error("GREŠKA U PREUZIMANJU PODATAKA !");
        reject(error);
        
      });


    }, throttle_time);  
  
  }); // kraj promisa  
    
};


function cit_toast(text, arg_style) {
  var toast_id = "toast" + cit_rand();
  var html = `<div class="cit_toast" id="${ toast_id }" style="${ arg_style || "" }"> ${text} </div>`;
  $(`#cit_toast_box`).append(html);
  setTimeout( function() {  $(`#`+toast_id).remove();  }, 3*1000);  
};


function save_metadata(data, arg_rand_id) {
  
  var new_data = cit_deep(data);
  
  if ( !window.cit_user ) {
    popup_warn(`Za ovu operaciju se morate ulogirati!`);
    return;
  };
  
  // ako data nema _id znači da jen NOVI DATA SET !!!!
  if ( !new_data._id ) {
    new_data.user_saved = { 
      _id: window.cit_user._id,
      user_number: window.cit_user.user_number,
      full_name: window.cit_user.full_name,
      email: window.cit_user.email 
    };
    new_data.saved = Date.now();
    
    $(`#${arg_rand_id || new_data.rand_id}_user_saved`).text( new_data.user_saved?.full_name || "" );
    $(`#${arg_rand_id || new_data.rand_id}_saved`).text( cit_dt(new_data.saved).date_time );
    
  } else {
    new_data.user_edited = { 
      _id: window.cit_user._id,
      user_number: window.cit_user.user_number,
      full_name: window.cit_user.full_name,
      email: window.cit_user.email
    };
    new_data.edited = Date.now();
    
    $(`#${arg_rand_id || new_data.rand_id}_user_edited`).text( new_data.user_edited?.full_name || "" );
    $(`#${arg_rand_id || new_data.rand_id}_edited`).text( cit_dt(new_data.edited).date_time );
    
  };
  
  return new_data;
  
};


function meta_data_html(data) {
  
  var html =       
`<div class="row">
  <div class="col-md-6 col-sm-12">
    <b>SPREMLJENO:</b> ${data.user_saved?.full_name || "" }
    &nbsp;&nbsp;
    ${ data.saved ? cit_dt(data.saved).date_time : "" }
  </div>

  <div class="col-md-5 col-sm-12">
    <b>EDITIRANO:</b> ${data.user_edited?.full_name || "" }
    &nbsp;&nbsp;
    ${ data.edited ? cit_dt(data.edited).date_time : "" }
  </div>
</div>`;
  
  return html;
  
};


function search_local(search_string, props) {
  
  current_input_results = null;
  
  var local_result = [];
  
  var local_list = null;
  
  // 
  if ( typeof props.list == "string" ) {
    
    local_list = cit_local_list[props.list];
    
  } else if ( typeof props.list == "function" ) {
    
    local_list = props.list();
    
  } else {
    // ako nije referenca na jedan od arrays unutar vel_lists.js
    // onda to znači da sam direktno dao neki array
    local_list = props.list;
  };
  
  /* ---------------------- ITERIRAJ ARRAY SVIH OBJEKATA ---------------------- */
  
  $.each(local_list, function(search_arr_index, search_object) {
  
    // ovak sprečavam da ponovim isti red ako u više polja/stupaca nadjem string koji user traži !!!!
    var this_row_created = false;

    /* ---------------------- ITERIRAJ PROPERTJE U JEDNOM OBJEKTU ---------------------- */
    $.each(search_object, function(item_key, item_value) {

      var search_this_field = props.find_in ? ( $.inArray(item_key, props.find_in) > -1 ) : true ; 

      /* ------------------ OVDJE ZAPRAVO PRONALAZIM TRAŽENI STRING  ------------------ */
      if (  
        search_string !== null                                       &&
        String(item_value).toLowerCase().indexOf(search_string) > -1 &&
        this_row_created == false                                    &&
        search_this_field == true 

        ) {
        this_row_created = true;
        local_result.push(search_object);
      };

      /* ------------------ AKO JE just show local ONDA PRIKAŽI SVE !!!!!!! ------------------ */
      if (  
        search_string == null       &&
        props.show_on_click == true &&
        this_row_created == false   &&
        search_this_field == true 

        ) {

        this_row_created = true;
        local_result.push(search_object);  
      };

    });

  });


  if ( props.filter ) local_result = props.filter(local_result);
  
  if ( local_result?.length > 0 ) {
    var criteria = [`naziv`];
    multisort(local_result, criteria);
  };
   
  if ( props.sort ) local_result = sort_cit_result(local_result, props.sort);
  
  $('#'+current_input_id).data('cit_result', local_result);

  current_input_results = local_result;
  return local_result;
};


function activate_result_line_with_arrow_keys(event) {

  var drop_list = $('#search_autocomplete_container');
  
  var list_rows = drop_list.length > 0 ? drop_list.find('.search_result_table .search_result_row') : null;
  
  if ( drop_list.css('opacity') == '1' && list_rows && list_rows.length > 0 ) {

    // if arrow UP
    if ( event.which == 38 ) {

      event.preventDefault();

      if ( SEARCH_RESULT_ROW_INDEX == null || SEARCH_RESULT_ROW_INDEX == 0 ) {
        // if line is frist or not initiated
        SEARCH_RESULT_ROW_INDEX = list_rows.length - 1;

      } else if ( SEARCH_RESULT_ROW_INDEX > 0 ) {

        SEARCH_RESULT_ROW_INDEX -= 1;
      };

      list_rows.removeClass('hovered');
      list_rows.eq(SEARCH_RESULT_ROW_INDEX).addClass('hovered');

    };



    // if arrow DOWN
    if ( event.which == 40 ) {
      event.preventDefault();

      if ( 
        SEARCH_RESULT_ROW_INDEX == null || 
        SEARCH_RESULT_ROW_INDEX == list_rows.length - 1 
      ) {
        // if line is first or not initiated
        SEARCH_RESULT_ROW_INDEX = 0 ;
        
      } else if ( SEARCH_RESULT_ROW_INDEX < list_rows.length - 1 ) {
        
        SEARCH_RESULT_ROW_INDEX += 1;
      };

      list_rows.removeClass('hovered');
      list_rows.eq(SEARCH_RESULT_ROW_INDEX).addClass('hovered');

    };


    // if ENTER
    if ( event.which == 13 ) {
      event.preventDefault();
      if ( SEARCH_RESULT_ROW_INDEX !== null )  list_rows.eq(SEARCH_RESULT_ROW_INDEX).trigger('click');
    };

    // if ESCAPE
    if ( event.which == 27 ) {
      
      console.log('user je pritusnuo ESC .... sakrivam drop listu');
      close_cit_drop_list();
      
    };
    

  };

};


function check_is_input_hidden(input) {
  
  var element_is_hidden = false;
  var parent_elem = $(input).closest('.show_hide_element');
  
  if ( parent_elem.length > 0 ) {
    
    var parent_height = parent_elem.css('height');
    parent_height = Number( parent_height.replace('px', ''));
    
    if ( parent_height < 20 ) {
      close_cit_drop_list();
      element_is_hidden = true;
    };
    
  };
  
  var element_parent = $(input).parent();
  
  if ( element_parent.css('display') == 'none' ||  element_parent.css('opacity') == '0' ) {
    close_cit_drop_list();
    element_is_hidden = true;
  };
  
  
  if ( element_is_hidden == true ) return true;
  
  return false;
};


function pozicioniraj_drop_listu(input_polje) {
  
  
  /* -------------------- POZICIONIRANJE DROP LISTE ISPOD POLJA ZA UPIS -------------------- */
  /* ----- START -------- POZICIONIRANJE DROP LISTE ISPOD POLJA ZA UPIS -------------------- */
  /* -------------------- POZICIONIRANJE DROP LISTE ISPOD POLJA ZA UPIS -------------------- */
  
  // ovo su podaci o poziciji kliknutog polja

  var top = $(input_polje).offset().top + parseInt( $(input_polje).css('height') );
  var curr_input_width = parseInt( $(input_polje).css('width') );
  var left_middle = $(input_polje).offset().left + curr_input_width / 2;
  
  
  // ako je mali ekran tj mobitel
  if ( $(window).width() < 700 ) {
    
    $('#search_autocomplete_container').css({
      'pointer-events': 'none',
      'opacity': '0',
      'top': top + 'px',
      'left': '0px',
      'width': '100%',
      'margin-left': '0px',
      'overflow-x': 'auto'
      
    });

    
    var search_box_width = '100%';
    
    if ( $(input_polje).hasClass('simple') ) {
      search_box_width = (current_input_props && current_input_props.list_width) ? current_input_props.list_width : 500;
      search_box_width = search_box_width + 'px';
    };
    
    
    // ako je ekran manji od širine input polja onda ostavi da širina drop liste bude kao input polje, a ne 100%
    if ( 
      ( $(input_polje).hasClass('same_width') )
      && 
      curr_input_width > $(window).width() 
      ) {
      search_box_width = curr_input_width + 'px';
    };

    
    $('#search_autocomplete_container .search_result_row.header').css({
      'width': search_box_width,
    });

    $('#search_autocomplete_container .search_result_table').css({
      'width': search_box_width,
    });
    
    return;
  };
  
  
  function recalc_when_outside() {
    
    search_box_width = (current_input_props && current_input_props.list_width) ? current_input_props.list_width : 500;
    
    if ( $(input_polje).hasClass('simple_calendar') ) search_box_width = 270;
    
    var margin_left =  -search_box_width/2;
        
    // ako drop lista izlazi s ekrana na ljevu stranu
    if ( left_middle - search_box_width/2 < 0 ) {
      var left_outside_screen = search_box_width/2 - left_middle;
      left_middle = left_middle + left_outside_screen;
    };
    
    // ako drop lista izlazi s ekrana na desnu stranu
    if ( left_middle + search_box_width/2 > $(window).width() ) {
      var right_outside_screen = search_box_width/2 + left_middle - $(window).width();
      left_middle = left_middle - right_outside_screen;
    };
      
    
  };
  
  
  if ( $(input_polje).hasClass('simple') ) {
    
    recalc_when_outside();
    
    $('#search_autocomplete_container').css({
      'pointer-events': 'none',
      'opacity': '0',
      'top': top + 'px',
      'left': left_middle + 'px',
      'width': search_box_width + 'px',
      'margin-left': ( -search_box_width/2) + 'px'

    });

    
  }
  else if ( $(input_polje).hasClass('same_width') ) {
    
    $('#search_autocomplete_container').css({
      'pointer-events': 'none',
      'opacity': '0',
      'top': top + 'px',
      'left': left_middle + 'px',
      'width': curr_input_width + 'px',
      'margin-left': ( -curr_input_width/2) + 'px'

    });
    
  } 
  
  else if ( $(input_polje).hasClass('simple_calendar') ) {
    
    recalc_when_outside();
    
    $('#search_autocomplete_container').css({
      'pointer-events': 'none',
      'opacity': '0',
      'top': top + 'px',
      'left': left_middle + 'px',
      'width': search_box_width + 'px',
      'margin-left': ( -270/2) + 'px'

    });
    
    
  }
  else {
    
    $('#search_autocomplete_container').css({
      'pointer-events': 'none',
      'opacity': '0',
      'top': top + 'px',
      'left': 10 + 'px',
      'width': 'calc(100vw - 40px)',
      'margin-left': 0 + 'px'

    });

  };
  
  /* -------------------- POZICIONIRANJE DROP LISTE ISPOD POLJA ZA UPIS -------------------- */
  /* ----- END ---------- POZICIONIRANJE DROP LISTE ISPOD POLJA ZA UPIS -------------------- */
  /* -------------------- POZICIONIRANJE DROP LISTE ISPOD POLJA ZA UPIS -------------------- */
  
    
  
};


function handle_cit_input_click() {
  
  current_input_id = this.id;
  console.log(` ------------------current_input_id------------------------ ` + current_input_id);
  
};


function close_cit_drop_list() {
  

  $('#search_autocomplete_container').css({
    'pointer-events': 'none',
    'opacity': '0'
  });

  $('#search_autocomplete_container').html('');

  SEARCH_RESULT_ROW_INDEX = null;


  
};


function handle_hide_autocomplete_list(e) {
  // ako je click target NIJE neki red ili element u search spremnik onda SAKRIJ AUTOCOMPLETE LISTU !!!!  
  if ( $(e.target).closest('#search_autocomplete_container').length == 0 ) {
    
    close_cit_drop_list();
    
  };

};



function handle_simple_calendar_blur() {
  refresh_simple_cal( $(this) );
};



function refresh_simple_cal(this_input, is_blur) {
  
  var input_value = this_input.val().trim();
  var new_date_ms = null;

  // po defaultu uzimam da je date time
  var input_format = `date_time`;
  
  if ( this_input.hasClass(`date`) ) input_format = `date`; // samo date
  if ( this_input.hasClass(`time`) ) input_format = `time`; // damo time


  if ( input_value !== "" ) {

    new_date_ms = cit_ms( input_value, 0, input_format ).ms;
    update_simple_calendar_data_attr(this_input, new_date_ms);
    update_simple_calendar_date(this_input);

  } else {
    
    current_simple_calendar_date = null;
  };

  this_input.data(`cit_run`)( new_date_ms, this_input);

  // samo ako nije blur tj ovo se aktivira samo kada kliknem
  if ( !is_blur ) select_active_cal_polja();  
  
};


function handle_cit_auto_keyup(e) {

  
  window.last_cit_user_interaction = Date.now();
  
  var this_value = $(this).val();
  
  current_input_id = this.id;
  
  var this_input = $(this);
    
  var input_hidden = check_is_input_hidden(this);
  
  
  /* 
  ------------------------------------------------------------------------
  stao sam ovdje 
  stao sam ovdje 
  stao sam ovdje  ------> moram vidjeti zašto kada obrišem datum u polju za datume  - onda se state ne promjeni u NULL !!!!!
  stao sam ovdje 
  stao sam ovdje 
  
  if ( this_input.hasClass(`simple_calendar`) ) {
    setTimeout( function() {
      refresh_simple_cal(this_input);
    }, 400);
    return;
  };
  ------------------------------------------------------------------------
  */
  
  var props = current_input_props = $(this).data('cit_props');
  
  // ako nema props onda stani !!!!
  // ali nemoj gledati jel ima propse ako je simple calendar input !!!
  
  if ( !props && this_input.hasClass(`simple_calendar`) == false ) {
    // console.error( `input element s idjem ${current_input_id} nema cit_props data !!!!`);
    // console.error( `prekidam autocomplete func`);
    return;
  };
  
  if ( this_input.hasClass(`simple_calendar`) ) {
    
    
    /*
    setTimeout( function() {
      refresh_simple_cal(this_input);
    }, 30 );
    
   
    
    */
    
    
    return;
    
    
  };
  
  
  if ( input_hidden ) return;
  
  // provjeri da nije strelica gore dolje ili enter ili escape ili tab 
  if (
    e.which !== 38 &&
    e.which !== 40 &&
    e.which !== 13 &&
    e.which !== 27 && 
    e.which !== 9 
  ) {
    
    // ako je iz nekog razloga sakrivena drop lista onda simuliraj klik na trenutno polje tako da se lista pojavi
    if ( $('#search_autocomplete_container').css('opacity') == '0' ) {
      console.log('lista je sakrivena -- pozivam  handle autocomplete click da je pokažem ...');
      $(this).trigger('click');
    };
    
  } else {
    
    // ako je gore ili dolje strelica
    if ( e.which == 38 || e.which == 40 || e.which == 13 || e.which == 27 ) {
      console.log('prije activate result line with arrow keys:   ' + SEARCH_RESULT_ROW_INDEX);
      activate_result_line_with_arrow_keys(e);
      console.log('POSLJE /////////////////// activate result line with arrow keys:   ' + SEARCH_RESULT_ROW_INDEX);
      return;
    };
      
  };
  
  
  // trezi tek nakon što user upiše 3 znaka
  var min_string_length = 3;
  
  if ( props.min_string_length ) min_string_length = props.min_string_length;

  
  // IGNORE AKO JE TIPKA TAB !!!!!!
  if ( e.which !== 9 && this_value.length < min_string_length ) { 
    
    // ako je user obrisao sve onda pokreni cit run za argumentom null !!!!!!!
    if ( this_value.length == 0 && props.cit_run ) props.cit_run(null);
    // ako je tab ili ako je broj znakova manji od minimuma ----> STANI !!!!
    return;
  };
  
  var search_string = this_value.toLowerCase();
  
  if ( props.local ) {
    var local_result = search_local(search_string, props);
    create_cit_result_list( local_result, props, e.target.id || null );
    
  } 
  else {

    search_database(search_string, props).then(
      function( remote_result ) { 
        
        // ako imam local filter onda provuci rezultat kroz njega !!!
        if ( props.local_filter && props.parent ) remote_result = props.local_filter(remote_result, props.parent );
        
        create_cit_result_list( remote_result, props, e.target.id || null ); 
        
        // registriraj evente u listi nakog generriranja liste (kad se lista pojavi u DOM-u)
        if ( props.reg_events && props.parent ) props.reg_events( props.parent );
        
      },
      function( error ) { 
        console.error(`SEARCH DATABASE ERROR ZA:\n${props.desc}`)
        console.error( error );
      }
    );
    
  };

}; // kraj handle cit auto keyup


async function get_kalk_module() {
  window.kalkulacija_module = await get_cit_module(`/modules/kalk/kalk_module.js`, `load_css`);  
};
get_kalk_module();

function get_price_m2(ulaz) {
  
  var jedinica = ulaz.kalk_unit || ulaz.osnovna_jedinica?.jedinica;

  if ( !jedinica ) {
    console.log(`Nedostaje jedinica za sirovinu !!!`);
    popup_warn(`Nedostaje jedinica cijene za robu koju ste izabrali!!!`);
    return null;
  };

  var unit_price = ulaz.kalk_unit_price || ulaz.cijena || 0;
  var val = ulaz.kalk_po_valu || ulaz.po_valu || ulaz.kontra_tok || null;
  var kontra = ulaz.kalk_po_kontravalu || ulaz.po_kontravalu || ulaz.tok || null;

  var price_m2 = null;

  // ako je cijena po komadu onda
  // moram podjeliti cijenu sa kvadraturom komada
  if ( jedinica == "kom" ) {
    price_m2 = unit_price / (val * kontra / 1000000); // npr 10kn / 5m2 = 2kn po kvadratnom metru
  };

  // ako je cijena po kvadratu onda ne diram ništa  - to je to 
  if ( jedinica == "m2" ) {
    price_m2 = unit_price;
  };

  
  return {
    
    price_m2,
    val,
    kontra,
    
  }
  
  
};

// stao sam ovdje

function get_price_duzina(ulaz) {
  
  var jedinica = ulaz.kalk_unit || ulaz.osnovna_jedinica?.jedinica;

  if ( !jedinica ) {
    console.log(`Nedostaje jedinica za sirovinu !!!`);
    popup_warn(`Nedostaje jedinica za robu koju ste izabrali!!!`);
    return null;
  };

  var unit_price = ulaz.kalk_unit_price || ulaz.cijena || 0;
  var duzina = ulaz.kalk_duzina || ulaz.duzina || 0;
  
  
  var price_duzina = null;

  // ako je cijena po komadu onda
  // moram podjeliti cijenu sa kvadraturom komada
  if ( jedinica == "m" ) {
    price_duzina = unit_price / duzina
  };

  // ako je cijena po kvadratu onda ne diram ništa  - to je to 
  if ( jedinica == "m2" ) {
    price_m2 = unit_price;
  };

  
  return {
    
    price_m2,
    val,
    kontra,
    
  }
  
  
};

window.cit_auto_time = 0;

function handle_cit_auto_click(e) {
  
  e.stopPropagation();
  
  // console.log(` ------------- !!!!------------- klik u global func `);
  
  /*
  ----------------------------------------------------------------------------------
  // console.log(`--------type--------`, e.type);
  // console.log(`--------time--------`, e.timeStamp);
  // ako se trigerira focus i click u isto vrijeme 
  // onda prekini drugi event jer nema smisla da radi istu stvar dva puta za redom
  if ( e.timeStamp - window.cit_auto_time < 100 ) {
    console.log("prekidam drugi event");
    return; 
  };
  window.cit_auto_time = e.timeStamp;
  // ovo je time out u slučaju blura na prethodnom cit auto inputu !!!
  // clearTimeout(window.close_drop_list_timeout);
  ----------------------------------------------------------------------------------
  */
  
  
  current_input_id = this.id;
  
  var this_input = $(`#` + current_input_id);

  
  function props_condition() {
    var props_ok = false
    
    if ( $(`#` + current_input_id).data('cit_props') ) props_ok = true;
    
    // ako je calendar polje onda samo gledaj jel ima cit_run
    if ( this_input.hasClass(`simple_calendar`) && this_input.data('cit_run') ) props_ok = true; 
    
    return props_ok;
  };

  // čekaj 5 sekundi da se registiraraju propsi
  wait_for( props_condition, function() {
    
    var props = current_input_props = $(`#` + current_input_id).data('cit_props');

   

    if ( this_input.hasClass(`simple_calendar`) ) {

      var input_format = `date_time`;
      if ( this_input.hasClass(`date`) ) input_format = `date`;
      if ( this_input.hasClass(`time`) ) input_format = `time`;

      var new_date_ms = null;
      var input_value = this_input.val().trim();

      if ( input_value !== "" ) {

        // ovdje dajem string
        new_date_ms = cit_ms( input_value, 0, input_format ).ms;

        update_simple_calendar_data_attr(this_input, new_date_ms);
        update_simple_calendar_date(this_input);
        current_simple_calendar_start = current_simple_calendar_date;

      };


      this_input.data(`cit_run`)(new_date_ms, this_input, `on_click`);

      update_simple_calendar_data_attr(this_input, new_date_ms);

      select_active_cal_polja();

      var calendar_html = create_simple_drop_calendar(new_date_ms);
      $('#search_autocomplete_container').html(calendar_html);

      var current_input = $('#'+current_input_id)[0];
      pozicioniraj_drop_listu(current_input);

      $('#search_autocomplete_container').css({
        'opacity': '1',
        'pointer-events': 'all'
      });

      return;

    };

    if ( !props ) {
      console.error( `input element s idjem ${current_input_id} nema cit_props data !!!!`);
      console.error( `prekidam autocomplete func`);
      return;
    };

    SEARCH_RESULT_ROW_INDEX = null;


    var input_hidden = check_is_input_hidden(this);

    if ( input_hidden ) return;


    // ako se array nalazi u local bazi:
    if ( props.local && props.show_on_click ) {

      var local_result = search_local(null, props);
      create_cit_result_list( local_result, props, e.target.id || null );

    };

    // ako NIJE local baza nego je u remote bazi
    if ( !props.local && props.show_on_click) {

      search_database(null, props).then(
        function( remote_result ) { 

          // ako imam local filter onda provuci rezultat kroz njega !!!
          if ( props.local_filter && props.parent ) remote_result = props.local_filter(remote_result, props.parent );

          create_cit_result_list( remote_result, props, e.target.id || null ); 

          // registriraj evente u listi nakog generiranja liste (kad se lista pojavi u DOM-u)
          if ( props.reg_events && props.parent ) props.reg_events( props.parent );

        },
        
        function( error ) { console.error( error ); }
        
      );

    }; // kraj ako je remote search


   }, 5*1000 );
  
  
  
}; // KRAJ handle cit auto click


// PRIMJER KORIŠTENJA U PROPSIMA:
/*
replace_sort_keys: { 
  datum_string : "datum",
  time_and_referent: "time",
} 
*/

function find_substitute_sort_key(props, arg_key) {
  
  // u startu stavi da replace key bude ISTI ORIGINALU
  var substitute_key = arg_key;
  
  // ovo je prop koje trebam staviti u propse od liste na primjer { datum_string : "datum" } 
  // ----> to znači ako kliknem da sortiram po datum_string zapravo trebam sortirati po datum !!!!
  if ( props.replace_sort_keys ) {
    
    $.each( props.replace_sort_keys, function(orig_key, subs_key) {
      
      if ( orig_key == arg_key) {
        substitute_key = subs_key;
      };
      
    });
    
  };
  
  return substitute_key;
  
};

function header_click_sort(e) {
  
  // console.log(this);
  
  e.stopPropagation();
  e.preventDefault();
  
  
  var cit_result_table = $(this).closest('.cit_result_table');
  
  var result_array = cit_result_table.data('cit_result');
  
  console.log(result_array);
  
  var props = cit_result_table.data('cit_props');
  
  
  if ( !props ) {
    popup_error(`Ova tablica nema ispravne podatke za sortiranje.<br>ERROR: 004`)
    return;
  };
  
  var input_id = cit_result_table.data('cit_input_id');
  
  var this_header = this;
  
  
  var key = this.id.substring(this.id.indexOf(`_`)+1);
  
  // ako na primjer imam est_deadline_date peroperty
  // to je property kojeg pretvaram u string npr iz 1664810028514 -----> odbijem 03.10.2022 17:14:25 
  // zato kada želim sortirati po tome onda trebam pronaći pravi numerički iznos a ne string !!!
  key = find_substitute_sort_key(props, key);
  
  
  var this_sort_direction = null;
  
  
  if ( $(this).hasClass('cit_asc') ) {
    // ako je već asc onda promjeni u desc
    $(this).removeClass('cit_asc').addClass('cit_desc');
    this_sort_direction = -1;
  } 
  else if ( $(this).hasClass('cit_desc') ) {
    // ako je već desc onda promjeni u DEFAULT TJ BEZ SORTA !!!!!
    $(this).removeClass('cit_desc');
    $(this).removeClass('cit_asc');
    this_sort_direction = null;
    
  } else {
    // ako nema klase niti asc niti desc ( ovo je STARTNO STANJE )  ----> onda po defaultu promjeni u asc
    $(this).addClass('cit_asc');
    this_sort_direction = 1;
  };
  
  
  if ( props && props.sort ) {
    // ako postoji property sort
    
    var found_prop = null;
    
    // ako postoji props sort koji je ARRAY
    $.each(props.sort, function( sort_index, sort_obj ) {
      
      // vrti po OBJECT PROPS / KEYS
      // loop po keys --- > ionako svaki objekt ima samo jedan key !!!! :)
      $.each(sort_obj, function( sort_key, sort_direction ) {
        
        if ( sort_key == key ) {
          props.sort[sort_index][key] = this_sort_direction;
          
          // ako je direction null onda posve obriši taj property u sort
          if ( this_sort_direction === null ) delete props.sort[sort_index][key]
          
          found_prop = true;
        };
        
      });
      
    });  
    
    if ( found_prop == null && this_sort_direction !== null ) props.sort.push({ [key]: this_sort_direction });
    
  } else if ( props && !props.sort && this_sort_direction !== null ) {
    
    // ako ne postoji props sort
    props.sort = [];
    props.sort.push({ [key]: this_sort_direction });
  
  };
  
  console.log( props.sort );
  
  /*
  UPDATE PODATAKA UNUTAR ELEMENATA TABLICE I CURRENT INPUTA
  */
  
  cit_result_table.data('cit_props', props);
  
  // ako se radi o drop listi ispod inputa
  // onda TAKOĐER ažuriraj propse koji se nalaze unutar data cit_props
  if (input_id) $('#'+input_id).data('cit_props', props );
  
  // sortiraj
  result_array = sort_cit_result(result_array, props.sort);
  
  
  create_cit_result_list(result_array, props);
  
  setTimeout( function() { reset_tooltips(); }, 200);
  
  
};
  
function select_result_red(e) {

  
  clicked_result_red = $(this);
  
  var cit_result_table = $(this).closest('.cit_result_table'); 
  
  if ( !cit_result_table ) return;
  
  var props = cit_result_table.data('cit_props');
  
  // if ( $(this).closest('#search_autocomplete_container').length == 0 ) return;
  if ( $(this).hasClass('header') ) return;
  
  // inače resultate spremam u data property od polja na kojem user traži nešto ili je kliknuo to polje
  // var input_results = $('#'+current_input_id).data('cit_result');
  
  var input_results = cit_result_table.data('cit_result');
  
  if ( !input_results ) return;
  
  var selected_object = null;
  var sifra = null;
  var _id = null;
  
  // var identifier = this.id.replace('list_item_', '');
  var identifier = this.id.substr( this.id.lastIndexOf("_") + 1, 10000000);
  
  if ( $(this).hasClass('_id') == true ) {
    _id = identifier;
    
    $.each(input_results, function(red_index, red) {
      if ( red._id == _id ) selected_object = red;
    });
  };  
  
  // ako ne ne postoji class _id unutar result reda
  // onda traži jel ima class sifra
  
  // na ovaj način mogu imati porperty sifra unutar objekta u bazi
  // jer ako nađe _id onda više neće tražiti sifra
  
  if ( _id == null && $(this).hasClass('sifra') == true ) {
    sifra = identifier;
    
    $.each(input_results, function(red_index, red) {
      if ( red.sifra == sifra ) selected_object = red;
    });  
    
  };
  
  selected_object = cit_deep(selected_object);
  
  props.cit_run( selected_object );

  // ako je multi select onda se prikazana lista 
  // ne smije zatvoriti nakon što izabereš neki red !!!
  if ( $("#"+current_input_id).hasClass("multi_select") == false ) {
  
    // sakrij drop listu rezultata !!!!!
    $('#search_autocomplete_container').css({
      'pointer-events': 'none',
      'opacity': '0'
    });
    
  };

};

function toggle_cit_switch(arg_state, elem) {


  var switch_state = null;
  
  if ( arg_state === true ) {
    elem.removeClass('off').addClass('on');
    switch_state = true;
  };
  
  if ( arg_state === false || typeof arg_state === "undefined" || arg_state === null ) {
    elem.removeClass('on').addClass('off');
    switch_state = false;
  };
  
  
  // nemoj ići dalje ako je ova funkcija dobila argument za state
  // ako nije true ili false onda to znači da je ova funkcije trigerirana sa klikom tj eventom
  
  if ( arg_state !== true && arg_state !== false && arg_state != null ) {
    
    current_input_id = this.id;
    

    if ( $(this).hasClass('off') ) {
      $(this).removeClass('off').addClass('on');
      switch_state = true;
    } else {
      $(this).removeClass('on').addClass('off');
      switch_state = false;
    };
    
    $(this).data('cit_run')( switch_state, this );
    
  };
  
  
};  

function toggle_cit_check_box(arg_state) {

  
  current_input_id = this.id;
  
  var check_state = null;
  
  if ( arg_state === true ) {
    $(this).removeClass('off').addClass('on');
    check_state = true;
  };
  
  if ( arg_state === false || typeof arg_state == "undefined" || arg_state == null ) {
    $(this).removeClass('on').addClass('off');
    check_state = false;
  };

  
  // nemoj ići dalje ako je ova funkcija dobila argument za state
  // ako nije true ili false onda to znači da je ova funkcije trigerirana sa klikom tj eventom
  // i argument je zapravo event 
  if ( arg_state !== true && arg_state !== false && arg_state != null ) {

    arg_state.stopPropagation();
    arg_state.preventDefault();
    
    
    if ( $(this).hasClass('off') ) {
      $(this).removeClass('off').addClass('on');
      check_state = true;
    } else {
      $(this).removeClass('on').addClass('off');
      check_state = false;
    };
    
  };
  
  $(this).data('cit_run')( check_state, this );
  
};  

function sort_parents(elements, parent, direction ) {

  var sorted = [];

  function loop_elements(elems, arg_parent) {

    // var elems = cit_deep(elements);
    if (!arg_parent) {

      $.each(elems, function (ind, element) {
        // samo ako nema parenta
        if (!element.elem_parent && !element.sorted ) {

          elems[ind].level = 0;

          elems[ind].sorted = true;
          sorted.push(element);
          var parent = element;
          // provjeri jel ovaj elem parent nekome
          loop_elements(elems, parent);

        } else if ( !element.sorted ) {
          // ----------------------------------------------------------------
          // OVAJ DIO VRIJEDI SAMO ZA SERVER DIO U FIND CONTROLER
          // ...ili ako je za listu na statuses page !!!!!

          // ----------------------------------------------------------------

          if ( 

            typeof window == "undefined" // na serveru !!

            ||

            ( 
              typeof window !== "undefined" && 
              ( window.location.hash.indexOf('#statuses_page') > -1 || window.location.hash.indexOf('#sirov/') > -1 || window.location.hash.indexOf('#partner/') > -1 ) 
            )

          ) {

            elems[ind].level = (element.level || 0 ) + 10 ;

            elems[ind].sorted = true;
            sorted.push(element);
            var parent = element;
            loop_elements(elems, parent);

          };

          // --- KRAJ -------------------------------------------------------------
          // OVAJ DIO VRIJEDI SAMO ZA SERVER DIO U FIND CONTROLER
          // --- KRAJ -------------------------------------------------------------


        };
      });

    } 
    else {

      $.each(elems, function (ind, element) {
        // provjeri jel ovaj elem djete od arg parenta
        if (element.elem_parent == arg_parent.sifra && !element.sorted ) {

          elems[ind].level = arg_parent.level + 10;

          elems[ind].sorted = true;
          sorted.push(element);
          var parent = element;
          loop_elements(elems, parent);

        };


      });

    };
  }; // kraj loop func

  loop_elements(elements, parent);

  $.each(sorted, function(s_ind, sorted_elem) {
    delete sorted_elem.sorted;
  });
  
  
  // samo ako nisam explicitno rekao da treba biti prema dolje
  if ( direction !== `down` && sorted.length > 0 ) {
    // I  SAMO AKO VEĆ NIJE SORTIRAN OD NAJNOVIJEG PREMA NAJSTRIJEM !!!
    // I  SAMO AKO VEĆ NIJE SORTIRAN OD NAJNOVIJEG PREMA NAJSTRIJEM !!!
    if ( sorted[0].time < sorted[sorted.length-1].time ) sorted = sorted.reverse();
  };
  
  return sorted;

}; // kraj sort parents

// kada user klikne na malu sliku u autocomplete search result tablici
// onda se slika prikaže uvećana u modal popup-u
function handle_click_result_image(e) {
  
  e.stopPropagation();
  
  // prvo removam spremnik ako postoji od prije
  $('#image_popup_spremnik').remove();
  
  // popup spremnik je samo element u koji stavljam klon od kliknute slike u autocomplete search result tablici
  var image_popup_HTML = 
      `
      <div id="image_popup_spremnik">
      </div>

      `;
  // ubacim spremnik u body
  $('body').prepend(image_popup_HTML);
  // zatim ubacim klon od slike u spremnik
  $('#image_popup_spremnik').prepend($(this).clone());
  
  // zatim registriram listener da kada user klikne na uvećanu sliku
  // onda se popup sa slikom obriše tj nestane
  $('#image_popup_spremnik').off('click');
  $('#image_popup_spremnik').on('click', function(e) {
    e.stopPropagation();
    $(this).remove();
  });
  
};

// kad user scrolla window onda lista rezultata prati trenutno kliknuto polje
function drop_list_follows_curr_input(e) {
  
  // console.log('  -------------- cit_main_area scroll ---------------- ')
  
  if ( $('#search_autocomplete_container').css('opacity') == "1") {
    
    var top_inputa = $('#' + current_input_id).offset().top;
    var height_inputa = parseInt( $('#' + current_input_id).css('height') );
    var bottom_inputa = top_inputa + height_inputa;
    
    $('#search_autocomplete_container').css('top', bottom_inputa + 'px');
    
  };

};

function concat_full_adresa(data, arg_vrsta_adrese) {

  var full_adresa = "";
  var found_sjediste = false;

  if (data.adrese && data.adrese.length > 0) {
    
    
    if ( arg_vrsta_adrese ) {

      $.each(data.adrese, function (index, adresa_obj) {
      
        if ( adresa_obj.vrsta_adrese?.sifra == arg_vrsta_adrese ) {

          var adresa = adresa_obj.adresa;
          var kvart = adresa_obj.kvart ? (", " + adresa_obj.kvart) : "";
          var naselje = adresa_obj.naselje ? (", " + adresa_obj.naselje) : "";
          var posta = adresa_obj.posta ? (", " + adresa_obj.posta) : "";
          var mjesto = adresa_obj.mjesto ? (", " + adresa_obj.mjesto) : "";
          var drzava = adresa_obj.drzava ? (", " + adresa_obj.drzava.naziv) : "";

          full_adresa = adresa + kvart + naselje + posta + mjesto + drzava;

        }; // kraj ako je sjedište

      });

      
    } 
    else {
      
      
      $.each(data.adrese, function (index, adresa_obj) {

        if ( full_adresa == "" ) {
          
          // kad nadjem prvu adresu onda NE TRAŽIM VIŠE !!!
          // kad nadjem prvu adresu onda NE TRAŽIM VIŠE !!!
          var adresa = adresa_obj.adresa;
          var kvart = adresa_obj.kvart ? (", " + adresa_obj.kvart) : "";
          var naselje = adresa_obj.naselje ? (", " + adresa_obj.naselje) : "";
          var posta = adresa_obj.posta ? (", " + adresa_obj.posta) : "";
          var mjesto = adresa_obj.mjesto ? (", " + adresa_obj.mjesto) : "";
          var drzava = adresa_obj.drzava ? (", " + adresa_obj.drzava.naziv) : "";

          full_adresa = adresa + kvart + naselje + posta + mjesto + drzava;
        };

      });
      
    };
      
  }; // kraj ako postoje adrese u partneru


  return full_adresa;

};

function concat_naziv_sirovine(data) {

  var full_naziv = 
      (data.klasa_sirovine ? data.klasa_sirovine.naziv+" " : "") +
      (data.tip_sirovine ? data.tip_sirovine.naziv+" " : "") +
      (data.vrsta_materijala ? data.vrsta_materijala.naziv+" " : "") +
      (data.naziv ? data.naziv + " " : "") +
      (data.kvaliteta_1 ? data.kvaliteta_1 + "/" : "") +
      (data.kvaliteta_2 ? data.kvaliteta_2 + " " : "") +
      (data.po_valu ? data.po_valu + "X" : "") +
      (data.po_kontravalu ? data.po_kontravalu : "");

  return full_naziv;

};



function reduce_sir(sir) {
  
  // ako je null ili undefined
  if ( !sir ) return null;
  
  
   var sir = cit_deep(sir);
  
  if ( sir.records?.length > 0 ) delete sir.records;
  if ( sir.statuses?.length > 0 ) delete sir.statuses;
  if ( sir.locations?.length > 0 ) delete sir.locations;
  if ( sir.sirov_pics?.length > 0 ) delete sir.sirov_pics;
  if ( sir.specs?.length > 0 ) delete sir.specs;
  
  if ( sir.povezani_kupci ) delete sir.povezani_kupci;

  if ( sir.dobavljac ) sir.dobavljac = { _id: sir.dobavljac._id || null, naziv: sir.dobavljac.naziv || null };
  if ( sir.alat_owner ) sir.alat_owner = { _id: sir.alat_owner._id || null, naziv: sir.alat_owner.naziv || null };

  // obriši sve flat propertije u sirovini
  // obriši sve flat propertije u sirovini
  // obriši sve flat propertije u sirovini
  $.each(sir, function(sir_key, key_data) {
    if (sir_key.indexOf(`_flat`) > -1) delete sir[sir_key];
  })
  
  return sir;
  
  
};



var map_name_to_type = {

  offer_dobav: `RFQ`,
  ulazna_ponuda: `PONUDA DOBAV.`,
  ulazna_ponuda_ok: `ODOBRENJE PD`,

  order_dobav: `NARUDŽBA DOB.`,
  in_record: `PRIMKA`,
  in_pro_record: `PR.PROIZ.`,
  
  offer_reserv: `REZERV. PO PK`,
  order_reserv: `REZERV. PO NK`,
  radni_nalog: `RADNI NALOG`,
  ms_record: `MEĐUSKLAD.`,
  utrosak: `UTROŠAK`,
  skart: `ŠKART`,
  pro_record: `PROIZVEDENO`,

  out_record: `OTPREMNICA`,

};


function gen_find_query_number( this_module, elem ) {

  var this_comp_id = $(elem).closest('.cit_comp')[0].id;
  var data = this_module.cit_data[this_comp_id];
  var rand_id =  this_module.cit_data[this_comp_id].rand_id;

  var key = null;
  if ( elem.id.indexOf(rand_id) > -1 ) key = elem.id.replace(rand_id+`_`, ``);  
  if ( elem.id.indexOf(data.id) > -1 ) key = elem.id.replace(data.id+`_`, ``);  

  var string_val = $(elem).val();
  // ako je user obrisao input onda također obriši taj prop u find queryju
  if ( string_val === "" ) {
    if (  window.cit_find_query.hasOwnProperty(key) ) delete window.cit_find_query[key];
    return;
  };

  var value = null;
  var min = null;
  var max = null;

  if ( string_val.indexOf(`-`) > -1 ) {
    // ------------------------------ MIN - MAX ------------------------------

    var min_txt = string_val.split(`-`)[0].trim();
    min = cit_number(min_txt);
    
    if ( min_txt !== "" && min == null ) {
      popup_warn(`Vrijednost za MINIMUM u polju ${ elem.id } nije broj!!!`);
      return;
    };

    var max_txt = string_val.split(`-`)[1].trim();
    max = cit_number(max_txt);

    if ( max_txt !== "" && max == null ) {
      popup_warn(`Vrijednost za MAXIMUM u polju ${ elem.id } nije broj!!!`);
      return;
    };

    window.cit_find_query[key] = { $gte: min, $lte: max  };
    return;

  } 
  else {
    // ------------------------------ ako NIJE MIN - MAX ------------------------------

    value = cit_number( string_val.trim() );  

    if ( string_val !== "" && value == null ) {

      popup_warn(`Vrijednost u polju ${ elem.id } nije broj!!!`);
      return;

    } else {

      window.cit_find_query[key] = value;
      return;

    };


  }; // kraj ako nema crticu između


  console.log(window.cit_find_query);

};

function gen_find_query_select( this_module, state, prop_key, find_value, DB_model ) {

  var elem = $(`#` + current_input_id );

  var this_comp_id = $(elem).closest('.cit_comp')[0].id;
  
  if ( window.location.hash.indexOf('#sirov/') > -1 ) this_comp_id = $(elem).closest('.sirovine_body.cit_comp')[0].id;
  
  var data = this_module.cit_data[this_comp_id];
  var rand_id =  this_module.cit_data[this_comp_id].rand_id;

// ako je user obrisao input onda također obriši taj prop u find queryju
  if ( state === null || find_value === null ) {
    if (  window.cit_find_query.hasOwnProperty(prop_key) ) delete window.cit_find_query[prop_key];
    elem.val(``);
    return;
  };

  if ( state.naziv ) elem.val(state.naziv);
  
  window.cit_find_query[prop_key] = find_value;
  
  
  
  // -------------------------------------------- AKO IMA DB MODEL --------------------------------------------
  
  // if ( DB_model )
  

  console.log(window.cit_find_query);

};


function ajax_upsert_record( record ) {

  return new Promise( function(resolve, reject) {

    $.ajax({
      headers: {
        'X-Auth-Token': window.cit_user.token
      },
      type: "POST",
      cache: false,
      url: `/upsert_sirov_record`,
      data: JSON.stringify( record ),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done( function (result) {

      console.log(result);

      if ( result.success == true ) {

        cit_toast(`REZERVACIJA SIROVINE JE SPREMLJENA!`);
        resolve( record );

      } else {

        // ako result NIJE SUCCESS = true

        if ( result.msg ) console.error(result.msg);
        if ( !result.msg ) console.error(`Greška prilikom spremanja REZERVACIJE!`);

        resolve(null);

      };

    })
    .fail(function (error) {

      console.error(`Došlo je do greške na serveru prilikom spremanja REZERVACIJE!`);
      console.log(error);
      resolve(null);
    })
    .always(function() {
      toggle_global_progress_bar(false);
    });

  }); // kraj promisa  


}; // kraj ajax save status


async function ajax_find_query(arg_query, arg_url, arg_filter) {
  
  var props = {
    filter: arg_filter,
    url: arg_url,
    query: arg_query,
    desc: `ajax_find_query ` + Date.now(),
  };


  var db_result = null;
  
  try {
    db_result = await search_database(null, props );
  } catch (err) {
    console.log(err);
    return null;
  };

  if ( db_result && $.isArray(db_result) ) {
    console.log(db_result);
  };
  
  return db_result;
  
  
};

function socket_check_in() {

  if ( window.cit_socket && window.cit_user ) {

    var socket_user = {
      user_number: window.cit_user.user_number,
      email: window.cit_user.email,
      full_name: window.cit_user.full_name,
    };

    cit_socket.emit('check_in', socket_user );

  };

};

function is_admin(user) {
  
  // TODO - pretvori u server logiku - za sada je lokalno što se može lako hakirati
  window.cit_admin = false;

  // 9 jer radmila
  // 10 je toni
  // 67 je bruno
  if ( $.inArray(user?.user_number, [ 9, 10, 67 ] ) > -1 ) window.cit_admin = true;
  
  return window.cit_admin;
  
};


function get_product_variant( arg_item_sifra ) {

  return new Promise( function( resolve, reject) {

    $.ajax({
      headers: {
        'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
      },
      type: "POST",
      cache: false,
      url: `/get_product_new_variant`,
      data: JSON.stringify( { item_sifra: arg_item_sifra } ),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done( function (result) {

      console.log(result);

      if ( result.success == true ) {

        resolve(result);

      } else {

        if ( result.msg ) popup_error(result.msg);
        if ( !result.msg ) popup_error(`Došlo je do greške na serveru prilikom dobivanja broja za NOVU NAKLADU produkta !!!`);

        resolve(null);

      };

    })
    .fail(function (error) {

      resolve(error);
      console.log(error);
      popup_error(`Došlo je do greške na serveru prilikom dobivanja novog broja za varijantu produkta !!!`);
    })
    .always(function() {
      

    });


  }); // kraj promisa  

};

function get_new_counter(doc_type, month_reset) {


  toggle_global_progress_bar(true);

  return new Promise( function(resolve, reject) {

    if ( !window.cit_user ) {
      resolve(false);
      return;
    };

    $.ajax({
      headers: {
        'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
      },
      type: "POST",
      cache: false,
      url: `/get_new_counter`,
      data: JSON.stringify({
        doc_type: doc_type,
        month_reset: month_reset,
      }),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done(function (result) {

      console.log(result);

      if ( result.success == true ) {


        resolve(result);

      } else {

        popup_error( `Greška prilikom generiranja šifre za dokument!<br><br>` + result.msg || `` );

        resolve(false);

      };

    })
    .fail(function (error) {

      resolve(false);

      console.log(error);
      popup_error(`Došlo je do greške na serveru prilikom generiranja šifre za dokument!`);
    })
    .always(function() {

      toggle_global_progress_bar(false);


    });

  }); // kraj promisa

}; 



function get_new_palet_sifre(sifre_count) {


  toggle_global_progress_bar(true);

  return new Promise( function(resolve, reject) {

    if ( !window.cit_user ) {
      resolve(false);
      return;
    };

    $.ajax({
      headers: {
        'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
      },
      type: "POST",
      cache: false,
      url: `/get_new_palet_sifre`,
      data: JSON.stringify({
        sifre_count: sifre_count,
      }),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done(function (result) {

      console.log(result);

      if ( result.success == true ) {
        
        resolve(result);

      } else {

        popup_error( `Greška prilikom generiranja šifre za pakiranje / paletu !<br><br>` + result.msg || `` );

        resolve(false);

      };

    })
    .fail(function (error) {

      resolve(false);

      console.log(error);
      popup_error(`Došlo je do greške na serveru prilikom generiranja šifre za karticu!`);
    })
    .always(function() {

      toggle_global_progress_bar(false);


    });

  }); // kraj promisa

}; 




function get_new_sub_nacrt( template_num, tip ) {


  toggle_global_progress_bar(true);

  return new Promise( function(resolve, reject) {

    if ( !window.cit_user ) {
      resolve(false);
      return;
    };

    $.ajax({
      headers: {
        'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
      },
      type: "POST",
      cache: false,
      url: `/get_new_sub_nacrt`,
      data: JSON.stringify({
        template_num: template_num,
        tip: tip
      }),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done(function (result) {

      console.log(result);

      if ( result.success == true ) {


        resolve(result);

      } else {

        popup_error( `Greška prilikom generiranja šifre za SUB nacrt!<br><br>` + result.msg || `` );

        resolve(false);

      };

    })
    .fail(function (error) {

      resolve(false);

      console.log(error);
      popup_error(`Došlo je do greške na serveru prilikom generiranja šifre za SUB nacrt!`);
    })
    .always(function() {

      toggle_global_progress_bar(false);


    });

  }); // kraj promisa

}; 



function expand_status_conf( res ) {
  $.each(res.admin_conf.status_conf, function(s_ind, tip) {

    if ( tip.next_statuses?.length > 0 ) {

      $.each(tip.next_statuses, function(n_ind, next) {

        var full_status = find_item( res.admin_conf.status_conf, next.sifra, `sifra`);

        res.admin_conf.status_conf[s_ind].next_statuses[n_ind] = {

          ...full_status, // dodaj sve propse od statusa

          status_num : next.status_num,
          next_status_num : next.next_status_num,
          naziv : next.naziv,
          sifra : next.sifra,

        };

      });


    };

    if ( tip.grana_finish?.length > 0 ) {



      $.each(tip.grana_finish, function(f_ind, fin) {

        var full_status = find_item( res.admin_conf.status_conf,fin.sifra, `sifra`);

        res.admin_conf.status_conf[s_ind].grana_finish[f_ind] = {

           ...full_status, // dodaj sve propse od statusa

          status_num : fin.status_num,
          next_status_num : fin.next_status_num,
          naziv : fin.naziv,
          sifra : fin.sifra,
        };

      });

    };


  });
  // kraj loop po svim status conf !!
  
  return res;
  
};



/// --------------------------------------------- ZA SADA NE KORISTIM OVO NIGDJE !!!!!!!!!
function reduce_status_tip(arg_status) {
  
  
  if ( arg_status.status_tip?.next_statuses?.length > 0 ) {

    $.each(arg_status.status_tip.next_statuses, function(n_ind, next) {


      arg_status.status_tip.next_statuses[n_ind] = {

        status_num : next.status_num,
        next_status_num : next.next_status_num,
        naziv : next.naziv,
        sifra : next.sifra,

      };

    });


  };

  if ( arg_status.status_tip?.grana_finish?.length > 0 ) {

    $.each(arg_status.status_tip.grana_finish, function(f_ind, fin) {

      arg_status.status_tip.grana_finish[f_ind] = {

        status_num : fin.status_num,
        next_status_num : fin.next_status_num,
        naziv : fin.naziv,
        sifra : fin.sifra,
      };

    });

  };


  return arg_status.status_tip;
  
  
};
function expand_status_tip(arg_status) {
  
  if ( !window.admin_conf ) {
    popup_warn(`Nedostaje admin datoteka za tipove statusa !!!`);
    return;
  };
  
  if ( arg_status.status_tip?.next_statuses?.length > 0 ) {

    $.each( arg_status.status_tip.next_statuses, function(n_ind, next) {

      var full_status = find_item( window.admin_conf.status_conf, next.sifra, `sifra`);

      arg_status.status_tip.next_statuses[n_ind] = {

        ...full_status, // dodaj sve propse od statusa

        status_num : next.status_num,
        next_status_num : next.next_status_num,
        naziv : next.naziv,
        sifra : next.sifra,

      };

    });


  };

  if ( arg_status.status_tip?.grana_finish?.length > 0 ) {

    $.each( arg_status.status_tip.grana_finish, function(f_ind, fin) {

      var full_status = find_item( window.admin_conf.status_conf, fin.sifra, `sifra`);

      arg_status.status_tip.grana_finish[f_ind] = {

         ...full_status, // dodaj sve propse od statusa

        status_num : fin.status_num,
        next_status_num : fin.next_status_num,
        naziv : fin.naziv,
        sifra : fin.sifra,
      };

    });

  };

  return arg_status.status_tip;
  
  
};



function get_admin_conf() {
  
  
  return new Promise(function(resolve, reject) {
    
      $.ajax({
        headers: {
          'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
        },
        type: "POST",
        cache: false,
        url: `/get_admin_conf`,
        data: JSON.stringify({ bla: "bla" } ),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done( function (result) {
        
        // var result = expand_status_conf(result);

        resolve(result);
        
        
        
      })
      .fail(function (error) {

        resolve(null);  
        
        console.log(error);
        popup_error(`Došlo je do greške na serveru prilikom učitavanja Admin konfiguracije !!!!!`);
      
        
      })
      .always(function() {

        toggle_global_progress_bar(false);


        /*
        toggle_global_progress_bar(false);
        $(`#${rand_id}_save_status`).css("pointer-events", "all");
        $(`#${data.rand_id}_status_work_list_box`).html("");
        */

      });

  
  }); // kraj promisa
  
  
};



async function initial_get_admin_conf() {

  // čekaj da dobijem usera !!!!
  // čekaj da se popuni cit lokal list
  function condition_for_admin_conf() { 
    if ( window.cit_user &&  window.cit_local_list ) { return true; } else { return false };
  };
  
  
  wait_for( 
    condition_for_admin_conf,
    async function () {
      
      var result = await get_admin_conf();
      
      if ( result?.success == true ) {
        window.admin_conf = result.admin_conf;
        console.log(`---------------------------------- dobio sam admin conf !!!! GLOBAL FUNC !!!! ---------------------------------- `);
        window.cit_local_list.status_conf = result.admin_conf.status_conf;
      };
    }, 
    5*1000 
  );
  
};

initial_get_admin_conf();

// obriši loop ako već postoji ( za dobivanje broja svih dstatusa za nekog usera  )
clearInterval(status_count_interval);
var get_status_errors = 0;
var last_status_error_time = 0;

function get_statuses_count() {
  
  
  wait_for( `typeof window.cit_user !== "undefined" && window.cit_user !== null`, function() {
    
    
    if ( !window.cit_user.dep ) return;
    
      $.ajax({
        headers: {
          'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
        },
        type: "POST",
        cache: false,
        url: `/get_statuses_count`,
        data: JSON.stringify( { user_number: window.cit_user.user_number, dep: window.cit_user.dep.sifra } ),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done(function (result) {

        console.log(result);
      
        if ( result.success == true ) {
          get_status_errors = 0;

          wait_for(`$("#cit_my_tasks").length > 0`, function() {

            $(`#cit_my_tasks .badge_value`).text(result.cit_my_tasks);
            $(`#cit_task_from_me .badge_value`).text(result.cit_task_from_me);
            $(`#cit_working .badge_value`).text(result.cit_working);

          }, 5*1000);

        } else {

          // ako result NIJE SUCCESS = true

          if ( get_status_errors == 0 && $(`#layer_under_cit_popup_modal`).hasClass(`cit_show`) == false ) {
            get_status_errors += 1;
            if ( result.msg ) popup_error(result.msg);
            if ( !result.msg ) popup_error(`Greška pretrage broja aktivnih statusa!`);
          }
        };

      })
      .fail(function (error) {
        
        
        if ( get_status_errors == 0 && $(`#layer_under_cit_popup_modal`).hasClass(`cit_show`) == false ) {
          get_status_errors += 1;
          console.log(error);
          popup_error(`Čini se da je privremeno nestala internet konekcija :(<br>Probaj spremiti posao koji radiš i zatim osvježi stranicu sa CTRL+F5 !!!`);
          
        }
      })
      .always(function() {

        toggle_global_progress_bar(false);

      });

  }, 
  3*1000,
  function() { 
    clearInterval(status_count_interval);
    
    $(`#cit_my_tasks .badge_value`).text(``);
    $(`#cit_task_from_me .badge_value`).text(``);
    $(`#cit_working .badge_value`).text(``);
    
  } /* u slučaju ako 3 sekunde nema usera tj nije ulogiran */  );
  
  
}; // kraj func za status count

function get_ex_rates() {
  
  return new Promise(function(resolve, reject) {

    $.ajax({
        type: "GET",
        dataType: "json",
        cache: false,
        url: `/hnb_tecaj`, // `https://www.hnb.hr/tecajn/htecajn.htm`,
        contentType: "application/json; charset=utf-8",
      })
    .done(function (response) {

      console.log(response);
      resolve( response );

    })
    .fail(function (err) {
      console.error("GREŠKA PRI UČITAVANJU TEČAJA SA HNB stranice");
      // popup_error(`GREŠKA PRI UČITAVANJU TEČAJA SA HNB stranice !!!`);
      reject(err);
    });
    
  }); //kraj promisa  
    
  
};

window.EUR_tecaj = 7.55;

get_ex_rates()
.then(function(hnb){
  if ( hnb.success == true ) {
    var EUR_obj = find_item( hnb.result, `EUR`, `valuta`);
    window.EUR_tecaj = cit_number( EUR_obj.srednji_tecaj );
    console.log(` --------------------- EUR TEČAJ ---------------------`, EUR_tecaj );
  } else {
    console.log(` --------------------- EUR TEČAJ GREŠKA ODGOVOR ---------------------`, hnb );
  };
})
.catch( function(error) {
  console.log(` --------------------- EUR TEČAJ GREŠKA REQUEST---------------------`, error );
});


function check_status_place( status_tip_list, arg_status_tip, arg_parent ) {

  // STATUS NE MOŽE BITI POČETAK GRANE AKO JE VEĆ UNTAR NEXT LISTE !!!!
  var is_next_status = false;
  // STATUS NE MOŽE BITI POČETAK GRANE AKO JE UNUTAR FINISH LISTE !!!!
  var is_finish_status = false;

  var has_children = false;

  // loopaj sve statuse i pogledaj jel se trenutni status nalazi unutar next statusa ili finish statusa
  $.each(status_tip_list , function(check_index, check_status,  ) {

    if ( check_status.next_statuses?.length > 0 ) {
      $.each(check_status.next_statuses , function(next_index, next_status ) {
        if ( next_status.sifra == arg_status_tip.sifra ) {
          is_next_status = true;
        };
      });
    };

    if ( check_status.grana_finish?.length > 0 ) {
      $.each(check_status.grana_finish , function(grana_index, grana_finish_object ) {
        if ( grana_finish_object.sifra == arg_status_tip.sifra ) {
          is_finish_status = true;
        };
      });
    };

  });
  
  

  if ( arg_status_tip.next_statuses?.length > 0 ) has_children = true;
  if ( arg_status_tip.grana_finish?.length > 0 ) has_children = true;
  
  
  var next_match = false;
  var finish_match = false;
  
  if ( arg_parent ) {
  
    // ------- OVDJE GLEDAM DIREKTNO PRAVI STATUS OBJEKT DA VIDIM JEL STATUS TIP KOJI SAM DAO KAO ARG PAŠE ILI NE PAŠE
    if ( arg_parent.status_tip?.next_statuses?.length > 0 ) {
      $.each(arg_parent.status_tip.next_statuses , function(next_index, next_status ) {
        if ( next_status.sifra == arg_status_tip.sifra ) {
          next_match = true;
        };
      });
    };

    if ( arg_parent.status_tip?.grana_finish?.length > 0 ) {
      $.each(arg_parent.status_tip.grana_finish , function(grana_index, grana_finish_object ) {
        if ( grana_finish_object.sifra == arg_status_tip.sifra ) {
          finish_match = true;
        };
      });
    };

  }; // kraj jel postoji current status ( koji upravo kreiram )


  return {
    next: is_next_status,
    finish: is_finish_status,
    has_children: has_children,
    
    next_match: next_match,
    finish_match: finish_match,
    
  };

};
// kraj check status place


function get_status_parent(data, curr_status) {
  
  var parent_status = null;
  
  $.each(data.statuses, function( s_ind, status ) {
    if ( status.sifra == curr_status.elem_parent ) parent_status = status;
  });
  
  return parent_status;
  
};

function get_status_children(data, arg_parent) {
  
  var children = [];
  $.each(data.statuses, function( s_ind, status ) {
    if ( status.elem_parent == arg_parent.sifra ) children.push(status);
  });
  return children;
  
};

function is_branch_done(data, arg_parent, arg_children) {
  
  var branch_done = false;
  
  if ( arg_parent.status_tip.grana_finish?.length > 0 ) {
    
    $.each( arg_parent.status_tip.grana_finish, function(f_index, finish) {

      $.each( arg_children, function(c_index, child_status) {
        // ako BILO KOJI CHILD STATUS IMA STATUS TIP KOJI JE  JEDAN OD FINISH OD PARENTA 
        // -----------> onda je grana gotova !!!!
        if ( child_status.status_tip.sifra == finish.sifra ) {
          branch_done = true;
        };
      });
    });
    
  };

  return branch_done;
  
};








function upsert_proces_element(elements, proces_elem) {
  
  
};




function ajax_complete_branch(status_reply) {

  return new Promise( function(resolve, reject) {

    $.ajax({
      headers: {
        'X-Auth-Token': window.cit_user.token
      },
      type: "POST",
      cache: false,
      url: `/get_complete_branch`,
      data: JSON.stringify( status_reply ),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done( async function (result) {

      console.log(result);

      if ( result.success == true ) {

        resolve(result.branch);

      } else {

        // ako result NIJE SUCCESS = true

        if ( result.msg ) console.error(result.msg);
        if ( !result.msg ) console.error(`Greška prilikom spremanja STATUSA!`);

        resolve(null);

      };

    })
    .fail(function (error) {
      console.error(`Došlo je do greške na serveru prilikom spremanja STATUSA!`);
      console.log(error);
      resolve(null);
    })
    .always(function() {

      toggle_global_progress_bar(false);

    });

  }); // kraj promisa  

}; // kraj get_ complete_branch



// reply_to_status_tip_sifra -------> NA PRIMJER 
// KADA JE STATUS ZA ORDER DOBAV ----> ODGOVOR NA OFFER DOBAV
// odogvaram na  "POSLANI UPITI DOBAV. SIROVINA", ---> tj offer status
// if ( data_for_doc.doc_type == `order_dobav` ) reply_to_status_tip_sifra = "IS16340541453248845"; 

function write_reply_for_or_work_done( current_status, branch, reply_to_status_tip_sifra ) {

  
  var last_status_time = 0;
  var last_status = null;
  var parent_status = null;


  var reply_for = null;
  var done_for_status = null;
  
  var elem_parent_object = null;
  var elem_parent = null;
  
  // neka najstarij budu prvi prema najnovijima
  multisort( branch, [ 'time' ] );
  
  $.each( branch, function(b_ind, branch_status) {

    // parent
    if ( branch_status.elem_parent == null ) {
      parent_status = cit_deep(branch_status);
      last_status = cit_deep(branch_status); // ako nema djece onda će last status biti parent
    };

    // zadnje djete ne smje biti elem parent null i time mora biti veći
    if ( branch_status.elem_parent !== null && branch_status.time > last_status_time ) {
      last_status_time = branch_status.time;
      last_status = cit_deep(branch_status); // u ovom loop će doći najvećeg datuma i to znači da je ovaj status zadnji
    };

  });
  
  
  if ( 
    current_status 
    &&
    current_status.status_tip 
    &&
    current_status?.status_tip?.work_done_for !== null
  ) {

    // ------------------------------------------------------------------------------------------
    //       ODGOVOR NA RADNI STATUS MOŽE SAMO ODGOVORITI USER KOJI GA JE NAPRAVIO !!!
    // ------------------------------------------------------------------------------------------


    reply_for = null;
    
     /* bilo koji child od tog parenta */
    // vrti po svim statusima u branch-u
    
    
    // ovo mi služi samo da prestane tražiti kad nadje prvi status koji odgovara uvijetima
    // jer se može dogoditi da ima više statusa koji trebaju biti završeni !!!
    var found_status_for_work_done = false; 
    
    $.each(branch, function( c_ind, find_status) {

        if ( 
          
          found_status_for_work_done == false
          &&
          /* bilo koji child od tog parenta */
          find_status.elem_parent == parent_status.sifra 
          &&
          /* i ako je tip statusa od child jednak završnom poslu (work_done_for) u trenutnom statusu */
          find_status.status_tip.sifra == current_status.status_tip.work_done_for
          /* ALI SAMO AKO DO SADA NIJE UPISANO DA JE TAJ POSAO GOTOV tj nije upisano vrijeme kraja !!! */
          &&
          !find_status.end

        ) {

          
          found_status_for_work_done = true;
          
          done_for_status = find_status.sifra;
          branch[c_ind].end = Date.now();
          
          elem_parent_object = cit_deep( parent_status );
          elem_parent = parent_status.sifra;

        };

    });
    
    /* AKO JE PARENT */
    // vrti po svim statusima u branch-u
    $.each(branch, function( c_ind, find_status) {

      if ( 

        found_status_for_work_done == false
        &&
        /* AKO JE PARENT */
        find_status.elem_parent == null
        &&
        /* i ako je tip statusa od PARENTA jednak završnom poslu (work_done_for) u trenutnom statusu */
        find_status.status_tip.sifra == current_status.status_tip.work_done_for
        /* ALI SAMO AKO DO SADA NIJE UPISANO DA JE TAJ POSAO GOTOV tj nije upisano vrijeme kraja !!! */
        &&
        !find_status.end

      ) {

        found_status_for_work_done = true;
        
        done_for_status = find_status.sifra;
        branch[c_ind].end = Date.now();
        
        elem_parent_object = cit_deep( parent_status );
        elem_parent = parent_status.sifra;
        

      };

    });
    
  } 
  else {

    // ------------------------------------------------------------------------------------------
    //       ODGOVOR NA OBIČNI STATUS KOJI NIJE RADNI 
    // ------------------------------------------------------------------------------------------

    // ako nije radni status onda nemoj upisivati bilo koju šifru
    done_for_status = null;


    // ako sam dao 
    if ( reply_to_status_tip_sifra  ) {
      
      $.each(branch, function( c_ind, find_status) {
        if ( 
          
          find_status.status_tip?.sifra == reply_to_status_tip_sifra 
          &&
          find_status.responded !== true 
          
        ) { 
          // ako je replay onda uvijek zapiši na sifru na koji to status odgovaram
          reply_for = find_status?.sifra || null;
        };
        
      });
      
      
    } else {
      
      // ako nema definiran status tip za reply onda UVIJEK odgovori na PARENT !!!!!
      reply_for = parent_status?.sifra || null; //     || current_status?.sifra 
    
    };
    
    
    elem_parent_object = cit_deep( parent_status );
    elem_parent = parent_status.sifra;
    

    // samo lokalno upiši update na statusu na koji odgovoram
    // IONAKO ĆE SE TO NA SERVERU UPISATI U BAZU ----> ovo je samo da se lokalno vidi odmah ----> razlika će biti mala u par milisekundi :)
    $.each(branch, function( s_ind, status ) {
      if ( reply_for && status.sifra == reply_for ) {
        branch[s_ind].end = Date.now();
      };
    });

  };
  // kraj else tj. ako NIJE RADNI STATUS !!!

  
  
  return {
    
    reply_for,
    done_for_status,
    
    last_status,
    
    elem_parent_object,
    elem_parent,
    
    statuses : branch
  }
  
};

window.registered_grab_scroll_array = {};

function register_grab_to_scroll( arg_elem_id, prop ) {
  
  const ele = $("#"+arg_elem_id);
  let pos = { top: 0, left: 0, x: 0, y: 0 };
  
  var kal_users_wrapper = $('#kal_users_wrapper');

  const mouseDownHandler = function(e) {

    if ( $(e.target).closest(`.kalk_naslov`).length > 0 ) {
      
      console.log(`KLIKNUT NASLOV`);
      
      /*
      $(document).off(`mousemove.${arg_elem_id}`);
      $(document).off(`mouseup.${arg_elem_id}`);
      $(document).off(`mousemove.${arg_elem_id}`);
      ele.off( `mousedown.${arg_elem_id}`);
      */
      return;
    };
    
    ele.css(`cursor`, 'grabbing');
    ele.css(`user-select`, 'none');

    pos = {
        // The current scroll 
        left: ele[0].scrollLeft,
        top: ele[0].scrollTop,
        // Get the current mouse position
        x: e.clientX,
        y: e.clientY,
    };

    // document.removeEventListener('mousemove', mouseMoveHandler);
    // document.addEventListener('mousemove', mouseMoveHandler);
  
    $(document).off(`mousemove.${arg_elem_id}`); // , mouseMoveHandler);
    $(document).on(`mousemove.${arg_elem_id}`, mouseMoveHandler);

    // document.removeEventListener('mouseup', mouseUpHandler);
    // document.addEventListener('mouseup', mouseUpHandler);
    
    $(document).off(`mouseup.${arg_elem_id}`) // , mouseUpHandler);
    $(document).on(`mouseup.${arg_elem_id}`, mouseUpHandler);
    
  };
  
  
  const mouseMoveHandler = function(e) {
    // How far the mouse has been moved
    const dx = e.clientX - pos.x;
    const dy = e.clientY - pos.y;

    // Scroll the element
    ele[0].scrollTop = pos.top - dy;
    ele[0].scrollLeft = pos.left - dx;

    var move_x =  -1 * ele[0].scrollLeft;

    if ( move_x > 0 ) move_x = 0

    var max_scroll = -1*( ele[0].scrollWidth - ele.width() );

    if ( move_x < max_scroll ) move_x = max_scroll;

    if ( kal_users_wrapper.length > 0 ) kal_users_wrapper.css(`transform`,  `translateX( ${move_x}px )`);

  };


  const mouseUpHandler = function(e) {

    // ele.style.cursor = 'grab';
    // ele.style.removeProperty('user-select');

    
    ele.css(`cursor`, ``);
    ele.css(`user-select`, ``);

    
    if ( prop == `kalk_scroll` ) {
      
      var jel_kalk_box = $(e.target).closest(`.kalk_box`).length > 0;
      if ( !jel_kalk_box ) return;
      
      var this_comp_id = (ele && ele.closest('.cit_comp').length > 0) ? (ele.closest('.cit_comp')[0].id || null) : null;
      
      if ( this_comp_id ) {
        
        var data = window.kalkulacija_module.cit_data[this_comp_id];
        var rand_id =  window.kalkulacija_module.cit_data[this_comp_id].rand_id;

        var product_id = $('#'+data.id).closest('.product_comp')[0].id;
        var product_data =  window.kalkulacija_module.product_module.cit_data[product_id];

        var kalk_type = ele.attr(`data-kalk_type`);
        var kalk_sifra = ele.attr(`data-kalk_sifra`);

        var curr_kalk = window.kalkulacija_module.kalk_search( data, kalk_type, kalk_sifra, null, null, null, null );

        curr_kalk[prop] = ele[0].scrollLeft;

        // console.log( curr_kalk );
        
      }; 
      
    }; // kraj ako ima argument za kalk scroll

    // document.removeEventListener('mousemove', mouseMoveHandler);
    
    $(document).off(`mousemove.${arg_elem_id}`) // , mouseMoveHandler);
    
  };  


  // const ele = document.getElementById(arg_elem_id);
  

  // document.removeEventListener('mousemove', mouseMoveHandler);
  // document.removeEventListener('mouseup', mouseUpHandler);
  // ele.removeEventListener('mousedown', mouseDownHandler);    
  // ele.addEventListener('mousedown', mouseDownHandler);
  
  $(document).off(`mousemove.${arg_elem_id}`); // , mouseMoveHandler);
  $(document).off(`mouseup.${arg_elem_id}`);  // , mouseUpHandler);
  ele.off( `mousedown.${arg_elem_id}`); // , mouseDownHandler);
  
  ele.on( `mousedown.${arg_elem_id}`, mouseDownHandler);
  
};

function send_mail_from_user( arg_body, arg_subject, to_arr, cc_arr, bcc_arr, att_files, arg_from ) {
  
  toggle_global_progress_bar(true);
  
  return new Promise(function(resolve, reject) {
    
      $.ajax({
        headers: {
          'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
        },
        type: "POST",
        cache: false,
        url: `/send_mail_from_user`,
        data: JSON.stringify({ arg_body, arg_subject, to_arr, cc_arr, bcc_arr, att_files, arg_from }),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done( function (result) {

        resolve(result);
      })
      .fail(function (error) {

        resolve(null);  
        
        console.log(error);
        popup_error(`Došlo je do greške na serveru prilikom slanja emaila !!!!!`);
      
      })
      .always(function() {

        toggle_global_progress_bar(false);


        /*
        toggle_global_progress_bar(false);
        $(`#${rand_id}_save_status`).css("pointer-events", "all");
        $(`#${data.rand_id}_status_work_list_box`).html("");
        */

      });


  
  }); // kraj promisa
  
  
};

function get_super_editors() {
  
  var super_editori = [

    67, // Bruno Žgela
    24, // Stjepan Gašljević
    30, // Anel
    10, // Toni
    9,  // Radmila
    42, // Alex
    62, // Zoran
    58, // Martina Šaravanja

  ];
  
  return super_editori;      
};

function get_fin_editors() {
  
  var fin_editori = [

    14, // Marina
    204, // Ivona
    10, // Toni
    9,  // Radmila
  ];
  
  return fin_editori;      
};



function focus_on_card_input() {
  
  $(`#login_user_card`).val(``);
  $(`#login_user_card`).trigger(`click`); 
  $(`#login_user_card`).trigger(`focus`);
  $(`#login_user_card`).trigger(`select`);

};


function ask_before_close_register_event() {

  window.onbeforeunload = function (e) {
    e = e || window.event;
    // For IE and Firefox prior to version 4
    if (e) { e.returnValue = 'Sure?'; };
    // For Safari
    return 'Sure?';
  };
    
};


function register_autocomplete_listeners() {


  $('body').off('click', '.cit_accord', click_accord);
  $('body').on('click', '.cit_accord', click_accord); 
  
  $('body').off('click', '.cit_tab_btn', open_tab);
  $('body').on('click', '.cit_tab_btn', open_tab); 
  
  
  $('body').off('click', '.cit_switch', toggle_cit_switch);
  $('body').on('click', '.cit_switch', toggle_cit_switch); 

  $('body').off('click', '.cit_check_box', toggle_cit_check_box);
  $('body').on('click', '.cit_check_box', toggle_cit_check_box); 
  
 
  // --------------------------- CIT AUTO ---- SA CLICK I SA TAB --------------------------------------- 
  $('body').off('click', '.cit_auto', handle_cit_auto_click);
  $('body').on('click', '.cit_auto', handle_cit_auto_click);
  
  $('body').off('focus', '.cit_auto', handle_cit_auto_click);
  $('body').on('focus', '.cit_auto', handle_cit_auto_click);
  
  
  $('body').off('keyup', '.cit_auto', handle_cit_auto_keyup);
  $('body').on('keyup', '.cit_auto', handle_cit_auto_keyup);
  
  
  
  $('body').off('keyup', '.simple_calendar', handle_simple_calendar_blur);
  $('body').on('keyup', '.simple_calendar', handle_simple_calendar_blur);
  
  
  
  
  // --------------------------- CIT INPUT ---- SA CLICK I SA TAB --------------------------------------- 
  $('body').off('click', '.cit_input', handle_cit_input_click);
  $('body').on('click', '.cit_input', handle_cit_input_click);
  $('body').off('focus', '.cit_input', handle_cit_input_click);
  $('body').on('focus', '.cit_input', handle_cit_input_click);
  
  
  
  /*
  window.close_drop_list_timeout = null;
  
  $('body').off('focus', '.cit_auto', handle_cit_auto_click);
  $('body').on('focus', '.cit_auto', handle_cit_auto_click );
  
  $('body').off('blur', '.cit_auto');
  $('body').on('blur', '.cit_auto', function() {
    window.close_drop_list_timeout = setTimeout( function() { close_cit_drop_list(); }, 300);
  });
  
  $('body').off('focus', '.cit_input', handle_cit_input_click);
  $('body').on('focus', '.cit_input', handle_cit_input_click);
  
  */
    
  
  $('body').off('click', '.cit_text_area', handle_cit_input_click);
  $('body').on('click', '.cit_text_area', handle_cit_input_click);
  

  $('body').off('click', handle_hide_autocomplete_list);
  $('body').on('click', handle_hide_autocomplete_list);


  $('body').off('click', '.cit_search_result_image', handle_click_result_image);
  $('body').on('click', '.cit_search_result_image', handle_click_result_image);    

  
  $('body').off('click', '.search_result_row', select_result_red);
  $('body').on('click', '.search_result_row', select_result_red);
  
  
  $(document).off('scroll.drop_list')
  $(document).on('scroll.drop_list', drop_list_follows_curr_input );
  
  

};


$(document).ready(function () {
  
  // toggle_global_progress_bar(true); 
  
  var loaded_count = 0;
  var loaded_comp_array = [];
  
  
  var modules_to_load = [
    '/modules/main_layout_module.js',    
  ];  
    
  // LOAD ALL MODULES FOR GENERAL COMPONENTS
  $.each(modules_to_load, function (index, module_url) {
    // get_cit_module(module_url, 'load_css');  
  });
  
  function all_components_loaded() {
    
    var all_comps_loaded = false;
    
    loaded_count = 0;
    loaded_comp_array = [];
    
    // loop listu za loadanje i provjeri jel svaka od njih loadana
    $.each(modules_to_load, function(index, url) {
      if ( window[url] && window[url].cit_loaded ) {
        loaded_count += 1;
        loaded_comp_array.push(url);
      };
    });
    
    // dodatni uvijet je da sam dobio response od requesta za listu svih parkinga
    if (modules_to_load.length == loaded_count) all_comps_loaded = true;
    return all_comps_loaded ;
  };

  /*
  wait_for( 
    all_components_loaded, 
    function() {

      console.log('ALL CIT COMPONENTS SUCCESSFULLY LOADED HOORAY !!!!'); 
      
      // loadaj module za popup-e SAMO JEDNOM !!!!
      // kasnije za svaki popup promijenim html ovisno o tome što mi treba
      // var popup_modal = window['js/popup_modal/cit_popup_modal.js'];
      // popup_modal.create(null, $('body'), 'prepend');  
      
    },
    ( 1000 * 60 * 5  ), 
    function() { 
      alert('Some modules or components failed to load !!!!');
      
      var missing_modules = [];

      $.each(modules_to_load, function(index, url) {
        var url_missing = true;
        $.each(loaded_comp_array, function(index, loaded_url) {
          if ( url == loaded_url ) url_missing = false;
        });
        if ( url_missing == false ) missing_modules.push(url);
      });

      console.log(missing_modules);
    }
  );    

  */
  
  register_autocomplete_listeners();
  
}); // END OF $(document).ready ....


