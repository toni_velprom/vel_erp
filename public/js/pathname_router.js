
let pathname_data = null;

function parse_router( pathname, pattern ) {
  
  let curr_url_array = pathname.split('/');
  let pattern_array = pattern.split('/');
  
  if ( curr_url_array.length !== pattern_array.length ) {
    // console.log('pathname NOT MATCHED!!!!!!')
    return false;
  };
  
  
  pathname_data = {};
  
  $.each(pattern_array, function( index, pattern_string ) {
    if ( pattern_string.indexOf(':') == -1 ) pathname_data[pattern_string] = index;
  });
  
  var pathname_fits_pattern = true;
  
  $.each(pathname_data, function( pathname_key, index ) {
    
    if ( pathname_fits_pattern && curr_url_array[index] == pathname_key ) {
      var pathname_value = null;
      if ( curr_url_array[index+1] ) {
        var next_url_string = curr_url_array[index+1];
        if ( $.isNumeric(next_url_string) ) {
          pathname_value = Number(next_url_string);
        } else {
          try {
            pathname_value = JSON.parse(next_url_string);
          } catch(error) {
            // console.log(error);
          }
        };
        // ostavi da bude string ako nije broj i nije array i nije objekt
        if ( pathname_value == null ) pathname_value = next_url_string;
      };
      
      pathname_data[pathname_key] = pathname_value;
      
    } else {
      
      pathname_fits_pattern = false;
      // console.log('pathname NOT MATCHED!!!!!!')
      
    };
      
  });

  return pathname_fits_pattern;
  
};


function router_listener() {
  
  
  // reset pathname data
  pathname_data = null;
  
  var curr_pathname = window.location.pathname.replace('/', '');
  curr_pathname = decodeURI(curr_pathname);
  
  if ( parse_router(curr_pathname, `user`) ) {
    console.log( pathname_data );
    return;
  };
  
  
  if ( parse_router(curr_pathname, `product`) ) {
    console.log( pathname_data );
    return;
  };
  
  
  if ( parse_router(curr_pathname, `product/:prod_id/count/:prod_count`) ) {
    console.log( pathname_data );
    return;
  };
  
  
  if ( parse_router(curr_pathname, `user/:user_id/role/:role_id`) ) {
    console.log( pathname_data );
    return;
  };
  
  console.log('pathname NOT MATCHED!!!!!!');
  
};

// first time on frist load
router_listener();  
  
// window.addEventListener('popstate', router_listener, false);



function update_pathname(new_pathname) {
  
  history.pushState({}, null, '/bla');
  
};
