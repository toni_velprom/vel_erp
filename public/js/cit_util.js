// ADDITIONAL OPTIONS FOR NATIVE JAVASCRIPT SORT ------- from http://jsfiddle.net/gfullam/sq9U7/  
// BITNO !!!
// reverse arg se prenosi tj. on je inherited
// dakle ako prvi stupac reversam (descending ) sa true
// onda drugi stupac ako ostavim true bit će ascending tj OBRNUTO OD PARENTA !!!!

window.cit_by = function(path, reverse, primer, then) {
  var get = function (obj, path) {
      if (path) {
        path = path.split('.');
        for (var i = 0, len = path.length - 1; i < len; i++) {
          obj = obj[path[i]];
        };
        return obj[path[len]];
      }
      return obj;
    },
    prime = function (obj) {
      return primer ? primer(get(obj, path)) : get(obj, path);
    };

  return function (a, b) {
    var A = prime(a),
      B = prime(b);

    return (
      (A < B) ? -1 :
      (A > B) ? 1 :
      (typeof then === 'function') ? then(a, b) : 0
    ) * [1, -1][+!!reverse];
  };
};

function filename_safe(str) {
  
  var just_name = str.substring(0, str.lastIndexOf(".") );
  var ext = str.substring( str.lastIndexOf(".")+1 );

  just_name = just_name.replace(/\-/gi, "_");  
    
  just_name = just_name.replace(/š/gi, "s");
  just_name = just_name.replace(/đ/gi, "dj");
  just_name = just_name.replace(/ć/gi, "c");
  just_name = just_name.replace(/ž/gi, "z");
  just_name = just_name.replace(/[^a-z0-9_\-]/gi, '_').toLowerCase();
  
  return (just_name+"."+ext);
  
};


function esc_html(htmlStr) {
  
  if ( !htmlStr ) return "";
  
  return htmlStr.replace(/&/g, "&amp;")
       .replace(/</g, "&lt;")
       .replace(/>/g, "&gt;")
       .replace(/"/g, "&quot;")
       .replace(/'/g, "&#39;");        

};


function un_esc_html(htmlStr) {
  
  if ( !htmlStr ) return "";

  htmlStr = htmlStr.replace(/&lt;/g , "<");	 
  htmlStr = htmlStr.replace(/&gt;/g , ">");     
  htmlStr = htmlStr.replace(/&quot;/g , "\"");  
  htmlStr = htmlStr.replace(/&#39;/g , "\'");   
  htmlStr = htmlStr.replace(/&amp;/g , "&");
  
  return htmlStr;
};


function time_path(arg_time) {
  
  var path = ``;
  
  if( !arg_time ) {
    popup_warn(`Nedostaje vrijeme kreiranja dokumenta !!!!`);
    return;
  };
  
  var yyyy = new Date(arg_time).getFullYear();
  var month = new Date(arg_time).getMonth() + 1;
  if (month < 10) month = `0` + month;
  
  path = `${yyyy}/${month}`;
  
  return path;
  
};

function inside_interval( time_start, time_end, interval_start, interval_end ) {
  
  
  var dur_start = null;
  var dur_end = null;
  var dur = null;
  var place = null;
  
  if ( time_start < interval_start && time_end <= interval_start ) {
    dur_start = null;
    dur_end = null;
    dur = null;
    place = `before/before`;
  };
  
  if ( time_start < interval_start && time_end > interval_start && time_end <= interval_end ) {
    dur_start = interval_start;
    dur_end = time_end;
    dur = dur_end - dur_start;
    place = `before/inside`;
  };
  
  
  if ( time_start >= interval_start && time_end <= interval_end ) {
    dur_start = time_start;
    dur_end = time_end;
    dur = dur_end - dur_start;
    place = `inside/inside`;
  };
  
  if ( time_start >= interval_start && time_start < interval_end && time_end >= interval_end ) {
    dur_start = time_start;
    dur_end = interval_end;
    dur = dur_end - dur_start;
    place = `inside/after`;
  };
  
  if ( time_start >= interval_end && time_end > interval_end ) {
    dur_start = null;
    dur_end = null;
    dur = null;
    place = `after/after`;
  };  
  
  
  return {
    dur_start,
    dur_end,
    dur,
    place,
  }
  
};


function cit_dt(number, date_format) {
   
  var date = null;

  // ako nije undefined ili null onda napravi datum od broja 
  if ( number || number === 0 ) {
    date = new Date( number );  
  } else {
    date = new Date();  
  };

  var sek = date.getSeconds();
  var min = date.getMinutes();
  var hr = date.getHours();
  var dd = date.getDate();
  var mm = date.getMonth() + 1; // January is 0!
  var yyyy = date.getFullYear();

  // ako je samo jedna znamenka
  if (dd < 10) {
    dd = '0' + dd;
  };
  // ako je samo jedna znamenka
  if (mm < 10) {
    mm = '0' + mm;
  };

  // ako je samo jedna znamenka
  if (sek < 10) {
    sek = '0' + sek;
  };


  // ako je samo jedna znamenka
  if (min < 10) {
    min = '0' + min;
  };

  // ako je samo jedna znamenka
  if (hr < 10) {
    hr = '0' + hr;
  };

  if ( !date_format || date_format == 'd.m.y' ) {
    date =  dd + '.' + mm + '.' + yyyy;
  };


  // ako nije neveden format ili ako je sa crticama
  if ( date_format == 'y_m_d' ) {
    date = yyyy + '_' + mm + '_' + dd;
  };  

  // ako nije neveden format ili ako je sa crticama
  if ( date_format == 'y-m-d' ) {
    date = yyyy + '-' + mm + '-' + dd;
  };


  time = hr + ':' + min + ':' + sek;


  return {
    date: date,
    time: time,
    date_time: date + " " + time,
  }

};

function cit_h(min) {
  
  
  if ( !min ) return "";
  
  var h = min/60;
  var h_int = parseInt(h);
  
  var ostatak_min = `00`;
      
  if ( h - h_int !== 0 ) {
    // npr 628 minuta = 628/60 = 10.466666666 ------> integer je 10
    // ostatak = 10.466666666  - 10 = 0.466666666 -----------> 0.466666666 * 60 = 28 min
    ostatak_min = cit_round((h - h_int)*60, 0);
    
    
  };

  
  h_int = h_int + "";
  if ( h_int.length < 2 ) h_int = "0" + h_int;
  
  ostatak_min = ostatak_min + "";
  if ( ostatak_min.length < 2 ) ostatak_min = "0" + ostatak_min;
  
  return (h_int + ":" + ostatak_min);
  
};

function allow_weekend_days(date, config) {

  if ( typeof date == 'number' ) date = new Date(date);

  if ( config.sub == false && config.ned == false ) {
    return ( date.getDay() !== 0 && date.getDay() !== 6 );
  } 
  else if ( config.sub == true && config.ned == false ) {
     return ( date.getDay() !== 0 );
  } 
  else if ( config.sub == false && config.ned == true ) {
     return ( date.getDay() !== 6 );
  } 
  else if ( config.sub == true && config.ned == true ) {
     return true;
  }     

};

function work_time_reverse( end_time, dani, arg_work_options ) {


  var radne_min_sum = 0;
  var minute_counter;
  var new_time;
  
  var work_options = null;
  
  if ( !arg_work_options ) {
    
     work_options = {
      in: 6,
      out: 21,
      sub: false,
      ned: false
    };
    
  } else {
    
    work_options = arg_work_options;
  };

  var radne_minute_ukupno = dani * (work_options.out - work_options.in) * 60; // brojim da radni dan ima 10 sati
  
  for( minute_counter = 0 ; minute_counter <= 1000*60*60*24*90; minute_counter += (1000*60) ) {

    new_time = end_time - minute_counter;
    var curr_date = new Date(new_time);
    
    var curr_yyyy = curr_date.getFullYear();
    var curr_mnt = curr_date.getMonth();
    var curr_day = curr_date.getDate();
    
    // var curr_hours   = curr_date.getHours();
    // var curr_minutes = curr_date.getMinutes();
    // var curr_seconds = curr_date.getSeconds();
    
    
    var curr_date_zero = new Date( curr_yyyy, curr_mnt, curr_day, 0 );
    curr_date_zero = Number(curr_date_zero);
    
    var start = curr_date_zero + ( 1000*60*60 *  work_options.in ) ; // brojim da se počine raditi u 8h
    var end = curr_date_zero + ( 1000*60*60 *  work_options.out ) ; // brojim da se počine raditi u 18h

    if ( 
      minute_counter
      &&
      allow_weekend_days(new_time, work_options)
      &&
      new_time >= start && new_time <= end
      // curr_date.getHours() >= 8 && curr_date.getHours() <= 18 // radni dan između 8 - 18
    ) {
      radne_min_sum += 1; // povećaj jednu minutu
      if ( radne_minute_ukupno <= radne_min_sum ) { break; }
    };

  };

  return new_time;

};

function work_time_forward( start_time, dani, arg_work_options ) {

  
  var radne_min_sum = 0;
  var minute_counter;
  var new_time;
  
  
  var work_options = null;
  
  if ( !arg_work_options ) {
    
     work_options = {
      in: 6,
      out: 21,
      sub: false,
      ned: false
    };
    
  } else {
    
    work_options = arg_work_options;
  };
  
  var radne_minute_ukupno = dani * (work_options.out - work_options.in) * 60; // brojim da radni dan ima 10 sati

  for( minute_counter = 0 ; minute_counter <= 1000*60*60*24*90; minute_counter += (1000*60) ) {

    new_time = start_time + minute_counter;
    var curr_date = new Date(new_time);
    
    var curr_yyyy = curr_date.getFullYear();
    var curr_mnt = curr_date.getMonth();
    var curr_day = curr_date.getDate();
    
    // var curr_hours   = curr_date.getHours();
    // var curr_minutes = curr_date.getMinutes();
    // var curr_seconds = curr_date.getSeconds();
    
    
    var curr_date_zero = new Date( curr_yyyy, curr_mnt, curr_day, 0 );
    curr_date_zero = Number(curr_date_zero);
    
    var start = curr_date_zero + ( 1000*60*60 *  work_options.in ) ; // brojim da se počine raditi u 8h
    var end = curr_date_zero + ( 1000*60*60 *  work_options.out ) ; // brojim da se počine raditi u 18h

    if ( 
      minute_counter // ako counter nije nula
      &&
      allow_weekend_days(new_time, work_options)
      &&
      new_time >= start && new_time <= end
      // curr_date.getHours() >= 8 && curr_date.getHours() <= 18 // radni dan između 8 - 18
    ) {
      radne_min_sum += 1; // povećaj jednu minutu
      if ( radne_minute_ukupno <= radne_min_sum ) { break; }
    };

  };

  return new_time;

};

function work_minutes_reverse( start_time, end_time, arg_work_options ) {

  
  if ( start_time < end_time ) {
    popup_warn(`Početno vrijeme ne smije biti MANJE od krajnjeg vremena jer work_minutes_reverse funkcija broji vrijeme unazad !!!`);
  };

  var radne_min_sum = 0;
  var minute_counter;
  var new_time;
  
  var work_options = null;
  
  if ( !arg_work_options ) {
    
     work_options = {
      in: 6,
      out: 21,
      sub: false,
      ned: false
    };
    
  } else {
    
    work_options = arg_work_options;
  };
  
  for( minute_counter = 0 ; minute_counter <= 1000*60*60*24*90; minute_counter += (1000*60) ) {

    new_time = start_time - minute_counter;
    var curr_date = new Date(new_time);
    
    var curr_yyyy = curr_date.getFullYear();
    var curr_mnt = curr_date.getMonth();
    var curr_day = curr_date.getDate();
    
    // var curr_hours   = curr_date.getHours();
    // var curr_minutes = curr_date.getMinutes();
    // var curr_seconds = curr_date.getSeconds();
    
    
    var curr_date_zero = new Date( curr_yyyy, curr_mnt, curr_day, 0 );
    curr_date_zero = Number(curr_date_zero);
    
    var start = curr_date_zero + ( 1000*60*60 *  work_options.in ) ; // brojim da se počine raditi u 8h
    var end = curr_date_zero + ( 1000*60*60 *  work_options.out ) ; // brojim da se počine raditi u 18h

    if ( 
      minute_counter
      &&
      allow_weekend_days(new_time, work_options)
      &&
      new_time >= start && new_time <= end
      // curr_date.getHours() >= 8 && curr_date.getHours() <= 18 // radni dan između 8 - 18
    ) {
      
      radne_min_sum += 1; 
      // pošto idem unazad vrijeme ne smije biti manje od krajnog vremena
      if ( new_time <= end_time ) { break; };
      
    };

  };

  return radne_min_sum;

};

function work_minutes_forward( start_time, end_time, arg_work_options ) {


  if ( start_time > end_time ) {
    popup_warn(`Početno vrijeme ne smije biti VEĆE od krajnjeg vremena!!!`);
  };
  
  
  var radne_min_sum = 0;
  var minute_counter;
  var new_time;
  
  var work_options = null;
  
  if ( !arg_work_options ) {
    
     work_options = {
      in: 6,
      out: 21,
      sub: false,
      ned: false
    };
    
  } else {
    
    work_options = arg_work_options;
  };

  
  for( minute_counter = 0 ; minute_counter <= 1000*60*60*24*90; minute_counter += (1000*60) ) {

    new_time = start_time + minute_counter;
    var curr_date = new Date(new_time);
    
    var curr_yyyy = curr_date.getFullYear();
    var curr_mnt = curr_date.getMonth();
    var curr_day = curr_date.getDate();
    
    // var curr_hours   = curr_date.getHours();
    // var curr_minutes = curr_date.getMinutes();
    // var curr_seconds = curr_date.getSeconds();
    
    
    var curr_date_zero = new Date( curr_yyyy, curr_mnt, curr_day, 0 );
    curr_date_zero = Number(curr_date_zero);
    
    var start = curr_date_zero + ( 1000*60*60 *  work_options.in ) ; // brojim da se počine raditi u 8h
    var end = curr_date_zero + ( 1000*60*60 *  work_options.out ) ; // brojim da se počine raditi u 18h

    if ( 
      minute_counter
      &&
      allow_weekend_days(new_time, work_options)
      &&
      new_time >= start && new_time <= end
      // curr_date.getHours() >= 8 && curr_date.getHours() <= 18 // radni dan između 8 - 18
    ) {
      
      radne_min_sum += 1; 
      // pošto idem UNAPRIJED vrijeme ne smije biti VEĆE od krajnog vremena
      if ( new_time >= end_time ) { break; };
      
    };

  };

  return radne_min_sum;

};


function cit_number(num_string) {
  
  var cit_num = null;
  
  // ako je pravi broj pretvori u string
  if ( typeof num_string == "number" ) {
    num_string = num_string + "";
    if ( num_string.indexOf(`.`) > -1 ) num_string = num_string.replace(`.`, `,`);
  };
  
  
  // ako je već dobio broj (  greškom ... :-) jer inače očekuje string )
  // if ( $.isNumeric(num_string) == true ) return Number(num_string);
  
  // ako je bilo što osim stringa i broja ( ako je broj onda stane na gornji return )
  if ( typeof num_string !== "string" ) return null;
  
  // obriši sve točke od tisućica
  num_string = num_string.replace(/\./g,'');
  
  // ako je netko napravio grešku :)
  num_string = num_string.replace(/\,\,\,/, `.`);
  // isto ----> ako je netko napravio grešku :)
  num_string = num_string.replace(/\,\,/, `.`);
  
  num_string = num_string.replace(`,`,`.`);
  
  if ( $.isNumeric(num_string) ) cit_num = Number(num_string);
  
  return cit_num;
  
};

function cit_format(num, zaokruzi_na) {
  // primjer : var num = 123456789.56789;
  
  if ( cit_number(num) === null ) return ''; // vrati prazan string ako rezultat cit number ---> null
  
  var brojZnamenki = 2;
  if ( typeof zaokruzi_na !== 'undefined' )  brojZnamenki = parseInt( zaokruzi_na );
  

  var zaokruzeniBroj = cit_round(num, brojZnamenki);
  

  var stringBroja = String(zaokruzeniBroj);
  // 123456789
  var lijeviDio = stringBroja.split('.')[0] + "";
  var tockaPostoji = stringBroja.indexOf(".");
  var desniDio = "00";
  if (tockaPostoji > -1) {
    desniDio = stringBroja.split('.')[1] + "";
  };

  /*
  if (Number(desniDio) < 10) {
    desniDio = '0' + Number(desniDio);
  };
  */

  
  var broj_je_pozitivan = true;
  
  // ako je broj negativan 
  if ( lijeviDio.substring(0,1) == '-' ) {
    lijeviDio = lijeviDio.substring(1);
    broj_je_pozitivan = false;
  };
  
  var stringLength = lijeviDio.length;
  
  // ne zanimaju me brojevi s tri znamenke i manje jer oni nemaju točke
  if (stringLength >= 4) {
    // pretvaram string u array
    lijeviDio = lijeviDio.split('');
    // gledam koliko stane grupa po 3 znamaneke
    // i onda uzmem ostatak - to je jednostavna modulus operacija
    var dotPosition = stringLength % 3;
    // zanima me koliko treba biti točaka u broju
    // to dobijem tako da podijelim broj znamenki sa 3 i gledam samo cijeli broj
    var dotCount = parseInt(stringLength / 3)

    // postavim prvu točku
    // NAPOMENA u slučajevima kada je modulus = 0 ova funkcije postavi točku čak i na počatek broja
    // tu točku kasnije obrišem
    // svaka sljedeća točka je UVIJEK 4 pozicije udesno !!!!
    // napomena - logično bi bilo da je svake 3 pozicije udesno, 
    // ali moram brojati i točku koju sam stavio i ona povećava length od stringa
    for (i = 1; i <= dotCount; i++) {
      // stavi točku
      lijeviDio.splice(dotPosition, 0, ".");
      // id na poziciju current + 4 
      dotPosition = dotPosition + 4;
    };
    
    
    // spoji nazad sve znakove u jedan string !!!
    lijeviDio = lijeviDio.join('')

    // kad se dogodi da je modulus === 0 onda postavim točku na nultu poziciju
    // i sad je moram obrisati !!!!!!!
    if (lijeviDio.charAt(0) === ".") {
      // uzimam sve osim prvog znaka tj točke
      lijeviDio = lijeviDio.substring(1);
    }
  };

  // ponovi nulu onoliko puta koliko treba da decimale imaju onoliko stringova 
  // koliko je definirano sa argumentom [zaokruzi_na] 
  // ( ... od kojeg gore dobijem varijablu brojZnamenki )
  if (desniDio.length < brojZnamenki) {
    
    desniDio = desniDio + '0'.repeat(brojZnamenki - desniDio.length);
    
    
    // stavi da minimalno bude 2 decimale pa makar bile i nule
    // ali ako treba zaokružiti na više decimala od 2 onda obriši sve nule koje su na kraju
    
    // npr ako je 0,00035 -----> onda ostavi tako kako je
    // npr ako je 0,35000 -----> onda pretvori u 0,35
  };
  

  

  // KADA SE RADI O JAKO VELIKOM BROJU DECIMALA (više od 3) npr ------> 0,0000035
  // KADA SE RADI O JAKO VELIKOM BROJU DECIMALA (više od 3) npr ------> 0,0000035
  // KADA SE RADI O JAKO VELIKOM BROJU DECIMALA (više od 3) npr ------> 0,0000035
  if ( brojZnamenki >= 3 && desniDio.length >= 3 && desniDio.charAt(desniDio.length - 1) == `0` ) {
    var cropped_desni_dio = desniDio;
    for ( let i = desniDio.length-1; i >= 2; i-- ) {
      if ( desniDio.charAt(i) == `0` ) {
        cropped_desni_dio = cropped_desni_dio.slice(0, -1); // onda briši sve nule na desnom kraju
      } else {
        break;
      }
    };
    
    desniDio = cropped_desni_dio;
  };
  
  if ( desniDio == "00" && brojZnamenki == 1 ) desniDio = '0';
  var formatiraniBroj = lijeviDio + "," + desniDio;
  
  if ( desniDio == "00" && brojZnamenki == 0 ) formatiraniBroj = lijeviDio;
  
  
  if ( broj_je_pozitivan == false ) formatiraniBroj = '-' + formatiraniBroj;
  
  return formatiraniBroj;
  
};

function cit_round(broj, brojZnamenki) {

 var broj = parseFloat(broj);

 if ( typeof brojZnamenki == 'undefined' ) {
   brojZnamenki = 0;
 };
 var multiplicator = Math.pow(10, brojZnamenki);
 broj = parseFloat((broj * multiplicator).toFixed(11));
 var test =(Math.round(broj) / multiplicator);
 // + znak ispred je samo js trik da automatski pretvori string u broj
 return +(test.toFixed(brojZnamenki));
};

function cit_rand() {
  var rand_id = String( Math.random()+Date.now() ).replace(`.`, ``);
  return rand_id;
};

function cit_rand_min_max(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};


/*
** Returns the caret (cursor) position of the specified text field (oField).
** Return value range is 0-oField.value.length.
*/

function caret_pos(oField) {

  // Initialize
  var iCaretPos = 0;

  // IE Support
  if (document.selection) {

    // Set focus on the element
    oField.focus();

    // To get cursor position, get empty selection range
    var oSel = document.selection.createRange();

    // Move selection start to 0 position
    oSel.moveStart('character', -oField.value.length);

    // The caret position is selection length
    iCaretPos = oSel.text.length;
  }

  // Firefox support
  else if (oField.selectionStart || oField.selectionStart == '0') {
    iCaretPos = oField.selectionDirection=='backward' ? oField.selectionStart : oField.selectionEnd;
  };

  // Return results
  return iCaretPos;
}


function create_string_from_date(date, add_days) {
  
  var date_units = get_date_units( date, add_days );
  
  var yyyy = date_units.yyyy;
  var mnt = date_units.mnt;
  var day = date_units.day + (add_days || 0); // plus or minus days    
  
  var current_hours   = date_units.hh;
  var current_minutes = date_units.mm;
  var current_seconds = date_units.sec;
  
  
  var string_yyyy = String(yyyy);
  var string_mnt = String(mnt+1);
  var string_day = String(day);
  
  if ( string_yyyy.length  < 2 ) string_yyyy  = '0' + string_yyyy;
  if ( string_mnt.length < 2 ) string_mnt = '0' + string_mnt;
  if ( string_day.length < 2 ) string_day = '0' + string_day;

  
  var string_hh = String(current_hours);
  var string_min = String(current_minutes);
  var string_sec = String(current_seconds);
  
  if ( string_hh.length  < 2 ) string_hh  = '0' + string_hh;
  if ( string_min.length < 2 ) string_min = '0' + string_min;
  if ( string_sec.length < 2 ) string_sec = '0' + string_sec;  
  
  
  return {
    string_yyyy: string_yyyy,
    string_mnt: string_mnt,
    string_day: string_day,
    string_hh: string_hh,
    string_min: string_min,
    string_sec: string_sec,
  }
  
};

function get_date_units( date, add_days ) {
  
  var yyyy = date.getFullYear();
  var mnt = date.getMonth();
  var day = date.getDate() + (add_days || 0); // plus or minus days    
  
  var current_hours   = date.getHours();
  var current_minutes = date.getMinutes();
  var current_seconds = date.getSeconds();
  
  return {
    yyyy: yyyy,
    mnt: mnt,
    day: day,
    hh: current_hours,
    mm: current_minutes,
    sec: current_seconds
  };
  
  
}

function cit_ms_no_dots(input_elem, add_days, format, arg_ms ) {
  
  // ovdje nikada ne može biti format time jer ako ima dvotočku nikada neće biti pokrenuta ova funkcija
  // ovdje nikada ne može biti format time jer ako ima dvotočku nikada neće biti pokrenuta ova funkcija
  // ovdje nikada ne može biti format time jer ako ima dvotočku nikada neće biti pokrenuta ova funkcija

  var date_time_arg = null;
  
  if ( typeof input_elem == "string" ) {
    date_time_arg = input_elem.trim();
  } else {
    date_time_arg = input_elem ? input_elem.val().trim() : null;  
  };
  
  var time_string = "";
  
  var day_at_zero = 0;
  
  var date = new Date();
  
  if ( arg_ms ) date = new Date(arg_ms);
  
  
  var str_dt = create_string_from_date(date, add_days);
  var date_units = get_date_units( date, add_days );
  
  // ako u sebi string ima razmak to znači da ima time
  if (date_time_arg.indexOf(' ') > -1 ) {
    date_string = date_time_arg.split(" ")[0];
    time_string = date_time_arg.split(" ")[1];
  } else {
    date_string = date_time_arg;
  };



  // ako je samo dan ili ako je prazan string
  if ( date_string.length <= 2 && date_string !== '') {
    date_units.day = Number(date_string.substring(0, 2) ) + (add_days || 0);
  }; 

  // ako je dan.mjesec
  if ( date_string.length > 2 ) {
    date_units.day = Number(date_string.substring(0, 2) ) + (add_days || 0);
    date_units.mnt = Number(date_string.substring(2, 4) ) - 1;
  }; 

  // ako je dan.mjesec.godina
  if ( date_string.length > 6 ) {
    date_units.day = Number(date_string.substring(0, 2) ) + (add_days || 0);
    date_units.mnt = Number(date_string.substring(2, 4) ) - 1;
    date_units.yyyy = Number(date_string.substring(4) );
  }; 


  var date_from_input = new Date(date_units.yyyy, date_units.mnt, date_units.day, 0, 0, 0, 0);

  var yyyy_from_str = date_from_input.getFullYear();
  var mnt_from_str = date_from_input.getMonth();
  var day_from_str = date_from_input.getDate(); // plus or minus days    


  str_dt.string_yyyy = String(yyyy_from_str);
  str_dt.string_mnt = String(mnt_from_str+1);
  str_dt.string_day = String(day_from_str);

  if ( str_dt.string_yyyy.length  < 2 ) str_dt.string_yyyy  = '0' + str_dt.string_yyyy;
  if ( str_dt.string_mnt.length < 2 ) str_dt.string_mnt = '0' + str_dt.string_mnt;
  if ( str_dt.string_day.length < 2 ) str_dt.string_day = '0' + str_dt.string_day;

 
  date_time_string = 
    str_dt.string_day + '.' + 
    str_dt.string_mnt + '.' + 
    str_dt.string_yyyy + ' ' + 
    str_dt.string_hh + ':' + 
    str_dt.string_min + ':' + 
    str_dt.string_sec;

  // izbaci sate minute i sec ako je format == date
  if ( format == `date` ) date_time_string = str_dt.string_day + '.' + str_dt.string_mnt + '.' + str_dt.string_yyyy;

  // ako input nije string nego je DOM element
  if ( input_elem && typeof input_elem !== "string" ) input_elem.val(date_time_string);
  
  day_at_zero = new Date(date_units.yyyy, date_units.mnt, date_units.day, 0, 0, 0, 0);
  
  var current_time = new Date(date_units.yyyy, date_units.mnt, date_units.day, date_units.hh, date_units.mm, date_units.sec, 0);
  var ms_current_time = Number(current_time);
 
  // nedjelja je 0 ---> subota je 6
  var week_day = day_at_zero.getDay();

  var ms_at_zero = Number(day_at_zero); //  day_at_zero.getMilliseconds();

  return {
    string: date_time_string,
    ms_at_zero: ms_at_zero,
    week_day: week_day,
    ms: ms_current_time,
    ms_from_zero: ms_current_time - ms_at_zero
  };  
  
  
  
};

function         cit_ms(input_elem, add_days, format, arg_ms ) {
  
  var result = null;
  
  var date_time_arg = null;

  
  if ( typeof input_elem == "string" ) {
    // ako sam kao arg dao string
    date_time_arg = input_elem.trim();
  } else {
    // ako sam dao html dom element od current inputa
    date_time_arg = input_elem ? input_elem.val().trim() : null;
  };
  
  
  if ( date_time_arg && date_time_arg.indexOf('.') == -1 && date_time_arg.indexOf(':') == -1 ) {
    // to znači da user upisuje datum bez točaka i nema dvotoče pa to znači da nije upisao sate i minute
    result = cit_ms_no_dots(input_elem, add_days, format, arg_ms);
    return result;
  };

  var day_at_zero = 0;
  
  var date = new Date();
  
  // ako sam dao četvrti argument tj milisekunde
  if ( arg_ms ) date = new Date(arg_ms);
 
  var str_dt = create_string_from_date(date, add_days);
  var date_units = get_date_units( date, add_days );

  var date_time_string = '';
  var date_string = '';
  var time_string = '';
  
  var date_je_ok = true;
  
  // ako nisam dao string arg onda kreiraj string od Date.now() !!!
  if ( !date_time_arg ) {
    

    // ovo je sve zajedno i datum i vrijeme
    date_time_string = 
      
      str_dt.string_day + '.' + 
      str_dt.string_mnt + '.' + 
      str_dt.string_yyyy + ' ' + 
      str_dt.string_hh + ':' + 
      str_dt.string_min + ':' + 
      str_dt.string_sec;
    // ovo je samo datum
    if ( format == `date` ) date_time_string = str_dt.string_day + '.' + str_dt.string_mnt + '.' + str_dt.string_yyyy;  
    // ovo je samo vrijeme
    if ( format == `time` ) date_time_string = str_dt.string_hh + ':' + str_dt.string_min + ':' + str_dt.string_sec;
    // ako sam ovoj funkciji dao inpute DOM element 
    // onda to znači da želim odmah upisati date time string u taj element
    if ( input_elem && typeof input_elem !== "string" ) input_elem.val(date_time_string);
  } 
  else {
  
    // ako sam dao argument onda parsiraj taj string i od njega kreiraj vrijeme

    // na primjer user je upisao 20.05...2021 14:30
    // ako je user greškom napisao više točaka
    date_time_string = date_time_arg.replace(/\.\.\./g, ".");
    date_time_string = date_time_arg.replace(/\.\./g, ".");
    date_time_string = date_time_arg.replace(/\.\./g, ".");

    
    date_time_string = date_time_arg.replace(/\:\:\:/g, ":");
    date_time_string = date_time_arg.replace(/\:\:/g, ":");
    date_time_string = date_time_arg.replace(/\:\:/g, ":");
    
    
    date_time_string = date_time_arg.replace(/\ \ \ /g, " ");
    date_time_string = date_time_arg.replace(/\ \ /g, " ");
    date_time_string = date_time_arg.replace(/\ \ /g, " ");
    
    
    
    if ( date_time_arg.indexOf(' ') > -1 && date_time_arg.indexOf(':') > -1 ) {
      // date & time ako ima razmak i ako ima dvotočku
      date_string = date_time_arg.split(" ")[0];
      time_string = date_time_arg.split(" ")[1];
      
    } else if ( date_time_arg.indexOf(' ') == -1 && date_time_arg.indexOf(':') > -1 && format == `time`) {
      // samo time
      // ako ima dvotočku i nema razmake i ako je format samo za time
      
      // ZA SADA NE ----> napravi ipak string datum ( koje je nastao po defaultu od ovog trenutka ako nema )
      // date_string = str_dt.string_day + '.' + str_dt.string_mnt + '.' + str_dt.string_yyyy; 
      date_string = null;
      time_string = date_time_arg;
      
    } else if ( date_time_arg.indexOf(':') == -1 && format == `date` )  {
      
      // ako nema dvotočku i ima format date
      
      // samo date         
      date_string = date_time_arg      
      time_string = null;
    } else {
      // oboje date i time
      date_string = date_time_string;
    };
    
    // ako ima dvotočku i ima format arg == time
    if ( format == `time` && date_time_arg.indexOf(':') > -1 ) {
      date_string = null;
      time_string = date_time_arg;
    };
  
  
    if ( format !== `time` ) {
      
      // NAPOMENA ----> ako je date_string == "" onda će split biti array s jednim praznim itemom [""] !!!!!
      var date_arr = date_string.split('.');
            
      // ako je samo dan ili ako je prazan string
      if ( date_arr.length == 1 && date_string !== '' ) {
        if ( date_arr[0] !== '' ) date_units.day = Number(date_arr[0]) + (add_days || 0);
      }; 

      // ako je dan.mjesec
      if ( date_arr.length >= 2 ) {
        if ( date_arr[0] !== '' ) date_units.day = Number(date_arr[0]) + (add_days || 0);
        if ( date_arr[1] !== '' ) date_units.mnt = Number(date_arr[1]) - 1;
      }; 

      // ako je dan.mjesec.godina
      if ( date_arr.length >= 3 ) {
        if ( date_arr[0] !== '' ) date_units.day = Number(date_arr[0]) + (add_days || 0);
        if ( date_arr[1] !== '' ) date_units.mnt = Number(date_arr[1]) - 1;
        if ( date_arr[2] !== '' ) date_units.yyyy = Number(date_arr[2]);
      }; 
      
      
      var date_from_input = new Date(date_units.yyyy, date_units.mnt, date_units.day, 0, 0, 0, 0);
      
      var yyyy_from_str = date_from_input.getFullYear();
      var mnt_from_str = date_from_input.getMonth();
      var day_from_str = date_from_input.getDate(); // plus or minus days    

      
      str_dt.string_yyyy = String(yyyy_from_str);
      str_dt.string_mnt = String(mnt_from_str+1);
      str_dt.string_day = String(day_from_str);
      
      if ( str_dt.string_yyyy.length  < 2 ) str_dt.string_yyyy  = '0' + str_dt.string_yyyy;
      if ( str_dt.string_mnt.length < 2 ) str_dt.string_mnt = '0' + str_dt.string_mnt;
      if ( str_dt.string_day.length < 2 ) str_dt.string_day = '0' + str_dt.string_day;

    
    };
      
    if ( time_string  ) {

      var time_arr = time_string.split(':');

      // ako je samo dan ili ako je prazan string
      if ( time_arr.length == 1 && date_string !== '' ) {
        if ( time_arr[0] !== '' ) date_units.hh = Number(time_arr[0]);
      }; 

      // ako je dan.mjesec
      if ( time_arr.length >= 2 ) {
        if ( time_arr[0] !== '' ) date_units.hh = Number(time_arr[0]);
        if ( time_arr[1] !== '' ) date_units.mm = Number(time_arr[1]);
      }; 

      // ako je dan.mjesec.godina
      if ( time_arr.length >= 3 ) {
        if ( time_arr[0] !== '' ) date_units.hh = Number(time_arr[0]);
        if ( time_arr[1] !== '' ) date_units.mm = Number(time_arr[1]);
        if ( time_arr[2] !== '' ) date_units.sec = Number(time_arr[2]);
      };     
      
    };
    
    str_dt.string_hh = String(date_units.hh);
    str_dt.string_min = String(date_units.mm);
    str_dt.string_sec = String(date_units.sec);

    if ( str_dt.string_hh.length  < 2 ) str_dt.string_hh  = '0' + str_dt.string_hh;
    if ( str_dt.string_min.length < 2 ) str_dt.string_min = '0' + str_dt.string_min;
    if ( str_dt.string_sec.length < 2 ) str_dt.string_sec = '0' + str_dt.string_sec;


    date_time_string = 
      str_dt.string_day + '.' + 
      str_dt.string_mnt + '.' + 
      str_dt.string_yyyy + ' ' + 
      str_dt.string_hh + ':' + 
      str_dt.string_min + ':' + 
      str_dt.string_sec;
    
    if ( format == `date` ) date_time_string = str_dt.string_day + '.' + str_dt.string_mnt + '.' + str_dt.string_yyyy;  
    if ( format == `time` ) date_time_string = str_dt.string_hh + ':' + str_dt.string_min + ':' + str_dt.string_sec;
    
    if ( input_elem && typeof input_elem !== "string" ) input_elem.val(date_time_string);




    // kraj ako postoji string kao arg
  };
    
  day_at_zero = new Date( date_units.yyyy, date_units.mnt, date_units.day, 0, 0, 0, 0);
  
  var current_time = new Date(
    date_units.yyyy,
    date_units.mnt,
    date_units.day,
    date_units.hh,
    date_units.mm,
    date_units.sec,
    0 );
  var ms_current_time = Number(current_time);
 
  // nedjelja je 0 ---> subota je 6
  var week_day = day_at_zero.getDay();

  var ms_at_zero = Number(day_at_zero); //  day_at_zero.getMilliseconds();

  return {
    string: date_time_string,
    ms_at_zero: ms_at_zero,
    week_day: week_day,
    ms: ms_current_time,
    ms_from_zero: ms_current_time - ms_at_zero
  };

};


// vraćam datum točno na početku dana u 00:00 !!!!!!
function cit_ms_at_zero(time) {
  
  var date = new Date(time);
  
  var date_units = get_date_units(date);
  var date_at_zero = new Date(date_units.yyyy, date_units.mnt, date_units.day, 0, 0, 0 );
  return Number(date);
  
  
};


function cit_time_str(arg_time, from_or_to) {
  
  if ( !arg_time && arg_time !== 0 ) return '';
  
  var time = arg_time;
  
  // pošto svako završno vrijeme  smanjim za 200 milisekundi 
  // sada kada vraćam vrijeme nazad u string moram to dodati !!!!!
  
  if (from_or_to == 'to') time = time + 200;
  
  var sat = parseInt( time/(60*60*1000) );
  var modulus_sat = time/(60*60*1000) - sat;
  
  var min = parseInt( modulus_sat*60 );
  var modulus_min = modulus_sat*60 - min;
  
  var sek_string = cit_format(modulus_min*60, 0);
  var sek = parseInt(sek_string) || 0;
  if ( sek == 60 ) sek = 59;
  
  var sat_num = sat;
  var min_num = min;
  var sek_num = sek;

  
  if ( String(sat).length < 2 ) sat = '0' + sat;
  if ( String(min).length < 2 ) min = '0' + min;
  if ( String(sek).length < 2 ) sek = '0' + sek;
   
  var time_string = sat + ':' + min + ':' + sek;
  
  
  return {
    string: time_string,
    sat: sat_num,
    min: min_num,
    sek: sek_num
  }
  
};


function sort_coverted_props(convert_rules, input_arr) {

  var convert_rules_length = convert_rules.length;

  var criteria = [];

  $.each(input_arr, function(ind, item) {
    $.each(item, function(key, value) {
      $.each(convert_rules, function(c_ind, rule) {

        if ( criteria.length < convert_rules_length ) {
          // ubaci string u kriterij za sortiranje
          var criteria_key = rule.new_key || rule.key;
          criteria.push( (rule.reverse == true ) ? '!'+criteria_key : criteria_key );
        };

        if ( rule.key == key ) item[ rule.new_key || rule.key ] = rule.convert(value);
      });
    });
  });


  multisort(input_arr, criteria);

  // sada nakon što je sortirano obriši sve new key propse koji mi više ne trebaju
  $.each(input_arr, function(ind, item) {
    $.each(convert_rules, function(c_ind, rule) {
      if ( item[rule.new_key] ) delete item[rule.new_key];
    });
  });

  return input_arr;


};

/*
-------------------------------- PRIMJER sort_coverted_props --------------------------------
var input_arr = [
  { br: "10", br_2: 10, tri: 1 },
  { br: "10", br_2: 20, tri: 2 },
  { br: "10", br_2: 20, tri: 3 },
  { br: "8" },
  { br: "100" },
];

var convert_rules = [
  { key: "br", new_key: `br_num`, convert: function(x) { return Number(x) }, reverse: false  },
  { key: "br_2", convert: function(x) { return x+100 }, reverse: true },
  { key: "tri", convert: function(x) { return x+100 }, reverse: false },
];

sort_coverted_props(convert_rules, input_arr);

-------------------------------- PRIMJER sort_coverted_props --------------------------------
*/


function cit_deep(data) {
  
  try {
    data = JSON.stringify(data);
    data = JSON.parse(data);
  }
  catch(err) {
    console.error(err);
  }
  
  return data;
};

function find_item(array, value, prop) {
  
  var obj = null;
  
  $.each( array, function(index, element) {
    
    if ( $.isPlainObject(element) ) {
      // ----------------------------------------------------------------
      // loopaj sve dok je obj null
      // ali kad nađe prvi element onda STANI !!!! 
      if ( obj == null && element[prop] == value ) {
        obj = element;
      };
      // ----------------------------------------------------------------
    } else if ( typeof element == "number" || typeof element == "string" ) {
      // stani ako je item već pronadjen
      if ( obj == null && element == value ) obj = element;
      // ----------------------------------------------------------------
    };
      
  });
  
  return obj;
  
};

function find_all_items(array, value, prop) {
  
  let result_arr = [];
  
  $.each( array, function(index, element) {
    
    // ----------------------------------------------------------------
    if ( $.isPlainObject(element) ) {
      
      if ( element[prop] == value ) {
        result_arr.push(element);
      };
      // ----------------------------------------------------------------
    } else if ( typeof element == "number" || typeof element == "string" ) {
      
      if ( element == value ) {
        result_arr.push(element);
      };
      // ----------------------------------------------------------------
    };
      
  });

  if ( result_arr.length == 0 ) result_arr = null;
  
  return result_arr;
};

function find_index(array, value, prop ) {
  
  var ind = null;
  
  $.each( array, function(index, element) {


    if ( $.isPlainObject(element) ) {
      // ----------------------------------------------------------------
      // loopaj sve dok je ind null
      // ali kad nađe ind onda STANI !!!! 
      if ( ind == null && element[prop] == value ) {
        ind = index;
      };
      // ----------------------------------------------------------------

    } else if ( typeof element == "number" || typeof element == "string" ) {    
      
      if ( ind == null && element == value ) ind = index;
      // ----------------------------------------------------------------
    };

  });

    
  return ind;
};

function upsert_item(array, new_item, prop ) {
  
  var array_copy = cit_deep(array);
  
  let found_index = null;
  
  $.each( array_copy, function(index, element) {
    
    
    if ( $.isPlainObject(element) ) {
      if ( element[prop] == new_item[prop] ) {
        found_index = index;
      };
    } else if ( typeof element == "number" || typeof element == "string" ) {
      if ( element == new_item ) {
        found_index = index;
      }; 
    };
    
  });

  if ( found_index !== null ) {
    array_copy[found_index] = new_item;
  } else {
    if ( !array_copy || $.isArray(array_copy) == false ) array_copy = [];
    array_copy.push(new_item);
  };
  
  return array_copy;
  
};



// može biti i multiple items !!!
function delete_item(array, value, prop ) {
  
  var array_copy = cit_deep(array);
  
  if ( $.isArray(array_copy) == false ) {
    console.error(array, "nije ARRAY !!!!!");
    return array_copy;
  };
  
  
  if ( $.isArray(array_copy) == true && array_copy.length == 0 ) {
    console.error(array, "ARRAY JE PRAZAN !!!!!");
    return array_copy;
  };
  
  
  
  var index;
  for( index=array_copy.length-1; index >= 0; index-- ) {
    
    var found_index = null;
    var element = array_copy[index];
    
    if ( $.isPlainObject(element) || $.isArray(element) ) {
    
      if ( element[prop] == value ) {
        found_index = index;
      };
      
    } 
    else if ( typeof element == "number" || typeof element == "string" ) {
      
      if ( element == value ) {
        found_index = index;
      };
      
    };
    
    
    if ( found_index !== null ) {
      array_copy.splice(found_index, 1);
    };
    
  }; // kraj for loopa UNAZAD

  
  return array_copy;
  
};

// ovo npr koristim za nacrt sifre tako da mi ne ponavlja nacrte sa istim tempalte-om NEGO SAMO UNIQUE
// ovo npr koristim za nacrt sifre tako da mi ne ponavlja nacrte sa istim tempalte-om NEGO SAMO UNIQUE
function insert_uniq( array, new_item, prop ) {
  
  var array_copy = cit_deep(array);
  
  let found_index = null;
  $.each( array_copy, function(index, element) {

    
    if ( $.isPlainObject(element) ) {
      if ( element[prop] == new_item[prop] ) {
        found_index = index;
      };
    } else if ( typeof element == "number" || typeof element == "string" ) {
      
      if ( element == new_item ) {
        found_index = index;
      };
      
    }

  });
  

  // samo ako nije našao objekt s istim propertijem
  if ( found_index == null ) {
    if ( !array_copy || $.isArray(array_copy) == false ) array_copy = [];
    array_copy.push(new_item);
  };
  
  return array_copy;
  
};


function only_uniq(value, index, self) {
  return self.indexOf(value) === index;
};


// usage example:
var a = ['a', 1, 'a', 2, '1'];
var uni = a.filter(only_uniq);
// console.log(uni); // ['a', 1, 2, '1']

function cit_scroll(elem, offset, interval) {
  
  $([document.documentElement, document.body]).animate({
      scrollTop: elem.offset().top + offset,
  }, interval);
  
};


function cit_is_same(val_1, val_2, check_key) {
  
  var same = false;
  // ako je valnull
  if ( val_1 == val_2 ) same = true;
  
  // ako su oba VAl-a falsey !!! -------> nije bitno ako su različita vrsta !!!! tretiram to kao da su isti !!!!
  if ( 
    (typeof val_1 == "undefined" || Number.isNaN( val_1 ) || val_1 == null ) 
    &&
    (typeof val_2 == "undefined" || Number.isNaN( val_2 ) || val_2 == null ) 
    
  ) same = true;
  
  // ako imam 2 objekta onda provjeravam isti key u ta dva objekta
  if ( check_key && $.isPlainObject(val_1) && $.isPlainObject(val_2) ) {
    
    if ( 
      val_1[check_key] && val_2[check_key] // provjeri jel taj key postoji u oba objekta
      &&
      val_1[check_key] === val_2[check_key] // provjer jesu li KEYS isti
    ) same = true;
    
  };
  
  
  return same;
  
};
  

function cit_is_mobile() {
  let check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};


function cit_is_mobile_or_tablet() {
  let check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};





function reset_tooltips() {

  setTimeout( function() {
    $('.tooltip.show').remove(); // na silu obriši sve tooltipove koji su trenutno prikazani !!!
    $('.cit_tooltip').tooltip('dispose').tooltip({ boundary: 'window' }); // ponovo inicijaliziraj sve tooltipove
  }, 500 );
  
};


function reverse_switch(state, elem) {
  
  setTimeout( function() {
    
    if ( state == true ) {
      // ako je true onda je sada na ON ----> vrati nazad na OFF
      $(elem).removeClass(`on`).addClass(`off`);  
    }
    else {
      // ako je false onda je sada na OFF ----> vrati nazad na ON
      $(elem).removeClass(`off`).addClass(`on`);  
    };
    
  }, 200 );
  
};



// https://stackoverflow.com/questions/1284314/easter-date-in-javascript
function Easter(Y) {
  var C = Math.floor(Y/100);
  var N = Y - 19*Math.floor(Y/19);
  var K = Math.floor((C - 17)/25);
  var I = C - Math.floor(C/4) - Math.floor((C - K)/3) + 19*N + 15;
  I = I - 30*Math.floor((I/30));
  I = I - Math.floor(I/28)*(1 - Math.floor(I/28)*Math.floor(29/(I + 1))*Math.floor((21 - N)/11));
  var J = Y + Math.floor(Y/4) + I + 2 - C + Math.floor(C/4);
  J = J - 7*Math.floor(J/7);
  var L = I - J;
  var M = 3 + Math.floor((L + 40)/44);
  var D = L + 28 - 31*Math.floor(M/4);

  // return padout(M) + '.' + padout(D);

  return {
    mm: M-1,
    dd: D,
  }
  
};

function padout(number) { return (number < 10) ? '0' + number : number; };


// Returns the ISO week of the date.
Date.prototype.getWeek = function() {
  var date = new Date(this.getTime());
  date.setHours(0, 0, 0, 0);
  // Thursday in current week decides the year.
  date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
  // January 4 is always in week 1.
  var week1 = new Date(date.getFullYear(), 0, 4);
  // Adjust to Thursday in week 1 and count number of weeks from date to week1.
  return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000 - 3 + (week1.getDay() + 6) % 7) / 7);
}

// Returns the four-digit year corresponding to the ISO week of the date.
Date.prototype.getWeekYear = function() {
  var date = new Date(this.getTime());
  date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
  return date.getFullYear();
}

// https://stackoverflow.com/questions/8572826/generic-deep-diff-between-two-objects
// deepDiffMapper je originalno ime funkcije
// koristit se ovako:
// var result = cit_deep_diff.map( object_1, object_2 );
var cit_deep_diff = function() {
  
  return {
    VALUE_CREATED: 'created',
    VALUE_UPDATED: 'updated',
    VALUE_DELETED: 'deleted',
    VALUE_UNCHANGED: 'unchanged',
    map: function(obj1, obj2) {
      if (this.isFunction(obj1) || this.isFunction(obj2)) {
        throw 'Invalid argument. Function given, object expected.';
      }
      if (this.isValue(obj1) || this.isValue(obj2)) {
        return {
          type: this.compareValues(obj1, obj2),
          data: obj1 === undefined ? obj2 : obj1
        };
      }

      var diff = {};
      for (var key in obj1) {
        if (this.isFunction(obj1[key])) {
          continue;
        }

        var value2 = undefined;
        if (obj2[key] !== undefined) {
          value2 = obj2[key];
        }

        diff[key] = this.map(obj1[key], value2);
      }
      for (var key in obj2) {
        if (this.isFunction(obj2[key]) || diff[key] !== undefined) {
          continue;
        }

        diff[key] = this.map(undefined, obj2[key]);
      }

      return diff;

    },
    compareValues: function (value1, value2) {
      if (value1 === value2) {
        return this.VALUE_UNCHANGED;
      }
      if (this.isDate(value1) && this.isDate(value2) && value1.getTime() === value2.getTime()) {
        return this.VALUE_UNCHANGED;
      }
      if (value1 === undefined) {
        return this.VALUE_CREATED;
      }
      if (value2 === undefined) {
        return this.VALUE_DELETED;
      }
      return this.VALUE_UPDATED;
    },
    isFunction: function (x) {
      return Object.prototype.toString.call(x) === '[object Function]';
    },
    isArray: function (x) {
      return Object.prototype.toString.call(x) === '[object Array]';
    },
    isDate: function (x) {
      return Object.prototype.toString.call(x) === '[object Date]';
    },
    isObject: function (x) {
      return Object.prototype.toString.call(x) === '[object Object]';
    },
    isValue: function (x) {
      return !this.isObject(x) && !this.isArray(x);
    }
  }
}();





// ------------------------------ ISPOD JE SVE TESTIRANJE ------------------------------
// ------------------------------ ISPOD JE SVE TESTIRANJE ------------------------------


var test_object_1 = [
  
    {a: 'hey'},
  
    1,
  
    [
      3, 
      {c: 1},
      2
    ]
  
  ];


var test_object_2 = [
  
    {a: 'hey'},
  
    1,
  
    [
      3, 
      {c: 1},
      2
    ]
  
  ];


// var kalk_diff = cit_deep_diff.map( test_object_1, test_object_2 );


// console.log( JSON.stringify(kalk_diff) );


var kalk_diff = 
    
    {
      "0": { "a": { "type":"unchanged","data":"hey"} },
      "1": { "type":"unchanged", "data":1 },
      "2": { 
        "0": { "type":"unchanged","data": 3 }, 
        "1": { "c": {"type":"unchanged","data":1} }, 
        "2": { "type":"unchanged","data":2 } 
      }
    
    };



window.cit_created_count = 0;
window.cit_updated_count = 0;
window.cit_deleted_count = 0;
window.cit_unchanged_count = 0;

function cit_traverse( tree, find_prop ) {
  
  
  if ( !$.isArray(tree) && !$.isPlainObject(tree) ) {
    popup_warn(`Data set nije niti array niti object !!!!`);
    return null;
  };
  
  
  $.each( tree, function( key, value )  {
    
    if ( key == find_prop &&  value == `created` )   window.cit_created_count   += 1;
    if ( key == find_prop &&  value == `updated` )   window.cit_updated_count   += 1;
    if ( key == find_prop &&  value == `deleted` )   window.cit_deleted_count   += 1;
    if ( key == find_prop &&  value == `unchanged` ) window.cit_unchanged_count += 1;
    
    if ( $.isArray(value) || $.isPlainObject(value) ) {
      cit_traverse( value, find_prop );
    };
    
  });

};








