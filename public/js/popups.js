
var w = window;

async function show_popup_ok(type, popup_text, ok_func) {

  var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
  var show_popup_modal = pop_mod.show_popup_modal;
  show_popup_modal(type, popup_text, null, 'ok', null, null, ok_func);

};


async function show_popup_yes_no(type, popup_text, yes_func, no_func ) {

    var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
    var show_popup_modal = pop_mod.show_popup_modal;
    show_popup_modal(type, popup_text, null, 'ok');

    $('#cit_popup_modal_OK').off('click');
    $('#cit_popup_modal_OK').on('click', function() {
      show_popup_modal(false, popup_text, null, 'yes/no', yes_func, no_func, null );
    });
    
};


function popup_nedostaje_user() {

/*  
var popup_text = 
`
${w.we_trans( 'popup_nedostaje_user', w.we_lang) }
`;
*/

  var popup_text = "ovo je proba";

  show_popup_ok('warning', popup_text);      
  
};

function popup_sva_polje_za_reg_pass_min_6() {

/*  
var popup_text = 
`
${w.we_trans( 'popup_nedostaje_user', w.we_lang) }
`;
*/

  var popup_text = "Potrebno je upisati više od 6 znakova<br> u označena polja !!!";

  show_popup_ok('warning', popup_text);      
  
};

function popup_msg(msg) {

  /*  
  var popup_text = 
  `
  ${w.we_trans( 'popup_nedostaje_user', w.we_lang) }
  `;
  */

  var popup_text = msg;

  show_popup_ok(true, popup_text);      
  
};

function popup_warn(msg, ok_func) {

/*  
var popup_text = 
`
${w.we_trans( 'popup_nedostaje_user', w.we_lang) }
`;
*/

  var popup_text = msg;

  show_popup_ok('warning', popup_text, ok_func);      
  
};

function popup_error(msg) {

/*  
var popup_text = 
`
${w.we_trans( 'popup_nedostaje_user', w.we_lang) }
`;
*/

  var popup_text = msg;

  show_popup_ok('error', popup_text);      
  
};





