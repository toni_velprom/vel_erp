"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// ovdje sve libs koji trebaju biti dostupni globalno !!!!!
var isEven = require('is-even'); // alert('teeeeeest');


load_lib("npm/is-even", isEven);
$(document).ready(function () {
  var cit_layout_module_data = {
    id: "cit_layout_module",
    text: null,
    name: 'Pero',
    last_name: 'Perić',
    street: 'Avenija Dubrovnik 23',
    place: 'Zagreb',
    post_num: 10000
  };
  wait_for("$('#close_left_menu').length > 0", /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    var left_menu_state, preview_mod;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            left_menu_state = window.localStorage.getItem('left_menu_state'); // ako je mali ekran ili ako je u local storage closed

            if ($(window).width() < 600 || left_menu_state == "closed") {
              // i ako left menu nema cit hide klasu tj ako je otvoren onda ga zatvori
              if ($('#close_left_menu').hasClass('cit_hide') == false) {
                $("#cit_left_menu_box").addClass('cit_hide');
                $("#cit_main_area_box").addClass('cit_wide');
                $("#kalendar_header").addClass('cit_wide');
                $("#close_left_menu").addClass('cit_hide'); //sakrij X

                $("#open_left_menu").removeClass('cit_hide'); // prikaži  ----> prikaži hamburger
              }

              ;
            } else {
              // ako prozor veći i ako nije upisano u local storage da je closed
              // i to samo ako je closed -----> onda ga otvori
              if ($('#close_left_menu').hasClass('cit_hide') == true) {
                window.localStorage.setItem('left_menu_state', "opened");
                $("#cit_left_menu_box").removeClass('cit_hide');
                $("#cit_main_area_box").removeClass('cit_wide');
                $("#kalendar_header").removeClass('cit_wide');
                $("#open_left_menu").addClass('cit_hide'); // sakrij hamburger

                $("#close_left_menu").removeClass('cit_hide'); // prikaži X
              }

              ;
            }

            ;
            _context.next = 5;
            return get_cit_module('/modules/previews/cit_previews.js', "load_css");

          case 5:
            preview_mod = _context.sent;
            preview_mod.create({
              id: 'cit_preview_modal'
            }, $('body'), 'prepend');

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  })), 50 * 1000); // odmah u startu kreiram HTML za popup da stoji unutar body

  var pop_mod = window['/preload_modules/site_popup_modal/site_popup_modal.js'];
  pop_mod.create({
    id: 'cit_popup_modal'
  }, $('body'), 'prepend');
  /*
  get_cit_module(`/preload_modules/cit_layout_module.js`, null)
  .then((cit_layout_module) => { 
    cit_layout_module.create( cit_layout_module_data, $(`#cont_main_box`) );
  });
  
  */

  get_cit_module("/preload_modules/top_menu/cit_top_menu.js", null).then(function (cit_top_menu) {
    var top_menu_data = {
      id: "cit_top_menu"
    };
    cit_top_menu.create(top_menu_data, $("#cit_top_menu_box"));
  });
  get_cit_module("/preload_modules/left_menu/cit_left_menu.js", null).then(function (cit_left_menu) {
    var left_menu_data = {
      id: "cit_left_menu"
    };
    cit_left_menu.create(left_menu_data, $("#cit_left_menu_box"));
  });
  get_cit_module("/preload_modules/right_menu/cit_right_menu.js", null).then(function (cit_right_menu) {
    var right_menu_data = {
      id: "cit_right_menu"
    };
    cit_right_menu.create(right_menu_data, $("#cit_right_menu_box"));
  });
}); // end of doc raedy

window.cit_page_active = null;

(function () {
  var hidden = "hidden"; // Standards:

  if (hidden in document) document.addEventListener("visibilitychange", onchange);else if ((hidden = "mozHidden") in document) document.addEventListener("mozvisibilitychange", onchange);else if ((hidden = "webkitHidden") in document) document.addEventListener("webkitvisibilitychange", onchange);else if ((hidden = "msHidden") in document) document.addEventListener("msvisibilitychange", onchange); // IE 9 and lower:
  else if ("onfocusin" in document) document.onfocusin = document.onfocusout = onchange; // All others:
    else window.onpageshow = window.onpagehide = window.onfocus = window.onblur = onchange;

  function onchange(evt) {
    var v = "visible";
    var h = "hidden";
    var evtMap = {
      focus: v,
      focusin: v,
      pageshow: v,
      blur: h,
      focusout: h,
      pagehide: h
    };
    evt = evt || window.event;

    if (evt.type in evtMap) {
      window.cit_page_active = evtMap[evt.type]; // document.body.className = evtMap[evt.type];
      // console.log(evtMap[evt.type]);

      console.log(window.cit_page_active);
      socket_check_in();
    } else {
      window.cit_page_active = window.cit_page_active == "hidden" ? "visible" : "hidden";
      console.log(window.cit_page_active);
      socket_check_in(); // console.log(evtMap[evt.type]);
      // document.body.className = this[hidden] ? "hidden" : "visible";
    }

    ;
  }

  ; // kraj func on change
  // set the initial state (but only if browser supports the Page Visibility API)

  if (document[hidden] !== undefined) onchange({
    type: document[hidden] ? "blur" : "focus"
  });
})();

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluaXRpYWxfc2NyaXB0cy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTtBQUNBLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxTQUFELENBQXBCLEMsQ0FFQTs7O0FBRUEsUUFBUSxnQkFBZ0IsTUFBaEIsQ0FBUjtBQUdBLENBQUMsQ0FBQyxRQUFELENBQUQsQ0FBWSxLQUFaLENBQWtCLFlBQVk7QUFHNUIsTUFBSSxzQkFBc0IsR0FBRztBQUMzQixJQUFBLEVBQUUscUJBRHlCO0FBRTNCLElBQUEsSUFBSSxFQUFFLElBRnFCO0FBRzNCLElBQUEsSUFBSSxFQUFFLE1BSHFCO0FBSTNCLElBQUEsU0FBUyxFQUFFLE9BSmdCO0FBSzNCLElBQUEsTUFBTSxFQUFFLHNCQUxtQjtBQU0zQixJQUFBLEtBQUssRUFBRSxRQU5vQjtBQU8zQixJQUFBLFFBQVEsRUFBRTtBQVBpQixHQUE3QjtBQVdBLEVBQUEsUUFBUSwwR0FBcUM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRXZDLFlBQUEsZUFGdUMsR0FFckIsTUFBTSxDQUFDLFlBQVAsQ0FBb0IsT0FBcEIsQ0FBNEIsaUJBQTVCLENBRnFCLEVBSTNDOztBQUNBLGdCQUFLLENBQUMsQ0FBQyxNQUFELENBQUQsQ0FBVSxLQUFWLEtBQW9CLEdBQXBCLElBQTJCLGVBQWUsSUFBSSxRQUFuRCxFQUE4RDtBQUU1RDtBQUNBLGtCQUFJLENBQUMsQ0FBQyxrQkFBRCxDQUFELENBQXNCLFFBQXRCLENBQStCLFVBQS9CLEtBQThDLEtBQWxELEVBQXlEO0FBRXZELGdCQUFBLENBQUMsc0JBQUQsQ0FBd0IsUUFBeEIsQ0FBaUMsVUFBakM7QUFDQSxnQkFBQSxDQUFDLHNCQUFELENBQXdCLFFBQXhCLENBQWlDLFVBQWpDO0FBQ0EsZ0JBQUEsQ0FBQyxvQkFBRCxDQUFzQixRQUF0QixDQUErQixVQUEvQjtBQUVBLGdCQUFBLENBQUMsb0JBQUQsQ0FBc0IsUUFBdEIsQ0FBK0IsVUFBL0IsRUFOdUQsQ0FNVjs7QUFDN0MsZ0JBQUEsQ0FBQyxtQkFBRCxDQUFxQixXQUFyQixDQUFpQyxVQUFqQyxFQVB1RCxDQU9UO0FBRS9DOztBQUFBO0FBRUYsYUFkRCxNQWVLO0FBRUg7QUFFQTtBQUNBLGtCQUFLLENBQUMsQ0FBQyxrQkFBRCxDQUFELENBQXNCLFFBQXRCLENBQStCLFVBQS9CLEtBQThDLElBQW5ELEVBQTBEO0FBRXhELGdCQUFBLE1BQU0sQ0FBQyxZQUFQLENBQW9CLE9BQXBCLENBQTRCLGlCQUE1QixFQUErQyxRQUEvQztBQUVBLGdCQUFBLENBQUMsc0JBQUQsQ0FBd0IsV0FBeEIsQ0FBb0MsVUFBcEM7QUFDQSxnQkFBQSxDQUFDLHNCQUFELENBQXdCLFdBQXhCLENBQW9DLFVBQXBDO0FBQ0EsZ0JBQUEsQ0FBQyxvQkFBRCxDQUFzQixXQUF0QixDQUFrQyxVQUFsQztBQUVBLGdCQUFBLENBQUMsbUJBQUQsQ0FBcUIsUUFBckIsQ0FBOEIsVUFBOUIsRUFSd0QsQ0FRYjs7QUFDM0MsZ0JBQUEsQ0FBQyxvQkFBRCxDQUFzQixXQUF0QixDQUFrQyxVQUFsQyxFQVR3RCxDQVNUO0FBRWhEOztBQUFBO0FBR0Y7O0FBQUE7QUF2QzBDO0FBQUEsbUJBd0NyQixjQUFjLENBQUMsbUNBQUQsYUF4Q087O0FBQUE7QUF3Q3pDLFlBQUEsV0F4Q3lDO0FBMEM3QyxZQUFBLFdBQVcsQ0FBQyxNQUFaLENBQW9CO0FBQUUsY0FBQSxFQUFFLEVBQUU7QUFBTixhQUFwQixFQUFpRCxDQUFDLENBQUMsTUFBRCxDQUFsRCxFQUE0RCxTQUE1RDs7QUExQzZDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQXJDLElBNkNMLEtBQUcsSUE3Q0UsQ0FBUixDQWQ0QixDQThENUI7O0FBQ0EsTUFBSSxPQUFPLEdBQUcsTUFBTSxDQUFDLHVEQUFELENBQXBCO0FBQ0EsRUFBQSxPQUFPLENBQUMsTUFBUixDQUFlO0FBQUUsSUFBQSxFQUFFLEVBQUU7QUFBTixHQUFmLEVBQTBDLENBQUMsQ0FBQyxNQUFELENBQTNDLEVBQXFELFNBQXJEO0FBS0E7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUUsRUFBQSxjQUFjLDhDQUE4QyxJQUE5QyxDQUFkLENBQ0MsSUFERCxDQUNNLFVBQUMsWUFBRCxFQUFrQjtBQUV0QixRQUFJLGFBQWEsR0FBRztBQUNsQixNQUFBLEVBQUU7QUFEZ0IsS0FBcEI7QUFJQSxJQUFBLFlBQVksQ0FBQyxNQUFiLENBQXFCLGFBQXJCLEVBQW9DLENBQUMscUJBQXJDO0FBQ0QsR0FSRDtBQWFBLEVBQUEsY0FBYyxnREFBZ0QsSUFBaEQsQ0FBZCxDQUNDLElBREQsQ0FDTSxVQUFDLGFBQUQsRUFBbUI7QUFFdkIsUUFBSSxjQUFjLEdBQUc7QUFDbkIsTUFBQSxFQUFFO0FBRGlCLEtBQXJCO0FBSUEsSUFBQSxhQUFhLENBQUMsTUFBZCxDQUFzQixjQUF0QixFQUFzQyxDQUFDLHNCQUF2QztBQUNELEdBUkQ7QUFZQSxFQUFBLGNBQWMsa0RBQWtELElBQWxELENBQWQsQ0FDQyxJQURELENBQ00sVUFBQyxjQUFELEVBQW9CO0FBRXhCLFFBQUksZUFBZSxHQUFHO0FBQ3BCLE1BQUEsRUFBRTtBQURrQixLQUF0QjtBQUlBLElBQUEsY0FBYyxDQUFDLE1BQWYsQ0FBdUIsZUFBdkIsRUFBd0MsQ0FBQyx1QkFBekM7QUFDRCxHQVJEO0FBYUQsQ0FuSEQsRSxDQW1ISTs7QUFHSixNQUFNLENBQUMsZUFBUCxHQUF5QixJQUF6Qjs7QUFFQSxDQUFDLFlBQVc7QUFFVixNQUFJLE1BQU0sR0FBRyxRQUFiLENBRlUsQ0FJVjs7QUFDQSxNQUFJLE1BQU0sSUFBSSxRQUFkLEVBQ0UsUUFBUSxDQUFDLGdCQUFULENBQTBCLGtCQUExQixFQUE4QyxRQUE5QyxFQURGLEtBRUssSUFBSSxDQUFDLE1BQU0sR0FBRyxXQUFWLEtBQTBCLFFBQTlCLEVBQ0gsUUFBUSxDQUFDLGdCQUFULENBQTBCLHFCQUExQixFQUFpRCxRQUFqRCxFQURHLEtBRUEsSUFBSSxDQUFDLE1BQU0sR0FBRyxjQUFWLEtBQTZCLFFBQWpDLEVBQ0gsUUFBUSxDQUFDLGdCQUFULENBQTBCLHdCQUExQixFQUFvRCxRQUFwRCxFQURHLEtBRUEsSUFBSSxDQUFDLE1BQU0sR0FBRyxVQUFWLEtBQXlCLFFBQTdCLEVBQ0gsUUFBUSxDQUFDLGdCQUFULENBQTBCLG9CQUExQixFQUFnRCxRQUFoRCxFQURHLENBRUw7QUFGSyxPQUdBLElBQUksZUFBZSxRQUFuQixFQUNILFFBQVEsQ0FBQyxTQUFULEdBQXFCLFFBQVEsQ0FBQyxVQUFULEdBQXNCLFFBQTNDLENBREcsQ0FFTDtBQUZLLFNBSUgsTUFBTSxDQUFDLFVBQVAsR0FBb0IsTUFBTSxDQUFDLFVBQVAsR0FBbUIsTUFBTSxDQUFDLE9BQVAsR0FBaUIsTUFBTSxDQUFDLE1BQVAsR0FBZ0IsUUFBeEU7O0FBSUYsV0FBUyxRQUFULENBQW1CLEdBQW5CLEVBQXdCO0FBRXRCLFFBQUksQ0FBQyxHQUFHLFNBQVI7QUFDQSxRQUFJLENBQUMsR0FBRyxRQUFSO0FBQ0EsUUFBSSxNQUFNLEdBQUc7QUFDVCxNQUFBLEtBQUssRUFBQyxDQURHO0FBQ0EsTUFBQSxPQUFPLEVBQUMsQ0FEUjtBQUNXLE1BQUEsUUFBUSxFQUFDLENBRHBCO0FBQ3VCLE1BQUEsSUFBSSxFQUFDLENBRDVCO0FBQytCLE1BQUEsUUFBUSxFQUFDLENBRHhDO0FBQzJDLE1BQUEsUUFBUSxFQUFDO0FBRHBELEtBQWI7QUFJQSxJQUFBLEdBQUcsR0FBRyxHQUFHLElBQUksTUFBTSxDQUFDLEtBQXBCOztBQUVBLFFBQUksR0FBRyxDQUFDLElBQUosSUFBWSxNQUFoQixFQUF3QjtBQUV0QixNQUFBLE1BQU0sQ0FBQyxlQUFQLEdBQXlCLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBTCxDQUEvQixDQUZzQixDQUd0QjtBQUNBOztBQUNBLE1BQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxNQUFNLENBQUMsZUFBbkI7QUFDQSxNQUFBLGVBQWU7QUFHaEIsS0FURCxNQVVLO0FBQ0gsTUFBQSxNQUFNLENBQUMsZUFBUCxHQUEyQixNQUFNLENBQUMsZUFBUCxJQUEwQixRQUE1QixHQUF5QyxTQUF6QyxHQUFxRCxRQUE5RTtBQUNBLE1BQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxNQUFNLENBQUMsZUFBbkI7QUFFQSxNQUFBLGVBQWUsR0FKWixDQU1IO0FBQ0E7QUFDRDs7QUFBQTtBQUdGOztBQUFBLEdBckRTLENBcURQO0FBRUg7O0FBQ0EsTUFBSSxRQUFRLENBQUMsTUFBRCxDQUFSLEtBQXFCLFNBQXpCLEVBQXNDLFFBQVEsQ0FBQztBQUFDLElBQUEsSUFBSSxFQUFFLFFBQVEsQ0FBQyxNQUFELENBQVIsR0FBbUIsTUFBbkIsR0FBNEI7QUFBbkMsR0FBRCxDQUFSO0FBQ3ZDLENBekREIiwiZmlsZSI6ImluaXRpYWxfc2NyaXB0c19iYWJlbC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIG92ZGplIHN2ZSBsaWJzIGtvamkgdHJlYmFqdSBiaXRpIGRvc3R1cG5pIGdsb2JhbG5vICEhISEhXG52YXIgaXNFdmVuID0gcmVxdWlyZSgnaXMtZXZlbicpO1xuXG4vLyBhbGVydCgndGVlZWVlZXN0Jyk7XG5cbmxvYWRfbGliKGBucG0vaXMtZXZlbmAsIGlzRXZlbik7XG5cblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xuICBcbiAgXG4gIHZhciBjaXRfbGF5b3V0X21vZHVsZV9kYXRhID0ge1xuICAgIGlkOiBgY2l0X2xheW91dF9tb2R1bGVgLFxuICAgIHRleHQ6IG51bGwsXG4gICAgbmFtZTogJ1Blcm8nLFxuICAgIGxhc3RfbmFtZTogJ1BlcmnEhycsXG4gICAgc3RyZWV0OiAnQXZlbmlqYSBEdWJyb3ZuaWsgMjMnLFxuICAgIHBsYWNlOiAnWmFncmViJyxcbiAgICBwb3N0X251bTogMTAwMDAsXG4gIH07XG5cblxuICB3YWl0X2ZvcihgJCgnI2Nsb3NlX2xlZnRfbWVudScpLmxlbmd0aCA+IDBgLCBhc3luYyBmdW5jdGlvbigpIHtcbiAgICBcbiAgICB2YXIgbGVmdF9tZW51X3N0YXRlID0gd2luZG93LmxvY2FsU3RvcmFnZS5nZXRJdGVtKCdsZWZ0X21lbnVfc3RhdGUnKTtcblxuICAgIC8vIGFrbyBqZSBtYWxpIGVrcmFuIGlsaSBha28gamUgdSBsb2NhbCBzdG9yYWdlIGNsb3NlZFxuICAgIGlmICggJCh3aW5kb3cpLndpZHRoKCkgPCA2MDAgfHwgbGVmdF9tZW51X3N0YXRlID09IFwiY2xvc2VkXCIgKSB7XG5cbiAgICAgIC8vIGkgYWtvIGxlZnQgbWVudSBuZW1hIGNpdCBoaWRlIGtsYXN1IHRqIGFrbyBqZSBvdHZvcmVuIG9uZGEgZ2EgemF0dm9yaVxuICAgICAgaWYgKCQoJyNjbG9zZV9sZWZ0X21lbnUnKS5oYXNDbGFzcygnY2l0X2hpZGUnKSA9PSBmYWxzZSkge1xuXG4gICAgICAgICQoYCNjaXRfbGVmdF9tZW51X2JveGApLmFkZENsYXNzKCdjaXRfaGlkZScpO1xuICAgICAgICAkKGAjY2l0X21haW5fYXJlYV9ib3hgKS5hZGRDbGFzcygnY2l0X3dpZGUnKTtcbiAgICAgICAgJChgI2thbGVuZGFyX2hlYWRlcmApLmFkZENsYXNzKCdjaXRfd2lkZScpO1xuXG4gICAgICAgICQoYCNjbG9zZV9sZWZ0X21lbnVgKS5hZGRDbGFzcygnY2l0X2hpZGUnKTsgIC8vc2FrcmlqIFhcbiAgICAgICAgJChgI29wZW5fbGVmdF9tZW51YCkucmVtb3ZlQ2xhc3MoJ2NpdF9oaWRlJyk7IC8vIHByaWthxb5pICAtLS0tPiBwcmlrYcW+aSBoYW1idXJnZXJcblxuICAgICAgfTtcblxuICAgIH0gXG4gICAgZWxzZSB7XG4gICAgICBcbiAgICAgIC8vIGFrbyBwcm96b3IgdmXEh2kgaSBha28gbmlqZSB1cGlzYW5vIHUgbG9jYWwgc3RvcmFnZSBkYSBqZSBjbG9zZWRcblxuICAgICAgLy8gaSB0byBzYW1vIGFrbyBqZSBjbG9zZWQgLS0tLS0+IG9uZGEgZ2Egb3R2b3JpXG4gICAgICBpZiAoICQoJyNjbG9zZV9sZWZ0X21lbnUnKS5oYXNDbGFzcygnY2l0X2hpZGUnKSA9PSB0cnVlICkge1xuXG4gICAgICAgIHdpbmRvdy5sb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnbGVmdF9tZW51X3N0YXRlJywgXCJvcGVuZWRcIik7XG5cbiAgICAgICAgJChgI2NpdF9sZWZ0X21lbnVfYm94YCkucmVtb3ZlQ2xhc3MoJ2NpdF9oaWRlJyk7XG4gICAgICAgICQoYCNjaXRfbWFpbl9hcmVhX2JveGApLnJlbW92ZUNsYXNzKCdjaXRfd2lkZScpO1xuICAgICAgICAkKGAja2FsZW5kYXJfaGVhZGVyYCkucmVtb3ZlQ2xhc3MoJ2NpdF93aWRlJyk7XG5cbiAgICAgICAgJChgI29wZW5fbGVmdF9tZW51YCkuYWRkQ2xhc3MoJ2NpdF9oaWRlJyk7IC8vIHNha3JpaiBoYW1idXJnZXJcbiAgICAgICAgJChgI2Nsb3NlX2xlZnRfbWVudWApLnJlbW92ZUNsYXNzKCdjaXRfaGlkZScpOyAvLyBwcmlrYcW+aSBYXG5cbiAgICAgIH07XG5cblxuICAgIH07XG4gIHZhciBwcmV2aWV3X21vZCA9IGF3YWl0IGdldF9jaXRfbW9kdWxlKCcvbW9kdWxlcy9wcmV2aWV3cy9jaXRfcHJldmlld3MuanMnLCBgbG9hZF9jc3NgKTtcbiAgICBcbiAgcHJldmlld19tb2QuY3JlYXRlKCB7IGlkOiAnY2l0X3ByZXZpZXdfbW9kYWwnIH0sICQoJ2JvZHknKSwgJ3ByZXBlbmQnKTsgIFxuICAgIFxuICAgIFxuICB9LCA1MCoxMDAwKTtcbiAgXG4gIFxuICAvLyBvZG1haCB1IHN0YXJ0dSBrcmVpcmFtIEhUTUwgemEgcG9wdXAgZGEgc3RvamkgdW51dGFyIGJvZHlcbiAgdmFyIHBvcF9tb2QgPSB3aW5kb3dbJy9wcmVsb2FkX21vZHVsZXMvc2l0ZV9wb3B1cF9tb2RhbC9zaXRlX3BvcHVwX21vZGFsLmpzJ107XG4gIHBvcF9tb2QuY3JlYXRlKHsgaWQ6ICdjaXRfcG9wdXBfbW9kYWwnIH0sICQoJ2JvZHknKSwgJ3ByZXBlbmQnKTtcbiAgICBcbiAgXG4gIFxuICBcbiAgLypcbiAgZ2V0X2NpdF9tb2R1bGUoYC9wcmVsb2FkX21vZHVsZXMvY2l0X2xheW91dF9tb2R1bGUuanNgLCBudWxsKVxuICAudGhlbigoY2l0X2xheW91dF9tb2R1bGUpID0+IHsgXG4gICAgY2l0X2xheW91dF9tb2R1bGUuY3JlYXRlKCBjaXRfbGF5b3V0X21vZHVsZV9kYXRhLCAkKGAjY29udF9tYWluX2JveGApICk7XG4gIH0pO1xuICBcbiAgKi9cbiAgICBcbiAgZ2V0X2NpdF9tb2R1bGUoYC9wcmVsb2FkX21vZHVsZXMvdG9wX21lbnUvY2l0X3RvcF9tZW51LmpzYCwgbnVsbClcbiAgLnRoZW4oKGNpdF90b3BfbWVudSkgPT4geyBcbiAgICBcbiAgICB2YXIgdG9wX21lbnVfZGF0YSA9IHtcbiAgICAgIGlkOiBgY2l0X3RvcF9tZW51YCxcbiAgICB9O1xuICAgIFxuICAgIGNpdF90b3BfbWVudS5jcmVhdGUoIHRvcF9tZW51X2RhdGEsICQoYCNjaXRfdG9wX21lbnVfYm94YCkgKTtcbiAgfSk7XG4gIFxuICBcbiAgXG4gIFxuICBnZXRfY2l0X21vZHVsZShgL3ByZWxvYWRfbW9kdWxlcy9sZWZ0X21lbnUvY2l0X2xlZnRfbWVudS5qc2AsIG51bGwpXG4gIC50aGVuKChjaXRfbGVmdF9tZW51KSA9PiB7IFxuICAgIFxuICAgIHZhciBsZWZ0X21lbnVfZGF0YSA9IHtcbiAgICAgIGlkOiBgY2l0X2xlZnRfbWVudWAsXG4gICAgfTtcbiAgICBcbiAgICBjaXRfbGVmdF9tZW51LmNyZWF0ZSggbGVmdF9tZW51X2RhdGEsICQoYCNjaXRfbGVmdF9tZW51X2JveGApICk7XG4gIH0pO1xuICBcbiAgXG4gIFxuICBnZXRfY2l0X21vZHVsZShgL3ByZWxvYWRfbW9kdWxlcy9yaWdodF9tZW51L2NpdF9yaWdodF9tZW51LmpzYCwgbnVsbClcbiAgLnRoZW4oKGNpdF9yaWdodF9tZW51KSA9PiB7IFxuICAgIFxuICAgIHZhciByaWdodF9tZW51X2RhdGEgPSB7XG4gICAgICBpZDogYGNpdF9yaWdodF9tZW51YCxcbiAgICB9O1xuICAgIFxuICAgIGNpdF9yaWdodF9tZW51LmNyZWF0ZSggcmlnaHRfbWVudV9kYXRhLCAkKGAjY2l0X3JpZ2h0X21lbnVfYm94YCkgKTtcbiAgfSk7XG4gIFxuICBcbiAgXG4gIFxufSk7IC8vIGVuZCBvZiBkb2MgcmFlZHlcblxuXG53aW5kb3cuY2l0X3BhZ2VfYWN0aXZlID0gbnVsbDtcblxuKGZ1bmN0aW9uKCkge1xuICBcbiAgdmFyIGhpZGRlbiA9IFwiaGlkZGVuXCI7XG5cbiAgLy8gU3RhbmRhcmRzOlxuICBpZiAoaGlkZGVuIGluIGRvY3VtZW50KVxuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJ2aXNpYmlsaXR5Y2hhbmdlXCIsIG9uY2hhbmdlKTtcbiAgZWxzZSBpZiAoKGhpZGRlbiA9IFwibW96SGlkZGVuXCIpIGluIGRvY3VtZW50KVxuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJtb3p2aXNpYmlsaXR5Y2hhbmdlXCIsIG9uY2hhbmdlKTtcbiAgZWxzZSBpZiAoKGhpZGRlbiA9IFwid2Via2l0SGlkZGVuXCIpIGluIGRvY3VtZW50KVxuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJ3ZWJraXR2aXNpYmlsaXR5Y2hhbmdlXCIsIG9uY2hhbmdlKTtcbiAgZWxzZSBpZiAoKGhpZGRlbiA9IFwibXNIaWRkZW5cIikgaW4gZG9jdW1lbnQpXG4gICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIm1zdmlzaWJpbGl0eWNoYW5nZVwiLCBvbmNoYW5nZSk7XG4gIC8vIElFIDkgYW5kIGxvd2VyOlxuICBlbHNlIGlmIChcIm9uZm9jdXNpblwiIGluIGRvY3VtZW50KVxuICAgIGRvY3VtZW50Lm9uZm9jdXNpbiA9IGRvY3VtZW50Lm9uZm9jdXNvdXQgPSBvbmNoYW5nZTtcbiAgLy8gQWxsIG90aGVyczpcbiAgZWxzZVxuICAgIHdpbmRvdy5vbnBhZ2VzaG93ID0gd2luZG93Lm9ucGFnZWhpZGU9IHdpbmRvdy5vbmZvY3VzID0gd2luZG93Lm9uYmx1ciA9IG9uY2hhbmdlO1xuXG4gIFxuICBcbiAgZnVuY3Rpb24gb25jaGFuZ2UgKGV2dCkge1xuICAgIFxuICAgIHZhciB2ID0gXCJ2aXNpYmxlXCI7XG4gICAgdmFyIGggPSBcImhpZGRlblwiO1xuICAgIHZhciBldnRNYXAgPSB7XG4gICAgICAgIGZvY3VzOnYsIGZvY3VzaW46diwgcGFnZXNob3c6diwgYmx1cjpoLCBmb2N1c291dDpoLCBwYWdlaGlkZTpoXG4gICAgICB9O1xuXG4gICAgZXZ0ID0gZXZ0IHx8IHdpbmRvdy5ldmVudDtcbiAgICBcbiAgICBpZiAoZXZ0LnR5cGUgaW4gZXZ0TWFwKSB7XG4gICAgICBcbiAgICAgIHdpbmRvdy5jaXRfcGFnZV9hY3RpdmUgPSBldnRNYXBbZXZ0LnR5cGVdO1xuICAgICAgLy8gZG9jdW1lbnQuYm9keS5jbGFzc05hbWUgPSBldnRNYXBbZXZ0LnR5cGVdO1xuICAgICAgLy8gY29uc29sZS5sb2coZXZ0TWFwW2V2dC50eXBlXSk7XG4gICAgICBjb25zb2xlLmxvZyh3aW5kb3cuY2l0X3BhZ2VfYWN0aXZlKTtcbiAgICAgIHNvY2tldF9jaGVja19pbigpO1xuICAgICAgXG4gICAgICBcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICB3aW5kb3cuY2l0X3BhZ2VfYWN0aXZlID0gKCB3aW5kb3cuY2l0X3BhZ2VfYWN0aXZlID09IFwiaGlkZGVuXCIgKSA/IFwidmlzaWJsZVwiIDogXCJoaWRkZW5cIjtcbiAgICAgIGNvbnNvbGUubG9nKHdpbmRvdy5jaXRfcGFnZV9hY3RpdmUpO1xuICAgICAgXG4gICAgICBzb2NrZXRfY2hlY2tfaW4oKTtcbiAgICAgIFxuICAgICAgLy8gY29uc29sZS5sb2coZXZ0TWFwW2V2dC50eXBlXSk7XG4gICAgICAvLyBkb2N1bWVudC5ib2R5LmNsYXNzTmFtZSA9IHRoaXNbaGlkZGVuXSA/IFwiaGlkZGVuXCIgOiBcInZpc2libGVcIjtcbiAgICB9O1xuICAgIFxuICAgIFxuICB9OyAvLyBrcmFqIGZ1bmMgb24gY2hhbmdlXG5cbiAgLy8gc2V0IHRoZSBpbml0aWFsIHN0YXRlIChidXQgb25seSBpZiBicm93c2VyIHN1cHBvcnRzIHRoZSBQYWdlIFZpc2liaWxpdHkgQVBJKVxuICBpZiggZG9jdW1lbnRbaGlkZGVuXSAhPT0gdW5kZWZpbmVkICkgIG9uY2hhbmdlKHt0eXBlOiBkb2N1bWVudFtoaWRkZW5dID8gXCJibHVyXCIgOiBcImZvY3VzXCJ9KTtcbn0pKCk7XG5cbiJdfQ==