// AIzaSyATGjQYIZd_NcVAwWFFJaUhqfsjaBrXmfs
window.google_maps_loaded = false;

function loadMapsApi() {

  // if ( navigator.connection.type === Connection.NONE ) {
  if ( !window.navigator.onLine ) {
    
    // ako nije online ne radi ništa
  } else {

    if ( !window.google && !window.google_maps_loaded ) {

      clearTimeout( window.gmap_timeout_id );
      window.gmap_timeout_id = setTimeout( function() {
        $.getScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyATGjQYIZd_NcVAwWFFJaUhqfsjaBrXmfs&language=en&region=HR&libraries=places&callback=Function.prototype');
      }, 1500);

    };

  };
};

loadMapsApi();

function isIconMouseEvent(e) {
  return "placeId" in e;
};

function write_place_address(place, rand_id) {
  
  var lat = place.geometry.location.lat();
  var lng = place.geometry.location.lng();
  
  $(`#`+rand_id+`_adresa_coords`).val(lat+","+lng);
  
  // stavi podatke od googla u temp div  
  var tempDiv = $('<div id="tempDiv">' + place.adr_address + '</div>');

  $(`#`+rand_id+`_naziv_adrese`).val( place.name || "" );

  var ulica_i_br = tempDiv.find(".street-address").text().trim();

  if ( tempDiv.find(".street-address").length == 0 )  {
    ulica_i_br = place.vicinity.split(',')[0] || "";
  };

  $(`#`+rand_id+`_adresa`).val( ulica_i_br || "" );


  $(`#`+rand_id+`_posta`).val( tempDiv.find(".postal-code").text().trim() || "" );
  $(`#`+rand_id+`_mjesto`).val( tempDiv.find(".locality").text().trim() || "" );
  var drzava = tempDiv.find(".country-name").text().trim();

  if ( drzava == "Hrvatska" ) drzava = "Croatia";
  $(`#`+rand_id+`_drzava`).val( drzava || "" );

  if ( !window.current_adresa ) window.current_adresa = {};
  // var parcial_country = drzava.slice(0, -);
  $.each( cit_local_list.countries, function(index, country) {
    if ( country.naziv.indexOf(drzava) > -1 ) {
      window.current_adresa.drzava = cit_deep(country);
    };
  });
  
  
};


// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.
// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

function init_google_map(rand_id) {
  
  const myLatlng = { lat: 45.79553452381733, lng: 15.97829271951457 };
  
  const map = new google.maps.Map(document.getElementById("partner_adresa_google_map"), {
    center: myLatlng,
    zoom: 13,
    mapTypeId: "roadmap",
  });
  
  // Create the initial InfoWindow.
  let infoWindow = new google.maps.InfoWindow({
    content: "KLIKNITE BILO GDJE NA KARTU ILI NA BILO KOJI PIN DA ZAPIŠTETE KOORDINATE!",
    position: myLatlng,
  });
  infoWindow.open(map);
  // Configure the click listener.
  
    
  
  /*
  map.addListener("dblclick", (mapsMouseEvent) => {
    // Close the current InfoWindow.
    infoWindow.close();
    // Create a new InfoWindow.
    
    infoWindow = new google.maps.InfoWindow({
      position: mapsMouseEvent.latLng,
    });
    infoWindow.setContent(
      JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2)
    );
    infoWindow.open(map);
    
  });  
  */
  
  // Create the search box and link it to the UI element.
  const input = document.getElementById("pac-input");
  
  const searchBox = new google.maps.places.SearchBox(input);
  
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  
  // Bias the SearchBox results towards current map's viewport.
  map.addListener("bounds_changed", () => {
    searchBox.setBounds(map.getBounds());
  });
  let markers = [];
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener("places_changed", () => {
    
    const places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    };
    // Clear out the old markers.
    markers.forEach((marker) => {
      marker.setMap(null);
    });
    
    markers = [];
    // For each place, get the icon, name and location.
    const bounds = new google.maps.LatLngBounds();


    write_place_address(places[0], rand_id);
    
    function toggleBounce(marker) {
      
      if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
      } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
      }
    };

    
    places.forEach((place) => {
      
      if (!place.geometry || !place.geometry.location) {
        console.log("Returned place contains no geometry");
        return;
      };
      
      const icon = {
        url: place.icon,
        size: new google.maps.Size(25, 25),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25),
      };
      // Create a marker for each place.

      var marker = new google.maps.Marker({
          map,
          /*icon,*/
          title: place.name,
          position: place.geometry.location,
        });
      

      
      marker.addListener("click", (event) => {

        toggleBounce(marker);
        
        var lat = marker.getPosition().lat();
        var lng = marker.getPosition().lng();
        console.log( lat, lng );
        
        $(`#`+rand_id+`_adresa_coords`).val(lat+","+lng);
        

        // If the event has a placeId, use it.
        if (isIconMouseEvent(event)) {
          
          console.log("You clicked on place:" + event.placeId);
          // Calling e.stop() on the event prevents the default info window from
          // showing.
          // If you call stop here when there is no placeId you will prevent some
          // other map click event handlers from receiving the event.
          event.stop();

          if (event.placeId) {
            get_place_info(map, event.placeId, rand_id);
          };
          
        }; 
        
      });
      
      marker.setAnimation(google.maps.Animation.BOUNCE);
      
      markers.push(marker);

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    
    map.fitBounds(bounds);
    
  });
  
  map.setOptions({draggableCursor:'crosshair'}); 
  
  
  new ClickEventHandler(map, origin, rand_id);
  
  
};


class ClickEventHandler {
  origin;
  map;
  rand_id;
  /*
  directionsService;
  directionsRenderer;
  */
  placesService;
  infowindow;
  infowindowContent;
  constructor(map, origin, rand_id) {
    this.origin = origin;
    this.map = map;
    this.rand_id = rand_id;
    
    /*
    this.directionsService = new google.maps.DirectionsService();
    this.directionsRenderer = new google.maps.DirectionsRenderer();
    this.directionsRenderer.setMap(map);
    */
    
    this.placesService = new google.maps.places.PlacesService(map);
    this.infowindow = new google.maps.InfoWindow();
    this.infowindowContent = document.getElementById("infowindow-content");
    this.infowindow.setContent(this.infowindowContent);
    // Listen for clicks on the map.
    this.map.addListener("click", this.handleClick.bind(this));
  }
  
  handleClick(event) {
    console.log("You clicked on: " + event.latLng);
    
    var lat = event.latLng.lat();
    var lng = event.latLng.lng();
    console.log( lat, lng );

    $(`#`+this.rand_id+`_adresa_coords`).val(lat+","+lng);
  
    // If the event has a placeId, use it.
    if (isIconMouseEvent(event)) {
      console.log("You clicked on place:" + event.placeId);
      // Calling e.stop() on the event prevents the default info window from
      // showing.
      // If you call stop here when there is no placeId you will prevent some
      // other map click event handlers from receiving the event.
      event.stop();

      if (event.placeId) {
        // this.calculateAndDisplayRoute(event.placeId);
        this.getPlaceInformation(event.placeId);
      }
    }
  };
  
  
  calculateAndDisplayRoute(placeId) {
    const me = this;
    this.directionsService
      .route({
        origin: this.origin,
        destination: { placeId: placeId },
        travelMode: google.maps.TravelMode.WALKING,
      })
      .then((response) => {
        me.directionsRenderer.setDirections(response);
      })
      .catch((e) => window.alert("Directions request failed due to " + status));
  };
  
  
  getPlaceInformation(placeId) {
    const me = this;
    this.placesService.getDetails({ placeId: placeId }, (place, status) => {
      if (
        status === "OK" &&
        place &&
        place.geometry &&
        place.geometry.location
      ) {
        
        write_place_address(place, me.rand_id);
        
        me.infowindow.close();
        
        me.infowindow.setPosition(place.geometry.location);
        me.infowindowContent.children["place-icon"].src = place.icon;
        me.infowindowContent.children["place-name"].textContent = place.name;
        
        
        // komentirao sam ovaj html dio u prozoru jer mi ne treba !!!
        /* me.infowindowContent.children["place-id"].textContent = place.place_id; */
        
        me.infowindowContent.children["place-address"].textContent = place.formatted_address;
        
        me.infowindow.open(me.map);
      }
    });
  };
  
};


function get_place_info(map, placeId, rand_id) {

  var placesService = new google.maps.places.PlacesService(map);

  placesService.getDetails({
    placeId: placeId
  }, (place, status) => {
    if (
      status === "OK" &&
      place &&
      place.geometry &&
      place.geometry.location
    ) {

      write_place_address(place, rand_id);

    }
  });
  
}
