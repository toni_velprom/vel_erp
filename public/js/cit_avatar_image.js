var cit_cropper = null;
var cit_avatar_input = null;


function cit_show_avatar(input) {
  
  cit_avatar_input = input;
  
  if (input.files && input.files[0]) {
    
    
    
    var reader = new FileReader();

    
    var avatar_modal =
`
<div id="under_avatar">
  <div id="close_avatar_modal"><i class="fal fa-times"></i></div>
  <div id="avatar_modal_box">
    <img id="cit_avatar_image" src="" />
  </div>
  <button id="ok_avatar_btn" class="blue_btn">PRIHVATI</button>  
</div>
`;
  
    if ( $('#under_avatar').length == 0 ) {
      
      cit_cropper = null;
      
      $(`body`).prepend(avatar_modal);
      
      setTimeout(function() {
        $('#under_avatar').addClass('cit_show');
        
        $(`#close_avatar_modal`).off('click');
        $(`#close_avatar_modal`).on('click', function() {
          $(`#under_avatar`).remove();
          cit_avatar_input.value = '';
          
        });
        
        $(`#ok_avatar_btn`).off('click');
        $(`#ok_avatar_btn`).on('click', function() {
          avatar_is_ok();
        });
        
        var cit_avatar_image = $('#cit_avatar_image')
        
        cit_avatar_image.off('load');
        cit_avatar_image.on('load', function() {
          avatar_cropper_setup();
        });

        
        // važno ---- svaki put moram obrisati src i ponovo ga ubaciti
        // jer inače load event neće biti trigeriran
        var image_src = cit_avatar_image[0].src;
        cit_avatar_image[0].src = "";
        
        reader.onload = function (e) {
          $('#cit_avatar_image').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);

      }, 50);
      
    }; // kraj ako ne već ne postoji modal za avatar
    
  }; // kraj ako je došlo do promjene file-a
  
};

function avatar_cropper_setup() {

  var $image = $('#cit_avatar_image');

  $image.cropper({
    aspectRatio: 1 / 1,
    crop: function(event) {
      console.log(event.detail.x);
      console.log(event.detail.y);
      console.log(event.detail.width);
      console.log(event.detail.height);
      console.log(event.detail.rotate);
      console.log(event.detail.scaleX);
      console.log(event.detail.scaleY);
    }
  });

  // Get the Cropper.js instance after initialized
  cit_cropper = $image.data('cropper');

  
};

function avatar_is_ok() {
  
  var cropped_img = cit_cropper.getCroppedCanvas({
    width: 256,
    height: 256,
    minWidth: 256,
    minHeight: 256,
    maxWidth: 4096,
    maxHeight: 4096,
    fillColor: '#fff',
    imageSmoothingEnabled: false,
    imageSmoothingQuality: 'high',
  });

  var dataURL = cropped_img.toDataURL();  

  var img_html =`<img src="" />`;

  // ubaci sliku u okrugli label od file upload inputa !!!!
  $('#cit_avatar_up_label').html(img_html);
  $('#cit_avatar_up_label img')[0].src = dataURL;  

  $(`#under_avatar`).remove();
  cit_avatar_input.value = '';


  return;  

};

function upload_avatar(is_update) {

  
  return new Promise( function(resolve, reject) {

    
    if ( !window.cit_user || !window.cit_user.user_number ) {
      resolve({success: false, msg: `Nedostaje korisnik za upload avatara!!!` });
      return;
    };

    
    if (cit_cropper == null ) {
      resolve({success: false, msg: `NIJE ODABRANA SLIKA` });
      return;
    };

    // Upload cropped image to server if the browser supports `HTMLCanvasElement.toBlob`.
    // The default value for the second parameter of `toBlob` is 'image/png', change it if necessary.
    cit_cropper.getCroppedCanvas().toBlob( (blob) => {
      
      const formData = new FormData();

      if ( is_update ) formData.append('user_number', window.cit_user.user_number );

      // Pass the image file name as the third parameter if necessary.
      formData.append('avatar_image', blob/*, 'example.png' */);

      
      var avatar_url = '/upload_avatar';

      // Use `jQuery.ajax` method for example
      $.ajax('/upload_avatar', {
        method: 'POST',
        data: formData,
        processData: false,
        contentType: false,
      })
      .done(function (avatar) {
        
        console.log(avatar);
        
        if ( avatar.success == true ) {
          resolve({ success: true, filename: avatar.filename });
          console.log(`Spremljen avatar`);
        } else {
          resolve({success: false, filename: null });
          console.log(`Nije spremljen avatar !!!!`);
        };
        
      })
      .fail(function(error) {
        console.log(error);
        resolve({success: false, filename: null, msg: `Greška na serveru kod spremanja avatara !!!` });
        console.log(`GREŠKA U AJAXU za avatar`);
      });
      
      
      
      
      
      
    });
    // END cit_cropper.getCroppedCanvas to blob


  });

};





