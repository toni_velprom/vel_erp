
let hash_data = {};
window.curr_pathname = ``;

function parse_router( pattern ) {
  
  let curr_url_array = window.curr_pathname.split('/');
  let pattern_array = pattern.split('/');
  
  if ( curr_url_array.length !== pattern_array.length ) {
    // console.log('pathname NOT MATCHED!!!!!!')
    return false;
  };
  
  
  hash_data = {};
  
  $.each(pattern_array, function( index, pattern_string ) {
    if ( pattern_string.indexOf(':') == -1 ) hash_data[pattern_string] = index;
  });
  
  var pathname_fits_pattern = true;
  
  $.each(hash_data, function( pathname_key, index ) {
    
    if ( pathname_fits_pattern && curr_url_array[index] == pathname_key ) {
      
      var pathname_value = null;
    
        // ako postoji neka informacija iza slash-a u trenutnom url-u
        if ( curr_url_array[index+1] ) {
          
          var next_url_string = curr_url_array[index+1];
          
          
          if ( $.isNumeric(next_url_string) ) {
            
            pathname_value = Number(next_url_string);
            
          } else {
            
            try {
              // PROBAJ VALUE PRETVORITI U OBJEKT 
              // ovo uključuje ako je value == "null"
              // json parse će ga pretvoriti u pravi null
              pathname_value = JSON.parse(next_url_string);
            } catch(error) {
              // console.log(error);
            };
            
          };


          // ako je bilo koja falsy vrijednost pretvori u NULL
          if ( !pathname_value ) {
            if ( typeof next_url_string == "string" ) pathname_value = next_url_string;
          };
          // if ( pathname_value == null ) pathname_value = next_url_string;
          // ako je samo string "null" pretvori ga u pravi null
          if ( pathname_value == "null" ) pathname_value = null;

          // ako je samo string "undefined" pretvori ga u pravi null
          if ( pathname_value == "undefined" ) pathname_value = null;

        };

        hash_data[pathname_key] = pathname_value;
      
    } else {
      
      pathname_fits_pattern = false;
      // console.log('pathname NOT MATCHED!!!!!!')
      
    };
      
  });

  return pathname_fits_pattern;
  
};

async function router_listener() {
  
  
  
  $('#card_input_select').addClass(`cit_hide`);
  
  // reset pathname data
  hash_data = null;
  
  // var curr_pathname = window.location.pathname.replace('/', '');
  
  var curr_pathname = window.location.hash.replace('#', '');
  window.curr_pathname = decodeURI(curr_pathname);
  
  
  
    if ( parse_router(`sklad/:sklad_obj`) ) {
    
    console.log( hash_data );
    // iako imam restrikciju na serveru lakše mi je staviti warning na frontendu !!!
    if ( !window.cit_user ) {
      popup_warn(`Za pregled ovog modula morate biti ulogirani!`);
      return;
    };
     
    toggle_global_progress_bar(true);
      
    $(`#search_main_box`).html(``);  
    
    var sklad_module = await get_cit_module(`/modules/sklad/sklad_module.js`, 'load_css');
    
    if ( !sklad_module ) {
      toggle_global_progress_bar(false);
      return;
    };
    
    var sklad_data = {
      id: `sklad_module`,
    };
    
    sklad_module.create( sklad_data, $(`#cont_main_box`) );
    
    return;
    
  };
  
  
  if ( parse_router(`kalendar/:kalendar_obj`) ) {
    console.log( hash_data );
    
    // iako imam restrikciju na serveru lakše mi je staviti warning na frontendu !!!
    
    if ( !window.cit_user ) {
      popup_warn(`Za pregled ovog modula morate biti ulogirani!`);
      return;
    };
     
    toggle_global_progress_bar(true);
    
    var kalendar_module = await get_cit_module(`/modules/kalendar/kalendar.js`, 'load_css');
    
    if ( !kalendar_module ) {
      toggle_global_progress_bar(false);
      return;
    };
    
    var today = new Date();

    var dd = today.getDate();
    var mm = today.getMonth();
    var yyyy = today.getFullYear();
    
    var start_date = new Date(yyyy, mm, dd-10);
    
    start_date = Number(start_date);
    
    var kalendar_data = {
      id: `kalendar_module`,
      show_hours: false,
      start: null
    };
    kalendar_module.create( kalendar_data, $(`#cont_main_box`) );
    
    
    return;
  };
  
  
  if ( parse_router(`sirov/:sirov_obj/status/:status_id`) ) {
    
    console.log( hash_data );
    
    
    // iako imam restrikciju na serveru lakše mi je staviti warning na frontendu !!!
    
    if ( !window.cit_user ) {
      popup_warn(`Za pregled ovog modula morate biti ulogirani!`);
      return;
    };
     
    toggle_global_progress_bar(true);
    
    var sirov_module = await get_cit_module(`/modules/sirov/sirov_module.js`, 'load_css');
    
    var find_sirov = await get_cit_module(`/modules/sirov/find_sirov.js`, null);
    
    // ----------------------------------------------------------------------
    if ( !find_sirov ) {
      toggle_global_progress_bar(false);
      return;
    };
    var find_sirov_data = {
      id: `find_sirov`,
      for_module: `sirov`,
    };
    find_sirov.create( find_sirov_data, $(`#search_main_box`) );    
    // ----------------------------------------------------------------------
    
    
    // ----------------------------------------------------------------------
    if ( !sirov_module ) {
      toggle_global_progress_bar(false);
      return;
    };
    
    
    var db_result = {};
    
    if ( hash_data.sirov ) {
      
      var props = {
        filter: find_sirov.find_sirov_filter,
        url: '/find_sirov',
        query: { _id: hash_data.sirov },
        goto_status: ( hash_data.status || null),
        desc: `pretraga u routeru po _id unutar URL-a za sirovinu`,
      };
      try {
        db_result = await search_database(null, props );
      } catch (err) {
        console.log(err);
      };
      
    };
    
    // ako je našao nešto onda uzmi samo prvi član u arraju
    if ( db_result && $.isArray(db_result) && db_result.length > 0 ) {
      db_result = db_result[0];
    };
    
    var sirov_data = {
      ...db_result,
      id: `sirov_module`,
      goto_status: (hash_data.status || null),
    };
    
    sirov_module.create( sirov_data, $(`#cont_main_box`) );
    // ----------------------------------------------------------------------

    return;
  };
  
  
  if ( parse_router(`sirov/:sirov_obj/sklad/:sklad_sifra/sector/:sector_sifra/level/:level_sifra`) ) {
    
    console.log( hash_data );
    
    
    // iako imam restrikciju na serveru lakše mi je staviti warning na frontendu !!!
    
    if ( !window.cit_user ) {
      popup_warn(`Za pregled ovog modula morate biti ulogirani!`);
      return;
    };
     
    toggle_global_progress_bar(true);
    
    var sirov_module = await get_cit_module(`/modules/sirov/sirov_module.js`, 'load_css');
    
    var find_sirov = await get_cit_module(`/modules/sirov/find_sirov.js`, null);
    
    // ----------------------------------------------------------------------
    if ( !find_sirov ) {
      toggle_global_progress_bar(false);
      return;
    };
    var find_sirov_data = {
      id: `find_sirov`,
      for_module: `sirov`,
    };
    find_sirov.create( find_sirov_data, $(`#search_main_box`) );    
    // ----------------------------------------------------------------------
    
    
    // ----------------------------------------------------------------------
    if ( !sirov_module ) {
      toggle_global_progress_bar(false);
      return;
    };
    
    
    var db_result = {};
    
    if ( hash_data.sirov ) {
      
      var props = {
        filter: find_sirov.find_sirov_filter,
        url: '/find_sirov',
        query: { _id: hash_data.sirov },
        goto_status: ( hash_data.status || null),
        desc: `pretraga u routeru po _id unutar URL-a za sirovinu`,
      };
      try {
        db_result = await search_database(null, props );
      } catch (err) {
        console.log(err);
      };
      
    };
    
    // ako je našao nešto onda uzmi samo prvi član u arraju
    if ( db_result && $.isArray(db_result) && db_result.length > 0 ) {
      db_result = db_result[0];
    };
    
    $.each( db_result.locations, function(l_ind, local) {
      if ( 
        local.sklad == hash_data.sklad 
        && 
        local.sector == hash_data.sector
        &&
        local.level == hash_data.level
      ) {
        
        popup_warn( 
`
<h3>${ db_result.sirovina_sifra + "--" + (db_result.dobavljac?.naziv || "") + "--" + db_result.full_naziv }</h3>
<h1>${local.count} ${ db_result.osnovna_jedinica.jedinica }</h1>

` );
      };
      
    });
    
    
    var sirov_data = {
      ...db_result,
      id: `sirov_module`,
      goto_status: null,
    };
    
    sirov_module.create( sirov_data, $(`#cont_main_box`) );
    // ----------------------------------------------------------------------

    return;
  };
  
  
  
  
  if ( parse_router(`partner/:partner_id/status/:status_id`) ) {
    
    console.log( hash_data );
    
    window.google_maps_loaded = false;
    toggle_global_progress_bar(true);
    
    
    var partner_module = await get_cit_module(`/modules/partners/partner_module.js`, `load_css`);
    
    var find_partner = await get_cit_module(`/modules/partners/find_partner.js`, null);
    
    // ----------------------------------------------------------------------
    if ( !find_partner ) {
      toggle_global_progress_bar(false);
      return;
    };
    var find_partner_data = {
      id: `find_partner`,
      for_module: "partner",
    };
    find_partner.create( find_partner_data, $(`#search_main_box`) );    
    // ----------------------------------------------------------------------
    
    
    if ( !partner_module ) {
      toggle_global_progress_bar(false);
      return;
    };
    
    
    var db_result = {};
    
    if ( hash_data.partner ) {
      
      var props = {
        filter: find_partner.find_partner_filter,
        url: '/find_partner',
        query: { _id: hash_data.partner },
        desc: `pretraga u routeru po _id unutar URL-a za partnera`,
      };
      
      try {
        db_result = await search_database(null, props );
      } catch (err) {
        console.log(err);
      };
      
    };
    
    // ako je našao nešto onda uzmi samo prvi član u arraju
    if ( db_result && $.isArray(db_result) && db_result.length > 0 ) {
      db_result = db_result[0];
    };
    
    var partner_data = {
      ...db_result,
      id: `partner_module`,
      goto_status: ( hash_data.status || null),
    };
    
    
    partner_module.create( partner_data, $(`#cont_main_box`) );
    
    return;
    
  };
  
  // ----------------------------------- LINK ZA STATUSE  -----------------------------------
  // ----------------------------------- LINK ZA STATUSE  -----------------------------------
  // ----------------------------------- LINK ZA STATUSE  -----------------------------------
  
  if ( parse_router(`project/:project_sifra/item/:item_sifra/variant/:variant/status/:status_sifra`) ) {
    
    
    if ( !window.cit_user ) {
      popup_warn(`Za pregled ovog modula morate biti ulogirani!`);
      return;
    };
    
    
    // TODO za sada nema pretrage -----> kasnije napravim !!
    $(`#search_main_box`).html("");
    
    console.log( hash_data );
    
    var project_module = await get_cit_module(`/modules/project/project_module.js`, `load_css`);
    var find_pis_module = await get_cit_module(`/modules/project/find_PIS.js`, null);
    // ----------------------------------------------------------------------
    if ( !find_pis_module ) {
      toggle_global_progress_bar(false);
      return;
    };
    var find_pis_data = {
      id: `find_pis`,
      for_module: "project",
    };
    find_pis_module.create( find_pis_data, $(`#search_main_box`) );    
    // ----------------------------------------------------------------------
        
    
    var db_result = {};
    
    if ( hash_data.project ) {
      var props = {
        filter: find_pis_module.find_project_filter,
        url: '/find_proj',
        query: { proj_sifra: hash_data.project+"" },
        desc: `pretraga u routeru po URL-u za project`,
      };
      try {
        db_result = await search_database(null, props );
      } catch (err) {
        console.log(err);
      }
    };
    
    // ako je našao nešto onda uzmi samo prvi član u arraju
    if ( db_result && $.isArray(db_result) && db_result.length > 0 ) {
      
      db_result = db_result[0];
      
      var criteria = [ 'order_num' ];
      if ( db_result.items && $.isArray(db_result.items) && db_result.items.length > 0 ) multisort( db_result.items, criteria );
      
    };
    
    var project_data = {

      ...db_result,

      id: `project_module`,
      
      proj_sifra: (hash_data.project ? String(hash_data.project) : null),

      goto_item: (hash_data.item ? String(hash_data.item) : null),
      goto_variant: ( hash_data.variant ? String(hash_data.variant) : null),
      goto_status: (hash_data.status || null),
      
    };

    // window.scroll_elems_ids = [];
    project_module.create( project_data, $(`#cont_main_box`) );
    
    
    return;
    
  };
  // ----------------------------------- LINK ZA STATUSE  -----------------------------------
  // ----------------------------------- LINK ZA STATUSE  -----------------------------------
  // ----------------------------------- LINK ZA STATUSE  -----------------------------------
  
  
  
  // ----------------------------------- LINK ZA KALKULACIJE  -----------------------------------
  // ----------------------------------- LINK ZA KALKULACIJE  -----------------------------------
  // ----------------------------------- LINK ZA KALKULACIJE  -----------------------------------
  if ( parse_router(`project/:project_sifra/item/:item_sifra/variant/:variant/kalk/:kalk_sifra/proces/:proces_sifra`) ) {
    
    if ( !window.cit_user ) {
      popup_warn(`Za pregled ovog modula morate biti ulogirani!`);
      return;
    };
    
    
    $(`#search_main_box`).html("");
    
    console.log( hash_data );
    
    var project_module = await get_cit_module(`/modules/project/project_module.js`, `load_css`);
    var find_pis_module = await get_cit_module(`/modules/project/find_PIS.js`, null);
    // ----------------------------------------------------------------------
    if ( !find_pis_module ) {
      toggle_global_progress_bar(false);
      return;
    };
    var find_pis_data = {
      id: `find_pis`,
      for_module: "project",
    };
    find_pis_module.create( find_pis_data, $(`#search_main_box`) );    
    // ----------------------------------------------------------------------
        
    
    var db_result = {};
    
    if ( hash_data.project ) {
      var props = {
        filter: find_pis_module.find_project_filter,
        url: '/find_proj',
        query: { proj_sifra: hash_data.project+"" },
        desc: `pretraga u routeru po URL-u za project ali kada imam KALK unutar URL-a`,
      };
      try {
        db_result = await search_database(null, props );
      } catch (err) {
        console.log(err);
      }
    };
    
    // ako je našao nešto onda uzmi samo prvi član u arraju
    if ( db_result && $.isArray(db_result) && db_result.length > 0 ) {
      
      db_result = db_result[0];
      var criteria = [ 'order_num' ];
      if ( db_result.items && $.isArray(db_result.items) && db_result.items.length > 0 ) multisort( db_result.items, criteria );
      
    };
    
    var project_data = {

      ...db_result,

      id: `project_module`,
      
      proj_sifra: (hash_data.project ? String(hash_data.project) : null),

      goto_item: (hash_data.item ? String(hash_data.item) : null),
      goto_variant: ( hash_data.variant ? String(hash_data.variant) : null),
      goto_kalk: (hash_data.kalk || null),
      goto_proces: (hash_data.proces || null),
      
    };

    // window.scroll_elems_ids = [];
    project_module.create( project_data, $(`#cont_main_box`) );
    
    
    return;
    
  };
  // ----------------------------------- LINK ZA KALKULACIJE  -----------------------------------
  // ----------------------------------- LINK ZA KALKULACIJE  -----------------------------------
  // ----------------------------------- LINK ZA KALKULACIJE  -----------------------------------
    

  
      
  if ( parse_router(`statuses_page/:status_type`) ) {
    
    
    $(`#search_main_box`).html("");
    
    console.log( hash_data );
    
    var status_mod = await get_cit_module(`/modules/status/status_module.js`, `load_css`);
 
    var status_query = {};

    /*
    ----------------------------------------
    $or: [ 
      { elem_parent: { $ne : null }, responded: null, seen: null }, // ako je reply i nije odgovoren
      { elem_parent: null, responded: null, seen: null  } // ako je početak grane i nema odgovor
    ],
    ----------------------------------------
    */

    var status_type_title = null;
    
    if (hash_data.statuses_page == `cit_my_tasks` ) {
     
      status_type_title = "Mene čekaju";
      
      status_query = {
        "to.user_number": window.cit_user?.user_number,
        responded: null, 
        seen: null,
        "status_tip.is_work" : { $ne: true }
      };
      
      
      if ( window.cit_user?.dep.sifra == "CAD" ) {
        
        status_query = {
          "dep_to.sifra": "CAD",
          responded: null, 
          seen: null,
          "status_tip.is_work" : { $ne: true },
          
          $or: [ { branch_done: false }, { branch_done: { $exists: false } } ],
          
          
        };
        
      };
      
      
      if ( window.cit_user?.dep.sifra == "GRF" ) {
        
        status_query = {
          
          "dep_to.sifra": "GRF",
          responded: null, 
          "status_tip.is_work" : { $ne: true },
          
          $or: [ { branch_done: false }, { branch_done: { $exists: false } } ],
          $or: [ { branch_done: true }, { seen: null } ],
          
        };
        
      };
      
      
      if ( window.cit_user?.dep.sifra == "SKLOFFICE" ) {
        
        status_query = {
          "dep_to.sifra": "SKLOFFICE",
          responded: null, 
          "status_tip.is_work" : { $ne: true },
          $or: [ { branch_done: false }, { branch_done: { $exists: false } } ],
          $or: [ { branch_done: true }, { seen: null } ],
          
        };
        
      };
            
      
      
      
      // ovo se odnosi na proizvodnja work i proizvodnja office
      if ( 
        window.cit_user?.dep.sifra == "PRO"
        ||
        window.cit_user?.dep.sifra == "PRO2"
      ) {
        
        status_query = {
          
          $or: [ 
            {
              "to.user_number": window.cit_user?.user_number,
              $or: [ { branch_done: false }, { branch_done: { $exists: false } }  ],
              $or: [ { branch_done: true }, { seen: null } ],
            },
            {
              $or: [ { "dep_to.sifra": "PRO" }, { "dep_to.sifra": "PRO2" } ],
              $or: [ { branch_done: false }, { branch_done: { $exists: false } } ],
              $or: [ { branch_done: true }, { seen: null } ],
              /* elem_parent: null, */
            } 
          ]
        
        };
        
      }; // ako je iz proizvodnje ili office proiz
      
      
    } 
    else if ( hash_data.statuses_page == `cit_task_from_me` ) {
      
      status_type_title = "Ja čekam";
      
      status_query = {
        
        $or: [
          
          /* ako NIJE radni status onda trebam statuse koji su poslani OD MENE */
          {
            "from.user_number": window.cit_user?.user_number,
            responded: null, 
            seen: null,
            "status_tip.is_work" : { $ne: true }
          },

          /* ako JESTE radni status onda trebam radne statuse poslane PREMA MENI -- to je samo indikator da primatelj radi na mom zahtjevu */
          {
          "to.user_number": window.cit_user?.user_number,
          responded: null, 
          seen: null,
          $or: [ { work_finished: null }, { work_finished: { $exists: false } } ],
          "status_tip.is_work" : true
          },  
        ]
          
      };
      
      
      // ovo se odnosi proizvodnja office
      // NA PRIMJER BRUNO VIDI JA ČEKAM ZA SEBE I ZA SVE U PROIZVODNJI !!!!
      if ( window.cit_user?.dep.sifra == "PRO" || window.cit_user?.dep.sifra == "PRO2" ) {
        
        // ako je user u proizvodnji u pogonu 
        status_query.$or.push({
          "dep_to.sifra": "PRO",
          $or: [ { branch_done: false }, { branch_done: { $exists: false } } ],
          /* elem_parent: null, */
        });
        
        // ili ako je user iz office-a onda mu prikaži i sve radne procese
        status_query.$or.push({
          "dep_to.sifra": "PRO2",
          $or: [ { branch_done: false }, { branch_done: { $exists: false } } ],
          /*elem_parent: null,*/
        });
        
      };
      
      
      
    } 
    else if ( hash_data.statuses_page == `cit_working` ) {
      
      status_type_title = "Radim";
      
      status_query = {
        "from.user_number": window.cit_user?.user_number,
        work_finished: null,
        "status_tip.is_work" : true
      };
      
      
      if ( window.cit_user?.dep.sifra == "PRO" ) {
        
        status_query = {
          "dep_to.sifra": "PRO",
          work_finished: null,
          "status_tip.is_work" : true
        };
        
      };
      
      
    };

    
    var db_result = [];
    
    var props = {
      filter: null, // status_mod.statuses_filter,
      url: `/find_status`, // /${ hash_data.statuses_page || "none" }`,
      query: status_query,
      desc: `pretraga u routeru po URL-u za statuses page`,
    };
    
    try {
      db_result = await search_database(null, props );
    } catch (err) {
      console.log(err);
    }

    var status_data = {
      for_module: `statuses_page`,
      status_type_title: status_type_title,
      id: `statuses_page`,
      statuses: db_result,
    };
    
    status_mod.create( status_data, $(`#cont_main_box`) );
    
    
    var filter_statusa_module = await get_cit_module( `/modules/status/filter_statusa.js`, null );
    // ----------------------------------------------------------------------
    if ( !filter_statusa_module ) {
      toggle_global_progress_bar(false);
      return;
    };
    var filter_statusa_data = {
      /* ovo su podaci od modula status listu ispod filtera */
      for_module: `statuses_page`,
      status_type_title: status_type_title,
      status_page_id: `statuses_page`,
      /* ovo su podaci od modula status listu ispod filtera */
      
      
      id: `filterstatusamodule`,
      statuses: db_result,
      filter_status_query: {},
    };
    filter_statusa_module.create( filter_statusa_data, $(`#search_main_box`) );    
    // ----------------------------------------------------------------------
    
    
    return;
    
  };
  
     
  if ( parse_router(`production/:status_sifra/proces/:proces_sifra`) ) {
    
    
    $('#card_input_select').removeClass(`cit_hide`);
    
    // TODO za sada nema pretrage -----> kasnije napravim !!!
    
    $(`#search_main_box`).html("");
    
    var status_mod = await get_cit_module(`/modules/status/status_module.js`, `load_css`);
    
    console.log( hash_data );
 
    var status_query = {};

    var status_type_title = "Proizvodnja";

    // nađi sve statuse koji imaju ovu šifru statusa ili kojima je taj status parent
    status_query = {
      $or: [ { sifra: hash_data.production }, { elem_parent: hash_data.production } ] 
    };
    
    var db_result = [];
    
    var props = {
      filter: null, // status_mod.statuses_filter,
      url: `/find_status_for_production`, // /${ hash_data.statuses_page || "none" }`,
      query: status_query,
      desc: `pretraga STATUSA u routeru po URL-u za production page`,
    };
    
    // ------------------------- OČEKUJEM SAMO JEDA db result jer tražim samo jedan status
    
    try {
      db_result = await search_database(null, props );
    } catch (err) {
      console.log(err);
    };
    
    
    // ako u sebi ima kalk znači da je to za proizvodnju !!!!
    // ako u sebi ima kalk znači da je to za proizvodnju !!!!
    // ako u sebi ima kalk znači da je to za proizvodnju !!!!
    
    var db_product = null;
    
    // uzmi prvi status koji imakalku u sebi
    var kalk_od_statusa = null;
    $.each(db_result, function(status_ind, stat) {
      if ( kalk_od_statusa == null && stat.kalk ) {
        // čim nadjem prvi kalk  ----> sve sljedeće ignoriraj
        kalk_od_statusa = stat.kalk;
      };
    });
    
    
    // svaki status ima u sebi kalk
    if ( kalk_od_statusa ) {

      var props = {
        filter: null, // status_mod.statuses_filter,
        url: `/find_product`, // /${ hash_data.statuses_page || "none" }`,
        query: { _id: kalk_od_statusa.product_id },
        desc: `pretraga u routeru po URL-u za production page`,
      };

      try {
        db_product = await search_database(null, props );
        if ( db_product?.length > 0  ) db_product = db_product[0];
        
      } catch (err) {
        console.log(err);
      }
  
    };
    
    // dodaj productu neki id da ne radi grešku u generairanju html-a !!!!
    if ( db_product ) db_product.id = `productmodule` + cit_rand();
    
    
    var status_data = {
      for_module: `production`,
      status_type_title: status_type_title,
      id: `production_page`,
      
      product: db_product || null,
      statuses: db_result || [], // db result bi trebao biti samo jedan jedini status !!!!
      proces_id: hash_data.proces || null,
      
    };
    
    status_mod.create( status_data, $(`#cont_main_box`) );
    
    return;
    
  };
  
  
  if ( parse_router(`admin/:segment`) ) {

    console.log( hash_data );

    // iako imam restrikciju na serveru lakše mi je staviti warning na frontendu !!!
    if ( !window.cit_user ) {
      popup_warn(`Za pregled ovog modula morate biti ulogirani!`);
      return;
    };


    toggle_global_progress_bar(true);

    // window.change_hash = false;

    var admin_module = await get_cit_module(`/modules/admin/vel_admin.js`, 'load_css');

    if ( !admin_module ) {
      toggle_global_progress_bar(false);
      return;
    };




    var result = await get_admin_conf();


    console.log(result);

    if ( result.success == true ) {


      window.admin_conf = result.admin_conf;
      
      function condition_to_write_status_conf() {
        var local_list = false;
        if ( window.cit_local_list ) local_list = true;
        return local_list;
      };
      
      wait_for( condition_to_write_status_conf, function() {
        console.log(`---------------------------------- dobio sam admin conf  !!! ADMIN PAGE !!!  ---------------------------------- `);
        window.cit_local_list.status_conf = result.admin_conf.status_conf
        
        $(`#search_main_box`).html(``);

        var admin_data = {
          ...window.admin_conf,
          id: `admin_module`,
          segment: hash_data.admin
        };

        admin_module.create( admin_data, $(`#cont_main_box`) );
        
      }, 50*1000);

    } 
    else {
      console.log(`Nedostaju podaci za Admin konfiguraciju !!! ----- unutar routera `);
      console.error(admin_conf_result.error);
    };

    return;
    
  };

  
  if ( parse_router(`users/:segment`) ) {

    console.log( hash_data );

    // iako imam restrikciju na serveru lakše mi je staviti warning na frontendu !!!
    if ( !window.cit_user ) {
      popup_warn(`Za pregled ovog modula morate biti ulogirani!`);
      return;
    };


    toggle_global_progress_bar(true);

    // window.change_hash = false;

    var users_module = await get_cit_module(`/modules/admin/vel_users.js`, 'load_css');

    if ( !users_module ) {
      toggle_global_progress_bar(false);
      return;
    };

    $(`#search_main_box`).html(``);

    var users_data = {
      id: `users_module`,
      segment: hash_data.users
    };

    users_module.create( users_data, $(`#cont_main_box`) );


    return;
  };
  
  
  
  if ( parse_router(`kviz/:test`) ) {

    console.log( hash_data );

    // iako imam restrikciju na serveru lakše mi je staviti warning na frontendu !!!
    if ( !window.cit_user ) {
      popup_warn(`Za pregled ovog modula morate biti ulogirani!`);
      return;
    };


    toggle_global_progress_bar(true);

    // window.change_hash = false;

    var kviz_module = await get_cit_module(`/modules/kviz/kviz_module.js`, 'load_css');

    if ( !kviz_module ) {
      toggle_global_progress_bar(false);
      return;
    };

    $(`#search_main_box`).html(``);

    var kviz_data = {
      id: `kviz_module`,
      test: hash_data.kviz
    };

    kviz_module.create( kviz_data, $(`#cont_main_box`) );


    return;
  };
    
  

  if ( parse_router(`info/:info_obj/anchor/:anchor_id`) ) {

    console.log( hash_data );

    // iako imam restrikciju na serveru lakše mi je staviti warning na frontendu !!!
    if ( !window.cit_user ) {
      popup_warn(`Za pregled ovog modula morate biti ulogirani!`);
      return;
    };

    // obriši sve u gornjem divu ako je bilo nešto  
    $(`#search_main_box`).html(``);  

    toggle_global_progress_bar(true);

    window.change_hash = false;

    var info_module = await get_cit_module(`/modules/info/info.js`, 'load_css');

    if ( !info_module ) {
      toggle_global_progress_bar(false);
      return;
    };
    var info_data = {
      id: `info_module`,
      anchor: hash_data.anchor
    };

    info_module.create( info_data, $(`#cont_main_box`) );

    return;
  };

  
  if ( parse_router(`product/:prod_id/count/:prod_count`) ) {
    console.log( hash_data );
    return;
  };
  
  
  if ( parse_router(`user/:user_id/role/:role_id`) ) {
    console.log( hash_data );
    return;
  };
  
  console.log('pathname NOT MATCHED!!!!!!');
  
};

window.user_logged = null;

wait_for(`window.user_logged !== null`, function() {
  
  window.initial_page_load = true;
  // first time on frist load
  router_listener();
  
  window.initial_page_load = false;
  
  // window.addEventListener('popstate', router_listener, false);
  
  // window.removeEventListener("hashchange", router_listener, false);
  window.addEventListener("hashchange", router_listener, false);
  
  
  /*
  window.addEventListener('popstate', (event) => {
    console.log("POP STATE ---------------> location: " + document.location + ", state: " + JSON.stringify(event.state));
  });
  */

}, 50*1000);  

function update_hash(hash_data) {
  // var hash_data = { project: [1,2, " text ggg fff "], upit: { prvi: 1, drugi: 2 } };
  if ( !hash_data ) return;
  
  var new_hash = "";
  
  $.each(hash_data, function( pathname_key, hash_obj ) {
    var decode_hash_obj = "";
    
    if ( hash_obj ) {
      
      if ( $.isPlainObject(hash_obj) || $.isArray(hash_obj) ) {
        decode_hash_obj = decodeURI( JSON.stringify(hash_obj) );
      } else {
        decode_hash_obj = decodeURI(hash_obj);
      };
      
    } else {
      decode_hash_obj = null;
    };
      
    new_hash += pathname_key + '/' + decode_hash_obj + '/';
    
  });
  
  console.log(new_hash);
  new_hash = new_hash.replace(/\/([^\/]*)$/, '$1'); // remove last slash from URL
  console.log(new_hash);
  history.pushState(hash_data, null, '/#' + new_hash);
  
}; // kraj update hash data


