function cit_comp(comp_id, valid, data, input_text, arg_style, arg_class ) {
  
  let component = '';
  
  if (!valid) {
    console.log(`valid is missing`);
    return "";
  };

  let lock = (valid && valid.lock == true) ? 'disabled' : '';
  let visible = (valid.visible == false) ? 'cit_hide' : '';
  
  if ( valid.element == 'single_select' ) {
    
  component =
`
<label  for="${comp_id}" class="cit_label ${visible}">${valid.label}</label>
<input  autocomplete="off"
        id="${comp_id}" 
        type="text" 
        class="cit_input cit_auto ${valid.element} ${valid.type} ${visible}"
        value="${input_text}"
        style="${ arg_style || '' }"
        ${lock} >
`;
  
  };
  
  if ( valid.element == 'input' ) {
 
    var value = data;
    var data_decimals = '';
    var data_number = '';

    // ako je value falsy onda nemoj prikazati ništa
    // OSIM AKO JE NULA ----> onda je ipak prikaži
    var text_value = (!value && value !== 0) ? "" : value ;

    if ( valid.type == 'number') {
      data_decimals = valid.decimals ? `data-decimals="${valid.decimals}"` : `data-decimals="0"`;
      data_number = `data-number="${ value || 0 }"`;
      if ( value !== null && value !== "" ) text_value = cit_format(value, valid.decimals || 0);
    };

      component =
`
<label for="${comp_id}" class="cit_label ${visible}">${valid.label}</label>

<input  id="${comp_id}" style="${ arg_style || '' }"
        autocomplete="off"
        type="text"
        class="cit_input ${valid.type} ${visible} ${ arg_class || "" }" ${lock} value="${text_value}"
        ${data_decimals}
        ${data_number}>
`;


  };
  
  if ( valid.element == 'text_area' ) {
  
    var value = data;
    component =
`
<label for="${comp_id}" class="cit_label ${visible}">${valid.label}</label>
<textarea id="${comp_id}" class="cit_text_area ${ arg_class || '' }" style="${ arg_style || '' }">${ value || "" }</textarea>
`;


  };  
  
  if ( valid.element == 'multi_select' ) {
    
    component =
`
<label  for="${comp_id}" class="cit_label ${visible}">${valid.label}</label>
<input  autocomplete="off" 
        id="${comp_id}" 
        type="text" 
        class="cit_input cit_auto ${valid.element} ${valid.type} ${visible}"
        value="${input_text}"
        style="${ arg_style || '' }"
        ${lock} >
`;
    
    
    /*
    var condition = function() {
      
      return (
        $(`#${comp_id}`).length > 0                                  &&
        typeof $(`#${comp_id}`).data("cit_props") !== "undefined"    &&
        $(`#${comp_id}`).data("cit_props") !== null
      )
      
    };
    wait_for(condition, function() {
      var props = $("#" + comp_id).data(`cit_props`);
      create_multi_list(comp_id, props);
    }, 5*1000);     
    */
    
    
  };  
  
  if ( valid.element == 'switch' ) {
    
    // pošto je ovo moj custom element onda disabled ne radi na njemu nego imam poseban class  
    lock = (valid.lock == true) ? 'cit_disable' : '';  
    var component_class = comp_id.substr( comp_id.indexOf("_") + 1, 10000000);
    
    
component =
`
<div class="cit_switch_wrapper">
  <label for="${comp_id}" class="cit_label ${visible}">${valid.label}</label>
  <div id="${comp_id}" class="cit_switch ${ data ? 'on' : 'off' } ${visible} ${lock} ${component_class}" style="${ arg_style || '' }">
    <div class="switch_circle"></div>
    <div class="on_text switch_text">DA</div>
    <div class="off_text switch_text">NE</div>
  </div>
</div>
`;
  
};  
  
  if ( valid.element == 'check_box' ) {
    
  // pošto je ovo moj custom element onda disabled ne radi na njemu nego imam poseban class  
  lock = (valid.lock == true) ? 'cit_disable' : '';  

    var label_html = "";
    
    if (valid.label) label_html = `<label for="${comp_id}" class="cit_label ${visible}">${valid.label}</label>`;
    
component =
`
<div class="cit_check_box_wrapper" style="${ arg_style || '' }" >
  ${label_html}
  <div  id="${comp_id}" 
        class="cit_check_box ${ data ? 'on' : 'off' } ${visible} ${lock} ${arg_class || "" } " 
        style="${ arg_style || '' }" >
        
    <i class="fas fa-check"></i>
    
  </div>
</div>
`;
  
};  
  
  if ( valid.element == 'upload' ) {

    // pošto je ovo moj custom element onda disabled ne radi na njemu nego imam poseban class  
    lock = (valid.lock == true) ? 'cit_disable' : '';  

    var multi_or_single = "";
    if ( valid.type == "multiple" ) multi_or_single = "multiple";
    
    var just_images = ``;
    if ( arg_class == "for_gallery" ) {
      // just_images = `accept="image/*"`
      just_images = `accept="image/png, image/gif, image/jpeg"`;
    };
    
component =
` 
  <label for="${comp_id}" class="blue_btn btn_small_text ${visible}" style="box-shadow: none; ${ arg_style || "" }">
    <i class="fas fa-cloud-upload-alt" style="margin-right: 5px; font-size: 22px;"></i>
    ${valid.label.toUpperCase() }
  </label>

  
  <input class="cit_input upload_comp" type="file" id="${comp_id}" ${multi_or_single} ${just_images} hidden />

  
  <div id="${comp_id}_list_box" class="upload_list_box" style="display: none;">
    <div id="${comp_id}_list_items" class="upload_list">
    <!-- OVDJE IDE LISTA ODABRANIH FILOVA -->  
    </div> 

    <button class="blue_btn btn_small_text ${arg_class || "" }" id="${comp_id}_upload_btn" style="margin: 5px 0; box-shadow: none;">
    SPREMI
    </button>
    
  </div>

`;
 
 
setTimeout( function() {
  
  $(`#${comp_id}`).off('change');
  $(`#${comp_id}`).on('change', function(e) {
    
    
    e.stopPropagation();
    e.preventDefault();
    
    var files = $(`#${comp_id}`)[0].files;
    
    if ( !files || files.length == 0 ) {
      return;
    };
    
    
    var all_items = ``;
    $.each(files, function(f_index, file) {
      
      var item_html = 
        `
          <div class="upload_list_item cit_tooltip"
          data-toggle="tooltip" data-placement="bottom" data-html="true" title="${ file.name }">
            ${ file.name }
          </div>
        `;
      all_items +=item_html
      
    });    
    
    
    $(`#${comp_id}_list_items`).html( all_items );
    $(`#${comp_id}_list_box`).css( `display`, `block` );
        
    // vrati gumb za uplaod ako je možda bio sakriven
    $(`#${comp_id}_upload_btn`).css(`display`, `flex`);
    
    
    // init tooltip za nove redove
    setTimeout( function() { reset_tooltips(); }, 200);
    
  });
  
  $(`#${comp_id}_upload_btn`).off('click');
  $(`#${comp_id}_upload_btn`).on('click', function (e) {
    
    var this_button = this;
    
    toggle_global_progress_bar(true, 60); // drug argument je 60 sekundi
    $(this_button).css("pointer-events", "none");
    
    
    e.preventDefault();

    const formData = new FormData();
    
    var this_comp_id =  $(`#${comp_id}_upload_btn`).closest('.cit_comp')[0].id;
    var this_module = $(`#${comp_id}`).data(`this_module`); // upisao sam u data od upload buttona cijel this_module !!!!
    var data = this_module.cit_data[this_comp_id];
    
    if ( data.id == "partner_module" ) {
      formData.append(`partner_sifra`, data.partner_sifra);
    };
    
    if ( $(this_button).hasClass(`for_gallery`) ) {
      formData.append(`for_gallery`, `for_gallery`);
    };
    
    
    var files = $(`#${comp_id}`)[0].files;
    
    if ( !files || files.length == 0 ) {
      popup_warn(`Niste izabrali niti jedan dokument !!!`);
      $(this_button).css("pointer-events", "all");
      return;
    };
    
    $.each(files, function(f_index, file) {
      var original_name = file.name;
      formData.append( `cit_docs` , file, original_name);
    });
    

    var avatar_url = '/upload_docs';

    
    $.ajax('/upload_docs', {
      method: 'POST',
      data: formData,
      processData: false,
      contentType: false,
    })
    .done(function (response) {

      console.log(response);

      // RESETIRAJ SVE FILOVE UNUTAR INPUT-a
      $(`#${comp_id}`).val('');
      
      // obriši listu
      // $(`#${comp_id}_list_items`).html( "" );
      // sakrij listu i button
      // $(`#${comp_id}_list_box`).css( `display`, `none` );
      
      // sakrij samo gumb da ne može spremiti ono što je već spremio
      $(`#${comp_id}_upload_btn`).css(`display`, `none`);

      if ( response.success == true ) {
        
        // zadnji arg time mi ovdje služi samo da budem 100% siguran u točno vrijeme tako da snam u koji folder da to stavim tj kako da generiram URL path 
        $(`#${comp_id}`).data(`cit_run`)( response.files, $(`#${comp_id}`), response.time ); 
        
        
        popup_msg(`Svi dokumenti spremljeni`);
        
        $(`#${comp_id}_upload_btn`).css(`display`, `none`);
        
        
      } else {
        
        popup_error(response.msg);
        
      };

    })
    .fail(function(error) {
      console.log(error);
      console.log(`GREŠKA U AJAXU za upload dokumenata na gumbu: ${comp_id}`);
    })
    .always(function() {
      
      toggle_global_progress_bar(false);
      $(this_button).css("pointer-events", "all");
      
    });
    
    
       
  });
  
}, 300 ); 
 
  
};   
  
  if ( valid.element == 'simple_calendar' ) {
    
    if ( valid.type == `date` ) {
      input_format = `date`;
      input_text = data ? cit_dt(data).date : "";
    } else if ( valid.type == `time` ) {
      input_format = `time`;
      input_text = data ? cit_dt(data).time : "";
    } else {
      input_text = data ? cit_dt(data).date+" "+cit_dt(data).time : "";
    };
    
    component =
`
<label for="${comp_id}" class="cit_label ${visible}">${valid.label}</label>
<input  autocomplete="off" id="${comp_id}" type="text" 
        class="cit_input cit_auto ${valid.element} ${valid.type} ${visible}"
        value="${input_text}"
        ${lock}>
`;
  
  };
  
  
  // spremi pravila validacije u element koji sam stvorio
  wait_for(`$('#${comp_id}').length > 0`, function() {
    $('#'+comp_id).data(`cit_valid`, valid);
  }, 5*1000);
  
  return component;  
    
};


function create_multi_list(comp_id, props) {
  

  if ( props.multi_list && props.multi_list.length > 0 ) {
    
    
    if ( props.filter && props.local == true ) {
      
      props.multi_list = cit_deep(props.multi_list);
      props.multi_list = props.filter( props.multi_list );
    };

    var multi_list_header = ``;
    var list_html = ``;
    
    var key_by_order = null;
    
    var added_col_already_created_header = [];
    
    var col_widths_sum = 0;

    $.each(props.multi_widths, function(width_key, width) {
      col_widths_sum += Number(width);
    });
    
    
    $.each(props.multi_col_order, function(order_index, key_by_order) {
      // ako nema neki property kao na primjer delete_invite
      // onda ga napravi
      $.each(props.multi_list, function(ml_index, list_item) {  
        if ( list_item.hasOwnProperty(key_by_order) == false  ) props.multi_list[ml_index][key_by_order] = true;
      });
    });
    
    
    $.each(props.multi_col_order, function(order_index, key_by_order) {
      
    // ovdje u multi list ne postoji  neki propertiji
    // kao na primjer delete invite koji stupac samo za gumb
    $.each(props.multi_list[0], function(key, value) {
      
      if ( key !== '_id' && key !== 'sifra' && key == key_by_order ) {
        
        // ovo je standardno ime stupca koje generiram iz prpertija        
        var header_cell = 

        `
        <div  class="multi_header_cell" style="width: ${ props.multi_widths[key]/col_widths_sum*100 }%;">
          ${key.replace(/\_/g, " ").toUpperCase()}
        </div>
        `;        
        
        
        // ali ako je neki od specijalnih stupaca kao naprimjer kada trebam ubaciti neki gumb ili specijalni element
        // ALI OVDJE SE ISKLJUČIVO RADI O PROPERTIJIMA KOJI VEĆ POSTOJE U DATA SETU
        $.each(props.add_cols, function(add_col_index, add_col_func) {
          var new_col = add_col_func(null);
          if ( new_col.prop_name == key ) {
            header_cell = new_col.header;
            added_col_already_created_header.push(key);
          };
        });
        
        multi_list_header += header_cell;
        
      }; // ne smje biti niti _id niti sifra i MORA biti točno taj key by order
      
    }); // kraj loopa za header po podacima liste
    
      
      
  });  /// loop po orderu


    $.each(props.multi_list, function(index, item) {
      
      var list_row = ``;

      var row_id = `multi_row_` + (item._id || item.sifra);

      var row_cells = ``;
      var added_col_already_created_cell = [];
      
      
      $.each(props.multi_col_order, function(order_index, key_by_order) {

        $.each(item, function(key, value) {

          if ( key !== '_id' && key !== 'sifra' && key == key_by_order ) {

            var cell = `<div class="multi_cell" style="width: ${ props.multi_widths[key]/col_widths_sum*100 }%;" >${value}</div>`;

            // ali ako je neki od specijalnih stupaca kao naprimjer kada trebam ubaciti neki gumb ili specijalni element
            // ONDA PREPIŠI GORNJU CELL VARIJABLU
            $.each(props.add_cols, function(add_col_index, add_col_func) {
              var new_col = add_col_func(row_id);
              if ( new_col.prop_name == key ) {
                cell = new_col.cell;
                added_col_already_created_cell.push(key);
              };
            });

            row_cells += cell;

          };

        });

      }); // loop po ordera arraju

      list_row = `<div id="${row_id}" class="multi_row">${row_cells}</div>`;
      list_html += list_row;
      
    }); // loop po cijeloj multi listi
    

    
    multi_list_header = 
      `<div class="multi_list_header">${multi_list_header}</div>`    
    

    list_html = 
`
<div id="${comp_id}_multi_list" class="multi_list">
${multi_list_header}
${list_html}
</div>
`
    // removaj postojeću listu ako već postoji
    $(`#${comp_id}_multi_list`).remove();
    
    if (props.multi_list_parent) {
      // ako sam odredio parent element u koji stavljam tablicu
      $(props.multi_list_parent).html(list_html);
    } else {
      // ako nema parenta stavi generiranu tablicu ispod drop liste
      $(`#${comp_id}`).after(list_html);
    };
    
    
  };
  
};




// ---------------------------------------- GALLERY COMPONENT ----------------------------------------





