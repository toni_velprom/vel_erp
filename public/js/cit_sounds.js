const AudioContext = window.AudioContext || window.webkitAudioContext;


var sound_array = [
  { name: "cit_new", src: `/sounds/message.mp3` },
  { name: "cit_my_tasks", src: `/sounds/message.mp3` },
  { name: "cit_task_from_me", src: `/sounds/swoosh.mp3` },
  { name: "cit_working", src: `/sounds/message.mp3` },
  
  
  { name: "scan_done", src: `/sounds/mario_coin.mp3` },
  { name: "scan_start", src: `/sounds/smb_flagpole.mp3` },
  { name: "scan_stop", src: `/sounds/mario_die.mp3` },
  
];


function cit_register_sounds() {
  
  $.each(sound_array, function(ind, sound) {
    sound.elem = document.createElement("audio");
    sound.elem.src = sound.src;
    document.body.appendChild(sound.elem);
    const audioContext = new AudioContext();
    sound.audioContext = audioContext;
    // pass it into the audio context
    var track = audioContext.createMediaElementSource(sound.elem);
    track.connect(audioContext.destination);
  });
    
};

cit_register_sounds();


function cit_stop(sound_name) {
  $.each(sound_array, function(ind, sound) {
    if ( sound.name == sound_name) {
      sound.elem.pause();
      sound.elem.currentTime = 0;
    };
  });
};

function cit_play(sound_name) {
  
  //stopiraj sve zvukove koji su u toku
  $.each(sound_array, function(ind, sound) {
    cit_stop(sound.name);
  });
  
  $.each(sound_array, function(ind, sound) {
    if ( sound.name == sound_name) {
      // check if context is in suspended state (autoplay policy)
      if (sound.audioContext.state === 'suspended') {
        sound.audioContext.resume();
      };
      sound.elem.pause();
      sound.elem.currentTime = 0;
      sound.elem.play();
    };
    
  });

};


