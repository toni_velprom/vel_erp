(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// ovdje sve libs koji trebaju biti dostupni globalno !!!!!
var isEven = require('is-even'); // alert('teeeeeest');


load_lib("npm/is-even", isEven);
$(document).ready(function () {
  var cit_layout_module_data = {
    id: "cit_layout_module",
    text: null,
    name: 'Pero',
    last_name: 'Perić',
    street: 'Avenija Dubrovnik 23',
    place: 'Zagreb',
    post_num: 10000
  };
  wait_for("$('#close_left_menu').length > 0", /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    var left_menu_state, preview_mod;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            left_menu_state = window.localStorage.getItem('left_menu_state'); // ako je mali ekran ili ako je u local storage closed

            if ($(window).width() < 600 || left_menu_state == "closed") {
              // i ako left menu nema cit hide klasu tj ako je otvoren onda ga zatvori
              if ($('#close_left_menu').hasClass('cit_hide') == false) {
                $("#cit_left_menu_box").addClass('cit_hide');
                $("#cit_main_area_box").addClass('cit_wide');
                $("#kalendar_header").addClass('cit_wide');
                $("#close_left_menu").addClass('cit_hide'); //sakrij X

                $("#open_left_menu").removeClass('cit_hide'); // prikaži  ----> prikaži hamburger
              }

              ;
            } else {
              // ako prozor veći i ako nije upisano u local storage da je closed
              // i to samo ako je closed -----> onda ga otvori
              if ($('#close_left_menu').hasClass('cit_hide') == true) {
                window.localStorage.setItem('left_menu_state', "opened");
                $("#cit_left_menu_box").removeClass('cit_hide');
                $("#cit_main_area_box").removeClass('cit_wide');
                $("#kalendar_header").removeClass('cit_wide');
                $("#open_left_menu").addClass('cit_hide'); // sakrij hamburger

                $("#close_left_menu").removeClass('cit_hide'); // prikaži X
              }

              ;
            }

            ;
            _context.next = 5;
            return get_cit_module('/modules/previews/cit_previews.js', "load_css");

          case 5:
            preview_mod = _context.sent;
            preview_mod.create({
              id: 'cit_preview_modal'
            }, $('body'), 'prepend');

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  })), 50 * 1000); // odmah u startu kreiram HTML za popup da stoji unutar body

  var pop_mod = window['/preload_modules/site_popup_modal/site_popup_modal.js'];
  pop_mod.create({
    id: 'cit_popup_modal'
  }, $('body'), 'prepend');
  /*
  get_cit_module(`/preload_modules/cit_layout_module.js`, null)
  .then((cit_layout_module) => { 
    cit_layout_module.create( cit_layout_module_data, $(`#cont_main_box`) );
  });
  
  */

  get_cit_module("/preload_modules/top_menu/cit_top_menu.js", null).then(function (cit_top_menu) {
    var top_menu_data = {
      id: "cit_top_menu"
    };
    cit_top_menu.create(top_menu_data, $("#cit_top_menu_box"));
  });
  get_cit_module("/preload_modules/left_menu/cit_left_menu.js", null).then(function (cit_left_menu) {
    var left_menu_data = {
      id: "cit_left_menu"
    };
    cit_left_menu.create(left_menu_data, $("#cit_left_menu_box"));
  });
  get_cit_module("/preload_modules/right_menu/cit_right_menu.js", null).then(function (cit_right_menu) {
    var right_menu_data = {
      id: "cit_right_menu"
    };
    cit_right_menu.create(right_menu_data, $("#cit_right_menu_box"));
  });
}); // end of doc raedy

window.cit_page_active = null;

(function () {
  var hidden = "hidden"; // Standards:

  if (hidden in document) document.addEventListener("visibilitychange", onchange);else if ((hidden = "mozHidden") in document) document.addEventListener("mozvisibilitychange", onchange);else if ((hidden = "webkitHidden") in document) document.addEventListener("webkitvisibilitychange", onchange);else if ((hidden = "msHidden") in document) document.addEventListener("msvisibilitychange", onchange); // IE 9 and lower:
  else if ("onfocusin" in document) document.onfocusin = document.onfocusout = onchange; // All others:
    else window.onpageshow = window.onpagehide = window.onfocus = window.onblur = onchange;

  function onchange(evt) {
    var v = "visible";
    var h = "hidden";
    var evtMap = {
      focus: v,
      focusin: v,
      pageshow: v,
      blur: h,
      focusout: h,
      pagehide: h
    };
    evt = evt || window.event;

    if (evt.type in evtMap) {
      window.cit_page_active = evtMap[evt.type]; // document.body.className = evtMap[evt.type];
      // console.log(evtMap[evt.type]);

      console.log(window.cit_page_active);
      socket_check_in();
    } else {
      window.cit_page_active = window.cit_page_active == "hidden" ? "visible" : "hidden";
      console.log(window.cit_page_active);
      socket_check_in(); // console.log(evtMap[evt.type]);
      // document.body.className = this[hidden] ? "hidden" : "visible";
    }

    ;
  }

  ; // kraj func on change
  // set the initial state (but only if browser supports the Page Visibility API)

  if (document[hidden] !== undefined) onchange({
    type: document[hidden] ? "blur" : "focus"
  });
})();

},{"is-even":3}],2:[function(require,module,exports){
/*!
 * Determine if an object is a Buffer
 *
 * @author   Feross Aboukhadijeh <https://feross.org>
 * @license  MIT
 */

// The _isBuffer check is for Safari 5-7 support, because it's missing
// Object.prototype.constructor. Remove this eventually
module.exports = function (obj) {
  return obj != null && (isBuffer(obj) || isSlowBuffer(obj) || !!obj._isBuffer)
}

function isBuffer (obj) {
  return !!obj.constructor && typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj)
}

// For Node v0.10 support. Remove this eventually.
function isSlowBuffer (obj) {
  return typeof obj.readFloatLE === 'function' && typeof obj.slice === 'function' && isBuffer(obj.slice(0, 0))
}

},{}],3:[function(require,module,exports){
/*!
 * is-even <https://github.com/jonschlinkert/is-even>
 *
 * Copyright (c) 2015, 2017, Jon Schlinkert.
 * Released under the MIT License.
 */

'use strict';

var isOdd = require('is-odd');

module.exports = function isEven(i) {
  return !isOdd(i);
};

},{"is-odd":5}],4:[function(require,module,exports){
/*!
 * is-number <https://github.com/jonschlinkert/is-number>
 *
 * Copyright (c) 2014-2015, Jon Schlinkert.
 * Licensed under the MIT License.
 */

'use strict';

var typeOf = require('kind-of');

module.exports = function isNumber(num) {
  var type = typeOf(num);

  if (type === 'string') {
    if (!num.trim()) return false;
  } else if (type !== 'number') {
    return false;
  }

  return (num - num + 1) >= 0;
};

},{"kind-of":6}],5:[function(require,module,exports){
/*!
 * is-odd <https://github.com/jonschlinkert/is-odd>
 *
 * Copyright (c) 2015-2017, Jon Schlinkert.
 * Released under the MIT License.
 */

'use strict';

var isNumber = require('is-number');

module.exports = function isOdd(i) {
  if (!isNumber(i)) {
    throw new TypeError('is-odd expects a number.');
  }
  if (Number(i) !== Math.floor(i)) {
    throw new RangeError('is-odd expects an integer.');
  }
  return !!(~~i & 1);
};

},{"is-number":4}],6:[function(require,module,exports){
var isBuffer = require('is-buffer');
var toString = Object.prototype.toString;

/**
 * Get the native `typeof` a value.
 *
 * @param  {*} `val`
 * @return {*} Native javascript type
 */

module.exports = function kindOf(val) {
  // primitivies
  if (typeof val === 'undefined') {
    return 'undefined';
  }
  if (val === null) {
    return 'null';
  }
  if (val === true || val === false || val instanceof Boolean) {
    return 'boolean';
  }
  if (typeof val === 'string' || val instanceof String) {
    return 'string';
  }
  if (typeof val === 'number' || val instanceof Number) {
    return 'number';
  }

  // functions
  if (typeof val === 'function' || val instanceof Function) {
    return 'function';
  }

  // array
  if (typeof Array.isArray !== 'undefined' && Array.isArray(val)) {
    return 'array';
  }

  // check for instances of RegExp and Date before calling `toString`
  if (val instanceof RegExp) {
    return 'regexp';
  }
  if (val instanceof Date) {
    return 'date';
  }

  // other objects
  var type = toString.call(val);

  if (type === '[object RegExp]') {
    return 'regexp';
  }
  if (type === '[object Date]') {
    return 'date';
  }
  if (type === '[object Arguments]') {
    return 'arguments';
  }
  if (type === '[object Error]') {
    return 'error';
  }

  // buffer
  if (isBuffer(val)) {
    return 'buffer';
  }

  // es6: Map, WeakMap, Set, WeakSet
  if (type === '[object Set]') {
    return 'set';
  }
  if (type === '[object WeakSet]') {
    return 'weakset';
  }
  if (type === '[object Map]') {
    return 'map';
  }
  if (type === '[object WeakMap]') {
    return 'weakmap';
  }
  if (type === '[object Symbol]') {
    return 'symbol';
  }

  // typed arrays
  if (type === '[object Int8Array]') {
    return 'int8array';
  }
  if (type === '[object Uint8Array]') {
    return 'uint8array';
  }
  if (type === '[object Uint8ClampedArray]') {
    return 'uint8clampedarray';
  }
  if (type === '[object Int16Array]') {
    return 'int16array';
  }
  if (type === '[object Uint16Array]') {
    return 'uint16array';
  }
  if (type === '[object Int32Array]') {
    return 'int32array';
  }
  if (type === '[object Uint32Array]') {
    return 'uint32array';
  }
  if (type === '[object Float32Array]') {
    return 'float32array';
  }
  if (type === '[object Float64Array]') {
    return 'float64array';
  }

  // must be a plain object
  return 'object';
};

},{"is-buffer":2}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy5udm0vdmVyc2lvbnMvbm9kZS92MTMuMTQuMC9saWIvbm9kZV9tb2R1bGVzL2Jyb3dzZXJpZnkvbm9kZV9tb2R1bGVzL2Jyb3dzZXItcGFjay9fcHJlbHVkZS5qcyIsInB1YmxpYy9qcy9pbml0aWFsX3NjcmlwdHMuanMiLCJwdWJsaWMvbm9kZV9tb2R1bGVzL2lzLWJ1ZmZlci9pbmRleC5qcyIsInB1YmxpYy9ub2RlX21vZHVsZXMvaXMtZXZlbi9pbmRleC5qcyIsInB1YmxpYy9ub2RlX21vZHVsZXMvaXMtbnVtYmVyL2luZGV4LmpzIiwicHVibGljL25vZGVfbW9kdWxlcy9pcy1vZGQvaW5kZXguanMiLCJwdWJsaWMvbm9kZV9tb2R1bGVzL2tpbmQtb2YvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7QUNBQTtBQUNBLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxTQUFELENBQXBCLEMsQ0FFQTs7O0FBRUEsUUFBUSxnQkFBZ0IsTUFBaEIsQ0FBUjtBQUdBLENBQUMsQ0FBQyxRQUFELENBQUQsQ0FBWSxLQUFaLENBQWtCLFlBQVk7QUFHNUIsTUFBSSxzQkFBc0IsR0FBRztBQUMzQixJQUFBLEVBQUUscUJBRHlCO0FBRTNCLElBQUEsSUFBSSxFQUFFLElBRnFCO0FBRzNCLElBQUEsSUFBSSxFQUFFLE1BSHFCO0FBSTNCLElBQUEsU0FBUyxFQUFFLE9BSmdCO0FBSzNCLElBQUEsTUFBTSxFQUFFLHNCQUxtQjtBQU0zQixJQUFBLEtBQUssRUFBRSxRQU5vQjtBQU8zQixJQUFBLFFBQVEsRUFBRTtBQVBpQixHQUE3QjtBQVdBLEVBQUEsUUFBUSwwR0FBcUM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRXZDLFlBQUEsZUFGdUMsR0FFckIsTUFBTSxDQUFDLFlBQVAsQ0FBb0IsT0FBcEIsQ0FBNEIsaUJBQTVCLENBRnFCLEVBSTNDOztBQUNBLGdCQUFLLENBQUMsQ0FBQyxNQUFELENBQUQsQ0FBVSxLQUFWLEtBQW9CLEdBQXBCLElBQTJCLGVBQWUsSUFBSSxRQUFuRCxFQUE4RDtBQUU1RDtBQUNBLGtCQUFJLENBQUMsQ0FBQyxrQkFBRCxDQUFELENBQXNCLFFBQXRCLENBQStCLFVBQS9CLEtBQThDLEtBQWxELEVBQXlEO0FBRXZELGdCQUFBLENBQUMsc0JBQUQsQ0FBd0IsUUFBeEIsQ0FBaUMsVUFBakM7QUFDQSxnQkFBQSxDQUFDLHNCQUFELENBQXdCLFFBQXhCLENBQWlDLFVBQWpDO0FBQ0EsZ0JBQUEsQ0FBQyxvQkFBRCxDQUFzQixRQUF0QixDQUErQixVQUEvQjtBQUVBLGdCQUFBLENBQUMsb0JBQUQsQ0FBc0IsUUFBdEIsQ0FBK0IsVUFBL0IsRUFOdUQsQ0FNVjs7QUFDN0MsZ0JBQUEsQ0FBQyxtQkFBRCxDQUFxQixXQUFyQixDQUFpQyxVQUFqQyxFQVB1RCxDQU9UO0FBRS9DOztBQUFBO0FBRUYsYUFkRCxNQWVLO0FBRUg7QUFFQTtBQUNBLGtCQUFLLENBQUMsQ0FBQyxrQkFBRCxDQUFELENBQXNCLFFBQXRCLENBQStCLFVBQS9CLEtBQThDLElBQW5ELEVBQTBEO0FBRXhELGdCQUFBLE1BQU0sQ0FBQyxZQUFQLENBQW9CLE9BQXBCLENBQTRCLGlCQUE1QixFQUErQyxRQUEvQztBQUVBLGdCQUFBLENBQUMsc0JBQUQsQ0FBd0IsV0FBeEIsQ0FBb0MsVUFBcEM7QUFDQSxnQkFBQSxDQUFDLHNCQUFELENBQXdCLFdBQXhCLENBQW9DLFVBQXBDO0FBQ0EsZ0JBQUEsQ0FBQyxvQkFBRCxDQUFzQixXQUF0QixDQUFrQyxVQUFsQztBQUVBLGdCQUFBLENBQUMsbUJBQUQsQ0FBcUIsUUFBckIsQ0FBOEIsVUFBOUIsRUFSd0QsQ0FRYjs7QUFDM0MsZ0JBQUEsQ0FBQyxvQkFBRCxDQUFzQixXQUF0QixDQUFrQyxVQUFsQyxFQVR3RCxDQVNUO0FBRWhEOztBQUFBO0FBR0Y7O0FBQUE7QUF2QzBDO0FBQUEsbUJBd0NyQixjQUFjLENBQUMsbUNBQUQsYUF4Q087O0FBQUE7QUF3Q3pDLFlBQUEsV0F4Q3lDO0FBMEM3QyxZQUFBLFdBQVcsQ0FBQyxNQUFaLENBQW9CO0FBQUUsY0FBQSxFQUFFLEVBQUU7QUFBTixhQUFwQixFQUFpRCxDQUFDLENBQUMsTUFBRCxDQUFsRCxFQUE0RCxTQUE1RDs7QUExQzZDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQXJDLElBNkNMLEtBQUcsSUE3Q0UsQ0FBUixDQWQ0QixDQThENUI7O0FBQ0EsTUFBSSxPQUFPLEdBQUcsTUFBTSxDQUFDLHVEQUFELENBQXBCO0FBQ0EsRUFBQSxPQUFPLENBQUMsTUFBUixDQUFlO0FBQUUsSUFBQSxFQUFFLEVBQUU7QUFBTixHQUFmLEVBQTBDLENBQUMsQ0FBQyxNQUFELENBQTNDLEVBQXFELFNBQXJEO0FBS0E7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUUsRUFBQSxjQUFjLDhDQUE4QyxJQUE5QyxDQUFkLENBQ0MsSUFERCxDQUNNLFVBQUMsWUFBRCxFQUFrQjtBQUV0QixRQUFJLGFBQWEsR0FBRztBQUNsQixNQUFBLEVBQUU7QUFEZ0IsS0FBcEI7QUFJQSxJQUFBLFlBQVksQ0FBQyxNQUFiLENBQXFCLGFBQXJCLEVBQW9DLENBQUMscUJBQXJDO0FBQ0QsR0FSRDtBQWFBLEVBQUEsY0FBYyxnREFBZ0QsSUFBaEQsQ0FBZCxDQUNDLElBREQsQ0FDTSxVQUFDLGFBQUQsRUFBbUI7QUFFdkIsUUFBSSxjQUFjLEdBQUc7QUFDbkIsTUFBQSxFQUFFO0FBRGlCLEtBQXJCO0FBSUEsSUFBQSxhQUFhLENBQUMsTUFBZCxDQUFzQixjQUF0QixFQUFzQyxDQUFDLHNCQUF2QztBQUNELEdBUkQ7QUFZQSxFQUFBLGNBQWMsa0RBQWtELElBQWxELENBQWQsQ0FDQyxJQURELENBQ00sVUFBQyxjQUFELEVBQW9CO0FBRXhCLFFBQUksZUFBZSxHQUFHO0FBQ3BCLE1BQUEsRUFBRTtBQURrQixLQUF0QjtBQUlBLElBQUEsY0FBYyxDQUFDLE1BQWYsQ0FBdUIsZUFBdkIsRUFBd0MsQ0FBQyx1QkFBekM7QUFDRCxHQVJEO0FBYUQsQ0FuSEQsRSxDQW1ISTs7QUFHSixNQUFNLENBQUMsZUFBUCxHQUF5QixJQUF6Qjs7QUFFQSxDQUFDLFlBQVc7QUFFVixNQUFJLE1BQU0sR0FBRyxRQUFiLENBRlUsQ0FJVjs7QUFDQSxNQUFJLE1BQU0sSUFBSSxRQUFkLEVBQ0UsUUFBUSxDQUFDLGdCQUFULENBQTBCLGtCQUExQixFQUE4QyxRQUE5QyxFQURGLEtBRUssSUFBSSxDQUFDLE1BQU0sR0FBRyxXQUFWLEtBQTBCLFFBQTlCLEVBQ0gsUUFBUSxDQUFDLGdCQUFULENBQTBCLHFCQUExQixFQUFpRCxRQUFqRCxFQURHLEtBRUEsSUFBSSxDQUFDLE1BQU0sR0FBRyxjQUFWLEtBQTZCLFFBQWpDLEVBQ0gsUUFBUSxDQUFDLGdCQUFULENBQTBCLHdCQUExQixFQUFvRCxRQUFwRCxFQURHLEtBRUEsSUFBSSxDQUFDLE1BQU0sR0FBRyxVQUFWLEtBQXlCLFFBQTdCLEVBQ0gsUUFBUSxDQUFDLGdCQUFULENBQTBCLG9CQUExQixFQUFnRCxRQUFoRCxFQURHLENBRUw7QUFGSyxPQUdBLElBQUksZUFBZSxRQUFuQixFQUNILFFBQVEsQ0FBQyxTQUFULEdBQXFCLFFBQVEsQ0FBQyxVQUFULEdBQXNCLFFBQTNDLENBREcsQ0FFTDtBQUZLLFNBSUgsTUFBTSxDQUFDLFVBQVAsR0FBb0IsTUFBTSxDQUFDLFVBQVAsR0FBbUIsTUFBTSxDQUFDLE9BQVAsR0FBaUIsTUFBTSxDQUFDLE1BQVAsR0FBZ0IsUUFBeEU7O0FBSUYsV0FBUyxRQUFULENBQW1CLEdBQW5CLEVBQXdCO0FBRXRCLFFBQUksQ0FBQyxHQUFHLFNBQVI7QUFDQSxRQUFJLENBQUMsR0FBRyxRQUFSO0FBQ0EsUUFBSSxNQUFNLEdBQUc7QUFDVCxNQUFBLEtBQUssRUFBQyxDQURHO0FBQ0EsTUFBQSxPQUFPLEVBQUMsQ0FEUjtBQUNXLE1BQUEsUUFBUSxFQUFDLENBRHBCO0FBQ3VCLE1BQUEsSUFBSSxFQUFDLENBRDVCO0FBQytCLE1BQUEsUUFBUSxFQUFDLENBRHhDO0FBQzJDLE1BQUEsUUFBUSxFQUFDO0FBRHBELEtBQWI7QUFJQSxJQUFBLEdBQUcsR0FBRyxHQUFHLElBQUksTUFBTSxDQUFDLEtBQXBCOztBQUVBLFFBQUksR0FBRyxDQUFDLElBQUosSUFBWSxNQUFoQixFQUF3QjtBQUV0QixNQUFBLE1BQU0sQ0FBQyxlQUFQLEdBQXlCLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBTCxDQUEvQixDQUZzQixDQUd0QjtBQUNBOztBQUNBLE1BQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxNQUFNLENBQUMsZUFBbkI7QUFDQSxNQUFBLGVBQWU7QUFHaEIsS0FURCxNQVVLO0FBQ0gsTUFBQSxNQUFNLENBQUMsZUFBUCxHQUEyQixNQUFNLENBQUMsZUFBUCxJQUEwQixRQUE1QixHQUF5QyxTQUF6QyxHQUFxRCxRQUE5RTtBQUNBLE1BQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxNQUFNLENBQUMsZUFBbkI7QUFFQSxNQUFBLGVBQWUsR0FKWixDQU1IO0FBQ0E7QUFDRDs7QUFBQTtBQUdGOztBQUFBLEdBckRTLENBcURQO0FBRUg7O0FBQ0EsTUFBSSxRQUFRLENBQUMsTUFBRCxDQUFSLEtBQXFCLFNBQXpCLEVBQXNDLFFBQVEsQ0FBQztBQUFDLElBQUEsSUFBSSxFQUFFLFFBQVEsQ0FBQyxNQUFELENBQVIsR0FBbUIsTUFBbkIsR0FBNEI7QUFBbkMsR0FBRCxDQUFSO0FBQ3ZDLENBekREOzs7QUNoSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDckJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNkQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3RCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDcEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCl7ZnVuY3Rpb24gcihlLG4sdCl7ZnVuY3Rpb24gbyhpLGYpe2lmKCFuW2ldKXtpZighZVtpXSl7dmFyIGM9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZTtpZighZiYmYylyZXR1cm4gYyhpLCEwKTtpZih1KXJldHVybiB1KGksITApO3ZhciBhPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIraStcIidcIik7dGhyb3cgYS5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGF9dmFyIHA9bltpXT17ZXhwb3J0czp7fX07ZVtpXVswXS5jYWxsKHAuZXhwb3J0cyxmdW5jdGlvbihyKXt2YXIgbj1lW2ldWzFdW3JdO3JldHVybiBvKG58fHIpfSxwLHAuZXhwb3J0cyxyLGUsbix0KX1yZXR1cm4gbltpXS5leHBvcnRzfWZvcih2YXIgdT1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlLGk9MDtpPHQubGVuZ3RoO2krKylvKHRbaV0pO3JldHVybiBvfXJldHVybiByfSkoKSIsIi8vIG92ZGplIHN2ZSBsaWJzIGtvamkgdHJlYmFqdSBiaXRpIGRvc3R1cG5pIGdsb2JhbG5vICEhISEhXG52YXIgaXNFdmVuID0gcmVxdWlyZSgnaXMtZXZlbicpO1xuXG4vLyBhbGVydCgndGVlZWVlZXN0Jyk7XG5cbmxvYWRfbGliKGBucG0vaXMtZXZlbmAsIGlzRXZlbik7XG5cblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xuICBcbiAgXG4gIHZhciBjaXRfbGF5b3V0X21vZHVsZV9kYXRhID0ge1xuICAgIGlkOiBgY2l0X2xheW91dF9tb2R1bGVgLFxuICAgIHRleHQ6IG51bGwsXG4gICAgbmFtZTogJ1Blcm8nLFxuICAgIGxhc3RfbmFtZTogJ1BlcmnEhycsXG4gICAgc3RyZWV0OiAnQXZlbmlqYSBEdWJyb3ZuaWsgMjMnLFxuICAgIHBsYWNlOiAnWmFncmViJyxcbiAgICBwb3N0X251bTogMTAwMDAsXG4gIH07XG5cblxuICB3YWl0X2ZvcihgJCgnI2Nsb3NlX2xlZnRfbWVudScpLmxlbmd0aCA+IDBgLCBhc3luYyBmdW5jdGlvbigpIHtcbiAgICBcbiAgICB2YXIgbGVmdF9tZW51X3N0YXRlID0gd2luZG93LmxvY2FsU3RvcmFnZS5nZXRJdGVtKCdsZWZ0X21lbnVfc3RhdGUnKTtcblxuICAgIC8vIGFrbyBqZSBtYWxpIGVrcmFuIGlsaSBha28gamUgdSBsb2NhbCBzdG9yYWdlIGNsb3NlZFxuICAgIGlmICggJCh3aW5kb3cpLndpZHRoKCkgPCA2MDAgfHwgbGVmdF9tZW51X3N0YXRlID09IFwiY2xvc2VkXCIgKSB7XG5cbiAgICAgIC8vIGkgYWtvIGxlZnQgbWVudSBuZW1hIGNpdCBoaWRlIGtsYXN1IHRqIGFrbyBqZSBvdHZvcmVuIG9uZGEgZ2EgemF0dm9yaVxuICAgICAgaWYgKCQoJyNjbG9zZV9sZWZ0X21lbnUnKS5oYXNDbGFzcygnY2l0X2hpZGUnKSA9PSBmYWxzZSkge1xuXG4gICAgICAgICQoYCNjaXRfbGVmdF9tZW51X2JveGApLmFkZENsYXNzKCdjaXRfaGlkZScpO1xuICAgICAgICAkKGAjY2l0X21haW5fYXJlYV9ib3hgKS5hZGRDbGFzcygnY2l0X3dpZGUnKTtcbiAgICAgICAgJChgI2thbGVuZGFyX2hlYWRlcmApLmFkZENsYXNzKCdjaXRfd2lkZScpO1xuXG4gICAgICAgICQoYCNjbG9zZV9sZWZ0X21lbnVgKS5hZGRDbGFzcygnY2l0X2hpZGUnKTsgIC8vc2FrcmlqIFhcbiAgICAgICAgJChgI29wZW5fbGVmdF9tZW51YCkucmVtb3ZlQ2xhc3MoJ2NpdF9oaWRlJyk7IC8vIHByaWthxb5pICAtLS0tPiBwcmlrYcW+aSBoYW1idXJnZXJcblxuICAgICAgfTtcblxuICAgIH0gXG4gICAgZWxzZSB7XG4gICAgICBcbiAgICAgIC8vIGFrbyBwcm96b3IgdmXEh2kgaSBha28gbmlqZSB1cGlzYW5vIHUgbG9jYWwgc3RvcmFnZSBkYSBqZSBjbG9zZWRcblxuICAgICAgLy8gaSB0byBzYW1vIGFrbyBqZSBjbG9zZWQgLS0tLS0+IG9uZGEgZ2Egb3R2b3JpXG4gICAgICBpZiAoICQoJyNjbG9zZV9sZWZ0X21lbnUnKS5oYXNDbGFzcygnY2l0X2hpZGUnKSA9PSB0cnVlICkge1xuXG4gICAgICAgIHdpbmRvdy5sb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnbGVmdF9tZW51X3N0YXRlJywgXCJvcGVuZWRcIik7XG5cbiAgICAgICAgJChgI2NpdF9sZWZ0X21lbnVfYm94YCkucmVtb3ZlQ2xhc3MoJ2NpdF9oaWRlJyk7XG4gICAgICAgICQoYCNjaXRfbWFpbl9hcmVhX2JveGApLnJlbW92ZUNsYXNzKCdjaXRfd2lkZScpO1xuICAgICAgICAkKGAja2FsZW5kYXJfaGVhZGVyYCkucmVtb3ZlQ2xhc3MoJ2NpdF93aWRlJyk7XG5cbiAgICAgICAgJChgI29wZW5fbGVmdF9tZW51YCkuYWRkQ2xhc3MoJ2NpdF9oaWRlJyk7IC8vIHNha3JpaiBoYW1idXJnZXJcbiAgICAgICAgJChgI2Nsb3NlX2xlZnRfbWVudWApLnJlbW92ZUNsYXNzKCdjaXRfaGlkZScpOyAvLyBwcmlrYcW+aSBYXG5cbiAgICAgIH07XG5cblxuICAgIH07XG4gIHZhciBwcmV2aWV3X21vZCA9IGF3YWl0IGdldF9jaXRfbW9kdWxlKCcvbW9kdWxlcy9wcmV2aWV3cy9jaXRfcHJldmlld3MuanMnLCBgbG9hZF9jc3NgKTtcbiAgICBcbiAgcHJldmlld19tb2QuY3JlYXRlKCB7IGlkOiAnY2l0X3ByZXZpZXdfbW9kYWwnIH0sICQoJ2JvZHknKSwgJ3ByZXBlbmQnKTsgIFxuICAgIFxuICAgIFxuICB9LCA1MCoxMDAwKTtcbiAgXG4gIFxuICAvLyBvZG1haCB1IHN0YXJ0dSBrcmVpcmFtIEhUTUwgemEgcG9wdXAgZGEgc3RvamkgdW51dGFyIGJvZHlcbiAgdmFyIHBvcF9tb2QgPSB3aW5kb3dbJy9wcmVsb2FkX21vZHVsZXMvc2l0ZV9wb3B1cF9tb2RhbC9zaXRlX3BvcHVwX21vZGFsLmpzJ107XG4gIHBvcF9tb2QuY3JlYXRlKHsgaWQ6ICdjaXRfcG9wdXBfbW9kYWwnIH0sICQoJ2JvZHknKSwgJ3ByZXBlbmQnKTtcbiAgICBcbiAgXG4gIFxuICBcbiAgLypcbiAgZ2V0X2NpdF9tb2R1bGUoYC9wcmVsb2FkX21vZHVsZXMvY2l0X2xheW91dF9tb2R1bGUuanNgLCBudWxsKVxuICAudGhlbigoY2l0X2xheW91dF9tb2R1bGUpID0+IHsgXG4gICAgY2l0X2xheW91dF9tb2R1bGUuY3JlYXRlKCBjaXRfbGF5b3V0X21vZHVsZV9kYXRhLCAkKGAjY29udF9tYWluX2JveGApICk7XG4gIH0pO1xuICBcbiAgKi9cbiAgICBcbiAgZ2V0X2NpdF9tb2R1bGUoYC9wcmVsb2FkX21vZHVsZXMvdG9wX21lbnUvY2l0X3RvcF9tZW51LmpzYCwgbnVsbClcbiAgLnRoZW4oKGNpdF90b3BfbWVudSkgPT4geyBcbiAgICBcbiAgICB2YXIgdG9wX21lbnVfZGF0YSA9IHtcbiAgICAgIGlkOiBgY2l0X3RvcF9tZW51YCxcbiAgICB9O1xuICAgIFxuICAgIGNpdF90b3BfbWVudS5jcmVhdGUoIHRvcF9tZW51X2RhdGEsICQoYCNjaXRfdG9wX21lbnVfYm94YCkgKTtcbiAgfSk7XG4gIFxuICBcbiAgXG4gIFxuICBnZXRfY2l0X21vZHVsZShgL3ByZWxvYWRfbW9kdWxlcy9sZWZ0X21lbnUvY2l0X2xlZnRfbWVudS5qc2AsIG51bGwpXG4gIC50aGVuKChjaXRfbGVmdF9tZW51KSA9PiB7IFxuICAgIFxuICAgIHZhciBsZWZ0X21lbnVfZGF0YSA9IHtcbiAgICAgIGlkOiBgY2l0X2xlZnRfbWVudWAsXG4gICAgfTtcbiAgICBcbiAgICBjaXRfbGVmdF9tZW51LmNyZWF0ZSggbGVmdF9tZW51X2RhdGEsICQoYCNjaXRfbGVmdF9tZW51X2JveGApICk7XG4gIH0pO1xuICBcbiAgXG4gIFxuICBnZXRfY2l0X21vZHVsZShgL3ByZWxvYWRfbW9kdWxlcy9yaWdodF9tZW51L2NpdF9yaWdodF9tZW51LmpzYCwgbnVsbClcbiAgLnRoZW4oKGNpdF9yaWdodF9tZW51KSA9PiB7IFxuICAgIFxuICAgIHZhciByaWdodF9tZW51X2RhdGEgPSB7XG4gICAgICBpZDogYGNpdF9yaWdodF9tZW51YCxcbiAgICB9O1xuICAgIFxuICAgIGNpdF9yaWdodF9tZW51LmNyZWF0ZSggcmlnaHRfbWVudV9kYXRhLCAkKGAjY2l0X3JpZ2h0X21lbnVfYm94YCkgKTtcbiAgfSk7XG4gIFxuICBcbiAgXG4gIFxufSk7IC8vIGVuZCBvZiBkb2MgcmFlZHlcblxuXG53aW5kb3cuY2l0X3BhZ2VfYWN0aXZlID0gbnVsbDtcblxuKGZ1bmN0aW9uKCkge1xuICBcbiAgdmFyIGhpZGRlbiA9IFwiaGlkZGVuXCI7XG5cbiAgLy8gU3RhbmRhcmRzOlxuICBpZiAoaGlkZGVuIGluIGRvY3VtZW50KVxuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJ2aXNpYmlsaXR5Y2hhbmdlXCIsIG9uY2hhbmdlKTtcbiAgZWxzZSBpZiAoKGhpZGRlbiA9IFwibW96SGlkZGVuXCIpIGluIGRvY3VtZW50KVxuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJtb3p2aXNpYmlsaXR5Y2hhbmdlXCIsIG9uY2hhbmdlKTtcbiAgZWxzZSBpZiAoKGhpZGRlbiA9IFwid2Via2l0SGlkZGVuXCIpIGluIGRvY3VtZW50KVxuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJ3ZWJraXR2aXNpYmlsaXR5Y2hhbmdlXCIsIG9uY2hhbmdlKTtcbiAgZWxzZSBpZiAoKGhpZGRlbiA9IFwibXNIaWRkZW5cIikgaW4gZG9jdW1lbnQpXG4gICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIm1zdmlzaWJpbGl0eWNoYW5nZVwiLCBvbmNoYW5nZSk7XG4gIC8vIElFIDkgYW5kIGxvd2VyOlxuICBlbHNlIGlmIChcIm9uZm9jdXNpblwiIGluIGRvY3VtZW50KVxuICAgIGRvY3VtZW50Lm9uZm9jdXNpbiA9IGRvY3VtZW50Lm9uZm9jdXNvdXQgPSBvbmNoYW5nZTtcbiAgLy8gQWxsIG90aGVyczpcbiAgZWxzZVxuICAgIHdpbmRvdy5vbnBhZ2VzaG93ID0gd2luZG93Lm9ucGFnZWhpZGU9IHdpbmRvdy5vbmZvY3VzID0gd2luZG93Lm9uYmx1ciA9IG9uY2hhbmdlO1xuXG4gIFxuICBcbiAgZnVuY3Rpb24gb25jaGFuZ2UgKGV2dCkge1xuICAgIFxuICAgIHZhciB2ID0gXCJ2aXNpYmxlXCI7XG4gICAgdmFyIGggPSBcImhpZGRlblwiO1xuICAgIHZhciBldnRNYXAgPSB7XG4gICAgICAgIGZvY3VzOnYsIGZvY3VzaW46diwgcGFnZXNob3c6diwgYmx1cjpoLCBmb2N1c291dDpoLCBwYWdlaGlkZTpoXG4gICAgICB9O1xuXG4gICAgZXZ0ID0gZXZ0IHx8IHdpbmRvdy5ldmVudDtcbiAgICBcbiAgICBpZiAoZXZ0LnR5cGUgaW4gZXZ0TWFwKSB7XG4gICAgICBcbiAgICAgIHdpbmRvdy5jaXRfcGFnZV9hY3RpdmUgPSBldnRNYXBbZXZ0LnR5cGVdO1xuICAgICAgLy8gZG9jdW1lbnQuYm9keS5jbGFzc05hbWUgPSBldnRNYXBbZXZ0LnR5cGVdO1xuICAgICAgLy8gY29uc29sZS5sb2coZXZ0TWFwW2V2dC50eXBlXSk7XG4gICAgICBjb25zb2xlLmxvZyh3aW5kb3cuY2l0X3BhZ2VfYWN0aXZlKTtcbiAgICAgIHNvY2tldF9jaGVja19pbigpO1xuICAgICAgXG4gICAgICBcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICB3aW5kb3cuY2l0X3BhZ2VfYWN0aXZlID0gKCB3aW5kb3cuY2l0X3BhZ2VfYWN0aXZlID09IFwiaGlkZGVuXCIgKSA/IFwidmlzaWJsZVwiIDogXCJoaWRkZW5cIjtcbiAgICAgIGNvbnNvbGUubG9nKHdpbmRvdy5jaXRfcGFnZV9hY3RpdmUpO1xuICAgICAgXG4gICAgICBzb2NrZXRfY2hlY2tfaW4oKTtcbiAgICAgIFxuICAgICAgLy8gY29uc29sZS5sb2coZXZ0TWFwW2V2dC50eXBlXSk7XG4gICAgICAvLyBkb2N1bWVudC5ib2R5LmNsYXNzTmFtZSA9IHRoaXNbaGlkZGVuXSA/IFwiaGlkZGVuXCIgOiBcInZpc2libGVcIjtcbiAgICB9O1xuICAgIFxuICAgIFxuICB9OyAvLyBrcmFqIGZ1bmMgb24gY2hhbmdlXG5cbiAgLy8gc2V0IHRoZSBpbml0aWFsIHN0YXRlIChidXQgb25seSBpZiBicm93c2VyIHN1cHBvcnRzIHRoZSBQYWdlIFZpc2liaWxpdHkgQVBJKVxuICBpZiggZG9jdW1lbnRbaGlkZGVuXSAhPT0gdW5kZWZpbmVkICkgIG9uY2hhbmdlKHt0eXBlOiBkb2N1bWVudFtoaWRkZW5dID8gXCJibHVyXCIgOiBcImZvY3VzXCJ9KTtcbn0pKCk7XG5cbiIsIi8qIVxuICogRGV0ZXJtaW5lIGlmIGFuIG9iamVjdCBpcyBhIEJ1ZmZlclxuICpcbiAqIEBhdXRob3IgICBGZXJvc3MgQWJvdWtoYWRpamVoIDxodHRwczovL2Zlcm9zcy5vcmc+XG4gKiBAbGljZW5zZSAgTUlUXG4gKi9cblxuLy8gVGhlIF9pc0J1ZmZlciBjaGVjayBpcyBmb3IgU2FmYXJpIDUtNyBzdXBwb3J0LCBiZWNhdXNlIGl0J3MgbWlzc2luZ1xuLy8gT2JqZWN0LnByb3RvdHlwZS5jb25zdHJ1Y3Rvci4gUmVtb3ZlIHRoaXMgZXZlbnR1YWxseVxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAob2JqKSB7XG4gIHJldHVybiBvYmogIT0gbnVsbCAmJiAoaXNCdWZmZXIob2JqKSB8fCBpc1Nsb3dCdWZmZXIob2JqKSB8fCAhIW9iai5faXNCdWZmZXIpXG59XG5cbmZ1bmN0aW9uIGlzQnVmZmVyIChvYmopIHtcbiAgcmV0dXJuICEhb2JqLmNvbnN0cnVjdG9yICYmIHR5cGVvZiBvYmouY29uc3RydWN0b3IuaXNCdWZmZXIgPT09ICdmdW5jdGlvbicgJiYgb2JqLmNvbnN0cnVjdG9yLmlzQnVmZmVyKG9iailcbn1cblxuLy8gRm9yIE5vZGUgdjAuMTAgc3VwcG9ydC4gUmVtb3ZlIHRoaXMgZXZlbnR1YWxseS5cbmZ1bmN0aW9uIGlzU2xvd0J1ZmZlciAob2JqKSB7XG4gIHJldHVybiB0eXBlb2Ygb2JqLnJlYWRGbG9hdExFID09PSAnZnVuY3Rpb24nICYmIHR5cGVvZiBvYmouc2xpY2UgPT09ICdmdW5jdGlvbicgJiYgaXNCdWZmZXIob2JqLnNsaWNlKDAsIDApKVxufVxuIiwiLyohXG4gKiBpcy1ldmVuIDxodHRwczovL2dpdGh1Yi5jb20vam9uc2NobGlua2VydC9pcy1ldmVuPlxuICpcbiAqIENvcHlyaWdodCAoYykgMjAxNSwgMjAxNywgSm9uIFNjaGxpbmtlcnQuXG4gKiBSZWxlYXNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuXG4gKi9cblxuJ3VzZSBzdHJpY3QnO1xuXG52YXIgaXNPZGQgPSByZXF1aXJlKCdpcy1vZGQnKTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBpc0V2ZW4oaSkge1xuICByZXR1cm4gIWlzT2RkKGkpO1xufTtcbiIsIi8qIVxuICogaXMtbnVtYmVyIDxodHRwczovL2dpdGh1Yi5jb20vam9uc2NobGlua2VydC9pcy1udW1iZXI+XG4gKlxuICogQ29weXJpZ2h0IChjKSAyMDE0LTIwMTUsIEpvbiBTY2hsaW5rZXJ0LlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLlxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIHR5cGVPZiA9IHJlcXVpcmUoJ2tpbmQtb2YnKTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBpc051bWJlcihudW0pIHtcbiAgdmFyIHR5cGUgPSB0eXBlT2YobnVtKTtcblxuICBpZiAodHlwZSA9PT0gJ3N0cmluZycpIHtcbiAgICBpZiAoIW51bS50cmltKCkpIHJldHVybiBmYWxzZTtcbiAgfSBlbHNlIGlmICh0eXBlICE9PSAnbnVtYmVyJykge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIHJldHVybiAobnVtIC0gbnVtICsgMSkgPj0gMDtcbn07XG4iLCIvKiFcbiAqIGlzLW9kZCA8aHR0cHM6Ly9naXRodWIuY29tL2pvbnNjaGxpbmtlcnQvaXMtb2RkPlxuICpcbiAqIENvcHlyaWdodCAoYykgMjAxNS0yMDE3LCBKb24gU2NobGlua2VydC5cbiAqIFJlbGVhc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS5cbiAqL1xuXG4ndXNlIHN0cmljdCc7XG5cbnZhciBpc051bWJlciA9IHJlcXVpcmUoJ2lzLW51bWJlcicpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGlzT2RkKGkpIHtcbiAgaWYgKCFpc051bWJlcihpKSkge1xuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ2lzLW9kZCBleHBlY3RzIGEgbnVtYmVyLicpO1xuICB9XG4gIGlmIChOdW1iZXIoaSkgIT09IE1hdGguZmxvb3IoaSkpIHtcbiAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcignaXMtb2RkIGV4cGVjdHMgYW4gaW50ZWdlci4nKTtcbiAgfVxuICByZXR1cm4gISEofn5pICYgMSk7XG59O1xuIiwidmFyIGlzQnVmZmVyID0gcmVxdWlyZSgnaXMtYnVmZmVyJyk7XG52YXIgdG9TdHJpbmcgPSBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nO1xuXG4vKipcbiAqIEdldCB0aGUgbmF0aXZlIGB0eXBlb2ZgIGEgdmFsdWUuXG4gKlxuICogQHBhcmFtICB7Kn0gYHZhbGBcbiAqIEByZXR1cm4geyp9IE5hdGl2ZSBqYXZhc2NyaXB0IHR5cGVcbiAqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGtpbmRPZih2YWwpIHtcbiAgLy8gcHJpbWl0aXZpZXNcbiAgaWYgKHR5cGVvZiB2YWwgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgcmV0dXJuICd1bmRlZmluZWQnO1xuICB9XG4gIGlmICh2YWwgPT09IG51bGwpIHtcbiAgICByZXR1cm4gJ251bGwnO1xuICB9XG4gIGlmICh2YWwgPT09IHRydWUgfHwgdmFsID09PSBmYWxzZSB8fCB2YWwgaW5zdGFuY2VvZiBCb29sZWFuKSB7XG4gICAgcmV0dXJuICdib29sZWFuJztcbiAgfVxuICBpZiAodHlwZW9mIHZhbCA9PT0gJ3N0cmluZycgfHwgdmFsIGluc3RhbmNlb2YgU3RyaW5nKSB7XG4gICAgcmV0dXJuICdzdHJpbmcnO1xuICB9XG4gIGlmICh0eXBlb2YgdmFsID09PSAnbnVtYmVyJyB8fCB2YWwgaW5zdGFuY2VvZiBOdW1iZXIpIHtcbiAgICByZXR1cm4gJ251bWJlcic7XG4gIH1cblxuICAvLyBmdW5jdGlvbnNcbiAgaWYgKHR5cGVvZiB2YWwgPT09ICdmdW5jdGlvbicgfHwgdmFsIGluc3RhbmNlb2YgRnVuY3Rpb24pIHtcbiAgICByZXR1cm4gJ2Z1bmN0aW9uJztcbiAgfVxuXG4gIC8vIGFycmF5XG4gIGlmICh0eXBlb2YgQXJyYXkuaXNBcnJheSAhPT0gJ3VuZGVmaW5lZCcgJiYgQXJyYXkuaXNBcnJheSh2YWwpKSB7XG4gICAgcmV0dXJuICdhcnJheSc7XG4gIH1cblxuICAvLyBjaGVjayBmb3IgaW5zdGFuY2VzIG9mIFJlZ0V4cCBhbmQgRGF0ZSBiZWZvcmUgY2FsbGluZyBgdG9TdHJpbmdgXG4gIGlmICh2YWwgaW5zdGFuY2VvZiBSZWdFeHApIHtcbiAgICByZXR1cm4gJ3JlZ2V4cCc7XG4gIH1cbiAgaWYgKHZhbCBpbnN0YW5jZW9mIERhdGUpIHtcbiAgICByZXR1cm4gJ2RhdGUnO1xuICB9XG5cbiAgLy8gb3RoZXIgb2JqZWN0c1xuICB2YXIgdHlwZSA9IHRvU3RyaW5nLmNhbGwodmFsKTtcblxuICBpZiAodHlwZSA9PT0gJ1tvYmplY3QgUmVnRXhwXScpIHtcbiAgICByZXR1cm4gJ3JlZ2V4cCc7XG4gIH1cbiAgaWYgKHR5cGUgPT09ICdbb2JqZWN0IERhdGVdJykge1xuICAgIHJldHVybiAnZGF0ZSc7XG4gIH1cbiAgaWYgKHR5cGUgPT09ICdbb2JqZWN0IEFyZ3VtZW50c10nKSB7XG4gICAgcmV0dXJuICdhcmd1bWVudHMnO1xuICB9XG4gIGlmICh0eXBlID09PSAnW29iamVjdCBFcnJvcl0nKSB7XG4gICAgcmV0dXJuICdlcnJvcic7XG4gIH1cblxuICAvLyBidWZmZXJcbiAgaWYgKGlzQnVmZmVyKHZhbCkpIHtcbiAgICByZXR1cm4gJ2J1ZmZlcic7XG4gIH1cblxuICAvLyBlczY6IE1hcCwgV2Vha01hcCwgU2V0LCBXZWFrU2V0XG4gIGlmICh0eXBlID09PSAnW29iamVjdCBTZXRdJykge1xuICAgIHJldHVybiAnc2V0JztcbiAgfVxuICBpZiAodHlwZSA9PT0gJ1tvYmplY3QgV2Vha1NldF0nKSB7XG4gICAgcmV0dXJuICd3ZWFrc2V0JztcbiAgfVxuICBpZiAodHlwZSA9PT0gJ1tvYmplY3QgTWFwXScpIHtcbiAgICByZXR1cm4gJ21hcCc7XG4gIH1cbiAgaWYgKHR5cGUgPT09ICdbb2JqZWN0IFdlYWtNYXBdJykge1xuICAgIHJldHVybiAnd2Vha21hcCc7XG4gIH1cbiAgaWYgKHR5cGUgPT09ICdbb2JqZWN0IFN5bWJvbF0nKSB7XG4gICAgcmV0dXJuICdzeW1ib2wnO1xuICB9XG5cbiAgLy8gdHlwZWQgYXJyYXlzXG4gIGlmICh0eXBlID09PSAnW29iamVjdCBJbnQ4QXJyYXldJykge1xuICAgIHJldHVybiAnaW50OGFycmF5JztcbiAgfVxuICBpZiAodHlwZSA9PT0gJ1tvYmplY3QgVWludDhBcnJheV0nKSB7XG4gICAgcmV0dXJuICd1aW50OGFycmF5JztcbiAgfVxuICBpZiAodHlwZSA9PT0gJ1tvYmplY3QgVWludDhDbGFtcGVkQXJyYXldJykge1xuICAgIHJldHVybiAndWludDhjbGFtcGVkYXJyYXknO1xuICB9XG4gIGlmICh0eXBlID09PSAnW29iamVjdCBJbnQxNkFycmF5XScpIHtcbiAgICByZXR1cm4gJ2ludDE2YXJyYXknO1xuICB9XG4gIGlmICh0eXBlID09PSAnW29iamVjdCBVaW50MTZBcnJheV0nKSB7XG4gICAgcmV0dXJuICd1aW50MTZhcnJheSc7XG4gIH1cbiAgaWYgKHR5cGUgPT09ICdbb2JqZWN0IEludDMyQXJyYXldJykge1xuICAgIHJldHVybiAnaW50MzJhcnJheSc7XG4gIH1cbiAgaWYgKHR5cGUgPT09ICdbb2JqZWN0IFVpbnQzMkFycmF5XScpIHtcbiAgICByZXR1cm4gJ3VpbnQzMmFycmF5JztcbiAgfVxuICBpZiAodHlwZSA9PT0gJ1tvYmplY3QgRmxvYXQzMkFycmF5XScpIHtcbiAgICByZXR1cm4gJ2Zsb2F0MzJhcnJheSc7XG4gIH1cbiAgaWYgKHR5cGUgPT09ICdbb2JqZWN0IEZsb2F0NjRBcnJheV0nKSB7XG4gICAgcmV0dXJuICdmbG9hdDY0YXJyYXknO1xuICB9XG5cbiAgLy8gbXVzdCBiZSBhIHBsYWluIG9iamVjdFxuICByZXR1cm4gJ29iamVjdCc7XG59O1xuIl19
