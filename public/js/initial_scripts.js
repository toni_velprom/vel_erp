// ovdje sve libs koji trebaju biti dostupni globalno !!!!!
var isEven = require('is-even');

// alert('teeeeeest');

load_lib(`npm/is-even`, isEven);


$(document).ready(function () {
  
  
  var cit_layout_module_data = {
    id: `cit_layout_module`,
    text: null,
    name: 'Pero',
    last_name: 'Perić',
    street: 'Avenija Dubrovnik 23',
    place: 'Zagreb',
    post_num: 10000,
  };


  wait_for(`$('#close_left_menu').length > 0`, async function() {
    
    var left_menu_state = window.localStorage.getItem('left_menu_state');

    // ako je mali ekran ili ako je u local storage closed
    if ( $(window).width() < 600 || left_menu_state == "closed" ) {

      // i ako left menu nema cit hide klasu tj ako je otvoren onda ga zatvori
      if ($('#close_left_menu').hasClass('cit_hide') == false) {

        $(`#cit_left_menu_box`).addClass('cit_hide');
        $(`#cit_main_area_box`).addClass('cit_wide');
        $(`#kalendar_header`).addClass('cit_wide');

        $(`#close_left_menu`).addClass('cit_hide');  //sakrij X
        $(`#open_left_menu`).removeClass('cit_hide'); // prikaži  ----> prikaži hamburger

      };

    } 
    else {
      
      // ako prozor veći i ako nije upisano u local storage da je closed

      // i to samo ako je closed -----> onda ga otvori
      if ( $('#close_left_menu').hasClass('cit_hide') == true ) {

        window.localStorage.setItem('left_menu_state', "opened");

        $(`#cit_left_menu_box`).removeClass('cit_hide');
        $(`#cit_main_area_box`).removeClass('cit_wide');
        $(`#kalendar_header`).removeClass('cit_wide');

        $(`#open_left_menu`).addClass('cit_hide'); // sakrij hamburger
        $(`#close_left_menu`).removeClass('cit_hide'); // prikaži X

      };


    };
  var preview_mod = await get_cit_module('/modules/previews/cit_previews.js', `load_css`);
    
  preview_mod.create( { id: 'cit_preview_modal' }, $('body'), 'prepend');  
    
    
  }, 50*1000);
  
  
  // odmah u startu kreiram HTML za popup da stoji unutar body
  var pop_mod = window['/preload_modules/site_popup_modal/site_popup_modal.js'];
  pop_mod.create({ id: 'cit_popup_modal' }, $('body'), 'prepend');
    
  
  
  
  /*
  get_cit_module(`/preload_modules/cit_layout_module.js`, null)
  .then((cit_layout_module) => { 
    cit_layout_module.create( cit_layout_module_data, $(`#cont_main_box`) );
  });
  
  */
    
  get_cit_module(`/preload_modules/top_menu/cit_top_menu.js`, null)
  .then((cit_top_menu) => { 
    
    var top_menu_data = {
      id: `cit_top_menu`,
    };
    
    cit_top_menu.create( top_menu_data, $(`#cit_top_menu_box`) );
  });
  
  
  
  
  get_cit_module(`/preload_modules/left_menu/cit_left_menu.js`, null)
  .then((cit_left_menu) => { 
    
    var left_menu_data = {
      id: `cit_left_menu`,
    };
    
    cit_left_menu.create( left_menu_data, $(`#cit_left_menu_box`) );
  });
  
  
  
  get_cit_module(`/preload_modules/right_menu/cit_right_menu.js`, null)
  .then((cit_right_menu) => { 
    
    var right_menu_data = {
      id: `cit_right_menu`,
    };
    
    cit_right_menu.create( right_menu_data, $(`#cit_right_menu_box`) );
  });
  
  
  
  
}); // end of doc raedy


window.cit_page_active = null;

(function() {
  
  var hidden = "hidden";

  // Standards:
  if (hidden in document)
    document.addEventListener("visibilitychange", onchange);
  else if ((hidden = "mozHidden") in document)
    document.addEventListener("mozvisibilitychange", onchange);
  else if ((hidden = "webkitHidden") in document)
    document.addEventListener("webkitvisibilitychange", onchange);
  else if ((hidden = "msHidden") in document)
    document.addEventListener("msvisibilitychange", onchange);
  // IE 9 and lower:
  else if ("onfocusin" in document)
    document.onfocusin = document.onfocusout = onchange;
  // All others:
  else
    window.onpageshow = window.onpagehide= window.onfocus = window.onblur = onchange;

  
  
  function onchange (evt) {
    
    var v = "visible";
    var h = "hidden";
    var evtMap = {
        focus:v, focusin:v, pageshow:v, blur:h, focusout:h, pagehide:h
      };

    evt = evt || window.event;
    
    if (evt.type in evtMap) {
      
      window.cit_page_active = evtMap[evt.type];
      // document.body.className = evtMap[evt.type];
      // console.log(evtMap[evt.type]);
      console.log(window.cit_page_active);
      socket_check_in();
      
      
    }
    else {
      window.cit_page_active = ( window.cit_page_active == "hidden" ) ? "visible" : "hidden";
      console.log(window.cit_page_active);
      
      socket_check_in();
      
      // console.log(evtMap[evt.type]);
      // document.body.className = this[hidden] ? "hidden" : "visible";
    };
    
    
  }; // kraj func on change

  // set the initial state (but only if browser supports the Page Visibility API)
  if( document[hidden] !== undefined )  onchange({type: document[hidden] ? "blur" : "focus"});
})();

