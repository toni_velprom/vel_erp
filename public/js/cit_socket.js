
console.log('SPOJEN NA SOCKET: ', cit_socket.connected);

cit_socket.on('connect', function() {
  
  console.log('SPOJEN NA SOCKET: ', cit_socket.connected);
  console.log(cit_socket.id);
});


function animate_badge(badge, num) {

  badge.css(`transform`, `scale(1.2)`);
  var value_box = badge.find(`.badge_value`);
  
  var badge_count = Number( value_box.text() ) || 0;
  setTimeout( function() { value_box.text(badge_count + num); }, 150);
  setTimeout( function() { badge.css(`transform`, ``); }, 300);
  
  // nemoj pustiti zvuk za my tasks jer je već pušten zvuk za NEW tasks !!!
  if ( badge[0].id !== "cit_my_tasks" ) cit_play(badge[0].id);
  
};


cit_socket.on('check_in', function(socket_id) {
  window.cit_socket_id = socket_id;
  console.log(`Server vratio socket_id: ${socket_id}`);
});


cit_socket.on('new_status', function(status) {
 
  cit_toast(`Novi status od ${status.from?.full_name || "" }`);
  
  /* animate_badge( $(`#cit_new`), 1 ); */
  animate_badge( $(`#cit_my_tasks`), 1 );
  
  console.log(status);
    
});




cit_socket.on('reply_status', function(status) {
 
  cit_toast(`ODGOVOR na status od ${status.from?.full_name || "" }`);
  
  /* animate_badge( $(`#cit_new`), 1 ); */
  animate_badge( $(`#cit_task_from_me`), -1 );
  
  console.log(status);
    
});




setInterval( function() {
  
  socket_check_in();
  
}, 5*1000 );
