var style_html_doc =

` 
<style id="main_style"> 

html {
  /*zoom: 0.74;*/
  margin: 0;
  padding: 0;
}

@font-face {
  font-family: 'Open Sans';
  src:  url('../../open-sans-v15-latin-ext_latin-700.woff2') format('woff2'), 
        url('../../open-sans-v15-latin-ext_latin-700.woff') format('woff'), 
        url('../../open-sans-v15-latin-ext_latin-700.ttf') format('truetype');
  
  font-weight: 700;
  font-style: normal;
}

@font-face {
  font-family: 'Open Sans';
  src:  url('../../open-sans-v15-latin-ext_latin-regular.woff2') format('woff2'), 
        url('../../open-sans-v15-latin-ext_latin-regular.woff') format('woff'), 
        url('../../open-sans-v15-latin-ext_latin-regular.ttf') format('truetype');
  font-weight: 400;
  font-style: normal;
}

body {
  margin: 0;
  padding: 0;
  width: 100%;
  height: 100%;
}

#measurement {
  width: 100mm;
  height: 100mm;
  pointer-events: none;
  opacity: 0;
  position: absolute;
  z-index: 99;
  top: 0;
  left: 0;
}

.page {
  line-height: 14pt;
  font-family: 'Open Sans';
  width: 100%;
  padding: 0 2px;
  box-sizing: border-box;
  background-color: #ffffff;
  font-size: 10pt;
  border: 0px solid #000;
  color: #3a3a3a;
}

.preview_table {
  width: 100%;
}

.header,
.header-space {
  width: 100%;
  height: 37mm;
  padding: 0;
  box-sizing: border-box;
  line-height: 14pt;
  font-family: 'Open Sans';
  background-color: #ffffff;
  font-size: 10pt;
}

.footer,
.footer-space {
  width: 100%;
  height: 20mm;
  padding: 0;
  box-sizing: border-box;
  line-height: 14pt;
  font-family: 'Open Sans';
  background-color: #ffffff;
  font-size: 9pt;
}

.header {
  position: fixed;
  top: 0;
  border-bottom: 2px solid #e9e8e8;
}

.footer {
  position: fixed;
  bottom: 0;
  border-top: 2px solid #e9e8e8;
  text-align: center;
  font-size: 9pt;
  page-break-after: always;
}

.logo {
  position: absolute;
  bottom: 0;
  margin-bottom: 2mm;
  width: 260px;
  height: 65px;
}

.generalije {
  float: right;
  width: 50%;
  height: 100%;
  font-size: 10pt;
  color: #3a3a3a;
  text-align: right;
  box-sizing: border-box;
  padding-top: 0;
  margin-top: 1mm;
}

.mjesto_vrijeme_dokumenta {
  width: 100%;
  height: 9mm;
  padding-top: 1mm;
  box-sizing: border-box;
  text-align: right;
}

.vrsta_i_broj_dokumenta {
  width: 100%;
  height: auto;
  box-sizing: border-box;
  text-align: right;
  font-size: 12pt;
  line-height: 26pt;
  font-weight: 700;
  color: #6b8fb5;
}

.broj_dokumenta {
  float: right;
  width: auto;
  margin-left: 8pt;
  text-align: right;
  font-size: 12pt;
}

.podaci_dokumenta_i_primatelja {
  width: 100%;
  height: auto;
  box-sizing: border-box;
  overflow: hidden;
}

.podaci_dokumenta {
  float: left;
  width: 60%;
  text-align: left;
  margin-top: 2mm;
  font-weight: 700;
  font-size: 9pt; 
  line-height: 15pt;
}

.podaci_primatelja {
  float: right;
  width: 40%;
  text-align: right;
  font-weight: 700;
}

.doc_pay {
  width: 100%;
  clear: both;
  font-size: 9pt; 
  line-height: 15pt;
  font-weight: 700;  
}

.doc_pay .def {
  
  width: 99%;
  clear: both;
  display: block;
  font-weight: 700;
  font-size: 8pt;
  border: 1pt solid #ccc;
  margin-top: 1pt;
  height: auto;
  overflow: hidden;
  
}

.doc_pay .def > div {
  
  float: left;
  font-weight: 700;
  
}

.sekcija_proizvoda {
  /*
  border-bottom: 1px solid #d9d9d9;
  width: 100%;
  margin-bottom: 4mm;
  float: left;
  clear: both;
*/
}

.slike_proizvoda {
  box-sizing: border-box;
  width: 30%;
  float: left;
  min-height: 10mm;
}

.slike_proizvoda img {
  width: 100%;
  height: auto;
  margin-bottom: 0;
}

.text_proizvoda {
  box-sizing: border-box;
  padding-left: 5mm;
  width: 70%;
  float: right;
  min-height: 10mm;
  margin-bottom: 3mm;
}


/* /////////////// START /////////////////// TABLICA  ////////////////////////////////// */

.table {
  width: 100%;
  height: auto;
  border: 1px solid #000;
  border-bottom: 1px solid #000;
  margin-bottom: 3mm;
  float: left;
  overflow: hidden;
  padding-bottom: 3mm;
  box-sizing: border-box;
}

.table_header {
  margin-top: 5mm;
  width: 100%;
  font-size: 10pt;
  font-weight: 700;
  box-sizing: border-box;
  border-top: 1px solid #d9d9d9;
  border-bottom: 2px solid #d9d9d9;
  float: left;
  overflow: hidden;
  height: auto;
  display: table;
}

.celija {
  box-sizing: border-box;
  padding: 0 3px;
  border-right: 1px solid #d9d9d9;
  /* float: left; */
  font-size: 10pt;
  font-weight: 400;
  line-height: 12pt;
  /*  
  display: flex;
  align-items: flex-end;
  */
  display: table-cell;
  vertical-align: bottom;
}

.celija.izmjera_podatak {
  text-transform: uppercase;
  position: relative;
}

.no_blue_line {
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -webkit-tap-highlight-color: transparent;
  outline-width: 0;
}

.izmjera_podatak {
  font-size: 10pt;
  vertical-align: top;
  padding: 6px;
}

.izm_label {
  font-size: 12px;
  font-weight: 700;
  vertical-align: middle;
  background-color: #e5e5e5;
  padding: 6px;
}

@media print {
  .izm_label {
    background-color: transparent;
  }
}

.text_right {
  /*justify-content: flex-end;*/
  text-align: right;
}

.left_border {
  border-left: 1px solid #d9d9d9;
}

.table_header .celija {
  height: 6mm;
  line-height: normal;
  font-size: 10pt;
  font-weight: 700;
  text-align: left;
}

.table_row {
  float: left;
  overflow: hidden;
  height: auto;
  width: 100%;
  clear: both;
  border-bottom: 1px solid #d9d9d9;
  box-sizing: border-box;
  /* display: flex; */
  display: table;
  vertical-align: bottom;
}

.table_row.za_izmjeru {
  min-height: 25px;
}

.table_row.no_border {
  border-bottom: 0px solid transparent;
}

.table_row.no_border .celija {
  border-right: 0px solid transparent;
}

.table_row.bold_text .celija {
  font-weight: 700;
}

#kraj_tablice,
#zadnji_text {
  width: 100%;
  height: auto;
  overflow: hidden;
  float: left;
  clear: both;
}

#print_napomena {
  width: 100%;
  height: auto;
  overflow: hidden;
  float: left;
  clear: both;
  padding-top: 5mm;
}

.img-responsive {
  display: block;
  max-width: 100%;
  height: auto;
}

div.greyGridTable {
  border: 2px solid #FFFFFF;
  width: 100%;
  text-align: right;
  border-collapse: collapse;
}

.divTable.greyGridTable .divTableCell,
.divTable.greyGridTable .divTableHead {
  border: 1px solid #FFFFFF;
  padding: 3px 4px;
}

.divTable.greyGridTable .divTableBody .divTableCell {
  font-size: 9pt;
}

.divTable.greyGridTable .divTableRow:nth-child(even) {
  background: #D0E4F5;
}

.divTable.greyGridTable .divTableHeading {
  background: #FFFFFF;
  border-bottom: 2px solid #6A6A6A;
}

.divTable.greyGridTable .divTableHeading .divTableHead {
  font-size: 9pt;
  font-weight: bold;
  color: #333333;
  text-align: center;
  border-left: 1px solid #333333;
}

.divTable.greyGridTable .divTableHeading .divTableHead:first-child {
  border-left: none;
}

.greyGridTable .tableFootStyle {
  font-size: 10pt;
  font-weight: bold;
  color: #333333;
  border-top: 2px solid #333333;
}

.greyGridTable .tableFootStyle {
  font-size: 10pt;
}


/* DivTable.com */

.divTable {
  display: table;
}

.divTableRow {
  display: table-row;
}

.divTableHeading {
  display: table-header-group;
}

.divTableCell,
.divTableHead {
  display: table-cell;
}

.divTableHeading {
  display: table-header-group;
}

.divTableFoot {
  display: table-footer-group;
}

.divTableBody {
  display: table-row-group;
}



.divTable.greyGridTable.title_location_row .divTableBody .divTableCell {
  font-size: 10px;
  font-weight: 700;
  text-align: center !important;
  padding: 0;
}


.divTable.greyGridTable.location_row .divTableBody .divTableCell {
  border: none;
}


.palet_sifra {
  width: 100%;
  text-align: center;
  background: black;
  color: #fff;
  border-radius: 3mm;
  font-size: 94px;
  letter-spacing: 0.9rem;
  line-height: 100px;
  font-weight: 700;
  margin-bottom: 5mm;
  
}


.palet_kolicina {
  width: 100%;
  text-align: right;
  display: flex;
  justify-content: flex-start;
  
  font-size: 36px;
  display: flex;
  font-weight: 700;
  line-height: 40px;
}


.palet_kolicina .jedinica_kolicine_small {
  font-size: 20px;
  margin-left: 3mm
}


.location_name {

  font-size: 24px;
  display: flex;
  font-weight: 700;
  line-height: 32px;
  width: 100%;

}


@media print {
  .cit_break_page {
    page-break-after: always;
  }
}





</style > 


`;