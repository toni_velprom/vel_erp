const cit_upload = function (fileEle, backendUrl) {
  return new Promise(function (resolve, reject) {
    // Get the list of selected files
    const files = fileEle.files;

    // Create a new FormData
    const formData = new FormData();

    // Loop over the files
        [].forEach.call(files, function (file) {
      formData.append(fileEle.name, file, file.name);
    });

    // Create new Ajax request
    const req = new XMLHttpRequest();
    req.open('POST', backendUrl, true);

    // Handle the events
    req.onload = function () {
      if (req.status >= 200 && req.status < 400) {
        resolve(req.responseText);
      }
    };
    req.onerror = function () {
      reject();
    };

    // Send it
    req.send(formData);
  });
};


/// USAGE !!!!

/*

const fileEle = document.getElementById('upload');

cit_upload(fileEle, '/path/to/back-end').then(function(response) {
    // `response` is what we got from the back-end
    // We can parse it if the server returns a JSON
    const data = JSON.parse(response);
    ...
});

*/

