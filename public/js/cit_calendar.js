
var current_simple_calendar_date = null;
var current_simple_calendar_start = Date.now();


function create_simple_drop_calendar(date_arg) {

  var this_start_time = date_arg || Date.now();  
  var today = new Date(this_start_time);
    
  var dd = today.getDate();
  var mm = today.getMonth();
  var yyyy = today.getFullYear();
  
  var end_date = new Date(yyyy, mm+1, dd);
  var end_date_ms = Number(end_date);
  
  var dates_and_days_html = ``;  
  var d;
  var prev_date = dd-1;
    
  for(d=1; d<=1000; d++ ) {

    var date = new Date(yyyy, mm, dd-10);
    var date_ms = Number(date);
    

    if ( end_date_ms >= date_ms ) {
      var day = date.getDay();
      var day_string = [ 'ned', 'pon', 'uto', 'sri', 'cet', 'pet', 'sub'];
      day = day_string[day];
    
      dates_and_days_html +=
      `
      <div class="kalendar_date_day cit_${day}">
        <div class="kalendar_date">${ cit_dt(date_ms).date }</div>
        <div class="kalendar_day">${day.replace(`c`, `č`).toUpperCase() }</div>
      </div>

      `;      
        
      dd +=1;
      
    }; // kraj end date veći ili jednak trentnom
    
  }; // end for loop
  
  var cal_html = 
`

<div id="simple_calendar_header">
 
  <div class="simple_cal_select_box">
    <div id="simple_month_minus"><i class="fas fa-caret-left"></i></div>
    <input id="simple_month_display" class="cit_input" autocomplete="off" type="text" value="${mm+1}">
    <div id="simple_month_plus"><i class="fas fa-caret-right"></i></div>
  </div>
  
  <div class="simple_cal_select_box">
    <div id="simple_year_minus"><i class="fas fa-caret-left"></i></div>
    <input id="simple_year_display" class="cit_input" autocomplete="off" type="text" value="${yyyy}">
    <div id="simple_year_plus"><i class="fas fa-caret-right"></i></div>
  </div>  
  
</div>

<div id="drop_list_dates">
  ${dates_and_days_html}
</div>
<div id="drop_list_hh">
  <div class="drop_item_hh">00</div>
  <div class="drop_item_hh">01</div>
  <div class="drop_item_hh">02</div>
  <div class="drop_item_hh">03</div>
  <div class="drop_item_hh">04</div>
  <div class="drop_item_hh">05</div>
  <div class="drop_item_hh">06</div>
  <div class="drop_item_hh">07</div>
  <div class="drop_item_hh">08</div>
  <div class="drop_item_hh">09</div>
  <div class="drop_item_hh">10</div>
  <div class="drop_item_hh">11</div>
  <div class="drop_item_hh">12</div>
  <div class="drop_item_hh">13</div>
  <div class="drop_item_hh">14</div>
  <div class="drop_item_hh">15</div>
  <div class="drop_item_hh">16</div>
  <div class="drop_item_hh">17</div>
  <div class="drop_item_hh">18</div>
  <div class="drop_item_hh">19</div>
  <div class="drop_item_hh">20</div>
  <div class="drop_item_hh">21</div>
  <div class="drop_item_hh">22</div>
  <div class="drop_item_hh">23</div>
</div>
<div id="drop_list_min">
  <div class="drop_item_min">00</div>
  <div class="drop_item_min">05</div>
  <div class="drop_item_min">10</div>
  <div class="drop_item_min">15</div>
  <div class="drop_item_min">20</div>
  <div class="drop_item_min">25</div>
  <div class="drop_item_min">30</div>
  <div class="drop_item_min">35</div>
  <div class="drop_item_min">40</div>
  <div class="drop_item_min">45</div>
  <div class="drop_item_min">50</div>
  <div class="drop_item_min">55</div>
  
</div>

`;  
    
  setTimeout(function() {
    
    register_simple_cal_events();
    
  }, 50);
    
    
  return cal_html;
    
};


function select_active_cal_polja() {
  
  var this_cal_date = $('#'+current_input_id).data(`cal_date`);
  var this_cal_hh = $('#'+current_input_id).data(`cal_hh`);
  var this_cal_min = $('#'+current_input_id).data(`cal_min`);
  
// loop po svim datumima u drop listi i ako nađem koji je u podacima ofarbaj ga  u crvenu boju tj daj mu class active
  $('#search_autocomplete_container .kalendar_date').each( function() {
    if ( this_cal_date && $(this).text().trim() == this_cal_date.trim() ) {
      $('#search_autocomplete_container .kalendar_date_day').removeClass(`cit_active`);
      $(this).parent().addClass(`cit_active`);
    };
  });
  
  $('#search_autocomplete_container .drop_item_hh').each( function() {
    if ( this_cal_hh && $(this).text().trim() == this_cal_hh.trim() ) {
      $('#search_autocomplete_container .drop_item_hh').removeClass(`cit_active`);
      $(this).addClass(`cit_active`);
    };
  });

  $('#search_autocomplete_container .drop_item_min').each( function() {
    if ( this_cal_min && $(this).text().trim() == this_cal_min.trim() ) {
      $('#search_autocomplete_container .drop_item_min').removeClass(`cit_active`);
      $(this).addClass(`cit_active`);
    };
  });  
  
};


function update_simple_calendar_data_attr(cal_input, new_date_ms) {
  
    var input_format = `date_time`;
    if ( cal_input.hasClass(`date`) ) input_format = `date`;
    if ( cal_input.hasClass(`time`) ) input_format = `time`;

    var cal_date = null;
    var cal_hh = null;
    var cal_min = null;

    if ( input_format == `date_time` || input_format == `date` ) cal_date = cit_dt(new_date_ms).date;

    if ( input_format == `date_time` || input_format == `time` ) {
      cal_hh = cit_dt(new_date_ms).time.split(`:`)[0];
      cal_min = cit_dt(new_date_ms).time.split(`:`)[1];
    };


    cal_input.data(`cal_date`, cal_date);
    cal_input.data(`cal_hh`, cal_hh);
    cal_input.data(`cal_min`, cal_min);  
  
};


function update_simple_calendar_date(this_input) {
  
  // prvo provjeri jel ovaj input ima data cal date
  var this_cal_date = this_input.data(`cal_date`);
  
  // ako nema data cal date ONDA NAPRAVI date iz current_simple_calendar_date ( koji može biti i null )
  if (!this_cal_date ) {
    this_cal_date = cit_dt(current_simple_calendar_date).date;
    this_input.data(`cal_date`, this_cal_date);
  };
    
  // zatim provjeri jel ima data HH
  var this_cal_hh = this_input.data(`cal_hh`);
  // ako nema isto napravi iz current_simple_calendar_date
  if ( !this_cal_hh ) {
    this_cal_hh = cit_dt(current_simple_calendar_date).time.split(`:`)[0];
    this_input.data(`cal_hh`, this_cal_hh);
  };
  
  // isto tako provjeri jel ima minute
  var this_cal_min = this_input.data(`cal_min`);
  
  // ako nema kreiraj minute iz current_simple_calendar_date
  if ( !this_cal_min ) {
    this_cal_min = cit_dt(current_simple_calendar_date).time.split(`:`)[1];
    this_input.data(`cal_min`, this_cal_min);
  };
  
  // stavi nule u sate i minute ako je samo datum
  if ( this_input.hasClass('date') == true ) {
    
    this_input.data(`cal_hh`, '00');
    this_input.data(`cal_min`, '00');
    this_cal_hh = "00";
    this_cal_min = "00";
    
  };
  
  
  var date_time_string = this_cal_date + ` ` + this_cal_hh + `:` + this_cal_min;
  
  var input_format = `date_time`;
  if ( this_input.hasClass(`date`) ) input_format = `date`;
  if ( this_input.hasClass(`time`) ) input_format = `time`;
  
  if ( input_format == `date` ) date_time_string = this_cal_date;
  if ( input_format == `time` ) date_time_string = this_cal_hh + `:` + this_cal_min;
  
  current_simple_calendar_date = cit_ms(date_time_string, 0, input_format ).ms;
  if ( input_format == `date` ) current_simple_calendar_date = cit_ms( date_time_string, 0, input_format ).ms_at_zero;
  
  this_input.val(date_time_string);
  
  this_input.data(`cit_run`)(current_simple_calendar_date, this_input);
  
  select_active_cal_polja();
    
};


function register_simple_cal_events() {

  wait_for( `$('#search_autocomplete_container .kalendar_date_day').length > 0`, function() {

    var this_input = $('#'+current_input_id);
    
    if ( this_input.val() !== `` ) select_active_cal_polja();
    

    $('#search_autocomplete_container .kalendar_date_day').off(`click`);
    $('#search_autocomplete_container .kalendar_date_day').on(`click`, function() {

      $('#search_autocomplete_container .kalendar_date_day').removeClass(`cit_active`);
      $(this).addClass(`cit_active`);
      var date_string = $(this).find(`.kalendar_date`).text().trim();
      
      this_input.data(`cal_date`, date_string);
      
      // stavi nule u sate i minuta ako je samo datum
      if ( this_input.hasClass('date') == true ) {
        this_input.data(`cal_hh`, '00');
        this_input.data(`cal_min`, '00');
      };
      
      update_simple_calendar_date(this_input);

    });

    $('#search_autocomplete_container .drop_item_hh').off(`click`);
    $('#search_autocomplete_container .drop_item_hh').on(`click`, function() {
      
      $('#search_autocomplete_container .drop_item_hh').removeClass(`cit_active`);
      $(this).addClass(`cit_active`);
      
      var hh_string = $(this).text().trim();
      if ( hh_string.length < 2 ) hh_string = `0` + hh_string;
      
      if ( this_input.hasClass('date') == false ) {
        this_input.data(`cal_hh`, hh_string);
      } else {
        this_input.data(`cal_hh`, '00');
      }
      
      update_simple_calendar_date(this_input);

    });


    $('#search_autocomplete_container .drop_item_min').off(`click`);
    $('#search_autocomplete_container .drop_item_min').on(`click`, function() {

      $('#search_autocomplete_container .drop_item_min').removeClass(`cit_active`);
      $(this).addClass(`cit_active`);
      var min_string = $(this).text().trim();
      if ( min_string.length < 2 ) min_string = `0` + min_string;
      
      if ( this_input.hasClass('date') == false ) {
        this_input.data(`cal_min`, min_string);
      } else {
        this_input.data(`cal_min`, '00');
      };
      
      update_simple_calendar_date(this_input);

    });

    $('#simple_month_minus').off(`click`);
    $('#simple_month_minus').on(`click`, function(e) {

      e.stopPropagation();
      e.preventDefault();

      var date = new Date(current_simple_calendar_start);

      var yyyy = date.getFullYear();
      var mnt = date.getMonth();
      var day = date.getDate(); // plus or minus days    

      var new_date = new Date(yyyy, mnt-1, day);
      current_simple_calendar_start = Number(new_date);

      var calendar_html = create_simple_drop_calendar(Number(new_date));

      $('#search_autocomplete_container').html(calendar_html);

    });


    $('#simple_month_plus').off(`click`);
    $('#simple_month_plus').on(`click`, function(e) {

      e.stopPropagation();
      e.preventDefault();

      var date = new Date(current_simple_calendar_start);

      var yyyy = date.getFullYear();
      var mnt = date.getMonth();
      var day = date.getDate(); // plus or minus days    

      var new_date = new Date(yyyy, mnt+1, day);
      current_simple_calendar_start = Number(new_date);

      var calendar_html = create_simple_drop_calendar(Number(new_date));

      $('#search_autocomplete_container').html(calendar_html);

    });


    $('#simple_year_minus').off(`click`);
    $('#simple_year_minus').on(`click`, function(e) {

      e.stopPropagation();
      e.preventDefault();

      var date = new Date(current_simple_calendar_start);

      var yyyy = date.getFullYear();
      var mnt = date.getMonth();
      var day = date.getDate(); // plus or minus days    

      var new_date = new Date(yyyy-1, mnt, day);
      current_simple_calendar_start = Number(new_date);

      var calendar_html = create_simple_drop_calendar(Number(new_date));

      $('#search_autocomplete_container').html(calendar_html);

    });


    $('#simple_year_plus').off(`click`);
    $('#simple_year_plus').on(`click`, function(e) {

      e.stopPropagation();
      e.preventDefault();

      var date = new Date(current_simple_calendar_start);

      var yyyy = date.getFullYear();
      var mnt = date.getMonth();
      var day = date.getDate(); // plus or minus days    

      var new_date = new Date(yyyy+1, mnt, day);
      current_simple_calendar_start = Number(new_date);

      var calendar_html = create_simple_drop_calendar(Number(new_date));

      $('#search_autocomplete_container').html(calendar_html);

    });      





  }, 5*1000);
  
  
};
