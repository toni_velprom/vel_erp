function open_close_accord(elem, open_or_close) {
  
  var panel = elem.next( ".cit_panel" );
  
  if ( elem.hasClass('cit_toggle_height') ) panel = elem;
  
  if ( panel.length == 0 ) return;
  
  if ( open_or_close == "open" ) {
    
    elem.addClass("cit_active");
    
    panel.find('.content_to_hide').each( function() {
      
      if ( $(this).hasClass('row') ) {
        $(this).css(`display`, 'flex');
      } else {
        $(this).css(`display`, 'block');
      };
      
    });
    
    panel.addClass(`cit_animate`);
    panel.css('height', panel[0].scrollHeight + "px");
    
    
    //panel[0].style.maxHeight = panel.scrollHeight + "px";
    setTimeout(function() { 
      panel.css({'overflow': 'unset', height: 'auto'}); 
      panel.removeClass(`cit_animate`);
    }, 320);
    
  } 
  else if ( !open_or_close || open_or_close == "close"  ) {
    
    elem.removeClass("cit_active");
    
    
    panel.css('height', panel[0].scrollHeight + "px");
    
    panel.addClass(`cit_animate`);
    
    var titles_height = 0;
    panel.find('.menu_cat_title').each( function() {
      titles_height += $(this).outerHeight();
    });
    
    panel.find('.content_to_hide').each( function() {
      $(this).css(`display`, 'none');
    });
    
    
    setTimeout( function() {
      panel.css({
        'overflow': 'hidden',
        'height': titles_height+"px"
      });
    }, 50);
    
    setTimeout( function() { panel.removeClass(`cit_animate`); }, 355 );
    
    // panel[0].style.maxHeight = 0;
  };
  
};

function click_accord(e) {
  
  var elem = $(e.target);
  
  // ako je user kliknuo na strelicu za accordion!!!!
  if ( elem.hasClass(`fa-angle-down`) ) {
    elem = elem.parent();
  };
  
  var parent = elem.closest('.accord_parent');
  
  if ( elem.hasClass('cit_active') ) {
    
    console.log(`zatvaram panel`);
    open_close_accord(elem, 'close');
    
  } else {
      
    if ( parent && parent.hasClass("close_others") ) {
      
      parent.find('.cit_accord').removeClass('cit_active');
      
      // ako je argument close others onda sve panele uunutar tog parenta smanji na nulu
      parent.find('cit_panel').css({
        'overflow': 'hidden',
        'height': "0px"
      });
      
    };
    console.log(`otvaram panel`);
    open_close_accord(elem, 'open');
  };
  
};



function open_tab(e) {
  
  var tab_btn = $(e.target);
  var tab_index = tab_btn.index();
  
  var tab_strip = tab_btn.parent();
  var parent = tab_btn.parent().parent();

  tab_strip.find(`.cit_tab_btn`).removeClass(`cit_active`);
  tab_strip.find(`.cit_tab_btn`).eq(tab_index).addClass(`cit_active`);
  
  parent.find(`.cit_tab_box`).css(`display`, `none`);
  parent.find(`.cit_tab_box`).eq(tab_index).css(`display`, `block`);
  
  
  /*
  $(`.proces_scroll_box`).each( function() {
    
    var this_scroll_box = this;
    function has_height() {
      return $(this_scroll_box)[0].scrollHeight > 0;
    };
    
    wait_for( has_height , function(elem) {
      
      var proces_scroll_box_height = $(elem).height(); // .outerHeight();
      $(elem).css(`height`, proces_scroll_box_height + `px`);
      $(elem).css(`height`, proces_scroll_box_height + `px`);
      
      
    }, 10*1000, null, null, this_scroll_box  ); 
    
  });
  */
      
     
      
  
  
  
};


