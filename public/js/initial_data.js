var init_store_1_data = {
  
    "_id": "604972ab95bbeebec8f0c1a4",
    "index": 0,
    "guid": "ad100914-59bc-4817-90e5-5298444cada7",
    "isActive": false,
    "balance": "$1,536.72",
    "picture": "http://placehold.it/32x32",
    "age": 26,
    "eyeColor": "green",
    "name": {
      "first": "Latasha",
      "last": "Lara"
    },
    "company": "ISOTRONIC",
    "email": "latasha.lara@isotronic.biz",
    "phone": "+1 (959) 493-3433",
    "address": "398 Campus Place, Carlos, New York, 2935",
    "about": "Laborum labore cupidatat cillum cupidatat dolore tempor commodo deserunt est incididunt. Dolor mollit voluptate amet dolore culpa proident et duis pariatur qui cupidatat et est. Sunt eu velit labore velit eiusmod et consectetur irure eiusmod ad. Consectetur nisi ad et nostrud magna culpa velit.",
    "registered": "Thursday, April 18, 2019 6:19 AM",
    "latitude": "9.018673",
    "longitude": "-151.042283",
    "tags": [
      "sunt",
      "qui",
      "veniam",
      "proident",
      "esse"
    ],
    "range": [
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9
    ],
    "friends": [
      {
        "id": 0,
        "name": "Colon Duke",
        "nested_prop": {
          "first": "Latasha",
          "last": "Lara",
          "array_inside_nested_prop": [ 1,2,3, { prop_deep_nest:  11111 } ]
        }
      },
      {
        "id": 1,
        "name": "Strickland Alexander"
      },
      {
        "id": 2,
        "name": "Angelica Levy"
      }
    ],
    "greeting": "Hello, Latasha! You have 5 unread messages.",
    "favoriteFruit": "banana"
  };
  
var init_users_array = [
  {
    "_id": "604ab456eb22d244399c08f6",
    "index": 0,
    "guid": "6f5b0088-b55c-41fb-b17b-17158548c6ee",
    "isActive": false,
    "balance": "$3,930.77",
    "picture": "http://placehold.it/32x32",
    "age": 26,
    "eyeColor": "brown",
    "name": {
      "first": "Burke",
      "last": "Hardy"
    },
    "company": "EWEVILLE",
    "email": "burke.hardy@eweville.com",
    "phone": "+1 (987) 573-2866",
    "address": "791 Willoughby Avenue, Roberts, Idaho, 8325",
    "about": "Id ea id incididunt incididunt duis pariatur laboris anim dolore irure. Fugiat eu ullamco ut deserunt aute. Aliquip duis consequat aliquip et.",
    "registered": "Thursday, June 21, 2018 1:55 PM",
    "latitude": "-79.211665",
    "longitude": "-41.953022",
    "tags": [
      "duis",
      "duis",
      "aliquip",
      "sint",
      "dolore"
    ],
    "range": [
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9
    ],
    "friends": [
      {
        "id": 0,
        "name": "Colon Duke",
        "nested_prop": {
          "first": "Latasha",
          "last": "Lara",
          "array_inside_nested_prop": [ 1,2,3, { prop_deep_nest:  22222 } ]
        }
      },
      {
        "id": 1,
        "name": "Strickland Alexander"
      },
      {
        "id": 2,
        "name": "Angelica Levy"
      }
    ],
    "greeting": "Hello, Burke! You have 10 unread messages.",
    "favoriteFruit": "banana"
  },
  {
    "_id": "604ab4568baadeb363396cc5",
    "index": 1,
    "guid": "8b7884f0-1ea0-4043-8576-c3446ce06adc",
    "isActive": false,
    "balance": "$1,296.63",
    "picture": "http://placehold.it/32x32",
    "age": 34,
    "eyeColor": "green",
    "name": {
      "first": "Franks",
      "last": "Robertson"
    },
    "company": "NORALI",
    "email": "franks.robertson@norali.us",
    "phone": "+1 (879) 534-3011",
    "address": "174 Prince Street, Bowie, Maryland, 7506",
    "about": "Excepteur esse in duis minim elit et aute veniam ea est. Dolore sunt commodo nostrud adipisicing deserunt fugiat et voluptate magna voluptate. Aliqua Lorem sint dolore mollit adipisicing ea fugiat dolore nulla in qui laborum. Ad sit eu non occaecat sunt aute esse. Et eiusmod aute ea eiusmod non ut incididunt ipsum.",
    "registered": "Thursday, March 16, 2017 12:11 AM",
    "latitude": "-79.199438",
    "longitude": "-53.94565",
    "tags": [
      "sunt",
      "et",
      "eiusmod",
      "Lorem",
      "occaecat"
    ],
    "range": [
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9
    ],
    "friends": [
      {
        "id": 0,
        "name": "Colon Duke",
        "nested_prop": {
          "first": "Latasha",
          "last": "Lara",
          "array_inside_nested_prop": [ 1,2,3, { prop_deep_nest:  33333 } ]
        }
      },
      {
        "id": 1,
        "name": "Florence Floyd"
      },
      {
        "id": 2,
        "name": "Gordon Hardin"
      }
    ],
    "greeting": "Hello, Franks! You have 7 unread messages.",
    "favoriteFruit": "banana"
  },
  {
    "_id": "604ab4568bab1221b8508d2f",
    "index": 2,
    "guid": "8d89cf80-35e2-49f6-926f-204cb96216da",
    "isActive": true,
    "balance": "$1,148.25",
    "picture": "http://placehold.it/32x32",
    "age": 30,
    "eyeColor": "green",
    "name": {
      "first": "Linda",
      "last": "Hooper"
    },
    "company": "AMTAS",
    "email": "linda.hooper@amtas.tv",
    "phone": "+1 (845) 492-2650",
    "address": "722 Clarendon Road, Sims, Alabama, 6388",
    "about": "Sit culpa eiusmod nostrud cupidatat deserunt commodo enim. Irure enim adipisicing duis quis consequat enim adipisicing tempor fugiat. Ullamco consequat et elit consequat eu elit. Labore laboris aute cillum enim consectetur dolor velit. Non anim veniam excepteur ipsum. Culpa eiusmod est cupidatat reprehenderit ad laboris esse mollit non aliqua tempor. Occaecat id consequat nulla eu eiusmod elit sit duis tempor dolore cillum officia aute.",
    "registered": "Friday, October 23, 2015 8:33 AM",
    "latitude": "54.987191",
    "longitude": "-168.776459",
    "tags": [
      "minim",
      "occaecat",
      "in",
      "aliquip",
      "fugiat"
    ],
    "range": [
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9
    ],
    "friends": [
      {
        "id": 0,
        "name": "Colon Duke",
        "nested_prop": {
          "first": "Latasha",
          "last": "Lara",
          "array_inside_nested_prop": [ 1,2,3, { prop_deep_nest:  44444 } ]
        }
      },
      {
        "id": 1,
        "name": "Cathy Noel"
      },
      {
        "id": 2,
        "name": "Fowler Wolf"
      }
    ],
    "greeting": "Hello, Linda! You have 8 unread messages.",
    "favoriteFruit": "strawberry"
  },
  {
    "_id": "604ab45612d854c7bee0af76",
    "index": 3,
    "guid": "1accd8b0-b02b-4e48-95df-e0a30908f609",
    "isActive": false,
    "balance": "$3,953.88",
    "picture": "http://placehold.it/32x32",
    "age": 22,
    "eyeColor": "blue",
    "name": {
      "first": "Townsend",
      "last": "Tyler"
    },
    "company": "ACCUPHARM",
    "email": "townsend.tyler@accupharm.io",
    "phone": "+1 (888) 550-3803",
    "address": "243 Otsego Street, Lowell, Virginia, 990",
    "about": "Lorem labore culpa consequat dolore adipisicing deserunt amet. Ut mollit duis id commodo consequat non eu ad laboris. Lorem duis consectetur duis commodo. Sunt Lorem proident sunt proident deserunt eu aliqua pariatur elit. Eu laboris commodo laborum fugiat consequat adipisicing consequat mollit proident consectetur aliqua qui laborum. Id id elit voluptate adipisicing. Proident dolore laborum consectetur adipisicing laborum nulla minim velit.",
    "registered": "Monday, December 31, 2018 10:39 AM",
    "latitude": "75.574022",
    "longitude": "93.41432",
    "tags": [
      "reprehenderit",
      "ex",
      "duis",
      "reprehenderit",
      "dolore"
    ],
    "range": [
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9
    ],
    "friends": [
      {
        "id": 0,
        "name": "Colon Duke",
        "nested_prop": {
          "first": "Latasha",
          "last": "Lara",
          "array_inside_nested_prop": [ 1,2,3, { prop_deep_nest:  55555 } ]
        }
      },
      {
        "id": 1,
        "name": "Mendez Guthrie"
      },
      {
        "id": 2,
        "name": "Sherry Estes"
      }
    ],
    "greeting": "Hello, Townsend! You have 8 unread messages.",
    "favoriteFruit": "banana"
  },
  {
    "_id": "604ab456e523aab4ade75a02",
    "index": 4,
    "guid": "f4a2f51d-a5fd-49bf-9084-a0926507f7cc",
    "isActive": false,
    "balance": "$3,337.08",
    "picture": "http://placehold.it/32x32",
    "age": 20,
    "eyeColor": "blue",
    "name": {
      "first": "Teresa",
      "last": "Hendricks"
    },
    "company": "INTRAWEAR",
    "email": "teresa.hendricks@intrawear.me",
    "phone": "+1 (843) 520-2022",
    "address": "165 Truxton Street, Coalmont, Puerto Rico, 5540",
    "about": "Aute occaecat duis ipsum culpa magna qui. Fugiat dolore veniam minim elit in tempor qui consequat qui officia pariatur elit amet. Deserunt do amet culpa voluptate ex fugiat. Qui do sint id aute voluptate voluptate laborum adipisicing sit duis. Occaecat labore enim Lorem reprehenderit. Tempor ad id in reprehenderit nisi pariatur officia esse pariatur magna eiusmod. Fugiat officia et proident non nisi voluptate qui consectetur pariatur.",
    "registered": "Saturday, July 4, 2015 12:16 PM",
    "latitude": "-15.362814",
    "longitude": "52.271251",
    "tags": [
      "ex",
      "magna",
      "tempor",
      "irure",
      "est"
    ],
    "range": [
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9
    ],
    "friends": [
      {
        "id": 0,
        "name": "Colon Duke",
        "nested_prop": {
          "first": "Latasha",
          "last": "Lara",
          "array_inside_nested_prop": [ 1,2,3, { prop_deep_nest:  66666 } ]
        }
      },
      {
        "id": 1,
        "name": "Osborne Gilmore"
      },
      {
        "id": 2,
        "name": "Lena Holloway"
      }
    ],
    "greeting": "Hello, Teresa! You have 5 unread messages.",
    "favoriteFruit": "strawberry"
  },
  {
    "_id": "604ab456c9067cf94d55289b",
    "index": 5,
    "guid": "3bbf9600-c154-4b74-8fc2-e3a9c27e9cc4",
    "isActive": true,
    "balance": "$3,749.41",
    "picture": "http://placehold.it/32x32",
    "age": 33,
    "eyeColor": "brown",
    "name": {
      "first": "Cherry",
      "last": "Bell"
    },
    "company": "AFFLUEX",
    "email": "cherry.bell@affluex.ca",
    "phone": "+1 (901) 600-2780",
    "address": "563 Royce Street, Hickory, Guam, 8694",
    "about": "Eu laboris cupidatat ea incididunt incididunt cupidatat commodo et fugiat eu ex occaecat tempor. Enim nisi eu consequat velit et irure laborum. Ea minim et aliqua Lorem. In laborum officia incididunt culpa aliquip fugiat sit consectetur mollit ad nostrud. In fugiat magna do amet deserunt excepteur et tempor. Consequat minim fugiat et velit culpa sunt pariatur et eiusmod reprehenderit non ex. Deserunt fugiat fugiat reprehenderit sint et.",
    "registered": "Thursday, August 28, 2014 12:40 PM",
    "latitude": "-73.579005",
    "longitude": "-171.125315",
    "tags": [
      "proident",
      "in",
      "non",
      "et",
      "aute"
    ],
    "range": [
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9
    ],
    "friends": [
      {
        "id": 0,
        "name": "Colon Duke",
        "nested_prop": {
          "first": "Latasha",
          "last": "Lara",
          "array_inside_nested_prop": [ 1,2,3, { prop_deep_nest:  77777 } ]
        }
      },
      {
        "id": 1,
        "name": "Barrett Mcfadden"
      },
      {
        "id": 2,
        "name": "Barker Santana"
      }
    ],
    "greeting": "Hello, Cherry! You have 9 unread messages.",
    "favoriteFruit": "banana"
  }
];
