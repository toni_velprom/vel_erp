var module_object = {
create: ( data, parent_element, placement ) => {
  var this_module = window[module_url]; 
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };
  
  
  this_module.set_init_data(data);
  
  console.log(data);
  
  /*
  
          id: ( `zad`+cit_rand() + `-` + Date.now() ),
        type: state,
        odgovoran: null,
        invites: [],
        start: null,
        end: null,
        deadline: null,
        est_time: null,
        finished_perc: 0,
        rok_za_def: null,
  
  
  */
  
  

  var valid = {

  /*user: {name: 'Toni', user_num: 765},*/
  zad_odgovoran: { element: "single_select", type: "simple", lock: false, visible: true, label: "Odgovoran" },
  zad_creator: { element: "single_select", type: "simple", lock: false, visible: true, label: "Kreirao zadatak" },
  zad_invites: { element: "multi_select", type: "simple", lock: false, visible: true, label: "Pozivnice" },

  zad_komentar: { element: "text_area", type: "string", lock: false, visible: true, label: "Komentar zadatka" },

  // rokovi i završenost
  zad_finished_perc: {  element: "input", type: "number", lock: false, visible: true, label: "Trajanje zadatka" },
  zad_rok_za_def: { element: "input", type: "date_time", lock: false, visible: true, label: "Rok za definiranje upita" },
  zad_est_time: {  element: "input", type: "date_time", lock: false, visible: true, label: "Trajanje zadatka" },

  // kreiranje i deadline
  zad_created_time: { element: "input", type: "date_time", lock: false, visible: true, label: "Rok" },
  zad_deadline: { element: "input", type: "date_time", lock: false, visible: true, label: "Rok" },

  // kad je zapravo počeo i završio
  zad_start: { element: "input", type: "date_time", lock: false, visible: true, label: "Početak zadatka" },
  zad_end: { element: "input", type: "date_time", lock: false, visible: true, label: "Kraj zadatka" },

};  


  this_module.valid = valid;
  
  

  var { id } = data;
  
  
  var component_html =
`
<div id="${id}" class="row cit_comp" style="padding: 15px; float: none;">
  <div class="col-md-3 col-sm-12">
    ${cit_comp(id+`_zad_odgovoran`, valid.zad_odgovoran, null, '' )}
  </div>

  <div class="col-md-3 col-sm-12">
    ${cit_comp(id+`_zad_invites`, valid.zad_invites, null, '' )}
  </div>

  <div id="${id}_zad_invites_box" class="col-md-6 col-sm-12">
    
  </div>
</div>

`;

  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };
  
  wait_for( `$('#${id}').length > 0`, function() {
        
    $(`#`+id+`_zad_odgovoran`).data('cit_props', {
      desc: 'izaberi tko je odgovoran za novi zadatak unutar zadatak modula',
      local: false,
      url: '/search_kal_users',
      find_in: [ 'email', 'full_name', 'name' ],
      return: { _id: 1, user_number: 1, email: 1, full_name: 1, name: 1 },
      /* ne upisujem širinu za _d jer sam napravio da ga preskočim posve */
      col_widths: [ 2, 3, 3, 2 ],
      show_cols: ['user_number', 'email', 'full_name', 'name' ],
      query: {},
      show_on_click: false,
      list_width: 700,
      cit_run: this_module.choose_odgovoran,
    });  
    
    var multi_widths = { 
        user_number: 1.5,
        email: 2.0,
        full_name: 2.5, 
        name: 2.0,
        delete_invite: 1.0,
        send_invite: 1.0 // dodajem ovaj cell sa add_cols
      };
    
    var col_widths_sum = 0;
    $.each(multi_widths, function(width_key, width) {
      col_widths_sum += Number(width);
    });
    
    $(`#`+id+`_zad_invites`).data('cit_props', {
      
      desc: 'izaberi sve druge ljude koji su povezani s ovim zadatkom',
      local: false,
      
      url: '/search_kal_users',
      find_in: [ 'email', 'full_name', 'name' ],
      return: { _id: 1, user_number: 1, email: 1, full_name: 1, name: 1 },
      
      col_widths: [0, 20, 30, 30, 20 ],
      show_cols: ['user_number', 'email', 'full_name', 'name' ],
      query: {},
      show_on_click: false,
      list_width: 700,
      cit_run: this_module.choose_invites,
      
      
      /* sve ispod su zapravo propsi za listu koja se kreira kad selektiraš red u drop listi */
      multi_list: this_module.cit_data[id].invites,
      multi_list_parent: `#${id}_zad_invites_box`,
      multi_col_order: [ 
        'user_number',
        'email',
        'full_name',
        'name',
        'send_invite',
        'delete_invite'
      ],
      multi_widths: multi_widths,
      add_cols: [
        function(row_id) { 
          
          var prop_name = `send_invite`;
          var header = `<div class="multi_header_cell" style="width: ${multi_widths.send_invite/col_widths_sum*100}%;">POZOVI</div>`;
          var send_invite = 'off';

          $.each(this_module.cit_data[id].invites, function(invite_index, item) {
            var curr_row_id = `multi_row_` + ( item._id || item.sifra );
            if ( row_id == curr_row_id && item.send_invite == true ) send_invite = 'on';
          });

          var cell = 
              `
              <div class="multi_cell" style="width: ${multi_widths.send_invite/col_widths_sum*100}%;" data-row_id="${row_id}" >
                <div class="cit_switch_wrapper">
                  <div data-row_id="${row_id}" class="cit_switch ${send_invite}">
                    <div class="switch_circle"></div>
                    <div class="on_text switch_text">DA</div>
                    <div class="off_text switch_text">NE</div>
                  </div>
                </div>
              </div>
              `;
          
            return {
              prop_name:  prop_name,
              header: header,
              cell: cell
            }
          
          },
        function(row_id) { 
          
          var prop_name = `delete_invite`;
          var header = `<div class="multi_header_cell" style="width: ${multi_widths.delete_invite/col_widths_sum*100}%;">OBRIŠI</div>`;
          var cell = 
            `
            <div class="multi_cell" style="width: ${multi_widths.delete_invite/col_widths_sum*100}%;" data-row_id="${row_id}" >
              <div class="multi_cell_delete_btn multi_list_btn"> <i class="fal fa-times"></i> </div> 
            </div>
            `;

          return {
            prop_name:  prop_name,
            header: header,
            cell: cell
          }
          
        },
      ]
    });      
    
    var props = $(`#`+id+`_zad_invites`).data('cit_props');
        
    create_multi_list( id+`_zad_invites`, props );
    
    wait_for(`$('#${id}_zad_invites_multi_list').length > 0`, function() {
      this_module.register_invites_events(id+`_zad_invites`);
    }, 5*1000);
    
    console.log(id + 'component injected into html');
  }, 500*1000 );

  return {
    html: component_html,
    id: data.id
  };

},
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};
  
  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
  };
  this_module.set_init_data = set_init_data;
  
  

  function choose_odgovoran(state) {

    console.log(state);
    
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    
    if ( state == null ) {
      $('#'+current_input_id).val(``);
      this_module.cit_data[this_comp_id].odgovoran = null;
      return;
    };  
    
    
    $('#'+current_input_id).val(state.full_name);
    
    this_module.cit_data[this_comp_id].odgovoran = state;
    
    console.log(this_module.cit_data[this_comp_id]);
    
  };
  this_module.choose_odgovoran = choose_odgovoran;
  
  

  function choose_invites(state) {
    
    console.log(state);
    
    // deep copy od state
    var state = JSON.stringify(state);
    state = JSON.parse(state);
    
    // postavim send invite odmah na true tako da svi novi imaju po defaultu da dobiju pozivnicu
    state.send_invite = true;
    
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    
    if ( state == null ) {
      $('#'+current_input_id).val(``);
      return;
    };
    
    
    $('#'+current_input_id).val(state.full_name);
    
    if ( !this_module.cit_data[this_comp_id].invites) this_module.cit_data[this_comp_id].invites = [];
    
    console.log(this_module.cit_data[this_comp_id].invites);
    this_module.cit_data[this_comp_id].invites = upsert_item(this_module.cit_data[this_comp_id].invites, state, '_id');
    console.log(this_module.cit_data[this_comp_id].invites);
    
    // update multi list in props
    $('#'+current_input_id).data(`cit_props`).multi_list = this_module.cit_data[this_comp_id].invites;
    
    var props = $('#'+current_input_id).data(`cit_props`);
    

    create_multi_list(current_input_id, props);
    
    
    wait_for(`$('#${current_input_id}_multi_list').length > 0`, function() {
      this_module.register_invites_events(current_input_id);
    }, 5*1000);
    
    
  };
  this_module.choose_invites = choose_invites;
    
  
  function register_invites_events(input_id) {

    // dodajem ovu funkciju u cit run za switch
    $(`#${input_id}_multi_list .cit_switch`).data('cit_run', function(switch_state, elem) {

        var this_comp_id = $(elem).closest('.cit_comp')[0].id;

        console.log(`prije send_invite switch: `, this_module.cit_data[this_comp_id].invites);

        var just_row_id = $(elem).attr(`data-row_id`).replace(`multi_row_`, ``);
        $.each( this_module.cit_data[this_comp_id].invites, function(index, item) {
          if ( item._id == just_row_id || item.sifra == just_row_id ) {
            this_module.cit_data[this_comp_id].invites[index].send_invite = switch_state;
          };
        });
        console.log(`nakon send_invite switch: `, this_module.cit_data[this_comp_id].invites);

      });


    $(`#${input_id}_multi_list .multi_cell_delete_btn`).off(`click`);
    $(`#${input_id}_multi_list .multi_cell_delete_btn`).on(`click`, function() {

      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      
      console.log(`prije brisanja: `,  this_module.cit_data[this_comp_id].invites);

      var row_id = $(this).parent().attr(`data-row_id`);
      var just_row_id = row_id.replace(`multi_row_`, ``);
      
      $(`#${row_id}`).remove();
      
      var delete_invite_index = null;
      $.each(this_module.cit_data[this_comp_id].invites, function(index, item) {
        if ( item._id == just_row_id || item.sifra == just_row_id ) {
          delete_invite_index = index;
        };
      });
      
      if ( delete_invite_index !== null ) this_module.cit_data[this_comp_id].invites.splice(delete_invite_index,1);
      console.log(`nakon brisanja: `, this_module.cit_data[this_comp_id].invites);

    });
    
  };
  this_module.register_invites_events = register_invites_events;
  

  this_module.cit_loaded = true;
 
} // end of module scripts
};
