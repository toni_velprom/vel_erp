var module_object = {
create: async function( data, parent_element, placement ) {
  
  
  var this_module = window[module_url]; 
  
  
  this_module.zad_module = await get_cit_module(`/modules/kalendar/zadatak.js`, null);
  
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };

  
  // TODO - kasnije će biti hrpa ovih itema kada budem učitavao iz baze
  // za sada ne koristim !!!!!
  var kalendar_test_data = { 
    
    kal_items: [
      {
        zad_id: 777,
        type: {sifra: `ZAD1`, naziv: "Sastanak" },
        odgovoran: {name: 'Toni', user_num: 765},
        invites: [],
        start: 1622671200000,
        end: null,
        deadline: null,
        est_time: 1000*60*30,
        finished_perc: 0.6,
        rok_za_def: Date.now()+1000*60*60*24*2,
      },
    ],
    
  };
  
  
  // TODO -----> OVO JE DUMMY DATA ZA TESTIRANJE ---- OBRISATI KASINJE !!!!
  // var data = { ...data, ...kalendar_test_data };
 
  
  var { id , show_hours, start, end } = data;
  
   
  this_module.set_init_data(data);
  var rand_id = `kalendar`+cit_rand();
  this_module.cit_data[id].rand_id = rand_id;
  
  
  var valid = {
    
    /* podaci od pojedinih zadataka */
    zad_new: { element: "single_select", type: "same_width", lock: false, visible: true, label: "Kreiraj novi zadatak" },

    /* podaci od cijelog kalendar modula */  
    kal_view_start: { element: "simple_calendar", type: "date_time", lock: false, visible: true, label: "Početak prikaza" },
    kal_view_end: { element: "simple_calendar", type: "date_time", lock: false, visible: true, label: "Kraj prikaza" },
    
    /* podaci od pojedinih zadataka */
    zad_new: { element: "single_select", type: "same_width", lock: false, visible: true, label: "Kreiraj novi zadatak" },
    
    kal_grupa: { element: "single_select", type: "same_width", lock: false, visible: true, label: "Grupa zadataka" },  
    
};  
  
  
  this_module.valid = valid;
  
  
  var component_html = this_module.create_kalendar(id, show_hours, start, end);


  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };
  
  wait_for( `$('#${data.id}').length > 0`, function() {
    
    
    $('#search_main_box').html('');
    
    document.title = "KALENDAR VELPROM";
    $(`#cit_page_title h3`).text(`Kalendar`);
    $('.cit_tooltip').tooltip();
    $(`html`)[0].scrollTop = 0;
    
    
    $('#'+data.id+'_kal_view_start').data(`cit_run`, this_module.update_kal_view_start );
    
    $('#'+data.id+'_kal_view_end').data(`cit_run`, this_module.update_kal_view_end );
    
    $('#'+data.id+'_zad_new').data('cit_props', {
      
      desc: 'izaberi tip novog zadatka u kalendar modulu',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'zad_new',
      cit_run: this_module.choose_zad_new,
      
    });   
    
    
    
    /*
    $('#stats_choose_user').data('cit_props', {
      desc: 'za odabir usera/customera u stats',
      local: false,
      url: '/kalendar_users',
      find_in: ['email', 'name', 'user_address', 'user_tel' ],
      return: { user_number: 1, email: 1, name: 1 },
      show_cols: ['user_number', 'name', 'email'],
      query: {},
      show_on_click: false,
      list_width: 700,
    });   
    */
    
   
    
    // ako glavni prostor je široki tj ako je lijevi menu smanjen
    if ( $(`#cont_main_box`).hasClass('cit_wide') ) {
      $(`#kalendar_header`).addClass('cit_wide');
    };
    
    register_grab_to_scroll(`kalendar_tasks_box`);
    
    var kalendar_header = $(`#${data.id}_accord_parent .cit_accord`);
    var kalendar_header_panel = kalendar_header.next( ".cit_panel" );
    
    if (  kalendar_header.hasClass(`cit_active`) == false  ) {
      
      kalendar_header_panel.css({
        'overflow': 'hidden',
        'height': "0px"
      });
      
    };
    
    
    $(`#kalendar_tasks_box`).off(`scroll`);
    $(`#kalendar_tasks_box`).on(`scroll`, function () {
      
      var kal_users_wrapper = $('#kal_users_wrapper');
      
      var move_x =  -1* this.scrollLeft;
      if ( move_x > 0 ) move_x = 0
      var max_scroll = -1*(this.scrollWidth - $(this).width());
      if ( move_x < max_scroll ) move_x = max_scroll;
      kal_users_wrapper.css(`transform`,  `translateX( ${move_x}px )`);  
      
    });
    
    
    
    $(`#kal_show_days`).off(`click`);
    $(`#kal_show_days`).on(`click`, function () {
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      this_module.cit_data[this_comp_id].show_hours = false;
      this_module.create( this_module.cit_data[this_comp_id], $(`#cont_main_box`) );
    });
    
    $(`#kal_show_hours`).off(`click`);
    $(`#kal_show_hours`).on(`click`, function () {
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      this_module.cit_data[this_comp_id].show_hours = true;
      this_module.create( this_module.cit_data[this_comp_id], $(`#cont_main_box`) );

    });
    
    $(`#update_kal_view_btn`).off(`click`);
    $(`#update_kal_view_btn`).on(`click`, function () {
      
      var start_ms = cit_ms( $(`#kalendar_module_kal_view_start`).val().trim(), 0, `date_time` ).ms;
      var end_ms = cit_ms( $(`#kalendar_module_kal_view_end`).val().trim(), 0, `date_time` ).ms;
      
      if ( start_ms >= end_ms ) {
        popup_warn(`Drugi datum mora biti veći od prvog datuma!`);
        return;
      };
      
      if ( !start_ms || !end_ms ) {
        popup_warn(`Potrebno je upisati oba datuma!`);
        return;
      };
      
      
      var kalendar_data = {
        id: `kalendar_module`,
        show_hours: false,
        start: start_ms,
        end: end_ms,
      };
      
      this_module.create( kalendar_data, $(`#cont_main_box`) );

    });
    
    
    console.log(data.id + 'component injected into html');
    toggle_global_progress_bar(false);
    
  }, 500*1000 );

  return {
    html: component_html,
    id: data.id
  };

},
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};

  
  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
  };
  this_module.set_init_data = set_init_data;
  
  function create_kalendar(id, hours_view, start, end) {
    
    var valid = this_module.valid;
    
    var this_comp_id = id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id = data.rand_id;
    
    
    
    var users_and_tasks = this_module.generate_user_tasks();
    
    var rand_id = `kalendar`+cit_rand();
    
    var start_val = '';
    var end_val = '';
    
    if ( data.start ) {
      start_val = cit_dt(data.start).date + ' ' + cit_dt(data.start).time;
    };
    
    if ( data.end ) {
      end_val = cit_dt(data.end).date + ' ' + cit_dt(data.end).time;
    };
    
    
             
      /*
      var new_zadatak = {
        zad_id: ( cit_rand() + `-` + Date.now() ),
        zad_type: {sifra: `ZAD1`, naziv: "Sastanak" },
        zad_odgovoran: null,
        zad_invites: [],
        zad_start: null,
        zad_end: null,
        zad_deadline: null,
        zad_est_time: null,
        zad_finished_perc: 0,
        zad_rok_za_def: null,
      };
      
      */
    
    
    var html = `

<div id="${id}" class="kalendar_body cit_comp">
  <!--<h1 class="kalendar_test">OVO JE kalendar</h1>-->
  
  <div id="kalendar_header" class="cit_wide">

    <div id="${data.id}_accord_parent" class="menu_cat accord_parent" id="kalendar_controls">
      
      <div  class="row menu_cat_title cit_accord" 
            style="background: #fff; height: 70px; margin-right: -15px; margin-left: -15px;">  
            
        <div class="col-md-3 col-sm-12">
          ${cit_comp(data.id+`_zad_new`, valid.zad_new, null, '' )}
        </div> 
        
        <div class="col-md-3 col-sm-12">
          ${cit_comp(data.id+`_kal_view_start`, valid.kal_view_start, null, start_val )}
        </div> 
        
        <div class="col-md-3 col-sm-12">
          ${cit_comp(data.id+`_kal_view_end`, valid.kal_view_end, null, end_val )}
        </div> 

        <div class="col-md-3 col-sm-12">
          <button class="blue_btn" id="update_kal_view_btn" 
                  style="margin: 18px auto 0 0; box-shadow: none;">
            PRIKAŽI
          </button>
        </div>
        
        <i id="kalendar_arrow" class="fal fa-angle-down" style="color: #000; opacity: 1;"></i>
      </div> 
      
      <div  class="cat_items cit_panel" style="background: #fff; padding: 0; height: 0;">
        <div id="zad_new_box" style="width: 100%;">
          <!-- OVDJE IDU POLJA KAD KREIRAM NOVI ZADATAK -->
        </div>

      </div>  

    </div>
    <div id="kalendar_users">
    
    <div id="kal_day_hour_box">
        <button class="cit_btn" id="kal_show_days"><i class="fal fa-calendar-day"></i></button>
        <button class="cit_btn" id="kal_show_hours"><i class="fal fa-clock"></i></button>
    </div>  
      <div id="kal_users_wrapper">
        ${ users_and_tasks.users }
      </div>
    </div> 

  </div>

  <div id="kalendar_cont">

    <div id="kalendar_date_day_box">

      ${this_module.generate_date_day_html(start, end, hours_view)}

    </div>

    <div id="kalendar_tasks_box">
       ${ users_and_tasks.tasks }
    </div>  

  </div>  
  
  
</div>
 

`;  
    
    return html;
    
  };
  this_module.create_kalendar = create_kalendar;
  
  
  
  function generate_date_day_html(start, end, cit_show_hours) {
  
    var date_now = new Date();
    
    var dd_now = date_now.getDate();
    var mm_now = date_now.getMonth();
    var yyyy_now = date_now.getFullYear(); 
    
    
    var start_date = new Date();
    if ( start ) start_date = new Date(start);

    var dd = start_date.getDate();
    var mm = start_date.getMonth();
    var yyyy = start_date.getFullYear();
    

    // ako nemam end onda uzmi mjesec dana poslje
    var end_date = new Date(yyyy, mm+1, dd);
    // ako imam end onda uzmi točni end
    if ( end )  end_date = new Date(end);
    var end_date_ms = Number(end_date);

    var date_day_html = ``;  
    var d;
    var prev_date = dd-1;

  for(d=1; d<=1000; d++ ) {

    // ako nema starta onda uzmi jedan mjesec prije danas
    var date = new Date(yyyy, mm-1, dd);
    
    var curr_dd = date.getDate();
    var curr_mm = date.getMonth();
    var curr_yyyy = date.getFullYear(); 
    
    // ak imam start onda točno uzmi taj start
    if ( start ) date = new Date(yyyy, mm, dd);
    var date_ms = Number(date);

    if ( end_date_ms >= date_ms ) {
      var day = date.getDay();
      var day_string = [ 'ned', 'pon', 'uto', 'sri', 'cet', 'pet', 'sub'];
      day = day_string[day];
    
      
      var week_num = '';
      
      if (day == 'pon') week_num = `<span class="week_num">W${new Date(date_ms).getWeek()}</span>`;
      var is_today = ( dd_now == curr_dd && mm_now == curr_mm && yyyy_now == curr_yyyy ) ? 'is_today' : '';
      
      if ( !cit_show_hours ) {
      

        
date_day_html +=
`
<div class="kalendar_date_day cit_${day} ${is_today}">
  <div class="kalendar_date">${ cit_dt(date_ms).date }</div>
  <div class="kalendar_day">${day.replace(`c`, `č`).toUpperCase() + week_num }</div>
</div>

`;      
        
      } else {
        
      var sati = ``;    
        
      for( s=1; s<=24; s++ ) {
        var next_date = date.getDate();  
        var bottom_thick_line = '';
        if ( prev_date !== next_date && s==24 ) {
          bottom_thick_line = `border-bottom: 2px solid #000;`; 
          prev_date = next_date;
        };
        
        sati += 
        `
        <div class="kalendar_date_day cit_${day} cit_show_hours ${is_today}" style="${bottom_thick_line}">
          <div class="kalendar_date">${ cit_dt(date_ms).date }</div>
          <div class="kalendar_day">${day.replace(`c`, `č`).toUpperCase() }</div>
          <div class="kalendar_sat">${ s+':00' }</div>
        </div>
        `;
      }; 


        date_day_html += sati
             
      }; // kraj else ako su sati
        
        
      dd +=1;
      
    }; // kraj end date veći ili jednak trentnom
    
  }; // end for loop za dane
  
  return date_day_html;  
    
};
  this_module.generate_date_day_html = generate_date_day_html;

      
  function generate_user_tasks() {
    
    var all_user_tasks = ``;
    var all_users = ``;
    
    for(u=1; u<=20; u++ ) {

    all_users  += 
`
<div class="user_title">
  ${ 'USER BR. ' + u }
</div>  

`;
  
    all_user_tasks  +=   
      
    `
<div class="user_box">
 
  <div class="user_lane"></div>
  <div class="user_lane"></div>
  <div class="user_lane"></div>
  
</div>  

`;  
      


    };
    
    return {
      tasks: all_user_tasks,
      users: all_users
    }
    
  };
  this_module.generate_user_tasks = generate_user_tasks;
  
  
  function choose_zad_new(state) {
    
    var new_zadatak = null;
    
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    
    var data = this_module.cit_data[this_comp_id];
        
    if ( state == null ) {
      $('#'+current_input_id).val(``);
      return;
    };  
   
    $('#'+current_input_id).val(state.naziv);
    
    if ( state.sifra == `ZAD1` ) {
      
      var { _id, user_number, email, full_name, name } = window.cit_user;
      
      new_zadatak = {
        id: ( `zad`+cit_rand() + `-` + Date.now() ),
        type: state,
        odgovoran: null,
        creator: { _id , user_number, email, full_name, name },
        invites: [
          {_id: "adadsa", user_number: 564, email: "asasd@dasdasda.com", full_name: "bladd bladsd ", name: "bla", send_invite: true },
          {_id: "dsdsdsd", user_number: 55, email: "asas222sdd@dada.com", full_name: "bla bla ", name: "bla dsd", send_invite: true },
          {_id: "dddd3d3d3d", user_number: 96, email: "a1111ddasd@d1111asda.com", full_name: "bla bla ", name: "bla", send_invite: true },
        ],
        start: null,
        end: null,
        deadline: null,
        est_time: null,
        finished_perc: 0,
        rok_za_def: null,
      };
      
    };
    
    
    this_module.zad_module.create(new_zadatak, $(`#zad_new_box`));
    
    setTimeout(function() {
      var kalendar_header = $(`#${data.id}_accord_parent .cit_accord`);
      open_close_accord(kalendar_header, `open`);
    }, 100 );
    
    
  };
  this_module.choose_zad_new = choose_zad_new;
  
  
  function update_kal_view_start(state) {
      
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;

    if ( state == null || $('#'+current_input_id).val() == ``) {
      $('#'+current_input_id).val(``);
      this_module.cit_data[this_comp_id].start = null;
      
      console.log(`new start: `, null );
      return;
    };
    
    this_module.cit_data[this_comp_id].start = state;
    console.log(`new start: `, new Date(state));
    
  };
  this_module.update_kal_view_start = update_kal_view_start;
  
  
  function update_kal_view_end(state) {
      
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;

    if ( state == null || $('#'+current_input_id).val() == ``) {
      $('#'+current_input_id).val(``);
      this_module.cit_data[this_comp_id].end = null;
      
      console.log(`new end: `, null );
      return;
    };
    
    this_module.cit_data[this_comp_id].end = state;
    console.log(`new end: `, new Date(state));
    
  };
  this_module.update_kal_view_end = update_kal_view_end;
  
  
  this_module.cit_loaded = true;
 
} // end of module scripts
};

