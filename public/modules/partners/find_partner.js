var module_object = {
create: async ( data, parent_element, placement ) => {
  var this_module = window[module_url]; 
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };
  
  var { id } = data;
  
  this_module.partners_module = await get_cit_module(`/modules/partners/partner_module.js`, 'load_css');
  
  
  var partner_label = ``;
  if ( data.for_module == "partner" ) partner_label = "Pretraga Partnera";
  if ( data.for_module == "sirov" ) partner_label = "Pretraga Dobavljača";
  if ( data.for_module == "project" ) partner_label = "Pretraga Kupca";
  
  
  
  var valid = {
    find_partner: { element:"single_select", type:"", lock:false, visible:true, label: partner_label  },
  };
  
  this_module.set_init_data(data);
  
  var rand_id = `parfind`+cit_rand();
  this_module.cit_data[id].rand_id = rand_id;
  
  var component_html = "";
  
  if ( data.for_module == "partner" ) {
  
  component_html =
`

<div id="${id}" class="partner_search_body cit_comp">
  <section>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-5 col-sm-12" style="margin-bottom: 5px;" >
          ${ cit_comp(rand_id+`_find_partner`, valid.find_partner, null, "") }
        </div>
      </div>
      <div class="row">
        <div  class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" 
              id="partner_search_box" style="padding-top: 0; padding-bottom: 0;">
          <!-- OVDJE IDE LISTA PRETRAGE SIROVINA TVRTKE -->    
        </div> 
      </div>
    </div>  
  </section>
</div>

`;
    
    
  } 
  else {
    
    // ako nije za otvaranje PARTNERA !!!!
    component_html =
`
<div id="${id}" class="partner_search_body cit_comp">
  ${ cit_comp(rand_id+`_find_partner`, valid.find_partner, null, "") }
</div>
`;
    
    
};
  
  
  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };
  
  wait_for( `$('#${data.id}').length > 0`, function() {
    
    console.log(data.id + 'component injected into html');
    
    var partner_valid = this_module.partners_module.valid;
    
    $(`#`+rand_id+`_find_partner`).data('cit_props', {
      // !!!findpartner!!!
      desc: 'pretraga partnera u modulu find partner',
      local: false,
      url: '/find_partner',
      
      
      find_in: [
        "partner_sifra",
        "oib",
        "vat_num",
        "naziv",
        "grupacija_flat",
        "status",
        "status_flat",
        "komentar_all_adrese",
        "komentar_all_kont",
        "kontakti_flat",
        "adrese_flat",
        "public_komentar",
        "private_komentar",
        "skla_komentar",
        "docs_flat",
      ],
      return: {},
      
      custom_headers: [
        "ŠIFRA",
        "OIB",
        "NAZIV",
        "GRUPACIJA",
        "STATUS PARTNERA",
        "SJEDIŠTE",
      ],
      
      col_widths:[
        1, // "sifra_link",
        1, // "oib",
        3, // "naziv",
        1, // "grupacija_naziv",
        1, // "status_naziv",
        5, // "sjediste",
      
      ],
      show_cols: [
        /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */
        "sifra_link",
        "oib",
        "naziv",
        "grupacija_naziv",
        "status_naziv",
        "sjediste",
      ],
      format_cols: {
        "sifra_link": "center",
        "oib": "center",
        "grupacija_naziv": "center",
        "status_naziv": "center",
      },
      query: {},
      filter: this_module.find_partner_filter,
      
      show_on_click: false,
      list_width: 700,
      
      cit_run: async function(state) {
        
        
        console.log(state);
        
        // stani ako je user obrisao sve u inputu !!!!
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          return;
        };
        
        var state = cit_deep(state);
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
 
        // SAMO AKO SLUŽI ZA OPEN PARTNER !!!!!!
        // SAMO AKO SLUŽI ZA OPEN PARTNER !!!!!!
        // SAMO AKO SLUŽI ZA OPEN PARTNER !!!!!!
        
        if ( data.for_module == "partner" ) {
          // ako iz nekog razloga nije uspio loadati partners module
          if ( !this_module.partners_module ) {
            console.error(`PARTNER MODULE NIJE UČITAN`);
            return;
          };
          
          window.google_maps_loaded = false;
          
          var hash_data = {
            partner: state._id,
            status: null,
          };

          update_hash(hash_data);

        
          var props = {
              filter: this_module.find_partner_filter,
              url: '/find_partner',
              query: { _id: state._id },
              desc: `pretraga nutar find partner unutar cit run tj kada user izabere red na drop listi`,
            };

            try {
              db_result = await search_database(null, props );
            } 
            catch (err) {
              console.log(err);
            };

          // ako je našao nešto onda uzmi samo prvi član u arraju
          if ( db_result && $.isArray(db_result) && db_result.length > 0 ) {
            db_result = db_result[0];
          };


          
          var partner_data = {
            ...db_result,
            id: `partner_module`,
          };
          this_module.partners_module.create( partner_data, $(`#cont_main_box`) );
          
          
        };
        
        // pipe je funkcija od nekog drugogog modula kojem trebam predati ovaj state
        // trenutno samo jedan pipe imam unutar project modula
        if ( data.pipe ) data.pipe( state );
        
      },
      
    });  
    
    
    
  }, 500*1000 );

  return {
    html: component_html,
    id: data.id
  };

},
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};

  
  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
  };
  this_module.set_init_data = set_init_data;

  

  function find_partner_filter(items, jQuery) {

    var $ = jQuery;

    var new_items = [];
    $.each(items, function(index, item) {
      
      // ova filter nekad pokrenem na serveru a nekad ga pokrenem lokalno !!!!
      var sjediste = typeof global !== "undefined" ? global.concat_full_adresa(item, `VAD1`) : window.concat_full_adresa(item, `VAD1`);

      new_items.push({
        ...item,
        sifra_link: `<a style="text-align: center;" href="#partner/${item._id}/status/null" target="_blank" onClick="event.stopPropagation();">${item.partner_sifra}</a>`,
        sjediste: sjediste,
        grupacija_naziv: (item.grupacija ? item.grupacija.name : ""),
        status_naziv: (item.status ? item.status.naziv : ""),
      });

    }); // kraj loopa po svim itemima

    return new_items;
  };
  this_module.find_partner_filter = find_partner_filter;
  
  
  
  this_module.cit_loaded = true;
 
} // end of module scripts
};

