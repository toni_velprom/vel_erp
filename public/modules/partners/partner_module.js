var module_object = {

  
create: ( data, parent_element, placement ) => {
  var this_module = window[module_url]; 
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };
  
  
    
  var valid = {
    
partner_sifra: { "element": "input", "type": "string", "lock": true, "visible": true, "label": "Šifra partnera" },
naziv: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "Naziv / Ime i prezime", required: true },
    
fiz_osoba: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Fizička osoba" },
kupac: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Kupac" },
dobavljac: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Dobavljac" },
web_kupac: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Web kupac" },
ino_kupac: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Inozemni kupac" },
   
    
uzima_visak: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Kupac uzima višak" },
placa_visak: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Kupac plaća višak" },
popust_visak: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Popust na višak", },      
    

iban_num: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "IBAN" },
iban_bank: { "element": "single_select", "type": "simple", "lock": false, "visible": true, "label": "Banka" },
    
oib: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "OIB" , required: true },
vat_num: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "VAT number" },
djelatnost: { "element": "text_area", "type": "string", "lock": false, "visible": true, "label": "Opis djelatnosti" },

new_grupacija_naziv: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "Naziv nove grupacije" },    

grupacija: { "element": "single_select", "type": "simple", "lock": false, "visible": true, "label": "Grupacija" },
    
status: { "element": "single_select", "type": "simple", "lock": false, "visible": true, "label": "Status" },
adrese: { "element": "multi_select", "type": "simple", "lock": false, "visible": true, "label": "Adrese" },
kontakti: { "element": "multi_select", "type": "simple", "lock": false, "visible": true, "label": "Kontakti" },
adresa_coords: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "Koordinate na mapi" },
    
public_komentar: { "element": "text_area", "type": "string", "lock": false, "visible": true, "label": "Napomena vidljiva svima (i partneru)" },
private_komentar: { "element": "text_area", "type": "string", "lock": false, "visible": true, "label": "Napomena vidljiva proizvodnji" },
skla_komentar: { "element": "text_area", "type": "string", "lock": false, "visible": true, "label": "Napomena vidljiva skladištu" },
doc_komentar: { "element": "text_area", "type": "string", "lock": false, "visible": true, "label": "Napomena vidljiva u dokumentu" },
    
    
doc_row_komentar: { "element": "text_area", "type": "string", "lock": false, "visible": true, "label": "Napomena vidljiva samo INTERNO" },
    

web_pristupi: { "element": "text_area", "type": "string", "lock": false, "visible": true, "label": "Editiraj Web pristupe" },

perc_placanja_kupac: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Postotak uplate OD KUPCA", },       
nacin_placanja_kupac: { "element": "single_select", "type": "simple", "lock": false, "visible": true, "label": "Način plaćanja OD KUPCA" },
time_placanja_kupac: { "element": "single_select", "type": "simple", "lock": false, "visible": true, "label": "Početak roka plaćanja OD KUPCA" },
rok_placanja_kupac: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "ROK plaćanja ZA KUPCA u danima", },   
    
    
perc_placanja_dobav: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Postotak uplate PREMA DOBAVLJAČU ", },
nacin_placanja_dobav: { "element": "single_select", "type": "simple", "lock": false, "visible": true, "label": "Način plaćanja PREMA DOBAVLJAČU" },  
time_placanja_dobav: { "element": "single_select", "type": "simple", "lock": false, "visible": true, "label": "Početak roka plaćanja PREMA DOBAVLJAČU" },
rok_placanja_dobav: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Rok plaćanja PREMA DOBAVLJAČU u danima", },
    
nacini_dostave: { "element": "multi_select", "type": "simple", "lock": false, "visible": true, "label": "Načini dostave" },
 
definicija_valute: { "element": "single_select", "type": "simple", "lock": false, "visible": true, "label": "Definicija valute" },
valuta_on_time: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Na datum plaćanja" },
valuta_value: { "element": "input", "type": "number", decimals: 2, "lock": false, "visible": true, "label": "Dogovoreni tečaj" },
   
/* valuta_placanja_kupac: { "element": "input", "type": "number", "lock": false, "visible": true, "label": "Kupac Valuta plaćanja" }, */
limit_zaduzenja_kupac: { "element": "input", decimals: 2, "type": "number", "lock": false, "visible": true, "label": "Limit zaduženja kod KUPCA" },

/* valuta_placanja_dobav: { "element": "input", "type": "number", "lock": false, "visible": true, "label": "Dobavljač (naša) Valuta plaćanja" }, */
limit_zaduzenja_dobav: { "element": "input", decimals: 2, "type": "number", "lock": false, "visible": true, "label": "Naš Limit zaduženja kod DOBAVLJAČA" },
    
zaduznica_from_date_kupac: { element: "simple_calendar", type: "date", lock: false, visible: true, label: "Kupac Zadužnica od Datuma" },
zaduznica_to_date_kupac: { element: "simple_calendar", type: "date", lock: false, visible: true, label: "Kupac Zadužnica do Datuma (neobavezno)" },
zaduznica_amount_kupac: { "element": "input", "type": "number", decimals: 2, "lock": false, "visible": true, "label": "Kupac Zadužnica Iznos" },

zaduznica_from_date_dobav: { element: "simple_calendar", type: "date", lock: false, visible: true, label: "Dobavljač Zadužnica od Datuma" },
zaduznica_to_date_dobav: { element: "simple_calendar", type: "date", lock: false, visible: true, label: "Dobavljač Zadužnica do Datuma (neobavezno)" },
zaduznica_amount_dobav: { "element": "input", "type": "number", decimals: 2, "lock": false, "visible": true, "label": "Dobavljač Zadužnica Iznos" },
    
    
cassa_sconto_days_kupac: { "element": "input", "type": "number", decimals: 0, "lock": false, "visible": true, "label": "KUPAC SPECIJALNI SCONTO za prijevremene uplate / dan " },
cassa_sconto_perc_kupac: { "element": "input", "type": "number", decimals: 2, "lock": false, "visible": true, "label": "Sconto Kupac %" },
    
cassa_sconto_days_dobav: { "element": "input", "type": "number", decimals: 0, "lock": false, "visible": true, "label": "DOBAVLJAČ SPECIJALNI SCONTO za prijevremene uplate / dan" },
cassa_sconto_perc_dobav: { "element": "input", "type": "number", decimals: 2, "lock": false, "visible": true, "label": "Sconto Dobavljač %" },    

bonus_amount_min_kupac: { "element": "input", "type": "number",  "lock": false, "visible": true, "label": "MIN IZNOS za Bonus Kupcu" },
bonus_amount_max_kupac: { "element": "input", "type": "number", "lock": false, "visible": true, "label": "MAX IZNOS za Bonus Kupcu" },
    
bonus_date_start_kupac: { element: "simple_calendar", type: "date", lock: false, visible: true, label: "Vrijedi OD (START)" },
bonus_date_end_kupac: { element: "simple_calendar", type: "date", lock: false, visible: true, label: "Vrijedi OD (END)" },
    
bonus_perc_kupac: { "element": "input", "type": "number", decimals: 2, "lock": false, "visible": true, "label": "Kupac Bonus %" },
    
   
   
bonus_amount_min_dobav: { "element": "input", "type": "number",  "lock": false, "visible": true, "label": "MIN IZNOS Bonus Dobavljača" },
bonus_amount_max_dobav: { "element": "input", "type": "number", "lock": false, "visible": true, "label": "MAX IZNOS Bonus Dobavljača" },

bonus_date_start_dobav: { element: "simple_calendar", type: "date", lock: false, visible: true, label: "Vrijedi OD (START)" },
bonus_date_end_dobav: { element: "simple_calendar", type: "date", lock: false, visible: true, label: "Vrijedi OD (END)" },
    
    
bonus_perc_dobav: { "element": "input", "type": "number", decimals: 2, "lock": false, "visible": true, "label": "Dobav. Bonus %" },   
    
    
ugovor_naziv: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "Naziv Ugovora" },
ugovor_date: {  element: "simple_calendar", type: "date", lock: false, visible: true, "label": "Datum Ugovora" },
ugovor_start_date: {  element: "simple_calendar", type: "date", lock: false, visible: true, "label": "Početak Ugovora" },
ugovor_end_date: {  element: "simple_calendar", type: "date", lock: false, visible: true, "label": "Kraj Ugovora" },
ugovor_komentar: { "element": "text_area", "type": "string", "lock": false, "visible": true, "label": "Penali i Komentari" },
ugovor_docs: { "element": "upload", "type": "multiple", "lock": false, "visible": true, "label": "Izaberi Dokumente ugovora" },     


// PODACI TENDER     
tender_naziv: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "Naziv Tendera" },
tender_date: {  element: "simple_calendar", type: "date", lock: false, visible: true, "label": "Datum Tendera" },
tender_rok_prijave: { element: "simple_calendar", type: "date", lock: false, visible: true, "label": "Rok za prijavu" },
tender_status: { element: "single_select", type: "simple", lock: false, visible: true, label: "Status tendera" },    
  
tender_start_date: { element: "simple_calendar", type: "date", lock: false, visible: true, label: "Datum Početka Tendera" },
tender_end_date: { element: "simple_calendar", type: "date", lock: false, visible: true, "label": "Datum Kraja Tendera" },
    
tender_komentar: { "element": "text_area", "type": "string", "lock": false, "visible": true, "label": "Opis Tendera" },

tender_docs: { "element": "upload", "type": "multiple", "lock": false, "visible": true, "label": "Izaberi dokumente tendera" },

tender_products: { "element": "multi_select", "type": "simple", "lock": false, "visible": true, "label": "Izaberi proizvode tendera" },
    
// PODACI ADRESE
    
komentar_all_adrese: { "element": "text_area", "type": "string", "lock": false, "visible": true, "label": "Komentar vezan za sve adrese" },    
    
naziv_adrese: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "Naziv adrese" },
vrsta_adrese: { "element": "single_select", "type": "simple", "lock": false, "visible": true, "label": "Vrsta adrese" },
 
adresa: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "Ulica i kućni br." },
kvart: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "Kvart" },

// ovo je sve ista drop lista !!!!!  samo različiti propertiji
posta: { "element": "single_select", "type": "", "lock": false, "visible": true, "label": "Poštanski broj" },
naselje: { "element": "single_select", "type": "", "lock": false, "visible": true, "label": "Naselje" },    
mjesto: { "element": "single_select", "type": "", "lock": false, "visible": true, "label": "Grad ili mjesto" },
drzava: { "element": "single_select", "type": "simple", "lock": false, "visible": true, "label": "Država" },


// treba prvo upisati podatke adrese (IZNAD)
// i onda treba kreirati kontakt na toj adresi tj odabrati tu adresu kod kreiranja kontakta
    
komentar_all_kont: { "element": "text_area", "type": "string", "lock": false, "visible": true, "label": "Komentar vezan za sve kontakte" },        
adresa_kontakta: { "element": "single_select", "type": "", "lock": false, "visible": true, "label": "Izaberi adresu kontakta" },

kontakt: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "Ime i Prezime / Naziv" },
pozicija_kontakta: { "element": "single_select", "type": "simple", "lock": false, "visible": true, "label": "Pozicija u tvrtki" },


mob: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "Mobitel" },
tel_1: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "Telefon 1" },
tel_2: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "Telefon 2" },
fax: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "Fax" },
mail: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "Email" },
    
work_pon: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "PON" },
work_uto: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "UTO" },
work_sri: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "SRI" },
work_cet: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "ČET" },
work_pet: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "PET" },
work_sub: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "SUB" },
work_ned: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "NED" },


partner_sirov: { element: "single_select", type: "", lock: false, visible: true, label: "Odabir Sirovine" },    
    
partner_product: { element: "single_select", type: "", lock: false, visible: true, label: "Odabir VELPROM PROIZVODA" },        

ask_kolicina_1: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label:"Količina 1"},
ask_kolicina_2: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label:"Količina 2"},  
ask_kolicina_3: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label:"Količina 3"}, 

    
ask_kolicina_4: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label:"Količina 4"},  
ask_kolicina_5: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label:"Količina 5"},  
ask_kolicina_6: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label:"Količina 6"},  

    
doc_lang: { element:"single_select", type:"simple", lock:false, visible:true, label:"Jezik dokumenta" },
doc_valuta: { element:"single_select", type:"simple", lock:false, visible:true, label: "Valuta" },
doc_valuta_manual: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label: "Prepiši Tečaj" },
    
ref_doc_sifra: { element:"input", type:"string", lock:false, visible:true, label:"Ref. šifra (ponuda/otpremnica)"},
custom_doc_price: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label: "Prepiši cijenu" },
    
doc_adresa: { element:"single_select", type:"same_width", lock:false, visible:true, label:"Adresa na dokumentu" },
otp_adresa: { element:"single_select", type:"same_width", lock:false, visible:true, label:"Adresa za dostavu" },

find_status_for_doc: { element:"single_select", type:"", lock:false, visible:true, label:"Izaberi povezane statuse" },

doc_est_time:  { element: "simple_calendar", type: "date_time", lock: false, visible: true, "label": "Datum dokumenta / procjenjeni datum " },        


ulazna_ponuda_sifra: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "Šifra ponude dobavljača" },
ulazna_ponuda_docs: { "element": "upload", "type": "multiple", "lock": false, "visible": true, "label": "Izaberi Dokumente Ponude" },  
    
order_avans: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Obavezno platiti prije dostave" },
    
/* ZA SADA NE KORISTIM OVO JER DJELOMIČNO MOŽE BITI NEKA SIROVINA A  NEKA DRUGA MOŽE BITI POTPUNO (na istoj primci ) */
primka_partial: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Roba došla DJELOMIČNO" },

    
choose_doc: { element:"check_box", type:"bool", lock:false, visible:true, label:"Označi"},    
    
doc_dostava: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label: "Cijena dostave" }, 
    
next_price: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label: "Cijena za sljedeći dokument" },
next_count: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label: "Količina za sljedeći dokument" },    
next_doc:  { element:"check_box", type:"bool", lock:false, visible:true, label:"Prikaži u sljedećem dokumentu" },
next_approved:  { element:"check_box", type:"bool", lock:false, visible:true, label:"ODOBRENJE UPRAVE"},
    
pay_value: { element:"input", type:"number", decimals: 2, lock:false, visible:true, label: "Uplaćen iznos izlazne fakture" }, 
pay_komentar: { element:"input", type:"string", lock:false, visible:true, label:"Komentar za plaćanje izlazne fakture"},
    
    
    
    
// od ovih propertija ISPOD ne kreiram komponenete već služe za validaciju

adrese: { "type": "array", "label": "Sjedište", required: true },   
    
    
    

};
  
  this_module.valid = valid;
  
  
  var new_partner = {

    partner_sifra: null,
    oib: null,
    vat_num: null,
    naziv: null,


    kupac: false,
    dobavljac: false,    
    fiz_osoba: false,

    web_kupac: false,
    ino_kupac: false,

    djelatnost: null,

    grupacija: null,
    grupacija_flat: null,
    
    
    statuses: [],

    ibans: null,

    ibans_flat: null,

    status: null,
    status_flat: null,

    komentar_all_adrese: null,
    adrese: null,
    adrese_flat: null,

    komentar_all_kont: null,
    kontakti: null,

    kontakti_flat: null,

    public_komentar: null,
    private_komentar: null,
    skla_komentar: null,

    web_pristupi: null,

    nacini_placanja_kupac: null,
    time_placanja_kupac: null,
    rok_placanja_kupac: null,
    placanje_kupac: null,
    
    nacini_placanja_dobav: null,
    time_placanja_dobav: null,
    rok_placanja_dobav: null,
    placanje_dobav: null,

    nacini_dostave: null,
    nacini_dostave_flat: null,
   
    valuta_placanja_kupac: null,
    valuta_placanja_dobav: null,

    def_pays: null,

    limit_zaduzenja_kupac: null,
    limit_zaduzenja_dobav: null,

    zaduznice_kupac: null,
    zaduznice_dobav: null,

    cassa_sconto_kupac: null,
    cassa_sconto_dobav: null,

    end_year_bonus_kupac: null,
    end_year_bonus_dobav: null,


    ugovori: null,
    ugovori_flat: null,

    tenders: null,
    tenders_flat: null,
    
    uzima_visak: true,
    placa_visak: true,
    popust_visak: null,
    
    /*
    ---------------------------------------------
    fixed_prices: [
      {
        "tip" : { "sifra" : "TP20",  "klasa" : "KP2",  "naziv" : "F201" },
        mark: 30,
        discount: 10,
        ugovor: 
      }
    ],
    ---------------------------------------------
    */

    products: [],
    docs: [],
    
    
  };
  
  
  // TODO -----> OVO JE DUMMY DATA ZA TESTIRANJE ---- OBRISATI KASNIJE !!!!
  // var data = { ...data, ...dummy_partner_data };
  
  if ( !data.partner_sifra ) data = { ...data, ...new_partner };
  
  
  data.perc_placanja_kupac = null;
  data.nacin_placanja_kupac = null;
  data.time_placanja_kupac = null;
  data.rok_placanja_kupac = null;
  
  data.perc_placanja_dobav = null;
  data.iznos_placanja_dobav = null;
  data.nacin_placanja_dobav = null;
  data.time_placanja_dobav = null;
  data.rok_placanja_dobav = null;
  
  
  
  data.ask_kolicina_1 = null;
  data.ask_kolicina_2 = null;
  data.ask_kolicina_3 = null;
  data.ask_kolicina_4 = null;
  data.ask_kolicina_5 = null;
  data.ask_kolicina_6 = null;
  data.record_est_time = null;
  data.doc_est_time = null;
  
  if ( !data.statuses ) data.statuses = [];
  
  data.ref_doc_sifra = null;
  data.custom_doc_price = null;
  data.doc_valuta_manual = null;
  
  data.doc_komentar = null;
  
  
  this_module.set_init_data(data);
  
  
  // text, name, last_name, street, place, post_num
  var { id } = data;
  
  var rand_id = `partner`+cit_rand();
  this_module.cit_data[id].rand_id = rand_id;
  
  
  var component_html =
`
<div id="${id}" class="partner_comp cit_comp">

<section>
  <div class="container-fluid" style="padding-left: 0;" >
      
      <div class="row">
        <div class="col-md-5 col-sm-12">
          <b>SPREMLJENO:</b> <span id="${rand_id}_user_saved">${data.user_saved?.full_name || "" }</span>
          &nbsp;&nbsp;
          <span id="${rand_id}_saved">${ data.saved ? cit_dt(data.saved).date+" "+cit_dt(data.saved).time : "" }</span>
        </div>
        <div class="col-md-5 col-sm-12">
          <b>EDITIRANO:</b> <span id="${rand_id}_user_edited">${data.user_edited?.full_name || "" }</span>
          &nbsp;&nbsp;
          <span id="${rand_id}_edited">${ data.edited ? cit_dt(data.edited).date+" "+cit_dt(data.edited).time : "" }</span>
        </div>
        
        <div class="col-md-2 col-sm-12">
          <button class="blue_btn btn_small_text" id="save_partner_btn">
            SPREMI PARTNERA
          </button>
        </div>
      </div>

     
    <div class="row">

      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_partner_sifra`, valid.partner_sifra, data.partner_sifra) }
      </div>

      <div class="col-md-4 col-sm-12">
        ${ cit_comp(rand_id+`_naziv`, valid.naziv, data.naziv) }
      </div>
  
      <div  class="col-md-2 col-sm-12 cit_tooltip" 
            data-toggle="tooltip" data-placement="top" data-html="true" title="Ako ne znaš OIB upiši: nepoznat" >
        ${ cit_comp(rand_id+`_oib`, valid.oib, data.oib) }
      </div>
        
      <div class="col-md-2 col-sm-12">
        
        <button   class="blue_btn btn_small_text" id="get_sud_reg_data_btn" 
                  style="margin: 20px 0 0 0; box-shadow: none;  float: left;">
          SUDSKI REGISTAR
        </button>

        <button   class="blue_btn btn_small_text" id="remove_sud_reg_results" 
                  style="margin: 20px 0 0 10px; box-shadow: none;  float: left; padding: 0 8px;">
          X
        </button>
        
      </div>
      
      

      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_vat_num`, valid.vat_num, data.vat_num) }
      </div>


    </div>
     
     
    <div class="row">
     
     <div class="col-md-12 col-sm-12" id="sud_reg_trazi_box">
      
      </div>
      
    </div> 
     
      
    <div class="row">


      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_status`, valid.status, data.status,  data.status?.naziv || "") }
      </div>

      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_kupac`, valid.kupac, data.kupac) }
      </div>

      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_dobavljac`, valid.dobavljac, data.dobavljac) }
      </div>
  
      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_fiz_osoba`, valid.fiz_osoba, data.fiz_osoba) }
      </div>
    
      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_web_kupac`, valid.web_kupac, data.web_kupac) }
      </div>    
    
      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_ino_kupac`, valid.ino_kupac, data.ino_kupac) }
      </div> 

    </div>

    <div class="row">

      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_uzima_visak`, valid.uzima_visak, data.uzima_visak ) }
      </div>

      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_placa_visak`, valid.placa_visak, data.placa_visak ) }
      </div>


      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_popust_visak`, valid.popust_visak, data.popust_visak ) }
      </div>

    </div>



    <div class="row">
      <div class="col-md-12 col-sm-12">
        ${ cit_comp(rand_id+`_djelatnost`, valid.djelatnost, data.djelatnost, null, `min-height: 40px;` ) }
      </div>
    </div>
    
    <div class="row">  
     
      <div class="col-md-4 col-sm-12">
        ${ cit_comp(rand_id+`_public_komentar`, valid.public_komentar, data.public_komentar) }
      </div>
      
      <div class="col-md-4 col-sm-12">
        ${ cit_comp(rand_id+`_private_komentar`, valid.private_komentar, data.private_komentar) }
      </div>  
          
      <div class="col-md-4 col-sm-12">
        ${ cit_comp(rand_id+`_skla_komentar`, valid.skla_komentar, data.skla_komentar) }
      </div>        

    </div>    
    
   
    

  <div class="row">  

    <!-- LIJEVA STRANA IBANS -->
    <div class="col-md-6 col-sm-12">
     <div class="row">

        <div class="col-md-4 col-sm-12">
          ${ cit_comp(rand_id+`_iban_num`, valid.iban_num, "") }
        </div>

        <div class="col-md-4 col-sm-12">
          ${ cit_comp(rand_id+`_iban_bank`, valid.iban_bank, null, "") }
        </div>

        <div class="col-md-4 col-sm-12">

            <button class="blue_btn btn_small_text" id="save_iban" style="margin: 20px 0 0 auto; box-shadow: none;">
              SPREMI NOVI IBAN
            </button>

            <button class="violet_btn btn_small_text" id="update_iban" style="margin: 20px 0 0 auto; box-shadow: none; display: none;">
              AŽURIRAJ IBAN
            </button>


        </div>
      </div>

      <div class="row">
        <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="partner_iban_list_box" style="padding-top: 20px;">
          <!-- OVDJE IDE LISTA IBANA OD OVE TVRTKE -->    
        </div> 

      </div>


    </div>  

    <!-- DESNA STRANA NAČINI PLAĆANJA -->
     

  </div> <!--KRAJ ROW-a-->


    
  <div class="row">
  
    <div class="col-md-6 col-sm-12">
     
      <div class="row">

        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_definicija_valute`, valid.definicija_valute, data.definicija_valute, "") }
        </div>

        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_valuta_on_time`, valid.valuta_on_time, false ) }
        </div>

        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_valuta_value`, valid.valuta_value, "" ) }
        </div>

        <div class="col-md-3 col-sm-12">
            <button class="blue_btn btn_small_text" id="save_def_pay" style="margin: 20px 0 0 auto; box-shadow: none;">
              SPREMI PLAĆANJE
            </button>
            <button class="violet_btn btn_small_text" id="update_def_pay" style="margin: 20px 0 0 auto; box-shadow: none; display: none;">
              AŽURIRAJ PLAĆANJE
            </button>
        </div>


      </div>

      <div class="row">
        <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="def_valute_box" style="padding-top: 20px;">
          <!-- OVDJE IDU DEFINICIJE VALUTA LISTA -->    
        </div> 
      </div>

    </div>  
    
    
  </div>  

    
  <div class="cit_tab_strip">
  
    <div id="partner_tab_adrese" class="cit_tab_btn" >ADRESE</div>
    <div id="partner_tab_kontakti" class="cit_tab_btn" >KONTAKTI</div>
    
    <div id="partner_tab_kupac" class="cit_tab_btn" style="display: ${data.kupac ? `flex` : `none` }">KUPAC</div>
    <div id="partner_tab_dobav" class="cit_tab_btn" style="display: ${data.dobavljac ? `flex` : `none` }">DOBAVLJAČ</div>
    
    
    <div id="partner_tab_povijest" class="cit_tab_btn">POVIJEST</div>
    <div id="partner_tab_statusi" class="cit_tab_btn">STATUSI</div>
    
    
    <div id="partner_tab_dokumenti" class="cit_tab_btn cit_active">KREIRAJ DOKUMENTE</div>
    
    
  </div>
     
     
     
    <div class="cit_tab_box adrese_tab" style="padding: 10px; margin-top: 0; display: none; ">


      <h6 style="margin-left: 20px;">ADRESE</h6>

      <div class="row" style="margin-top: 20px;">


        <div class="col-md-12 col-sm-12">

          <input id="pac-input" class="controls" type="text" placeholder="Search Box" />
          <div id="partner_adresa_google_map" style="width: 100%; height: 300px;">
            <!--OVDJE IDE GOOGLE MAP-->  
          </div>

          <div id="infowindow-content">

            <img id="place-icon" src="" height="16" width="16" style="margin-right: 5px; display: block; float: left; margin-top: 3px;" />
            <span id="place-name" class="google_title" style="display: block; float: left; margin-top: 0; font-size: 18px;"></span>

            <br>
            <br>
            <!-- 
            Place ID <span id="place-id"></span>
            <br>
            -->
            <span id="place-address" style="font-size: 16px; font-weight: 700;"></span>
          </div>


        </div>
      </div>

      <div class="row" style="margin-top: 20px;" >

        <div class="col-md-4 col-sm-12">
          ${ cit_comp(rand_id+`_adresa`, valid.adresa, "") }
        </div>

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_vrsta_adrese`, valid.vrsta_adrese, null, "") }
        </div>

        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_naziv_adrese`, valid.naziv_adrese, "") }
        </div>

        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_adresa_coords`, valid.adresa_coords, "") }
        </div>

      </div>  

      <div class="row" style="padding-bottom: 5px;">

        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_kvart`, valid.kvart, "") }
        </div>

        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_naselje`, valid.naselje, null, "") }
        </div>

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_posta`, valid.posta, null, "") }
        </div>

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_mjesto`, valid.mjesto, null, "") }
        </div>

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_drzava`, valid.drzava, null, "") }
        </div>


      </div>

      <div class="row">
        <div class="col-md-3 col-sm-12">

          <button class="blue_btn btn_small_text" id="save_adresa" style="margin: 20px auto 0 0; box-shadow: none;">
            SPREMI ADRESU
          </button>

          <button class="violet_btn btn_small_text" id="update_adresa" style="margin: 20px auto 0 0; box-shadow: none; display: none;">
            AŽURIRAJ ADRESU
          </button>

        </div>

      </div>


      <div class="row" style="margin-top: 20px;">
        <div class="col-md-12 col-sm-12">
          ${ cit_comp(rand_id+`_komentar_all_adrese`, valid.komentar_all_adrese, data.komentar_all_adrese ) }
        </div>
      </div>

      <div class="row" style="margin-top: 0;">

        <h6 style="margin-left: 20px; margin-top: 20px;">Lista svih adresa</h6>
        <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="sve_adrese_box" style="padding: 2px;" >

          <!-- OVDJE JE POPIS SVIH ADRESA OD OVE TVRTKE -->

        </div>
      </div>




    </div>
    <!-- KRAJ ADRESE TAB BOX -->
 
    <div class="cit_tab_box kontakti_tab" style="padding: 10px; margin-top: 0; display: none; ">



      <h6 style="margin-left: 20px;">KONTAKTI</h6>


      <div class="row" style="margin-bottom: 5px;">
        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_kontakt`, valid.kontakt, "") }
        </div>


        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_adresa_kontakta`, valid.adresa_kontakta, null, "") }
        </div>

        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_pozicija_kontakta`, valid.pozicija_kontakta, null, "") }
        </div>


        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_mail`, valid.mail, "") }
        </div>



      </div>  

      <div class="row" style="margin-bottom: 5px;">


        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_mob`, valid.mob, "") }
        </div>

        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_tel_1`, valid.tel_1, "") }
        </div>

        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_tel_2`, valid.tel_2, "") }
        </div>

        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_fax`, valid.fax, "") }
        </div>
      </div>


      <div class="row" style="margin-bottom: 5px; justify-content: center;" >

        <div class="col-md-1 col-sm-12" style="padding: 2px;" >
          ${ cit_comp(rand_id+`_work_pon`, valid.work_pon, "9-17") }
        </div>

        <div class="col-md-1 col-sm-12" style="padding: 2px;" >
          ${ cit_comp(rand_id+`_work_uto`, valid.work_uto, "9-17") }
        </div>

        <div class="col-md-1 col-sm-12" style="padding: 2px;" >
          ${ cit_comp(rand_id+`_work_sri`, valid.work_sri, "9-17") }
        </div>

        <div class="col-md-1 col-sm-12" style="padding: 2px;" >
          ${ cit_comp(rand_id+`_work_cet`, valid.work_cet, "9-17") }
        </div>

        <div class="col-md-1 col-sm-12" style="padding: 2px;" >
          ${ cit_comp(rand_id+`_work_pet`, valid.work_pet, "9-17") }
        </div>

        <div class="col-md-1 col-sm-12" style="padding: 2px;" >
          ${ cit_comp(rand_id+`_work_sub`, valid.work_sub, "9-17") }
        </div>

        <div class="col-md-1 col-sm-12" style="padding: 2px;" >
          ${ cit_comp(rand_id+`_work_ned`, valid.work_ned, "0-0") }
        </div>


        <div class="col-md-3 col-sm-12">

            <button class="blue_btn btn_small_text" id="save_kontakt" style="margin: 20px auto 0 0; box-shadow: none;">
              SPREMI NOVI KONTAKT
            </button>

            <button class="violet_btn btn_small_text" id="update_kontakt" style="margin: 20px auto 0 0; box-shadow: none; display: none;">
              AŽURIRAJ KONAKT
            </button>

        </div>      

      </div>

      <div class="row" style="margin-top: 20px;" >
        <div class="col-md-12 col-sm-12">
          ${ cit_comp(rand_id+`_komentar_all_kont`, valid.komentar_all_kont, data.komentar_all_kont ) }
        </div>
      </div>    


      <h6 style="margin-left: 20px; margin-top: 20px;">Lista svih kontakata</h6>

      <div class="row" style="">
        <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="svi_kontakti_box" style="padding: 2px;" >
          <!-- OVDJE JE POPIS SVIH KONTAKTA OD OVE TVRTKE -->
        </div>
      </div>







    </div> 
    <!-- KRAJ KONTAKTI TAB BOX -->
     


    <div class="cit_tab_box kupac_tab" style="padding: 10px; background: #f7f3ee; display: none;">

      <div class="row" style="margin-top: 20px;">
        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_limit_zaduzenja_kupac`, valid.limit_zaduzenja_kupac, data.limit_zaduzenja_kupac) }
        </div>
      </div>

      <div class="row" style="margin-top: 5px;">

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_perc_placanja_kupac`, valid.perc_placanja_kupac, 100 ) }
        </div>

        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_nacin_placanja_kupac`, valid.nacin_placanja_kupac, data.nacin_placanja_kupac, "") }
        </div>

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_time_placanja_kupac`, valid.time_placanja_kupac, data.time_placanja_kupac, "") }
        </div>

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_rok_placanja_kupac`, valid.rok_placanja_kupac, null ) }
        </div>

        <div class="col-md-1 col-sm-12">

          <button id="add_placanje_kupac"
                  class="blue_btn btn_circle" 
                  data-toggle="tooltip" data-placement="top" data-html="true" title="Dodaj novu definiciju plaćanja"
                  style="margin: 20px auto 0 0; box-shadow: none;">
            <i class="fas fa-plus"></i>
          </button>

        </div>

        <div class="col-md-2 col-sm-12">
          <button class="blue_btn btn_small_text" id="save_placanje_kupac" style="margin: 20px 0 0 auto; box-shadow: none;">
            SPREMI PLAĆANJE
          </button>
        </div>

      </div>

      <div class="row" style="margin-top: 0px;" > 
        <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="curr_placanje_kupac_box" style="padding-top: 0;">
          <!-- OVDJE IDE LISTA PLAĆANJA KOJI NISU JOŠ SPREMLJENI -->
        </div>
      </div>

      <div class="row" style="margin-top: 0px;" > 
        <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="saved_placanje_kupac_box" style="padding-top: 0;">
          <!-- OVDJE IDE LISTA NAČINA PLAĆANJA KOJA JE SPREMLJENA -->
        </div>
      </div>


      <div class="row">

        <div class="col-md-3 col-sm-12">
          ${cit_comp(rand_id+`_zaduznica_from_date_kupac`, valid.zaduznica_from_date_kupac, null, "" )}
        </div>

        <div class="col-md-3 col-sm-12">
          ${cit_comp(rand_id+`_zaduznica_to_date_kupac`, valid.zaduznica_to_date_kupac, null, "" )}
        </div>

        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_zaduznica_amount_kupac`, valid.zaduznica_amount_kupac, "") }
        </div>

        <div class="col-md-3 col-sm-12">

          <button class="blue_btn btn_small_text" id="save_debt_kupac" style="margin: 20px 0 0 auto; box-shadow: none;">
            SPREMI ZADUŽNICU
          </button>

          <button class="violet_btn btn_small_text" id="update_debt_kupac" style="margin:20px 0 0 auto; box-shadow: none; display: none;">
            AŽURIRAJ ZADUŽNICU
          </button>

        </div>

      </div>

      <div class="row" style="margin-top: 0px;" >
        <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="debt_kupac_box" style="padding-top: 0;">
        <!-- OVDJE IDE TABLICA SVIH ZADUŽNICA ZA KUPCA -->
        </div>
      </div>


      <div class="row">

        <div class="col-md-4 col-sm-12">
          ${ cit_comp(rand_id+`_cassa_sconto_days_kupac`, valid.cassa_sconto_days_kupac, "") }
        </div>

        <div class="col-md-4 col-sm-12">
          ${ cit_comp(rand_id+`_cassa_sconto_perc_kupac`, valid.cassa_sconto_perc_kupac, "") }
        </div>

        <div class="col-md-4 col-sm-12">

          <button class="blue_btn btn_small_text" id="save_sconto_kupac" style="margin: 20px 0 0 auto; box-shadow: none;">
            SPREMI SCONTO
          </button>

          <button class="violet_btn btn_small_text" id="update_sconto_kupac" style="margin: 20px 0 0 auto; box-shadow: none; display: none;">
            AŽURIRAJ SCONTO
          </button>

        </div>

      </div>

      <div class="row" style="margin-top: 0px;" >
        <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="sconto_kupac_box" style="padding-top: 0;">
        <!-- OVDJE IDE TABLICA SVIH SCONTO ZA KUPCA -->
        </div>
      </div>

      <div class="row">

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_bonus_amount_min_kupac`, valid.bonus_amount_min_kupac, "") }
        </div>

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_bonus_amount_max_kupac`, valid.bonus_amount_max_kupac, "") }
        </div>
        
        <div class="col-md-2 col-sm-12">
          ${cit_comp(rand_id+`_bonus_date_start_kupac`, valid.bonus_date_start_kupac, null, "" )}
        </div>
        
        <div class="col-md-2 col-sm-12">
          ${cit_comp(rand_id+`_bonus_date_end_kupac`, valid.bonus_date_end_kupac, null, "" )}
        </div>
        

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_bonus_perc_kupac`, valid.bonus_perc_kupac, "") }
        </div>


        <div class="col-md-2 col-sm-12">

            <button class="blue_btn btn_small_text" id="save_bonus_kupac" style="margin: 20px 0 0 auto; box-shadow: none;">
              SPREMI BONUS
            </button>

            <button class="violet_btn btn_small_text" id="update_bonus_kupac" style="margin: 20px 0 0 auto; box-shadow: none; display: none;">
              AŽURIRAJ BONUS
            </button>

        </div>

      </div>

      <div class="row">
        <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="god_bonusi_box_kupac" style="padding-top: 0px;">
          <!-- OVDJE IDE LISTA GODIŠNJIH BONUSA TVRTKE -->    
        </div> 

      </div>


      <br style="float: left; clear: both; width: 100%; display: block;">


    </div>
    <!-- KRAJ KUPAC TAB BOX -->

    <div class="cit_tab_box dobavljac_tab" style="padding: 10px; background: aliceblue; display: none;" >

      <div class="row" style="margin-top: 0px;">
        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_limit_zaduzenja_dobav`, valid.limit_zaduzenja_dobav, data.limit_zaduzenja_dobav) }
        </div>
      </div>

      <div class="row" style="margin-top: 5px;">

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_perc_placanja_dobav`, valid.perc_placanja_dobav, 100 ) }
        </div>

        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_nacin_placanja_dobav`, valid.nacin_placanja_dobav, data.nacin_placanja_dobav, "") }
        </div>

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_time_placanja_dobav`, valid.time_placanja_dobav, data.time_placanja_dobav, "") }
        </div>

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_rok_placanja_dobav`, valid.rok_placanja_dobav, null ) }
        </div>

        <div class="col-md-1 col-sm-12">

          <button id="add_placanje_dobav"
                  class="blue_btn btn_circle" 
                  data-toggle="tooltip" data-placement="top" data-html="true" title="Dodaj novu definiciju plaćanja"
                  style="margin: 20px auto 0 0; box-shadow: none;">
            <i class="fas fa-plus"></i>
          </button>

        </div>

        <div class="col-md-2 col-sm-12">
          <button class="blue_btn btn_small_text" id="save_placanje_dobav" style="margin: 20px 0 0 auto; box-shadow: none;">
            SPREMI PLAĆANJE
          </button>
        </div>

      </div>

      <div class="row" style="margin-top: 0px;" > 
        <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="curr_placanje_dobav_box" style="padding-top: 0;">
          <!-- OVDJE IDE LISTA PLAĆANJA KOJI NISU JOŠ SPREMLJENI -->
        </div>
      </div>

      <div class="row" style="margin-top: 0px;" > 
        <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="saved_placanje_dobav_box" style="padding-top: 0;">
          <!-- OVDJE IDE LISTA NAČINA PLAĆANJA KOJA JE SPREMLJENA -->
        </div>
      </div>    




      <div class="row" style="margin-top: 5px;" >

        <div class="col-md-3 col-sm-12">
          ${cit_comp(rand_id+`_zaduznica_from_date_dobav`, valid.zaduznica_from_date_dobav, null, "" )}
        </div>

        <div class="col-md-3 col-sm-12">
          ${cit_comp(rand_id+`_zaduznica_to_date_dobav`, valid.zaduznica_to_date_dobav, null, "" )}
        </div>

        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_zaduznica_amount_dobav`, valid.zaduznica_amount_dobav, "") }
        </div>

        <div class="col-md-3 col-sm-12">

          <button class="blue_btn btn_small_text" id="save_debt_dobav" style="margin: 20px 0 0 auto; box-shadow: none;">
            SPREMI ZADUŽNICU
          </button>

          <button class="violet_btn btn_small_text" id="update_debt_dobav" style="margin: 20px 0 0 auto; box-shadow: none; display: none;">
            AŽURIRAJ ZADUŽNICU
          </button>

        </div>

      </div>

      <div class="row" style="margin-top: 0px;" >
        <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="debt_dobav_box" style="padding-top: 0;">
        <!-- OVDJE IDE TABLICA SVIH ZADUŽNICA OD NAS PREMA DOBAVLJAČU -->
        </div>
      </div>      

      <div class="row">


        <div class="col-md-4 col-sm-12">
          ${ cit_comp(rand_id+`_cassa_sconto_days_dobav`, valid.cassa_sconto_days_dobav, "") }
        </div>


        <div class="col-md-4 col-sm-12">
          ${ cit_comp(rand_id+`_cassa_sconto_perc_dobav`, valid.cassa_sconto_perc_dobav, "") }
        </div>

        <div class="col-md-4 col-sm-12">

          <button class="blue_btn btn_small_text" id="save_sconto_dobav" style="margin: 20px 0 0 auto; box-shadow: none;">
            SPREMI SCONTO
          </button>

          <button class="violet_btn btn_small_text" id="update_sconto_dobav" style="margin: 20px 0 0 auto; box-shadow: none; display: none;">
            AŽURIRAJ SCONTO
          </button>

        </div>

      </div>

      <div class="row" style="margin-top: 0px;" >
        <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="sconto_dobav_box" style="padding-top: 0;">
        <!-- OVDJE IDE TABLICA SVIH SCONTO OD NAS PREMA DOBAVLJAČU -->
        </div>
      </div>      


      <div class="row">

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_bonus_amount_min_dobav`, valid.bonus_amount_min_dobav, "") }
        </div>

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_bonus_amount_max_dobav`, valid.bonus_amount_max_dobav, "") }
        </div>
        
        
        <div class="col-md-2 col-sm-12">
          ${cit_comp(rand_id+`_bonus_date_start_dobav`, valid.bonus_date_start_dobav, null, "" )}
        </div>
        
        <div class="col-md-2 col-sm-12">
          ${cit_comp(rand_id+`_bonus_date_end_dobav`, valid.bonus_date_end_dobav, null, "" )}
        </div>
        

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_bonus_perc_dobav`, valid.bonus_perc_dobav, "") }
        </div>


        <div class="col-md-2 col-sm-12">

            <button class="blue_btn btn_small_text" id="save_bonus_dobav" style="margin: 20px 0 0 auto; box-shadow: none;">
              SPREMI BONUS
            </button>

            <button class="violet_btn btn_small_text" id="update_bonus_dobav" style="margin: 20px 0 0 auto; box-shadow: none; display: none;">
              AŽURIRAJ BONUS
            </button>


        </div>
      </div>

      <div class="row">
        <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="god_bonusi_box_dobav" style="padding-top: 20px;">
          <!-- OVDJE IDE LISTA GODIŠNJIH BONUSA TVRTKE DOBAVLJAČA -->    
        </div> 

      </div>



      <br style="float: left; clear: both; width: 100%; display: block;">  

  </div>         
    <!-- KRAJ DOBAV TAB BOX -->


    <div class="cit_tab_box povijest_tab" style="padding: 0; display: none;">

      <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="partner_doc_box" style="padding: 20px 0 0;">
        <!-- OVDJE IDE LISTA SVIH PONUDA NK OPTREMNICA ITD..... -->
      </div> 
      <br>
      <br style="float: left; clear: both; width: 100%; display: block;">

    </div>
    <!-- KRAJ POVIJEST TAB BOX -->

    <div class="cit_tab_box statusi_tab" style="display: none; background: #f7f3ee;" >
      <div class="row" id="${data.id}_statuses_box" style="padding: 0; margin: 0;" >
        <!--OVDJE UBACUJEM STATUSE OD PARTNERA -->
      </div> 

      <br>
      <br style="float: left; clear: both; width: 100%; display: block;">

    </div>  
    
    <!-- KRAJ STATUSI TAB BOX -->

    <div class="cit_tab_box dokumenti_tab" style="padding: 10px; margin-top: 0; ">


      <!--  <h4  style="margin-top: 20px;">NABAVA</h4>  -->

      <div class="row">
        <div class="col-md-11 col-sm-12">
          ${ cit_comp(rand_id+`_partner_sirov`, valid.partner_sirov, null, "") }
        </div>


        <div class="col-md-1 col-sm-12" >
          <a  id="link_to_partner_sirov" href="#" target="_blank" onclick="event.stopPropagation();"
              class="blue_btn btn_small_text small_btn_link_left cit_disable" 
              style="box-shadow: none; margin: 22px auto 0 0; width: 28px; height: 28px;" >

            <i style="font-size: 18px;" class="far fa-external-link-square-alt"></i>

          </a>
        </div>


      </div>


      <div class="row">
        <div class="col-md-11 col-sm-12">
          ${ cit_comp(rand_id+`_partner_product`, valid.partner_product, null, "") }
        </div>


        <div class="col-md-1 col-sm-12" >
          <a  id="link_to_partner_product" href="#" target="_blank" onclick="event.stopPropagation();"
              class="blue_btn btn_small_text small_btn_link_left cit_disable" 
              style="box-shadow: none; margin: 22px auto 0 0; width: 28px; height: 28px;" >

            <i style="font-size: 18px;" class="far fa-external-link-square-alt"></i>

          </a>
        </div>




      </div>    

      <div class="row" >

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_ask_kolicina_1`, valid.ask_kolicina_1, "") }
        </div>

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_ask_kolicina_2`, valid.ask_kolicina_2, "") }
        </div>

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_ask_kolicina_3`, valid.ask_kolicina_3, "") }
        </div>

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_ask_kolicina_4`, valid.ask_kolicina_4, "") }
        </div>

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_ask_kolicina_5`, valid.ask_kolicina_5, "") }
        </div>

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_ask_kolicina_6`, valid.ask_kolicina_6, "") }
        </div>

      </div>                    

      <div class="row" >

        <div class="col-md-6 col-sm-12">
          <div class="row">

            <div class="col-md-4 col-sm-12">
              ${ cit_comp(rand_id+`_doc_lang`, valid.doc_lang, { sifra: `hr`, naziv: `HRVATSKI` },  "HRVATSKI" ) }
            </div>

            <div class="col-md-4 col-sm-12">
              ${ cit_comp(rand_id+`_doc_valuta`, valid.doc_valuta, { "sifra": "DEFV1", "naziv": "Hrvatska", "valuta": "HRK" },  "HRK") }
            </div>

            <div class="col-md-4 col-sm-12">
              ${ cit_comp(rand_id+`_doc_valuta_manual`, valid.doc_valuta_manual, "") }
            </div>

          </div>

        </div> <!--KRAJ LJEVOG STUPCA-->

        <div class="col-md-6 col-sm-12">
          <div class="row">

            <div class="col-md-12 col-sm-12">
              ${ cit_comp(rand_id+`_doc_adresa`, valid.doc_adresa, null,  "" ) }
            </div>

          </div>
        </div> <!--KRAJ DESNOG STUPCA-->

      </div> <!-- KRAJ REDA -->

      <div class="row">


        <div class="col-md-2 col-sm-12">
           ${ cit_comp(rand_id+`_custom_doc_price`, valid.custom_doc_price, "") }
        </div>

        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_ref_doc_sifra`, valid.ref_doc_sifra, "") }
        </div>


        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_doc_est_time`, valid.doc_est_time, null, "") }
        </div>


        <div class="col-md-6 col-sm-12">
          ${ cit_comp(rand_id+`_find_status_for_doc`, valid.find_status_for_doc, null, "") }
        </div>

      </div>

      <div class="row">
        <div class="col-md-12 col-sm-12">
          ${ cit_comp(rand_id+`_doc_komentar`, valid.doc_komentar, data.doc_komentar) }
        </div>
      </div>

      <div class="row">         
        <div class="col-md-12 col-sm-12">
          ${ cit_comp(rand_id+`_otp_adresa`, valid.otp_adresa, null,  "" ) }
        </div>
      </div>


      <div class="row">
        <div  class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" 
              id="selected_statuses_box" style="padding-top: 20px;">
            <!-- OVDJE IDE LISTA SVIH STATUSA NA KOJE ŽELIM POSLATI STATUS ODGOVOR -->    
        </div> 
      </div>  


      <div class="row">
        <div class="col-md-12 col-sm-12" style="display: flex; justify-content: center;">
          <button class="blue_btn btn_small_text" id="add_sirov_to_doc_btn" style="margin: 20px auto; box-shadow: none; max-width: 400px; width: 100%;" >
            DODAJ SIROVINU U DOKUMENT
          </button>
        </div>
      </div>



      <div class="row">
        <div  class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" 
              id="doc_sirovs_list_box" style="padding-top: 20px;">
            <!-- OVDJE IDE LISTA SVIH SIROVINA ZA GENERIRANJE RFQ ILI ZA NARUDŽBU ROBE UNUTAR MODULA PARTNERA -->    
        </div> 
      </div>  

      <div class="row">


        <div class="col-md-2 col-sm-12" >
          <button class="blue_btn btn_small_text" id="offer_dobav_btn" style="margin: 20px auto; box-shadow: none;" >
            ZAHTJEV ZA PONUDU
          </button>
        </div>

        <div class="col-md-2 col-sm-12" >

          <div class="row">
            <div class="col-md-12 col-sm-12" >
              <button class="blue_btn btn_small_text" id="ulazna_ponuda_btn" style="margin: 20px auto; box-shadow: none; min-width: 130px;" >
                PONUDA DOBAV.
              </button>
            </div>
          </div>


          <div class="row">
            <div class="col-md-12 col-sm-12" >
              <button class="violet_btn btn_small_text" id="update_ponuda_files_btn" style="margin: 20px auto; box-shadow: none; min-width: 130px; display: none;" >
                DODAJ DOKUMENTE U PONUDU
              </button>
            </div>
          </div>



          <div class="row">
            <div class="col-md-12 col-sm-12">
              ${ cit_comp(rand_id + `_ulazna_ponuda_sifra`, valid.ulazna_ponuda_sifra, "", "", `margin: 0 auto 0;` ) }
            </div>
          </div>

          <div class="row">
            <div class="col-md-12 col-sm-12">
              ${ cit_comp(rand_id + `_ulazna_ponuda_docs`, valid.ulazna_ponuda_docs, "", "", `margin: 20px auto 0;` ) }
            </div>
          </div>
        </div>

        <div class="col-md-2 col-sm-12" >

          <div class="row">
            <div class="col-md-12 col-sm-12" >
              <button class="blue_btn btn_small_text" id="order_dobav_btn" style="margin: 20px auto; box-shadow: none; min-width: 130px;" >
                NARUDŽBA
              </button>
            </div>
          </div>  

          <div class="row">
            <div class="col-md-12 col-sm-12" id="order_avans_switch_box">
              ${ cit_comp(rand_id+`_order_avans`, valid.order_avans, false ) }
            </div>
          </div>  

        </div>

        <div class="col-md-2 col-sm-12">

          <div class="row">
            <div class="col-md-12 col-sm-12">
              <button class="blue_btn btn_small_text" id="primka_btn" style="margin: 20px auto; box-shadow: none; min-width: 130px;">
                PRIMKA
              </button>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12 col-sm-12">
              <button class="blue_btn btn_small_text" id="primka_prodon_btn" style="margin: 20px auto; box-shadow: none; min-width: 130px; background: #bf0060; ">
                PRIMKA IZ PROIZ.
              </button>
            </div>
          </div>        


          <!--
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <button class="blue_btn btn_small_text" id="primka_approve_btn" style="margin: 20px auto; box-shadow: none; min-width: 130px; background: #bf0060;">
                POTVRDI PRIMKU
              </button>
            </div>
          </div>
          -->

          <!--
          <div class="row" id="primka_partial_switch_box">
            <div class="col-md-12 col-sm-12">
              ${ cit_comp(rand_id+`_primka_partial`, valid.primka_partial, false ) }
            </div>
          </div>
          -->

        </div>

        <div class="col-md-2 col-sm-12">

          <button id="doc_otp_btn" 
                  class="blue_btn btn_small_text" 
                  style="margin: 20px auto; box-shadow: none; min-width: 130px; background: #248e03;" >
            OTPREMNICA
          </button>

        </div>



        <div class="col-md-2 col-sm-12" style="flex-direction: column;">

          <button id="out_bill_btn" 
                  class="blue_btn btn_small_text" 
                  style="margin: 20px auto; box-shadow: none; min-width: 100%; background: #248e03;" >
            IZLAZNI RAČUN ZA KUPCA
          </button>



          <button id="in_bill_btn" 
                  class="blue_btn btn_small_text" 
                  style="margin: 20px auto; box-shadow: none; min-width: 100%; background: #bf0060;" >
            ULAZNI RAČUN OD DOBAVLJAČA
          </button>

        </div>




      </div>
      <!-- KRAJ ROW-a SA BUTTONS -->





      <br style="float: left; clear: both; width: 100%; display: block;">

    </div>
    <!-- KRAJ DOKUMENTI TAB BOX -->
    

    <br style="float: left; clear: both; width: 100%; display: block;">  

    <div class="row" style="margin-top: 30px; margin-bottom: 20px; padding-top: 15px;" >
        <div class="col-md-6 col-sm-12">
         <div id="web_pristupi_box" style="float: left; overflow: hidden; padding-top: 20px;">

         </div>
          <!-- OVDJE STAVLJAM SADRŽAJ IZ TEXT AREA KOJI PRETVORIM U HTML -->
        </div>    

        <div class="col-md-6 col-sm-12">
          ${ cit_comp(rand_id+`_web_pristupi`, valid.web_pristupi, data.web_pristupi) }
        </div>
      </div>

    <h6 style="margin-left: 20px;">Ugovori</h6>
    
    <div class="row">
      
     
      <div class="col-md-4 col-sm-12">
        ${ cit_comp(rand_id+`_ugovor_naziv`, valid.ugovor_naziv, "") }
      </div>


      <div class="col-md-3 col-sm-12">
        ${ cit_comp(rand_id+`_ugovor_date`, valid.ugovor_date, null, "") }
      </div>

      
      
    </div>
    
    <div class="row">
     
      <div class="col-md-12 col-sm-12">
        ${ cit_comp(rand_id+`_ugovor_komentar`, valid.ugovor_komentar, "", null, `min-height: 80px;` ) }
      </div>
    </div>
    
    <div class="row">
     
      <div class="col-md-3 col-sm-12">
        ${ cit_comp(rand_id+`_ugovor_start_date`, valid.ugovor_start_date, null, "") }
      </div>
      
      <div class="col-md-3 col-sm-12">
        ${ cit_comp(rand_id+`_ugovor_end_date`, valid.ugovor_end_date, null, "") }
      </div>
      
      
      <div class="col-md-3 col-sm-12">
        ${ cit_comp(rand_id+`_ugovor_docs`, valid.ugovor_docs, "", "", `margin: 20px auto 0 0;` ) }
      </div>
      
      <div class="col-md-3 col-sm-12">
        <button class="blue_btn btn_small_text" id="save_ugovor" style="margin: 20px auto 0 0; box-shadow: none;">
          SPREMI UGOVOR
        </button>

        <button class="violet_btn btn_small_text" id="update_ugovor" style="margin: 20px auto 0 0; box-shadow: none; display: none;">
          AŽURIRAJ UGOVOR
        </button>
      </div>
      
      
    </div>
    
    <div class="row">
      <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="ugovori_list_box" style="padding-top: 20px;">
        <!-- OVDJE IDE LISTA UGOVORA S OVIM PARTNEROM -->    
      </div> 
    </div>
    
    
    <h6 style="margin-left: 20px;">Tenderi</h6>
    
    <div class="row">
      

      <div class="col-md-4 col-sm-12">
        ${ cit_comp(rand_id+`_tender_naziv`, valid.tender_naziv, "") }
      </div>
      
      
      <div class="col-md-3 col-sm-12">
        ${ cit_comp(rand_id+`_tender_date`, valid.tender_date, null, "") }
      </div>

      <div class="col-md-3 col-sm-12">
        <!--SINGLE SELECT-->
        ${cit_comp(rand_id+`_tender_status`, valid.tender_status, data.tender_status, "")}
      </div>

      <div class="col-md-2 col-sm-12">
        <!--DATE-->
        ${ cit_comp(rand_id+`_tender_rok_prijave`, valid.tender_rok_prijave, null, "") }
      </div>


  
    </div>    
   
    <div class="row">
      <div class="col-md-12 col-sm-12">
        ${ cit_comp(rand_id+`_tender_komentar`, valid.tender_komentar, "", null, `min-height: 80px;` ) }
      </div>
    </div>   
      
       
    <div class="row">
      
      <div class="col-md-3 col-sm-12">
        <!--DATE-->
        ${ cit_comp(rand_id+`_tender_start_date`, valid.tender_start_date, null, "") }
      </div>
      
      <div class="col-md-3 col-sm-12">
        <!--DATE-->
        ${ cit_comp(rand_id+`_tender_end_date`, valid.tender_end_date, null, "") }
      </div>      
      
      <div class="col-md-3 col-sm-12">
        ${ cit_comp(rand_id+`_tender_docs`, valid.tender_docs, "", "", `margin: 20px auto 0 0;` ) }
      </div>

      
      <div class="col-md-3 col-sm-12">
        <button class="blue_btn btn_small_text" id="save_tender" style="margin: 20px auto 0 0; box-shadow: none;">
          SPREMI TENDER
        </button>

        <button class="violet_btn btn_small_text" id="update_tender" style="margin: 20px auto 0 0; box-shadow: none; display: none;">
          AŽURIRAJ TENDER
        </button>
      </div>
      
    </div>
    
    

    

    <div class="row">

      <div class="col-md-3 col-sm-12">
        ${ cit_comp(rand_id+`_tender_products`, valid.tender_products, data.tender_products, "") }
      </div>

    </div>  
     
    <div class="row" style="margin-top: 20px;"> 
      <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="current_tender_products_box" style="padding-top: 0;">
          <!--OVDJE IDE MULTI LISTA SVIH PRODUKATA OD CURRENT TENDERA-->
      </div>
    </div>     
     
     
      
    <div class="row" style="margin-top: 20px;"> 
      <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="tenders_box" style="padding-top: 0;">
          <!--OVDJE IDE LISTA SVIH TENDERA -->
      </div>
    </div>
      


    
    <h6 style="margin-left: 20px; margin-top: 20px;">Načini dostave</h6>
    <div class="row">
      
      <div class="col-md-6 col-sm-12">
       
        <div class="row">
          <div class="col-md-12 col-sm-12">
            ${ cit_comp(rand_id+`_nacini_dostave`, valid.nacini_dostave, null, "") }
          </div>
        </div>
        <div class="row" style="margin-top: 20px;" > 
          <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="nacini_dostave_box" style="padding-top: 0;">
            <!--OVDJE IDE LISTA NAČINA DOSTAVE-->
          </div>
        </div>
        
      </div>       
      
      
    </div>


    <h6 style="margin-left: 20px; margin-top: 50px;">Grupacija</h6>
    <div class="row">

      <div class="col-md-3 col-sm-12">
        ${ cit_comp(rand_id+`_grupacija`, valid.grupacija, data.grupacija, data.grupacija?.name || "" ) }
      </div>
      
      <div class="col-md-2 col-sm-12">
          <button class="blue_btn btn_small_text" id="edit_grupacija_name" style="margin: 20px auto 0 0; box-shadow: none;">
            PROMJENI IME GRUPACIJE
          </button>
      </div>
  
      <div class="col-md-3 col-sm-12">
        ${ cit_comp(rand_id+`_new_grupacija_naziv`, valid.new_grupacija_naziv, "") }
      </div>
  
      <div class="col-md-2 col-sm-12">
          <button class="blue_btn btn_small_text" id="new_grupacija_btn" style="margin: 20px auto 0 0; box-shadow: none;">
            KREIRAJ NOVU GRUPACIJU
          </button>
      </div>
      
      
      <div class="col-md-2 col-sm-12">
          <button class="blue_btn btn_small_text" id="remove_from_grupacija" style="margin: 20px auto 0 0; box-shadow: none; background: #bf0060;">
            IZBACI IZ GRUPACIJE
          </button>
      </div>

      
      
      
    </div>
    
    
    <div class="row">
      <div class="col-md-3 col-sm-12 cit_result_table result_table_inside_gui" id="grupacija_box" style="padding-top: 20px;">
        <!-- OVDJE IDE LISTA SVIH FIRMI UNUTAR GRUPACIJE -->    
      </div> 

    </div>


  </div>  <!-- END CONTAINER FLUID -->
  
</section>
    
</div>

`;


// ` // samo da popravim loše farbanje koda jer stari brackets ne preopznaje optional chaining


  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };
  
  wait_for( `$('#${data.id}').length > 0`, async function() {
    
    $('.cit_tooltip').tooltip();
    

    if ( data._id ) {
      document.title = data.partner_sifra + "--" + data.naziv;
    } else {
      document.title = "PARTNERI VELPROM";  
    };
    
    $(`#cit_page_title h3`).text(`Partneri`);
    $(`html`)[0].scrollTop = 0;
    
    
    
    ask_before_close_register_event();
    
    
    setTimeout( async function() {
    
      var status_data = {
        id: `sirovstatus`+cit_rand(),
        statuses: data.statuses,
        
        statuses_count: data.statuses_count || null,
        
        goto_status: (data.goto_status || null),
        for_module: `partner`,

      };
      
      this_module.status_module = await get_cit_module(`/modules/status/status_module.js`, `load_css`);

      this_module.status_module.create( status_data, $(`#${data.id}_statuses_box`) );

    }, 100);  
    
    
    
    var rand_id = this_module.cit_data[data.id].rand_id;
    
    
    // init google map za adrese
    this_module.init_partner_map();
    
    toggle_global_progress_bar(false);

    console.log(data.id + ' component injected into html');
    
    
    
    this_module.find_sirov = await get_cit_module(`/modules/sirov/find_sirov.js`, null);
    this_module.sirov_module = await get_cit_module(`/modules/sirov/sirov_module.js`, `load_css`);
    this_module.sirov_valid = this_module.sirov_module.valid;
    this_module.status_module = await get_cit_module(`/modules/status/status_module.js`, `load_css`);
    
    
    
    $(`#`+rand_id+`_partner_sirov`).data('cit_props', {
      // !!!findsirov!!! ----> isto kao standardni find sirov samo je ovo SAMO za ovog partnera !!!!
      desc: 'pretraga za odabir sirovine od samo ovog dobavljača ' + rand_id,
      local: false,
      url: '/find_sirov',
      find_in: [
        "sirovina_sifra", 
        "naziv",
        "full_naziv",
        "stari_naziv",
        "povezani_nazivi",
        "detaljan_opis",
        "grupa_flat",  

        "kvaliteta_1",
        "kvaliteta_2", 
        "kvaliteta_mix",

        "kontra_tok_x_tok",
        "val_x_kontraval",
        "alat_prirez",

        "dobavljac_flat", 
        "dobav_naziv", 
        "dobav_sifra", 
        "fsc_flat", 
        "povezani_kupci_flat", 
        "boja",   
        "zemlja_pod_flat",

        "specs_flat",

      ],
      query: { "dobavljac": data._id }, // traži samo sirovine od ovog dobavljača !!!!!!!  
      return: {},

      /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */

      col_widths: [
        1, // "sirovina_sifra",
        2, // "full_naziv",
        2, // "full_dobavljac",
        1, // grupa_naziv
        1, // "boja",
        1, // "cijena",
        1, // "kontra_tok",
        1, // "tok",
        1, // "po_valu",
        1, // "po_kontravalu",
        2, // "povezani_nazivi",

        1, // "pk_kolicina",
        1, // "nk_kolicina",
        1, // "order_kolicina",
        1, // "sklad_kolicina"
        1, // last_status

        4, // spec_docs

      ],
      show_cols: [
        "sifra_link",
        "full_naziv",
        "full_dobavljac",
        "grupa_naziv",
        "boja",
        "cijena",
        "kontra_tok",
        "tok",
        "po_valu",
        "po_kontravalu",
        "povezani_nazivi",

        `pk_kolicina`,
        `nk_kolicina`,
        `order_kolicina`,
        `sklad_kolicina`,
        `last_status`,

        `spec_docs`, 

      ],
      custom_headers: [
        `ŠIFRA`,  // "sirovina_sifra",
        `NAZIV`,  // "full_naziv",
        `DOBAV.`,  // "full_dobavljac",
        `GRUPA`,  // "grupa_naziv",
        `BOJA`,  // "boja",
        `CIJENA`,  // "cijena",
        `KONTRA TOK`,  // "kontra_tok",
        `TOK`,  // "tok",
        `VAL`,  // "po_valu",
        `KONTRA VAL`,  // "po_kontravalu",
        `POVEZANO`,  // "povezani_nazivi",

        `PK RESERV`,  // "pk_kolicina",
        `NK RESERV`,  // "nk_kolicina",
        `NARUČENO`,  // "order_kolicina",
        `SKLADIŠTE`,  // "sklad_kolicina",
        `STATUS`,

        `DOCS`, 

      ],

      format_cols: {

        "sifra_link": "center",
        "grupa_naziv": "center",
        "boja": "center",
        "cijena": this_module.sirov_valid.cijena.decimals,
        "kontra_tok": this_module.sirov_valid.kontra_tok.decimals,
        "tok": this_module.sirov_valid.tok.decimals,
        "po_valu": this_module.sirov_valid.po_valu.decimals,
        "po_kontravalu": this_module.sirov_valid.po_kontravalu.decimals,

        "pk_kolicina": this_module.sirov_valid.pk_kolicina.decimals,
        "nk_kolicina": this_module.sirov_valid.nk_kolicina.decimals,
        "order_kolicina": this_module.sirov_valid.order_kolicina.decimals,
        "sklad_kolicina": this_module.sirov_valid.sklad_kolicina.decimals,

      },

      filter: this_module.find_sirov.find_sirov_filter,

      show_on_click: false,
      cit_run: async function(state) {
        
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          
          $("#link_to_partner_sirov").attr(`href`, `#`); 
          $("#link_to_partner_sirov").addClass(`cit_disable`); 
          
          return;
        };

        
        $("#link_to_partner_sirov").attr(`href`, `#sirov/${state._id}/status/null`); 
        $('#'+current_input_id).val( state.sirovina_sifra + "--" + state.full_naziv  + "--" + (state.dobavljac?.naziv || "") );
        window.current_partner_sirov = state;
        
      },
    });
    
    
    
    $(`#`+rand_id+`_partner_product`).data('cit_props', {
      // !!!findsirov!!! ----> isto kao standardni find sirov samo je ovo SAMO za ovog partnera !!!!
      desc: 'pretraga za odabir VELPROM PRODUCTA za samo ovog kupca ' + rand_id,
      local: false,
      url: '/find_sirov',
      find_in: [
        "sirovina_sifra", 
        "naziv",
        "full_naziv",
        "stari_naziv",
        "povezani_nazivi",
        "detaljan_opis",
        "grupa_flat",  

        "kvaliteta_1",
        "kvaliteta_2", 
        "kvaliteta_mix",

        "kontra_tok_x_tok",
        "val_x_kontraval",
        "alat_prirez",

        "dobavljac_flat", 
        "dobav_naziv", 
        "dobav_sifra", 
        "fsc_flat", 
        "povezani_kupci_flat", 
        "boja",   
        "zemlja_pod_flat",

        "specs_flat",

      ],
      query: { "list_kupaca": data._id }, // traži samo proizvode tj robu koje imaju _id ovog kupca u array-u list kupaca !!!!!!!  
      return: {},

      /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */

      col_widths: [
        1, // "sirovina_sifra",
        2, // "full_naziv",
        2, // "full_dobavljac",
        1, // grupa_naziv
        1, // "boja",
        1, // "cijena",
        1, // "kontra_tok",
        1, // "tok",
        1, // "po_valu",
        1, // "po_kontravalu",
        2, // "povezani_nazivi",

        1, // "pk_kolicina",
        1, // "nk_kolicina",
        1, // "order_kolicina",
        1, // "sklad_kolicina"
        1, // last_status

        4, // spec_docs

      ],
      show_cols: [
        "sifra_link",
        "full_naziv",
        "full_dobavljac",
        "grupa_naziv",
        "boja",
        "cijena",
        "kontra_tok",
        "tok",
        "po_valu",
        "po_kontravalu",
        "povezani_nazivi",

        `pk_kolicina`,
        `nk_kolicina`,
        `order_kolicina`,
        `sklad_kolicina`,
        `last_status`,

        `spec_docs`, 

      ],
      custom_headers: [
        `ŠIFRA`,  // "sirovina_sifra",
        `NAZIV`,  // "full_naziv",
        `DOBAV.`,  // "full_dobavljac",
        `GRUPA`,  // "grupa_naziv",
        `BOJA`,  // "boja",
        `CIJENA`,  // "cijena",
        `KONTRA TOK`,  // "kontra_tok",
        `TOK`,  // "tok",
        `VAL`,  // "po_valu",
        `KONTRA VAL`,  // "po_kontravalu",
        `POVEZANO`,  // "povezani_nazivi",

        `PK RESERV`,  // "pk_kolicina",
        `NK RESERV`,  // "nk_kolicina",
        `NARUČENO`,  // "order_kolicina",
        `SKLADIŠTE`,  // "sklad_kolicina",
        `STATUS`,

        `DOCS`, 

      ],

      format_cols: {

        "sifra_link": "center",
        "grupa_naziv": "center",
        "boja": "center",
        "cijena": this_module.sirov_valid.cijena.decimals,
        "kontra_tok": this_module.sirov_valid.kontra_tok.decimals,
        "tok": this_module.sirov_valid.tok.decimals,
        "po_valu": this_module.sirov_valid.po_valu.decimals,
        "po_kontravalu": this_module.sirov_valid.po_kontravalu.decimals,

        "pk_kolicina": this_module.sirov_valid.pk_kolicina.decimals,
        "nk_kolicina": this_module.sirov_valid.nk_kolicina.decimals,
        "order_kolicina": this_module.sirov_valid.order_kolicina.decimals,
        "sklad_kolicina": this_module.sirov_valid.sklad_kolicina.decimals,

      },

      filter: this_module.find_sirov.find_sirov_filter,

      show_on_click: false,
      cit_run: async function(state) {
        
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          $("#link_to_partner_product").attr(`href`, `#`);
          return;
        };
        
        $("#link_to_partner_product").attr(`href`, `#sirov/${state._id}/status/null`); 
        $('#'+current_input_id).val( state.sirovina_sifra + "--" + state.full_naziv  + "--" + (state.dobavljac?.naziv || "") );
        window.current_partner_sirov = state;
        
      },
    });
        
    
    
    $(`#`+rand_id+`_doc_lang`).data('cit_props', {
      desc: 'odabir jezika za generiranje dokumenta order dobavljacu  u sirov module',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'lang',
      cit_run: function(state) {

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.doc_lang = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.doc_lang = state;
        };

      },
    });  

    
    $(`#`+rand_id+`_doc_valuta`).data('cit_props', {
      desc: 'odabir valute u kojoj će e generirati dokument sa cijenama u toj valuti :)',
      local: true,
      show_cols: ['naziv', 'valuta'],
      return: {},
      show_on_click: true,
      list: 'valute',
      cit_run: function(state) {
       
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.doc_valuta = null;
        } else {
          $('#'+current_input_id).val(state.valuta);
          data.doc_valuta = state;
        };
        
      },
      
    });
    

    $('#'+rand_id+'_doc_est_time').data(`cit_run`, function choose_doc_est_time(state, this_input) {
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      data.doc_est_time = null;
      console.log(`doc_est_time: `, null );
      return;
    };
    
    data.doc_est_time = state;
    console.log(`new doc_est_time: `, new Date(state));
    
  });
    
    
    $(`#`+rand_id+`_doc_adresa`).data('cit_props', {
      desc: 'odabir adrese iz liste kontakata za generiranje dokumenta order dobavljacu u PARTNER module',
      local: true,
      show_cols: ['full_adresa'],
      return: {},
      show_on_click: true,
      list: function() {
        return $(`#svi_kontakti_box`).data(`cit_result`);
      },
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.doc_adresa = null;
        } else {
          $('#'+current_input_id).val(state.full_adresa);
          data.doc_adresa = state;
        };
        
      },
    });  
    
    
    $(`#`+rand_id+`_otp_adresa`).data('cit_props', {
      desc: 'odabir _otp_adresa iz liste kontakata za generiranje otpremnice',
      local: true,
      show_cols: ['full_adresa'],
      return: {},
      show_on_click: true,
      list: function() {
        return $(`#svi_kontakti_box`).data(`cit_result`);
      },
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.otp_adresa = null;
        } else {
          $('#'+current_input_id).val(state.full_adresa);
          data.otp_adresa = state;
        };
        
      },
    });  
        
    


    $(`#`+rand_id+`_find_status_for_doc`).data('cit_props', {
      //!!!findstatus!!!
      desc: 'pretraga statusa u modulu PARTNER za kreiranje popisa status na koje dogovaram',
      local: false,
      url: '/find_status',
      
      find_in: [
        "sirov_flat",
        "full_product_name",
        "dep_to_flat",
        "to_flat",
        "dep_from_flat",
        "from_flat",
        "komentar",
        "status_tip_flat",
      ],
      query: {
        dep_to: { sifra: "NAB", naziv: "Nabava" },
        start: { $gte: Date.now() - 1000*60*60*24*180 }, // samo zadnjih 6 mjeseci
        branch_done: {$ne: true },
      },
      return: {},
      show_cols: [ 
      
        "product_link",
        "status_tip_naziv",
        "komentar",
        "from_full_name",
        "to_full_name",
        "est_deadline_hours",
        "work_hours",
        "est_deadline_date",
        "rok_isporuke",
        "start",
        "end",
        "duration",
        "late",
        "docs",
        'button_delete',
      ],
      custom_headers: [
      
        "STAVKA",
        "STATUS",
        "KOMENTAR",
        "OD",
        "PREMA",
        "PROCJENA SATI",
        "UPISANO SATI",
        "PROCJENA DATUM",
        "ROK ISPORUKE",
        "START",
        "END",
        "TRAJANJE SATI",
        "KASNI SATI",
        "DOCS",
        'OBRIŠI',
        
      ],
      col_widths: [
      
        1.7, // "Stavka"
        1.5, // "Status",
        3, //"Komentar",
        1, //"Od",
        1, //"Prema",
        0.7, //"Procjena sati",
        0.7, // "Upisano sati",
        1, // "Procjena datum",
        1, // "Rok isporuke",
        1, //"Start",
        1, //"End",
        0.7, //"Trajanje",
        0.7, //"Kasni",
        3.8, //"Docs",
        0.5, //'OBRIŠI,
      ],
      
      format_cols: this_module.status_module.format_cols_obj,
      filter: [ this_module.status_module.statuses_filter, this_module.sirov_module.remove_duplicate_sirov_statuses ],
      show_on_click: true,
      list_width: 700,
      
      cit_run: function (state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          
        } else {
          
          // var komentar_no_br = state.komentar.replace(/\<br\>/ig, " ");
          // $('#'+current_input_id).val( komentar_no_br );
          
          if ( !data.selected_statuses_in_sirov ) data.selected_statuses_in_sirov = [];
          
          data.selected_statuses_in_sirov = upsert_item(data.selected_statuses_in_sirov, state, `sifra`);
          
          data.selected_statuses_in_sirov = cit_deep( data.selected_statuses_in_sirov );  
            
        };
        
        this_module.sirov_module.make_selected_statuses_list(data, this_module);
        
      },
      
      
    }); 
    
        
    $('#offer_dobav_btn').off("click");
    $('#offer_dobav_btn').on("click", this_module.partner_offer_dobav ); 
  
    
    $('#ulazna_ponuda_btn').off("click");
    $('#ulazna_ponuda_btn').on("click", this_module.save_ulazna_ponuda ); 
  
  
      
    $('#update_ponuda_files_btn').off("click");
    $('#update_ponuda_files_btn').on("click", this_module.update_ponuda_files ); 
  
    
    $('#order_dobav_btn').off("click");
    $('#order_dobav_btn').on("click", this_module.partner_ORDER_dobav ); 
  
    $('#'+rand_id+'_order_avans').data(`cit_run`, function(state) { data.order_avans = state; console.log(data) } );
  
    
    $('#primka_btn').off("click");
    $('#primka_btn').on("click", this_module.partner_PRIMKA_dobav ); 
  
    
    $('#primka_prodon_btn').off("click");
    $('#primka_prodon_btn').on("click", this_module.prodon_primka ); 
  
    
    
    $('#doc_otp_btn').off("click");
    $('#doc_otp_btn').on("click", this_module.doc_otp ); 
      
        
    $('#out_bill_btn').off("click");
    $('#out_bill_btn').on("click", this_module.partner_out_bill ); 
  
    
    
    // sve kompanije u grupaciji
    this_module.make_grupacija_list(data);
    

    $(`#`+rand_id+`_grupacija`).data('cit_props', {

      desc: 'prikači kompaniju u grupaciju u partner modulu',
      local: false,

      url: '/find_grupacija',
      find_in: [ 'name', 'companies_flat' ],
      
      /* return: { name: 1, companies: 1, company_links: 1 }, */
        /* _id ne moram upisivati width jer ga automatski preskače kad radi result tablicu */
        /* ALI ZATO moram upisati nula za companies jer ih ne želim prikazati i pošto nisu string nego objekt  */
      col_widths: [ 2, 3 ],
      filter: function(items, jQuery) {

        var $ = jQuery;
        var new_items = [];
        $.each(items, function(index, item) {
          var companies_html = ``;
          $.each(item.companies, function(comp_ind, comp) {
            companies_html += 
`<a style="text-align: center;" href="#partner/${comp._id}/status/null" target="_blank" onClick="event.stopPropagation();">${comp.name}</a>`;
          });
          new_items.push({ 
            _id: item._id,
            name: item.name,
            companies: item.companies,
            company_links: companies_html 
          });
        });

        return new_items;
      },


      show_cols: ['name', 'company_links'],
      format_cols: { 'name': "center", 'company_links': "center" },
      query: {},
      show_on_click: false,
      list_width: 500,

      cit_run: async function (state) {

        var state = cit_deep(state);
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];

        // ako ova kompanija još nije saved
        // onda nemoj ići dalje

        if ( data._id == null ) {
          popup_warn(`Prije odabira grupe potrebno je spremiti ovu kompaniju !`);
          return;
        };


        if ( state == null ) {
          $('#'+current_input_id).val("");
          data.grupacija = null;
          console.log(data);
          return;
        };

        
        
        
              var popup_text =
`Jeste li sigurni da želite prebaciti ovu tvrtku u grupaciju koju ste izabrali ???`;
      
        
        function choose_grupacija_yes() {
          
          
          var old_grupacija_id = null

        // ako već ima grupaciju i id od grupacije koja je izabrana na listi nije ista kao current grupacija
        if (data.grupacija && data.grupacija._id !== state._id ) {
          old_grupacija_id = data.grupacija._id;
        };

        data.grupacija = state;

        if ( !data.grupacija.companies ) data.grupacija.companies = []; 

        var this_company_obj = {
          name:  data.naziv,
          _id: data._id,
        };

        

        data.grupacija.companies = upsert_item(data.grupacija.companies,  this_company_obj, "_id" );
        data.grupacija.companies_flat = JSON.stringify(data.grupacija.companies);
        data.grupacija_flat = JSON.stringify(data.grupacija);


        // sve companies u grupi
        this_module.make_grupacija_list(data);

        $('#'+current_input_id).val(state.name);
        console.log(data);

        var grupacija = cit_deep(data.grupacija);

        // ova dva propertija samo dodajem u grupaciju iako ne trebaju tamo biti 
        // samo zato da pošaljem request
        // KASNIJE IH OBRIŠEM NA SERVERU !!!!!
        grupacija.old_grupacija_id = old_grupacija_id;
        grupacija.company_id = data._id;

        $.ajax({
          headers: {
            'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
          },
          type: "POST",
          cache: false,
          url: `/update_grupacija`,
          data: JSON.stringify(grupacija),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
        .done(function (result) {
          console.log(result);
          toggle_global_progress_bar(false);

          if ( result.success == true ) {
            
            popup_msg(`Ova Tvrtka je dodana odabranoj Grupaciji`);
            
            setTimeout(function () {
              $(`#save_partner_btn`).trigger(`click`);
            }, 200);
            
            
          } else {
            if ( result.msg ) popup_error(result.msg);
            if ( !result.msg ) popup_error(`Greška prilikom dodavanja tvrtke u ovu grupaciju!`);
          };

        })
        .fail(function (error) {
          toggle_global_progress_bar(false);
          console.log(error);
          popup_error(`Došlo je do greške na serveru prilikom dodavanja tvrtki u ovu grupaciju!`);
        });
          
          
          
          
        };

        function choose_grupacija_no() {
          show_popup_modal(false, popup_text, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, popup_text, null, 'yes/no', choose_grupacija_yes, choose_grupacija_no, null);
        
        
        
        
        
        
        
      },

    });
    

    $(`#edit_grupacija_name`).off('click');
    $(`#edit_grupacija_name`).on('click', function() {

      if ( $(`#`+rand_id+`_grupacija`).val() == "" ) {
        popup_warn(`Ime ne može biti prazno!!!`);
        return;
      };

      if ( !data.grupacija || !data.grupacija._id ) {
        popup_warn(`Trenutno niste izabrali niti jednu grupaciju !!!`);
        return;
      };

      toggle_global_progress_bar(true);

      $.ajax({
        headers: {
          'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
        },
        type: "POST",
        cache: false,
        url: `/edit_grupacija_name`,
        data: JSON.stringify({
          new_name:  $(`#`+rand_id+`_grupacija`).val(),
          id: data.grupacija._id,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done(function (result) {

        console.log(result);

        toggle_global_progress_bar(false);

        if ( result.success == true ) {
          // update lokalno unutar ovaj partner data
          data.grupacija.name = $(`#`+rand_id+`_grupacija`).val();
          data.grupacija_flat = JSON.stringify(data.grupacija);

          popup_msg(`Ime Grupacije je promjenjeno!`);
        } else {
          popup_error(result.msg);
        };

      })
      .fail(function (error) {
        toggle_global_progress_bar(false);
        console.log(error);
        popup_error(`Došlo je do greške na serveru prilikom promjene imena Grupacije`);
      });


    });

    
    $(`#new_grupacija_btn`).off('click');
    $(`#new_grupacija_btn`).on('click', async function() {

      if ( $(`#`+rand_id+`_new_grupacija_naziv`).val() == "" ) {
        popup_warn(`Ime ne može biti prazno!!!`);
        return;
      };

      var old_grupacija_id = null
      // ako već ima grupaciju
      if ( data.grupacija && data.grupacija._id ) {
        old_grupacija_id = data.grupacija._id;
      };

      var company_id = data._id;
      
      if ( !company_id ) {
        popup_warn(`Potrebno spremiti partnera prije kreiranja grupacije !!!!`);
        return;
      };
      
      
      
        var popup_text =
`Jeste li sigurni da želite KREIRATI GRUPACIJU:<br>${$(`#`+rand_id+`_new_grupacija_naziv`).val()}<br> i postaviti ovu tvrtku u tu grupaciju ???`;
      
        
        function new_grupacija_yes() {
          
      
          toggle_global_progress_bar(true);

          $.ajax({
            headers: {
              'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
            },
            type: "POST",
            cache: false,
            url: `/new_grupacija`,
            data: JSON.stringify({
              name:  $(`#`+rand_id+`_new_grupacija_naziv`).val(),
              company_id: data._id,
              company_naziv: data.naziv,
              old_grupacija_id: old_grupacija_id,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json"
          })
          .done(function (result) {

            console.log(result);

            toggle_global_progress_bar(false);

            if ( result.success == true ) {
              // update lokalno unutar partner data
              data.grupacija = result.grupacija;
              data.grupacija_flat = JSON.stringify(data.grupacija);

              // upiši ime nove grupacije u grupacija input polje
              $(`#`+rand_id+`_grupacija`).val( $(`#`+rand_id+`_new_grupacija_naziv`).val() );
              // obriši ime iz polja za kreiranje nove grupacije
              $(`#`+rand_id+`_new_grupacija_naziv`).val("");

              this_module.make_grupacija_list(data);
              popup_msg(`Nova grupacija je kreirana i ova tvrtka je dodana u grupaciju !`);


              setTimeout( function() { $(`#save_partner_btn`).trigger(`click`); }, 200 );



            } else {
              popup_error(result.msg);
            };

          })
          .fail(function (error) {
            toggle_global_progress_bar(false);
            console.log(error);
            popup_error(`Došlo je do greške na serveru prilikom  kreiranja nove grupacije`);
          });
          
          
        };

        function new_grupacija_no() {
          show_popup_modal(false, popup_text, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, popup_text, null, 'yes/no', new_grupacija_yes, new_grupacija_no, null);
      

    });

    
    
    
    $(`#remove_from_grupacija`).off('click');
    $(`#remove_from_grupacija`).on('click', async function() {

      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id = data.rand_id;


      if ( !data.grupacija || !data.grupacija._id ) {
        popup_warn(`Trenutno niste izabrali niti jednu grupaciju !!!`);
        return;
      };
      
      
        var popup_text =
`Jeste li sigurni da želite<br>IZBACITI OVU TVRTKU IZ GRUPACIJE:<br>${ data.grupacija?.name || ""  } ???`;


      function remove_grupacija_yes() {


        toggle_global_progress_bar(true);

        $.ajax({
          headers: {
            'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
          },
          type: "POST",
          cache: false,
          url: `/remove_from_grupacija`,
          data: JSON.stringify({
            partner_id:  data._id || null,
            id: data.grupacija?._id || null,
          }),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
        .done(function (result) {

          console.log(result);

          toggle_global_progress_bar(false);

          if ( result.success == true ) {
            // update lokalno unutar sirovina data
            data.grupacija = null;
            data.grupacija_flat = null;

            $(`#`+rand_id+`_grupacija`).val(``);

            popup_msg(`Ovaj partner je izbačen iz grupacije !!!`);

            // sve sirovine u grupi
            this_module.make_grupacija_list(data);


            setTimeout( function() { $(`#save_partner_btn`).trigger(`click`); }, 200 );


          } else {
            popup_error(result.msg);
          };

        })
        .fail(function (error) {
          toggle_global_progress_bar(false);
          console.log(error);
          popup_error(`Došlo je do greške na serveru prilikom izbacivanja ovog partnera iz grupacije !!!`);
        });



      };

      function remove_grupacija_no() {
        show_popup_modal(false, popup_text, null );
      };

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', remove_grupacija_yes, remove_grupacija_no, null);

      
      
    }); // kraj click na gumb remove from grupacija
        
    
    
    
    
    // WEB PRISTUPI
    this_module.web_pristupi_to_html(rand_id);
    
    this_module.make_iban_list(data);

    this_module.make_saved_placanja_kupac_list(data);
    this_module.make_saved_placanja_dobav_list(data);
    
    
    $(`#`+rand_id+`_nacin_placanja_kupac`).data('cit_props', {
      desc: 'odabir nacina placanja na padajucoj listi za naše kupce',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'pay_type',
      cit_run: function (state) {

        console.log(state);

        // deep copy od state
        var state = cit_deep(state);

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          return;
        };

        $('#'+current_input_id).val(state.naziv);
          data.nacin_placanja_kupac = state;

      },
  
      
    });
    
    $(`#`+rand_id+`_time_placanja_kupac`).data('cit_props', {
      desc: 'odabir TIME placanja na padajucoj listi za naše kupce',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'pay_times',
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id = this_module.cit_data[this_comp_id].rand_id;
            
        var state = cit_deep(state);
        
        if ( state == null ) {
          $('#'+current_input_id).val("");
          data.time_placanja_kupac = null;
          return;
        };
        
        data.time_placanja_kupac = state;
        
        $('#'+current_input_id).val(state.naziv);
        console.log(data);
        
      },
       
       
    });
    
    $(`#add_placanje_kupac`).off('click');
    $(`#add_placanje_kupac`).on('click', function() {
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id = this_module.cit_data[this_comp_id].rand_id;

      if ( !data.curr_placanje_kupac ) data.curr_placanje_kupac = [];
      
      // ako je perc plaćanja prazan stavi da auto bude 100% !!!!!!
      if ( !data.perc_placanja_kupac ) data.perc_placanja_kupac = 100;
      
      
      if ( !data.time_placanja_kupac || !data.nacin_placanja_kupac ) {
        popup_warn(`Potrebno je upisati postotak te izabrati način plaćanja i vrijeme plaćanja.<br>Valuta plaćanja nije obavezna, ali ako je ne upišete onda će biti upisana nula!!!`);
        return;
      };
      
      data.curr_placanje_kupac.push({ 
        sifra: `payrow`+cit_rand(),
        perc: data.perc_placanja_kupac,
        type: data.nacin_placanja_kupac, 
        type_naziv: data.nacin_placanja_kupac.naziv,
        event: data.time_placanja_kupac,
        event_naziv: data.time_placanja_kupac.naziv,
        valuta: data.rok_placanja_kupac || 0 
      });
     
      console.log(data.curr_placanje_kupac);
      
      this_module.make_curr_placanje_kupac_list(data);
      
      data.perc_placanja_kupac = null;
      data.time_placanja_kupac = null;
      data.nacin_placanja_kupac = null;
      data.rok_placanja_kupac = null;
      $(`#` + rand_id + `_perc_placanja_kupac`).val(``);
      $(`#` + rand_id + `_time_placanja_kupac`).val(``);
      $(`#` + rand_id + `_nacin_placanja_kupac`).val(``);
      $(`#` + rand_id + `_rok_placanja_kupac`).val(``);
      
      
    });
    
    $(`#save_placanje_kupac`).off('click');
    $(`#save_placanje_kupac`).on('click', function() {
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id = this_module.cit_data[this_comp_id].rand_id;

      if ( !data.placanje_kupac ) data.placanje_kupac = [];
      
      if ( !data.curr_placanje_kupac || data.curr_placanje_kupac?.length == 0 ) return;
      
      
      var new_placanje = {
        sifra: `pay`+cit_rand(),
        time: Date.now(),
        def: cit_deep( data.curr_placanje_kupac ),
      };
      
      data.placanje_kupac.push(new_placanje);
      console.log(data.placanje_kupac);
      
      data.curr_placanje_kupac = null;
      $( "#curr_placanje_kupac_box" ).html(``);
      this_module.make_saved_placanja_kupac_list(data);
      
      
    });


    $(`#`+rand_id+`_nacin_placanja_dobav`).data('cit_props', {
      desc: 'odabir nacina placanja na padajucoj listi za naše dobavljaca',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'pay_type',
      cit_run: function (state) {

        console.log(state);

        // deep copy od state
        var state = cit_deep(state);

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          return;
        };

        $('#'+current_input_id).val(state.naziv);
          data.nacin_placanja_dobav = state;

      },


    });

    $(`#`+rand_id+`_time_placanja_dobav`).data('cit_props', {
      desc: 'odabir TIME placanja na padajucoj listi za naše dobavljaca',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'pay_times',
      cit_run: function(state) {

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id = this_module.cit_data[this_comp_id].rand_id;

        var state = cit_deep(state);

        if ( state == null ) {
          $('#'+current_input_id).val("");
          data.time_placanja_dobav = null;
          return;
        };

        data.time_placanja_dobav = state;

        $('#'+current_input_id).val(state.naziv);
        console.log(data);

      },


    });

    $(`#add_placanje_dobav`).off('click');
    $(`#add_placanje_dobav`).on('click', function() {

      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id = this_module.cit_data[this_comp_id].rand_id;

      if ( !data.curr_placanje_dobav ) data.curr_placanje_dobav = [];

      // ako je perc plaćanja za dobavljača prazan onda stavi da bude 100% automatski !!!!!!!!!!!!
      if ( !data.perc_placanja_dobav ) data.perc_placanja_dobav = 100;
      
      if ( !data.time_placanja_dobav || !data.nacin_placanja_dobav ) {
        popup_warn(`Potrebno je upisati postotak te izabrati način plaćanja i vrijeme plaćanja.<br>Valuta plaćanja nije obavezna, ali ako je ne upišete onda će biti upisana nula!!!`);
        return;
      };

      data.curr_placanje_dobav.push({ 
        sifra: `payrow`+cit_rand(),
        perc: data.perc_placanja_dobav,
        type: data.nacin_placanja_dobav, 
        type_naziv: data.nacin_placanja_dobav.naziv,
        event: data.time_placanja_dobav,
        event_naziv: data.time_placanja_dobav.naziv,
        valuta: data.rok_placanja_dobav || 0 
      });

      console.log(data.curr_placanje_dobav);

      this_module.make_curr_placanje_dobav_list(data);

      data.perc_placanja_dobav = null;
      data.time_placanja_dobav = null;
      data.nacin_placanja_dobav = null;
      data.rok_placanja_dobav = null;
      $(`#` + rand_id + `_perc_placanja_dobav`).val(``);
      $(`#` + rand_id + `_time_placanja_dobav`).val(``);
      $(`#` + rand_id + `_nacin_placanja_dobav`).val(``);
      $(`#` + rand_id + `_rok_placanja_dobav`).val(``);


    });

    $(`#save_placanje_dobav`).off('click');
    $(`#save_placanje_dobav`).on('click', function() {

      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id = this_module.cit_data[this_comp_id].rand_id;

      if ( !data.placanje_dobav ) data.placanje_dobav = [];

      if ( !data.curr_placanje_dobav ) return;


      var new_placanje = {
        sifra: `pay`+cit_rand(),
        time: Date.now(),
        def: cit_deep( data.curr_placanje_dobav ),
      };

      data.placanje_dobav.push(new_placanje);
      console.log(data.placanje_dobav);

      data.curr_placanje_dobav = null;
      $( "#curr_placanje_dobav_box" ).html(``);
      this_module.make_saved_placanja_dobav_list(data);

    });

    
    
    $(`#`+rand_id+`_status`).data('cit_props', {
      desc: 'odabir u kojem se statusu nalazi partner -- aktivan, u stečaju i slično',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'partner_status',
      cit_run: function (state) {
        
        var state = cit_deep(state);
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        
        
        if ( state == null ) {
          $('#'+current_input_id).val("");
          data.status = null;
          return;
        };
        

        data.status = state;
        data.status_flat = JSON.stringify(data.status);
        
        $('#'+current_input_id).val(state.naziv);
        console.log(data);
      },
    });
    
    
    $(`#`+rand_id+`_pozicija_kontakta`).data('cit_props', {
      desc: 'odabir pozicije u tvrtki',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'pozicija_kontakta',
      cit_run: function (state) {
        
        
        if ( !window.current_kontakt ) window.current_kontakt = {};
        
        if ( state == null ) {
          $('#'+current_input_id).val("");
          if ( window.current_kontakt ) window.current_kontakt.pozicija_kontakta = null;
          return;
        };
        
        var state = cit_deep(state);
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        
        window.current_kontakt.pozicija_kontakta = state;
        $('#'+current_input_id).val(state.naziv);
        console.log(data);
      },
    });
    
    
    $('#'+rand_id+'_kupac').data(`cit_run`, function(state) { 
      data.kupac = state; 
      if ( state == true ) $('#partner_tab_kupac').css(`display`, `flex`);
      if ( !state ) $('#partner_tab_kupac').css(`display`, `none`);
      $('#partner_tab_adrese').trigger(`click`);
    });
    
    $('#'+rand_id+'_dobavljac').data(`cit_run`, function(state) { 
      data.dobavljac = state; 
      if ( state == true ) $('#partner_tab_dobav').css(`display`, `flex`);
      if ( !state ) $('#partner_tab_dobav').css(`display`, `none`);
      $('#partner_tab_adrese').trigger(`click`);
    });
    
    $('#'+rand_id+'_fiz_osoba').data(`cit_run`, function(state) { data.fiz_osoba = state; } );
    $('#'+rand_id+'_web_kupac').data(`cit_run`, function(state) { data.web_kupac = state; } );
    $('#'+rand_id+'_ino_kupac').data(`cit_run`, function(state) { data.ino_kupac = state; } );
    
    $('#'+rand_id+'_uzima_visak').data(`cit_run`, function(state) { data.uzima_visak = state; } );
    $('#'+rand_id+'_placa_visak').data(`cit_run`, function(state) { data.placa_visak = state; } );
    
    
    
    
    $(`#`+rand_id+`_iban_bank`).data('cit_props', {
      desc: 'odabir banke za upisivanje ibana',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'banks',
      cit_run: this_module.choose_bank,
    });
    

    this_module.make_def_pay_list(data);
    
    
    
    $(`#`+rand_id+`_definicija_valute`).data('cit_props', {
      desc: 'odabir valute kod definiranja kako se plaća tom partneru - ako je strana valute onda se određuje jel je tečaj fixni ili je u trenutku plaćanja',
      local: true,
      show_cols: ['naziv', 'valuta'],
      return: {},
      show_on_click: true,
      list: 'valute',
      cit_run: this_module.choose_valutu,
    });

    
    this_module.make_adrese_list(data); 
    this_module.make_adresa_kontakt_list(data)

    
    $(`#`+rand_id+`_vrsta_adrese`).data('cit_props', {
      desc: 'odabir vrste adrese kod upisa podataka adrese u partneru',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'address_type',
      cit_run: this_module.choose_vrsta_adrese,
    });
    
    // KONTAKTI TABLICA
    this_module.make_kontakt_list(data);
    
    // NAČINI DOSTAVE
    var multi_widths_dostava = { naziv: 1, delete_dostava: 1 };
    var col_widths_sum_dostava = 0;
    $.each(multi_widths_dostava, function(width_key, width) {
      col_widths_sum_dostava += Number(width);
    });
    
    $(`#`+rand_id+`_nacini_dostave`).data('cit_props', {
      
      desc: 'unutar partnera  - odabir nacina DOSTAVE drop list',
      local: true,
      /*col_widths: [70, 15, 15],*/
      show_cols: ['naziv' ], // 'button_edit', 'button_delete' ----> ovo moram ukljuciti ako želim da se prikaže !!!!!!
      return: {},
      show_on_click: true,
      list: 'nacini_dostave',
      cit_run: this_module.choose_nacin_dostave,
      
      /* sve ispod su zapravo propsi za listu koja se kreira kad selektiraš red u drop listi */
      multi_list: this_module.cit_data[id].nacini_dostave,
      multi_list_parent: `#nacini_dostave_box`,
      multi_col_order: [ 'naziv', 'delete_dostava'],
      multi_widths: multi_widths_dostava,
      add_cols: [
        
        function(row_id) { 
          var prop_name = `delete_dostava`;
          var header = `<div class="multi_header_cell" style="width: ${multi_widths_dostava.delete_dostava/col_widths_sum_dostava*100}%;">OBRIŠI</div>`;
          var cell = 
            `
            <div class="multi_cell" style="width: ${multi_widths_dostava.delete_dostava/col_widths_sum_dostava*100}%;" data-row_id="${row_id}" >
              <div class="multi_cell_delete_btn multi_list_btn"> <i class="fal fa-times"></i> </div> 
            </div>
            `;

          return {
            prop_name:  prop_name,
            header: header,
            cell: cell
          }
          
        },
      ]
      
    });
    var props = $(`#`+rand_id+`_nacini_dostave`).data('cit_props');
    create_multi_list( rand_id+`_nacini_dostave`, props );
    wait_for(`$('#${rand_id}_nacini_dostave_multi_list').length > 0`, function() {
      this_module.register_nacini_dostave_events(rand_id+`_nacini_dostave`);
    }, 50*1000);
    
    
    this_module.make_debt_kupci_list(data);
    this_module.make_debt_dobav_list(data);
    
    
    
    // CASA SCONTO KUPCI
    this_module.make_sconto_kupac_list(data);
    
    
    // CASA SCONTO DOBAVLJAČ
    this_module.make_sconto_dobav_list(data);
    
        
    // GODIŠNJI BONUSI KUPAC
    this_module.make_bonus_kupac_list(data);
    
    // GODIŠNJI BONUSI DOBAV
    this_module.make_bonus_dobav_list(data);
    
    
    
    $('#'+rand_id+'_tender_status').data('cit_props', {
      desc: 'izaberi trenutni status tendera',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'tender_status',
      cit_run: this_module.choose_tender_status,
    }); 
    
    
    
    
    // TENDER PRODUCTS 
    var multi_widths_tender = { 
      proj_sifra: 1,
      item_sifra: 1,
      variant: 1,
      link: 4,
      interni_komentar: 2,
      start_date: 1,
      end_date: 1,
      remove_product: 1,
    };
    
    var col_widths_sum_tender = 0;
    $.each(multi_widths_tender, function(width_key, width) {
      col_widths_sum_tender += Number(width);
    });
    
    $(`#`+rand_id+`_tender_products`).data('cit_props', {
      
      desc: 'unutar partnera  - odabir proizvoda u current tenderu',
      local: true,
      list_width: 1000,
      /*col_widths: [70, 15, 15],*/
      format_cols: { variant: "center", proj_sifra: "center", item_sifra: "center", start_date: "center", end_date: "center" },
      show_cols: [ "link", "proj_sifra", "item_sifra", "variant", "interni_komentar", "start_date", "end_date" ],
      return: {},
      show_on_click: true,
      filter: function(products) {
        var new_products = [];
        $.each(products, function(p_index, product) {
          
          product.link = 
`<a href="#project/${product.proj_sifra}/item/${product.item_sifra}/variant/${product.variant}" target="_blank" 
    onClick="event.stopPropagation();">${product.full_product_name}</a>`;
          
          product.start_date = cit_dt(product.start).date;
          product.end_date = cit_dt(product.end).date;
          new_products.push(product);
        });
        return new_products;
      },
      
      list: 'current_tender_products',
      
      cit_run: this_module.choose_tender_product,
      
      /* sve ispod su zapravo propsi za listu koja se kreira kad selektiraš red u drop listi */
      multi_list: (cit_local_list.current_tender_products || []),
      
      multi_list_parent: `#current_tender_products_box`,
      multi_col_order: [ 
        "link",
        "proj_sifra", 
        "item_sifra", 
        "variant", 
        "interni_komentar",
        "start_date",
        "end_date",
        "remove_product"
      ],
      multi_widths: multi_widths_tender,
      add_cols: [
        
        function(row_id) { 
          var prop_name = `remove_product`;
          var header = `<div class="multi_header_cell" style="width: ${multi_widths_tender.remove_product/col_widths_sum_tender*100}%;">OBRIŠI</div>`;
          var cell = 
            `
            <div class="multi_cell" style="width: ${multi_widths_tender.remove_product/col_widths_sum_tender*100}%;" data-row_id="${row_id}" >
              <div class="multi_cell_delete_btn multi_list_btn"> <i class="fal fa-times"></i> </div> 
            </div>
            `;

          return {
            prop_name:  prop_name,
            header: header,
            cell: cell
          }
          
        },
      ]
      
    });
    
    var props = $(`#`+rand_id+`_tender_products`).data('cit_props');
    create_multi_list( rand_id+`_tender_products`, props );
    wait_for(`$('#${rand_id}_tender_products_multi_list').length > 0`, function() {
      this_module.register_tender_products_events(rand_id+`_tender_products`);
    }, 50*1000);    
    
    
    $('#'+rand_id+'_valuta_on_time').data(`cit_run`, this_module.choose_valuta_on_time );
    
    $('#'+rand_id+'_zaduznica_from_date_kupac').data(`cit_run`, this_module.update_debt_from_date_kupac );
    $('#'+rand_id+'_zaduznica_to_date_kupac').data(`cit_run`, this_module.update_debt_to_date_kupac );
    
    $('#'+rand_id+'_zaduznica_from_date_dobav').data(`cit_run`, this_module.update_debt_from_date_dobav );
    $('#'+rand_id+'_zaduznica_to_date_dobav').data(`cit_run`, this_module.update_debt_to_date_dobav );
    
    
    $('#'+rand_id+'_bonus_date_start_kupac').data(`cit_run`, this_module.update_bonus_date_start_kupac );
    $('#'+rand_id+'_bonus_date_end_kupac').data(`cit_run`, this_module.update_bonus_date_end_kupac );
    
    
    $('#'+rand_id+'_bonus_date_start_dobav').data(`cit_run`, this_module.update_bonus_date_start_dobav );
    $('#'+rand_id+'_bonus_date_end_dobav').data(`cit_run`, this_module.update_bonus_date_end_dobav );
    
    
    
    
      
    function condition_for_listeners() {
      
      if (data.doc?.length > 0) {
        return window.doc_list_events_registered; // ako trebam kreirati listu docs onda čekaj da se kreira
      } else {
        return true; // ako nema docsa onda odmah pokreni registraranje evenata 
      };
      
    };
      
    
    // window.doc_list_events_registered = false;
    // wait_for(condition_for_listeners, function() {

    $(`#${data.id} .cit_input.number`).off(`blur`);
    $(`#${data.id} .cit_input.number`).on(`blur`, function() {

      var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id = data.rand_id;

      var is_in_doc_list = $('#'+current_input_id).closest('#partner_doc_box').length > 0;

      // pokreni samo ako input polje NIJE u partner doc listi
      if ( !is_in_doc_list ) set_input_data(this, data);

    });

    
    $(`#${data.id} .cit_input.string`).off(`blur`);
    $(`#${data.id} .cit_input.string`).on(`blur`, function() {
      
      var this_input = this;

      var this_comp_id = null;
      var cit_comp_element = $('#'+current_input_id).closest('.cit_comp');
      if ( cit_comp_element.length > 0 ) this_comp_id = cit_comp_element[0].id;
      
      if ( !this_comp_id ) {
        console.log(`--------- NEMA CIT COMP-a ----------`);
        console.log(this_input);
        return;  
      };
      
      
      var data = this_module.cit_data[this_comp_id];
      var rand_id = data.rand_id;


      if ( this.id.indexOf(`_oib`) > -1 ) {
        
        var oib_text = $(this).val(); 

        // if ( $(this).val().toLocaleLowerCase().indexOf(`nepoz`) == -1 ) {

          if ( oib_text !== "" && oib_text.length !== 11 ) {

            var this_input = this;
            
            popup_warn(`OIB mora imati 11 znamenki !!!`, function() {
              
              $(this_input).trigger(`click`);
              $(this_input).trigger(`focus`);
              $(this_input).select();
            });

            return;
          };
        
          

          if ( oib_text !== "" && cit_number(oib_text) == null ) {

            var this_input = this;
            popup_warn(`OIB u sebi mora imati samo brojeve !!!`, function() {

              $(this_input).trigger(`click`);
              $(this_input).trigger(`focus`);
              $(this_input).select();

            });

            return;
          };

        // };

      };

      set_input_data( this, data );

    });

    $(`#${data.id} .cit_text_area`).off(`blur`);
    $(`#${data.id} .cit_text_area`).on(`blur`, function() {
      set_input_data( this, this_module.cit_data[data.id]);
    });

    
    this_module.make_partner_doc_list(data);
    
    
    // }, 5 * 1000);
    
    $(`#get_sud_reg_data_btn`).off(`click`);
    $(`#get_sud_reg_data_btn`).on(`click`, async function() {
      
      
      /*
      
      // ------------ TESTIRANJE ------------
      $('#'+rand_id+'_naziv').trigger(`click`);
      $('#'+rand_id+'_naziv').trigger(`focus`);
      $('#'+rand_id+'_naziv').select();
      document.execCommand("copy");
      var clip_text = await navigator.clipboard.readText();
      alert( clip_text );
      return;
      // ------------ TESTIRANJE ------------
      
      */
      
      
      $('#'+rand_id+'_naziv').trigger(`click`);
      $('#'+rand_id+'_naziv').trigger(`focus`);
      $('#'+rand_id+'_naziv').select();
      
      document.execCommand("copy");
      
      var sud_reg_win = window.open("https://sudreg.pravosudje.hr/registar/", '_blank', 'location=yes,height=600,width=600,scrollbars=yes,status=yes');
      
      
      setTimeout(function() {

        sud_reg_win.opener.focus();
        
        
        console.log( `sud_reg_win --------------` );
        console.log( sud_reg_win );

        var passed_time = 100;

        var interval_id = setInterval( async function () {
          
          passed_time += 100;

          var clip_text = "";
          try {
            var clip_text = await navigator.clipboard.readText();
            console.log(clip_text);
            
          } 
          catch(error) {
            console.log(error);
            console.log("GREŠKA KOD KOPIRANJA TEXT-a U CLIP");  
          };

          if ( clip_text && clip_text.indexOf('report-standard') > -1 ) {

            sud_reg_win.close();

            // alert( clip_text );

            $(`#sud_reg_trazi_box`).html(clip_text);

            // modificiraj linkove na firme jer je sada moj origin path
            $(`#sud_reg_trazi_box .report-standard a`).each( function() {
              var current_url = $(this).attr("href");
              $(this).attr("href", `https://sudreg.pravosudje.hr/registar/`+current_url);
              $(this).attr(`target`, `_blank`);
            });


            $(`#sud_reg_trazi_box .report-standard a`).each( function() {
              var select_company_btn = `<div class="sud_reg_details_btn" data-mbs="${ $(this).text().trim() }"><i class="fas fa-info"></i></div>`;

              $(this).before(select_company_btn);

              $(this).parent().addClass(`details_btn_parent`);

            });

            // malo sačekaj pa onda postavi evenete na svaki button koji sam napravio
            setTimeout( function() {

              $(`.sud_reg_details_btn`).off(`click`);
              $(`.sud_reg_details_btn`).on(`click`, function() {

                var mbs = $(this).attr(`data-mbs`);

                $.ajax({
                  headers: {
                      'Ocp-Apim-Subscription-Key': "e4184bd234fb4b449825a2718c48a6dd"
                  },
                  type: "GET",
                  cache: false,
                  url: `https://sudreg-api.pravosudje.hr/javni/subjekt_detalji?tipIdentifikatora=mbs&identifikator= ${mbs}&expand_relations=true`,
                  contentType: "application/json; charset=utf-8",
                  dataType: "json"
                })
                .done(function (result) {

                  if ( result && result.potpuni_oib ) {

                    var oib = result.potpuni_oib;
                    var naziv = result.skracene_tvrtke[0].ime;
                    var adresa = result.sjedista[0].ulica + " " + (result.sjedista[0].kucni_broj || "");
                    var naselje = result.sjedista[0].naziv_naselja;


                    var this_comp_id = $(`#sud_reg_trazi_box`).closest('.cit_comp')[0].id;
                    var data = this_module.cit_data[this_comp_id];
                    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


                    $('#'+rand_id+`_oib`).val(oib);
                    $('#'+rand_id+`_naziv`).val(naziv);
                    $('#'+rand_id+`_adresa`).val(adresa);
                    $('#'+rand_id+`_naselje`).val(naselje);

                    data.oib = oib;
                    data.naziv = naziv;

                    if ( !window.current_adresa ) window.current_adresa = {};
                    window.current_adresa.naselje = $('#'+rand_id+`_naselje`);
                    window.current_adresa.adresa = $('#'+rand_id+`_adresa`);

                    popup_msg(`Podaci su uspješno kopirani.`);

                  } else {

                    popup_warn(`Nedostaju podaci. Vjerojatno tvrtka nije više aktivna!`);

                  };


                })
                .fail(function (error) {
                  console.log(error);
                });

              }); // kraj click na button za detalje  

            }, 100);

            clearInterval(interval_id);

          } 
          else if (passed_time > 6*1000) {
            sud_reg_win.close();
            clearInterval(interval_id);
          };

        }, 100);

      }, 2000);  
        
        
    });
    
    $(`#remove_sud_reg_results`).off(`click`);
    $(`#remove_sud_reg_results`).on(`click`, function() {
      
       $(`#sud_reg_trazi_box`).html("");
      
    });
    
        
    
    // if ( STORE.find(data.id, `all`) == null ) STORE.make(data.id, data);
    
    /*
    var on_change = function() { 
      console.log(`promijeno sam data unutar ${data.id} `);
      // this_module.create( new_data, $(`#cit_root`) );
      this_module.data_to_html(STORE.find(data.id, 'all' ));
    };
    
    // OBIČNO STAVIM DA SE STORE ZOVE PO COMPONENT IDJU (1. i 4. arg su često isti)
    var component_listener_id = store_listener( data.id, 'all', on_change, data.id);
    
    */
    
    
    
    $(`#${rand_id}_iban_num`).off('blur');
    $(`#${rand_id}_iban_num`).on('blur', function() {
      
      var new_value = set_input_data(
        this, // input elem
        data, // data parent
        null, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
        "dont_update" // određujem ovako jel treba napraviti update dataseta ili samo vratiti value
      );
      
      if ( !window.current_iban ) window.current_iban = {};
      window.current_iban.num = new_value;
      
    });
    
    
    
    $(`#${rand_id}_cassa_sconto_perc_kupac`).off('blur');
    $(`#${rand_id}_cassa_sconto_perc_kupac`).on('blur', function() {
      
      var new_value = set_input_data(
        this, // input elem
        data, // data parent
        null, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
        "dont_update" // određujem ovako jel treba napraviti update dataseta ili samo vratiti value
      );
      
      if ( !window.current_sconto_kupac ) window.current_sconto_kupac = {};
      window.current_sconto_kupac.perc = new_value;
      
    });
    
    
    $(`#${rand_id}_cassa_sconto_perc_dobav`).off('blur');
    $(`#${rand_id}_cassa_sconto_perc_dobav`).on('blur', function() {
      
      var new_value = set_input_data(
        this, // input elem
        data, // data parent
        null, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
        "dont_update" // određujem ovako jel treba napraviti update dataseta ili samo vratiti value
      );
      
      if ( !window.current_sconto_dobav ) window.current_sconto_dobav = {};
      window.current_sconto_dobav.perc = new_value;
      
    });    
    
    
    
    $(`#${rand_id}_valuta_value`).off('blur');
    $(`#${rand_id}_valuta_value`).on('blur', function() {
      
      
      if ( window.current_def_pay && window.current_def_pay.valuta_on_time == true ) {
        popup_warn(`Nije moguće u isto vrijeme imati Dogovoreni tečaj i Tečaj na datum plaćanja.`);
        window.current_def_pay.valuta_value = null;
        $(this).val(``);
      };
      
      var new_value = set_input_data(
        this, // input elem
        data, // data parent
        null, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
        "dont_update" // određujem ovako jel treba napraviti update dataseta ili samo vratiti value
      );
      
      if ( !window.current_def_pay ) window.current_def_pay = {};
      window.current_def_pay.valuta_value = new_value;
      
    });
    
    
    
    $(`#${rand_id}_zaduznica_amount_kupac`).off('blur');
    $(`#${rand_id}_zaduznica_amount_kupac`).on('blur', function() {
      
      var new_value = set_input_data(
        this, // input elem
        data, // data parent
        null, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
        "dont_update" // određujem ovako jel treba napraviti update dataseta ili samo vratiti value
      );
      
      if ( !window.current_debt_kupac ) window.current_debt_kupac = {};
      window.current_debt_kupac.amount = new_value;
      
    });

    
    $(`#${rand_id}_zaduznica_amount_dobav`).off('blur');
    $(`#${rand_id}_zaduznica_amount_dobav`).on('blur', function() {
      
      var new_value = set_input_data(
        this, // input elem
        data, // data parent
        null, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
        "dont_update" // određujem ovako jel treba napraviti update dataseta ili samo vratiti value
      );
      
      if ( !window.current_debt_dobav ) window.current_debt_dobav = {};
      window.current_debt_dobav.amount = new_value;
      
    });
    
    
    $(`#${rand_id}_web_pristupi`).off('blur');
    $(`#${rand_id}_web_pristupi`).on('blur', function() {
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      // upiši u dataset text iz text area
      data.web_pristupi = $(`#${rand_id}_web_pristupi`).val();
      
      // pretvori u html na ljevoj strani
      this_module.web_pristupi_to_html(rand_id);
      
    });
    
    
    $('#'+rand_id+'_ugovor_date').data(`cit_run`, this_module.choose_ugovor_date );
    $('#'+rand_id+'_ugovor_start_date').data(`cit_run`, this_module.choose_ugovor_start_date );
    $('#'+rand_id+'_ugovor_end_date').data(`cit_run`, this_module.choose_ugovor_end_date );
    
    $('#'+rand_id+'_ugovor_docs').data(`this_module`, this_module );
    $('#'+rand_id+'_ugovor_docs').data(`cit_run`, this_module.update_ugovor_docs );
    
    $('#'+rand_id+'_ulazna_ponuda_docs').data(`this_module`, this_module );
    $('#'+rand_id+'_ulazna_ponuda_docs').data(`cit_run`, this_module.update_ulazna_ponuda_docs );
    
    
    
    $('#'+rand_id+'_tender_date').data(`cit_run`, this_module.choose_tender_date );
    $('#'+rand_id+'_tender_rok_prijave').data(`cit_run`, this_module.choose_tender_rok_prijave );
    $('#'+rand_id+'_tender_start_date').data(`cit_run`, this_module.choose_tender_start_date );
    $('#'+rand_id+'_tender_end_date').data(`cit_run`, this_module.choose_tender_end_date );
    
    $('#'+rand_id+'_tender_docs').data(`this_module`, this_module );
    $('#'+rand_id+'_tender_docs').data(`cit_run`, this_module.update_tender_docs );
    
    
    
    
    this_module.make_ugovori_list(data);
    
    this_module.make_tenders_list(data);
    
    
    
    $(`#`+rand_id+`_naselje`).data('cit_props', {
      desc: 'odabir naselja u partner modulu',
      local: true,
      show_cols: ['post_num', 'mjesto', 'naselje', 'zupanija'],
      return: {},
      show_on_click: true,
      list: 'post_offices',
      cit_run: this_module.choose_place,
    });
    
    
    $(`#`+rand_id+`_mjesto`).data('cit_props', {
      desc: 'odabir mjesta u partner modulu',
      local: true,
      show_cols: ['post_num', 'mjesto', 'naselje', 'zupanija'],
      return: {},
      show_on_click: true,
      list: 'post_offices',
      cit_run: this_module.choose_place,
    }); 
    
    $(`#`+rand_id+`_posta`).data('cit_props', {
      desc: 'odabir pošte u partner modulu',
      local: true,
      show_cols: ['post_num', 'mjesto', 'naselje', 'zupanija'],
      return: {},
      show_on_click: true,
      list: 'post_offices',
      cit_run: this_module.choose_place,
    });     
    
    
    $(`#`+rand_id+`_drzava`).data('cit_props', {
      desc: 'odabir drzave u partner modulu',
      local: true,
      show_cols: ['sifra', 'naziv'],
      return: {},
      show_on_click: true,
      list: 'countries',
      cit_run: this_module.choose_country,
    });
    
    
    
    $(`#save_partner_btn`).off(`click`);
    $(`#save_partner_btn`).on(`click`, this_module.save_partner );
    
            
    $(`#save_iban`).off('click');
    $(`#save_iban`).on('click', this_module.save_current_iban );
        
    $(`#update_iban`).off('click');
    $(`#update_iban`).on('click', this_module.save_current_iban );
    
        
    $(`#save_def_pay`).off('click');
    $(`#save_def_pay`).on('click', this_module.save_current_def_pay );
        
    $(`#update_def_pay`).off('click');
    $(`#update_def_pay`).on('click', this_module.save_current_def_pay );
    
    
    $(`#save_debt_kupac`).off('click');
    $(`#save_debt_kupac`).on('click', this_module.save_current_debt_kupac );
        
    $(`#update_debt_kupac`).off('click');
    $(`#update_debt_kupac`).on('click', this_module.save_current_debt_kupac );
        
    
    $(`#save_sconto_kupac`).off('click');
    $(`#save_sconto_kupac`).on('click', this_module.save_current_sconto_kupac );
        
    $(`#update_sconto_kupac`).off('click');
    $(`#update_sconto_kupac`).on('click', this_module.save_current_sconto_kupac );
    
    
    $(`#save_sconto_dobav`).off('click');
    $(`#save_sconto_dobav`).on('click', this_module.save_current_sconto_dobav );
        
    $(`#update_sconto_dobav`).off('click');
    $(`#update_sconto_dobav`).on('click', this_module.save_current_sconto_dobav );
    
        
    $(`#save_debt_dobav`).off('click');
    $(`#save_debt_dobav`).on('click', this_module.save_current_debt_dobav );
        
    $(`#update_debt_dobav`).off('click');
    $(`#update_debt_dobav`).on('click', this_module.save_current_debt_dobav );
    
    
    
        
    $(`#save_bonus_kupac`).off('click');
    $(`#save_bonus_kupac`).on('click', this_module.save_current_bonus_kupac );
        
    $(`#update_bonus_kupac`).off('click');
    $(`#update_bonus_kupac`).on('click', this_module.save_current_bonus_kupac );
        
        
    $(`#save_bonus_dobav`).off('click');
    $(`#save_bonus_dobav`).on('click', this_module.save_current_bonus_dobav );
        
    $(`#update_bonus_dobav`).off('click');
    $(`#update_bonus_dobav`).on('click', this_module.save_current_bonus_dobav );

    
    
    $(`#save_adresa`).off('click');
    $(`#save_adresa`).on('click', this_module.save_current_adresa );
        
    $(`#update_adresa`).off('click');
    $(`#update_adresa`).on('click', this_module.save_current_adresa );
    
    
    $(`#save_kontakt`).off('click');
    $(`#save_kontakt`).on('click', this_module.save_current_kontakt );
    
    $(`#update_kontakt`).off('click');
    $(`#update_kontakt`).on('click', this_module.save_current_kontakt );
    
    
    $(`#save_ugovor`).off('click');
    $(`#save_ugovor`).on('click', this_module.save_current_ugovor );
        
    $(`#update_ugovor`).off('click');
    $(`#update_ugovor`).on('click', this_module.save_current_ugovor );
    
    
    $(`#save_tender`).off('click');
    $(`#save_tender`).on('click', this_module.save_current_tender );
        
    $(`#update_tender`).off('click');
    $(`#update_tender`).on('click', this_module.save_current_tender );
    
    
     
  }, 500*1000 ); // kraj ako je html generiran

  return {
    html: component_html,
    id: data.id
  };

},
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};
  
  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
  };
  this_module.set_init_data = set_init_data;
  
  
  
  function web_pristupi_to_html(rand_id) {
    
    var this_comp_id = $(`#${rand_id}_web_pristupi`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    
    if ( data.web_pristupi ) {

      var web_html = "";
      var web_text_arr =   data.web_pristupi.split("\n");

      $.each(web_text_arr, function(index, line) {
        var line = line.trim();
        if ( line.indexOf(`https://`) > -1 || line.indexOf(`http://`) > -1 ) {
          // kreiraj a tag
          line = `<a href="${line}" target="_blank" onClick="event.stopPropagation();">${line}</a><br>`;
        } else {
          line += `<br>`; // if ( line.length > 5 )
        };
        web_html += line;
      });



      $(`#web_pristupi_box`).html(web_html);
      
      

      setTimeout( function() {
        // stavi custom visinu text area 
        $('#'+rand_id+`_web_pristupi`).css(`height`, $('#web_pristupi_box').height()+20 + "px" );

      }, 100 );

    } else {
      $(`#web_pristupi_box`).html(``);
    };

  };
  this_module.web_pristupi_to_html = web_pristupi_to_html;
  
  function data_to_html(data) {
    $.each(data, function(prop, value) {
      var elem_to_update = $(`#${data.id}_${prop}_label`);
      if ( elem_to_update.length > 0 && elem_to_update.html() !== value ) {
        elem_to_update.html(value);
      };
    });
  };
  this_module.data_to_html = data_to_html;
  
  
  function choose_valutu(state) {
    
    console.log(state);
    
    // deep copy od state
    var state = cit_deep(state);
    
    if ( !window.current_def_pay ) window.current_def_pay = {};
    
    if ( state == null ) {
      $('#'+current_input_id).val(``);
      window.current_def_pay.naziv = null;
      window.current_def_pay.valuta = null;
      return;
    };
      
    window.current_def_pay.naziv = state.naziv;
    window.current_def_pay.valuta = state.valuta;
    
    $('#'+current_input_id).val(state.naziv);
    
  };
  this_module.choose_valutu = choose_valutu;
  
  function choose_bank(state) {
      
    console.log(state);
    
    // deep copy od state
    var state = cit_deep(state);
    
    if ( state == null ) {
      $('#'+current_input_id).val(``);
      return;
    };
    
    if ( !window.current_iban ) window.current_iban = {};
      
    window.current_iban.bank = state;
    
    $('#'+current_input_id).val(state.naziv);
    
  };
  this_module.choose_bank = choose_bank;
  
  
  function choose_valuta_on_time(state, elem) {
    
    var this_comp_id = $(elem).closest('.cit_comp')[0].id;
    var rand_id = this_module.cit_data[this_comp_id].rand_id;
    
    
    if ( state == true && window.current_def_pay && window.current_def_pay.valuta_value ) {
      popup_warn(`Nije moguće u isto vrijeme imati Dogovoreni tečaj i Tečaj na datum plaćanja.`);
      setTimeout(function() { $('#'+rand_id+'_valuta_on_time').trigger(`click`);  }, 500);
    };
    
    if ( !window.current_def_pay ) window.current_def_pay = {};
    window.current_def_pay.valuta_on_time = state;
    
  };
  this_module.choose_valuta_on_time = choose_valuta_on_time;
  
  
  function save_current_iban() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    if ( !window.current_iban ) window.current_iban = {};
    if ( !window.current_iban.sifra ) window.current_iban.sifra = `ibn`+cit_rand();
    

    
    
    if ( !window.current_iban.num ) {
      popup_warn(`Niste upisali IBAN !!!`);
      return;
    };
    
    
    
        
    
    $.each(cit_local_list.banks, function(bank_ind, bank ) {
      if (
        window.current_iban.num.toUpperCase().indexOf( bank.sifra ) > -1
        ||
        window.current_iban.num.toUpperCase().indexOf( bank.vodeci_broj ) > -1
        ) {
        
          window.current_iban.bank = bank;
        
          };
      
    });
    
    
    
    data.ibans = upsert_item(data.ibans, window.current_iban, `sifra`);
    
    data.ibans_flat = JSON.stringify(data.ibans);
    
    this_module.make_iban_list(data);
    
    window.current_iban = null;
    
    $('#'+rand_id+`_iban_bank`).val("");
    $('#'+rand_id+`_iban_num`).val("");
   
    if ( this.id == `update_iban` ) {
      $(this).css(`display`, `none`);
      $(`#save_iban`).css(`display`, `flex`);
    };  
    
    
  };
  this_module.save_current_iban = save_current_iban;
  
  

  function write_kolicina_value( data, input_value, doc_sifra, kol_sifra, arg_sir_sifra, prop ) {


    $.each( data.docs, function(doc_ind, doc) {  

      if ( doc.sifra == doc_sifra && doc.doc_sirovs?.length > 0 ) {

        $.each( doc.doc_sirovs, function(sir_ind, sir) {

          if ( sir.doc_kolicine?.length > 0 ) {

            $.each( sir.doc_kolicine, function(kol_ind, kol) {

              if( arg_sir_sifra == sir.sifra && kol.sifra == kol_sifra ) {
                // ako je  dobar doc sir _sifra i ako je dobra sifra objekta za količinu !!!!
                // ONDA UPIŠI VALUE IZ POLJA
                data.docs[doc_ind].doc_sirovs[sir_ind].doc_kolicine[kol_ind][prop] = input_value;
              };

            }); // loop po kolicinama (to su objekti)

          }; // jel sirov ima kolicine

        }); // loop po doc sirovs

      }; // jel ima doc sirovs ???

    }); // loop po docs
    
    return data;

  };
  this_module.write_kolicina_value = write_kolicina_value;
  
  
  
  function write_payments( data, input_value, doc_sifra, prop ) {

    $.each( data.docs, function(doc_ind, doc) {  
      if ( doc.sifra == doc_sifra ) {
        data.docs[doc_ind][prop] = input_value;
      }; 
    }); 
    
    return data;

  };
  this_module.write_payments = write_payments;
    
  
  
  function doc_sum_row( data, doc_obj_index ) {

    data.docs[doc_obj_index].sum = 0;
    
    var doc_obj = data.docs[doc_obj_index];

    
    if ( doc_obj.doc_sirovs?.length > 0 ) {
      
      $.each(doc_obj.doc_sirovs, function(s_ind, sir) {
        
        if ( sir.doc_kolicine?.length > 0 ) {
          
          $.each(sir.doc_kolicine, function(k_ind, kol) {
            data.docs[doc_obj_index].sum += kol.price;
          });
          
        };
        
      });

    };
    
      
    
    
    var ad_acta_btn_disable = ``;

    // ako IMA ad acta onda Dodaj cit disable
    // ILI ako nije u grupi FIN editora cit disable
    if ( doc_obj.ad_acta || get_fin_editors().indexOf( window.cit_user.user_number ) == -1 ) ad_acta_btn_disable = `cit_disable`;

    var ad_acta_btn = 
      `<button class="blue_btn btn_small_text ad_acta_btn ${ad_acta_btn_disable}" id="${doc_obj.sifra}_ad_acta_btn" style="margin: 0; margin-left: 10px; box-shadow: none;">
        <i class="fas fa-check"></i>
      </button>
      `;


    var pay_value_valid = cit_deep( this_module.valid.pay_value );
    var pay_komentar_valid = cit_deep( this_module.valid.pay_komentar );

    if ( get_fin_editors().indexOf( window.cit_user.user_number ) == -1 ) {
      pay_value_valid.lock = true;
      pay_komentar_valid.lock = true;
    };


    var pay_sum = ``;

    if ( doc_obj.doc_type == "order_reserv" ) {
      pay_sum = cit_format(data.docs[doc_obj_index].sum, 2) + ` -- ` + cit_format( (data.docs[doc_obj_index].pay_value || 0) / data.docs[doc_obj_index].sum, 0) + ` % `;
    }; 

    var pay_value = cit_comp( 
      doc_obj.sifra + `_pay_value`,
      pay_value_valid,
      doc_obj.pay_value || null,
      null,
      "padding: 3px; height: 24px; float: left;  width: calc(100% - 50px);",
      "pay_value" /* ovo je klasa  koju sam ubacio */
    );
    
    var pay_komentar = cit_comp( 
      doc_obj.sifra + `_pay_komentar`,
      pay_komentar_valid,
      doc_obj.pay_komentar || null,
      null,
      "padding: 3px; height: 50px; float: none;  width: calc(100% - 15px); min-height: 50px; font-size: 13px; margin: 5px 0;",
      "pay_komentar" /* ovo je klasa  koju sam ubacio */
    );

    
    
    
    
        

    var sum_row = 

    `
    <div style="display: flex; width: 100%; background: #d6eaf7;">

      <div style="width: 15%; display: flex;" class="cit_tooltip" data-toggle="tooltip" data-placement="top" data-html="true" title="Iznos ukupne uplate & RJEŠENO" >
        ${ pay_value }
        ${ ad_acta_btn }
      </div>

      <div style="width: 65%;">

        &b

      </div>
      <div  style="width: 20%;" 
            class="cit_tooltip payment_sum_box" data-toggle="tooltip" data-placement="top" data-html="true" title="Ukupno" >
          ${pay_sum}
      </div>

    </div>
    `;

    
    return sum_row;
    
    
  };
  this_module.doc_sum_row = doc_sum_row;
  
  
  
  function get_kolicina(doc, arg_kol_sifra) {
    
    var kol = null;
    
    if ( doc.doc_sirovs?.length > 0 ) {
      
      $.each( doc.doc_sirovs, function(ss_ind, sir_find) {
        $.each(sir_find.doc_kolicine, function(kk_ind, kol_find) {
          if ( kol_find.sifra == arg_kol_sifra ) kol = kol_find;
          
        });  
      });
    };
    
    
    return kol;
    
  };
  
  
  function set_kolicina( arg_doc, arg_kol_sifra, arg_kol_prop, new_value, add_value) {
    
    var kol = null;
    
    var this_comp_id = $(`#partner_doc_box`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    
    $.each( data.docs, function(dd_ind, doc_find) {

      if ( arg_doc.sifra == doc_find.sifra && doc_find.doc_sirovs?.length > 0 ) {

        $.each( doc_find.doc_sirovs, function(ss_ind, sir_find) {
          $.each(sir_find.doc_kolicine, function(kk_ind, kol_find) {
            
            if ( kol_find.sifra == arg_kol_sifra ) {
              kol = kol_find;
              
              if ( add_value ) {
                data.docs[dd_ind].doc_sirovs[ss_ind].doc_kolicine[kk_ind][arg_kol_prop] += add_value;
              } else if ( new_value ) {
                data.docs[dd_ind].doc_sirovs[ss_ind].doc_kolicine[kk_ind][arg_kol_prop] = new_value;
              };
              
            };

          });  
        });
      };

    });

    
    return kol;
    
  };
  
  
  
  
  function make_partner_doc_list(data) {

    var valid = this_module.valid;

    $(`#partner_doc_box`).html(``);

    var docs_za_table = [];
    
    var criteria = [ '!time' ];
    if ( data.docs?.length > 0 ) multisort( data.docs, criteria );
    
    // OVDJE AUTOMATSKI DODJELJUJEM PARENTE NA DOCSIMA KOJI SU NASTALI PUTEM KALK REZERVACIJE !!!!
    // OVDJE AUTOMATSKI DODJELJUJEM PARENTE NA DOCSIMA KOJI SU NASTALI PUTEM KALK REZERVACIJE !!!!
    // OVDJE AUTOMATSKI DODJELJUJEM PARENTE NA DOCSIMA KOJI SU NASTALI PUTEM KALK REZERVACIJE !!!!
    
    var curr_time = Date.now();
    
    $.each( data.docs, function(d_ind, doc) {
      
      $.each( doc.doc_sirovs, function(s_ind, sir) {
        
        
        // važno !!!! 
        // važno !!!! 
        // važno !!!! 
        // --------> pošto neki doc sirovs koji nastanu KAO IZLAZNI TJ KADA MI PRODAJEMO ---> sa offer reserv i order reserv nemaju _id
        // moram ovom doc sirov dati neku sifru 

        if ( !sir.sifra ) data.docs[d_ind].doc_sirovs[s_ind].sifra = "docsir" + cit_rand();
        
        
        if ( doc.doc_type == `order_dobav` ) {
          
          var primka_kolicine = {};
          
          $.each( data.docs, function(dd_ind, doc_find) {
            
            if ( doc_find.nd_parent == doc.sifra && doc_find.doc_type == `in_record` ) { // nadji sve primke koje imaju nd_parent od ovog ND
              
              $.each( doc_find.doc_sirovs, function(sirov_ind, sirov) {
                $.each( sirov.doc_kolicine, function(kolicina_ind, kolicina) {
                  
                  if ( !primka_kolicine[kolicina.sifra] ) primka_kolicine[kolicina.sifra] = 0; 
                  primka_kolicine[kolicina.sifra] += kolicina.count;
                });
              });
              
            };
            
          });
          
          
          $.each( doc.doc_sirovs, function(sirov_ind, sirov) {
            
            $.each( sirov.doc_kolicine, function(kolicina_ind, kolicina) {
              
              // ako ne postoji ovaj prop stavi da bude nula
              if ( !data.docs[d_ind].doc_sirovs[sirov_ind].doc_kolicine[kolicina_ind].in_record_sum ) {
                data.docs[d_ind].doc_sirovs[sirov_ind].doc_kolicine[kolicina_ind].in_record_sum  = 0;
              };
              // PRIKAŽI U OVOM ND koliko je došlo robe preko RAZNIH PRIMKI za ovu sirovinu !!!
              data.docs[d_ind].doc_sirovs[sirov_ind].doc_kolicine[kolicina_ind].in_record_sum += primka_kolicine[kolicina.sifra] || 0;
            });
            
          });
          
          
          
        }; // kraj ako je ovo ND

        
        $.each(sir.doc_kolicine, function(k_ind, kol) {
          
          
          data.docs[d_ind].doc_sirovs[s_ind].doc_kolicine[k_ind].next_count    = null;
          data.docs[d_ind].doc_sirovs[s_ind].doc_kolicine[k_ind].next_doc      = false;
          
          // KOD PONUDE I PRIMKE OBAVEZNO OSTAVI NEXT APPROVED ----> sve ostalo resetiraj !!!!
          if ( doc.doc_type !== `ulazna_ponuda` && doc.doc_type !== `in_record` ) {
            data.docs[d_ind].doc_sirovs[s_ind].doc_kolicine[k_ind].next_approved = false;
          };
          
          // SAMO KOD ULAZNE PONUDE OSTAVI NEXT PRICE 
          if ( doc.doc_type !== `ulazna_ponuda` ) {
            data.docs[d_ind].doc_sirovs[s_ind].doc_kolicine[k_ind].next_price    = null;
          };
          
          
        });  
      });
      
      
      // zamjeni stare propse koji u sebi imaju riječ record SA RIJEČI doc ... ako je ostao neki stari :) 
      if ( doc.record_parent ) data.docs[d_ind].doc_parent = doc.record_parent;
      if ( doc.record_est_time ) data.docs[d_ind].doc_est_time = doc.record_est_time;
      
      
      // pretvori sve doc_parente koji su sifra u prave objekte tj pronađi parente u samoj listi docsa
      
      if ( doc.doc_parent && typeof doc.doc_parent == `string` ) {
        
        var find_parent = find_item( data.docs, doc.doc_parent, `sifra` );
        if ( find_parent ) doc.doc_parent = find_parent;
        
        // parent od parenta STRING
        if ( find_parent.doc_parent && typeof doc.doc_parent == `string` ) {
          var find_parent_parent = find_item( data.docs, find_parent.doc_parent, `sifra` );
          if ( find_parent_parent ) doc.doc_parent = find_parent_parent;
        };
        
        // parent od parenta OBJECT
        if ( find_parent.doc_parent && $.isPlainObject(find_parent.doc_parent)  ) {
          var find_parent_parent = find_parent.doc_parent
          doc.doc_parent = find_parent_parent;
        };
        
      };
      
      
      
      if ( doc.doc_parent && $.isPlainObject(doc.doc_parent)  ) {
        
        // parent od parenta STRING
        if ( doc.doc_parent.doc_parent && typeof doc.doc_parent == `string` ) {
          var find_parent_parent = find_item( data.docs, doc.doc_parent.doc_parent, `sifra` );
          doc.doc_parent = find_parent_parent;
        };
        
        // parent od parenta OBJECT
        if ( doc.doc_parent.doc_parent && $.isPlainObject(doc.doc_parent.doc_parent)  ) {
          doc.doc_parent = doc.doc_parent.doc_parent;
        };
        
      };
      
      
      
      // kada spremam RFQ i ND onda ubacujem u dokument partner modula i partner data
      // ali kada kod save partner pretvaram to u JSON JAVLJA MI GREŠKU ZBOG CIRCULAR REFERENCE ERORR
      // zato to moram obrisati
      if ( doc.partner_module ) delete data.docs[d_ind].partner_module;
      if ( doc.partner_data ) delete data.docs[d_ind].partner_data;

      // dodjeli doc partner koji je offer
      if ( doc.doc_type == "order_reserv" ) {
        
        $.each( data.docs, function(find_ind, find_doc) {
          
          
          // nadji offer reserv sa istim project sifrom
          if ( 
            find_doc.doc_type == "offer_reserv" 
            && 
            find_doc.proj_sifra == doc.proj_sifra 
          ) {
            
            /*
            var this_comp_id = $(`#partner_doc_box`).closest('.cit_comp')[0].id;
            var data = this_module.cit_data[this_comp_id];
            var rand_id =  this_module.cit_data[this_comp_id].rand_id;
            */
            
            // uzmi da je doc parent od order reserv ovaj offer !!!
            data.docs[d_ind].doc_parent = find_doc.sifra;
            
            // ako find_doc već ima svoj parent onda uzmi njegov parent
            if ( find_doc.doc_parent ) data.docs[d_ind].doc_parent = find_doc.doc_parent.sifra; 
            
          };
          
        });
        
      };
      
      // dodjeli doc partner koji je order
      if ( doc.doc_type == "radni_nalog" ) {
        
        $.each( data.docs, function(find_ind, find_doc) {
          
          if ( 
            ( find_doc.doc_type == "order_reserv" || find_doc.doc_type == "offer_reserv" ) 
            &&
            find_doc.proj_sifra == doc.proj_sifra 
            
          ) {
            
            /*
            var this_comp_id = $(`#partner_doc_box`).closest('.cit_comp')[0].id;
            var data = this_module.cit_data[this_comp_id];
            var rand_id =  this_module.cit_data[this_comp_id].rand_id;
            */
            
            data.docs[d_ind].doc_parent = find_doc;
            
            // ako find_doc već ima svoj parent onda uzmi njegov parent
            if ( find_doc.doc_parent ) data.docs[d_ind].doc_parent = find_doc.doc_parent; 
            
          };
          
        });
        

        
      };
      
    }); 
    // -------------------------------------------- GOTOVA PRIPREMA DOCSA --------------------------------------------
    
    var docs_list = data.docs;
    
    // ako je AKTIVAN FILTER tj kliknut gumb za filter
    // ako je AKTIVAN FILTER tj kliknut gumb za filter
    // ako je AKTIVAN FILTER tj kliknut gumb za filter
    
    if ( window.partner_docs_are_filtered ) docs_list = data.filtered_docs;

    if ( docs_list && docs_list.length > 0 ) {
      
      var criteria = [ '~time' ];
      multisort( docs_list, criteria );

      $.each( docs_list, function(doc_obj_index, doc_obj ) {

        var {

          sifra,
          time,
          
          proj_sifra,
          status,
          doc_pay,
          referent,
          doc_type,
          doc_num,
          oib,
          
          partner_name,
          partner_adresa,
          
          doc_adresa,
          dostav_adresa,
          partner_id,

          rok,
          
          items,
          
          doc_link,
          
          doc_parent,
          
          doc_sifra,
          pdf_name,
          
          doc_files, // svaki doc može u sebi imati sub docs tj multiple files
          sirov_status,
          
        } = doc_obj;
        
        // napravi novu sifru za doc samo ako je NEMA !!!!
        if ( !doc_obj.sifra ) doc_obj.sifra = `partnerdoc` + cit_rand();

        var grana_btn = 
  `
  <button id="${doc_obj.sifra}_doc_grana" class="blue_btn small_btn doc_grana" style="margin: 0 auto; box-shadow: none;">
    <i class="fas fa-code-branch" style="font-size: 19px;"></i>
  </button>
  `;

        
        var doc_dostava = ``;
        
        if ( doc_obj.doc_type == "ulazna_ponuda" || doc_obj.doc_type == "order_dobav" || doc_obj.doc_type == "in_record" ) {

          doc_dostava = cit_comp( 
            doc_obj.sifra + `_doc_dostava`,
            valid.doc_dostava,
            doc_obj.doc_dostava || null,
            null,
            "padding: 3px; height: 24px; float: none;  width: 100%",
            "doc_dostava" /* ovo je klasa  koju sam ubacio */
          );
          
        };
        
        
        var storno_doc = ``;
        var storno_pill = ``;
        
        if ( doc_obj.doc_type == "order_dobav"  ) {
        
          
          if ( !doc_obj.storno ) {
            storno_doc = 
                `
                <button class="blue_btn btn_small_text storno_doc" id="${doc_obj.sifra}_storno_ND" 
                        style="margin: 5px auto; box-shadow: none; width: 100%; background: #bf0060;" >
                  STORNO
                </button>
                `;

            // ako je posve ad acta ( bez obzira koliko je uplaćeno )
          }
          else {

            storno_pill = 
              `<span class="cit_doc_pill" style="background: #123253; margin-top: 5px;" >
                ${ `STORNO<br>` + doc_obj.storno.user + "<br>" + cit_dt( doc_obj.storno.time).date_time }
              </span>`;

          };

        };
        
        var doc_row_komentar =  cit_comp( 
            doc_obj.sifra + `_doc_row_komentar`,
            valid.doc_row_komentar,
            doc_obj.doc_row_komentar || null,
            null,
            "padding: 3px; height: 50px; float: none;  width: calc(100% - 15px); min-height: 50px; font-size: 13px; margin: 5px 0;",
            "doc_row_komentar" /* ovo je klasa  koju sam ubacio */
          );
        
        
        
        
        var payment_pill = ``;

        var product_links = ``;
        
        if ( doc_obj.doc_sirovs?.length > 0 ) {
          
          $.each( doc_obj.doc_sirovs, function(item_ind, item) {
            
            
            var item_statuses = ``;
            
            if ( item.doc_statuses?.length > 0 ) {
              
              $.each( item.doc_statuses, function(s_ind, stat) {
                
                var count = ``;
                
                if ( stat.order_sirov_count ) count = stat.order_sirov_count + " kom";
                
                item_statuses +=
                  `
                  <div style="display: flex; width: 100%;">
                    <div style="width: 10%;">&nbsp;</div>
                    <div style="width: 90%;">
                      <a  href="#project/${stat.proj_sifra }/item/${stat.item_sifra || null}/variant/${stat.variant || null}/kalk/null/proces/null" 
                          target="_blank"
                          onClick="event.stopPropagation();"> 
                        ${ (s_ind + 1) + ". " + stat.order_sirov_count + " " + stat.full_product_name } 
                      </a>
                    </div>
                  </div>
                  `;
                  
              });
              
            };
            
            
            if ( item.doc_kolicine?.length > 0 ) {
              
              $.each( item.doc_kolicine, function(k_ind, kol) {
                
                  
                var next_count = cit_comp( 
                  doc_obj.sifra + '_' + kol.sifra + '_' + item.sifra + `_next_count`,
                  valid.next_count,
                  kol.next_count || null,
                  null,
                  "padding: 3px; height: 24px; float: left;  width: calc(100% - 50px);",
                  "next_count" /* ovo je klasa  koju sam ubacio */
                );

                var next_price = cit_comp( 
                  doc_obj.sifra + '_' + kol.sifra + '_' + item.sifra + `_next_price`,
                  valid.next_price,
                  kol.next_price || null,
                  null,
                  "padding: 3px; height: 24px; float: left;  width: calc(100% - 50px);",
                  "next_price" /* ovo je klasa  koju sam ubacio */
                );


                var next_doc = cit_comp( 
                  doc_obj.sifra + '_' + kol.sifra + '_' + item.sifra + `_next_doc`, 
                  this_module.valid.next_doc, 
                  kol.next_doc || false,
                  null,
                  "margin: 0 auto;",
                  "next_doc" 
                );


                // napravi kopiju od validation objekta tako da ga mogu promijeniti i da ne utječem na druge objekte
                var next_approved_valid = cit_deep( this_module.valid.next_approved );
                // ako nije NITI JEDAN OD EDITORA onda zaklučaj
                if ( get_super_editors().indexOf( window.cit_user.user_number ) == -1 ) next_approved_valid.lock = true;

                var next_approved = cit_comp( 
                  doc_obj.sifra + '_' + kol.sifra + '_' + item.sifra + `_next_approved`, 
                  next_approved_valid, 
                  kol.next_approved || false,
                  null,
                  "margin: 0 auto;",
                  "next_approved" 
                );


                var LEFT_inputs_html = 
                `
                <div  style="width: 15%;" class="cit_tooltip" 
                      data-toggle="tooltip" 
                      data-placement="top" data-html="true" title="Količina za sljedeći dokument i prikaži sljedećem u dokumentu" >
                ${ next_count + next_doc }
                </div>
                `;
                
                
                var roba_link = ``;
                
                // OVO JE PROIZVOD !!!!
                if ( item.out_roba ) {
                  
                  var price_text =  cit_format(kol.price, 2) + " -- " + cit_format(kol.dis_unit_price, 2) + " /kom";
                  
                  roba_link = `
                  <a  href="#project/${item.proj_sifra}/item/${item.item_sifra || null }/variant/${item.variant || null}/status/null"
                      style="text-align: left;"
                      target="_blank"
                      onClick="event.stopPropagation();"> 
                    ${ cit_format(kol.count, 0) + ` -- ` + item.naziv  + ` -- ` + price_text } 
                  </a>

                  `;
                
                };
                  
                // OVO JE ULAZNA SIROVINA  !!!!  
                
                if ( item.in_roba ) {
                  
                  var in_record_sum = ``;
                  
                  if ( kol.in_record_sum ) in_record_sum = kol.in_record_sum + ` / `;
                  
                  var price_text =  cit_format(kol.price*kol.count, 2) + " -- " + cit_format(kol.price, 2) + " /kom";
                  
                  if ( kol.doc_dostava ) {
                    price_text += `<br>DOSTAVA: ${ cit_format(kol.doc_dostava, 2) }kn --- J.C. S DOSTAVOM: ${ cit_format(kol.price + kol.doc_dostava, 2) }kn`;
                  };
                  
                  roba_link =
                  `
                  <a  href="#sirov/${item._id}/status/null" 
                      style="text-align: left;"
                      target="_blank"
                      onClick="event.stopPropagation();"> 
                      ${ in_record_sum + kol.count + " -- " + item.sirovina_sifra + " -- " + item.full_naziv  + ` ` + price_text  } 
                  </a>

                  `;
                };
                

                product_links +=
                  `<div style="display: flex; width: 100%;">
                  
                    ${LEFT_inputs_html}
                    
                    <div style="width: 70%;">
                      ${roba_link}
                    </div>
                    <div style="width: 15%;" class="cit_tooltip" data-toggle="tooltip" data-placement="top" data-html="true" title="Cijena za sljedeći dokument i ODOBRENJE UPRAVE" >
                      ${ next_price + next_approved }
                    </div>
                    
                  </div>
                  ${item_statuses}
                  `;
                
              }); // kraj loopa po količinama 
              
            }; // jel ima dok količine
            
          }); // kraj loopa po doc_sirovs
          
        }; // ako ima doc sirovs !!!
        
        
        if ( doc_obj.doc_files?.length > 0 ) {
            
          doc_obj.doc_link = ``;
          
          $.each( doc_obj.doc_files, function(file_ind, file) {
            doc_obj.doc_link += file.link;
          });
          
          // na kraju dodaj tip dokumenta i šifru ponude
          doc_obj.doc_link = `${ window.map_name_to_type[doc_obj.doc_type] }: ${ doc_obj.doc_sifra }<br>` + doc_obj.doc_link;
          
        };

       /*
       umjesto doc_sum_row napravio sam cijele dokumente IN_BILL ili OUT_BIlL
       
       ------------------------------------------------------------ ZA SADA NE KORISTIM ------------------------------------------------------------
       ------------------------------------------------------------ ZA SADA NE KORISTIM ------------------------------------------------------------
       
        if ( 
          doc_obj.doc_type == "order_reserv" // dodaj SUMU samo ako je ovo Narudžba kupca ( kada je ponuda onda nema smisla jer može biti bilo što ) 
          ||
          doc_obj.doc_type == "out_record" // DODAJ SUMU AKO JE OVO OTPREMNICA
          
        ) {
          product_links =  this_module.doc_sum_row( data, doc_obj_index ) + product_links;
        };
        
        
        ------------------------------------------------------------ ZA SADA NE KORISTIM ------------------------------------------------------------
        ------------------------------------------------------------ ZA SADA NE KORISTIM ------------------------------------------------------------
        */
        
        
        
        
        // UVIJEK DODAJ KOMENTAR REDA
        product_links += doc_row_komentar;
        

        // ako nije ad acta ali je nešto uplaćeno
        if ( doc_obj.pay_value && !doc_obj.ad_acta ) {
          payment_pill = 
            `<span class="cit_doc_pill">${ `PARCIJALNO PLAĆENO<br>` + doc_obj.pay_value_name + ` ` + cit_dt( doc_obj.pay_value_time).date_time }</span>`;
        };

        // ako je posve ad acta ( bez obzira koliko je uplaćeno )
        if ( doc_obj.ad_acta ) {
          payment_pill = 
            `<span class="cit_doc_pill" style="background: #31a299;" >${ `PLAĆENO<br>` + doc_obj.pay_value_name + ` ` + cit_dt( doc_obj.pay_value_time).date_time }</span>`;
        };


        
        var time_and_referent = cit_dt(time).date_time + `<br>` + referent;
        
        var choose_doc = cit_comp( sifra + `_` + data.rand_id + `_choose_doc`, valid.choose_doc, false, null, "margin: 0 auto;", "choose_doc_row" );
        
        
        if ( doc_obj.storno_time ) doc_link = `<span class="cit_doc_pill">STARA FAZA</span>` + doc_link;

        // samo testiram da vidim koje su šifre parenta
        // doc_obj.doc_link += `<br>` + doc_obj.sifra + `<br>` + doc_obj.doc_parent?.doc_type + `<br>` + doc_obj.doc_parent?.sifra; 
        
        docs_za_table.push({

          grana_btn,
          choose_doc,
          time,
          referent,
          time_and_referent,
          doc_parent,
          sifra,
          doc_sifra,
          doc_type,
          doc_link: (doc_obj.doc_link + payment_pill + doc_dostava + storno_doc + storno_pill ),
          pdf_name,
          rok,
          product_links,
          
        });

      }); // kraj loopa svih adresa


      var docs_props = {
        desc: 'samo za kreiranje tablice svih docsa UNUTAR PARTNER MODULE koji je zapravo data_for_doc prilikom generiranja dokumenta ',
        local: true,

        list: docs_za_table,

        show_cols: [
          
          `grana_btn`,
          `choose_doc`,
          `time_and_referent`,
          `doc_link`,
          `rok`,
          `product_links`,
          
        ],
        custom_headers: [
          `Grana`,
          `Označi`,
          `Vrijeme / User`,
          `Tip/Dokument`,
          `Est. time`,
          `Roba`,
          
        ],
        col_widths: [
          1, // `grana_btn`,
          1, // `choose_doc`,
          2, // `time`,
          2, // `Tip/Dokument`,
          2, // `rok,
          12, // `product_links`,
         
        ],

        format_cols: { 
          product_links: "left",
          time: "center",
          rok: "date_time",
          
          doc_link: "left",
          product_links: "left",
          
        },

        parent: "#partner_doc_box",
        return: {},
        show_on_click: false,

        cit_run: function(state) { console.log(state); },

      }; // kraj docs props


      if (docs_za_table.length > 0 ) {
        
        create_cit_result_list(docs_za_table, docs_props );
        

        // OVO JE ZA _choose_doc 
        // OVO JE ZA _choose_doc 
        // OVO JE ZA _choose_doc 
        
        wait_for( `$("#partner_doc_box .choose_doc_row").length > 0`, function() {

          
          $('#partner_doc_box .doc_row_komentar').each( function() {

            $(this).off(`blur`);
            $(this).on(`blur`, function(e) { 

              var this_input = this;
              
              var doc_sifra = this.id.split(`_`)[0];

              var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;
              
              
              var curr_doc_index = find_index( data.docs, doc_sifra, `sifra` );

              var curr_doc = this_module.cit_data[this_comp_id].docs[curr_doc_index];

              // stavi komentar u glavni document
              curr_doc.doc_row_komentar = $(this_input).val();
              
              
              setTimeout( function() { $(`#save_partner_btn`).trigger(`click`); }, 200 );

              
              
            }); // kraj blur eventa na offer dobav price
          
          });
                    

                    
          $('#partner_doc_box .doc_dostava').each( function() {

            $(this).off(`blur`);
            $(this).on(`blur`, function(e) { 

              var this_input = this;
              
              var doc_sifra = this.id.split(`_`)[0];

              var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;

              
              var input_value = cit_number( $(this_input).val() );
              
              // ako nije prazno ali je input val null to znači KRIVI UPIS
              if ( $(this_input).val() !== `` && input_value == null ) {
                popup_warn(`Nije broj :-P`);
                $(this_input).val(``);
              };
 
              
              
              var curr_doc_index = find_index( data.docs, doc_sifra, `sifra` );

              var curr_doc = this_module.cit_data[this_comp_id].docs[curr_doc_index];
              // prov pogledaj staru dostavu
              var old_dostav = curr_doc.doc_dostava; // može biti undefined

              // pretvori u broj
              var old_dostav = cit_number( old_dostav );

              // ako nova cijea dostave nije ista kao stara i ako trenutni doc ima doc sirovs
              if ( old_dostav !== input_value && curr_doc.doc_sirovs?.length > 0 ) {

                // --------------------------------------------- IZRAČUNAJ SUM CIJENU SVIH STAVKI tj svih kolicina u svim sirovinama
                // samo ako je označeno s next doc !!!!
                // 
                var kol_sum_price = 0;

                $.each( curr_doc.doc_sirovs, function(s_ind, sir) {
                  $.each( sir.doc_kolicine, function(k_ind, kol) {

                    if ( kol.next_doc ) { // samo ako je označeno s next doc !!!!
                      var sir_sum_price = ( kol.next_price || kol.price || 0) * ( kol.next_count || kol.count || 0);
                      kol_sum_price += sir_sum_price;
                    };

                  });
                });


                // --------------------------------------------- IZRAČUNAJ UDIO CIJENE U SUMI i izračunaj udio dostave
                $.each( curr_doc.doc_sirovs, function(s_ind, sir) {

                  $.each( sir.doc_kolicine, function(k_ind, kol) {

                    if ( kol.next_doc ) {
                      // pojedinačna cijena
                      var sir_sum_price = ( kol.next_price || kol.price || 0) * ( kol.next_count || kol.count || 0);
                      // pojedinačna cijena / ukupna cijena 
                      // ------> dobijem udio pojedinačne cijene
                      // npr 5 posto
                      var udio_kol_sum_price = sir_sum_price / kol_sum_price; // udio jedne stavke u svim stavkama (ukupna cijena NE JEDINIČNA)
                      
                      // onih gore 5 posto je udio u ukupnoj cijeni pa zato dodam 5% troškova dostave za tu cijenu !!!
                      // dakle koliko je udio cijene sirovine u ponudi
                      // toliko je udio dostave !!!
                      curr_doc.doc_sirovs[s_ind].doc_kolicine[k_ind].doc_dostava = (udio_kol_sum_price * input_value) / (kol.next_count || kol.count || 0); // udio dostave
                    };

                  });
                  
                });


              }; // kraj ako je dostava promjenjena i ako ima doc sirovs 

              // stavi dostavu u glavni document
              curr_doc.doc_dostava = input_value;

              $(this_input).val( cit_format( input_value, 3 ) );
              
              
              this_module.make_partner_doc_list(data);
              

              setTimeout( function() { $(`#save_partner_btn`).trigger(`click`); }, 200 );


              console.log(data.docs);
              
              
            }); // kraj blur eventa na offer dobav price
          
          });
          
          
          $('#partner_doc_box .choose_doc_row').each( function() {

            $(this).data(`cit_run`, async function(state, check_elem) { 

              var this_comp_id = $(check_elem).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;

              console.log(`--------check box ---------`);
              console.log(data);

              // ugasi sve docs check boxes 
              // osim ove koje sam kliknuo !!!!
              

                              
                $('#partner_doc_box .choose_doc_row').each( function() {
                  if ( this.id !== check_elem.id ) $(this).removeClass('on').addClass('off');
                });
              
              // id od check box je na primjer 
              // dfd354354334_sir3243432434_choose_doc

              var choosed_sifra = check_elem.id.split(`_`)[0];

              if ( state !== false ) {
                
                data.current_doc_parent = find_item( data.docs, choosed_sifra, `sifra`);
                
                
                if ( data.current_doc_parent.storno ) {
                  
                  popup_warn(`Ne možete generirati dokumente ako je ND storniran !!!`);
                  reverse_switch(state, check_elem);
                  return;
                  
                };
                
                

                if ( data.current_doc_parent.doc_sirovs?.length > 0 ) {
                  data.current_doc_parent.doc_sirovs = this_module.reduce_doc_sirovs( data.current_doc_parent.doc_sirovs );
                };
                
                
                // RESETIRAJ DA BUDE  PLAVI BUTTON ZA -----> SAVE NEW ULAZNU PONUDU
                $(`#ulazna_ponuda_btn`).css(`display`, `flex` );
                $(`#update_ponuda_files_btn`).css(`display`, `none` );
                  
                
                if ( data.current_doc_parent.doc_type == "ulazna_ponuda" ) {
                  // prikaži violet button za update filova za ponudu
                  $(`#ulazna_ponuda_btn`).css(`display`, `none` );
                  $(`#update_ponuda_files_btn`).css(`display`, `flex` );
                };
                

                if ( 
                  data.current_doc_parent.doc_type == "ulazna_ponuda"  //  AKO SAM OZNAČIO RED KOJI JE ULAZNA PONUDA  ----> kreiram ND
                  ||
                  data.current_doc_parent.doc_type == "order_dobav"  //  AKO SAM OZNAČIO RED KOJI JE NARUDŽBA DOBAVLJACA -----> kreiram in_record tj primku
                ) {
                  
                  //  resetiraj listu sirovina
                  //  resetiraj listu sirovina
                  data.partner_doc_sirovs = [];
                  $(`#doc_sirovs_list_box`).html(``);
                  
                  var sirovine_ids = [];
                  $.each( data.current_doc_parent.doc_sirovs, function(s_ind, sirov) {
                    if ( sirov._id ) sirovine_ids.push( sirov._id );
                  });
                  
                  // get iste te sirovine iz mongo DB
                  var DB_sirovine = await ajax_find_query( { _id: { $in: sirovine_ids } }, `/find_sirov`, this_module.find_sirov.find_sirov_filter );
                  
                  
                  var sirovine_copy = cit_deep(data.current_doc_parent.doc_sirovs);
                  
                  
                  $.each( sirovine_copy, function(s_ind, sir) {
                    
                    
                    var kolicine_for_doc = [];
                    
                    $.each( sir.doc_kolicine, function(k_ind, kol) {
                      
                      if ( kol.next_doc ) {
                        // ubaci JEDNU količinu samo ako je nema
                        // na ovaj način sam siguran da ima samo jedan item za količinu u arrayu
                        if ( kolicine_for_doc.length == 0 ) kolicine_for_doc.push(kol); // SAMO ULAZNA PONUDA MOŽE IMATI VIŠE DOC KOLIČINA ZA JEDNU SIROVINU !!
                      };
                    }); // loop po količinama iz označen od parent dokumenta
                    
                    
                    if ( DB_sirovine?.length > 0 && kolicine_for_doc.length > 0 ) {
                      
                      
                      // ------------------------ UBACI DOC statuses i doc kolicine u sirovine iz DB !!!!
                      
                      $.each( DB_sirovine, function(db_ind, db_sirov) {
                        if ( db_sirov._id == sir._id) {
                          
                          DB_sirovine[db_ind].doc_statuses = cit_deep(sir.doc_statuses);
                          
                          DB_sirovine[db_ind].doc_kolicine = kolicine_for_doc;
                           
                          // KOPIRAJ NEXT PRICE U PRICE I NEXT COUNT U COUNT !!!!
                          // DB_sirovine[db_ind].doc_kolicine[0].count = kolicine_for_doc[0].next_count; 
                          // DB_sirovine[db_ind].doc_kolicine[0].price = kolicine_for_doc[0].next_price; 
                          
                        };
                        
                      });
                      // ------------------------ ubaci količine u sirovinu iz baze
                    };
                    
                  }); // loop po sirovinama iz doc parenta

                  
                  // sve sirovine prekopiraj nazad u partner doc sirovs tako da sada imam FULL SVE PODATKE OD SVAKE SIROVINE
                  // sve sirovine prekopiraj nazad u partner doc sirovs tako da sada imam FULL SVE PODATKE OD SVAKE SIROVINE
                  // sve sirovine prekopiraj nazad u partner doc sirovs tako da sada imam FULL SVE PODATKE OD SVAKE SIROVINE
                  
                  if ( DB_sirovine?.length > 0 ) {
                    $.each( DB_sirovine, function(s_ind, sir) {
                      data.partner_doc_sirovs = upsert_item( data.partner_doc_sirovs, sir, `_id`);
                    });
                  };
                  
                  
                  this_module.make_doc_sirovs_list(data);
                
                
                // ----------------------------------- AKO SAM OZNAČIO RED KOJI JE ULAZNA PONUDA ILI ORDER DOBAV -----------------------------------
                };
                
                
              // ----------------------------------- KRAJ AKO STATE NIJE FALSE -----------------------------------
              
              } 
              else {
                

                $('#partner_doc_box .choose_doc_row').each( function() {
                  if ( this.id !== check_elem.id ) $(this).removeClass('on').addClass('off');
                });

                
                data.current_doc_parent = null;
                data.partner_doc_sirovs = null;
                // obriši tablicu sirovina ako je bilo nešto od prije !!!
                $(`#doc_sirovs_list_box`).html(``);
                
              };

              console.log( data.current_doc_parent );

            }); // kraj cit run func in data

          }); // kraj each check box

          $('#partner_doc_box .doc_grana').each( function() {

            $(this).off(`click`);
            $(this).on(`click`, function(e) { 
              
              var this_comp_id = $(this).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;
              
              // ako je već fiiltrirana lista onda reseriraj !!!
              // ako je već fiiltrirana lista onda reseriraj !!!
              
              if ( window.partner_docs_are_filtered ) {
                
                window.partner_docs_are_filtered = false;
                this_module.make_partner_doc_list(data);
                
                setTimeout( function() {
                  $('#partner_doc_box .doc_grana').each( function() {
                    $(this).css(`background`, `#3f6ad8`);
                  });
                }, 100 );

                return;
                
              };
              
              window.partner_docs_are_filtered = true;
              
              console.log(`-------- doc grana  ---------`);
              console.log(data);
              
              var doc_sifra = this.id.split(`_`)[0];
              var doc_index = find_index( data.docs, doc_sifra , `sifra` );
              
              var current_doc_sifra = data.docs[doc_index].sifra || null; // samo offer_dobav i offer_reserv nemaju parent sifru !!!!!
              var curr_doc_parent_sifra = data.docs[doc_index].doc_parent?.sifra || null;
              
              // ako je parent već pretvoren u samo string
              if ( !curr_doc_parent_sifra ) curr_doc_parent_sifra = data.docs[doc_index].doc_parent || null;


              data.filtered_docs = [];
              
              $.each( data.docs, function(r_ind, doc) {
                
                if (
                  
                  current_doc_sifra && ( doc.sifra == current_doc_sifra || doc.doc_parent?.sifra == current_doc_sifra )
                  ||
                  curr_doc_parent_sifra && ( doc.sifra == curr_doc_parent_sifra || doc.doc_parent?.sifra == curr_doc_parent_sifra ) 
                   
                  ) { 
                  
                  data.filtered_docs = upsert_item( data.filtered_docs, doc, `sifra` );
                
                };
                
              });
              
              
              // neoj ništa filtrirati ako UOPĆE NEMA NITI JEDAN ITEM !!!
              if ( data.filtered_docs.length == 0 ) {
                window.partner_docs_are_filtered = false;
                return;
              };
              
              this_module.make_partner_doc_list(data);
              
              setTimeout( function() {
                $('#partner_doc_box .doc_grana').each( function() {
                  $(this).css(`background`, `#bf0060`);
                });
                
              }, 100 );
              
              
            }); // kraj click eventa

          }); // kraj each doc grana button
          
          

          $('#partner_doc_box .storno_doc').each( function() {

            $(this).off(`click`);
            $(this).on(`click`, async function(e) { 
              

              e.stopPropagation();
              e.preventDefault();
              
              
              var this_button = this;
              
              var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;
              
              
              
              var popup_text = `Jeste li sigurni da želite napraviti STORNO  ovog ND dokumenta?`;
              

              function storno_doc_yes() {

                var doc_sifra = this_button.id.split(`_`)[0];
                var curr_doc_index = find_index( data.docs, doc_sifra , `sifra` );

                this_module.cit_data[this_comp_id].docs[curr_doc_index].storno = {
                  user: window.cit_user.full_name,
                  time: Date.now(),
                };

                this_module.make_partner_doc_list(data);

                setTimeout( function() { $(`#save_partner_btn`).trigger(`click`); }, 200 );
              
                
              };

              function storno_doc_no() {
                
                show_popup_modal(false, popup_text, null );
                return;
              };

              var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
              var show_popup_modal = pop_mod.show_popup_modal;
              show_popup_modal(`warning`, popup_text, null, 'yes/no', storno_doc_yes, storno_doc_no, null);
              
            }); // kraj click eventa

          }); // kraj each doc grana button
                    
          
          
          
          $('#partner_doc_box .next_price').each( function() {

            $(this).off(`blur`);
            $(this).on(`blur`, function(e) { 

              var this_input = this;

              var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;
              
              var doc_sifra = this_input.id.split(`_`)[0];
              var kol_sifra = this_input.id.split(`_`)[1];
              var sirov_id = this_input.id.split(`_`)[2];

              if ( $(this_input).val() == `` ) {
                this_module.cit_data[this_comp_id] = this_module.write_kolicina_value( data, null, doc_sifra, kol_sifra, sirov_id, "next_price" );
                
                setTimeout( function() { $(`#save_partner_btn`).trigger(`click`); }, 200 );
                
                return;
              };

              
              var input_value = cit_number( $(this_input).val() );
              
              if ( input_value !== null ) {
                this_module.cit_data[this_comp_id] = this_module.write_kolicina_value( data, input_value, doc_sifra, kol_sifra, sirov_id, "next_price" );
                $(this_input).val( cit_format( input_value, 3 ) );
                
                setTimeout( function() { $(`#save_partner_btn`).trigger(`click`); }, 200 );
                
              } 
              else {
                popup_warn(`Nije broj :-P`);
                $(this_input).val(``);
              };

              console.log(data.docs);
              
              
            }); // kraj blur eventa na offer dobav price
          
          });
          
          $('#partner_doc_box .next_count').each( function() {

            $(this).off(`blur`);
            $(this).on(`blur`, function(e) { 

              var this_input = this;

              var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;
              
              var doc_sifra = this_input.id.split(`_`)[0];
              var kol_sifra = this_input.id.split(`_`)[1];
              var sirov_id = this_input.id.split(`_`)[2];

              if ( $(this_input).val() == `` ) {
                this_module.cit_data[this_comp_id] = this_module.write_kolicina_value( data, null, doc_sifra, kol_sifra, sirov_id, "next_count" );
                
                setTimeout( function() { $(`#save_partner_btn`).trigger(`click`); }, 200 );
                
                return;
              };

              
              var input_value = cit_number( $(this_input).val() );
              
              if ( input_value !== null ) {
                this_module.cit_data[this_comp_id] = this_module.write_kolicina_value( data, input_value, doc_sifra, kol_sifra, sirov_id, "next_count" );
                $(this_input).val( cit_format( input_value, 3 ) );
                
                setTimeout( function() { $(`#save_partner_btn`).trigger(`click`); }, 200 );
                
              } 
              else {
                popup_warn(`Nije broj :-P`);
                $(this_input).val(``);
              };

              console.log(data.docs);
              
              
            }); // kraj blur eventa na offer dobav price
          
          });
          
        
          $('#partner_doc_box .next_doc').each( function() {

            $(this).data(`cit_run`, function(state, check_elem) { 

              var this_comp_id = $(check_elem).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;
              
              
              var parent_row_id = $(check_elem).closest('.search_result_row')[0].id;
              var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
              var index = find_index(data.ibans, sifra , `sifra` );
              

              console.log(`-------- APPROVE check box ---------`);
              console.log(data);

              var doc_sifra = check_elem.id.split(`_`)[0];
              var kol_sifra = check_elem.id.split(`_`)[1];
              var sirov_sifra = check_elem.id.split(`_`)[2];
              
              // pretvori sve ostale check boxove u 
              /*
              if ( state == true ) {
                $.each( `#` + parent_row_id + ` .next_doc` ).each(function() {
                  if ( this.id !== check_elem.id ) $(this).removeClass('on').addClass('off');
                });
                
              };
              */
              
              
              var user_izabrao_iste_sirovine = false;
              
              $.each( data.docs, function(d_ind, doc) {
                $.each( doc.doc_sirovs, function(s_ind, sir) {
                  $.each( sir.doc_kolicine, function(k_ind, kol) {
                    
                    // ako je neka druga količina aili je ista sirovina onda STOP !!!!!!
                    // ako je neka druga količina aili je ista sirovina onda STOP !!!!!!
                    // jer nema smisla birati istu sirovinu i dvije različite količine !!!!!!!!!!!!!!
                    if ( 
                      kol.next_doc == true
                      &&
                      sir.sifra == sirov_sifra 
                      &&
                      kol.sifra !== kol_sifra 
                      
                    ) {
                      user_izabrao_iste_sirovine = true;
                      
                    };
                  
                  });  
                  
                });  
              });
              

              if ( user_izabrao_iste_sirovine ) {
                popup_warn(`Nema smisla izabrati dvije različite količine za ISTU sirovinu !!!!!!`);
                reverse_switch(state, check_elem);
                return;
              };
              
              
              this_module.cit_data[this_comp_id] = this_module.write_kolicina_value( data, state, doc_sifra, kol_sifra, sirov_sifra, "next_doc" );
              
              console.log( data.docs );
              
              setTimeout( function() { $(`#save_partner_btn`).trigger(`click`); }, 200 );
              

            }); // kraj cit run func in data

          }); // kraj each check box

          $('#partner_doc_box .next_approved').each( function() {

            $(this).data(`cit_run`, async function(state, check_elem) { 

              var popup_text = ``;
              if (state == true) popup_text = `Jeste li sigurni da želite ODOBRITI ovu liniju !!!`;
              if (state == false) popup_text = `Jeste li sigurni da želite PONIŠTITI ODOBRENJE za ovu liniju !!!`;

              function approve_yes() {

                var this_comp_id = $(check_elem).closest('.cit_comp')[0].id;
                var data = this_module.cit_data[this_comp_id];
                var rand_id =  this_module.cit_data[this_comp_id].rand_id;

                console.log(`-------- APPROVE check box ---------`);
                console.log(data);

                var doc_sifra = check_elem.id.split(`_`)[0];
                var kol_sifra = check_elem.id.split(`_`)[1];
                var sirov_sifra = check_elem.id.split(`_`)[2];

                this_module.cit_data[this_comp_id] = this_module.write_kolicina_value( data, state, doc_sifra, kol_sifra, sirov_sifra, "next_approved" );

                console.log( data.docs );

                setTimeout( function() { $(`#save_partner_btn`).trigger(`click`); }, 200 );
              
                
              };

              function approve_no() {
                
                show_popup_modal(false, popup_text, null );
                setTimeout( function() { reverse_switch(state, check_elem); }, 200);
                return;
                
              };

              var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
              var show_popup_modal = pop_mod.show_popup_modal;
              show_popup_modal(`warning`, popup_text, null, 'yes/no', approve_yes, approve_no, null);

              

            }); // kraj cit run func in data

          }); // kraj each check box  
  
        
          // -------------------------------- DOC ZA KUPCA KAO NPR OFFER RESERV I ORDER RESERV 
          
          $('#partner_doc_box .pay_value').each( function() {

            $(this).off(`blur`);
            $(this).on(`blur`, function(e) { 

              var this_input = this;
              
              if ( !window.cit_user ) {
                popup_warn(`Niste se ulogirali !!!`);
                return;
              };
              

              var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;
              
              var doc_sifra = this_input.id.split(`_`)[0];

              if ( $(this_input).val() == `` ) {
                
                this_module.cit_data[this_comp_id] = this_module.write_payments( data, null, doc_sifra, "pay_value" );
                
                
                this_module.make_partner_doc_list(data);
                
                setTimeout( function() { $(`#save_partner_btn`).trigger(`click`); }, 200 );
                
                return;
              };

              
              var input_value = cit_number( $(this_input).val() );
              
              if ( input_value !== null ) {
                
                this_module.cit_data[this_comp_id] = this_module.write_payments( data, input_value, doc_sifra, "pay_value" );
                this_module.cit_data[this_comp_id] = this_module.write_payments( data, Date.now(), doc_sifra, "pay_value_time" );
                this_module.cit_data[this_comp_id] = this_module.write_payments( data, window.cit_user.full_name, doc_sifra, "pay_value_name" );
                
                
                $(this_input).val( cit_format( input_value, 2 ) );
                
                
                this_module.make_partner_doc_list(data);
                
                
                setTimeout( function() { $(`#save_partner_btn`).trigger(`click`); }, 200 );
                
              } 
              else {
                popup_warn(`Nije broj :-P`);
                $(this_input).val(``);
              };

              console.log(data.docs);
              
              
            }); // kraj blur eventa na offer dobav price
          
          });
          
          $('#partner_doc_box .pay_komentar').each( function() {

            $(this).off(`blur`);
            $(this).on(`blur`, function(e) { 

              var this_input = this;
              
              if ( !window.cit_user ) {
                popup_warn(`Niste se ulogirali !!!`);
                return;
              };

              var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;
              
              var doc_sifra = this_input.id.split(`_`)[0];
              
              var input_text = $(this_input).val();

              if ( input_text == `` ) {
                this_module.cit_data[this_comp_id] = this_module.write_payments( data, null, doc_sifra, "pay_komentar" );
                setTimeout( function() { $(`#save_partner_btn`).trigger(`click`); }, 200 );
                return;
              };

              this_module.cit_data[this_comp_id] = this_module.write_payments( data, input_text, doc_sifra, "pay_komentar" );
              setTimeout( function() { $(`#save_partner_btn`).trigger(`click`); }, 200 );
              console.log(data.docs);
              
            }); // kraj blur eventa na offer dobav price
          
          });
          
          $('#partner_doc_box .ad_acta_btn').each( function() {

            $(this).off(`click`);
            $(this).on(`click`, async function(e) { 
              
              
              if ( !window.cit_user ) {
                popup_warn(`Niste se ulogirali !!!`);
                return;
              };
              
              e.stopPropagation();
              e.preventDefault();

              
              var this_button = this;
              
              
              var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;

              
              var popup_text = `Jeste li sigurni da želite spremiti ovaj dokument kao da je u potpunosti plaćen !!!!<br>PAZITE !!!<br>Nije moguće vratiti nazad ovu radnju!!!`;

              function ad_acta_yes() {

                var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
                var data = this_module.cit_data[this_comp_id];
                var rand_id =  this_module.cit_data[this_comp_id].rand_id;

                var doc_sifra = this_button.id.split(`_`)[0];
                
                this_module.cit_data[this_comp_id] = this_module.write_payments( data, true, doc_sifra, "ad_acta" );
                this_module.cit_data[this_comp_id] = this_module.write_payments( data, Date.now(), doc_sifra, "ad_acta_time" );
                this_module.cit_data[this_comp_id] = this_module.write_payments( data, window.cit_user?.full_name, doc_sifra, "ad_acta_name" );
                
                $(this_button).addClass(`cit_disable`);
                // obriši cijeli gumb !!!
                // $(this_button).remove();
                
                
                
                setTimeout( function() { $(`#save_partner_btn`).trigger(`click`); }, 200 );

                
              };

              function ad_acta_no() {
                show_popup_modal(false, popup_text, null );
                return;
              };

              var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
              var show_popup_modal = pop_mod.show_popup_modal;
              show_popup_modal(`warning`, popup_text, null, 'yes/no', ad_acta_yes, ad_acta_no, null);

              
            }); // kraj click eventa

          }); // kraj each doc grana button
          
          
          // ------------END-------------------- DOC ZA KUPCA KAO NPR OFFER RESERV I ORDER RESERV 
          
          
          jQuery('#partner_doc_box .scrollbar-macosx').scrollbar( { autoUpdate: true });
          
          reset_tooltips();
          
          window.doc_list_events_registered = true;
          
          
        },5*1000 );

      }; // kraj ifa ako docs table array nije prazan


    }; // kraj ako postoji docs length > 0





  };
  this_module.make_partner_doc_list = make_partner_doc_list;
  
  
  function make_iban_list(data) {
    
    // IBANS
    cit_local_list.company_ibans_for_table = (data.ibans && data.ibans.length > 0) ? data.ibans : [];
    
    
    
    $.each( cit_local_list.company_ibans_for_table, function(iban_ind, iban) {
      cit_local_list.company_ibans_for_table[iban_ind].bank_name = iban.bank?.naziv || "";
    });
    
    var ibans_props = {
      desc: 'samo za kreiranje tablice svih ibana u partneru',
      local: true,
      
      list: 'company_ibans_for_table',
      
      show_cols: ['num', "bank_name", 'button_edit', 'button_delete' ],
      col_widths: [3, 3, 1, 1],
      parent: "#partner_iban_list_box",
      return: {},
      show_on_click: false,
      
      cit_run: function(state) { console.log(state); },
      
      button_edit: function(e) {
        
        e.stopPropagation(); 
        e.preventDefault();

        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var index = find_index(data.ibans, sifra , `sifra` );

        window.current_iban = cit_deep( data.ibans[index] );


        $('#save_iban').css(`display`, `none`);
        $('#update_iban').css(`display`, `flex`);

        $('#'+rand_id+`_iban_bank`).val(window.current_iban.bank?.naziv || "");
        $('#'+rand_id+`_iban_num`).val(window.current_iban.num || "");

      
      },
      button_delete: async function(e) {
        
        e.stopPropagation();
        e.preventDefault();
        
      
        var this_button = this;
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          data.ibans = delete_item(data.ibans, sifra , `sifra` );
          
          $(`#`+parent_row_id).remove();
 

        };
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj iban?`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj iban?`, null, 'yes/no', delete_yes, delete_no, null);
         
        
      },
      
    };
    if (cit_local_list.company_ibans_for_table.length > 0 ) create_cit_result_list(cit_local_list.company_ibans_for_table, ibans_props );
    
    
    
  };
  this_module.make_iban_list = make_iban_list;
  
  
  
  function save_current_def_pay() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    
    if ( !window.current_def_pay ) window.current_def_pay = {};
    
    if ( !window.current_def_pay.sifra ) window.current_def_pay.sifra = `defp`+cit_rand();
    
    if ( !window.current_def_pay.valuta_on_time && !window.current_def_pay.valuta_value ) {
      popup_warn(`Potrebno je odrediti jel valuta na datum plaćanja ili je dogovoreni tečaj!!!!`);
      return;
    };
    
    
    if ( !window.current_def_pay.naziv ) {
      popup_warn(`Potrebno je izabrati oznaku valute !!!!`);
      return;
    };
    
    
    data.def_pays = upsert_item(data.def_pays, window.current_def_pay, `sifra`);
    
    data.def_pays_flat = JSON.stringify(data.def_pays);
    
    this_module.make_def_pay_list(data);
    
    window.current_def_pay = null;
    
    $('#'+rand_id+`_definicija_valute`).val("");
    $('#'+rand_id+`_valuta_value`).val("");
    $('#'+rand_id+`_valuta_on_time`).removeClass(`on`).addClass(`off`);
    
    if ( this.id == `update_def_pay` ) {
      $(this).css(`display`, `none`);
      $(`#save_def_pay`).css(`display`, `flex`);
    };  
    
    
  };
  this_module.save_current_def_pay = save_current_def_pay;
  
  
  function make_def_pay_list(data) {
    
    // DEF VALUTA
    cit_local_list.def_pay_for_table = (data.def_pays && data.def_pays.length > 0) ? data.def_pays : [];
    var def_pay_props = {
      desc: 'samo za kreiranje tablice svih definicija valute u partneru',
      local: true,
      
      list: 'def_pay_for_table',
      
      show_cols: ['valuta', "valuta_on_time", "valuta_value", 'button_edit', 'button_delete' ],
      col_widths: [1, 1, 2, 1, 1],
      parent: "#def_valute_box",
      return: {},
      show_on_click: false,
      
      cit_run: function(state) { console.log(state); },
      
      button_edit: function(e) { 
        
        
        e.stopPropagation(); 
        e.preventDefault();
        
        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var index = find_index(data.def_pays, sifra , `sifra` );

        window.current_def_pay = cit_deep( data.def_pays[index] );
        
        $('#save_def_pay').css(`display`, `none`);
        $('#update_def_pay').css(`display`, `flex`);
        
        $('#'+rand_id+`_definicija_valute`).val(window.current_def_pay.naziv || "");
        
        toggle_cit_switch( window.current_def_pay.valuta_on_time || false, $('#'+rand_id+`_valuta_on_time`) );
        format_number_input(window.current_def_pay.valuta_value, $('#'+rand_id+`_valuta_value`)[0], null);
        
      },
      button_delete: async function(e) {
        
        
        e.stopPropagation();
        e.preventDefault();
        
      
        var this_button = this;
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          data.def_pays = delete_item(data.def_pays, sifra , `sifra` );
          
          $(`#`+parent_row_id).remove();


        };
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovu definiciju valute?`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovu definiciju valute?`, null, 'yes/no', delete_yes, delete_no, null);
                
        
      
      
      },
      
    };
    if (cit_local_list.def_pay_for_table.length > 0 ) create_cit_result_list(cit_local_list.def_pay_for_table, def_pay_props );
        
    
  };
  this_module.make_def_pay_list = make_def_pay_list;
  
  
  
  function save_current_def_pay() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    
    if ( !window.current_def_pay ) window.current_def_pay = {};
    
    if ( !window.current_def_pay.sifra ) window.current_def_pay.sifra = `defp`+cit_rand();
    
    if ( !window.current_def_pay.valuta_on_time && !window.current_def_pay.valuta_value ) {
      popup_warn(`Potrebno je odrediti jel valuta na datum plaćanja ili je dogovoreni tečaj!!!!`);
      return;
    };
    
    if ( !window.current_def_pay.valuta_value ) window.current_def_pay.valuta_value = null;
    
    if ( !window.current_def_pay.naziv ) {
      popup_warn(`Potrebno je izabrati oznaku valute !!!!`);
      return;
    };
    
    
    data.def_pays = upsert_item(data.def_pays, window.current_def_pay, `sifra`);
    
    data.def_pays_flat = JSON.stringify(data.def_pays);
    
    this_module.make_def_pay_list(data);
    
    window.current_def_pay = null;
    
    $('#'+rand_id+`_definicija_valute`).val("");
    $('#'+rand_id+`_valuta_value`).val("");
    $('#'+rand_id+`_valuta_on_time`).removeClass(`on`).addClass(`off`);
    
    if ( this.id == `update_def_pay` ) {
      $(this).css(`display`, `none`);
      $(`#save_def_pay`).css(`display`, `flex`);
    };  
    
    
  };
  this_module.save_current_def_pay = save_current_def_pay;
  
  
  
  
  function init_partner_map() {
    
    var condition = function() {
      
      var is_ok = false;
      if (
        
        !window.google_maps_loaded           &&
        $("#pac-input").length > 0           &&
        typeof window.google !== "undefined" && 
        window.google !== null
        
      ) { is_ok = true; };
      
      return is_ok;  
    };
    
    wait_for( condition, function() {
      
      window.google_maps_loaded = true;
      var this_comp_id = $("#pac-input").closest('.cit_comp')[0].id;
      var rand_id = this_module.cit_data[this_comp_id].rand_id;
      init_google_map(rand_id);
      
    }, 50*1000);
  };
  this_module.init_partner_map = init_partner_map;
  
  
  
 
  
  function choose_nacin_dostave(state) {
    
    console.log(state);
    
    // deep copy od state
    var state = cit_deep(state);
    
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    
    if ( state == null ) {
      $('#'+current_input_id).val(``);
      return;
    };
    
    
    $('#'+current_input_id).val(state.naziv);
    
    if ( !data.nacini_dostave) data.nacini_dostave = [];
    
    // UPDATE ILI INSERT AKO NE POSTOJI
    data.nacini_dostave = upsert_item(data.nacini_dostave, state, 'sifra');
    
    data.nacini_dostave_flat = JSON.stringify(data.nacini_dostave);
    
    // update multi list in props
    $('#'+current_input_id).data(`cit_props`).multi_list = data.nacini_dostave;
    var props = $('#'+current_input_id).data(`cit_props`);
    // ispočetka kreiraj multi list
    create_multi_list(current_input_id, props);
    // pričekaj da se kreira multi list i onda nakači evente ponovo
    wait_for(`$('#${current_input_id}_multi_list').length > 0`, function() {
      this_module.register_nacini_dostave_events(current_input_id);
    }, 5*1000);
    
    
  };
  this_module.choose_nacin_dostave = choose_nacin_dostave;

  
  function register_nacini_dostave_events(input_id) {

    $(`#${input_id}_multi_list .multi_cell_delete_btn`).off(`click`);
    $(`#${input_id}_multi_list .multi_cell_delete_btn`).on(`click`, function() {

      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      
      console.log(`prije brisanja: `,  data.nacini_dostave);

      var row_id = $(this).parent().attr(`data-row_id`);
      var sifra = row_id.replace(`multi_row_`, ``);
      
      $(`#${row_id}`).remove();
      
      data.nacini_dostave = delete_item(data.nacini_dostave, sifra , `sifra` );

      console.log(`nakon brisanja: `, this_module.cit_data[this_comp_id].nacini_dostave);

    });
    
  };
  this_module.register_nacini_dostave_events = register_nacini_dostave_events;
  
  
  function update_debt_from_date_kupac(state, this_input) {
      
    if ( !window.current_debt_kupac ) window.current_debt_kupac = {};
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;

    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      window.current_debt_kupac.date_from = null;
      console.log(`current_debt_kupac_date_from: `, null );
      return;
    };
    
    window.current_debt_kupac.date_from = state;
    console.log("current_debt_kupac_date_from", new Date(state));
    
  };
  this_module.update_debt_from_date_kupac = update_debt_from_date_kupac;
  

  function update_debt_to_date_kupac(state, this_input) {
      
    if ( !window.current_debt_kupac ) window.current_debt_kupac = {};
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;

    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      window.current_debt_kupac.date_to = null;
      console.log(`current_debt_kupac_date_to: `, null );
      return;
    };
    
    window.current_debt_kupac.date_to = state;
    console.log("current_debt_kupac_date_to", new Date(state));
    
  };
  this_module.update_debt_to_date_kupac = update_debt_to_date_kupac;
  
  
  function update_debt_from_date_dobav(state, this_input) {
      
    if ( !window.current_debt_dobav ) window.current_debt_dobav = {};
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;

    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      window.current_debt_dobav.date_from = null;
      console.log(`current_debt_dobav.date_from: `, null );
      return;
    };
    
    window.current_debt_dobav.date_from = state;
    console.log("current_debt_dobav.date_from", new Date(state));
    
  };
  this_module.update_debt_from_date_dobav = update_debt_from_date_dobav;
  
    
  function update_debt_to_date_dobav(state, this_input ) {
      
    if ( !window.current_debt_dobav ) window.current_debt_dobav = {};
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;

    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      window.current_debt_dobav.date_to = null;
      console.log(`current_debt_dobav.date_to: `, null );
      return;
    };
    
    window.current_debt_dobav.date_to = state;
    console.log("current_debt_dobav.date_to", new Date(state));
    
    
  };
  this_module.update_debt_to_date_dobav = update_debt_to_date_dobav;
  
  
  function update_ugovor_docs(files, button, arg_time) {

    
    var new_docs_arr = [];
    $.each(files, function(f_index, file) {
      
      var file_obj = {
        sifra: `doc`+cit_rand(),
        time: arg_time,
        link: `<a href="/docs/${time_path(arg_time)}/${file.filename}" target="_blank" onClick="event.stopPropagation();">${ file.originalname }</a>`,
      };
      new_docs_arr.push( file_obj );
    });
    
    console.log( new_docs_arr );
    
    // napravi novi objekt ako current ne postoji
    if ( !window.current_ugovor ) window.current_ugovor = {};
    
    
    if ( $.isArray(window.current_ugovor.docs) ) {
      // ako postoji docs array onda dodaj nove filove
      window.current_ugovor.docs = [ ...window.current_ugovor.docs, ...new_docs_arr ];
    } else {
      // ako ne postoji onda stavi ove nove filove
      window.current_ugovor.docs = new_docs_arr;
    };    
    
    
  };
  this_module.update_ugovor_docs = update_ugovor_docs;
  
  
  function choose_ugovor_date(state, this_input) {
    
    if ( !window.current_ugovor ) window.current_ugovor = {};
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;

    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      window.current_ugovor.date = null;
      console.log(`current_ugovor_date: `, null );
      return;
    };
    
    window.current_ugovor.date = state;
    console.log(`new ugovor date: `, new Date(state));
    
  };
  this_module.choose_ugovor_date = choose_ugovor_date;
  
  

  function choose_ugovor_start_date(state, this_input) {
    
    if ( !window.current_ugovor ) window.current_ugovor = {};
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;

    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      window.current_ugovor.start = null;
      console.log(`current_ugovor start: `, null );
      return;
    };
    
    window.current_ugovor.start = state;
    console.log(`new ugovor start: `, new Date(state));
    
  };
  this_module.choose_ugovor_start_date = choose_ugovor_start_date;
    
  
  function choose_ugovor_end_date(state, this_input) {
    
    if ( !window.current_ugovor ) window.current_ugovor = {};
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;

    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      window.current_ugovor.end = null;
      console.log(`current_ugovor end: `, null );
      return;
    };
    
    window.current_ugovor.end = state;
    console.log(`new ugovor end: `, new Date(state));
    
  };
  this_module.choose_ugovor_end_date = choose_ugovor_end_date;
  
  
  
  function choose_tender_date(state, this_input) {
    
    if ( !window.current_tender ) window.current_tender = {};
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;

    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      window.current_tender.date = null;
      console.log(`current_tender_date: `, null );
      return;
    };
    
    window.current_tender.date = state;
    console.log(`new tender date: `, new Date(state));
    
  };
  this_module.choose_tender_date = choose_tender_date;
  
  
  function choose_tender_rok_prijave(state, this_input) {
    
    if ( !window.current_tender ) window.current_tender = {};
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;

    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      window.current_tender.rok = null;
      console.log(`current_tender rok: `, null );
      return;
    };
    
    window.current_tender.rok = state;
    console.log(`new tender rok: `, new Date(state));
    
  };
  this_module.choose_tender_rok_prijave = choose_tender_rok_prijave;
  
  
  function choose_tender_start_date(state, this_input) {
    
    if ( !window.current_tender ) window.current_tender = {};
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;

    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      window.current_tender.start = null;
      console.log(`current_tender start: `, null );
      return;
    };
    
    window.current_tender.start = state;
    console.log(`new tender start: `, new Date(state));
    
  };
  this_module.choose_tender_start_date = choose_tender_start_date;
    
  
  function choose_tender_end_date(state, this_input) {
    
    if ( !window.current_tender ) window.current_tender = {};
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;

    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      window.current_tender.end = null;
      console.log(`current_tender end: `, null );
      return;
    };
    
    window.current_tender.end = state;
    console.log(`new tender end: `, new Date(state));
    
  };
  this_module.choose_tender_end_date = choose_tender_end_date;
  
  
  
  function choose_tender_status(state) {
    
    var this_input = $(`#`+current_input_id);
    
    if ( !window.current_tender ) window.current_tender = {};
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;

    if ( state == null ) {
      this_input.val(``);
      window.current_tender.status = null;
      console.log(`current_tender status: `, null );
      return;
    };
    
    this_input.val(state.naziv);
    
    window.current_tender.status = state;
    console.log(`new tender status: `, state );
    
  };
  this_module.choose_tender_status = choose_tender_status;
  
  
  
  function make_sconto_kupac_list(data) {
    
    cit_local_list.sconto_kupac_za_table = (data.cassa_sconto_kupac && data.cassa_sconto_kupac.length > 0) ? data.cassa_sconto_kupac : [];
    var sconto_kupac_props = {
      desc: 'za kreiranje tablice svih cassa sconto od kupca',
      local: true,

      list: 'sconto_kupac_za_table',

      show_cols: ['days', "perc", 'button_edit', 'button_delete' ],
      // ako postoji property format_cols
      // to znači da taj property treba foramtirati kao broj s tim brojem decimala
      format_cols: { days: 0, perc: 2 },
      col_widths: [1, 1, 1, 1],
      parent: "#sconto_kupac_box",
      return: {},
      show_on_click: false,

      cit_run: function(state) { console.log(state); },

      button_edit: function(e) {
        
        e.stopPropagation(); 
        e.preventDefault();

        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var index = find_index(data.cassa_sconto_kupac, sifra , `sifra` );

        window.current_sconto_kupac = cit_deep( data.cassa_sconto_kupac[index] );


        $('#save_sconto_kupac').css(`display`, `none`);
        $('#update_sconto_kupac').css(`display`, `flex`);
        
        
        $('#'+rand_id+`_cassa_sconto_days_kupac`).val(window.current_sconto_kupac.days || "");
        format_number_input(window.current_sconto_kupac.perc, $('#'+rand_id+`_cassa_sconto_perc_kupac`)[0], null);
        
        
      },
      button_delete: async function(e) { 
        
        e.stopPropagation(); 
        e.preventDefault();
            
        var this_button = this;
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          data.cassa_sconto_kupac = delete_item(data.cassa_sconto_kupac, sifra , `sifra` );
          
          $(`#`+parent_row_id).remove();

        };
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj Sconto?`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj Sconto?`, null, 'yes/no', delete_yes, delete_no, null);

      
      
      },

    };
    if (cit_local_list.sconto_kupac_za_table.length > 0 ) create_cit_result_list(cit_local_list.sconto_kupac_za_table, sconto_kupac_props );
  };
  this_module.make_sconto_kupac_list = make_sconto_kupac_list;

  
  
  function save_current_sconto_kupac() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    if ( !window.current_sconto_kupac ) window.current_sconto_kupac = {};
    
    if ( !window.current_sconto_kupac.sifra ) window.current_sconto_kupac.sifra = `sconto`+cit_rand();

    if ( 
      $('#'+rand_id+`_cassa_sconto_days_kupac`).val() == "" 
      &&
      !window.current_sconto_kupac.perc 
    ) {
      popup_warn(`Potrebno je upisati broj dana i postotak !!!!`);
      return;
    };
    
    var days = cit_number($('#'+rand_id+`_cassa_sconto_days_kupac`).val());
    window.current_sconto_kupac.days = $.isNumeric(days) ? days : null;
    
 
    data.cassa_sconto_kupac = upsert_item(data.cassa_sconto_kupac, window.current_sconto_kupac, `sifra`);
    
    data.cassa_sconto_kupac_flat = JSON.stringify(data.cassa_sconto_kupac);
    
    this_module.make_sconto_kupac_list(data);
    
    window.current_sconto_kupac = null;
    
    
    
    $('#'+rand_id+`_cassa_sconto_days_kupac`).val("");
    $('#'+rand_id+`_cassa_sconto_perc_kupac`).val("");
    
    
    if ( this.id == `update_sconto_kupac` ) {
      $(this).css(`display`, `none`);
      $(`#save_sconto_kupac`).css(`display`, `flex`);
    };
    
  };
  this_module.save_current_sconto_kupac = save_current_sconto_kupac;
    
  
  
  function make_sconto_dobav_list(data) {
    cit_local_list.sconto_dobav_za_table = (data.cassa_sconto_dobav && data.cassa_sconto_dobav.length > 0) ? data.cassa_sconto_dobav : [];
    var sconto_dobav_props = {
      desc: 'za kreiranje tablice svih cassa sconto od dobavljača',
      local: true,

      list: 'sconto_dobav_za_table',

      show_cols: ['days', "perc", 'button_edit', 'button_delete' ],
      // ako postoji property format_cols
      // to znači da taj property treba foramtirati kao broj s tim brojem decimala
      format_cols: { days: 0, perc: 2 },
      col_widths: [1, 1, 1, 1],
      parent: "#sconto_dobav_box",
      return: {},
      show_on_click: false,

      cit_run: function(state) { console.log(state); },

      button_edit: function(e) { 
        
        e.stopPropagation(); 
        e.preventDefault();

        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var index = find_index(data.cassa_sconto_dobav, sifra , `sifra` );

        window.current_sconto_dobav = cit_deep( data.cassa_sconto_dobav[index] );


        $('#save_sconto_dobav').css(`display`, `none`);
        $('#update_sconto_dobav').css(`display`, `flex`);

        $('#'+rand_id+`_cassa_sconto_days_dobav`).val(window.current_sconto_dobav.days || "");
        format_number_input(window.current_sconto_dobav.perc, $('#'+rand_id+`_cassa_sconto_perc_dobav`)[0], null);
      
      },
      button_delete: async function(e) {
        
        e.stopPropagation(); 
        e.preventDefault();
        
        var this_button = this;
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          data.cassa_sconto_dobav = delete_item(data.cassa_sconto_dobav, sifra , `sifra` );
          
          $(`#`+parent_row_id).remove();
          
        };
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj Sconto?`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj Sconto?`, null, 'yes/no', delete_yes, delete_no, null);
        
        
        
      },

    };
    if (cit_local_list.sconto_dobav_za_table.length > 0 ) create_cit_result_list(cit_local_list.sconto_dobav_za_table, sconto_dobav_props );
  };
  this_module.make_sconto_dobav_list = make_sconto_dobav_list;


  function save_current_sconto_dobav() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    if ( !window.current_sconto_dobav ) window.current_sconto_dobav = {};
    
    if ( !window.current_sconto_dobav.sifra ) window.current_sconto_dobav.sifra = `sconto`+cit_rand();

    if ( 
      $('#'+rand_id+`_cassa_sconto_days_dobav`).val() == "" 
      &&
      !window.current_sconto_dobav.perc ) {
      popup_warn(`Potrebno je upisati broj dana i postotak !!!!`);
      return;
    };
 
    var days = cit_number($('#'+rand_id+`_cassa_sconto_days_dobav`).val());
    window.current_sconto_dobav.days = $.isNumeric(days) ? days : null;
    
    
    data.cassa_sconto_dobav = upsert_item(data.cassa_sconto_dobav, window.current_sconto_dobav, `sifra`);
    
    data.cassa_sconto_dobav_flat = JSON.stringify(data.cassa_sconto_dobav);
    
    this_module.make_sconto_dobav_list(data);
    
    window.current_sconto_dobav = null;
    
    
    
    $('#'+rand_id+`_cassa_sconto_days_dobav`).val("");
    $('#'+rand_id+`_cassa_sconto_perc_dobav`).val("");
    
    
    if ( this.id == `update_sconto_dobav` ) {
      $(this).css(`display`, `none`);
      $(`#save_sconto_dobav`).css(`display`, `flex`);
    };
    
  };
  this_module.save_current_sconto_dobav = save_current_sconto_dobav;
  
  
  
  
  // ---------------START--------------------- BONUS KUPAC ------------------------------------

  function make_bonus_kupac_list(data) {
    
    cit_local_list.bonusi_table_kupac = (data.end_year_bonus_kupac && data.end_year_bonus_kupac.length > 0) ? data.end_year_bonus_kupac : [];
    
    var bonus_kupac_props = {
      desc: 'samo za kreiranje tablice svih godišnjih bonusa koje imamo kod nekog kupca',
      local: true,

      list: 'bonusi_table_kupac',

      show_cols: ['amount_min', "amount_max", 'date_from', 'date_to', 'perc', 'button_edit', 'button_delete' ],
      format_cols: { amount_min: 0, amount_max: 0, date_from: "date", date_to: "date", perc: 2 },
      col_widths: [3, 3, 2, 2, 2, 1, 1],
      parent: "#god_bonusi_box_kupac",
      return: {},
      show_on_click: false,

      cit_run: function(state) { console.log(state); },

      button_edit: function(e) { 
      
      
        e.stopPropagation(); 
        e.preventDefault();

        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var index = find_index(data.end_year_bonus_kupac, sifra , `sifra` );

        window.current_bonus_kupac = cit_deep( data.end_year_bonus_kupac[index] );


        $('#save_bonus_kupac').css(`display`, `none`);
        $('#update_bonus_kupac').css(`display`, `flex`);


        format_number_input( window.current_bonus_kupac.amount_min, $('#'+rand_id+`_bonus_amount_min_kupac`)[0], null);
        format_number_input( window.current_bonus_kupac.amount_max, $('#'+rand_id+`_bonus_amount_max_kupac`)[0], null);
        
        $('#'+rand_id+`_bonus_date_start_kupac`).val( cit_dt(window.current_bonus_kupac.date_from).date || "" );
        $('#'+rand_id+`_bonus_date_end_kupac`).val( cit_dt(window.current_bonus_kupac.date_to).date || "" );
        
        
        format_number_input( window.current_bonus_kupac.perc, $('#'+rand_id+`_bonus_perc_kupac`)[0], null);      
      
      
      },
      button_delete: async function(e) {
        
        
        e.stopPropagation(); 
        e.preventDefault();
            
        var this_button = this;
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          data.end_year_bonus_kupac = delete_item(data.end_year_bonus_kupac, sifra , `sifra` );
          
          $(`#`+parent_row_id).remove();

        };
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj Bonus?`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj Bonus?`, null, 'yes/no', delete_yes, delete_no, null);
      
        
        
      },

    };
    // ako imam išta u tom arraju onda napravi tablicu !!!!
    if (cit_local_list.bonusi_table_kupac.length > 0 ) create_cit_result_list(cit_local_list.bonusi_table_kupac, bonus_kupac_props );
  };
  this_module.make_bonus_kupac_list = make_bonus_kupac_list;
  

  function update_bonus_date_start_kupac(state, this_input) {

    if ( !window.current_bonus_kupac ) window.current_bonus_kupac = {};


    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      window.current_bonus_kupac.date_from = null;
      console.log(`current_bonus_kupac.date_from: `, null );
      return;
    };

    window.current_bonus_kupac.date_from = state;
    console.log("current_bonus_kupac.date_from", new Date(state));

  };
  this_module.update_bonus_date_start_kupac = update_bonus_date_start_kupac;


  function update_bonus_date_end_kupac(state, this_input) {

    if ( !window.current_bonus_kupac ) window.current_bonus_kupac = {};


    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      window.current_bonus_kupac.date_to = null;
      console.log(`current_bonus_kupac.date_to: `, null );
      return;
    };

    window.current_bonus_kupac.date_to = state;
    console.log("current_bonus_kupac.date_to", new Date(state));

  };
  this_module.update_bonus_date_end_kupac = update_bonus_date_end_kupac;

  
  function save_current_bonus_kupac() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    if ( !window.current_bonus_kupac ) window.current_bonus_kupac = {};
    
    if ( !window.current_bonus_kupac.sifra ) window.current_bonus_kupac.sifra = `bonus`+cit_rand();

    
    refresh_simple_cal( $('#'+rand_id+`_bonus_date_start_kupac`) );
    refresh_simple_cal( $('#'+rand_id+`_bonus_date_end_kupac`) );
    
    
    
    var amount_min = cit_number($('#'+rand_id+`_bonus_amount_min_kupac`).val());
    window.current_bonus_kupac.amount_min = $.isNumeric(amount_min) ? amount_min : null;
    
    var amount_max = cit_number($('#'+rand_id+`_bonus_amount_max_kupac`).val());
    window.current_bonus_kupac.amount_max = $.isNumeric(amount_max) ? amount_max : null;
    
    
    var perc = cit_number($('#'+rand_id+`_bonus_perc_kupac`).val());
    window.current_bonus_kupac.perc = $.isNumeric(perc) ? perc : null;
    

    if ( 
      ( !window.current_bonus_kupac.amount_min && window.current_bonus_kupac.amount_min !== 0 ) 
      ||
      !window.current_bonus_kupac.amount_max
      ||
      !window.current_bonus_kupac.perc
    ) {
      popup_warn(`Potrebno je upisati broj sve podatke !!!!`);
      return;
    };
    
    
    
    data.end_year_bonus_kupac = upsert_item(data.end_year_bonus_kupac, window.current_bonus_kupac, `sifra`);
    
    data.end_year_bonus_kupac_flat = JSON.stringify(data.end_year_bonus_kupac);
    
    this_module.make_bonus_kupac_list(data);
    
    window.current_bonus_kupac = null;
    
    
    $('#'+rand_id+`_bonus_amount_min_kupac`).val("");
    $('#'+rand_id+`_bonus_amount_max_kupac`).val("");
    $('#'+rand_id+`_bonus_perc_kupac`).val("");
    
    
    if ( this.id == `update_bonus_kupac` ) {
      $(this).css(`display`, `none`);
      $(`#save_bonus_kupac`).css(`display`, `flex`);
    };
    
  };
  this_module.save_current_bonus_kupac = save_current_bonus_kupac;
      
  // ---------------END--------------------- BONUS KUPAC ------------------------------------
  
  
  // ---------------START--------------------- BONUS DOBAV ------------------------------------
  
  function make_bonus_dobav_list(data) {
    
    cit_local_list.bonusi_table_dobav = (data.end_year_bonus_dobav && data.end_year_bonus_dobav.length > 0) ? data.end_year_bonus_dobav : [];
    
    var bonus_dobav_props = {
      desc: 'samo za kreiranje tablice svih godišnjih bonusa koje imamo kod nekog kupca',
      local: true,

      list: 'bonusi_table_dobav',

      show_cols: ['amount_min', "amount_max", 'date_from', 'date_to', 'perc', 'button_edit', 'button_delete' ],
      format_cols: { amount_min: 0, amount_max: 0, date_from: "date", date_to: "date", perc: 2 },
      col_widths: [3, 3, 2, 2, 2, 1, 1],
      
      parent: "#god_bonusi_box_dobav",
      return: {},
      show_on_click: false,

      cit_run: function(state) { console.log(state); },

      button_edit: function(e) { 

        e.stopPropagation(); 
        e.preventDefault();

        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var index = find_index(data.end_year_bonus_dobav, sifra , `sifra` );

        window.current_bonus_dobav = cit_deep( data.end_year_bonus_dobav[index] );


        $('#save_bonus_dobav').css(`display`, `none`);
        $('#update_bonus_dobav').css(`display`, `flex`);

        format_number_input(window.current_bonus_dobav.amount_min, $('#'+rand_id+`_bonus_amount_min_dobav`)[0], null);
        format_number_input(window.current_bonus_dobav.amount_max, $('#'+rand_id+`_bonus_amount_max_dobav`)[0], null);
        
        
        $('#'+rand_id+`_bonus_date_start_dobav`).val( cit_dt(window.current_bonus_dobav.date_from).date || "" );
        $('#'+rand_id+`_bonus_date_end_dobav`).val( cit_dt(window.current_bonus_dobav.date_to).date || "" );
        
    
        
        
        format_number_input(window.current_bonus_dobav.perc, $('#'+rand_id+`_bonus_perc_dobav`)[0], null);     

      },
      button_delete: async function(e) {


          e.stopPropagation(); 
          e.preventDefault();

          var this_button = this;

          function delete_yes() {

            var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
            var data = this_module.cit_data[this_comp_id];
            var rand_id =  this_module.cit_data[this_comp_id].rand_id;

            var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
            var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
            data.end_year_bonus_dobav = delete_item(data.end_year_bonus_dobav, sifra , `sifra` );

            $(`#`+parent_row_id).remove();

          };

          function delete_no() {
            show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj Bonus?`, null );
          };

          var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
          var show_popup_modal = pop_mod.show_popup_modal;
          show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj Bonus?`, null, 'yes/no', delete_yes, delete_no, null);


      },

    };
    // ako imam išta u tom arraju onda napravi tablicu !!!!
    if (cit_local_list.bonusi_table_dobav.length > 0 ) create_cit_result_list(cit_local_list.bonusi_table_dobav, bonus_dobav_props );
  };
  this_module.make_bonus_dobav_list = make_bonus_dobav_list;

  
  function update_bonus_date_start_dobav(state, this_input) {

    if ( !window.current_bonus_dobav ) window.current_bonus_dobav = {};

    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      window.current_bonus_dobav.date_from = null;
      console.log(`current_bonus_dobav.date_from: `, null );
      return;
    };

    window.current_bonus_dobav.date_from = state;
    console.log("current_bonus_dobav.date_from", new Date(state));

  };
  this_module.update_bonus_date_start_dobav = update_bonus_date_start_dobav;


  function update_bonus_date_end_dobav(state, this_input) {

    if ( !window.current_bonus_dobav ) window.current_bonus_dobav = {};

    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      window.current_bonus_dobav.date_to = null;
      console.log(`current_bonus_dobav.date_to: `, null );
      return;
    };

    window.current_bonus_dobav.date_to = state;
    console.log("current_bonus_dobav.date_to", new Date(state));

  };
  this_module.update_bonus_date_end_dobav = update_bonus_date_end_dobav;
    
  
  function save_current_bonus_dobav() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    if ( !window.current_bonus_dobav ) window.current_bonus_dobav = {};
    
    if ( !window.current_bonus_dobav.sifra ) window.current_bonus_dobav.sifra = `bonus`+cit_rand();
    
    
    refresh_simple_cal( $('#'+rand_id+`_bonus_date_start_dobav`) );
    refresh_simple_cal( $('#'+rand_id+`_bonus_date_end_dobav`) );
    
    
    var amount_min = cit_number($('#'+rand_id+`_bonus_amount_min_dobav`).val());
    window.current_bonus_dobav.amount_min = $.isNumeric(amount_min) ? amount_min : null;
    
    var amount_max = cit_number($('#'+rand_id+`_bonus_amount_max_dobav`).val());
    window.current_bonus_dobav.amount_max = $.isNumeric(amount_max) ? amount_max : null;
        
    var perc = cit_number($('#'+rand_id+`_bonus_perc_dobav`).val());
    window.current_bonus_dobav.perc = $.isNumeric(perc) ? perc : null;
    
    

    if ( 
      (!window.current_bonus_dobav.amount_min && window.current_bonus_dobav.amount_min !== 0) 
      ||
      !window.current_bonus_dobav.amount_max
      ||
      !window.current_bonus_dobav.perc
    ) {
      popup_warn(`Potrebno je upisati broj sve podatke !!!!`);
      return;
    };
    
 
    data.end_year_bonus_dobav = upsert_item(data.end_year_bonus_dobav, window.current_bonus_dobav, `sifra`);
    
    data.end_year_bonus_dobav_flat = JSON.stringify(data.end_year_bonus_dobav);
    
    this_module.make_bonus_dobav_list(data);
    
    window.current_bonus_dobav = null;
    
    
    $('#'+rand_id+`_bonus_amount_min_dobav`).val("");
    $('#'+rand_id+`_bonus_amount_max_dobav`).val("");
    $('#'+rand_id+`_bonus_perc_dobav`).val("");
    
    
    if ( this.id == `update_bonus_dobav` ) {
      $(this).css(`display`, `none`);
      $(`#save_bonus_dobav`).css(`display`, `flex`);
    };
    
  };
  this_module.save_current_bonus_dobav = save_current_bonus_dobav;

  // ---------------END--------------------- BONUS DOBAV ------------------------------------
  
  
  
  function save_current_ugovor() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    var comp_id = rand_id+`_ugovor_docs`;
    
    
    // vrati nazad da gumb za save dokumenata  bude vidljiv
    // ionako ću stakriti parent i cijelu listu obrisati
    $(`#${comp_id}_upload_btn`).css(`display`, `flex`);
    
    // obriši listu
    $(`#${comp_id}_list_items`).html( "" );
    // sakrij listu i button
    $(`#${comp_id}_list_box`).css( `display`, `none` );
    
    
    if ( !window.current_ugovor ) window.current_ugovor = {};
    
    if ( !window.current_ugovor.sifra ) window.current_ugovor.sifra = `ugovor`+cit_rand();
    // ovako updatam current ugovor date
    refresh_simple_cal( $('#'+rand_id+`_ugovor_date`) );
    
    refresh_simple_cal( $('#'+rand_id+`_ugovor_start_date`) );
    refresh_simple_cal( $('#'+rand_id+`_ugovor_end_date`) );
    
    
    window.current_ugovor.komentar = $('#'+rand_id+`_ugovor_komentar`).val();
    window.current_ugovor.naziv = $('#'+rand_id+`_ugovor_naziv`).val();
    
    if ( !window.current_ugovor.naziv || !window.current_ugovor.date ) {
      popup_warn(`Potrebno je upisati barem naziv i datum ugovora !!!!`);
      return;
    };
    
    if ( !window.current_ugovor.docs ) window.current_ugovor.docs = null;
    
    data.ugovori = upsert_item(data.ugovori, window.current_ugovor, `sifra`);
    
    data.ugovori_flat = JSON.stringify(data.ugovori);
    
    this_module.make_ugovori_list(data);
    
    window.current_ugovor = null;
    
    $('#'+rand_id+`_ugovor_date`).val("");
    $('#'+rand_id+`_ugovor_start_date`).val("");
    $('#'+rand_id+`_ugovor_end_date`).val("");
    
    
    $('#'+rand_id+`_ugovor_naziv`).val("");
    $('#'+rand_id+`_ugovor_komentar`).val("");
    
    
    if ( this.id == `update_ugovor` ) {
      $(this).css(`display`, `none`);
      $(`#save_ugovor`).css(`display`, `flex`);
    };
    
  };
  this_module.save_current_ugovor = save_current_ugovor;
  
  
  function choose_place(state) {
    
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  data.rand_id;
    
    
    
    if ( state == null ) {

      if ( window.current_adresa ) {
        
        $('#'+rand_id+`_naselje`).val("");
        $('#'+rand_id+`_mjesto`).val("");
        $('#'+rand_id+`_posta`).val("");
        
        window.current_adresa.naselje = null;
        window.current_adresa.mjesto = null;
        window.current_adresa.posta = null;
        
        console.log(`new place: `, null );  
      };
      
      return;
    };
    
    
    $('#'+rand_id+`_naselje`).val(state.naselje);
    $('#'+rand_id+`_mjesto`).val(state.mjesto);
    $('#'+rand_id+`_posta`).val(state.post_num);
    
    if ( !window.current_adresa ) window.current_adresa = {};
    
    window.current_adresa.naselje = state.naselje;
    window.current_adresa.mjesto = state.mjesto;
    window.current_adresa.posta = state.post_num;
    
    
  };
  this_module.choose_place = choose_place;
  
  function choose_country(state) {
    
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  data.rand_id;
    
    
    if ( state == null ) {

      if ( window.current_adresa ) {
        $('#'+rand_id+`_drzava`).val("");
        window.current_adresa.drzava = null;
        console.log(`new drzava: `, null );  
      };
      return;
    };
    
    $('#'+rand_id+`_drzava`).val(state.naziv);
    if ( !window.current_adresa ) window.current_adresa = {};
    window.current_adresa.drzava = state;
    
  };
  this_module.choose_country = choose_country;
  
  
  function make_adrese_list(data) {
    
    // ADRESE TABLICA
    cit_local_list.adrese_za_table = [];
    var adrese_za_table = [];
    
    if ( data.adrese && data.adrese.length > 0 ) {
      
      $.each(data.adrese, function(index, adresa_obj ) {
        
        var { sifra, adresa, naziv_adrese, kvart, naselje, posta, mjesto  } = adresa_obj;
        var vrsta_adrese = adresa_obj.vrsta_adrese?.naziv || null ;
        var drzava = adresa_obj.drzava?.naziv || null;
        
        var map = "&nbsp;";
        if ( adresa_obj.adresa_coords ) {
          map = 
          `<a class="cit_search_result_map_link" 
              href="http://www.google.com/maps/place/${ adresa_obj.adresa_coords.replace(/ /g, '') }" 
              target="_blank" >
               <i class="fas fa-map-marker-alt"></i>
          </a>
          `
        };
        
        adrese_za_table.push({ sifra, adresa, map, vrsta_adrese, naziv_adrese, kvart, naselje, posta, mjesto, drzava });
        
      }); // kraj loopa svih adresa
      
      cit_local_list.adrese_za_table =  adrese_za_table;
    };
    
    var adrese_props = {
      
      desc: 'samo za kreiranje tablice svih adresa u partneru',
      local: true,
      
      list: 'adrese_za_table',
      
      show_cols: ['adresa', 'map', 'vrsta_adrese', 'naziv_adrese', 'kvart', 'naselje', 'posta', 'mjesto', 'drzava', 'button_edit', 'button_delete' ],
      col_widths: [4, 1, 2, 4, 2, 2, 2, 3, 2, 1, 1 ],
      parent: "#sve_adrese_box",
      return: {},
      show_on_click: false,
      cit_run: function(state) { console.log(state); },
      
      button_edit: function(e) { 
        
        console.log("kliknuo edit za adrese");
        
        e.stopPropagation(); 
        e.preventDefault(); 
        
        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var index = find_index(data.adrese, sifra , `sifra` );

        window.current_adresa = cit_deep( data.adrese[index] );
  
        
        $('#save_adresa').css(`display`, `none`);
        $('#update_adresa').css(`display`, `flex`);
        
        $('#'+rand_id+`_adresa`).val( window.current_adresa.adresa || "");
        $('#'+rand_id+`_naziv_adrese`).val(window.current_adresa.naziv_adrese || "");
        $('#'+rand_id+`_vrsta_adrese`).val(window.current_adresa.vrsta_adrese ? window.current_adresa.vrsta_adrese.naziv : "");
        $('#'+rand_id+`_kvart`).val(window.current_adresa.kvart || "");
        $('#'+rand_id+`_naselje`).val(window.current_adresa.naselje || "");
        $('#'+rand_id+`_mjesto`).val(window.current_adresa.mjesto || "");
        $('#'+rand_id+`_posta`).val(window.current_adresa.posta || "");
        $('#'+rand_id+`_drzava`).val(window.current_adresa.drzava ? window.current_adresa.drzava.naziv : "");
        $('#'+rand_id+`_adresa_coords`).val(window.current_adresa.adresa_coords || "");
        
      },
      button_delete: async function(e) {
        
        e.stopPropagation(); 
        e.preventDefault();
        console.log("kliknuo DELETE za adrese");
 
        var this_button = this;
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          data.adrese = delete_item(data.adrese, sifra , `sifra` );
          
          $(`#`+parent_row_id).remove();

        };
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovu adresu?`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovu adresu?`, null, 'yes/no', delete_yes, delete_no, null);
        
      },
      
    };
    if (cit_local_list.adrese_za_table.length > 0 ) create_cit_result_list(cit_local_list.adrese_za_table, adrese_props );
    
    
  };
  this_module.make_adrese_list = make_adrese_list;
  
  
  function make_kontakt_list(data, parent_box ) {
    
    var kontakti_za_table = [];
    
    if ( data.kontakti && data.kontakti.length > 0 ) {
      
      $.each(data.kontakti, function(index, kontakt_obj ) {

        var { sifra, kontakt, mail, mob, tel_1, tel_2, fax  } = kontakt_obj;

        var full_adresa = "";
        
        if ( kontakt_obj.adresa_kontakta ) {
          // stavljam ovo kao array tako da mogu koristiti donju funkciju koja prima data.adrese kao argument
          kontakt_obj.adrese = [ kontakt_obj.adresa_kontakta ];
          full_adresa = window.concat_full_adresa( kontakt_obj );
        };
        
        var pozicija_kontakta = kontakt_obj.pozicija_kontakta?.naziv || null;
        
        var work_time = "";
        $.each(kontakt_obj.work_time, function(key, time) {
          work_time += key.toUpperCase() + ": " + time + "<br>";
        });
        
        // obriši zadnji <br>
        work_time = work_time.slice(0, -4);
        work_time = `<span style="font-size: 11px; font-weight: 700;">${work_time}</span>`

        kontakti_za_table.push({ 
          sifra,
          kontakt,
          full_adresa,
          pozicija_kontakta,
          mail,
          mob,
          tel_1,
          tel_2,
          fax,
          work_time,
          adresa_kontakta : kontakt_obj.adresa_kontakta 
        });

      });
      
    };
    
    var kont_props = {
      desc: 'samo za kreiranje tablice svih kontakata  za  ' + data.id,
      local: true,
      list: kontakti_za_table,
      show_cols: [ 
        'kontakt',
        'pozicija_kontakta',
        'mail',
        'mob',
        'tel_1',
        'tel_2',
        'fax',
        'full_adresa',
        'work_time',
        'button_edit',
        'button_delete'
      ],
      col_widths: [ 
        4,
        3,
        4,
        3,
        3,
        3,
        3,
        3,
        2.7,
        1,
        1
      ],
      parent: (parent_box || "#svi_kontakti_box"),
      return: {},
      show_on_click: false,
      cit_run: function(state) { console.log(state); },

      button_edit: function(e) { 
      

        e.stopPropagation(); 
        e.preventDefault(); 
        
        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var index = find_index(data.kontakti, sifra , `sifra` );

        window.current_kontakt = cit_deep( data.kontakti[index] );
  
        
        $('#save_kontakt').css(`display`, `none`);
        $('#update_kontakt').css(`display`, `flex`);

        $('#'+rand_id+`_kontakt`).val( window.current_kontakt.kontakt || "");

        $('#'+rand_id+`_adresa_kontakta`).val(window.current_kontakt.adresa_kontakta ? window.current_kontakt.adresa_kontakta.adresa : "");
        $('#'+rand_id+`_pozicija_kontakta`).val(window.current_kontakt.pozicija_kontakta ? window.current_kontakt.pozicija_kontakta.naziv : "");

        $('#'+rand_id+`_fax`).val( window.current_kontakt.fax || "");
        $('#'+rand_id+`_mail`).val( window.current_kontakt.mail || "");
        $('#'+rand_id+`_mob`).val( window.current_kontakt.mob || "");
        
        $('#'+rand_id+`_tel_1`).val(window.current_kontakt.tel_1 || "");
        $('#'+rand_id+`_tel_2`).val(window.current_kontakt.tel_2 || "");


        $('#'+rand_id+`_work_pon`).val(window.current_kontakt.work_time ? window.current_kontakt.work_time.pon : "");
        $('#'+rand_id+`_work_uto`).val(window.current_kontakt.work_time ? window.current_kontakt.work_time.uto : "");
        $('#'+rand_id+`_work_sri`).val(window.current_kontakt.work_time ? window.current_kontakt.work_time.sri : "");
        $('#'+rand_id+`_work_cet`).val(window.current_kontakt.work_time ? window.current_kontakt.work_time.cet : "");
        $('#'+rand_id+`_work_pet`).val(window.current_kontakt.work_time ? window.current_kontakt.work_time.pet : "");
        $('#'+rand_id+`_work_sub`).val(window.current_kontakt.work_time ? window.current_kontakt.work_time.sub : "");
        $('#'+rand_id+`_work_ned`).val(window.current_kontakt.work_time ? window.current_kontakt.work_time.ned : "");
        
      
      
      },
      button_delete: async function(e) {
        
        e.stopPropagation(); 
        e.preventDefault();
        
        
        var this_button = this;
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          data.kontakti = delete_item(data.kontakti, sifra , `sifra` );
          
          $(`#`+parent_row_id).remove();

        };
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj Kontakt?`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj Kontakt?`, null, 'yes/no', delete_yes, delete_no, null);
        
      },

    };
    
    if ( kontakti_za_table.length > 0 ) create_cit_result_list( kontakti_za_table, kont_props );
    
  };
  this_module.make_kontakt_list = make_kontakt_list;
  
  
  function save_current_adresa() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    
    if ( !window.current_adresa ) window.current_adresa = {};
    
    if ( !window.current_adresa.sifra ) window.current_adresa.sifra = `adres`+cit_rand();
    
    data.komentar_all_adrese = $('#'+rand_id+`_komentar_all_adrese`).val();
    
    window.current_adresa.adresa = $('#'+rand_id+`_adresa`).val();
    window.current_adresa.naziv_adrese = $('#'+rand_id+`_naziv_adrese`).val();
    window.current_adresa.kvart = $('#'+rand_id+`_kvart`).val();
    window.current_adresa.naselje = $('#'+rand_id+`_naselje`).val();
    window.current_adresa.mjesto = $('#'+rand_id+`_mjesto`).val();
    window.current_adresa.posta = $('#'+rand_id+`_posta`).val();
    window.current_adresa.adresa_coords = $('#'+rand_id+`_adresa_coords`).val();
    
    
    if ( !window.current_adresa.drzava ) {
      popup_warn(`Obavezno izabrati državu!`);
      return;
    };
    
    
    // provjeravaj sve
    if (
      window.current_adresa.drzava.naziv == `Croatia` 
      &&
      (
        !window.current_adresa.adresa  ||
        !window.current_adresa.naselje ||
        !window.current_adresa.mjesto  ||
        !window.current_adresa.posta
      )
    ) {
      popup_warn(`Potrebno upisati adresu, naselje, mjesto i poštanski broj za hrvatske tvrtke!`);
      return;
    };
  
  
    /* RADMILA TRAŽILA DA NAZIV ADRESE NE BUDE OBAVEZAN 2023_01_13
    if (
      !window.current_adresa.naziv_adrese 
      ||
      !window.current_adresa.vrsta_adrese
    ) {
      popup_warn(`Potrebno upisati vrstu i naziv adrese!`);
      return;
    };
    */
    
    
    if (
      !window.current_adresa.vrsta_adrese
    ) {
      popup_warn(`Potrebno upisati vrstu adrese!`);
      return;
    };
    
    
  
    // obriši listu odabranih proizvoda za ovu adresu
    $("#sve_adrese_box").html("");
    
    
    data.adrese = upsert_item(data.adrese, window.current_adresa, `sifra`);
    
    data.adrese_flat = JSON.stringify(data.adrese);
    
    
    // update tablicu
    this_module.make_adrese_list(data);
    
    window.current_adresa = null;

    $('#'+rand_id+`_adresa`).val("");
    $('#'+rand_id+`_naziv_adrese`).val("");
    $('#'+rand_id+`_vrsta_adrese`).val("");
    $('#'+rand_id+`_kvart`).val("");
    $('#'+rand_id+`_naselje`).val("");
    $('#'+rand_id+`_mjesto`).val("");
    $('#'+rand_id+`_posta`).val("");
    $('#'+rand_id+`_drzava`).val("");
    $('#'+rand_id+`_adresa_coords`).val("");

    
    this_module.make_adresa_kontakt_list(data);
    
    
    if ( this.id == `update_adresa` ) {
      $(this).css(`display`, `none`);
      $(`#save_adresa`).css(`display`, `flex`);
    };
    
  };
  this_module.save_current_adresa = save_current_adresa;

  
  
  function save_current_kontakt() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    
    if ( !window.current_kontakt ) window.current_kontakt = {};
    
    if ( !window.current_kontakt.sifra ) window.current_kontakt.sifra = `kont`+cit_rand();
    
    data.komentar_all_kont = $('#'+rand_id+`_komentar_all_kont`).val();
    
    // adresa i pozocija kontakte se upisuju prilikom odabira na drop listi
    // window.current_kontakt.adresa_kontakta
    // window.current_kontakt.pozicija_kontakta
    
    
    window.current_kontakt.kontakt = $('#'+rand_id+`_kontakt`).val();
    window.current_kontakt.fax = $('#'+rand_id+`_fax`).val();
    window.current_kontakt.mail = $('#'+rand_id+`_mail`).val();
    window.current_kontakt.mob = $('#'+rand_id+`_mob`).val();

    window.current_kontakt.tel_1 = $('#'+rand_id+`_tel_1`).val();
    window.current_kontakt.tel_2 = $('#'+rand_id+`_tel_2`).val();


    if ( !window.current_kontakt.work_time ) window.current_kontakt.work_time = {};
    
    window.current_kontakt.work_time.pon = $('#'+rand_id+`_work_pon`).val();
    window.current_kontakt.work_time.uto = $('#'+rand_id+`_work_uto`).val();
    window.current_kontakt.work_time.sri = $('#'+rand_id+`_work_sri`).val();
    window.current_kontakt.work_time.cet = $('#'+rand_id+`_work_cet`).val();
    window.current_kontakt.work_time.pet = $('#'+rand_id+`_work_pet`).val();
    window.current_kontakt.work_time.sub = $('#'+rand_id+`_work_sub`).val();
    window.current_kontakt.work_time.ned = $('#'+rand_id+`_work_ned`).val();
    
    
    if (
      !window.current_kontakt.kontakt
      
      ||
      
      (
        !window.current_kontakt.mail  &&
        !window.current_kontakt.mob   &&
        !window.current_kontakt.tel_1 &&
        !window.current_kontakt.tel_2
      )

    ) {
      popup_warn(`Potrebno je upisati naziv i barem jedan od medija za kontaktiranje !!!`);
      return;
    };
    
    
    
    data.kontakti = upsert_item(data.kontakti, window.current_kontakt, `sifra`);
    
    data.kontakti_flat = JSON.stringify(data.kontakti);
    
    
    // update tablicu
    this_module.make_kontakt_list(data);
    
    window.current_kontakt = null;


    $('#'+rand_id+`_kontakt`).val("");

    $('#'+rand_id+`_adresa_kontakta`).val("");
    $('#'+rand_id+`_pozicija_kontakta`).val("");

    $('#'+rand_id+`_fax`).val("");
    $('#'+rand_id+`_mail`).val("");
    $('#'+rand_id+`_mob`).val("");

    $('#'+rand_id+`_tel_1`).val("");
    $('#'+rand_id+`_tel_2`).val("");


    $('#'+rand_id+`_work_pon`).val("");
    $('#'+rand_id+`_work_uto`).val("");
    $('#'+rand_id+`_work_sri`).val("");
    $('#'+rand_id+`_work_cet`).val("");
    $('#'+rand_id+`_work_pet`).val("");
    $('#'+rand_id+`_work_sub`).val("");
    $('#'+rand_id+`_work_ned`).val("");

    
    if ( this.id == `update_kontakt` ) {
      $(this).css(`display`, `none`);
      $(`#save_kontakt`).css(`display`, `flex`);
    };
    
  };
  this_module.save_current_kontakt = save_current_kontakt;

    
  function make_adresa_kontakt_list(data) {
    
    var rand_id =  data.rand_id;
    
    // DROP LIST ZA ADRESE U KONTAKTU
    cit_local_list.all_current_adrese = (data.adrese && data.adrese.length > 0) ? data.adrese : [];
    
    
    $(`#`+rand_id+`_adresa_kontakta`).data('cit_props', {
      desc: 'za select adrese kada radim novi kontakt - tako da prikačim novi kontakt na tu adresu',
      local: true,
      show_cols: ["naziv_adrese", "vrsta_adrese_naziv", "adresa", "kvart", "naselje", "mjesto", "posta", "drzava_naziv"],
      col_widths: [2, 1, 3, 2, 2, 2, 1, 1],
      return: {},
      show_on_click: true,
      list: 'all_current_adrese',
      cit_run: this_module.choose_adresa_kontakta,
      
      filter: function(adrese) {
        
        var new_adrese = [];
        $.each(adrese, function(ind, adresa_obj) {
          
          var { sifra, adresa, kvart, naselje, mjesto, posta, drzava } = adresa_obj;
          var naziv_adrese =  adresa_obj.naziv_adrese || null;
          
         
          new_adrese.push({
            sifra,
            naziv_adrese, 
            vrsta_adrese: adresa_obj.vrsta_adrese,
            vrsta_adrese_naziv: adresa_obj.vrsta_adrese.naziv,
            adresa,
            kvart,
            naselje,
            mjesto,
            posta,
            drzava,
            drzava_naziv: drzava ? drzava.naziv : "",
          });
          
        });
        
        return new_adrese;
      },
      
    });  
    
    
  };
  this_module.make_adresa_kontakt_list = make_adresa_kontakt_list;

  
  function choose_adresa_kontakta(state) {
    
    console.log(state);
    
    // deep copy od state
    var state = cit_deep(state);
    
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    if ( !window.current_kontakt ) window.current_kontakt = {};
    
    if ( state == null ) {
      $('#'+current_input_id).val(``);
      window.current_kontakt.adresa_kontakta = null;
      return;
    };
    
    window.current_kontakt.adresa_kontakta = state;

    $('#'+current_input_id).val(state.adresa);
    
  };
  this_module.choose_adresa_kontakta = choose_adresa_kontakta; 
    
  
  
  function choose_vrsta_adrese(state) {
    
    console.log(state);
    
    // deep copy od state
    var state = cit_deep(state);
    
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    if ( !window.current_adresa ) window.current_adresa = {};
    
    if ( state == null ) {
      $('#'+current_input_id).val(``);
      window.current_adresa.vrsta_adrese = null;
      return;
    };
    
    
    window.current_adresa.vrsta_adrese = state;

    $('#'+current_input_id).val(state.naziv);
  
    
  };
  this_module.choose_vrsta_adrese = choose_vrsta_adrese

  
  
  function make_ugovori_list(data) {
    
    // UGOVORI
    cit_local_list.ugovori_za_table = [];
    var ugovori_za_table = [];
    if ( data.ugovori && data.ugovori.length > 0 ) {
      
      $.each(data.ugovori, function(index, ugovor_obj ) {
        
        var { sifra, naziv, start, end, date, komentar } = ugovor_obj;
        
        var all_docs = "";
        if ( ugovor_obj.docs && $.isArray(ugovor_obj.docs) ) {
          
          
          var criteria = [ '~time' ];
          multisort( ugovor_obj.docs, criteria );
          
          $.each(ugovor_obj.docs, function(doc_index, doc) {
            var doc_html = 
            `
            <div class="docs_row">
              <div class="docs_date">${ cit_dt(doc.time).date+" "+cit_dt(doc.time).time }</div>
              <div class="docs_link">${ doc.link }</div>
            </div>
            `;
            
            all_docs += doc_html
            
          }); // kraj loopa po docs
          
        }; // kraj ako ima docs
        
        ugovori_za_table.push({ sifra, naziv, start, end, date, komentar, docs: all_docs });
        
      }); // kraj loopa svih adresa
      
      cit_local_list.ugovori_za_table =  ugovori_za_table;
    };
    var ugovori_props = {
      
      desc: 'samo za kreiranje tablice svih ugovora u partneru',
      local: true,
      
      list: 'ugovori_za_table',
      
      show_cols: [ "naziv", "date", "start", "end", "komentar", "docs", 'button_edit', 'button_delete' ],
      format_cols: { date: "date", start: "date", end: "date" },
      col_widths: [3, 1, 1, 1, 5, 4, 1, 1 ],
      parent: "#ugovori_list_box",
      return: {},
      show_on_click: false,
      cit_run: function(state) { console.log(state); },
      
      button_edit: function(e) { 
        
        e.stopPropagation(); 
        e.preventDefault();
        
        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var ugovor_sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var ugovor_index = find_index(data.ugovori, ugovor_sifra , `sifra` );

        window.current_ugovor = cit_deep( data.ugovori[ugovor_index] );
  
        
        $('#save_ugovor').css(`display`, `none`);
        $('#update_ugovor').css(`display`, `flex`);
        
        $('#'+rand_id+`_ugovor_date`).val( cit_dt(window.current_ugovor.date).date || "");
        $('#'+rand_id+`_ugovor_start_date`).val( cit_dt(window.current_ugovor.start).date || "" );
        $('#'+rand_id+`_ugovor_end_date`).val( cit_dt(window.current_ugovor.end).date || "" );
        
        $('#'+rand_id+`_ugovor_naziv`).val(window.current_ugovor.naziv || "");
        $('#'+rand_id+`_ugovor_komentar`).val(window.current_ugovor.komentar || "");
        
        
        console.log("kliknuo edit za ugovor");
        return;
        
      },
      button_delete: async function(e) {
        
        e.stopPropagation();
        e.preventDefault();
        var this_button = this;
        
        console.log("kliknuo DELETE za ugovor");
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var ugovor_sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          data.ugovori = delete_item(data.ugovori, ugovor_sifra , `sifra` );
          
          $(`#`+parent_row_id).remove();
  
        };
        
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj ugovor?`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj ugovor?`, null, 'yes/no', delete_yes, delete_no, null);
        
        
      },
      
    };
    if (cit_local_list.ugovori_za_table.length > 0 ) create_cit_result_list(cit_local_list.ugovori_za_table, ugovori_props );
      
    
  };
  this_module.make_ugovori_list = make_ugovori_list;
  
  
  function save_current_tender() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    var comp_id = rand_id+`_tender_docs`;
    
    
    // vrati nazad da gumb za save dokumenata  bude vidljiv
    // ionako ću stakriti parent i cijelu listu obrisati
    $(`#${comp_id}_upload_btn`).css(`display`, `flex`);
    
    // obriši listu
    $(`#${comp_id}_list_items`).html( "" );
    // sakrij listu i button
    $(`#${comp_id}_list_box`).css( `display`, `none` );
    
    
    if ( !window.current_tender ) window.current_tender = {};
    
    if ( !window.current_tender.sifra ) window.current_tender.sifra = `tender`+cit_rand();
    // ovako updatam current tender datume
    
    refresh_simple_cal( $('#'+rand_id+`_tender_date`) );
    refresh_simple_cal( $('#'+rand_id+`_tender_rok_prijave`) );
    refresh_simple_cal( $('#'+rand_id+`_tender_start_date`) );
    refresh_simple_cal( $('#'+rand_id+`_tender_end_date`) );
    
    window.current_tender.komentar = $('#'+rand_id+`_tender_komentar`).val();
    window.current_tender.naziv = $('#'+rand_id+`_tender_naziv`).val();
    
    if ( !window.current_tender.naziv || !window.current_tender.status ) {
      popup_warn(`Potrebno je upisati barem naziv i status tendera !!!!`);
      return;
    };
    
    // obriši listu odabranih proizvoda za ovaj tender
    $("#current_tender_products_box").html("");
    
    cit_local_list.current_tender_products = [];
    
    
    if ( !window.current_tender.docs ) window.current_tender.docs = null;
    if ( !window.current_tender.products ) window.current_tender.products = null;
    
    data.tenders = upsert_item(data.tenders, window.current_tender, `sifra`);
    
    data.tenders_flat = JSON.stringify(data.tenders);
    
    // update tablicu
    this_module.make_tenders_list(data);
    
    window.current_tender = null;
    
    
    $('#'+rand_id+`_tender_naziv`).val("");
    $('#'+rand_id+`_tender_komentar`).val("");
    
    $('#'+rand_id+`_tender_status`).val("");
    
    
    $('#'+rand_id+`_tender_date`).val("");
    $('#'+rand_id+`_tender_rok_prijave`).val("");
    $('#'+rand_id+`_tender_start_date`).val("");
    $('#'+rand_id+`_tender_end_date`).val("");
    
    
    
    if ( this.id == `update_tender` ) {
      $(this).css(`display`, `none`);
      $(`#save_tender`).css(`display`, `flex`);
    };
    
  };
  this_module.save_current_tender = save_current_tender;
  
 
  function make_tenders_list(data) {
    
    // UGOVORI
    cit_local_list.tenders_za_table = [];
    var tenders_za_table = [];
    if ( data.tenders && data.tenders.length > 0 ) {
      
      $.each(data.tenders, function(index, tender_obj ) {

        var { sifra, naziv, rok, start, end, komentar, date } = tender_obj;
        
        // TENDER DOCS
        var all_docs = "";
        if ( tender_obj.docs && $.isArray(tender_obj.docs) ) {
          
          var criteria = [ '~time' ];
          multisort( tender_obj.docs, criteria );
          
          $.each(tender_obj.docs, function(doc_index, doc) {
            var doc_html = 
            `
            <div class="docs_row">
              <div class="docs_date">${ cit_dt(doc.time).date+" "+cit_dt(doc.time).time }</div>
              <div class="docs_link">${ doc.link }</div>
            </div>
            `;
            
            all_docs += doc_html
            
          }); // kraj loopa po docs
          
        }; // kraj ako ima docs
        
        // TENDER PRODUCTS
        
        var all_products = "";
        if ( tender_obj.products && $.isArray(tender_obj.products) ) {
          
          var criteria = [ '~start' ];
          multisort( tender_obj.products, criteria );
          
          $.each(tender_obj.products, function(prod_index, product) {
            var product_html = 
`
<div class="docs_row">
  <div class="docs_cell" style="width: 30%;" >${ cit_dt(product.start).date }</div>  
  <div class="docs_cell" style="width: 70%;" >
    <a href="#project/${product.proj_sifra}/item/${product.item_sifra}/variant/${product.variant}" target="_blank" onClick="event.stopPropagation();">${product.full_product_name}</a>
  </div>
</div>
`;
            
            all_products += product_html
            
          }); // kraj loopa po docs
          
        }; // kraj ako ima docs
        
        
        tenders_za_table.push({
          sifra,
          naziv,
          date,
          rok,
          start,
          end,
          komentar,
          status: tender_obj.status.naziv,
          docs: all_docs,
          products: all_products,
        });
        
      }); // kraj loopa svih adresa
      
      cit_local_list.tenders_za_table =  tenders_za_table;
    };
    var tenders_props = {
      
      desc: 'samo za kreiranje tablice svih TENDERA u partneru',
      local: true,
      
      list: 'tenders_za_table',
      
      show_cols: [ "naziv", "date", "rok", "start", "end", "komentar", "status", "docs", "products", 'button_edit', 'button_delete' ],
      format_cols: { date: "date", rok: "date", start: "date", end: "date" },
      col_widths: [1,0.6,0.6,0.6,0.6,1,1,2,2,0.5,0.5 ],
      parent: "#tenders_box",
      return: {},
      show_on_click: false,
      cit_run: function(state) { console.log(state); },
      
      button_edit: function(e) { 
        
        e.stopPropagation(); 
        e.preventDefault();
        
        console.log("kliknuo edit za TENDER");
        
        
        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var tender_sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var tender_index = find_index(data.tenders, tender_sifra , `sifra` );

        window.current_tender = cit_deep( data.tenders[tender_index] );
        
        cit_local_list.current_tender_products = window.current_tender.products || [];
        
        var props = $(`#`+rand_id+`_tender_products`).data('cit_props');
        props.multi_list = cit_local_list.current_tender_products;
        $(`#`+rand_id+`_tender_products`).data('cit_props', props);
        
        create_multi_list( rand_id+`_tender_products`, props );
        
        wait_for(`$('#${rand_id}_tender_products_multi_list').length > 0`, function() {
          this_module.register_tender_products_events(rand_id+`_tender_products`);
        }, 50*1000);           
        
        
        $('#save_tender').css(`display`, `none`);
        $('#update_tender').css(`display`, `flex`);
        
        
        $('#'+rand_id+`_tender_naziv`).val(window.current_tender.naziv || "");
        $('#'+rand_id+`_tender_komentar`).val(window.current_tender.komentar || "");

        $('#'+rand_id+`_tender_status`).val(window.current_tender.status ? window.current_tender.status.naziv : "");

        
        $('#'+rand_id+`_tender_date`).val( cit_dt(window.current_tender.date).date || "");
        $('#'+rand_id+`_tender_rok_prijave`).val( cit_dt(window.current_tender.rok).date || "" );
        $('#'+rand_id+`_tender_start_date`).val( cit_dt(window.current_tender.start).date || "" );
        $('#'+rand_id+`_tender_end_date`).val( cit_dt(window.current_tender.end).date || "" );
 
        
      },
      
      button_delete: async function(e) {
        
        e.stopPropagation();
        e.preventDefault();
        
        var this_button = this;
        
        console.log("kliknuo DELETE za TENDER");
        
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var tender_sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          data.tenders = delete_item(data.tenders, tender_sifra , `sifra` );
          
          $(`#`+parent_row_id).remove();
        };
        
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj Tender?`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj Tender?`, null, 'yes/no', delete_yes, delete_no, null);
        
        
      },
      
    };
    if (cit_local_list.tenders_za_table.length > 0 ) create_cit_result_list(cit_local_list.tenders_za_table, tenders_props );
      
    
  };
  this_module.make_tenders_list = make_tenders_list;
  

  function update_tender_docs(files, button, arg_time) {

      var new_docs_arr = [];
      $.each(files, function(f_index, file) {

        var file_obj = {
          sifra: `doc`+cit_rand(),
          time: arg_time,
          link: `<a href="/docs/${time_path(arg_time)}/${file.filename}" target="_blank" onClick="event.stopPropagation();" >${ file.originalname }</a>`,
        };
        new_docs_arr.push( file_obj );
      });

      console.log( new_docs_arr );

      // napravi novi objekt ako current ne postoji
      if ( !window.current_tender ) window.current_tender = {};


      if ( $.isArray(window.current_tender.docs) ) {
        // ako postoji docs array onda dodaj nove filove
        window.current_tender.docs = [ ...window.current_tender.docs, ...new_docs_arr ];
      } else {
        // ako ne postoji onda stavi ove nove filove
        window.current_tender.docs = new_docs_arr;
      };    


    };
  this_module.update_tender_docs = update_tender_docs;

  
  
  function choose_tender_product(state) {
    
    console.log(state);
    
    // deep copy od state
    var state = cit_deep(state);
    
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    //var data = this_module.cit_data[this_comp_id];
    
    if ( state == null ) {
      $('#'+current_input_id).val(``);
      return;
    };
    
    // ne trebam stavljati naziv u input jer je to ionako multi select
    // $('#'+current_input_id).val(state.naziv);

    if ( !window.current_tender ) window.current_tender = {};
    if ( !window.current_tender.products) window.current_tender.products = [];
    
    console.log( JSON.stringify(window.current_tender.products)  );
    window.current_tender.products = upsert_item(window.current_tender.products, state, 'sifra');
    console.log( JSON.stringify(window.current_tender.products)  );
    
    // update multi list in props
    $('#'+current_input_id).data(`cit_props`).multi_list = window.current_tender.products;
    
    var props = $('#'+current_input_id).data(`cit_props`);

    create_multi_list(current_input_id, props);
    
    
    wait_for(`$('#${current_input_id}_multi_list').length > 0`, function() {
      this_module.register_tender_products_events(current_input_id);
    }, 5*1000);
    
    
  };
  this_module.choose_tender_product = choose_tender_product;
  
    
  function register_tender_products_events(input_id) {


    $(`#${input_id}_multi_list .multi_cell_delete_btn`).off(`click`);
    $(`#${input_id}_multi_list .multi_cell_delete_btn`).on(`click`, function() {

      if ( !window.current_tender ) {
        console.log(`NEMA CURRENT TENDER OBJEKTA`);
        return;
      };
      
      var this_button = this;
      
      var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var parent_row_id = $(this_button).closest('.multi_row')[0].id;
      var product_sifra = parent_row_id.replace(`multi_row_`, ``);
      window.current_tender.products = delete_item(window.current_tender.products, product_sifra , `sifra` );

      $(`#`+parent_row_id).remove();
      
    });
    
  };
  this_module.register_tender_products_events = register_tender_products_events;

  
  function make_debt_kupci_list(data) {
    // ZADUŽNICE KUPCI
    cit_local_list.debt_kupci_for_table = (data.zaduznice_kupac && data.zaduznice_kupac.length > 0) ? data.zaduznice_kupac : [];
    var debt_kupci_props = {
      desc: 'za kreiranje tablice svih zadužnica od kupca',
      local: true,

      list: 'debt_kupci_for_table',

      show_cols: ['date_from', "date_to", "amount", 'button_edit', 'button_delete' ],
      // ako postoji property format_cols
      // to znači da taj property treba foramtirati kao broj s tim brojem decimala
      format_cols: { date_from: "date", date_to: "date", amount: 2 },
      col_widths: [2, 2, 2, 1, 1],
      parent: "#debt_kupac_box",
      return: {},
      show_on_click: false,

      cit_run: function(state) { console.log(state); },

      button_edit: function(e) { 
      
      
        e.stopPropagation(); 
        e.preventDefault();

        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var index = find_index(data.zaduznice_kupac, sifra , `sifra` );

        window.current_debt_kupac = cit_deep( data.zaduznice_kupac[index] );

        
        $('#save_debt_kupac').css(`display`, `none`);
        $('#update_debt_kupac').css(`display`, `flex`);

        $('#'+rand_id+`_zaduznica_from_date_kupac`).val( cit_dt(window.current_debt_kupac.date_from).date || "");
        $('#'+rand_id+`_zaduznica_to_date_kupac`).val( cit_dt(window.current_debt_kupac.date_to).date || "");
        
        format_number_input(window.current_debt_kupac.amount, $('#'+rand_id+`_zaduznica_amount_kupac`)[0], null);
        
      },
      button_delete: async function(e) { 
        
        e.stopPropagation(); 
        e.preventDefault();
        
        var this_button = this;
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          data.zaduznice_kupac = delete_item(data.zaduznice_kupac, sifra , `sifra` );
          
          $(`#`+parent_row_id).remove();

        };
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovu zadužnicu?`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovu zadužnicu?`, null, 'yes/no', delete_yes, delete_no, null);
        
        
      },

    };
    if (cit_local_list.debt_kupci_for_table.length > 0 ) create_cit_result_list(cit_local_list.debt_kupci_for_table, debt_kupci_props );
  };
  this_module.make_debt_kupci_list = make_debt_kupci_list;

  
  
  
  
  
  
  
  function save_current_debt_kupac() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    if ( !window.current_debt_kupac ) window.current_debt_kupac = {};
    
    if ( !window.current_debt_kupac.sifra ) window.current_debt_kupac.sifra = `debt`+cit_rand();
    // ovako updatam current  date
    refresh_simple_cal( $('#'+rand_id+`_zaduznica_from_date_kupac`) );
    refresh_simple_cal( $('#'+rand_id+`_zaduznica_to_date_kupac`) );
    

    if ( !window.current_debt_kupac.amount || !window.current_debt_kupac.date_from ) {
      popup_warn(`Potrebno je upisati barem iznos i početni datum  !!!!`);
      return;
    };
 
    data.zaduznice_kupac = upsert_item(data.zaduznice_kupac, window.current_debt_kupac, `sifra`);
    
    data.zaduznice_kupac_flat = JSON.stringify(data.zaduznice_kupac);
    
    this_module.make_debt_kupci_list(data);
    
    window.current_debt_kupac = null;
    
    $('#'+rand_id+`_zaduznica_from_date_kupac`).val("");        
    $('#'+rand_id+`_zaduznica_to_date_kupac`).val(""); 
    $('#'+rand_id+`_zaduznica_amount_kupac`).val(""); 
    
    
    if ( this.id == `update_debt_kupac` ) {
      $(this).css(`display`, `none`);
      $(`#save_debt_kupac`).css(`display`, `flex`);
    };
    
  };
  this_module.save_current_debt_kupac = save_current_debt_kupac;
  
  
  function make_debt_dobav_list(data) {
    // ZADUŽNICE DOBAVLJAČI
    cit_local_list.debt_dobav_for_table = (data.zaduznice_dobav?.length > 0) ? data.zaduznice_dobav : [];
                                           
    if(window){}; // samo da popravim farbanje koda jer imam brackets koji ne prepoznaje optional chaining

    var debt_dobav_props = {
      desc: 'za kreiranje tablice svih zadužnica za dobavljča od nas',
      local: true,

      list: 'debt_dobav_for_table',

      show_cols: ['date_from', "date_to", "amount", 'button_edit', 'button_delete' ],
      // ako postoji property format_cols
      // to znači da taj property treba foramtirati kao broj s tim brojem decimala
      format_cols: { date_from: "date", date_to: "date", amount: 2 },
      col_widths: [2, 2, 2, 1, 1],
      parent: "#debt_dobav_box",
      return: {},
      show_on_click: false,

      cit_run: function(state) { console.log(state); },

      button_edit: function(e) { 
      
      
        e.stopPropagation(); 
        e.preventDefault();

        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var index = find_index(data.zaduznice_dobav, sifra , `sifra` );

        window.current_debt_dobav = cit_deep( data.zaduznice_dobav[index] );

        
        $('#save_debt_dobav').css(`display`, `none`);
        $('#update_debt_dobav').css(`display`, `flex`);

        $('#'+rand_id+`_zaduznica_from_date_dobav`).val( cit_dt(window.current_debt_dobav.date_from).date || "");        
        $('#'+rand_id+`_zaduznica_to_date_dobav`).val( cit_dt(window.current_debt_dobav.date_to).date || ""); 
        format_number_input( window.current_debt_dobav.amount, $('#'+rand_id+`_zaduznica_amount_dobav`)[0], null);
        
      
      },
      button_delete: async function(e) { 
        
        e.stopPropagation(); 
        e.preventDefault();
        
        var this_button = this;
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          data.zaduznice_dobav = delete_item(data.zaduznice_dobav, sifra , `sifra` );
          
          $(`#`+parent_row_id).remove();

        };
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovu zadužnicu?`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovu zadužnicu?`, null, 'yes/no', delete_yes, delete_no, null);
        
        
      },
    };
  
    if (cit_local_list.debt_dobav_for_table.length > 0 ) create_cit_result_list(cit_local_list.debt_dobav_for_table, debt_dobav_props );
  };
  this_module.make_debt_dobav_list = make_debt_dobav_list;


  function save_current_debt_dobav() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    if ( !window.current_debt_dobav ) window.current_debt_dobav = {};
    
    if ( !window.current_debt_dobav.sifra ) window.current_debt_dobav.sifra = `debt`+cit_rand();
    // ovako updatam current  date
    refresh_simple_cal( $('#'+rand_id+`_zaduznica_from_date_dobav`) );
    refresh_simple_cal( $('#'+rand_id+`_zaduznica_to_date_dobav`) );
    

    if ( !window.current_debt_dobav.amount || !window.current_debt_dobav.date_from ) {
      popup_warn(`Potrebno je upisati barem iznos i početni datum  !!!!`);
      return;
    };
 
    data.zaduznice_dobav = upsert_item(data.zaduznice_dobav, window.current_debt_dobav, `sifra`);
    
    data.zaduznice_dobav_flat = JSON.stringify(data.zaduznice_dobav);
    
    this_module.make_debt_dobav_list(data);
    
    window.current_debt_dobav = null;
    
    $('#'+rand_id+`_zaduznica_from_date_dobav`).val("");        
    $('#'+rand_id+`_zaduznica_to_date_dobav`).val(""); 
    $('#'+rand_id+`_zaduznica_amount_dobav`).val(""); 
    
    
    if ( this.id == `update_debt_dobav` ) {
      $(this).css(`display`, `none`);
      $(`#save_debt_dobav`).css(`display`, `flex`);
    };
    
  };
  this_module.save_current_debt_dobav = save_current_debt_dobav;
  
   
  function make_grupacija_list(data) {
    
    var new_companies = [];
    
    
    if ( 
      data.grupacija 
      && 
      data.grupacija.companies 
      && 
      data.grupacija.companies.length > 0
    ) {
      
      $.each(data.grupacija.companies, function(index, comp) {
        var company_link = `<a style="text-align: center;" href="#partner/${comp._id}/status/null" target="_blank" onClick="event.stopPropagation();">${comp.name}</a>`;
        new_companies.push( { name: company_link } );
      });
      
    } else {
      
      
      $("#grupacija_box").html(``);
      return;
      
    };

    // SVE KOMPANIJE U GRUPACIJI
    cit_local_list.grupacija_for_table = new_companies.length > 0 ? new_companies : [];
    
    
    var companies_props = {
      desc: 'samo za kreiranje tablice svih firmi unutar trenutno izabrane grupacije u partneru',
      local: true,
      list: 'grupacija_for_table',
      show_cols: ['name'],
      col_widths: [1],
      parent: "#grupacija_box",
      return: {},
      show_on_click: false,
      cit_run: function(state) { console.log(state); },
  
    };
    if (cit_local_list.grupacija_for_table.length > 0 ) create_cit_result_list(cit_local_list.grupacija_for_table, companies_props );
    
  };
  this_module.make_grupacija_list = make_grupacija_list;
  
  
  
  function reduce_doc_status( status ) {
    
    
    if ( !status ) return null;
    
    var smaller_status = status;
    
    // ako je status null ili undefined onda samo vrati null 
    if ( !status ) return null;
    
    // reduciraj samo ako do sada nije reduciran !!!!
    // reduciraj samo ako do sada nije reduciran !!!!
    
    if ( !status.reduced ) {
    
      smaller_status = {

        _id: status._id || null,
        status_id: status.status_id || status._id || null,

        kupac_naziv: status.kupac_naziv || null,
        kupac_id: status.kupac_id || null,

        elem_parent: status.elem_parent || null,
        elem_parent_object: (status.elem_parent_object ? reduce_doc_status(status.elem_parent_object) : null),

        sifra: status.sifra || null,
        status: status.status || null,

        order_sirov_count: status.order_sirov_count || null,

        product_id: status.product_id || null,
        proj_sifra: status.proj_sifra || null,
        item_sifra: status.item_sifra || null,
        variant: status.variant || null,

        full_product_name: status.full_product_name || null,
        link : status.link || null,
        status_tip_naziv : status.status_tip?.naziv || status.status_tip_naziv || null,

        reduced: true,

      };
      
    };

    return smaller_status;
    
  };
  this_module.reduce_doc_status = reduce_doc_status;
  
  
  function reduce_doc_sirovs( arg_sirovs ) {
    
    var smaller_sirovs = [];
    
    
    if (  !arg_sirovs ) return null;
    
    $.each( arg_sirovs, function(sir_ind, sirov) {

      var smaller_statuses = [];

      $.each( sirov.doc_statuses, function(stat_ind, status) {
        
        var small_status = reduce_doc_status(status);
        smaller_statuses.push(small_status);

      });
      
      var list_kupaca_ids = [];
      
      if ( sirov.list_kupaca && sirov.list_kupaca?.length > 0 ) {
      
        $.each( sirov.list_kupaca, function(k_ind, kupac) {
          if (kupac._id) list_kupaca_ids.push( kupac._id );
          
          // ako je ovaj array već stripan i sastoji se samo od _id -jeva ------> koji su string
          if ( typeof kupac == `string` ) list_kupaca_ids.push( kupac );
        });
      };
      

      smaller_sirovs.push({
        
        /* AKO SE RADI O IN ROBI TJ ULAZNOJ SIROVINI  !!!!! */
        
        in_roba : sirov.in_roba || null,
        
        "_id" : sirov._id || null,
        "sirovina_sifra" : sirov.sirovina_sifra || null,
        "full_naziv" : sirov.full_naziv || null,
        "cijena" : sirov.cijena || null,
        "alt_cijena" : sirov.alt_cijena || null,
        "doc_kolicine" : sirov.doc_kolicine || null,
        "doc_statuses" : smaller_statuses || null,
        
        list_kupaca: list_kupaca_ids,
        
        /* AKO SE RADI O OUT ROBI TJ IZLAZNOM PROZIVODU !!!!! */
        
        out_roba : sirov.out_roba || null,
        naziv: sirov.naziv || null,
        product_id: sirov.product_id || null,
        proj_sifra : sirov.proj_sifra || null,
        item_sifra : sirov.item_sifra || null,
        variant : sirov.variant || null,
        
        

        /* offer_dobav_price: sirov.offer_dobav_price || null, */

      });


    });


    return smaller_sirovs;
    
  };
  this_module.reduce_doc_sirovs = reduce_doc_sirovs;
  
  function save_partner() {
    
    
    
    $(`#save_partner_btn`).css("pointer-events", "none");
    toggle_global_progress_bar(true);
    
    
    
    
    var this_comp_id = $('#save_partner_btn').closest('.cit_comp')[0].id; // useo sam ovaj gumb bezveze  - nije bitno koji id
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    
    $.each( this_module.cit_data[this_comp_id], function( key, value ) {

      // ako je objekt ili array onda ga flataj
      if ( $.isPlainObject(value) || $.isArray(value) ) {
        this_module.cit_data[this_comp_id][key+'_flat'] = JSON.stringify(value);
      };
    });
    
    
    if ( !this_module.cit_data[this_comp_id].oib && !this_module.cit_data[this_comp_id].vat_num ) {
      $(`#save_partner_btn`).css("pointer-events", "all");
      toggle_global_progress_bar(false);
      popup_warn(`Morate upisati ili OIB ili VAT broj.<br><br>Može biti oboje, ali minimalno mora biti upisan jedan od tih brojeva !!!`);
      return;
    };
    
    this_module.cit_data[this_comp_id] = save_metadata( this_module.cit_data[this_comp_id] );
    
    var required_ok = cit_required( this_module.valid, this_module.cit_data[this_comp_id] );
    
    if ( required_ok.counter > 0 ) {
      $(`#save_partner_btn`).css("pointer-events", "all");
      toggle_global_progress_bar(false);
      popup_warn(`Niste upisali ove obavezne podatke:<br>${required_ok.podaci}`);
      return;
    };

    // od sada radim s kopijom data
    // od sada radim s kopijom data
    // od sada radim s kopijom data
    
    var data_copy = cit_deep( this_module.cit_data[this_comp_id] );
    
    // od sada radim s kopijom data
    // od sada radim s kopijom data
    // od sada radim s kopijom data
    
    // ------------- START --------------- PRETVORI SVE STATUS OBJEKTE U SAMO _id array ----------------------------
    
    
    
    
    
    // ako nemam docs_flat napravi da bude prazan array ali string !!!
    if ( !data_copy.docs_flat ) data_copy.docs_flat = `[]`;
    
    
    if ( data_copy.docs?.length > 0 ) {
      
      // pretvori docs flat nazad u array !!!
      data_copy.docs_flat = JSON.parse(data_copy.docs_flat);
      
      $.each( data_copy.docs, function(d_ind, doc) {
        
        // U DOCS FLAT UZIMAM SAMO SIFRE DOKUMENATA !!!!
        data_copy.docs_flat = upsert_item( data_copy.docs_flat, doc.doc_sifra, null ); // nema treći arg jer je to array stringova
        
        if ( data_copy.docs[d_ind]?.doc_sirovs?.length > 0 ) data_copy.docs[d_ind].doc_sirovs = this_module.reduce_doc_sirovs( cit_deep(doc.doc_sirovs) );
        // uzmi samo neke propse (NE SVE JER IH IMA JAKO PUNO !!!!)
        if ( doc.sirov_status ) {
          doc.sirov_status = this_module.reduce_doc_status( cit_deep(doc.sirov_status) );
        };
        
        if ( doc.kalk && doc.kalk.kalk_sifra ) {
          doc.kalk = doc.kalk.kalk_sifra;
        };
        
        if ( doc.pro_kalk ) {
          delete doc.pro_kalk;
        };
        
        // stavi da doc parent bude samo sifra
        if ( doc.doc_parent && doc.doc_parent.sifra ) {
          doc.doc_parent = doc.doc_parent.sifra;
        };
        
        
        
      }); // kraj loopa po docsima
      
      // vrati nazad da docs flat bude string !!!!
      data_copy.docs_flat = JSON.stringify( data_copy.docs_flat );
      
    };
    
    
    var status_ids = [];
    // obriši statuse jer ću id uzimati ručno !!!!!
    if ( data_copy.statuses?.length > 0 ) {
      $.each(data_copy.statuses, function( ind, status )  {
        status_ids.push(status._id);
      });
    };
    
    data_copy.statuses = status_ids;
    
    // if ( data_copy.grupacija ) data_copy.grupacija = data_copy.grupacija._id;

    
    // return;
    
    $.ajax({
      headers: {
        'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
      },
      type: "POST",
      cache: false,
      url: `/save_partner`,
      data: JSON.stringify( data_copy ),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done(function (result) {
      
      console.log(result);
      if ( result.success == true ) {

        this_module.cit_data[this_comp_id]._id = result.partner._id;
        this_module.cit_data[this_comp_id].partner_sifra = (result.partner.partner_sifra || null);
        var rand_id = this_module.cit_data[this_comp_id].rand_id;
        $(`#`+rand_id+`_partner_sifra`).val( this_module.cit_data[this_comp_id].partner_sifra );
        
        cit_toast(`PODACI SU SPREMLJENI!`);
        
        
        var hash_data = {
            partner: result.partner._id,
            status: null,
          };

        update_hash(hash_data);
        

      } else {
        if ( result.msg ) popup_error(result.msg);
        if ( !result.msg ) popup_error(`Greška prilikom spremanja!`);
      };

    })
    .fail(function (error) {
      
      console.log(error);
      popup_error(`Došlo je do greške na serveru prilikom spremanja!`);
    })
    .always(function() {
      
      toggle_global_progress_bar(false);
      $(`#save_partner_btn`).css("pointer-events", "all");
    });
    
  };
  this_module.save_partner = save_partner;
  
  
  
  
  
  function make_curr_placanje_kupac_list(data) {
    
    var curr_pay_kupac_props = {
      desc: 'samo za kreiranje curr placanje kupac list',
      local: true,
      
      list: data.curr_placanje_kupac,
      
      show_cols: ['perc', 'type_naziv', 'event_naziv', 'valuta', 'button_delete' ],
      col_widths: [
        1, // 'perc',
        2, // 'type_naziv',
        2, // 'event_naziv',
        1, // 'valuta',
        1, // 'button_delete'
      ],
      
       format_cols: { 
        perc: 2,
        type_naziv: "center",
        event_naziv: "center",
        valuta: "center",
      },
      parent: "#curr_placanje_kupac_box",
      return: {},
      show_on_click: false,
      
      cit_run: function(state) { console.log(state); },
     
      button_delete: async function(e) {
        
        e.stopPropagation();
        e.preventDefault();
        
      
        var this_button = this;
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          
          data.curr_placanje_kupac = delete_item(data.curr_placanje_kupac, sifra , `sifra` );
          
          $(`#`+parent_row_id).remove();
 

        };
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj zapis plaćanja?`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj zapis plaćanja?`, null, 'yes/no', delete_yes, delete_no, null);
         
        
      },
      
    };
    if ( data.curr_placanje_kupac?.length > 0 ) create_cit_result_list(data.curr_placanje_kupac, curr_pay_kupac_props );
    
  };
  this_module.make_curr_placanje_kupac_list = make_curr_placanje_kupac_list;
  
  function make_saved_placanja_kupac_list(data) {
    
    
    $.each(data.placanje_kupac, function(p_ind, pay) {
      
      var def_rows = ``;
      $.each(pay.def, function(def_ind, def) {
        
        def_rows += `
        <div class="def">
          <div style="width: 10%; text-align: right;">${ cit_format(def.perc, 2) }%</div>
          <div style="width: 35%; text-align: center;">${def.type_naziv}</div>
          <div style="width: 35%;  text-align: center;">${def.event_naziv}</div>
          <div style="width: 20%;  text-align: center;">VALUTA ${def.valuta} DANA</div>
        </div>
        `;
        
      });
      
      data.placanje_kupac[p_ind].def_rows = def_rows;
      
    });
    
    
    var saved_pay_kupac_props = {
      
      desc: 'samo za kreiranje SAVED placanje kupac list',
      local: true,
      
      list: data.placanje_kupac,
      
      show_cols: ['time', 'def_rows', 'button_delete' ],
      col_widths: [
        1.5, // 'time',
        10, // 'def',
        1, // 'button_delete'
      ],
      
       format_cols: { 
        time: "date_time",
      },
      parent: "#saved_placanje_kupac_box",
      return: {},
      show_on_click: false,
      
      cit_run: function(state) { console.log(state); },
     
      button_delete: async function(e) {
        
        e.stopPropagation();
        e.preventDefault();
        
      
        var this_button = this;
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          
          data.placanje_kupac = delete_item(data.placanje_kupac, sifra , `sifra` );
          
          $(`#`+parent_row_id).remove();

        };
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj zapis plaćanja?`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj zapis plaćanja?`, null, 'yes/no', delete_yes, delete_no, null);
         
        
      },
      
    };
    if ( data.placanje_kupac?.length > 0 ) create_cit_result_list(data.placanje_kupac, saved_pay_kupac_props );
    
  };
  this_module.make_saved_placanja_kupac_list = make_saved_placanja_kupac_list;
  

  function make_curr_placanje_dobav_list(data) {

    var curr_pay_dobav_props = {
      desc: 'samo za kreiranje curr placanje dobav list',
      local: true,

      list: data.curr_placanje_dobav,

      show_cols: ['perc', 'type_naziv', 'event_naziv', 'valuta', 'button_delete' ],
      col_widths: [
        1, // 'perc',
        2, // 'type_naziv',
        2, // 'event_naziv',
        1, // 'valuta',
        1, // 'button_delete'
      ],

       format_cols: { 
        perc: 2,
        type_naziv: "center",
        event_naziv: "center",
        valuta: "center",
      },
      parent: "#curr_placanje_dobav_box",
      return: {},
      show_on_click: false,

      cit_run: function(state) { console.log(state); },

      button_delete: async function(e) {

        e.stopPropagation();
        e.preventDefault();

        var this_button = this;

        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);

          data.curr_placanje_dobav = delete_item(data.curr_placanje_dobav, sifra , `sifra` );

          $(`#`+parent_row_id).remove();

        };

        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj zapis plaćanja?`, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj zapis plaćanja?`, null, 'yes/no', delete_yes, delete_no, null);


      },

    };
    if ( data.curr_placanje_dobav?.length > 0 ) create_cit_result_list(data.curr_placanje_dobav, curr_pay_dobav_props );

  };
  this_module.make_curr_placanje_dobav_list = make_curr_placanje_dobav_list;

  function make_saved_placanja_dobav_list(data) {


    $.each(data.placanje_dobav, function(p_ind, pay) {

      var def_rows = ``;
      $.each(pay.def, function(def_ind, def) {

        def_rows += `
        <div class="def">
          <div style="width: 10%; text-align: right;">${ cit_format(def.perc, 2) }%</div>
          <div style="width: 35%; text-align: center;">${def.type_naziv}</div>
          <div style="width: 35%;  text-align: center;">${def.event_naziv}</div>
          <div style="width: 20%;  text-align: center;">VALUTA ${def.valuta} DANA</div>
        </div>
        `;

      });

      data.placanje_dobav[p_ind].def_rows = def_rows;

    });


    var saved_pay_dobav_props = {
      desc: 'samo za kreiranje SAVED placanje dobav list',
      local: true,

      list: data.placanje_dobav,

      show_cols: ['time', 'def_rows', 'button_delete' ],
      col_widths: [
        1, // 'time',
        10, // 'def',
        1, // 'button_delete'
      ],

       format_cols: { 
        time: "date_time",
      },
      parent: "#saved_placanje_dobav_box",
      return: {},
      show_on_click: false,

      cit_run: function(state) { console.log(state); },

      button_delete: async function(e) {

        e.stopPropagation();
        e.preventDefault();


        var this_button = this;

        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);

          data.placanje_dobav = delete_item(data.placanje_dobav, sifra , `sifra` );

          $(`#`+parent_row_id).remove();

        };

        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj zapis plaćanja?`, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj zapis plaćanja?`, null, 'yes/no', delete_yes, delete_no, null);


      },

    };
    if ( data.placanje_dobav?.length > 0 ) create_cit_result_list(data.placanje_dobav, saved_pay_dobav_props );

  };
  this_module.make_saved_placanja_dobav_list = make_saved_placanja_dobav_list;

  
  
  $("#add_sirov_to_doc_btn").off("click");
  $("#add_sirov_to_doc_btn").on("click", function() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    if ( !window.current_partner_sirov ) {
      popup_warn(`Niste izabrali robu / sirovinu !!!`);
      return;
    };
    
    if ( !data.ask_kolicina_1 ) {
      popup_warn(`Upišite barem prvu količinu za generiranje dokumenta!`);
      return;
    };
    
    // uvijek resetiraj doc kolicine
    window.current_partner_sirov.doc_kolicine = [];
    
    for( ask=1; ask<=6; ask++ ) {
      
      var kol = data[`ask_kolicina_`+ask];
      
      kol = cit_number(kol);
      
      if ( data.custom_doc_price ) window.current_partner_sirov.custom_doc_price = data.custom_doc_price;
      
      if ( kol !== null ) {
        
        window.current_partner_sirov.doc_kolicine.push({
          
          sifra: "dockol"+cit_rand(),
          
          price: data.custom_doc_price || window.current_partner_sirov.cijena || null, // uzmi custom price ako je upisana ili originalnu cijenu sirovine
          count: kol,
          
          
          next_price: data.custom_doc_price || window.current_partner_sirov.cijena || null, // uzmi custom price ako je upisana ili originalnu cijenu sirovine
          next_count: kol,
          
          next_approved: false,
          next_doc: true, // automatski označi da je za OK za sljedeći dokument !!!!!!!!!!
          
        });
        
      };
      
    };
    
    
    // ako je user izabrao neke statuse povezane s dokumentom    
    if ( data.selected_statuses_in_sirov?.doc_statuses ) {
      // onda te statuse ubaci u current sirov koji je izabrao
      window.current_partner_sirov.doc_statuses = cit_deep( data.selected_statuses_in_sirov );
      // zatim obriši sve izabrane statuse za sljedeći put
      data.selected_statuses_in_sirov = null;
      $(`#selected_statuses_box`).html(``);
    };
    
    // ovo je array svih sirovina koje je user izabrao za dokument ----> ako nema arraya onda napravi prazan array
    if ( !data.partner_doc_sirovs ) data.partner_doc_sirovs = [];
    
    // ubaci trenutni sirov u array
    data.partner_doc_sirovs = upsert_item( data.partner_doc_sirovs, window.current_partner_sirov, `_id`);
    
    
    // resetiraj window sirov
    window.current_partner_sirov = null;
    $(`#`+rand_id+`_partner_sirov`).val(``);
    
    // resetiraj polje custom price
    data.custom_doc_price = null;
    $(`#`+rand_id+`_custom_doc_price`).val(``);
    
    this_module.make_doc_sirovs_list(data);
    
  });
  
  
  function make_doc_sirovs_list(data) {
    
    
    var partner_doc_sirovs_for_table = [];
    
    $.each( data.partner_doc_sirovs, function(s_ind, doc_sirov) {
      
      var doc_kolicine_html = ``;
      $.each( doc_sirov.doc_kolicine, function(s_ind, kol) {
        doc_kolicine_html += cit_format( kol.count, 3 ) + `<br>`;
      });
      
      // stavi sve to u container i align text u desno
      doc_kolicine_html = `<div style="text-align: right; width: 100%;">${doc_kolicine_html}</div>`
      
      partner_doc_sirovs_for_table.push({
        ...doc_sirov,
        doc_kolicine_html,
      });
      
    });

    
    
    
    $(`#doc_sirovs_list_box`).html(``);
    
    
    var doc_sirov_props = {
      // !!!findsirov!!!
      desc: 'za tablicu sirovina koje trebaju biti prikazane unutar dokumenta na PAGE-u PARTNERA ',
      local: true,
      
      list: partner_doc_sirovs_for_table,
      
      parent: `#doc_sirovs_list_box`,
      
      return: { },
      /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */
      col_widths:[
        1, // "sirovina_sifra",
        2, // "full_naziv",
        2, // "doc_kolicine_html",
        1, // grupa_naziv
        1, // "boja",
        1, // "cijena",
        1, // "kontra_tok",
        1, // "tok",
        1, // "po_valu",
        1, // "po_kontravalu",
        2, // "povezani_nazivi",
        
        1, // "pk_kolicina",
        1, // "nk_kolicina",
        1, // "order_kolicina",
        1, // "sklad_kolicina"
        1, // last_status
        
        4, // spec_docs
      
        0.7, // 'button_delete',
      ],
      show_cols: [
        "sifra_link",
        "full_naziv",
        "doc_kolicine_html",
        "grupa_naziv",
        "boja",
        "cijena",
        "kontra_tok",
        "tok",
        "po_valu",
        "po_kontravalu",
        "povezani_nazivi",
        
        `pk_kolicina`,
        `nk_kolicina`,
        `order_kolicina`,
        `sklad_kolicina`,
        `last_status`,
        
        `spec_docs`,
        
        'button_delete',
        
      ],
      custom_headers: [
        `ŠIFRA`,  // "sirovina_sifra",
        `NAZIV`,  // "full_naziv",
        `KOLIČINE`,  // "full_dobavljac",
        `GRUPA`,  // "grupa_naziv",
        `BOJA`,  // "boja",
        `CIJENA`,  // "cijena",
        `KONTRA TOK`,  // "kontra_tok",
        `TOK`,  // "tok",
        `VAL`,  // "po_valu",
        `KONTRA VAL`,  // "po_kontravalu",
        `POVEZANO`,  // "povezani_nazivi",
        
        `PK RESERV`,  // "pk_kolicina",
        `NK RESERV`,  // "nk_kolicina",
        `NARUČENO`,  // "order_kolicina",
        `SKLADIŠTE`,  // "sklad_kolicina",
        `STATUS`,
        
        `DOCS`, // spec_docs
        
        `DELETE`,
        
      ],
            
      format_cols: {
        "sifra_link": "center",
        "grupa_naziv": "center",
        "boja": "center",
        "cijena": this_module.sirov_valid.cijena.decimals,
        "kontra_tok": this_module.sirov_valid.kontra_tok.decimals,
        "tok": this_module.sirov_valid.tok.decimals,
        "po_valu": this_module.sirov_valid.po_valu.decimals,
        "po_kontravalu": this_module.sirov_valid.po_kontravalu.decimals,
        
        "pk_kolicina": this_module.sirov_valid.pk_kolicina.decimals,
        "nk_kolicina": this_module.sirov_valid.nk_kolicina.decimals,
        "order_kolicina": this_module.sirov_valid.order_kolicina.decimals,
        "sklad_kolicina": this_module.sirov_valid.sklad_kolicina.decimals,
        
      },
      query: {},
      
      show_on_click: false,
      
      cit_run: async function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        console.log(`kliknuo sam na red sirovine za document`);
        return;
        
      },
      
      button_delete: async function(e) {
        
        e.stopPropagation();
        e.preventDefault();
      
        var this_button = this;
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          
          data.partner_doc_sirovs = delete_item( data.partner_doc_sirovs, sifra , `_id` );
          
          
          $(`#`+parent_row_id).remove();


        };
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite izbaciti ovaj zapis iz dokumenta?`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite izbaciti ovaj zapis iz dokumenta?`, null, 'yes/no', delete_yes, delete_no, null);
        
      },      
      
      
    };
    
    
    if ( partner_doc_sirovs_for_table?.length > 0 ) create_cit_result_list(partner_doc_sirovs_for_table , doc_sirov_props );
    
    
  };
  this_module.make_doc_sirovs_list = make_doc_sirovs_list;
  
  
  async function partner_offer_dobav() {

    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    refresh_simple_cal( $('#'+rand_id+`_doc_est_time`) );
    
    
    if ( !data.doc_lang ) {
      data.doc_lang = { sifra: `hr`, naziv: `HRVATSKI` };
    };
    
    if ( !data.doc_valuta ) {
      data.doc_valuta = { "sifra": "DEFV1", "naziv": "Hrvatska", "valuta": "HRK" };
    };
    
    
    if ( !data._id ) {
      popup_warn(`Potrebno izabrati partnera / dobavljača!`);
      return;
    };
    
    
    if ( !data.doc_est_time ) {
      popup_warn(`Potrebno izabrati datum dokumenta!`);
      return;
    };
    
    
    var preview_mod = await get_cit_module('/modules/previews/cit_previews.js', `load_css`);
    
    // pošto se nalazim na page-u od dobavljača
    var db_partner = data;
    
    var items = [];
    
    $.each(data.partner_doc_sirovs, function(s_ind, sirov)  {
      
      
      data.partner_doc_sirovs[s_ind].in_roba = true;
      
      $.each( sirov.doc_kolicine, function(k_ind, kol) {
        
        var koef = 1;
        
        if ( sirov.min_order_unit?.jedinica !== sirov.osnovna_jedinica.jedinica ) {
          koef = sirov.normativ_omjer || 1;
        };
        
        items.push({
          /* statuses: sirov.doc_statuses, */
          naziv: ( (sirov.dobav_sifra || "") + "--" + (sirov.dobav_naziv || "") + " (" + sirov.sirovina_sifra + ")" ),
          jedinica_in: sirov.osnovna_jedinica.jedinica || "",
          jedinica_out: sirov.min_order_unit?.jedinica || "",
          count: kol.count,
          decimals: 3,
          koef: koef,
          sirov_id: sirov._id,
          sirovina_sifra: sirov.sirovina_sifra,
          full_naziv: sirov.full_naziv,
        });
        
      });  
      
    });
    
    
      
    var data_for_doc = {
      doc_type: `offer_dobav`,
      
      partner_module: this_module,
      partner_data: data,
      
      prev_doc: null,
      doc_parent: null,
      
      
      sifra: "dfd"+cit_rand(),
      time: Date.now(),

      for_module: `sirov`,
      
      proj_sifra : null,
      item_sifra : null,
      variant : null,
      status: null,
      
      
      doc_sirovs: data.partner_doc_sirovs,
      /* full_doc_name: ( data.sirovina_sifra + "--" + window.concat_naziv_sirovine(data) + "--" + data.dobavljac?.naziv), */
      
      doc_valuta: ( data.doc_valuta?.valuta || "HRK" ), // default je HRK
      doc_valuta_manual: (data.doc_valuta_manual || null ), // manual tečaj
      referent: window.cit_user.full_name,
      place: `Velika Gorica`,

      doc_num: `---`,

      
      oib: (db_partner.oib || db_partner.vat_num || ""),
      partner_name: db_partner.naziv,
      partner_adresa: data.doc_adresa?.full_adresa || "",
      partner_id: db_partner._id,
      
      /*
      partner_place: `67122 ALTRIP`,
      partner_country: `Germany`,
      */
      
      rok: data.doc_est_time,
      ref_doc_sifra: ``,
      
      items: items,
      
      doc_komentar: data.doc_komentar,

    };
    
    // return;
    
    preview_mod.generate_cit_doc( data_for_doc, data.doc_lang.sifra );

  };
  this_module.partner_offer_dobav = partner_offer_dobav;
  
  
  function update_ulazna_ponuda_docs(files, button, arg_time) {
    
    
    var this_comp_id = button.closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    
    
    var new_docs_arr = [];
    
    $.each(files, function(f_index, file) {
      
      var file_obj = {
        sifra: `doc`+cit_rand(),
        time: arg_time,
        link: `<a href="/docs/${time_path(arg_time)}/${file.filename}" target="_blank" onClick="event.stopPropagation();">${ file.originalname }</a>`,
      };
      
      new_docs_arr.push( file_obj );
    });
    
    
    console.log( new_docs_arr );
    
    // napravi novi objekt ako current ne postoji
    if ( !window.current_ulazna_ponuda ) window.current_ulazna_ponuda = {};
    
    if ( $.isArray(window.current_ulazna_ponuda.doc_files) ) {
      // ako postoji docs array onda dodaj nove filove
      window.current_ulazna_ponuda.doc_files = [ ...window.current_ulazna_ponuda.doc_files, ...new_docs_arr ];
    } else {
      // ako ne postoji onda stavi ove nove filove
      window.current_ulazna_ponuda.doc_files = new_docs_arr;
    }; 
    
  
    
    
  };
  this_module.update_ulazna_ponuda_docs = update_ulazna_ponuda_docs;
  

  function save_ulazna_ponuda() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    if ( !data.current_doc_parent ) {
      popup_warn( `Niste izabrali niti jedan Zahtjev za ponudu (RFQ)!!!<br>Nije moguće spremiti Ponudu dobavljača bez povezanog Zahtjeva !!!` );
      return;
    };

    var comp_id = rand_id+`_ulazna_ponuda_docs`;

    // vrati nazad da gumb za save dokumenata  bude vidljiv
    // ionako ću stakriti parent od gumba i cijelu listu obrisati
    $(`#${comp_id}_upload_btn`).css(`display`, `flex`);
    // obriši listu
    $(`#${comp_id}_list_items`).html( "" );
    // sakrij listu i button
    $(`#${comp_id}_list_box`).css( `display`, `none` );

    
    
    var parent_copy = cit_deep( data.current_doc_parent );
    
    // ----------------------------------- NOVA ULAZNA PONUDA -----------------------------------
    
    if ( parent_copy.doc_type !== `offer_dobav` ) {
      popup_warn(`Potrebno je označiti zahtjev za ponudu da bi upisali ulaznu ponudu!!`);
      return;
    };
    
      

    if ( !window.current_ulazna_ponuda ) window.current_ulazna_ponuda = {};

    window.current_ulazna_ponuda.sifra = $('#'+rand_id+`_ulazna_ponuda_sifra`).val();

    if ( !window.current_ulazna_ponuda.sifra || !window.current_ulazna_ponuda.doc_files ) {
      popup_warn(`Potrebno je upisati šifru ulazne ponude i napraviti UPLOAD dokumenta ponude !!!!`);
      return;
    };



    if ( parent_copy.doc_sirovs?.length > 0 ) {

      $.each(parent_copy.doc_sirovs, function( s_ind, sir ) {
        
        if ( !sir.sifra ) parent_copy.doc_sirovs[s_ind].sifra = `docsir` + cit_rand();
        
        $.each(sir.doc_kolicine, function( k_ind, kol ) {


          // NEXT POSTAJE TRENUTNA VRIJEDNOST
          parent_copy.doc_sirovs[s_ind].doc_kolicine[k_ind].count = kol.next_count;
          parent_copy.doc_sirovs[s_ind].doc_kolicine[k_ind].price = kol.next_price;

          parent_copy.doc_sirovs[s_ind].doc_kolicine[k_ind].next_approved = false;
          parent_copy.doc_sirovs[s_ind].doc_kolicine[k_ind].next_doc = false;


        });  

      });

    };

    

    // kopiraj parent dokument
    // ali promjeni sifru vrijeme i doc type
    var new_doc = {

      ...parent_copy,

      sifra: "dfd"+cit_rand(),
      time: Date.now(),
      doc_type: `ulazna_ponuda`,

      doc_files: cit_deep(window.current_ulazna_ponuda.doc_files),
      doc_sifra: window.current_ulazna_ponuda.sifra,

    };

    // i postavi da taj parent RFQ dokument bude u doc parent propertiju
    // i postavi da taj parent RFQ dokument bude u doc parent propertiju
    // i postavi da taj parent RFQ dokument bude u doc parent propertiju
    new_doc.doc_parent = cit_deep( data.current_doc_parent );
    new_doc.prev_doc = data.current_doc_parent.sifra;

    data.docs = upsert_item( data.docs, new_doc, `sifra`);

    this_module.make_partner_doc_list(data);


    popup_warn(`Novi red za ulaznu ponudu je generiran!`);

    window.current_ulazna_ponuda = null;

    $('#'+rand_id+`_ulazna_ponuda_sifra`).val("");

      
  
    
    setTimeout( function() {
      $(`#save_partner_btn`).trigger(`click`);
    }, 200);
    

  };
  this_module.save_ulazna_ponuda = save_ulazna_ponuda;
  
  
  
  function update_ponuda_files() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    if ( !data.current_doc_parent ) {
      popup_warn( `Niste izabrali niti jednu ulaznu ponudu !!!<br>Nije moguće spremiti nove datoteke ako niste izabrali ulaznu ponudu !!!` );
      return;
    };

    var comp_id = rand_id+`_ulazna_ponuda_docs`;

    // vrati nazad da gumb za save dokumenata  bude vidljiv
    // ionako ću stakriti parent od gumba i cijelu listu obrisati
    $(`#${comp_id}_upload_btn`).css(`display`, `flex`);
    // obriši listu
    $(`#${comp_id}_list_items`).html( "" );
    // sakrij listu i button
    $(`#${comp_id}_list_box`).css( `display`, `none` );
    
    
    
    
    if ( !window.current_ulazna_ponuda || window.current_ulazna_ponuda?.doc_files?.length < 1 ) {
      popup_warn(`Niste izabrali niti jednu novu datoteku !!!!`);
      return;
    };
    
    
    var parent_copy = cit_deep( data.current_doc_parent );

    
    
    if ( parent_copy.doc_type !== `ulazna_ponuda` ) {
      popup_warn(`Red koji ste izabrali nije ulazna ponuda !!!!`);
      return;
    };
    

    if ( parent_copy.doc_type == `ulazna_ponuda` ) {
      
      var new_files = window.current_ulazna_ponuda.doc_files || [];
      var curr_doc_index = find_index( data.docs, data.current_doc_parent.sifra, `sifra` );
      var old_files = data.docs[curr_doc_index].doc_files || [];
      
      // ------------------------------------ AKO JE USER IZABRAO RED ULAZNE  PONUDE I ŽELI DODATI NEKI DOKUMENT !!!
      if ( data.current_doc_parent && data.current_doc_parent.doc_type == "ulazna_ponuda" ) {
        window.current_ulazna_ponuda.doc_files = data.current_doc_parent.doc_files || [];
      };

      
      data.docs[curr_doc_index].doc_files = [ ...old_files, ...new_files ];
      
      
      popup_warn(`Nove datateke su dodane u označenu ulaznu ponudu !`);

      window.current_ulazna_ponuda = null;
      $('#'+rand_id+`_ulazna_ponuda_sifra`).val("");
      
      
      this_module.make_partner_doc_list(data);
      

    };
    
    
    setTimeout( function() {
      $(`#save_partner_btn`).trigger(`click`);
    }, 200);
    

  };
  this_module.update_ponuda_files = update_ponuda_files;
  
  
  
  
  async function partner_ORDER_dobav() {
    
    var this_comp_id = $(`#order_dobav_btn`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    if ( !data.current_doc_parent ) {
      
      popup_warn(`Niste izabrali niti jedanu ulaznu ponudu!!!<br>Zaista želite napravi ND bez ponude?`);
      
      // ------------------------------  ND BEZ PONUDE ??? ------------------------------------------
      function doc_parent_missing_yes() {
        
        this_module.run_ORDER_dobav();
        
      };

      function doc_parent_missing_no() {
        show_popup_modal(false, pop_text, null );
        return;
      };

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, pop_text, null, 'yes/no', doc_parent_missing_yes, doc_parent_missing_no, null);
      
      // ------------------------------------------------------------------------
      
      
    } 
    else {
      // ako ima doc parent onda samo kreni dalje !!!
      this_module.run_ORDER_dobav();
      
    };
    
    
  };
  this_module.partner_ORDER_dobav = partner_ORDER_dobav;
  
  
  async function run_ORDER_dobav() {
    
    var this_comp_id = $(`#order_dobav_btn`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    refresh_simple_cal( $('#'+rand_id+`_doc_est_time`) );
    
    
    if ( !data.doc_lang ) {
      data.doc_lang = { sifra: `hr`, naziv: `HRVATSKI` };
    };
    
    if ( !data.doc_valuta ) {
      data.doc_valuta = { "sifra": "DEFV1", "naziv": "Hrvatska", "valuta": "HRK" };
    };
    
    
    if ( !data._id ) {
      popup_warn(`Potrebno izabrati partnera / dobavljača!`);
      return;
    };
    
    
    if ( !data.doc_est_time ) {
      popup_warn(`Potrebno izabrati datum dokumenta!`);
      return;
    };
    
   
    if ( !data.partner_doc_sirovs || data.partner_doc_sirovs?.length == 0 ) {
      popup_warn(`Potrebno izabrati izabrati barem jednu sirovinu za ND !`);
      return;
    };
    
    
    var preview_mod = await get_cit_module('/modules/previews/cit_previews.js', `load_css`);
    
    // pošto se nalazim na page-u od dobavljača
    var db_partner = data;
    
    var items = [];
    
    var order_sirov_arr = [];
    
    var doc_sirovs_copy = cit_deep(data.partner_doc_sirovs);
    
    $.each(doc_sirovs_copy, function(s_ind, sirov) {
      
      doc_sirovs_copy[s_ind].in_roba = true;
      
      
      $.each( sirov.doc_kolicine, function(k_ind, kol) {
        
        // mora biti označen next doc i mora biti upisana next cijena !!!
        if ( kol.next_doc && kol.next_price ) {
          
          var koef = 1;

          if ( sirov.min_order_unit?.jedinica !== sirov.osnovna_jedinica.jedinica ) {
            koef = sirov.normativ_omjer || 1;
          };

          
          // upiši i u novi doc sirov koje su nove cijene
          doc_sirovs_copy[s_ind].doc_kolicine[k_ind].price = kol.next_price;
          doc_sirovs_copy[s_ind].doc_kolicine[k_ind].count = kol.next_count;
          
          doc_sirovs_copy[s_ind].doc_kolicine[k_ind].next_approved = false;
          doc_sirovs_copy[s_ind].doc_kolicine[k_ind].next_doc = false;

          order_sirov_arr.push( doc_sirovs_copy[s_ind] );

          items.push({

            /*statuses: sirov.doc_statuses,*/
            naziv: ( (sirov.dobav_sifra || "") + "--" + (sirov.dobav_naziv || "") + " (" + sirov.sirovina_sifra + ")" ),
            jedinica_in: sirov.osnovna_jedinica.jedinica || "",
            jedinica_out: sirov.min_order_unit?.jedinica  || "",
            koef: koef,
            price: kol.next_price, 
            count:  kol.next_count,
            decimals: 3,
            sirov_id: sirov._id,
            sirovina_sifra: sirov.sirovina_sifra,
            full_naziv: sirov.full_naziv,

          });
          
          
        }; // samo uvrsti ako je označen next doc

      });  
      
    }); // loop po sivm sirovinama za dokument
    

    // ako je definirana dostava dobavljača u ponudi onda je ubaci i u ND !!!!
    // ako je definirana dostava dobavljača u ponudi onda je ubaci i u ND !!!!
    
    if ( data.current_doc_parent?.doc_dostava ) {
      
      items.push({

        /*statuses: sirov.doc_statuses,*/
        naziv: "DOSTAVA",
        jedinica_in: "kom",
        jedinica_out: "kom",
        koef: 1,
        price: data.current_doc_parent.doc_dostava, 
        count:  1,
        decimals: 3,
        sirov_id: "INDOSTAVA",
        sirovina_sifra: "INDOSTAVA",
        full_naziv: "",

      });
      
      
    };
    
    

    var data_for_doc = {
      
      doc_type: `order_dobav`,
      
      order_avans: data.order_avans || null,
      
      partner_module: this_module,
      partner_data: data,
      prev_doc: data.current_doc_parent.sifra,
      doc_parent: {
        
        sifra: data.current_doc_parent.doc_parent.sifra, /* ovdje namjerno preskačem ulaznu ponudu i direktno uzimam RFQ kao doc_parent */
        doc_sifra: data.current_doc_parent.doc_parent.doc_sifra,
        doc_type: data.current_doc_parent.doc_parent.doc_type,
        
      },
      
      sirov_status: data.current_doc_parent?.sirov_status || null,
      
      doc_dostava: data.current_doc_parent?.doc_dostava || null,
      
      sifra: "dfd"+cit_rand(),
      time: Date.now(),

      for_module: `sirov`,
      
      proj_sifra : null,
      item_sifra : null,
      variant : null,
      status: null,
      
      
      doc_sirovs: order_sirov_arr,
      /* full_doc_name: ( data.sirovina_sifra + "--" + window.concat_naziv_sirovine(data) + "--" + data.dobavljac?.naziv), */
      
      doc_valuta: ( data.doc_valuta?.valuta || "HRK" ), // default je HRK
      doc_valuta_manual: (data.doc_valuta_manual || null ), // manual tečaj
      referent: window.cit_user.full_name,
      
      place: `Velika Gorica`,

      doc_num: `---`,

      
      oib: (db_partner.oib || db_partner.vat_num || ""),
      partner_name: db_partner.naziv,
      partner_adresa: data.doc_adresa?.full_adresa || "",
      partner_id: db_partner._id,
      
      /*
      partner_place: `67122 ALTRIP`,
      partner_country: `Germany`,
      */
      
      rok: data.doc_est_time,
      ref_doc_sifra: data.ref_doc_sifra || "",
      
      items: items,
      
      doc_komentar: data.doc_komentar,

    };
    
    // return;
    
    preview_mod.generate_cit_doc( data_for_doc, data.doc_lang.sifra );    
    
  };
  this_module.run_ORDER_dobav = run_ORDER_dobav;
  


  async function partner_PRIMKA_dobav() {

    var this_comp_id = $(`#primka_btn`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    if ( !data.current_doc_parent ) {

      popup_warn(`Niste izabrali niti jedan ND!!!<br>Obavezno je izabrati ND!`);
      
    } 
   
    else {

      this_module.run_PRIMKA_dobav();

    };


  };
  this_module.partner_PRIMKA_dobav = partner_PRIMKA_dobav;
  
  
  async function run_PRIMKA_dobav() {
    
    var this_comp_id = $(`#primka_btn`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    refresh_simple_cal( $('#'+rand_id+`_doc_est_time`) );
    
    
    if ( !data.doc_lang ) {
      data.doc_lang = { sifra: `hr`, naziv: `HRVATSKI` };
    };
    
    if ( !data.doc_valuta ) {
      data.doc_valuta = { "sifra": "DEFV1", "naziv": "Hrvatska", "valuta": "HRK" };
    };
    
    
    if ( !data._id ) {
      popup_warn(`Potrebno izabrati partnera / dobavljača!`);
      return;
    };
    
    
    if ( !data.doc_est_time ) {
      popup_warn(`Potrebno izabrati točan datum PRIMKE!`);
      return;
    };
    
    
    var preview_mod = await get_cit_module('/modules/previews/cit_previews.js', `load_css`);
    
    // pošto se nalazim na page-u od dobavljača
    var db_partner = data;
    
    var items = [];
    
    
    var doc_sirovs_copy = cit_deep( data.partner_doc_sirovs );
    
    var primka_sirovs_arr = [];
    
    $.each( data.partner_doc_sirovs, function(s_ind, sirov) {
      
      doc_sirovs_copy[s_ind].in_roba = true;
      
      $.each( sirov.doc_kolicine, function(k_ind, kol) {
        
        
        // SAMO AKO JE USER IZABRAO DA SE PRIKAŽE U PRIMCI !!!
        if ( kol.next_doc && kol.next_price ) {
          
          var koef = 1;

          if ( sirov.min_order_unit?.jedinica !== sirov.osnovna_jedinica.jedinica ) {
            koef = sirov.normativ_omjer || 1;
          };

          
          // upiši i u novi doc sirov koje su nove cijene
          doc_sirovs_copy[s_ind].doc_kolicine[k_ind].price = kol.next_price;
          doc_sirovs_copy[s_ind].doc_kolicine[k_ind].count = kol.next_count;
          
          
          doc_sirovs_copy[s_ind].doc_kolicine[k_ind].next_approved = false;
          doc_sirovs_copy[s_ind].doc_kolicine[k_ind].next_doc = false;

          primka_sirovs_arr.push( doc_sirovs_copy[s_ind] );
          
          

          items.push({

            /*statuses: sirov.doc_statuses,*/
            naziv: ( (sirov.dobav_sifra || "") + "--" + (sirov.dobav_naziv || "") + " (" + sirov.sirovina_sifra + ")" ),
            
            jedinica_in: sirov.osnovna_jedinica.jedinica || "",
            jedinica_out: sirov.min_order_unit?.jedinica  || "",
            koef: koef,
            price: kol.next_price, 
            
            nd_count: kol.count,
            count:  kol.next_count,
            
            decimals: 3,
            sirov_id: sirov._id,
            sirovina_sifra: sirov.sirovina_sifra,
            full_naziv: sirov.full_naziv,

          });
          
        }; // ako je select primka true

      });  
      
    });
    
    
    var data_for_doc = {
      doc_type: `in_record`,
      
      order_avans: null,
      
      partner_module: this_module,
      partner_data: data,
      
      
      nd_parent: data.current_doc_parent.sifra, // ovo mora biti ND
      
      prev_doc: data.current_doc_parent.sifra,
      
      doc_parent: {
        
        sifra: data.current_doc_parent.doc_parent.sifra,
        doc_sifra: data.current_doc_parent.doc_parent.doc_sifra,
        doc_type: data.current_doc_parent.doc_parent.doc_type,
        
      },
      
      sirov_status: data.current_doc_parent?.sirov_status || null,
      
      sifra: "dfd"+cit_rand(),
      time: Date.now(),

      for_module: `sirov`,
      
      proj_sifra : null,
      item_sifra : null,
      variant : null,
      status: null,
      
      
      doc_sirovs: primka_sirovs_arr,
      /* full_doc_name: ( data.sirovina_sifra + "--" + window.concat_naziv_sirovine(data) + "--" + data.dobavljac?.naziv), */
      
      doc_valuta: ( data.doc_valuta?.valuta || "HRK" ), // default je HRK
      doc_valuta_manual: (data.doc_valuta_manual || null ), // manual tečaj
      referent: window.cit_user.full_name,
      place: `Velika Gorica`,

      doc_num: `---`,

      
      oib: (db_partner.oib || db_partner.vat_num || ""),
      partner_name: db_partner.naziv,
      partner_adresa: data.doc_adresa?.full_adresa || "",
      partner_id: db_partner._id,
      
      /*
      partner_place: `67122 ALTRIP`,
      partner_country: `Germany`,
      */
      
      rok: data.doc_est_time,
      ref_doc_sifra: data.ref_doc_sifra || "",
      
      items: items,
      
      orient: "landscape",
      
      doc_komentar: data.doc_komentar,
      

    };
    
    // return;
    
    preview_mod.generate_cit_doc( data_for_doc, data.doc_lang.sifra );    
    
  };
  this_module.run_PRIMKA_dobav = run_PRIMKA_dobav;
  
  
  // -------------------------------------------------------- primka iz proizvodnje !!!
  // -------------------------------------------------------- primka iz proizvodnje !!!
  // -------------------------------------------------------- primka iz proizvodnje !!!
  async function prodon_primka() {
    
    var this_comp_id = $(`#primka_prodon_btn`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    refresh_simple_cal( $('#'+rand_id+`_doc_est_time`) );
    
    if ( !data.doc_lang ) {
      data.doc_lang = { sifra: `hr`, naziv: `HRVATSKI` };
    };
    
    if ( !data.doc_valuta ) {
      data.doc_valuta = { "sifra": "DEFV1", "naziv": "Hrvatska", "valuta": "HRK" };
    };
    
    
    if ( !data._id ) {
      popup_warn(`Potrebno izabrati partnera / dobavljača!`);
      return;
    };
    
    
    if ( !data.doc_est_time ) {
      popup_warn(`Potrebno izabrati točan datum PRIMKE IZ PROIZVODNJE!`);
      return;
    };
    
    if ( !data.current_doc_parent ) {
      popup_warn(`Potrebno izabrati jedan od RADNIH NALOGA na listi !`);
      return;
    };
    
    
    if ( !data.ask_kolicina_1 ) {
      popup_warn(`Upišite točnu količinu koju zaprimate iz proizvodnje!`);
      return;
    };
    
    
    var preview_mod = await get_cit_module('/modules/previews/cit_previews.js', `load_css`);
    
    // pošto se nalazim na page-u od dobavljača
    var db_partner = data;
    
    var items = [];
    
    
    if ( !data.partner_doc_sirovs || data.partner_doc_sirovs?.length == 0 ) {
      popup_warn(`Za primku iz proizvodnje potrebno je izabrati JEDAN ILI VIŠE VELPROM PROIZVODA !!!`);
      return;
    };
    
    
    var product_ids = [];
    var original_missing = false;
    $.each( data.partner_doc_sirovs, function(s_ind, sirov) {
      
      if ( sirov.original ) product_ids.push(sirov.original);
      
      if ( !sirov.original && original_missing == false ) {
        original_missing = true;
        popup_warn(`Nedostaju podaci proizvoda za robu ${sirov.naziv} !!!`);
      };
      
    });
    
    if ( original_missing == true ) return; // ako bilo koji sirov nema original prop onda to nije prozivod !!!
    
    
    var DB_products = await ajax_find_query( 
      { _id: { $in: product_ids } },
      `/find_product`,
      null
    );
    
    
    $.each(data.partner_doc_sirovs, function(s_ind, sirov) {
      
      // pronadji product koji mi treba od svih producata koje sam dobio sa baze !!!
      var product = find_item( DB_products, data.partner_doc_sirovs[s_ind]._id, `_id`);
      
      var cijena_sirov = sirov.pro_cijena;
      var custom_doc_price = sirov.cijena;

      var order_kolicina = product.naklada;
      var primka_kolicina = product.produced;

      // DODAJ SAMO JEDAN DOC KOLIČINE 
      data.partner_doc_sirovs[s_ind].doc_kolicine = [{
        sifra: "dockol"+cit_rand(),
        primka_count: data.ask_kolicina_1,
      }];

      items.push({

        /*statuses: sirov.doc_statuses,*/
        naziv: product.full_product_name,
        jedinica: sirov.osnovna_jedinica?.jedinica || "",
        custom_doc_price: custom_doc_price,
        primka_price: cijena_sirov,
        order_count: order_kolicina, // u slučaju primke iz proiz ovo je NAKLADA
        primka_count: data.ask_kolicina_1, // u slučaju primke iz proiz ovo je PRODUCED prop u proizvodu
        decimals: 3,
        sirov_id: sirov._id,
        sirovina_sifra: sirov.sirovina_sifra,
        full_naziv: product.full_product_name,

      });

    });
    
    
    var data_for_doc = {
      
      doc_type: `in_pro_record`,
      
      order_avans: null,
      
      partner_module: this_module,
      partner_data: data,
      doc_parent: null,
      
      sirov_status: null,
      
      sifra: "dfd"+cit_rand(),
      time: Date.now(),

      for_module: `sirov`,
      
      proj_sifra : null,
      item_sifra : null,
      variant : null,
      status: null,
      
      
      doc_sirovs: data.partner_doc_sirovs,
      /* full_doc_name: ( data.sirovina_sifra + "--" + window.concat_naziv_sirovine(data) + "--" + data.dobavljac?.naziv), */
      
      doc_valuta: ( data.doc_valuta?.valuta || "HRK" ), // default je HRK
      doc_valuta_manual: (data.doc_valuta_manual || null ), // manual tečaj
      referent: window.cit_user.full_name,
      place: `Velika Gorica`,

      doc_num: `---`,

      
      oib: (db_partner.oib || db_partner.vat_num || ""),
      partner_name: db_partner.naziv,
      partner_adresa: data.doc_adresa?.full_adresa || "",
      
      otp_adresa: data.otp_adresa?.full_adresa || "",
      
      partner_id: db_partner._id,
      
      /*
      partner_place: `67122 ALTRIP`,
      partner_country: `Germany`,
      */
      
      rok: data.doc_est_time,
      ref_doc_sifra: data.ref_doc_sifra || "",
      
      items: items,
      orient: "landscape",
      
      doc_komentar: data.doc_komentar,
      

    };
    
    // return;
    
    preview_mod.generate_cit_doc( data_for_doc, data.doc_lang.sifra );    
    
  };
  this_module.prodon_primka = prodon_primka;
  
  
  async function doc_otp() {
    
    var this_comp_id = $(`#primka_prodon_btn`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    refresh_simple_cal( $('#'+rand_id+`_doc_est_time`) );
    
    if ( !data.doc_lang ) {
      data.doc_lang = { sifra: `hr`, naziv: `HRVATSKI` };
    };
    
    if ( !data.doc_valuta ) {
      data.doc_valuta = { "sifra": "DEFV1", "naziv": "Hrvatska", "valuta": "HRK" };
    };
    
    
    if ( !data._id ) {
      popup_warn(`Potrebno izabrati partnera / dobavljača!`);
      return;
    };
    
    
    if ( !data.doc_est_time ) {
      popup_warn(`Potrebno izabrati točan datum PRIMKE IZ PROIZVODNJE!`);
      return;
    };
    
    
    if ( !data.ask_kolicina_1 ) {
      popup_warn(`Upišite točnu količinu koju zaprimate iz proizvodnje!`);
      return;
    };
    
    
    var preview_mod = await get_cit_module('/modules/previews/cit_previews.js', `load_css`);
    
    // pošto se nalazim na page-u od dobavljača
    var db_partner = data;
    
    var items = [];
    
    
    if ( !data.partner_doc_sirovs || data.partner_doc_sirovs?.length == 0 ) {
      popup_warn(`Za primku iz proizvodnje potrebno je izabrati JEDAN ILI VIŠE VELPROM PROIZVODA !!!`);
      return;
    };
    
    
    var product_ids = [];
    var original_missing = false;
    $.each(data.partner_doc_sirovs, function(s_ind, sirov)  {
      
      if ( sirov.original ) product_ids.push(sirov.original);
      
      if ( !sirov.original && original_missing == false ) {
        original_missing = true;
        popup_warn(`Nedostaju podaci proizvoda za robu ${sirov.naziv} !!!`);
      };
      
    });
    
    if ( original_missing == true ) return; // ako bilo koji sirov nema original prop onda to nije prozivod !!!
    

    
    var DB_products = await ajax_find_query( 
      { _id: { $in: product_ids } },
      `/find_product`,
      null
    );
    
    
    $.each(data.partner_doc_sirovs, function(s_ind, sirov) {
      

      var product = find_item( DB_products, data.partner_doc_sirovs[s_ind]._id, `_id`);
      
      var cijena_sirov = sirov.pro_cijena;
      var custom_doc_price = sirov.cijena;

      var order_kolicina = product.naklada;
      var primka_kolicina = product.produced;

      // DODAJ SAMO JEDAN DOC KOLIČINE 
      data.partner_doc_sirovs[s_ind].doc_kolicine = [{
        sifra: "dockol"+cit_rand(),
        primka_count: data.ask_kolicina_1,
      }];

      items.push({

        /*statuses: sirov.doc_statuses,*/
        naziv: product.full_product_name,
        jedinica: sirov.osnovna_jedinica?.jedinica || "",
        custom_doc_price: custom_doc_price,
        primka_price: cijena_sirov,
        order_count: order_kolicina, // u slučaju primke iz proiz ovo je NAKLADA
        primka_count: data.ask_kolicina_1, // u slučaju primke iz proiz ovo je PRODUCED prop u proizvodu
        decimals: 3,
        sirov_id: sirov._id,
        sirovina_sifra: sirov.sirovina_sifra,
        full_naziv: product.full_product_name,

      });

    });
    
    
    var data_for_doc = {
      doc_type: `out_record`,
      
      order_avans: null,
      
      partner_module: this_module,
      partner_data: data,
      doc_parent: null,
      
      sirov_status: null,
      
      sifra: "dfd"+cit_rand(),
      time: Date.now(),

      for_module: `sirov`,
      
      proj_sifra : null,
      item_sifra : null,
      variant : null,
      status: null,
      
      
      doc_sirovs: data.partner_doc_sirovs,
      /* full_doc_name: ( data.sirovina_sifra + "--" + window.concat_naziv_sirovine(data) + "--" + data.dobavljac?.naziv), */
      
      doc_valuta: ( data.doc_valuta?.valuta || "HRK" ), // default je HRK
      doc_valuta_manual: (data.doc_valuta_manual || null ), // manual tečaj
      referent: window.cit_user.full_name,
      place: `Velika Gorica`,

      doc_num: `---`,

      
      oib: (db_partner.oib || db_partner.vat_num || ""),
      partner_name: db_partner.naziv,
      partner_adresa: data.doc_adresa?.full_adresa || "",
      
      otp_adresa: data.otp_adresa?.full_adresa || "",
      
      partner_id: db_partner._id,
      
      /*
      partner_place: `67122 ALTRIP`,
      partner_country: `Germany`,
      */
      
      rok: data.doc_est_time,
      ref_doc_sifra: data.ref_doc_sifra || "",
      
      items: items,
      orient: "landscape",
      
      doc_komentar: data.doc_komentar,
      

    };
    
    // return;
    
    preview_mod.generate_cit_doc( data_for_doc, data.doc_lang.sifra );    
    
  };
  this_module.doc_otp = doc_otp;

  


  
  async function partner_out_bill() {
    
    var this_comp_id = $(`#order_dobav_btn`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    refresh_simple_cal( $('#'+rand_id+`_doc_est_time`) );
    
    
    if ( !data.current_doc_parent ) {
      popup_warn(`Potrebno označiti ili NK ili OTPREMNICU kako bi generirali IZLAZNI RAČUN !!!`);
      return;
    };
      
    var parent_copy = cit_deep( data.current_doc_parent );
    
    if ( parent_copy.doc_type !== `order_reserv` && parent_copy.doc_type !== `out_record` ) {
      popup_warn(`Potrebno označiti ili NK ili OTPREMNICU kako bi generirali IZLAZNI RAČUN !!!`);
      return;
    };
    
    
    if ( !data.doc_lang ) {
      data.doc_lang = { sifra: `hr`, naziv: `HRVATSKI` };
    };
    
    if ( !data.doc_valuta ) {
      data.doc_valuta = { "sifra": "DEFV1", "naziv": "Hrvatska", "valuta": "HRK" };
    };
    
    
    if ( !data._id ) {
      popup_warn(`Potrebno izabrati partnera / dobavljača!`);
      return;
    };
    
    
    if ( !data.doc_est_time ) {
      popup_warn(`Potrebno izabrati datum dokumenta!`);
      return;
    };
    
   
    if ( !data.partner_doc_sirovs || data.partner_doc_sirovs?.length == 0 ) {
      popup_warn(`Potrebno izabrati izabrati barem jednu sirovinu ili uslugu za IZLAZNI RAČUN !`);
      return;
    };
    
    
    var preview_mod = await get_cit_module('/modules/previews/cit_previews.js', `load_css`);
    
    // pošto se nalazim na page-u od dobavljača
    var db_partner = data;
    
    var items = [];
    
    var order_sirov_arr = [];
    
    var doc_sirovs_copy = cit_deep(data.partner_doc_sirovs);
    
    $.each(doc_sirovs_copy, function(s_ind, sirov) {
      
      doc_sirovs_copy[s_ind].in_roba = true;
      
      
      $.each( sirov.doc_kolicine, function(k_ind, kol) {
        
        // mora biti označen next doc i mora biti upisana next cijena !!!
        if ( kol.next_doc && kol.next_price ) {
          
          var koef = 1;

          if ( sirov.min_order_unit?.jedinica !== sirov.osnovna_jedinica.jedinica ) {
            koef = sirov.normativ_omjer || 1;
          };

          
          // upiši i u novi doc sirov koje su nove cijene
          doc_sirovs_copy[s_ind].doc_kolicine[k_ind].price = kol.next_price;
          doc_sirovs_copy[s_ind].doc_kolicine[k_ind].count = kol.next_count;
          
          doc_sirovs_copy[s_ind].doc_kolicine[k_ind].next_approved = false;
          doc_sirovs_copy[s_ind].doc_kolicine[k_ind].next_doc = false;

          order_sirov_arr.push( doc_sirovs_copy[s_ind] );

          items.push({

            /*statuses: sirov.doc_statuses,*/
            naziv: ( (sirov.dobav_sifra || "") + "--" + (sirov.dobav_naziv || "") + " (" + sirov.sirovina_sifra + ")" ),
            jedinica_in: sirov.osnovna_jedinica.jedinica || "",
            jedinica_out: sirov.min_order_unit?.jedinica  || "",
            koef: koef,
            price: kol.next_price, 
            count:  kol.next_count,
            decimals: 3,
            sirov_id: sirov._id,
            sirovina_sifra: sirov.sirovina_sifra,
            full_naziv: sirov.full_naziv,

          });
          
          
        }; // samo uvrsti ako je označen next doc

      });  
      
    }); // loop po sivm sirovinama za dokument
    

    // ako je definirana dostava dobavljača u ponudi onda je ubaci i u ND !!!!
    // ako je definirana dostava dobavljača u ponudi onda je ubaci i u ND !!!!
    
    if ( data.current_doc_parent?.doc_dostava ) {
      
      items.push({

        /*statuses: sirov.doc_statuses,*/
        naziv: "DOSTAVA",
        jedinica_in: "kom",
        jedinica_out: "kom",
        koef: 1,
        price: data.current_doc_parent.doc_dostava, 
        count:  1,
        decimals: 3,
        sirov_id: "INDOSTAVA",
        sirovina_sifra: "INDOSTAVA",
        full_naziv: "",

      });
      
      
    };
    
    

    var data_for_doc = {
      
      doc_type: `order_dobav`,
      
      order_avans: data.order_avans || null,
      
      partner_module: this_module,
      partner_data: data,
      prev_doc: data.current_doc_parent.sifra,
      doc_parent: {
        
        sifra: data.current_doc_parent.doc_parent.sifra, /* ovdje namjerno preskačem ulaznu ponudu i direktno uzimam RFQ kao doc_parent */
        doc_sifra: data.current_doc_parent.doc_parent.doc_sifra,
        doc_type: data.current_doc_parent.doc_parent.doc_type,
        
      },
      
      sirov_status: data.current_doc_parent?.sirov_status || null,
      
      doc_dostava: data.current_doc_parent?.doc_dostava || null,
      
      sifra: "dfd"+cit_rand(),
      time: Date.now(),

      for_module: `sirov`,
      
      proj_sifra : null,
      item_sifra : null,
      variant : null,
      status: null,
      
      
      doc_sirovs: order_sirov_arr,
      /* full_doc_name: ( data.sirovina_sifra + "--" + window.concat_naziv_sirovine(data) + "--" + data.dobavljac?.naziv), */
      
      doc_valuta: ( data.doc_valuta?.valuta || "HRK" ), // default je HRK
      doc_valuta_manual: (data.doc_valuta_manual || null ), // manual tečaj
      referent: window.cit_user.full_name,
      
      place: `Velika Gorica`,

      doc_num: `---`,

      
      oib: (db_partner.oib || db_partner.vat_num || ""),
      partner_name: db_partner.naziv,
      partner_adresa: data.doc_adresa?.full_adresa || "",
      partner_id: db_partner._id,
      
      /*
      partner_place: `67122 ALTRIP`,
      partner_country: `Germany`,
      */
      
      rok: data.doc_est_time,
      ref_doc_sifra: data.ref_doc_sifra || "",
      
      items: items,
      
      doc_komentar: data.doc_komentar,

    };
    
    // return;
    
    preview_mod.generate_cit_doc( data_for_doc, data.doc_lang.sifra );    
    
  };
  this_module.partner_out_bill = partner_out_bill;
  
  
  
  /*
  ------------------------------------------------------------------------------------------
  ZA SADA NE KORISTIM OVU FUNC ISPOD !!! TODO ------> potrebno napraviti ovo kasnije  
  ------------------------------------------------------------------------------------------
  */

  function ajax_edit_partner_doc( doc ) {



    var data = cit_deep(data_for_doc);


    return new Promise(function(resolve, reject) {

      $.ajax({
        headers: {
          'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
        },
        type: "POST",
        cache: false,
        url: `/edit_partner_doc`,
        data: JSON.stringify({
          
          doc: doc,
          
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done(function (result) {

        console.log(result);

        if ( result.success == true ) {

          resolve(true);

        } else {
          if ( result.msg ) popup_error( `Greška prilikom spremanja šifre dokumenta u povijest sirovine!<br>` + result.msg );
          if ( !result.msg ) popup_error( `Greška prilikom spremanja šifre dokumenta u povijest sirovine!` );
        };

      })
      .fail(function (error) {
        console.log(error);
        resolve(null);
      })
      .always(function() {

      });

    }); // kraj promisa



  }; 
  this_module.ajax_edit_partner_doc = ajax_edit_partner_doc;  

  
  /*
  ------------------------------------------------------------------------------------------
  ZA SADA NE KORISTIM OVU FUNC IZNAD !!! 
  ------------------------------------------------------------------------------------------
  */
  
  
  
  
  
  this_module.cit_loaded = true;
 
} // end of module scripts
  
}; // end of module object

