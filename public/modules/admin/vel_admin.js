var module_object = {
create: ( data, parent_element, placement ) => {
  
  var this_module = window[module_url]; 
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };
  
  
  // VAŽNO -----> data u sebi sadrži:
  /*
  ---------------------------------------
  
  data.proj_sifra
  data.item_sifra
  data.variant
  ---------------------------------------
  */
  
  
  var { id } = data;


  var valid = {
   
    naziv: { element: "input", type: "string", lock: false, visible: true, label: "Naziv statusa" },
    
    active: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Aktivan" },
    status_num: { element: "input", "type": "number", decimals: 0, lock: false, visible: true, label: "Redni broj statusa" },
    next_status_num: { element: "input", "type": "number", decimals: 0, lock: false, visible: true, label: "Redni broj" },
    
    choose_next_status: { element: "single_select", type: "simple", lock: false, visible: true, label: "Statusi u grani" },
    
    choose_grana_finish: { element: "single_select", type: "simple", lock: false, visible: true, label: "Mogući krajevi grane" },
    is_work: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Radni status" },
    work_done_for: { element: "single_select", type: "simple", lock: false, visible: true, label: "Gotov posao za:" }, 
    
    def_deadline: { element: "input", "type": "number", decimals: 0, lock: false, visible: true, label: "Sati rada" },
    cat: { element: "single_select", type: "simple", lock: false, visible: true, label: "Kategorija statusa" },
    dep: { element: "single_select", type: "simple", lock: false, visible: true, label: "Za odjel" },
    outside: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Jel status za partnera" },
    
  };

  this_module.valid = valid;  


  console.log(data);
  
  // if ( !data.status_sifra ) data = { ...data, ...dummy_status_data };
  
  
  // OVERRIDE
  // OVERRIDE
  // OVERRIDE
  // OVERRIDE
  // data.status_conf = []
  
  this_module.set_init_data(data);
  
  var rand_id = `admin`+cit_rand();
  this_module.cit_data[id].rand_id = rand_id;
  
  
  var component_html =
`
<div  id="${id}" class="cit_comp admin_comp" >
  <section>

    <div class="container-fluid">
      
      
      <div class="row" style="margin-bottom: 30px;">
        
        <div class="col-md-10 col-sm-12" id="meta_data_box">
          ${ meta_data_html(data) }
        </div>
       
       
        <div class="col-md-2 col-sm-12">
          <button class="blue_btn btn_small_text" id="save_admin_conf" style="margin: 0 0 0 auto; box-shadow: none;">
            SPREMI SVE
          </button>
        </div>

      </div>
      
      
      <div class="cit_tab_strip">
        <div class="cit_tab_btn cit_active">STATUSI</div>
        <div class="cit_tab_btn">GRANE</div>
        <div class="cit_tab_btn">UPUTE</div>
      </div>
    
      <div class="cit_tab_box">

        <div class="row" id="new_admin_status_row">

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_active`, valid.active, data.active || true)  }
          </div>


          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_status_num`, valid.status_num, "") }
          </div>

          <div class="col-md-4 col-sm-12">
            ${ cit_comp(rand_id+`_naziv`, valid.naziv, "") }
          </div>

          <div class="col-md-4 col-sm-12">
            ${ cit_comp(rand_id+`_is_work`, valid.is_work, data.is_work || false ) }
          </div>

        </div>  

        <div class="row">   

         
          
          <div class="col-md-4 col-sm-12">
            ${ cit_comp(rand_id+`_work_done_for`, valid.work_done_for, null, "" )}
          </div>
          

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_def_deadline`, valid.def_deadline, "" ) }
          </div>


          <div class="col-md-2 col-sm-12">
            ${cit_comp(rand_id+`_cat`, valid.cat, null, "" )}
          </div>

          <div class="col-md-2 col-sm-12">
            ${cit_comp(rand_id+`_dep`, valid.dep, null, "" )}
          </div>


          <div class="col-md-2 col-sm-12">

            <button class="blue_btn btn_small_text" id="save_admin_status" style="margin: 20px 0 0 auto; box-shadow: none;">
              NOVI STATUS
            </button>

            <button class="violet_btn btn_small_text" id="update_admin_status" style="margin: 20px 0 0 auto; box-shadow: none; display: none;">
              AŽURIRAJ STATUS
            </button>

          </div>

        </div>

        <div class="row">
          <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="all_statuses_list_box" style="padding-top: 20px;">
            <!-- OVDJE IDE LISTA SVIH STATUSA -->    
          </div> 

        </div>       

      </div>
      
      
      <div class="cit_tab_box" style="display: none;" >

        <div class="row">
          <div class="col-md-12 col-sm-12 cit_result_table cit_excel" id="grane_list_box" style="padding-top: 20px;">
            <!-- OVDJE IDE LISTA SVIH GRANA -->    
          </div> 

        </div>  

      </div>        
      
      <div class="cit_tab_box" style="display: none; margin-bottom: 50px; padding-bottom: 50px;" >

        <div class="row">
        <div class="col-md-12 col-sm-12" style="padding-top: 30px; padding-left: 40px; padding-right: 40px;">
        
          <h4>Kako kopirati sve potrebne filove sa kompa na lokalni server ?</h4>
          U folderu gdje je projekt (VEL_ERP) u treminalu pokreni:
          npm run copy_to_server
          <br>
          <br>
          NAKON TOGA SE ULOGIRAJ NA SA SSH NA LOKALNI SERVER ssh toni@192.168.88.112 <br>
          
          1. sa sudo su postati root <br>
          2. zatim u folderu /var/www/erp.velprom.hr/ <br>
          3. pokreni: <br>
          4. chmod -R 777 ./  <br>
          5. (i za svaki slučaj pokreni još) chmod -R 777 * 
          
         
          <br>
          <br>
         
          <h4>Kako napraviti ssh ključ da izbjegneš svaki put login ?</h4>
          1. Ako NEMAŠ svoj generirani ključ: <br>
          ssh-keygen -t rsa
          <br>
          <br>
          
          2. Ako imaš svoj ssh ključ onda preskoči prvi korak i samo: <br>
          ssh-copy-id toni@192.168.88.112

          <br>
          <br>
          
          <h4>User na serveru 192.168.88.112 (root user) </h4>
          toni
          <br>
          <br>
          
          <h4>Pass za ssh na 192.168.88.112 (root user) </h4>
          t0n1
          <br>
          <br>
         
          <h4>Pokrenuti forever demon za rad node servera </h4>
          1. sa sudo su postati root <br>
          2. zatim u folderu /var/www/erp.velprom.hr/ <br>
          3. pokreni: <br>
          4. forever start -o out.log -e err.log server.js
          <br>
          Ako želiš pokrenuti forever ali u debug modu ... i debugirati na chrome dev tools: <br>
          forever -w -c 'node --inspect' server.js <br>
          
          <br>
          <br>

          <h4>Kako ugasiti forever demon </h4>
          1. sa sudo su postati root <br>
          2. zatim u folderu /var/www/erp.velprom.hr/ <br>
          3. pokreni: <br>
          4. forever list <br>
          5. Pogledaj na početku reda broj koji stoji u uglattoj zagradi npr [0] <br>
          6. Taj broj upiši u naredbu: <br>
          7. forever stop 0 ----- ovo je broj iz prethodne točke
          <br>
          <br>
          
          <h4>Za development koristim nodemon za rad node servera </h4>

          <br>
          <code style="font-size: 20px;">nodemon --ignore './public/*' --inspect ./server.js</code>
          <br>
          <br>
          S ovom naredbom mu kažem da ne osvježi server ako se napravi bilo koja promjena u public folderu
          
          <br>
          <br>
          
          <h4>Ako želim debuggirati MRAV server i u isto vreme IB server, ali lokalno </h4>
          <br>
          Pokrenem naredbu za MRAV server (moram biti u root folderu od MRAV aplikacije): <br>
          <code style="font-size: 20px;">node --inspect-brk=127.0.0.1:9900 server.js</code>
          <br>
          <br>
          Pokrenem naredbu za IB server (moram biti u root folderu od IB aplikacije): <br>
          <code style="font-size: 20px;">node --inspect-brk=127.0.0.1:9999 server.js</code>
          <br>
          <br>
          
          Nakon toga moram otići u chrome i otvoriti stranicu: <br>
          <br>
          <b style="font-size: 20px;">chrome://inspect/#devices</b>
          <br>
          <br>          
          Tu će biti dva linka koja otvaraju debugger za ova dva servera !!!
          <br>
          <br>
          
          
          

          <h4>Kako kopirati bazu s localhost na server u lokalnoj mreži http://192.168.88.112:9000/ </h4>
          Prvo napravi dump na svom kompu: <br>
          mongodump --username velprom --password ToNi3007 -d velprom  -o /Users/tonikutlic/TONI/2021/VELPROM/VEL_ERP_MATERIJALI/mongo_baza_bckp <br><br>
          Onda kopiraj na localni server: <br>
          scp -r /Users/tonikutlic/TONI/2021/VELPROM/VEL_ERP_MATERIJALI/mongo_baza_bckp/velprom toni@192.168.88.112:/var/www/mongo_local_dump/ <br><br>
          Zatim na terminalu na serveru: <br>
          mongorestore --host localhost --port 27017 --username 'radmila' --password 'radmila_123!!!' -d velprom /var/www/mongo_local_dump/velprom/ <br><br>
          <br>
          <br>
          
          <h4 style="font-weight: 700; color: darkred;">OBRNUTO Kako kopirati BAZU s ubuntu servera na lokalni komujuter</h4>
          
          U root folderu gdje se nalazi veprom projekt ( na istom levelu gdje je package.json) napisati naredbu u terminalu: <br>
          <br>
          <code style="font-size: 20px;">npm run copy_ubuntu_db 2022_12_29_velprom_upuntu</code>
          <br>
          <br>
          Naravno stavi svoje ime za kopiju baze umjesto : 2022_12_29_velprom_upuntu <br>
          Obavezno je staviti neko ime i pazi da se ne ponavlja !!!! <br>
          <br>
          Tko bude radio ovo treba promjeniti pathove po svojoj želji u 
          <pre style="margin: 0 10px; display: inline; background: black; color: white; padding: 4px; border-radius: 4px;">bin/copy_ubuntu_db.sh</pre>
          (to je lokalno) <br>
          <br>
          i ako želiš i trebaš promjeniti pathove na serveru u 
          <pre style="margin: 0 10px; display: inline; background: black; color: white; padding: 4px; border-radius: 4px;">/home/toni/db_dumb.sh</pre>
           (to je na serveru) <br>
           
           <br>
           NAPOMENA: Da bi ovo radilo moraš kopirati ssh key kako sam gore napisao (  ....ssh-copy-id toni@192.168.88.112 )
          
          
          
          <br>
          <br>
          <br>
          
          <div style="background: #1d1b27; color: #fff; padding: 20px; float: left; width: 100%; height: auto; border-radius: 6px;">

            <br>
            <h4 style="font-size: 20px;">STARO ----> RUČNO KOPIRANJE BAZE SA UBUNTU SERVERA NA SVOJ KOMJUTER</h4>

            PRVO NAPRAVITI MONGODUMP NA SERVERU: <br>
            mongodump --username radmila --password 'radmila_123!!!' -d velprom  -o /home/toni/mongo_baza_bckp <br>


            To će napraviti folder "velprom" unutar foldera koji napišeš na kraju gornje linije <br>


            <b style="color: red;">TREBAŠ PAZITI DA VEĆ NE POSTOJI "velprom" FOLDER JER ĆE GA GORNJA LINIJA PREPISATI (ako ti treba sadržaj tog foldera)</b><br>


            Preimenuj velprom folder u folder s datumom - na primjer 2021_11_15_velprom_ubuntu<br>

            <b>Zatim sa terminala na LOKALNOM KOMPU </b> kopiraj sadržaj tog foldera na lokalni komp sa scp komandom:<br>

            scp -r toni@192.168.88.112:/home/toni/mongo_baza_bckp/2021_11_15_velprom_ubuntu /Users/tonikutlic/TONI/2021/VELPROM/VEL_ERP_MATERIJALI/mongo_baza_bckp <br>
            <br>
            <br>
            
          </div>
          <br style="clear: both;">
          <br>
          
          <h4>Koristim ovaj program za spajanje na mngodb bazu:</h4>
          <a href="https://robomongo.org/" target="_blank">ROBO 3T</a>
          <br>
          <br>
          !!! ----> Naravno trebaš napraviti lokalnu verziju baze u terminalu i trebaš dodjeliti usera velprom za tu bazu: <br>
          
<pre>
db.createUser({
  user: "velprom",
  pwd: "ToNi3007",
  roles: [{
    role: "readWrite",
    db: "velprom"
  }]
})
</pre>
          
          <br>
          <br>
          
          Za spajanje na bazu kroz ovaj program koristi username i pass koji piše iznad. <br>
          IZABRATI AUTH MECHANISM: SCRAM-SHA-1 <br>
          <br>
            
          1. Otvoriti lokalnu verziju baze kroz ovaj program <br>
          2. U prozoru u ROBO 3T na početku kopiraj ovu liniju ( da obrišeš sve kolekcije u bazi tj da ba bude posve prazna, ali da kolkecije OSTANU) : <br>
          
          db.getCollectionNames().forEach(function(c) { if (c.indexOf("system.") == -1) db.getCollection(c).deleteMany({}); })<br>
          <br>
          <br>
          Ako želiš sačuvati neku od kolekcije kao na primjer users !!! <br><br>


<pre style="margin: 0;">
<code>
db.getCollectionNames().forEach(function(db_name) {
  if ( db_name.indexOf("system.") == -1 && db_name.indexOf("users") == -1 ) {
    db.getCollection(db_name).deleteMany({});
  }
}) 
</code>
</pre>
   
         <b style="color: darkred;">NARAVNO TREBAŠ U FOLDERU SA EXPORTIRANIM FILOVIMA IZ BAZE, OBRISATI FILOVE KOJE NE ŽELIŠ IMPORTIRATI </b><br>
         na primjer ako ne želiš importirati users collection onda obriši files: <br>
         users.bson <br>
         users.metadata.json <br>
         
          
         <br>
          
          
          
          <br>
          Trebalo bi pisati ispod: <br>
          <b>Script executed sucsessfully .....itd</b> <br>
          
          Zatim sa terminala na LOKALNOM KOMPU napravi ovo: <br>
          
          mongorestore --host localhost --port 27017 --username 'velprom' --password 'ToNi3007' -d velprom /Users/tonikutlic/TONI/2021/VELPROM/VEL_ERP_MATERIJALI/mongo_baza_bckp/2021_11_15_velprom_ubuntu/
          <br>
          <br>
          I to je to  - sada imaš kopiju baze sa servera na lokalnom kompu : ) !!!! <br>
          <br>
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          <h4>Lozinka za admin usera za bilo koju bazu na 192.168.88.112 ?</h4>
          
<pre>
 db.createUser({
  user: "velprom_ubuntu",
  pwd: "ToNi3007",
  roles: [{
    role: "userAdminAnyDatabase",
    db: "admin"
  }]
})
</pre>

          <br>
          <br>
          
          
          <h4>Kako se spojiti sa remote desktop aplikacijom na server</h4>
          
          Ja koristim VNC VIEWER <br>
          <br>
          Ako VNC server nije instaliran na serveru treba ga naravno instalirati:<br>
          <br>
          
          <a href="https://www.snel.com/support/install-vnc-ubuntu-16-04/" target="_blank">UPUTE ZA INSTALACIJU VNC SERVERA</a> <br>
          <br>
          <a href="https://linuxize.com/post/how-to-install-and-configure-vnc-on-ubuntu-20-04/" target="_blank">UPUTE ZA INSTALACIJU 2.</a> <br>
          Treba definirati password za VNC tako da unutar SSH na terminalu upišeš vncpasswd i server će te tražiti da upišeš lozinku. <br>
          
          <br>
          <br>
          Prije spajanja na server sa VNC treba sa SSH otići na server i napraviti sljedeće. <br>
          
          Ubiti ako postoji bilo koja već pokrenuta sesija: <br>
          vncserver -kill :1
          <br>
          <br>
          Zatim ponovo pokrenuti server: <br>
          vncserver
          <br>
          <br>
          Naravno na svom kompu treba instalirati VNC VIEWER po svom izboru <br>
          Treba ga postaviti da se spoji na: <br>
          
          <h5>192.168.88.112:5901</h5>
          
          i naravno treba u programu upisati lozinku za spajanje koju sam prije definirao kod instaliranja vnc servera <br>
          <br>
          <br>
           

        
          <h4>Kako provjeriti jel se mongodb server za bazu pokrenuo</h4>
        
          sudo systemctl status mongod
          <br>
          <br>
          
          <h4>Kako pokrenuti mongodb servis/server</h4>
        
          sudo systemctl start mongod
          
          <br>
          <br>
          
          
          
          <h4>Kako napraviti autentikaciju u terminalu na serveru 192.168.88.112 ?</h4>
          Prvo treba: <br>
          db.auth("velprom_ubuntu", "ToNi3007") <br>
          zatim:<br>
          use velprom <br>
          
          <br>
          <br>
          
         
          <h4>Kako sam kreirao usera za pristup na velprom bazu na 192.168.88.112 ? </h4>   
          
<pre>
db.createUser({
  user: "radmila",
  pwd: "radmila_123!!!",
  roles: [{
    role: "readWrite",
    db: "velprom"
  }]
})
</pre>


        <h4>Kako koristi mongodb u terminalu ? </h4>   
        
        pokreni u terminalu kada si ulogiran s ssh na 192.168.88.112 : <br>
        mongo <br>
        <br>
        use velprom <br>
        <br>
        zatim: <br>
        db.auth('radmila', 'radmila_123!!!') <br>
        <br>
        <br>
        <h4>Kako napraviti mount i umount mrav foldera na NAS-u ?</h4>
        
        sudo mount -t cifs -o username=MRAV,password=eukaliptus69 //192.168.88.205/mrav /mnt/mrav <br>
        <br>
        sudo umount /mnt/mrav/ <br>
        <br>
        <br> 
         
        <h4>Kako napraviti SYM LINK koji vodi na mrav folder na NAS-u ?</h4>
         
        sudo ln -s /mnt/mrav/docs /var/www/erp.velprom.hr/public/docs <br>
        <br>
        <b style="color: darkred;">napomena VAŽNO !!!!:</b> <br>
        /mnt/mrav/docs -----> treba biti kreiran prije naredbe<br>
        /var/www/erp.velprom.hr/public/docs  -----> folder ne treba biti kreiran prije naredbe<br>
        
        <br>
        <br>  
          
        <h4>Kako konfigurirati mongodb bazu ?</h4>
        
            
        OVO JE SADRŽAJ <b style="color: darkred;">/etc/mongod.conf</b> i tako treba biti da baza radi :) :<br>
        <br>
        <br>
      
                                

<pre>
# mongod.conf

# for documentation of all options, see:
#   http://docs.mongodb.org/manual/reference/configuration-options/


# Where and how to store data.
storage:
  dbPath: /var/lib/mongodb
  journal:
    enabled: true
#  engine:
#  mmapv1:
#  wiredTiger:

# where to write logging data.
systemLog:
  destination: file
  logAppend: true
  path: /var/log/mongodb/mongod.log

# network interfaces
net:
  port: 27017
  bindIp: 0.0.0.0


# how the process runs
processManagement:
  timeZoneInfo: /usr/share/zoneinfo

#security:
security:
    authorization: "enabled"

#operationProfiling:


#replication:

#sharding:

## Enterprise-Only Options:

#auditLog:

#snmp: 
</pre>     
      <br>
      


<h4>Kako instalirati wkhtmltopdf ?  ----> to je CLI program za generiranje PDF dokumenata </h4>


# Uncomment the next line if you have installed wkhtmltopdf<br>
# sudo apt remove wkhtmltopdf<br>
cd ~<br>
wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz<br>
sudo tar xvf wkhtmltox-0.12.4_linux-generic-amd64.tar.xz<br>
sudo mv wkhtmltox/bin/wkhtmlto* /usr/bin/<br>
#create simple html test file<br>
echo "<html><body>test</body></html>" >> test.html<br>
#perform conversion<br>

sudo wkhtmltopdf  --disable-smart-shrinking --enable-external-links --enable-internal-links test.html test.pdf<br>
<br>
... ili ...<br>
<br>
wkhtmltopdf --enable-local-file-access --disable-smart-shrinking ./neki_file_na_kompu.html test.pdf<br>
<br>
<br>

<h4>Ponekad se chrome ne otvara na ubuntu mašini? ( kada sam na VNC )</h4>

      
ps -A|grep chrome<br>
kill -9  process_number <br>
<br>
PRIMJER:<br>
<br>
toni@komp:~$ ps -A|grep chrome<br>
9855 ? 00:00:00 chrome<br>
9864 ? 00:00:00 chrome<br>
9868 ? 00:00:00 chrome<br>
9888 ? 00:00:00 chrome<br>
9891 ? 00:00:00 chrome<br>
toni@komp:~$ kill -9 9855<br>
    
      
        
<br>
<br>       
    
<h4>TRELLO ZA MRAV</h4>          

Tu se nalaze svi zadaci koje treba napraviti ili su napravljeni  <br>
            
<a href="https://trello.com/b/6wiQ7AYm/mrav" target="_blank">TRELLO MRAV</a> <br>
<br>
email: velprom.tonikutlic@gmail.com <br>
<br>
pass: ToNi3007



<br>
<br>
<br>
<br>
<br>


<h4>OSTALO (nebitno za rad - samo podsjetnik za mene)</h4>

Kako provjeriti koliko linija koda ima u nekom folderu (rekurzivno):<br>
Prvo odeš u folder koji želiš provjeriti i onda: <br>
<br>

<code>
find . -name '*.js' | xargs wc -l
</code>

<br>
<br>
Ja obično radim s filovima u folderu public/modules/, public/js/ i api/, i vrste filova su *.js, *.scss, i *.html <br>
Treba malo kemijati (privremeno prebaciti neke velike filove ili library-je prije pokretanja naredbe ...pa ih kasnije vratiti nazad) <br>
<br>
<br>

15.09.2022 ---- Za sada imam cca 85.000 linija koda u projektu !!!
       
 

       
       
        </div>
      </div>
      
      </div>
      
      
    </div>
    
  </section>
  
</div>

`;



  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };
  
  wait_for( `$('#${data.id}').length > 0`, function() {
    
    console.log(data.id + 'component injected into html');
    toggle_global_progress_bar(false);
    
    $('.cit_tooltip').tooltip();
    
    $(`html`)[0].scrollTop = 0;
    
    
    $(`#cit_page_title h3`).text(`Admin`);
    document.title = "ADMIN VELPROM";
    
    
    ask_before_close_register_event();
    
    
    
    this_module.make_all_admin_statuses(data);
    

    
    $(`#`+rand_id+`_cat`).data('cit_props', {
      desc: 'odabir kategorije statusa na admin page',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'status_cat',
      cit_run: this_module.choose_status_cat,
    }); 
    
    
    $('#'+rand_id+'_dep').data('cit_props', {
      desc: `odaberi ODJEL koji treba odgovoriti na određeni satatus ----> unutar ADMIN modula ${rand_id}`,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'deps',
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( !window.current_admin_status ) window.current_admin_status = {};
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          window.current_admin_status.dep = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          window.current_admin_status.dep = state;
        };
        
      },
      
    });    
    


    $('#'+rand_id+'_work_done_for').data('cit_props', {
      
      desc: 'odabir _work_done_for  unutar admin page za id ' + current_input_id,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: window.cit_local_list.status_conf,

      filter: null,
      cit_run: function(state) {

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( !window.current_admin_status ) window.current_admin_status = {};

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          window.current_admin_status.work_done_for = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          window.current_admin_status.work_done_for = state.sifra; // upisaujem samo sifru jer ne želim bezveze povećati veličinu jsona : )
        };

      },

    });



    
    $(`#save_admin_conf`).off('click');
    $(`#save_admin_conf`).on('click', this_module.save_admin_conf );

    
    $(`#save_admin_status`).off('click');
    $(`#save_admin_status`).on('click', this_module.save_admin_status );
        
    $(`#update_admin_status`).off('click');
    $(`#update_admin_status`).on('click', this_module.save_admin_status );
    
    
    
    $('#'+rand_id+'_is_work').data(`cit_run`, function(state) { 
      if ( !window.current_admin_status ) window.current_admin_status = {};
      window.current_admin_status.is_work = state;
      console.log(window.current_admin_status);
    });
    
    $('#'+rand_id+'_active').data(`cit_run`, function(state) { 
      if ( !window.current_admin_status ) window.current_admin_status = {};
      window.current_admin_status.active = state;
      console.log(window.current_admin_status);
    });
    
    
    $(`#${data.id} .cit_input.number`).off(`blur`);
    $(`#${data.id} .cit_input.number`).on(`blur`, function() {
      
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      var prop = this.id.replace( rand_id+'_', '');
      
      if ( !window.current_admin_status ) window.current_admin_status = {}
      if ( !window.current_admin_status[prop] ) window.current_admin_status[prop] = null;
      
      set_input_data(this, window.current_admin_status, rand_id  );
    });
    
    
    
    $(`#${data.id} .cit_input.string`).off(`blur`);
    $(`#${data.id} .cit_input.string`).on(`blur`, function() {
 
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      var prop = this.id.replace( rand_id+'_', '');
      
      if ( !window.current_admin_status ) window.current_admin_status = {}
      if ( !window.current_admin_status[prop] ) window.current_admin_status[prop] = null;
      
      set_input_data( this, window.current_admin_status, rand_id );
    });
    

    
    this_module.make_status_grane_list(data);
        
    
    

    
  }, 50*1000 );

  return {
    html: component_html,
    id: data.id
  };

},
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};

  
  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
  };
  this_module.set_init_data = set_init_data;
  
  
  function save_admin_status() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    
    var max_status_num = 0
    $.each(data.status_conf, function(st_ind, status) {
      if ( max_status_num <= status.status_num ) max_status_num = status.status_num;
    });


    if ( !window.current_admin_status ) window.current_admin_status = {};

    if ( !window.current_admin_status.sifra ) window.current_admin_status.sifra = `IS`+cit_rand();
    
    if ( typeof window.current_admin_status.is_work == "undefined" ) window.current_admin_status.is_work = null;
    if ( typeof window.current_admin_status.active == "undefined" ) window.current_admin_status.active = true;
    
    if ( typeof window.current_admin_status.next_statuses == "undefined" ) window.current_admin_status.next_statuses = [];
    if ( typeof window.current_admin_status.grana_finish == "undefined" ) window.current_admin_status.grana_finish = [];
    
    var deadline_value = $('#'+rand_id+`_def_deadline`).val();
    
    window.current_admin_status.def_deadline = cit_number( deadline_value );
    
    if ( 
      deadline_value !== "" 
      &&
      $.isNumeric( deadline_value ) == false ) {
      console.log(` ---------------- nije broooooooooj ----------------  `);
      return;
    };
    


    if ( $('#'+rand_id+`_status_num`).val() == "" ) {
      window.current_admin_status.status_num = max_status_num + 100; 
      $('#'+rand_id+`_status_num`).val( window.current_admin_status.status_num );
    } else {
      window.current_admin_status.status_num = cit_number( $('#'+rand_id+`_status_num`).val() );
    };
  
    
    if ( 
      $('#'+rand_id+`_naziv`).val() == "" 
      ||
      $('#'+rand_id+`_status_num`).val() == "" 
      || 
      !window.current_admin_status.cat
      ||
      !window.current_admin_status.dep
    ) {
      popup_warn(`Potrebno je upisati sve podatke za status !!!!`);
      return;
    };
    

    if ( this.id == `update_admin_status` ) {

      var updated_status = cit_deep(window.current_admin_status);
      
      $.each( this_module.cit_data[this_comp_id].status_conf, function( s_ind, status ) {

        if ( status.next_statuses?.length > 0 ) {
          $.each( status.next_statuses, function( n_ind, next ) {
            if ( next.sifra == updated_status.sifra ) {

              updated_status.next_status_num = next.next_status_num;

              // zamjeni stati status s update - anim statusom
              this_module.cit_data[this_comp_id].status_conf[s_ind].next_statuses[n_ind] = updated_status;
            }
          });
        };


        if ( status.grana_finish?.length > 0 ) {
          $.each( status.grana_finish, function( f_ind, finish ) {
            if ( finish.sifra == updated_status.sifra ) {

              updated_status.next_status_num = finish.next_status_num;
              // zamjeni stati status s update - anim statusom
              this_module.cit_data[this_comp_id].status_conf[s_ind].grana_finish[f_ind] = updated_status;
            }
          });
        };

      });

    
    }; // kraj ako je ovo update statusa
    
    

    data.status_conf = upsert_item(data.status_conf, window.current_admin_status, `sifra`);

    var criteria = [ 'status_num' ];
    multisort( data.status_conf, criteria );

    
    this_module.make_all_admin_statuses(data);

    this_module.make_status_grane_list(data);
    


    if ( this.id == `update_admin_status` ) {
      $(this).css(`display`, `none`);
      $(`#save_admin_status`).css(`display`, `flex`);
    }; // kraj ako je button update
    
    
    
    window.current_admin_status = null;

    
    toggle_cit_switch( true, $('#'+rand_id+`_active`) );
    
    $('#'+rand_id+`_status_num`).val("");
    $('#'+rand_id+`_naziv`).val("");
    
    toggle_cit_switch( false, $('#'+rand_id+`_is_work`) );
    
    $('#'+rand_id+`_def_deadline`).val("");
    $('#'+rand_id+`_cat`).val("");
    $('#'+rand_id+`_dep`).val("");
    $('#'+rand_id+`_work_done_for`).val("");
    

  };
  this_module.save_admin_status = save_admin_status;
  


  function make_all_admin_statuses(data) {
    
    $(`#all_statuses_list_box`).html(``);
    
    
    
    var false_pill_style = 
`
padding: 0 5px;
background: red;
display: flex;
align-items: center;
color: #fff;
border-radius: 10px;
`;
    
    
    if (data.status_conf && data.status_conf.length > 0 ) {
      
      $.each(data.status_conf, function(s_ind, status) {
        
        var curr_status = cit_deep(status);
        var status_place = check_status_place(data.status_conf, curr_status);
        
        var is_next_status = status_place.next;
        var is_finish_status = status_place.finish;
        var has_children = status_place.has_children;

        var btn_disable = "";
        
        if ( 
          has_children                    ||
          is_next_status                  || 
          is_finish_status                ||
          curr_status.sifra == "ISerror"  ||
          !curr_status.active           
        ) btn_disable = `cit_disable`;
        
                
        data.status_conf[s_ind].nova_grana = 
        `<button class="blue_btn btn_small_text add_nova_grana ${btn_disable}" style="margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;">
          <i class="fas fa-plus" style="margin-right: 10px;" ></i> NOVA GRANA
        </button>`;
        
        // dodaj samo string od kategorije
        data.status_conf[s_ind].cat_naziv = data.status_conf[s_ind].cat?.naziv;
        
        // dodaj samo string naziva od odjela
        data.status_conf[s_ind].dep_naziv = data.status_conf[s_ind].dep?.naziv;
        
        data.status_conf[s_ind].active_status = data.status_conf[s_ind].active;
          
        if ( data.status_conf[s_ind].active ==  false) {
          data.status_conf[s_ind].active_status  = `<span style="${false_pill_style}">${data.status_conf[s_ind].active}</span>`;
        };

        
        if ( status.work_done_for ) {
          data.status_conf[s_ind].work_done_for_naziv = this_module.find_work_done_naziv(status.work_done_for, data);
        } else {
          data.status_conf[s_ind].work_done_for = null;
          data.status_conf[s_ind].work_done_for_naziv = "";
        };
        
        // ako ne postoji work finished
        if ( !data.status_conf[s_ind].work_finished ) data.status_conf[s_ind].work_finished = null;
        
        
      }); // kraj loopa po svim statusima unutar satus_conf
      
      wait_for( `$(".add_nova_grana").length > 0`, function() {
        
        $(".add_nova_grana").each( function() {
        
          $(this).off(`click`);
          $(this).on(`click`, function(e) {
            
            e.stopPropagation(); 
            e.preventDefault();
            
            
            $(this).addClass(`cit_disable`);
            
            var this_comp_id = $(this).closest('.cit_comp')[0].id;
            var data = this_module.cit_data[this_comp_id];
            var rand_id =  this_module.cit_data[this_comp_id].rand_id;

            var parent_row_id = $(this).closest('.search_result_row')[0].id;
            var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
            var index = find_index(data.status_conf, sifra , `sifra` );
            
            
            this_module.add_new_grana( data, index );
            
          });
          
        });
        
        
        
      }, 5*1000 );
      
      
    };
    
    
    var all_admin_statuses_props = {
      
      desc: 'za kreiranje tablice definiranih statusa u admin sučelju',
      local: true,

      list: data.status_conf,

      show_cols: [
        `active_status`,
        `naziv`,
        `work_done_for_naziv`,
        `status_num`,
        `is_work`, 
        `def_deadline`,
        `cat_naziv`,
        `dep_naziv`,
        'button_edit',
        'nova_grana',
      ],
      
      
      
      custom_headers: [
        `Aktivan`,
        `Naziv`,
        `Kraj posla za`,
        `RB`,
        `Radni status`, 
        `Max sati rada`,
        `Kategorija`,
        `Odjel`,
        'EDITIRAJ',
        'NOVA GRANA',
      ],
      
      // ako postoji property format_cols
      // to znači da taj property treba foramtirati kao broj s tim brojem decimala
      format_cols: { active_status: `center`, 'status_num': "center", 'is_work': "center", 'def_deadline': "center", cat_naziv: "center", dep_naziv: "center"  },
      
      col_widths: [
        1, //`Aktivan`,
        4, // `Naziv`,
        3, // `Kraj posla za`,
        1, // `RB`,
        1, // `Radni status`, 
        1, // `Sati rada`,
        1.5, // `Kategorija`,
        1.5, // `Odjel`,
        1, // 'EDITIRAJ',
        2, // 'NOVA GRANA'
      ],

      parent: `#all_statuses_list_box`,
      
      return: {},
      show_on_click: false,

      cit_run: function(state) { console.log(state); },

      button_edit: function(e) {
        
        e.stopPropagation(); 
        e.preventDefault();

        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var index = find_index(data.status_conf, sifra , `sifra` );

        window.current_admin_status = cit_deep( data.status_conf[index] );


        $('#save_admin_status').css(`display`, `none`);
        $('#update_admin_status').css(`display`, `flex`);
        
        
        toggle_cit_switch( window.current_admin_status.active, $('#'+rand_id+`_active`) );
        $('#'+rand_id+`_status_num`).val( window.current_admin_status.status_num );
        $('#'+rand_id+`_naziv`).val( window.current_admin_status.naziv );
        toggle_cit_switch( window.current_admin_status.is_work, $('#'+rand_id+`_is_work`) );
        $('#'+rand_id+`_def_deadline`).val( window.current_admin_status.def_deadline );
        
        $('#'+rand_id+`_cat`).val( window.current_admin_status.cat?.naziv || "" );
        $('#'+rand_id+`_dep`).val( window.current_admin_status.dep?.naziv  || "");
        
        if ( window.current_admin_status.work_done_for ) {
          var work_done_for_naziv = this_module.find_work_done_naziv(window.current_admin_status.work_done_for, data);
        }
        $('#'+rand_id+`_work_done_for`).val( work_done_for_naziv || "");
      
        cit_scroll( $("#new_admin_status_row"), -200, 200);
        
      },
      
      /*
        button_delete: async function(e) { 
        
        e.stopPropagation(); 
        e.preventDefault();
            
        var this_button = this;
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          data.status_conf = delete_item(data.status_conf, sifra , `sifra` );
          
          $(`#`+parent_row_id).remove();

        };
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj Status`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj Status?`, null, 'yes/no', delete_yes, delete_no, null);

      
      },
      */
    };
    
    
    if (data.status_conf && data.status_conf.length > 0 ) {
      
      var criteria = [ 'status_num' ];
      multisort( data.status_conf, criteria );  
      
      create_cit_result_list(data.status_conf , all_admin_statuses_props );
    };
    
  };
  this_module.make_all_admin_statuses = make_all_admin_statuses;
  
  
  
  function find_work_done_naziv(work_done_sifra, data) {
    
    var work_done_for_naziv = "";
    if ( work_done_sifra ) {
      $.each(data.status_conf , function( s_index, status ) {
        if (status.sifra == work_done_sifra ) work_done_for_naziv = status.naziv;
      });
    };
    
    return work_done_for_naziv;
    
  };
  this_module.find_work_done_naziv = find_work_done_naziv;
  
  
  
  function add_new_grana(data, status_conf_index) {
    
    
    if ( $.isArray( data.status_conf[status_conf_index].grana_finish ) == false  ) data.status_conf[status_conf_index].grana_finish = [];
    
    var max_status_num = 0
    $.each(data.status_conf[status_conf_index].grana_finish, function(f_ind, finish) {
      if ( max_status_num <= finish.next_status_num ) max_status_num = finish.next_status_num;
    });
    max_status_num += 50;

    
    var error_status = {
      
      "sifra" : "ISerror",
      "active" : true,
      "next_status_num" : max_status_num,
      "naziv" : "GREŠKA UPISA",
      "next_statuses" : [],
      "grana_finish" : [],
      "is_work" : null,
      "def_deadline" : null,
      "cat" : { "sifra" : "preprod", "naziv" : "PRED-PROIZVODNJA" },
      "next_statuses_string" : "",
      "grana_finish_string" : ""
      
    };
    
    // odmah uguraj error u finish granu ----> to je za sve grane UVIJEK !!!!!
    // odmah uguraj error u finish granu ----> to je za sve grane UVIJEK !!!!!
    data.status_conf[status_conf_index].grana_finish.push(error_status);

    
    this_module.make_status_grane_list(data);
    
    
    
    
    setTimeout( function() {
      
      $(`#admin_module .cit_tab_btn`).eq(1).trigger(`click`);
    
      setTimeout( function() {
        
        // u startu uzimam da mi je novi red za granu na kraju liste -----> ALI NE MORA BITI JER TO OVISI O REDNOM BROJU STATUSA !!!!!!
        var goto_created_row = $("#grane_list_box .search_result_row").last(); 

        // usporedi idjeve od redova i kada naletiš na id koji u seb ima ID od novo krairanog početka grane
        $("#grane_list_box .search_result_row").each( function() {
          if ( this.id.indexOf( `_list_item_`+data.status_conf[status_conf_index].sifra ) > -1 ) goto_created_row = $(this);
        });
        
        cit_scroll( goto_created_row, -100, 200);
        
        setTimeout( function() { goto_created_row.addClass(`blink_bg`);  }, 500);
        setTimeout( function() { goto_created_row.removeClass(`blink_bg`);  }, 2500);
        
        
        
      }, 100 ); // čakaj da klikne na tab
      
    }, 200 ); // čekaj da kreira listu grana
      
    
    
  };
  this_module.add_new_grana = add_new_grana;
  
  

  function choose_status_cat(state) {
    
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  data.rand_id;
    
    
    if ( state == null ) {
      $('#'+current_input_id).val("");
      if ( window.current_admin_status ) window.current_admin_status.cat = null;
      return;
    };
    
    if ( !window.current_admin_status ) window.current_admin_status = {};
    $('#'+rand_id+`_cat`).val(state.naziv);
    window.current_admin_status.cat = state;
    
    
  };
  this_module.choose_status_cat = choose_status_cat;
  
  
  
  function make_status_grane_list(data) {
    
  
    var valid = this_module.valid;
    
    var grane_for_table = [];
        
    $.each(data.status_conf , function(e_index, status ) {
      
      // ovo je primjer status conf objekta
      /*
        {
            "sifra" : "IS1",
            "active" : true,
            "status_num" : 100,
            "naziv" : "UPIT KUPCA",
            "next_statuses" : [],
            "grana_finish" : [ 
                {
                    "sifra" : "IS38",
                    "active" : true,
                    "naziv" : "KUPAC POTVRDIO PONUDU",
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_status_num" : 20
                }, 
                {
                    "sifra" : "IS72",
                    "active" : true,
                    "naziv" : "PROIZVOD GOTOV I PREUZET",
                    "next_statuses" : "",
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "next_status_num" : 40
                }
            ],
            "is_work" : null,
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "next_statuses_string" : "",
            "grana_finish_string" : "IS38,IS72"
        }, 

      */

      var curr_status = cit_deep(status);
      
      var status_place = check_status_place(data.status_conf, curr_status);
      var is_next_status = status_place.next;
      var is_finish_status = status_place.finish;
      var has_children = status_place.has_children;
      
      if (
        curr_status.active == true
        &&
        is_next_status == false
        &&
        is_finish_status == false
        &&
        has_children == true
        
      ) {

        var status_obj = {};

        $.each(status , function(status_key, value ) {


          var grana_id = "grana" + cit_rand();


          var elem_html = "";

          this_module.input_styles = {

            "active": "text-align: center;",
            "status_num": "text-align: center;",
            "naziv": "text-align: center;",

            "next_status_num": 
`
text-align: center;
border: 1px solid #ccc;
margin: 0;
padding: 0;
width: calc(100% - 2px);
height: 24px;
border-radius: 9px;
`,
            "choose_next_status": 
`
text-align: center;
border: 1px solid #ccc;
margin: 0;
padding: 0;
width: calc(100% - 2px);
height: 24px;
border-radius: 9px;
`,
            "is_work": "text-align: center;",
            "def_deadline": "text-align: center;",
            "cat": "text-align: center;",
          };

          var special_cols = {
            next_statuses: this_module.generate_next_statuses_html,
            grana_finish: this_module.generate_grana_finish_html,

          };


          if ( status_key !== "sifra" ) { // nemoj raditi nikakve elemente za sifru

            // if ( valid[status_key] ) { 
              // ako postoji valid za ovaj objekt

              if ( special_cols[status_key] ) {
                elem_html = special_cols[status_key](value, grana_id, this_module.input_styles);
              }

              /*
              else if ( valid[status_key].element == "switch" || valid[status_key].element == "check_box" ) {
                // AKO JE SWITCH
                elem_html = cit_comp(grana_id+`_active`, valid.active, data.active || false) 
              } 
              */

              else if ( valid[status_key]?.element == "single_select" ) {
                
                // AKO JE SINGLE SELECT TJ DROP LIST
                elem_html = cit_comp(
                  
                  status.sifra+`_`+status_key,                // sifra
                  valid[status_key],                          // objekt za validaciju
                  value || null,                              // data
                  value?.naziv || "",                         // naziv
                  this_module.input_styles[status_key] || ""  // style 
                  
                );
                
              } 

              /*
              else {
                // AKO JE BILO ŠTO DRUGO
                elem_html =  
              };
              */

            // }; // kraj ako postoji valid

          }; // kraj ako nije sifra

          status_obj[status_key] = elem_html || value;

        }); // kraj loopa po svim propertijim elementa

        grane_for_table.push(status_obj);

      }; // kraj ako status ima next_statuses  ----> to ga čini granom :)
      
    }); // kraj loopa po svim arg elementima
    
    
    var grane_props = {
      
      desc: `samo za kreiranje tablice svih grana statusa  u modulu admin` ,
      local: true,
      
      list: grane_for_table,
      
      show_cols: [
        `naziv`,
        `status_num`,
        `cat_naziv`,
        `next_statuses`,
        `grana_finish`,
      ],
      
      custom_headers: [
        "Naziv",
        "RB",
        "Kategorija",
        "Statusi u grani",
        "Kraj grane",
      ],
      
      /* format_cols: { rok: "date", start: "date", end: "date" }, */
      col_widths: [
        3, // "Naziv",
        1, // "RB",
        2, // "Kategorija",
        6, // "Statusi u grani",
        6, // "Kraj grane",
      ],
   
      format_cols: { status_num: `center`,  },
      
      parent:  `#grane_list_box` , // `
      return: {},
      show_on_click: false,
      cit_run: function(state) { console.log(state); },
    
    };
  
    if (grane_for_table.length > 0 ) {
      
      $(`#grane_list_box`).html("");
      
      create_cit_result_list(grane_for_table, grane_props );
      
      wait_for(`$("#grane_list_box .search_result_row").length > 0`, function() {
        
        remove_sort_arrows( $(`#grane_list_box`) );
        
        this_module.register_grane_events(data);

        data.status_conf_events_registered = true;
        
        
      }, 5*1000 );
      
      
      
      
      
    } else {
      // ako nema elemenata onda odmah trigeriraj da su eventi registrirani
      data.status_conf_events_registered = true;
    };
    
  };
  this_module.make_status_grane_list = make_status_grane_list;
  
  
  function generate_next_statuses_html (next_statuses, grana_id, input_styles) {
    
    var valid = this_module.valid;
            
    var criteria = [ 'next_status_num' ];
    multisort( next_statuses, criteria );

    var all_next_statuses = `

      <div style="display: flex; width: 100%; font-size: 12px;">

        <div style="width: 10%;"> ${ cit_comp(grana_id+`_next_status_num`, valid.next_status_num, "", "", this_module.input_styles.next_status_num ) }</div>
        <div style="width: 90%;" class="choose_next_status_box"> ${ cit_comp(grana_id+`_choose_next_status`, valid.choose_next_status, null, "", this_module.input_styles.choose_next_status  ) } </div>
        
        <!--    
        <div style="width: 10%;"> 
          <button class="blue_btn btn_small_text add_next_status_btn" 
                  style="margin: 0 auto;
                  padding: 0 7px;
                  height: 22px;
                  width: auto;
                  box-shadow: none;">
            <i class="fas fa-plus"></i>
          </button> 
        </div>
        -->
        
      </div>
    `;


    $.each(next_statuses, function(ind, next_status) {
      var next_status_html = 
      `

      <div class="next_status_row" style="display: flex; width: 100%; font-size: 12px; margin: 3px 0; align-items: center;">
        <div style="width: 10%;">${next_status.next_status_num}</div>
        <div style="width: 80%;">${next_status.naziv}</div>
        <div style="width: 10%;"> 
          <div class="result_row_delete_btn remove_next_status" data-next_status_sifra="${next_status.sifra}">
            <i class="fas fa-trash-alt"></i>
          </div> 
        </div>
      </div>
      `;

      all_next_statuses += next_status_html;
    });
    var html = `<div class="custom_result_box next_statuses_box" style="background: transparent;" >${all_next_statuses}</div>`;
    return html;  
  };
  this_module.generate_next_statuses_html = generate_next_statuses_html;
  
  
  
  function generate_grana_finish_html (grana_finish, grana_id, input_styles) {
            
    var valid = this_module.valid;
    
    var criteria = [ 'next_status_num' ];
    multisort( grana_finish, criteria );

    var all_grana_finish = `

      <div style="display: flex; width: 100%; font-size: 12px;">

        <div style="width: 10%;"> ${ cit_comp(grana_id+`_next_status_num`, valid.next_status_num, "", "", this_module.input_styles.next_status_num ) }</div>
        <div style="width: 90%;" class="choose_grana_finish_box"> ${ cit_comp(grana_id+`_choose_grana_finish`, valid.choose_grana_finish, null, "", this_module.input_styles.choose_next_status ) } </div>
        
        <!--
        
        <div style="width: 10%;"> 
          <button class="blue_btn btn_small_text add_grana_finish_btn" 
                  style="margin: 0 auto;
                          padding: 0 7px;
                          height: 22px;
                          width: auto;
                          box-shadow: none;">

            <i class="fas fa-plus"></i>

          </button> 
        </div>
        
        -->
        
      </div>
    `;


    $.each(grana_finish, function(ind, grana_obj) {
      var grana_html = 
      `

      <div class="grana_finish_row" style="display: flex; width: 100%; font-size: 12px; margin: 3px 0; align-items: center;">
        <div style="width: 10%;">${grana_obj.next_status_num}</div>
        <div style="width: 80%;">${grana_obj.naziv}</div>
        <div style="width: 10%;"> <div class="result_row_delete_btn remove_grana_finish" data-grana_finish_sifra="${grana_obj.sifra}" ><i class="fas fa-trash-alt"></i></div> </div>
      </div>
      `;

      all_grana_finish += grana_html;
    });
    var html = `<div class="custom_result_box grana_finish_box" style="background: transparent;" >${all_grana_finish}</div>`;
    return html;  
  };
  this_module.generate_grana_finish_html = generate_grana_finish_html;

  
  function register_grane_events(data) {

    $(`#grane_list_box .choose_next_status_box .single_select`).each( function() {

      var next_statuses_parent_box = $(this).closest(`.next_statuses`);

      var this_input = this;
      
      $(this_input).data('cit_props', {
        desc: 'odabir next_statuses_drop_list single_select unutar admin page za id ' + this_input.id,
        local: true,
        show_cols: ['naziv'],
        return: {},
        show_on_click: true,
        list: data.status_conf,
        
        filter: function(list) {
          
          var filtered_list = [];
          
          var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;
          
          var parent_row =  $('#'+current_input_id).closest('.search_result_row');
          var parent_row_id = parent_row[0].id;
          
          var parent_status_sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          
          var index = find_index(data.status_conf, parent_status_sifra , `sifra` );
          
          var parent_status = data.status_conf[index];
           

          $.each(list, function( ind, status ) {
          
            
            var curr_status = cit_deep(status);
            
            var status_place = check_status_place(data.status_conf, curr_status);

            var is_next_status = status_place.next;
            var is_finish_status = status_place.finish;
            var has_children = status_place.has_children;
            
            var already_in_next = false;
            $.each(parent_status.next_statuses, function( n_index, next_obj ) {
              if ( next_obj.sifra == curr_status.sifra ) {
                already_in_next = true;
              };
            });
            
            var already_in_finish = false;
            $.each(parent_status.grana_finish, function( g_index, grana_obj ) {
              if ( grana_obj.sifra == curr_status.sifra ) {
                already_in_finish = true;
              };
            });
            
            if ( !has_children && already_in_next == false && already_in_finish == false ) filtered_list.push(curr_status);
            
          });  
          
          return filtered_list;
          
        },
        cit_run: function(state) {
          
          
          if (state == null) {
            $('#'+current_input_id).val(``);
            return;
          };

          var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $('#'+current_input_id).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          var index = find_index(data.status_conf, sifra , `sifra` );

          var grana_id = current_input_id.split("_")[0];
          var current_prop = current_input_id.replace( grana_id +`_`, ``);

          var next_status_num =  $(`#${grana_id}_next_status_num`).val();

          if (next_status_num == "") {
            popup_warn(`Obavezno upisati redni broj statusa !!!`);
            return;
          };

          if ( cit_number(next_status_num) == null) {
            popup_warn(`Redni broj statusa nije pravilan broj !!!`);
            return;
          };

          state.next_status_num = Number(next_status_num);

          data.status_conf[index].next_statuses = upsert_item(data.status_conf[index].next_statuses, state, `sifra`);

          var criteria = [ 'next_status_num' ];
          multisort( data.status_conf[index].next_statuses, criteria );

          var next_statuses_html = this_module.generate_next_statuses_html(data.status_conf[index].next_statuses, grana_id, this_module.input_styles)
          next_statuses_parent_box.html(next_statuses_html);

          console.log(state);
          
          // opet pozovi ovu istu funkciju kako bi osvježio evente za drop list
          this_module.register_grane_events(data);
          
          this_module.make_all_admin_statuses(data);
          

        },
      });

    });
    
    $(`#grane_list_box .choose_grana_finish_box .single_select`).each( function() {

      var grana_finish_parent_box = $(this).closest(`.grana_finish`);

      var this_input = this;
  
      $(this_input).data('cit_props', {
        desc: 'odabir grana_finish single_select unutar admin page ' + this_input.id,
        local: true,
        show_cols: ['naziv'],
        return: {},
        show_on_click: true,
        list: data.status_conf,
        
        filter: function(list) {
          
          var filtered_list = [];
          
          var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;
          
          var parent_row =  $('#'+current_input_id).closest('.search_result_row');
          var parent_row_id = parent_row[0].id;
          
          var parent_status_sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          
          var index = find_index(data.status_conf, parent_status_sifra , `sifra` );
          
          var parent_status = data.status_conf[index];
           

          $.each(list, function( ind, status ) {
          
            
            var curr_status = cit_deep(status);
            
            var status_place = check_status_place(data.status_conf, curr_status);

            var is_next_status = status_place.next;
            var is_finish_status = status_place.finish;
            var has_children = status_place.has_children;
            
            var already_in_next = false;
            $.each(parent_status.next_statuses, function( n_index, next_obj ) {
              if ( next_obj.sifra == curr_status.sifra ) {
                already_in_next = true;
              };
            });
            
            var already_in_finish = false;
            $.each(parent_status.grana_finish, function( g_index, grana_obj ) {
              if ( grana_obj.sifra == curr_status.sifra ) {
                already_in_finish = true;
              };
            });
            
            if ( !has_children && already_in_next == false && already_in_finish == false ) filtered_list.push(curr_status);
            
            
          });  
          
          return filtered_list;
          
        },
        cit_run: function(state) {
          
          
          if (state == null) {
            $('#'+current_input_id).val(``);
            return;
          };
          

          var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $('#'+current_input_id).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          var index = find_index(data.status_conf, sifra , `sifra` );

          var grana_id = current_input_id.split("_")[0];
          
          var current_prop = current_input_id.replace( grana_id +`_`, ``);

          var next_status_num =  $(`#${grana_id}_next_status_num`).val();

          if (next_status_num == "") {
            popup_warn(`Obavezno upisati redni broj statusa !!!`);
            return;
          };

          if ( cit_number( next_status_num ) == null ) {
            popup_warn(`Redni broj statusa nije pravilan broj !!!`);
            return;
          };

          state.next_status_num = Number(next_status_num);

          data.status_conf[index].grana_finish = upsert_item(data.status_conf[index].grana_finish, state, `sifra`);

          var criteria = [ 'next_status_num' ];
          multisort( data.status_conf[index].grana_finish, criteria );

          var grana_finish_html = this_module.generate_grana_finish_html( data.status_conf[index].grana_finish, grana_id, this_module.input_styles)
          grana_finish_parent_box.html(grana_finish_html);

          console.log(state);
          
          // opet pozovi ovu istu funkciju kako bi osvježio evente za drop list
          this_module.register_grane_events(data);
          
          this_module.make_all_admin_statuses(data);

        },
      });

    });

    wait_for(`$("#grane_list_box .remove_next_status").length > 0`, function() {
        
        $("#grane_list_box .remove_next_status").off(`click`);
        $("#grane_list_box .remove_next_status").on(`click`, function(e) {
          
          e.stopPropagation(); 
          e.preventDefault();
          
          var next_status_sifra = $(this).attr(`data-next_status_sifra`);
          var next_status_row = $(this).closest(`.next_status_row`);

          var this_comp_id = $(this).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;
          
          
          var parent_row = $(this).closest('.search_result_row');
          var parent_row_id = parent_row[0].id;
          
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          var index = find_index(data.status_conf, sifra , `sifra` );
          
          data.status_conf[index].next_statuses = delete_item( data.status_conf[index].next_statuses, next_status_sifra , `sifra` );
          
          next_status_row.remove();
          
          if ( data.status_conf[index].next_statuses.length == 0 && data.status_conf[index].grana_finish.length == 0 ) {
            
            parent_row.remove();
            // ponovo generiraj cijelu tablicu sa svim statusima jer se sada GUMB NOVA GRANA ENABLE-ao !!!!
            this_module.make_all_admin_statuses(data);
            
          };
          
          
        });
      }, 5*1000);
  
    wait_for(`$("#grane_list_box .remove_grana_finish").length > 0`, function() {
        
        $("#grane_list_box .remove_grana_finish").off(`click`);
        $("#grane_list_box .remove_grana_finish").on(`click`, function(e) {
          
          e.stopPropagation(); 
          e.preventDefault();
          
          var grana_finish_sifra = $(this).attr(`data-grana_finish_sifra`);
          var grana_finish_row = $(this).closest(`.grana_finish_row`);

          var this_comp_id = $(this).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;
          
          
          var parent_row = $(this).closest('.search_result_row');
          var parent_row_id = parent_row[0].id;
          
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          var index = find_index(data.status_conf, sifra , `sifra` );
          
          data.status_conf[index].grana_finish = delete_item( data.status_conf[index].grana_finish, grana_finish_sifra , `sifra` );
          
          grana_finish_row.remove();
          
          if ( data.status_conf[index].next_statuses.length == 0 && data.status_conf[index].grana_finish.length == 0 ) {
            
            parent_row.remove();
            // ponovo generiraj cijelu tablicu sa svim statusima jer se sada GUMB NOVA GRANA ENABLE-ao !!!!
            this_module.make_all_admin_statuses(data);
          };
          
          
        });
        
      }, 5*1000);

    $(`#grane_list_box .cit_input.string`).each( function(){

      $(this).off(`blur`);
      $(this).on(`blur`, function() {
        
        
        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        var first_underscore = this.id.indexOf(`_`);
        var sifra = this.id.substring(0, first_underscore);
        var key = this.id.substring(first_underscore+1);
        var index = find_index(data.status_conf, sifra , `sifra` );

        if ( index !== null ) {

          console.log(data.status_conf[index]);
          data.status_conf[index][key] = $(this).val() || null;
          console.log(data.status_conf[index]);
        };

      });

    });

    $(`#grane_list_box .cit_input.number`).each( function(){

      $(this).off(`blur`);
      $(this).on(`blur`, function() {

        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        
        var first_underscore = this.id.indexOf(`_`);
        var sifra = this.id.substring(0, first_underscore);
        var key = this.id.substring(first_underscore+1);

        var index = find_index(data.status_conf, sifra , `sifra` );

        if ( index !== null ) {

          set_input_data(
            this, // input elem
            data.status_conf[index], // data parent
            sifra, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
          );
          console.log(data.status_conf[index]);

        };

      });

    });
    
  };
  this_module.register_grane_events = register_grane_events;
  
 
  function save_admin_conf() {

    var this_button = this;

    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    data = save_metadata( cit_deep( this_module.cit_data[this_comp_id]) );
    
    toggle_global_progress_bar(true);

    $(`#save_admin_conf`).css("pointer-events", "none");

    // ----------------------------------------------------------------------------------------------------------------
    // START SAVE TO DB
    // ----------------------------------------------------------------------------------------------------------------

    $.ajax({
      headers: {
        'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
      },
      type: "POST",
      cache: false,
      url: `/save_admin_conf`,
      data: JSON.stringify( data ),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done(function (result) {

      console.log(result);
      if ( result.success == true ) {
        
        cit_toast(`KONFIGURACIJA JE SPREMLJENA!`);
        
        var meta_data = meta_data_html( result.admin_conf );
        $(`#meta_data_box`).html(meta_data);
        
      } else {

        // ako result NIJE SUCCESS = true

        if ( result.msg ) popup_error(result.msg);
        if ( !result.msg ) popup_error(`Greška prilikom spremanja!`);
      };

    })
    .fail(function (error) {

      console.log(error);
      popup_error(`Došlo je do greške na serveru prilikom spremanja ADMIN KONFIGURACIJE!`);
    })
    .always(function() {
      toggle_global_progress_bar(false);
      $(`#save_admin_conf`).css("pointer-events", "all");

    });

    // ----------------------------------------------------------------------------------------------------------------
    // END SAVE TO DB
    // ----------------------------------------------------------------------------------------------------------------


  };
  this_module.save_admin_conf = save_admin_conf;
  
  
  this_module.cit_loaded = true;
 
} // end of module scripts
  
  
};

