var module_object = {
  
create: ( data, parent_element, placement ) => {
  
  var this_module = window[module_url]; 
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };
  
  
  // VAŽNO -----> ovako izgledaju user podaci
  /*
  ---------------------------------------
  
  
  {
  "user_saved" : null,
  "user_edited" : null,


  "saved" : 1674212093657,
  "edited" : 1674212093657.0,
  "last_login" : 1674212093657.0,


  "user_number": 528,

  "user_address" : null,
  "user_tel" : null,

  "name" : "ibris123",
  "pass": "ibris123",
  "full_name" :"Goran Ibrišević",
  "email": "gibrisevic@gmail.com",

  "email_confirmed" : true,
  "welcome_email_id" : "452studidosendgibrisevicc12acip13d2568",
  "roles" : {
      "info" : "rwd",
      "sale" : true,
      "admin" : false
  },
  "new_pass_request" : null,
  "new_pass_email_id" : null,
  "new_pass_email_confirmed" : true,
  "avatar" : "/img/avatars/avatar_image-1622579352769.png",
  "title" : "RegularUser",
  "help_times" : [],
  
  
  dep: null,
  
  

},

  




  ---------------------------------------
  */
  
  
  var { id } = data;
  
  
  


  var valid = {
  
  
    user_number: { element: "input", type: "string", lock: true, visible: true, label: "ID" },
    name: { element: "input", type: "string", lock: false, visible: true, label: "Nadimak" },
    
    pass: { element: "input", type: "string", lock: false, visible: true, label: "Lozinka" },
    pass_2: { element: "input", type: "string", lock: false, visible: true, label: "Ponovi Lozinku" },
    
    full_name: { element: "input", type: "string", lock: false, visible: true, label: "Ime i Prezime" },
    
    
    
    dep: { element: "single_select", type: "simple", lock: false, visible: true, label: "Odjel" },           
    email: { element: "input", type: "string", lock: false, visible: true, label: "Email" },
    user_tel: { element: "input", type: "string", lock: false, visible: true, label: "Telefon" },
    user_address: { element: "input", type: "string", lock: false, visible: true, label: "Adresa" },
    is_admin: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "SuperAdmin !!!" },
    
    card_num: { element: "input", type: "string", lock: false, visible: true, label: "Broj Kartice" },
    
    
  };

  this_module.valid = valid;  


  console.log(data);
  
  var rand_id = `velusers`+cit_rand();
  
  this_module.component_data = {
    id: data.id,
    segment: data.segment,
    rand_id: rand_id,
    
  };
  
  
  this_module.fresh_user = {
    
    user_saved : null,
    user_edited : null,


    saved : null,
    edited : null,
    last_login : null,

    user_number: null,

    user_address : null,
    user_tel : null,

    name : null,
    pass: null,
    full_name : null,
    email: null,

    email_confirmed : true,
    welcome_email_id : cit_rand() + "acdlsfmcanfcal" ,
    roles : {
        "info" : "rwd",
        "sale" : true,
        "admin" : false
    },
    new_pass_request : null,
    new_pass_email_id : null,
    new_pass_email_confirmed : true,
    avatar : "/img/avatars/avatar_image-1622579352769.png",
    title : "RegularUser",
    help_times : [],

    card_num: null,

    dep: null,
    
  };
  
  
  if ( !data._id ) {
    
    data = {
      
      ...data,
      ...this_module.fresh_user,
    
    };
    
  }; // kraj ako nema _id
  
  this_module.set_init_data(data);
  
  
  this_module.cit_data[id].rand_id = rand_id;
  
  
  var component_html =
`
<div  id="${id}" class="cit_comp users_comp" >
  <section>

    <div class="container-fluid">
      
      
      <div class="row" style="margin-bottom: 10px;">
        
        <div class="col-md-12 col-sm-12" id="meta_data_box">
          ${ meta_data_html(data) }
        </div>
       

      </div>
      
      
      <div class="cit_tab_strip">
        <div id="vel_users_list_tab" class="cit_tab_btn cit_active">LISTA</div>
        <div id="vel_users_basic_tab" class="cit_tab_btn">OSNOVNI PODACI</div>
      </div>
    
      <div id="vel_users_list_box" class="cit_tab_box" style="padding-left: 10px; padding-right: 10px; margin-bottom: 50px;">


        <div class="row" style="margin-top: 30px;">
        
          <div  id="all_users_list_box" 
                class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" 
                style=" padding-top: 20px;
                        display: flex;
                        justify-content: center;
                        align-items: center;
                        flex-direction: column; " >
                
            <!-- OVDJE IDE LISTA SVIH STATUSA -->    
            
            
            <i class="fas fa-cog fa-spin" style="font-size: 40px; color: #a0b6ef; " ></i>
            
            
          </div> 
          

        </div>       

      </div>
      
      <div id="vel_users_basic_box" class="cit_tab_box" style="display: none; padding-left: 10px; padding-right: 10px; margin-bottom: 50px;">
       
       
        <div class="row" style="margin-top: 30px; " >

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_user_number`, valid.user_number, data.user_number || "")  }
          </div>


          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_name`, valid.name, data.name || "" ) }
          </div>

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_email`, valid.email, data.email || "" ) }
          </div>
          
          <div class="col-md-4 col-sm-12">
            ${ cit_comp(rand_id+`_full_name`, valid.full_name, data.full_name || "" ) }
          </div>

        </div>  
        
        <div class="row" >

          <div class="col-md-2 col-sm-12">
             ${ cit_comp(rand_id+`_dep`, valid.dep, data.dep,  data.dep?.naziv || "") }
          </div>

          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_card_num`, valid.card_num, data.card_num || "") }
          </div>
          

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_user_tel`, valid.user_tel, data.user_tel || "") }
          </div>

          <div class="col-md-4 col-sm-12">
            ${ cit_comp(rand_id+`_user_address`, valid.user_address, data.user_address || "" ) }
          </div>
          
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_is_admin`, valid.is_admin, data.super_admin || false ) }
          </div>

        </div>         
        
        
        <div class="row">
         
          <div class="col-md-2 col-sm-12" id="vel_users_pass_box" >
            <i class="fas fa-eye"></i>
            <i class="fas fa-eye-slash"></i>
            
            ${ cit_comp(rand_id+`_pass`, valid.pass, "", null, `letter-spacing: 0.1rem;`) }
            
          </div>
          
          <div class="col-md-2 col-sm-12" id="vel_users_pass_2_box" >
            <i class="fas fa-eye"></i>
            <i class="fas fa-eye-slash"></i>
            
            ${ cit_comp(rand_id+`_pass_2`, valid.pass_2, "", null, `letter-spacing: 0.1rem;`) }
          </div>
          
          
        </div>
        
        
        <div class="row" style="margin-top: 30px; margin-bottom: 30px;" >
        
          <div class="col-md-2 col-sm-12">
           
            <button class="blue_btn btn_small_text" id="odustani_vel_user_btn" style="margin: 0 auto; box-shadow: none;">
              ODUSTANI
            </button>
            
          </div>
          
          <div class="col-md-8 col-sm-12">
            &nbsp;
          </div>
        
          <div class="col-md-2 col-sm-12">
           
            <button class="blue_btn btn_small_text" id="save_vel_user_btn" style="margin: 0 auto 0 0; box-shadow: none;">
              SPREMI KORISNIKA
            </button>

            <button class="violet_btn btn_small_text" id="update_vel_user_btn" style="margin: 0 auto 0 0; box-shadow: none; display: none;">
              AŽURIRAJ KORISNIKA
            </button>

          </div>

        
        </div>
                

      </div>
      
      <!-- KRAJ SADRŽAJA ZA TAB BASIC DATA -->
     
      
    </div>
    
  </section>
  
</div>

`;



  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };
  
  wait_for( `$('#${data.id}').length > 0`, function() {
    
    console.log(data.id + 'component injected into html');
    toggle_global_progress_bar(false);
    
    $('.cit_tooltip').tooltip();
    
    $(`html`)[0].scrollTop = 0;
    
    
    $(`#cit_page_title h3`).text(`Korisnici`);
    
    document.title = "KORISNICI VELPROM";
    
    
    ask_before_close_register_event();
    
    
    
    $('#'+rand_id+'_pass').attr(`type`, `password`);
    $('#'+rand_id+'_pass_2').attr(`type`, `password`);
    
    
    
    
    
    
    
    this_module.make_user_list(data);
    

    
    $('#'+rand_id+'_dep').data('cit_props', {
      desc: `odaberi ODJEL od ovog korisnika -------> unutar vel users  ${rand_id}`,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'deps',
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.dep = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.dep = state;
        };
        
      },
      
    });    
    


    
    $('#'+rand_id+'_is_admin').data(`cit_run`, function(state, this_elem) { 
      
      
      var this_comp_id = $(this_elem).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      data.super_admin = state;
      
      
    });

    
    
    $(`#vel_users_pass_box .fa-eye-slash, #vel_users_pass_2_box .fa-eye-slash` ).off(`click`);
    $(`#vel_users_pass_box .fa-eye-slash, #vel_users_pass_2_box .fa-eye-slash` ).on(`click`, function() {
      
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
    
      $(`#vel_users_pass_box .fa-eye-slash`).css(`display`, `none`);
      $(`#vel_users_pass_2_box .fa-eye-slash`).css(`display`, `none`);
      
      
      $(`#vel_users_pass_box .fa-eye`).css(`display`, `flex`);
      $(`#vel_users_pass_2_box .fa-eye`).css(`display`, `flex`);
      
      
      $('#'+rand_id+'_pass').attr(`type`, `password`);
      $('#'+rand_id+'_pass_2').attr(`type`, `password`);
      
      
      
    });
    
    
    $(`#vel_users_pass_box .fa-eye, #vel_users_pass_2_box .fa-eye`).off(`click`);
    $(`#vel_users_pass_box .fa-eye, #vel_users_pass_2_box .fa-eye`).on(`click`, function() {
      
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
    
      $(`#vel_users_pass_box .fa-eye-slash`).css(`display`, `flex`);
      $(`#vel_users_pass_2_box .fa-eye-slash`).css(`display`, `flex`);
      
      
      $(`#vel_users_pass_box .fa-eye`).css(`display`, `none`);
      $(`#vel_users_pass_2_box .fa-eye`).css(`display`, `none`);
      
      
      $('#'+rand_id+'_pass').attr(`type`, `text`);
      $('#'+rand_id+'_pass_2').attr(`type`, `text`);
      
      
      
    });
    

    
    $(`#${data.id} .cit_input.number`).off(`blur`);
    $(`#${data.id} .cit_input.number`).on(`blur`, function() {
      
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      
      set_input_data( this, this_module.cit_data[data.id] );
      
      
      
    });
    
    
    
    $(`#${data.id} .cit_input.string`).off(`blur`);
    $(`#${data.id} .cit_input.string`).on(`blur`, function() {
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      set_input_data( this, this_module.cit_data[data.id] );

      
    });
    

    
    
    $(`#save_vel_user_btn`).off(`click`);
    $(`#save_vel_user_btn`).on(`click`, function(e) {
      this_module.save_vel_user(e, null);
    });

    
    
    $(`#update_vel_user_btn`).off(`click`);
    $(`#update_vel_user_btn`).on(`click`, function(e) {
      this_module.save_vel_user(e, null);
    });


    $(`#odustani_vel_user_btn`).off(`click`);
    $(`#odustani_vel_user_btn`).on(`click`, this_module.quit_vel_user );

    
    
    
  }, 50*1000 );

  return {
    html: component_html,
    id: data.id
  };

},
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};

  
  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
  };
  this_module.set_init_data = set_init_data;
  
  
  
  function update_user(arg_user) {
    
    
    return new Promise( function(resolve, reject) {
      

      toggle_global_progress_bar(true);

      $.ajax({
        headers: {
          'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
        },
        type: "POST",
        cache: false,
        url: `/update_user`,
        data: JSON.stringify( arg_user ),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done( async function (result) {

        console.log(result);
        
        if ( result.success == true ) {

          var ib_users = await this_module.get_IB_users(arg_user);
          
          if ( ib_users?.length > 0 ) {
            
            ib_user = ib_users[0];
            var help_times = null;
            if ( ib_user ) help_times = ib_user.help_times || null;
            result.user.help_times = help_times;
            
          };
          
          resolve(result.user);

        } else {

          if ( result.msg ) popup_error(result.msg);
          if ( !result.msg ) popup_error(`Greška prilikom UPDATE user!`);

          if ( result.err || result.error ) {
            console.error(`----------- GRAŠKA U update_user`);
            console.error( result.err || result.error);
          };

        };

      })
      .fail(function (error) {

        console.log(error);
        popup_error(`Došlo je do greške na serveru prilikom dobivanja svih korisnika!`);
      })
      .always(function() {

        toggle_global_progress_bar(false);

        // $(`#save_partner_btn`).css("pointer-events", "all");

      });


    }); // kraj promisa  
      
  };
  this_module.update_user = update_user;

  
  
  function get_all_users(arg_ids) {
    
    
    return new Promise( function(resolve, reject) {
      

      toggle_global_progress_bar(true);

      $.ajax({
        headers: {
          'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
        },
        type: "POST",
        cache: false,
        url: `/get_all_users`,
        data: JSON.stringify( { user_ids: arg_ids || null } ),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done( async function (result) {

        console.log(result);
        if ( result.success == true ) {

          
          // var ib_users = await this_module.get_IB_users();
          
          resolve(result.users);

        } else {

          if ( result.msg ) popup_error(result.msg);
          if ( !result.msg ) popup_error(`Greška prilikom spremanja!`);

          if ( result.err || result.error ) {
            console.error(`----------- GREŠKA U get_all_users`);
            console.error( result.err || result.error);
          };

        };

      })
      .fail(function (error) {

        console.log(error);
        popup_error(`Došlo je do greške na serveru prilikom dobivanja svih korisnika!`);
      })
      .always(function() {

        toggle_global_progress_bar(false);

        // $(`#save_partner_btn`).css("pointer-events", "all");

      });


    }); // kraj promisa  
      
  };
  this_module.get_all_users = get_all_users;

  
  
  
  function get_IB_users(arg_user) {



    return new Promise( function(resolve, reject) {


      toggle_global_progress_bar(true);

      $.ajax({
        headers: {
          'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
        },
        type: "POST",
        cache: false,
        url: `https://help.velprom.hr/get_IB_users`,
        data: JSON.stringify( { user_id: arg_user._id || null } ),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done(function (result) {

        console.log(result);
        
        if ( result.success == true ) {

          resolve(result.users);

        } else {

          if ( result.msg ) popup_error(result.msg);
          if ( !result.msg ) popup_error(`Greška prilikom spremanja!`);

          if ( result.err || result.error ) {
            console.error(`----------- GRAŠKA U get_all_users`);
            console.error( result.err || result.error);
          };

        };

      })
      .fail(function (error) {

        console.log(error);
        popup_error(`Došlo je do greške na serveru prilikom dobivanja svih korisnika!`);
      })
      .always(function() {

        toggle_global_progress_bar(false);

        // $(`#save_partner_btn`).css("pointer-events", "all");

      });


    }); // kraj promisa  

};
  this_module.get_IB_users = get_IB_users;
  
  
  
  async function make_user_list(data) {
    
    
    
    var this_comp_id = $('#all_users_list_box').closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    
    var users_result = await this_module.get_all_users();

    
    $(`#all_users_list_box`).html(``);

    
    if ( !users_result || users_result?.length == 0 ) {
      popup_warn(`MRAV nije uspio skinuti podatke korisnika :( `);
      return;
    };
    
    
    data.all_users = users_result;
    
    
    if (data.all_users && data.all_users.length > 0 ) {
      
      $.each(data.all_users, function(u_ind, user) {
        
        // data.all_users[u_ind].deleted_text = data.all_users[u_ind].deleted ? `<span class="cit_pill">DA</span>` : "NE";
        
        data.all_users[u_ind].dep_naziv = user.dep?.naziv || "";
        data.all_users[u_ind].saved_text =  (user.user_saved?.full_name || "")  + "<br>" + (user.saved ?  cit_dt(user.saved).date_time : "" );
        data.all_users[u_ind].edited_text = (user.user_edited?.full_name || "") + "<br>" + (user.saved ?  cit_dt(user.edited).date_time : "" );
        data.all_users[u_ind].super_admin_text = data.all_users[u_ind].super_admin ? `<span class="cit_pill">DA</span>` : "NE";
        
    
        
      }); // kraj loop po svim dobivenim userima iz baze

      
      var user_list_props = {

        desc: 'za kreiranje tablice svih usera u modulu vel users !!!!',
        local: true,

        list: data.all_users,

        show_cols: [
          `saved_text`,
          `edited_text`,
          `user_number`,
          `full_name`,
          `email`, 
          `super_admin_text`,
          `help_time_sum`,
          `dep_naziv`,
          'button_edit',
          'button_delete',
        ],



        custom_headers: [
          `SAVED`,
          `EDIT`,
          `BROJ`,
          `IME I PREZIME`,
          `EMAIL`, 
          `SUPER ADMIN ?`,
          `IB / min`,
          `ODJEL`,
          'EDIT',
          'DEL',
        ],


        col_widths: [
          3, // `SAVED`,
          3, // `EDIT`,
          1, // `BROJ`,
          3, // `IME I PREZIME`,
          4, // `EMAIL`, 
          1, // `SUPER ADMIN ?`,
          1, // `IB / min`,
          2, // `ODJEL`,
          1, // 'EDIT',
          1, // 'DEL',
        ],

        format_cols: {
          
          help_time_sum: 0,
          user_number: "center",
          help_time_sum: "center",
          super_admin_text: "center",
          
        },
        
        parent: `#all_users_list_box`,

        return: {},
        show_on_click: false,

        cit_run: function(state) { console.log(state); },

        
        button_edit: function(e) {

          e.stopPropagation(); 
          e.preventDefault();


          
          var this_comp_id = $(this).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          
          var index = find_index(data.all_users, sifra , `_id` );

          var selected_user = cit_deep( data.all_users[index] );
          
          var all_users = data.all_users ? cit_deep(data.all_users) : null;
          
          // napravi refresh data tj ubaci usera kojeg sam odabrao
          
          this_module.cit_data[this_comp_id] = {
            
            ...selected_user,
            ...this_module.component_data,
            all_users: all_users,
            
          };
          
          data = this_module.cit_data[this_comp_id];
          rand_id =  this_module.cit_data[this_comp_id].rand_id;

          
          var meta_data = meta_data_html( selected_user );
          $(`#meta_data_box`).html(meta_data);
          
          $(`#`+rand_id+`_user_number`).val(data.user_number || "");
          
          $(`#`+rand_id+`_card_num`).val(data.card_num || "");
          
          $(`#`+rand_id+`_name`).val(data.name || "");
          $(`#`+rand_id+`_name`).prop( "disabled", true );
          
          $(`#`+rand_id+`_full_name`).val(data.full_name || "");
          $(`#`+rand_id+`_dep`).val(data.dep?.naziv || "");
          
          $(`#`+rand_id+`_email`).val(data.email || "");
          $(`#`+rand_id+`_email`).prop( "disabled", true );
          
          $(`#`+rand_id+`_user_tel`).val(data.user_tel || "");
          $(`#`+rand_id+`_user_address`).val(data.user_address || "");
          
          toggle_cit_switch( data.super_admin || false, $('#'+rand_id+`_is_admin`) );
          
                    
          setTimeout( function() {
            cit_scroll( $("#meta_data_box"), -200, 300);
          }, 100 );
          
          setTimeout( function() {
            $(`#vel_users_basic_tab`).trigger(`click`);
          }, 500 );
          

          $('#save_vel_user_btn').css(`display`, `none`);
          $('#update_vel_user_btn').css(`display`, `flex`);

        },

        
        button_delete: async function(e) { 

          e.stopPropagation(); 
          e.preventDefault();
          
          console.log(`klik na delete usera !!!`);
          
          var popup_text = `Jeste li sigurni da želite OBRISATI ovog korisnika???`;
          
          var this_button = this;

          async function delete_user_yes() {

            var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
            var data = this_module.cit_data[this_comp_id];
            var rand_id =  this_module.cit_data[this_comp_id].rand_id;

            var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
            var user_id = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
            
            // ---------------------- izbaci taj red usera u html
            $(`#`+parent_row_id).remove();
            
            
            var deleted_user = find_item(data.all_users, user_id , `_id` );
            deleted_user_copy = cit_deep(deleted_user);
            
            // obriši usera iz lokalne liste
            data.all_users = delete_item(data.all_users, user_id , `_id` );
            // zatim napravi kopiju liste usera
            var all_users = data.all_users ? cit_deep(data.all_users) : null;
            
            
            // upaci prazni template za usera tj fresh usera
            this_module.cit_data[this_comp_id] = {
              ...this_module.fresh_user,
              ...this_module.component_data,
              all_users: all_users,
            };
            
            
            deleted_user_copy.deleted = true;
            this_module.save_vel_user( null, deleted_user_copy );
            
            

          };

          function delete_user_no() {
            show_popup_modal(false, popup_text, null );
          };

          var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
          var show_popup_modal = pop_mod.show_popup_modal;
          show_popup_modal(`warning`, popup_text, null, 'yes/no', delete_user_yes, delete_user_no, null);


        },
        
      };


      if (data.all_users?.length > 0 ) {

        var criteria = [ '!saved' ];
        multisort( data.all_users, criteria );  

        create_cit_result_list(data.all_users, user_list_props );
      };


      
    };  // kraj AKO IMA NEŠTO U ALL USERS !!!!!!!!!!
    
    
    
    
    
    
    
  
  
  
  
  
    
  };
  this_module.make_user_list = make_user_list;
  
  

  function active_save_btns() {
    
    $(`#save_vel_user_btn`).css("pointer-events", "all");
    $(`#update_vel_user_btn`).css("pointer-events", "all")
    
  };
  this_module.active_save_btns = active_save_btns;
  
 
  async function save_vel_user( e, arg_delete_user) {
    
    
    var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
    var show_popup_modal = pop_mod.show_popup_modal;
    
    
    var go_save = true;
    
    // resetiraj sva crvena polja za greške
    $(`#vel_users_basic_box .cit_input`).removeClass(`missing_data`);

    var this_button = $(`#save_vel_user_btn`)[0];

    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    // ova func napravi cit deep !!!!!!
    var data_copy = null;
    
    // ako imam delete usera onda neka to bude data copy za sve daljnje radnje !!!!!!
    if ( arg_delete_user ) data_copy = cit_deep(arg_delete_user);
    
    // ako nisam dao usera za brisanje onda spremi meta podatke
    if ( !arg_delete_user ) data_copy = save_metadata( this_module.cit_data[this_comp_id] );
    
    // removaj all users array
    if ( data_copy.all_users ) delete data_copy.all_users;
    if ( data_copy.id ) delete data_copy.id;
    if ( data_copy.segment ) delete data_copy.segment;
    
    
    // disable oba gumba i save i update
    $(`#save_vel_user_btn`).css("pointer-events", "none");
    $(`#update_vel_user_btn`).css("pointer-events", "none");
    

    var req_url = `/save_user_as_admin`;
    
    // ako ima _id onda je update
    if ( data_copy._id || arg_delete_user ) req_url = `/update_user_as_admin`;
    
    
    // ako nema _id znači da je novi i moram provjeriti jel sve upisano kako treba !!!!!
    
    // -------------------------------- NEW SAVE VALIDATION --------------------------------
    if ( !data_copy._id ) {
      
      if ( data.name.length < 6 ) {
        popup_warn(`Nadimak mora imati minimalno 6 znakova`);
        this_module.active_save_btns();
        return;
      };
      
      if ( !data.full_name || !data.email || !data.user_tel || !data.user_address ) {
        popup_warn(`Obavezno upisati Ime i prezime, Email, Telefon i Adresu !!!`);
        this_module.active_save_btns();
        return;
      };
    
    
      var is_email_ok = data.email.search(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);

      if ( is_email_ok === -1 || data.email.indexOf('.') === -1) {
        popup_warn(`Email nije ispravan`);
        $(`#`+rand_id+`_email`).addClass(`missing_data`);
        this_module.active_save_btns();
        return;
      };
    
      
      if ( !data.dep ) {
        popup_warn(`Obavezno izabrati odjel!`);
        this_module.active_save_btns();
        return;
      };
      
      
      if ( data.pass.length < 6 ) {
        popup_warn(`Lozinka mora imati minimalno 6 znakova`);
        this_module.active_save_btns();
        return;
      };
      
      if ( $('#'+rand_id+'_pass').val().indexOf(` `) > -1 ) {
        popup_warn(`Lozinka ne smije imati prazan razmak u sebi !!!`);
        this_module.active_save_btns();
        return;
      };
      
      
      if ( $('#'+rand_id+'_pass').val() !== $('#'+rand_id+'_pass_2').val() ) {
        popup_warn(`Lozinka i Ponovljena Lozinka nisu iste !!!`);
        this_module.active_save_btns();
        return;
      };

      
    };
    // -------------------------------- NEW SAVE VALIDATION --------------------------------
    
    
    // -------------------------------- UPDATE PASS --------------------------------
    // dakle ADMIN želi promieniti pass !!!!
    if ( data_copy._id && $('#'+rand_id+'_pass').val() !== `` ) {
      
      
      var popup_text = `Jeste li sigurni da želite PROMIJENITI LOZINKU ovog korisnika???<br>Ako ne želite promjenu lozinke, obrišite tekst u poljima za lozinku!`;
          

      function change_pass_yes() {

        if ( $('#'+rand_id+'_pass').val().indexOf(` `) > -1 ) {
          
          popup_warn(`Lozinka ne smije imati prazan razmak u sebi !!!`);
          this_module.active_save_btns();
          
        } 
        else if ( $('#'+rand_id+'_pass').val() !== $('#'+rand_id+'_pass_2').val() ) {
          
          popup_warn(`Lozinka i Ponovljena Lozinka nisu iste !!!`);
          this_module.active_save_btns();
          
        }
        else  {
          
          data_copy.pass = $('#'+rand_id+'_pass').val();
          
          this_module.run_save_user(data_copy, arg_delete_user, req_url);
          
        };
        

      };

      function change_pass_no() {
        show_popup_modal(false, popup_text, null );
        
        this_module.active_save_btns();
      };

      
      show_popup_modal(`warning`, popup_text, null, 'yes/no', change_pass_yes, change_pass_no, null);

      // kraj ako admin želi promjeniti lozinku !!!
      // -------------------------------- UPDATE PASS --------------------------------
    } else {
    
      // ako nije change pass !!!
      this_module.run_save_user(data_copy, arg_delete_user, req_url);
    };

  };
  this_module.save_vel_user = save_vel_user;
  
  
  
  
  async function run_save_user(data_copy, arg_delete_user, req_url ) {
    
    
    var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
    var show_popup_modal = pop_mod.show_popup_modal;
    

    var popup_text = `Jeste li provjerili sve podatke korisnika ???`;

    function u_sure_yes() {

      toggle_global_progress_bar(true);
      
      $.ajax({
        headers: {
          'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
        },
        type: "POST",
        cache: false,

        url: req_url,

        data: JSON.stringify( data_copy ),

        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done(function (result) {

        console.log(result);

        if ( result.success == true ) {


          var this_comp_id = $(`#save_vel_user_btn`).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          // -------------------------- ako je save ili update

          if ( !arg_delete_user ) {
            
            if ( data_copy.pass ) cit_toast(`LOZINKA JE PROMJENJENA !!!`);

            cit_toast(`KORISNIK JE SPREMLJEN!`);

            var meta_data = meta_data_html( result.user );
            $(`#meta_data_box`).html(meta_data);

            // ------------------------------------------------------- SAVE ILI UPDATE --------------------------------------------------------------
            var all_users = data.all_users ? cit_deep(data.all_users) : [];

            // dodaj ako je new save ili zamjeni usera ako je update !!!
            all_users = upsert_item(all_users, result.user, `_id`);

            $(`#`+rand_id+`_user_number`).val( result.user?.user_number || "");


            this_module.cit_data[this_comp_id] = {
              ...result.user,
              ...this_module.component_data,
              all_users: all_users,
            };

            data = this_module.cit_data[this_comp_id];


            this_module.make_user_list(data);

            // ------------------------------------------------------- SAVE ILI UPDATE --------------------------------------------------------------


          } else {

            // --------------- ako je delete user onda sam već sve obavio lokalno sva ažuriranja za HTML i lokalne podatke
            // --------------- unutar button_delete funkcije u propsima za listu svih usera

            cit_toast(`KORISNIK JE OBRISAN!`);

            // obriši sva polja da budu prazna !!!!
            this_module.quit_vel_user();

          };


        } else {

          // ako result NIJE SUCCESS = true

          if ( result.msg ) popup_error(result.msg);
          if ( !result.msg ) popup_error(`Greška prilikom spremanja!`);
          
        };

      })
      .fail(function (error) {

        console.log(error);
        popup_error(`Došlo je do greške na serveru prilikom spremanja KORISNIKA!`);
      })
      .always(function() {

        toggle_global_progress_bar(false);
        
        this_module.active_save_btns();

      });

    };

    function u_sure_no() {

      show_popup_modal(false, popup_text, null );
      this_module.active_save_btns();

    };

    show_popup_modal(`warning`, popup_text, null, 'yes/no', u_sure_yes, u_sure_no, null);

   
    
    
  };
  this_module.run_save_user = run_save_user;
  
  
  function quit_vel_user() {
    
    var this_comp_id = $(`#odustani_vel_user_btn`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    var all_users = data.all_users ? cit_deep(data.all_users) : null;
    
    // napravi refresh data tj ubaci usera kojeg sam odabrao

    this_module.cit_data[this_comp_id] = {

      ...this_module.fresh_user,
      ...this_module.component_data,
      all_users: all_users,

    };
    
    data = this_module.cit_data[this_comp_id];
    rand_id =  this_module.cit_data[this_comp_id].rand_id;
          
    var meta_data = meta_data_html( this_module.cit_data[this_comp_id] );
    $(`#meta_data_box`).html(meta_data);

    $(`#`+rand_id+`_user_number`).val("");
    $(`#`+rand_id+`_card_num`).val("");
    
    
    $(`#`+rand_id+`_name`).val("");
    $(`#`+rand_id+`_full_name`).val("");
    $(`#`+rand_id+`_dep`).val("");
    $(`#`+rand_id+`_email`).val("");
    $(`#`+rand_id+`_user_tel`).val("");
    $(`#`+rand_id+`_user_address`).val("");
    
    
    $(`#`+rand_id+`_pass`).val("");
    $(`#`+rand_id+`_pass_2`).val("");
    
    
    toggle_cit_switch( false, $('#'+rand_id+`_is_admin`) );
    
    
    $('#save_vel_user_btn').css(`display`, `flex`);
    $('#update_vel_user_btn').css(`display`, `none`);
    
    
    $(`#`+rand_id+`_name`).prop( "disabled", false );
    $(`#`+rand_id+`_email`).prop( "disabled", false );
    
    
  };
  this_module.quit_vel_user = quit_vel_user;
  
  
  
  this_module.cit_loaded = true;
 
} // end of module scripts
  
  
};

