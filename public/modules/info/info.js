var module_object = {
create: ( data, parent_element, placement ) => {
  var this_module = window[module_url]; 
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };
  
  
  this_module.set_init_data(data);
  
  var { id, anchor } = data;
  
  
  var component_html =
`

<div id="${id}" class="info_body">

<div id="cit_find_box">
 
  <div id="cit_find_result_box">
    <div id="cit_find_count">0/0</div>
    <div id="cit_find_prev"><i class="fal fa-angle-up"></i></div>
    <div id="cit_find_next"><i class="fal fa-angle-down"></i></div>
  </div>

  <input autocomplete="off" id="cit_find_all" type="text" class="cit_input">

  <div id="cit_find_all_btn">
    <i class="fal fa-search"></i>
  </div>
</div>


<!--
    TODO:<br>
    1. Kako funkcionira klimatizacijski sustav<br>
    2. Pitati Zorana za Box Plus 2 (APR solution) - nije mi jasno što piše u WORD-u<br>
    3. Određivanje dimezija stalka <br>
    4. MODEL ploče : u tekstu piše "navedene u tablici se odnose na svaku dimenziju" -- pitati Martinu koja tablica? <br>
    
    5.
    ovo je bilo ispod Kreativni tisak d.o.o.
    treba pitati Radmilu što ću s tim
    
    6.
    <h3>Ostalo (prebaciti na drugo mjesto)</h3>
      Forma Purus - rezanje materijala <br>
    <br>
    
    7.
    KOD Model d.o.o. dimenzije ploča --- ovo nema smisla je ne postoji tablica: 
    minimalna količina za narudžbe navedene u tablici s
-->
 
   
<br>
<br>
<br>
<br>
  
<h3 style="text-align: center; font-size: 20px; color: darkred;">!!! BITNO !!!</h3>  
<h3 style="text-align: center; font-size: 20px; color: darkred;">Strogo je zabranjeno video snimanje unutarnjih i vanjskih prostora VELPROMA 
<br>kao i postavljanje takvih video materijala na bilo koje javno dostupne medije, platforme i društvene mreže.</h3>

  

<br>
<br>

<h3 style="text-align: center; font-size: 20px;">Svi Zakonici VELPROMA nalaze se na NAS/Administracija.</h3>
<h3 style="text-align: center; font-size: 20px;">(naša lokalna mreža koja je svim korisnicima unutar VELPROMA dostupna)</h3>
<h3 style="text-align: center; font-size: 20px;">Do pisane verzije moze se doći i u uredu administracije.</h3>
<br>
  
  
<br>
<br>
  
<h3 style="text-align: center; font-size: 20px;">
 Doznake za bolovanja mogu slati na<br>administracija@velprom.hr<br>ili<br>predavati u postanski sandučić ispred vrata računovodstva.
</h3>

<br>
<br>
<br>
  

  <div class="vel_info_page">

  <div class="vel_info_text">
     
    <div class="no_col_break">
      <h4 style="margin-top: 0;">Podaci za R1 račun</h4>
      <span style="font-size: 16px; font-weight: 700;"> 
      Velprom d.o.o,<br>
      Dalmatinska 27, 10410 Velika Gorica,<br>
      OIB:33428207264 <br>
      tel: 01/6215541
      </span>
    </div> 
  </div>
  
  </div>


<h2 style="margin-left: 20px;">Upute za nove kolege</h2> 

<div class="vel_info_page">
  
    <div class="vel_info_text">
     
      
      <div class="no_col_break">
      
      <h4 style="margin-top: 0;">Kako otvarati vrata RF karticom</h4>
      
        <!--
        <a class="video_link" href="/img/info/video/otvaranje_vrata_1.mp4" target="_blank">
          <i class="far fa-play-circle"></i>&nbsp;Kako koristiti karticu
        </a>
        -->
        
        <a class="video_link" href="https://youtu.be/6aQwbiSVzNQ" target="_blank">
          <i class="far fa-play-circle"></i>&nbsp;Kako koristiti karticu
        </a>
        
        
        <br>
        
        <!--
        <a class="video_link" href="/img/info/video/otvaranje_vrata_2.mp4" target="_blank">
          <i class="far fa-play-circle"></i>&nbsp;Ulazna vrata 
        </a>
        -->
        
        <a class="video_link" href="https://youtu.be/7mmq8lP2BAE" target="_blank">
          <i class="far fa-play-circle"></i>&nbsp;Ulazna vrata 
        </a>
        

      </div> <!-- end no col break -->
           
            
    </div> <!-- end info text --> 
    
    <div class="vel_info_text">
     
      <div class="no_col_break">
        <h4 style="margin-top: 0;">Pristup WEB mailu</h4>
        <a href="https://lin62.mojsite.com:2096/" target="_blank" style="font-size: 18px; line-height: 2;" >https://lin62.mojsite.com:2096/</a>
      </div> 
    </div>
    
    
    <h4 style="margin-top: 0;">Presmjeravanje fiksnog telefona na mobilni telefon</h4>
    
    <div class="vel_info_text">
    
     <div class="no_col_break">
      
        <a class="video_link" href="https://youtu.be/P1MzDTNpE4k" target="_blank">
          <i class="far fa-play-circle"></i>&nbsp;TELEFON TIP 1: Aktivacija preusmjerenja
        </a>
        <br>
        Ukoliko ste uspješno proveli aktivaciju znak da je prusmjeren poziv na drugi uređaj stajat će u lijevom gornjem uglu telefona. Kako je prikazano u videu iznad.
        <br>
        
        <br>
        <a class="video_link" href="https://youtu.be/INBKjwX3lkQ" target="_blank">
          <i class="far fa-play-circle"></i>&nbsp;TELEFON TIP 1: De-aktivacija preusmjerenja
        </a>
        <br>
        
      </div>
      
      <div class="no_col_break">
      
        <a class="video_link" href="https://youtu.be/on47DWDwL7M" target="_blank">
          <i class="far fa-play-circle"></i>&nbsp;TELEFON TIP 2: Aktivacija preusmjerenja
        </a>
        <br>
        <a class="video_link" href="https://youtu.be/WvkiJpwQz9Q" target="_blank">
          <i class="far fa-play-circle"></i>&nbsp;TELEFON TIP 2: De-aktivacija preusmjerenja
        </a>
        <br>
        
      </div>
      
      
      
      
    </div>
    
    
    <h4 style="margin-top: 0;">Presmjeravanje fiksnog telefona na fiksni telefon</h4>
    
    <div class="vel_info_text">
    
     <div class="no_col_break">
      
        <a class="video_link" href="https://youtu.be/qMI6HLvcOhw" target="_blank">
          <i class="far fa-play-circle"></i>&nbsp;VIDEO UPUTE
        </a>
        
        <br>
          

          Javite se na poziv, klik na  gumbić prikazan na videu.
          I tek kada vam se javi osoba na telefon kojoj namjeravate spojiti 
          poziv, tek tada spusite slušalicu.
          I poziv je spojen.

        <br>
        
      </div>
      
    </div>
    
    
</div> <!-- end info page -->


<h2 id="procedure_pravila">Procedure i pravila</h2>


<div class="vel_info_page">


  <!--

  <div class="vel_info_text">

    <div class="no_col_break">

      <h4>CAD Radno mjesto</h4>

      1. Ako nam je kupac poslao GP onda je izrada nacrta prioritet<br>


    </div> 

  </div> 
  -->

   
   
  <h3>Procedura nabave kod novih dobavljača</h3>

  <div class="vel_info_text col_count_2">
   <h4 style="margin-top: 0;">Kod kontaktiranja novih dobaljača bitno je obratiti pažnju na:</h4>

    <br>
    
    <ul>
      <li style="margin-top: 5px;">Dimenzije su uvijek bitne I uvijek ih je bitno objasniti AxB mm(npr.995X695mm) – za svaku prvu komunikaciju sa dobavljačem UVIJEK je potrebno odrediti koja dimenzija predstavlja val, a koja kontraval.</li>
      <li style="margin-top: 5px;">Potrebno je definirati slojeve ( koliko ih ima) i kakvi su: u boji ( bijel, smeđ, premazni) I u unosu ( gramaturi) I u kvaliteti ( kraft, tesliner),te Koji unosi idu u lice, a koji u naličje,te koji unos ide u val.</li>
      <li style="margin-top: 5px;">Također je bitno kod peteroslojnih materijala definirati koji val ce biti bliže licu.</li>
      <li style="margin-top: 5px;">Treba se kod dobavljača raspitati i o visini vala jer ima raznih visina valova, a mi se dimenzijski zbog konstrukcije trebamo zadrzati unutar nekih nama bitnih gabarita</li>
      <li style="margin-top: 5px;">Kod komunikacije sa stranim dobavljačima potrebno je detaljno sve opisati,i ne koristiti oznake koje samo nama nešto znače (BT/e npr).</li>
    </ul>

    <br>

    <div class="no_col_break">
      <h4 style="margin-top: 0;">Primjer maila prema stranom dobavljaču (specifikacijski dio):</h4>
      

      Remark - first dimension is in flute direction !!! <br>
      For example: 995(flute direction) X 695(perpendicular to flute) <br>
      <br>

      <b>1. two-layer corrugated cardboard:</b>
      <ul>  
        <li>outside 120g Testliner ,  inside (wave)100g fluting,</li>
        <li>100% recycled</li>
      </ul>  
      Cardboard dimensions:
      <ul>
        <li>995x695mm</li>
        <li>695x995mm</li>
      </ul>

      <br>

      <b>2. two-layer corrugated cardboard </b>
      <ul>    
        <li>outside 125g white tesliner, inside (wave)100g fluting</li>
        <li>100% recycled</li>
      </ul>
      Cardboard dimensions:
      <ul>
        <li>995x695mm</li>
        <li>695x995mm</li>
      </ul>

      <br>        
      What is the height of the E flute?

    </div> <!--KRAJ NO COL BREAK-->



  </div>    
    

   
      
         
            
               
                  
                     
                        
                           
                              
                                 
                                    
                                       
                                             
   
   
    
  <h3>Procedura rada s alatima</h3>

  <div class="vel_info_text">

    <style>
      #procedura_alata li {
        margin-bottom: 10px;
      }

    </style>

    <ol id="procedura_alata">
      <li><b>CAD u Sloning ušifrira</b> alat na način da popuni sve tražene podatke.</li>
      <li><b>Policu alata i lokaciju</b> isčitava prema slobodnim mjestima iz tablice na <b>Google sheetsima</b> koja je sherana svim zaposlenima u CAD-u <b>"Police za alate"</b>.</li>
      <li>Kad CAD osoba odabere policu u koju ide alat potrebno je <b>ODUZETI</b> jedan od <b>"Broj slobodnih mjesta"</b></li>
      <li><b>Na alat se ucrtava/upisuje uz sve tražene podatke i Polica na koju se alat sprema.</b> Upiše se samo Polica, bez Skladista i to u veličini fonta</li>
      
      <li><b>Ukoliko alat IZRAĐUJEMO MI</b> - u Software se pod Stanje alata piše <b>"U IZRADI datum"</b> (npr. "U IZRADI 15.01.2021.")</li>
      
      <li>Ukoliko se alat <b>IZRAĐUJE U LPF</b> - u Stanje alata se piše <b>"LPF datum"</b> (npr. "LPF 12.05.2020.")</li>
      
      <li><b style="color: darkred;">Važna napomena:</b> svaki alat ima svog vlasnika. Vlasnik alata je onaj koji taj alat plaća. Ukoliko kupac kupuje alat onda je on vlasnik alata. Ukoliko trošak alata nije prikazan kupcu onda je vlasnik alata Velprom, bez obzira tko je naručitelj.</li>
      
      <li><b style="color: darkred;">Strogo vodite brigu tko je vlasnik alata.</b></li>
      
      <li><b>Ukoliko nam alat dostavlja kupac</b>, taj alat svejedno ušifriravate i dodajete mu policu. Vlasnik alata je kupac, a Stanje alata <b>"PRIMLJEN i datum"</b>. Isti alat se po primitku <b>odmah dostavlja u CAD</b> da se alat pravilno unese u Software.</li>
      
      <li>Ukoliko se utvrdi da je potrebna izrada novog alata jer je <b>alat koji imamo kriv</b> onda se alat daje na <b>uništenje</b>  na način da ga se fizički ukloni iz police i pod Polica, Skladište pišemo <b>"UNIŠTEN"</b>, a pod Stanje alata <b>"UNIŠTEN datum"</b>.</li>

      <li><b style="color: darkred;" >Nikad pod istu šifru ne stavljamo drugi alat</b>. Za novi alat (u ovom slučaju ponovljeni/drugi alat) otvaramo novu šifru i sve odrađujemo ispočetka. (za ovaj postupak zadužen je CAD i VODITELJ PROIZVODNJE)</li>


      <li>Ukoliko se <b>od nekog alata odustane</b> kad je dat u izradu u status mu pisemo <b>"STORNIRAN i datum"</b> (upisuje CAD)</li>

      <li><b>Kad alat posuđujemo</b> u status dopisujemo  <b>"POSUĐEN i ime onoga kome je posuđen, br.otpremnice i datum"</b>. 
      
        <ul>
          <li>Dopisivanje vršimo kao u primjeru koji slijedi: "NA STANJU 04.05.2019, POSUĐEN MARKEK br.1768/2022, 05.08.2019."  i kad se posudba vrati i alat ponovo dođe kod nas dopisujemo  status "NA STANJU 09.10.2020.". Što u konačnici izgleda ovako : " NA STANJU 04.05.2019, POSUĐEN MARKEK br.1768/2022,  05.08.2019, NA STANJU 10.10.2020"</li>
        </ul>
      
      </li>


      <li><b>Kad alat vratimo kupcu</b> u status upisujemo <b>"VRAĆEN KUPCU br.otpremnice i datum"</b></li>

      <li> <b style="color: darkred;" >Ukoliko postoji jos neka situacija, a sad nije navedena naknadno će se upisati. U polje Status alata stane 250 znakova.</b> </li>

      <li>Alati koji imaju status <b>"U IZRADI i datum"</b> – po izradi alata Poslovođa ili Mato javljaju CAD-u da je alat izrađen, a CAD mijenja status u kartoteci alata u  <b>"NA STANJU od datuma"</b> (npr. " NA STANJU 12.12.2021")</li>

      <li>Alat koji ima status <b>"LPF datum"</b> po primitku alata na skladište <b>VODITELJ SKLADIŠTA ili GL. SKLADIŠTAR</b> dostavljaju alat u proizvodnju i u Status unose <b>"NA STANJU od datuma"</b></li>

      <li><b>Kada se alat POSUĐUJE.</b> Onaj tko posuđuje (PRODAJA ili VODITELJ PROIZVODNJE) <b style="color: darkred;" >obavezno mora izdati otpremnicu i navesti šifru alata</b>, te obavijestiti CAD da napravi traženu izmjenu statusa i dopiše <b>"......POSUĐEN, br.otpremnice i datum...."</b>.</li> 
      <li><b>Kada se taj isti alat vraća</b>, VODITELJ SKLADISTA ili GL. SKLADISTAR u Status alata dopisuje  <b>".... NA STANJU i datum"</b>. </li>

      <li>
      <b>Alat iako je posuđen ne smije izgubiti svoje mjesto u polici</b>, jer kad se vrati tamo ćemo ga spremiti. Preporuka je da se u polje Skladište dopise " posudili", samo zbog toga kad ga povučemo na RN ako nije u firmi nećemo skužiti dok netko ne pogleda u Software, a do tog trenutka radnik može izgubiti puno vremena dok pokušava pronaći alat.</li>

      <li>Kad alat vraćamo kupcu u Status dopisujemo <b>"........ VRAĆEN KUPCU, br.otpremnice i datum"</b>, a u polja Polica i Skladište  takodjer pisemo <b>"Vraćen kupcu"</b> radi lakšeg uočavanja na RN</li>

    </ol>    

    <br>

    <b style="color: darkred">Ukoliko uočite neku nepravilnost, molim ukažite na nju da je ispravimo ili dopišemo. Takodjer ako s vremenom uočite da nema navedene prakse koju trebate  - javite o čemu se radi pa ćemo je dopisati.</b>

  </div>    
    


  <div class="table_wrap">
    <table class="info_table" style="width: 100%;  min-width: 860px;">
      <tbody>

        <tr>
          <td style="width: 300px; text-align: center;"><b>Na nacrt alata potrebno je upisati: </b></td>
          <td style="width: 300px; text-align: center;"><b>Font i font size: </b></td>
          <td style="text-align: center;"><b>Primjer :</b></td>
        </tr>

        <tr>
          <td>
            <ul>
              <li>*broj alata*</li>
              <li>*Naziv*</li>
              <li>Materijal + kaširanje ako ga ima</li>
              <li>Prirez</li>
              <li>Nož- Big</li>
              <li>Ostali noževi (perforiracija, perforir za lijepljenje ...)</li>
            </ul>

          </td>

          <td>
            <ul>
              <li>Arial</li>
              <li>Front size ovisi o veličini plohe na koju je moguće upisati tekst (20-40 pt)</li>
              <li>Tekst je potrebno minimalno odmaknuti od noža za debljinu gumice koja je 11mm</li>
            </ul>
          </td>
          <td>

            <img style="min-width: unset;
                        min-height: unset;
                        max-width: unset;
                        max-height: unset;
                        width: 60%;
                        padding: 0;
                        height: auto;
                        display: block;" 
                        src="/img/info/alat_text_1.png" alt="" />

          </td>

        </tr>

        <tr>
          <td>Oznaka police</td>
          <td>
            <ul>
              <li>Arial</li>
              <li>Front size: 250-300 pt</li>
            </ul>
          </td>
          <td rowspan="4">
            <img style="min-width: unset;
                        min-height: unset;
                        max-width: unset;
                        max-height: unset;
                        width: 60%;
                        padding: 0;
                        height: auto;
                        display: block;" 
                        src="/img/info/alat_text_2.png" alt="" />


          </td>
        </tr>

        <tr>
          <td>Simbol za tok valovite ljepenke</td>
          <td>&nbsp;</td>
        </tr>

        <tr>
          <td>Perforir za lijepljenje ( ako se alat štanca s lica)</td>
          <td>&nbsp;</td>
        </tr>


        <tr>
          <td>Oštećenje alata za cilindar</td>
          <td>&nbsp;</td>
        </tr>

      </tbody>
    </table>
  </div>

  <br>
  <br>
   
   
  <h3>Procedura u situaciji kada <span style="color: red;">kupac sam tiska te nama šalje otisnute ploče (arke) na rezanje ili štancanje</span> </h3>
    
  U slučaju kada kupac sam tiska te nama šalje otisnute ploče (arke) <br>
  na rezanje ili štancanje potrebno je pridržavati se sljedeće procedure:  
  
  <br>
  <br>
    
  <div class="vel_info_text">
    <div class="no_col_break">
      <h4 style="margin-top: 0;">PREMA KUPCU:</h4>
      <ol>
        <li>U samom startu komunikacije kupcu potrebno je napomenuti da je dužan poslati  nam montažnu pripremu prije tiska kako bi na nju dodali naše markere.</li>
        <br>
        <li>U suprotnom ako kupac otisne arke bez naših markera potrebano mu je naplatiti dodatni iznos zbog gubitka vremena u samom procesu rada.</li>
      </ol>
    </div> <!-- end no col break -->
    
    <div class="no_col_break">
      <h4 style="margin-top: 0;">KOD NAS U FIRMI KAD KUPAC POŠALJE PRIPREMU:</h4>
      
      Kad kupac referentu prodaje pošalje montažnu pripremu ista se šalje na cad@velprom.hr i priprema@velprom.hr<br>
      <br>
      Priprema i Cad obavljaju svoji dio posla,te vraćaju montažnu priprema sa dodanim potrebnim elementima referentu prodaje.
      Prodaja prosljeđuje našu montažnu pripremu kupcu koji je dužan otisnuti montažu koju smo mu mi poslali , jer u suprtonom ako tako ne učini snosi dodatni trošak (stavka2.) prema utrošenom vremenu.
      <br>
      <b>Satnica koja se računa 400 kn/h.</b>
      <br>
      <br>
      Ukoliko se u Prodaji odluči kupcu zbog višeg interesa pokloniti dodatno vrijeme , onda se na narudžbu /račun  kupca dodaje ova stavka u izračunatom iznosu i daje rabat (odobrenje) kupcu.
      <br>
      Odobrenje može biti i 100%.
      <br>
      <br>
      <b>Kupac mora biti svjestan benefita koji je dobio!</b>
      
    </div>
    
  </div>  
    
    
   
  <h3>Procedura za UZORKE</h3>
  <h4>(lokacija, isporuka)</h4>
    
  <br>
    
  Svaki izrađeni uzorak nalazi se na lokaciji ODJEL CAD_KOD PROZORA_NASUPROT ULAZA <br>

  Nakon što se uzorak napravi, Dejan javi referentu da je uzorak u odjelu CAD-a.<br><br>
  Izrađen uzorak od Dejana (stol kod plotera) preuzima osoba iz CAD-a koja je pustila radni nalog radi provjere samog uzorka te ga nosi na mjesto isporuke: <br>
  
  <b>ODJEL_CAD_KOD_PROZORA_NASUPROT_ULAZA</b>
  
  
  <br>
  <br>
  <h4 style="color: red; margin: 10px 0;">Svaki uzorak treba imati otpremnicu!</h4>
  
  Daljni proces ovisi o načinu isporuke:<br>
  
  <ol>
    <li>Kupac sam dolazi</li>
    <li>Dostava prodajnog predstavnika</li>
    <li>Kurirska služba</li>
    <li>Naša dostava</li>
  </ol>
  
  <br>
    
  <div class="vel_info_text">
   
    <div class="no_col_break">
     
      <h4 style="margin-top: 0;">1. KUPAC SAM DOLAZI</h4>
      <ul>
        <li>Po dogovoru s referentom kupac se javlja na skladište ili dolazi kod referenta koji preuzima uzorak s dogovorene lokacije <b>ODJEL_CAD_KOD_PROZORA_NASUPROT_ULAZA</b></li>
        <li>Referent koji sam dostavlja/predaje uzorak izrađuje otpremnicu i obavezno u LNK mora promijeniti status isporučeno. Otpremnicu dostavlja u Financije</li>
        <li>Ako se uzorak za kupca samo fotografira, referent radi otpremnicu i mijenja status u isporučeno.</li>
      </ul>

    </div> <!-- end no col break -->
    
    <div class="no_col_break">
      <h4 style="margin-top: 0;">2. DOSTAVA PRODAJNOG PREDSTAVNIKA</h4>
      <ul>
        <li>Predstavnik preuzima uzorak s otpremnicom iz CAD-a</li>
      </ul>
      
      <h4 style="margin-top: 0;">3. KURIRSKA SLUŽBA</h4>
      <ul>
        <li>Skladište preuzima uzorke naznačene kurirska služba. Dejan specifikaciju paketa javlja skladištu koje navedene podatke javlja kupcu (ako sam organizira kurirsku) ili Ivanki (ako mi naručujemo kurirsku) i stavlja referenta u cc maila.</li>
      </ul>
    </div>
    
    <div class="no_col_break">
      <h4 style="margin-top: 0;">4. NAŠA DOSTAVA</h4>
      <ul>
        <li>Skladište preuzima uzorke naznačene naša dostava iz CAD-a te šalje na isporuku</li>
      </ul>
    </div>
    
    
    
  </div>  
   
  <h4 style="color: red; margin: 10px 0;">Potpisane otpremnice predati u financije!</h4>
    
  Uzorak od kupca bilo proizvod ili roba nakon razrade vraća se referentu koji postupa s njim po dogovoru s kupcem. 
  <br>
  <br>
  
  Napomena:<br>
  Kod kreiranja radnog naloga UZORKA obavezno navesti: <br>
  <ul>
    <li>Datum isporuke uzorka</li>
    <li>Način dostave uzorka (naša dostava, dostava prodajnog predstavnika, kupac sam dolazi i kurirska služba)</li>
    <li>Da li se uzorak isporučuje formiran ili neformiran</li>
    <li>Gratis ili se naplaćuje.</li>
  </ul>
  
      
  <br>
  <br>
  <br>
  
  
  <h3>DONACIJA i SPONZORSTVO</h3> 
  <br>
  <br>
  
  
  <div class="vel_info_text">
   
    <div class="no_col_break">
      <h4 style="margin-top: 0;">DONACIJA</h4>
  
      <ul>
        <li>darovanje u novcu, stvarima ili uslugama primatelju,</li>
        <li><b>bez ikakve naknade ili protuusluge</b></li>
        <li>potrebno je donijeti pisanu odluku uprave o darovanju: koga, u kojem iznosu i obliku te za koje svrhe se daruje</li>
        <li>porezno se priznaju kao porezni trošak</li>
        <li>za donacije u stvarima/uslugama potrebno je platiti PDV</li>
        <li>za donacije u novcu ne plaća se PDV</li>
      </ul>

    </div>
 
 
  <div class="no_col_break">
    <h4 style="margin-top: 0;">SPONZORSTVO</h4>

    <ul>
      <li><b>davanje za koje postoji protuusluga reklame</b></li>
      <li><b>sponzor daje novac, robu ili usluge, a primatelj ga zauzvrat reklamira</b></li>
      <li>potrebno je sklopiti ugovor o sponzorstvu</li>
      <li>za pruženu uslugu reklame <b>primatelj sponzorstva</b> mora izdati račun</li>
      <li>za sponzorstvo u novcu, <b>sponzor nema obveze</b> izdavanja računa ni plaćanja PDV-a</li>
      <li>za sponzorstvo u stvarima ili uslugama, <b>sponzor</b> izdaje račun za protuvrijednost, a <b>primatelj</b> račun za usluge reklame; nakon toga se putem izjave o prijeboju vrši kompenzacija</li>
      <li>troškovi su porezno priznati kao trošak promidžbe</li>  
    </ul>
    
  </div>
  
  
</div>  
  
  
   
    
</div> <!-- end info page -->


<br>
<br>



<h2 id="info_kupci">Kupci</h2>

  <div class="vel_info_page">
  <h3 style="font-weight: 700;font-size: 36px;" >Stemmler</h3> 
   
   
  <h3>POSTUPAK SKLADIŠTA KOD ISPORUKE ROBE ZA STEMMLER</h3> 
  
  <div style="max-width: 600px; width: 100%;" > 
    Postoje tri različite vrste slanja robe za Stemmlera:<br>
    <ol>
      <li>Slanje palete s uzorcima koje nakon što se paletna kutija napuni, ide direktno Stemmleru u Altrip.</li>
      <li>Slanje uzoraka prije naklade – šalje se kurirskom preko FAS logistike ili neke druge povoljnije opcije.</li>
      <li>Slanje naklade krajnjem kupcu</li>
    </ol>
    
  </div>

<br>
  
   
    <div class="vel_info_text">
    
    
      <div class="no_col_break">
        <h4 style="margin-top: 0;">SLANJE PALETE S UZORCIMA</h4>    

        <ol style="padding-left: 20px;">            
          <li>Prodajni referent, nakon što se paleta napuni, otvara nk s proizvodom „izrada uzoraka“ i u opis upisuje „paleta s uzorcima“.</li>
          <li>Skladište javlja referentu logističke podatke navedene palete i kreira našu otpremnicu u koju upisuje logističke podatke.</li>
          <li>Referent naručuje dostavu i javlja skladištu.</li>
          <li>Paleta s uzorcima uvijek se isporučuje na običnoj paleti, kao i naklada.</li>
          <li>Skladište za dostavu ima samo Velprom otpremnicu i paletnu – Stemmlerova otpremnica i paletna u ovom slučaju nisu potrebni jer se roba isporučuje direktno njima na adresu.</li>
          <li>Uz otpremnicu, radi se i CMR u kojem se kao pošiljatelj upisuje samo Velprom.</li>
        </ol>
        
        <br>
        
              
        <b>NAPOMENA:</b> Navedena paleta ne smije biti viša od 1900mm, jer je ulaz u Stemmlerovo skladište nizak i paleta neće moći proći.<br>
        <br>
        Stemmler nikad ne vraća obične palete, tako da ih obavezno moramo upisati kao proizvod na otpremnicu kako bi se mogle fakturirati.<br>
        <br>
        Najpovoljnije su isporuke ponedjeljkom i petkom. <br>
        <br>
        

        
      </div>  
      
      
      <div class="no_col_break">
        <h4 style="margin-top: 0;">SLANJE POJEDINAČNIH UZORAKA</h4>    

        <ol style="padding-left: 20px;">            
          <li>Prodajni referent otvara nk s proizvodom „izrada uzoraka“ i veže ga uz broj Stemmlerove otpremnice (primjerice #1111).</li>
          <li>Ako uzorak ide krajnjem kupcu, potrebni su Stemmlerova otpremnica i paletna kartica, ali ako ide direktno Stemmleru, isporuku prate naši dokumenti.</li>
          <li>Ako isporuka ide krajnjem kupcu, 2.list CMR-a je drugačiji.</li>
          <li>Ako isporuka ide Stemmleru, cijeli blok CMR se piše jednako.</li>
          <li>Sve što isporučujemo za Stemmlera mora se fotografirati – kako roba, tako i dokumenti koji se zatim šalju na mail referentu.</li>
        </ol>

        <br>

      </div>
      
      
      
      <div class="no_col_break">
        <h4 style="margin-top: 0;">SLANJE NAKLADE KRAJNJEM KUPCU</h4>    

        <ol style="padding-left: 20px;">              
          <li>Nakon što je roba proizvedena, potrebno je provjeriti nalazi li se u folderu RN Stemmlerova paletna kartica.</li>
          <li>U NAS-u, prodaja, folder nk, referent sprema Stemmlerovu otpremnicu. Uvijek je označena slovima LS (Lieferanschrift).</li>
          <li>Nakon što skladište napravi logističke podatke, javlja ih referentu koji naručuje kurirsku službu.</li>
          <li>Sva roba koja se isporučuje za Stemmlera i njegovom krajnjem kupcu, mora biti na običnim paletama.</li>
          <li>Skladište printa Stemmlerovu otpremnicu u tri primjerka, našu otpremnicu u jedan primjerak i pripremiti cmr.</li>
          <li>Cmr u ovom slučaju ispunjavamo na sljedeći način:
            <ul>
              <li>Upisujemo podatke pod rednim brojevima 2,3,5,7, i 11.</li>
              <li>Odvojimo drugi list i nastavljamo pisati bez njega brojeve 4 i 21 te udaramo pečat na brojeve 1 i 22. Na broj 16 i 23 prijevoznik udari vlastiti pečat i potpisuje se te upisuje reg.oznaku.</li>
              <li>Ponovno uzimamo list broj 2 i na njega upisujemo brojeve 1 i 22 kao: Stemmler Display Group GMBH&amp;CO.KG, Akazienweg 25, 67122 Altrip.</li>
            </ul>
          </li>
          
          
          
          <li>Stemmlerovu otpremnicu ispunjavamo na sljedeći način:</li>   
       
        </ol>

        <br>

      </div>


      
      
      
      
    </div>
    
    <div class="vel_info_text col_count_2">
      <div class="no_col_break">
        <img src="/img/info/upute_stemmler_skladiste.png" alt="" style="width: 100%;" />
      </div>
     
     
     <div class="no_col_break">
     
     8. Prijevoznik potpisuje naš primjerak otpremnice koji ostaje nama, cmr i Stemmlerove otpremnice te ih slažemo na sljedeći način:<br>
     <br>
Prijevozniku ide:<br>
-	Blok cmr-a osim prvog i drugog lista <br>
-	Stemmlerova otpremnica <br>
<br>

Kupcu ide: <br>
-	List 2 CMR-a <br>
-	Stemmlerova otpremnica <br>
<br>
Nama ostaje: <br>
-	Prvi list CRM-a <br>
-	Stemmlerova otpremnica <br>
-	Naša otpremnica. <br>
<br>
Nakon isporuke, sva se navedena dokumentacija slika i šalje referentu na mail.

     
     
     
     </div>
     
     
      
    </div>
    
    
    
    
  </div>
  <!-- end info page za info_kupci -->

<br>
<br>




  <h2 id="vrste_materijala_info">Vrste materijala</h2>

  <div class="vel_info_page">
    

    <h3 id="materijali_info">Označavanje materijala</h3>
   
    <div class="vel_info_text col_count_2">

      <h4>Valovi</h4>
      <div class="no_col_break">
        <img src="/img/info/Valovita_ljepenka_svi_slojevi.png" alt="" />
        <img src="/img/info/Valovita_ljepenka_-_valovi_NOVO1.jpg" alt="" />

      </div> <!-- end no col break -->
      <br>

      <div class="no_col_break">
        <h4>Oznake materijala</h4>
        P - premaz<br>
        T - smeđe (testliner)<br>
        B - bijelo<br>
        Š - šrenc<br>
        <b>Troslojni materijal:</b> _ _ /_ 3 slova<br>
        <b>Primjer:</b> BT/e bijelo-smeđi e val<br>
        <b>Iznimka:</b> P2B/e jednostrano premazni - obostrano bijeli e-val<br>
        <br>
        <b>Peteroslojni materijal:</b> _ _ _ /_ _ 5 slova<br>
        <b>Primjer:</b> 2TŠ/eb Smeđe-smeđi eb val <br>
        <b>Iznimka:</b> 2P2B/eb Obostrano premazni, obostrano bijeli eb - val <br>
        <br>
        <b>
          Napomena: <br>
          P = jedostrano premazni <br>
          2P = obostrano premazni <br>
          Oznaka "/" dijeli oznaku materijala od vala<br>
        </b>
        <br>
        <br>
        Dvoslojni i četveroslojni materijal će se u 99% slučajeva koristiti kod kaširanja, kod tih materijala <b style="color: #a40404;">val će uvijek biti smeđe boje</b>.
        <br>
        <br>
        Dvoslojni materijal: _ _ _ /_ 4 slova<br>
        Primjer: SFB/e bijeli dvoslojni mikroval<br>
        Primjer: SFT/b smeđi dvoslojni b val<br>
        <br>
        Četveroslojni materijal: _ _ _ /_ _ 5 slova <br>
        Primjer: SFB/eb bijeli četveroslojni eb val<br>
        Primjer: SFT/eb smeđi četveroslojni eb val<br>
        <br>
        <b style="color: #a40404;">Oznaka SF je engleska skraćenica od single-faced što označava da valovita ljepenka ima samo jednu ravnu stranu.</b>
        



      </div> <!-- end no col break -->

    </div>

      
    
    
  </div>


  <div class="vel_info_page">
    
    <h3>Pojašnjenje označavanja materijala</h3>


    <div class="vel_info_text">


      <b>Troslojni:</b> 3 sloja (2 ravna i 1 valoviti) <br>


      Smeđe-smeđi označavamo sa 2T <br>

      (Kako je smeđe oznaka T, a tu imamo 2
      smeđa, znači bilo bi TT ili kraće 2T) <br>

      Bijelo-smeđi = BT <br>
      Bijelo-bijeli = 2B <br>

      <br>
      <br>

      <b>Peteroslojni:</b> 5 slojeva (3 ravna, 2 valovita) <br>
      Ima u sredini ravan sloj od ŠRENCA- oznaka Š <br>

      Smeđe-smeđi = 2TŠ (dva smeđa vanjska i šrenc u sredini) <br>

      Bijelo-smeđi = BTŠ <br>

      Bijelo-bijeli = 2BŠ <br>

      <br>
      <br>
      <div class="no_col_break">

        Kad smo odredili slojeve materijala potrebno je odrediti val.<br>

        U Velpromu najčešće radimo sa C, B i E valom koji su ovdje imenovani od najvećeg prema najmanjem. <br>
        C je najveći <br>
        B je srednji <br>
        E je najmanji tzv. Mikroval <br>
        Probajte ovdje pročitano primjeniti na materijal s kojim radite. <br>
        <br>
      </div>

      <div class="no_col_break">

        <b>Najčešće korištene valovite ljepenke sa pripadajućim opisima: </b> <br>
        2T/e – Smeđe smeđa troslojna valovita ljepenka e-val <br>
        2B/e – Bijelo bijela troslojna valovita ljepenka e-val <br>
        BT/e – Bijelo smeđa troslojna valovita ljepenka e-val <br>
        2T/b – Smeđe smeđa troslojna valovita ljepenka b-val <br>
        2B/b – Bijelo bijela troslojna valovita ljepenka b-val <br>
        BT/b – Bijelo smeđa troslojna valovita ljepenka b-val <br>
        P2B/e – Jednostrano premazna bijela troslojna valovita ljepenka e-val <br>
        2TŠ/eb – Smeđe smeđa peteroslojna valovita ljepenka eb-val <br>
        BTŠ/eb – Bijelo smeđa peteroslojna valovita ljepenka eb-val <br>
        2P2B/eb – Obostrano premazna, obostrano bijela peteroslojna valovita ljepenka eb-val <br>
        P2B/eb – Jednostrano premazna, obostrano bijela peteroslojna valovita ljepenka eb-val <br>
        2TŠ/bc – Smeđe smeđa peteroslojna valovita ljepenka bc-val <br>
        KK/ge – Smeđe smeđa četveroslojna valovita ljepenka ge-val <br>

      </div> <!-- end no col break -->

    </div>



    <div class="vel_info_text">


      <div class="no_col_break">

        
        <b>Dvoslojni:</b> 2 sloja (1 ravni i 1 valoviti)<br>
        SF = single-faced<br>
        B = bijeli ravni sloj<br>
        T = smeđi ravni sloj<br>
        <br>
        <br>
        <b>Četveroslojni:</b> 4 sloja (2 ravna i 2 valovita, od čega je jedan „otvoren“, dok se drugi nalazi između 2 ravna sloja) <br>
        <b>Kod četveroslojnog materijala <b>ne označavamo ravni sloj između dva vala</b> kao kod peteroslojnog materijala.</b><br>
        SF = single-faced<br>
        B = bijeli ravni sloj<br>
        T = smeđi ravni sloj<br>
        <br>
        <br>

      </div>

      <div class="no_col_break">
        Primjeri najčešće korištenih dvoslojnih i četveroslojnih materijala <b>za kaširanje:</b> <br>
        <br>
        SFB/e bijeli dvoslojni mikroval <br>
        SFT/e smeđi dvoslojni mikroval<br>
        SFB/b bijeli dvoslojni b val <br>
        SFT/b smeđi dvoslojni b val <br>
        SFB/eb bijeli četveroslojni eb val<br> 
        SFT/eb smeđi četveroslojni eb val<br>
        <br> 
        <br>

      </div>

    </div>



  </div>
  <br>
  <br>


  <div class="vel_info_page">
  
    <b style="text-align: left; color: #6b8fb5;">
      Legenda: <br>
      Latex - znači da se otiskuje na Latex stroju (HP Latex 365) <br>
      UV - znači da se otiskuje UV boja na strojevima Fuji Acuity LED 1600 II ili HP Scitex 15500 <br>
    </b>
    <br>
    <h3>Papir</h3>
    <h4 class="mini_h4">Vrsta tiska: Latex, UV</h4>

    <div class="vel_info_text">

      Najčešći materijal za tisak proizveden
      od celuloznih vlakana (i drugih aditiva) s različitim vanjskim premazima. Premazni papiri se koriste za tisak umjetničkih reprodukcija, fotografija itd.Nepremazni papiri u pravilu pružaju lošiju kvalitetu tiska. Prema gramaturi (takozvanoj površinskoj masi) razlikuju se:
      <br>
      - lakši papiri (od 10 do 60 g/m2), <br>
      - srednje teški papiri (od 60 do 120 g/m2 ),<br>
      - polukartoni (od 150 do 200 g/m ),<br>
      - kartoni (od 200 do 600 g/m2) i <br>
      - ljepenke (više od 600 g/m2). <br>
      Kartoni su krući i čvršći od papira, mogu biti različito površinski obrađeni i sastoje se od jednog ili više slojeva (dupleks karton, tripleks karton). U višeslojnim kartonima pojedini se slojevi proizvode od različitih sirovina i različitih su svojstava. Sloj koji će prilikom upotrebe kartona služiti kao vanjski ili gornji sloj obično je gladak, svjetliji i izrađen od kvalitetnije sirovine. Kartoni se veoma mnogo primjenjuju za pakiranje (ambalažni materijal), rjeđe za pisanje ili za tisak.

    </div>
    
    <h3>Oznake papira</h3>
    
    
    <h4>Solid Bleached Board (SBB, GZ) multiply construction</h4>

    <div class="vel_info_text">
     
      <div class="no_col_break">
        <img src="/img/info/1_oznake_kartona.png" alt="" style="padding-bottom: 5px; padding-top: 0;">
      </div> <!-- end no col break -->
     
      SBB se proizvodi isključivo od izbijeljene kemijske pulpe.
      Obično ima gornju površinu obloženu pigmentom, a može biti i pigmentirana na stražnjoj strani.
    </div>
    
    
    
    <h4>Folding Box Board (FBB, GC1), white back, multiply construction</h4>
    <div class="vel_info_text">
     
      <div class="no_col_break">
        <img src="/img/info/2_oznake_kartona.png" alt="" style="padding-bottom: 5px; padding-top: 0;">
      </div> <!-- end no col break -->
     
      FBB, bijela poleđina, proizvodi se od slojeva mehaničke pulpe u sendviču između slojeva izbijeljene kemijske pulpe.
      <br>
      Gornji sloj je obložen pigmentom. Poleđina je deblja od FBB-a, kremasta leđa, a može i pigmentirana.
    </div>
    
    
    <h4>Folding Box Board (FBB, GC2), cream back, multiply construction</h4>
    <div class="vel_info_text">
     
      <div class="no_col_break">
        <img src="/img/info/2_oznake_kartona.png" alt="" style="padding-bottom: 5px; padding-top: 0;">
      </div> <!-- end no col break -->
     
      FBB, smeđa poleđina, proizvodi se od slojeva mehaničke pulpe u sendviču između slojeva izbijeljene kemijske pulpe.
    </div>
    
    
    
    
    <style>
      
      
      #oznake_papira_table tr,
      #termini_oznake_papira tr {
        height: 35px;
      }
      
      #oznake_papira_table td,
      #termini_oznake_papira td {
        font-size: 13px;
      }
    
    </style>
    


<h4>Skraćenice</h4>

<table id="termini_oznake_papira" class="info_table incoterms_table" style="width: 50%; min-width: 320px; margin: 0;">
  
  <tbody>
   
    <tr class="incoterms_header" style="background: #eaeaea;">
      <td style="width: 30%; text-align: center;"><b>OZNAKA</b></td>
      <td style="width: 70%; text-align: center;"><b>OPIS PO DIN 19303</b></td>
    </tr>
   
    <tr>
      <td><strong>GZ</strong></td>
      <td><strong>Coated SBB</strong></td>
    </tr>
    
    <tr>
      <td>AZ</td>
      <td>Cast Coated SBB</td>
    </tr>
    <tr>
      <td><strong>GC1</strong></td>
      <td><strong>Coated FBB, white back</strong></td>
    </tr>
    <tr>
      <td><strong>GC2</strong></td>
      <td><strong>Coated FBB, cream back</strong></td>
    </tr>
    <tr>
      <td>GN</td>
      <td>Coated SUB, white or brown back</td>
    </tr>
    <tr>
      <td>GT</td>
      <td>Coated WLC, cream or white back</td>
    </tr>
    <tr>
      <td>GD1</td>
      <td>Coated WLC, grey back (spec.volume &lt;1.45 cm³/g)</td>
    </tr>
    <tr>
      <td>GD2</td>
      <td>Coated WLC, grey back (spec.volume 1.3 to 1.45 cm³/g)</td>
    </tr>
    <tr>
      <td>GD3</td>
      <td>Coated WLC, grey back (spec.volume &lt;1.3 cm³/g)</td>
    </tr>
    <tr>
      <td>UZ</td>
      <td>Uncoated SBB</td>
    </tr>
    <tr>
      <td>UC1</td>
      <td>Uncoated FBB, white back</td>
    </tr>
    <tr>
      <td>UC2</td>
      <td>Uncoated FBB, cream back</td>
    </tr>
    <tr>
      <td>UT</td>
      <td>Uncoated WLC, cream or white back</td>
    </tr>
    <tr>
      <td>UD</td>
      <td>Uncoated WLC, grey back</td>
    </tr>
    <tr>
      <td><strong>SBB</strong></td>
      <td><strong>Solid Bleached Board</strong></td>
    </tr>
    <tr>
      <td><strong>FBB</strong></td>
      <td><strong>Folding Box Board</strong></td>
    </tr>
    <tr>
      <td>SUB</td>
      <td>Solid Unbleached Board</td>
    </tr>
    <tr>
      <td>WLC</td>
      <td>White Lined Chipboard</td>
    </tr>
    <tr>
      <td>G</td>
      <td>Gestrichen, coated</td>
    </tr>
    <tr>
      <td>U</td>
      <td>Ungestrichen, uncoated</td>
    </tr>
    <tr>
      <td>A</td>
      <td>Gussgestrichen, cast coated</td>
    </tr>
    <tr>
      <td>Z</td>
      <td>Chemisch gebleichte Frischfasern, bleached virgin chemical pulp</td>
    </tr>
    <tr>
      <td>C</td>
      <td>Holzstoff, virgin mechnical pulp</td>
    </tr>
    <tr>
      <td>N</td>
      <td>Chemisch ungebleichte Frischfasern, unbleached virgin chemical pulp</td>
    </tr>
    <tr>
      <td>T</td>
      <td>Rezyklierter mit weisser oder gelber Rückseite, recycled with white or cream back</td>
    </tr>
    <tr>
      <td>D</td>
      <td>Rezyklierter mit grauer Rückseite, recycled with grey back</td>
    </tr>
  </tbody>
	
</table>
   
   <br>
   <br>
   


<table id="oznake_papira_table" class="info_table incoterms_table" style="width: 100%;">

	<tr class="incoterms_header" style="background: #eaeaea;">
      <td style="width: 12%; text-align: center;"><b>OZNAKA</b></td>
      <td style="width: 25%; text-align: center;"><b>TEHNIČKI NAZIV</b></td>
      <td style="text-align: center;"><b>BRAND</b></td>
      <td style="text-align: center;"><b>PROIZVOĐAČ</b></td>
      <td style="text-align: center;"><b>DETALJI</b></td>
    </tr>
    
	<tr>
	     <td>GC1 KARTON</td>
		<td>2/1 PREMAZANI GC1 KARTON</td>
		<td><b>High Point</b></td>
		<td>Roxcel</td>
		<td>
        <a href="https://igepa.hr/media/28571/high_point.pdf" target="_blank">
        PDF&nbsp;<i class="fal fa-external-link"></i>
        </a>
       </td>
	</tr>
	<tr>
	     <td>GC1 KARTON</td>
		<td>2/1 PREMAZANI GC1 KARTON</td>
		<td><b>Incada Silk</b></td>
		<td>Iggesund</td>
		<td>
        <a href="https://igepa.hr/media/28572/incada-silk.pdf" target="_blank">
        PDF&nbsp;<i class="fal fa-external-link"></i>
        </a>
       </td>
	</tr>
	<tr>
	<td>GC1 KARTON</td>
		<td>2/1 PREMAZANI GC1 KARTON</td>
		<td><b>Aegle White</b></td>
		<td>Kotkamills</td>
		<td>
        <a href="https://igepa.hr/media/28573/aegle-white-spekt-070-rev1.pdf" target="_blank" >
        PDF&nbsp;<i class="fal fa-external-link"></i>
        </a>
       </td>
	</tr>
	<tr>
	<td>GC2 KARTON</td>
		<td>JEDNOSTRANO PREMAZAN, VOLUMINOZNI GC2 KARTON</td>
		<td><b>Aegle Pro</b></td>
		<td>Kotkamills</td>
		<td>
        <a href="https://igepa.hr/media/28574/aegle-pro-new-1.pdf" target="_blank" >
        PDF&nbsp;<i class="fal fa-external-link"></i>
        </a>
       </td>
	</tr>
	<tr>
	<td>GC2 KARTON DUPLEKS</td>
		<td>BIJELO/SIVI DUPLEKS KARTON - GD2</td>
		<td><b>Umka Color</b></td>
		<td>Umka</td>
		<td>
        <a href="https://igepa.hr/media/28576/umka_gd2_color.pdf" target="_blank" >
        PDF&nbsp;<i class="fal fa-external-link"></i>
        </a>
       </td>
	</tr>
	<tr>
	<td>KROMO KARTON</td>
		<td>VISOKO KVALITETAN, 3/1 PREMAZAN, BIJELO-BIJELI KROMO KARTON, PROIZVEDEN OD RECIKLIRANOG PAPIRA</td>
		<td><b>Umka Special</b></td>
		<td>Umka</td>
		<td>
        <a href="https://igepa.hr/media/28578/2015-03-19_techspec_umka_special_gt2.pdf" target="_blank" >
        PDF&nbsp;<i class="fal fa-external-link"></i>
        </a>
       </td>
	</tr>
	<tr>
	<td>GT2 KARTON</td>
		<td>BIJELO/BIJELI DUPLEKS KARTON - GT2</td>
		<td><b>Belpak</b></td>
		<td>MM Karton</td>
		<td>
        <a href="https://igepa.hr/media/28579/product_8_32173_bel_en-1.pdf" target="_blank" >
        PDF&nbsp;<i class="fal fa-external-link"></i>
        </a>
       </td>
	</tr>
	<tr>
	<td>GT4 KARTON</td>
		<td>BIJELO - SMEĐI GT4 KARTON IZRAŽENE ČVRSTOĆE, IDEALAN ZA PROIZVODNJU PREHRAMBENE AMBALAŽE</td>
		<td><b>Grafopak Kraft</b></td>
		<td>MM Karton</td>
		<td>
        <a href="https://igepa.hr/media/28581/product_8_34101_grk_en.pdf" target="_blank" >
        PDF&nbsp;<i class="fal fa-external-link"></i>
        </a>
       </td>
	</tr>
	<tr>
	<td>NATUR KARTON</td>
		<td>VISOKOKVALITETNI AMBALAŽNI NATUR KARTON</td>
		<td><b>Kraftpak</b></td>
		<td>Kapstone</td>
		<td>
        <a href="https://igepa.hr/media/28583/sve-o-kraftpaku.pdf" target="_blank" >
        PDF&nbsp;<i class="fal fa-external-link"></i>
        </a>
       </td>
	</tr>
	<tr>
	<td>SBS/GZ KARTON</td>
		<td>VISOKO KVALITETAN, BEZDRVNI, 3/1 PREMAZAN, ZAGLAĐENI SBS/GZ KARTON POGODAN ZA GRAFIČKU PRIMJENU TE IZDRADU ZAHTJEVNE AMBALAŽE</td>
		<td><b>Invercote G</b></td>
		<td>Iggesund</td>
		<td>
        <a href="https://igepa.hr/media/28587/invercote-g.pdf" target="_blank" >
        PDF&nbsp;<i class="fal fa-external-link"></i>
        </a>
       </td>
	</tr>
	<tr>
	<td>GZ KARTON</td>
		<td>OBOSTRANO PREMAZANI GZ KARTON</td>
		<td><b>Invercote Creato</b></td>
		<td>Iggesund</td>
		<td>
        <a href="https://igepa.hr/media/28589/invercote-creato.pdf" target="_blank" >
        PDF&nbsp;<i class="fal fa-external-link"></i>
        </a>
       </td>
	</tr>
	<tr>
	<td>GZ KARTON</td>
		<td>2/1 PREMAZANI GZ KARTON I 2/2 PREMAZANI GZ KARTON</td>
		<td><b>Symbol Card</b></td>
		<td>Fedrigoni</td>
		<td>
        <a href="https://igepa.hr/media/28592/symbol-card.pdf" target="_blank" >
        PDF&nbsp;<i class="fal fa-external-link"></i>
        </a>
        &nbsp;
        &nbsp;
        <a href="https://igepa.hr/media/28591/symbol-card-2sc.pdf" target="_blank" >
        PDF&nbsp;<i class="fal fa-external-link"></i>
        </a>
        
       </td>
	</tr>
	<tr>
	<td>GC KARTON</td>
		<td>GC KARTON + METALIZIRANI PET FILM</td>
		<td><b>Metalprint</b></td>
		<td>Iggesund</td>
		<td>
        <a href="https://igepa.hr/media/28593/metalprint.pdf" target="_blank" >
        PDF&nbsp;<i class="fal fa-external-link"></i>
        </a>
       </td>
	</tr>
	<tr>
	<td>&nbsp;</td>
		<td>SIVA KNJIGOVEŽKA LJEPENKA</td>
		<td><b>Eska Board</b></td>
		<td>Eska</td>
		<td>
        <a href="https://igepa.hr/media/28599/eska-board.pdf" target="_blank" >
        PDF&nbsp;<i class="fal fa-external-link"></i>
        </a>
       </td>
	</tr>
	<tr>
	<td>&nbsp;</td>
		<td>SIVA LJEPENKA JEDNOSTRANO KAŠIRANA BIJELIM OFFSETNIM PAPIROM</td>
		<td><b>Eska Mono White</b></td>
		<td>Eska</td>
		<td>
        <a href="https://igepa.hr/media/28600/productleaflet-eska-mono-white.pdf" target="_blank" >
        PDF&nbsp;<i class="fal fa-external-link"></i>
        </a>
       </td>
	</tr>
	<tr>
	<td>&nbsp;</td>
		<td>PLAVA LJEPENKA ZA PUZZLE</td>
		<td><b>Eska Puzzle Blue</b></td>
		<td>Eska</td>
		<td>
        <a href="https://igepa.hr/media/28601/productleaflet-eska-puzzle.pdf" target="_blank" >
        PDF&nbsp;<i class="fal fa-external-link"></i>
        </a>
       </td>
	</tr>
	<tr>
	<td>&nbsp;</td>
		<td>CRNA LJEPENKA BOJANA U MASI</td>
		<td><b>Eskablack</b></td>
		<td>Eska</td>
		<td>
        <a href="https://igepa.hr/media/28602/productleaflet-eska-black.pdf" target="_blank" >
        PDF&nbsp;<i class="fal fa-external-link"></i>
        </a>
       </td>
	</tr>
	<tr>
	<td>&nbsp;</td>
		<td>BIJELA UPIJAJUĆA LJEPENKA</td>
		<td><b>Agrippina Fresh</b></td>
		<td>Cordenons</td>
		<td>
        &nbsp;
       </td>
	</tr>
	<tr>
	<td>SBS LJEPENKA</td>
		<td>BIJELA SBS LJEPENKA</td>
		<td><b>Agrippina Board</b></td>
		<td>Adex Cling</td>
		<td>
        &nbsp;
       </td>
	</tr>
</table>
    
    
    <br>
    <br>
    
    

    <h3>Ljepenka (siva, puna)</h3>
    <h4 class="mini_h4">Vrsta tiska: Latex, UV</h4>


    <div class="vel_info_text">

      Ljepenka je deblja i veće površinske mase od kartona. Čvrsta granica između kartona i ljepenke nije određena, no najčešće se ljepenkom nazivaju proizvodi s gramaturom većom od 600 g/m2. Ponekad se, međutim, dorađeni proizvodi od kvalitetnijih sirovina nazivaju kartonima bez obzira na veću gramaturu i debljinu. Prema upotrijebljenim sirovinama razlikuju se bijela i smeđa ljepenka, koje se izrađuju od bijele ili smeđe drvenjače bez dodataka, zatim siva ljepenka (nekeljena) od miješanih papirnih otpadaka, miješana ljepenka od miješanog starog papira i žute slamne celuloze i slamna ljepenka samo od žute slamne celuloze. Površina ljepenke može biti dorađena keljenjem, impregniranjem i slično.

    </div>


    <h3>Valovita ljepenka</h3>
    <h4 class="mini_h4">Vrsta tiska: Latex, UV</h4>

    <div class="vel_info_text">

      Ambalažni materijal sastavljen od više slojeva. Slojevi su međusobno slijepljeni, a neki od tih slojeva su valoviti. S obzirom na broj valova i slojeva, valovita ljepenka, najčešće se dijeli na dvoslojnu, troslojnu i peteroslojnu. Između slojeva, koji su ravni, nalazi se val. Valovi su u obliku sinusoide, napravljene tako da se lako oblikuju bez oštećenja, najčešće od krutog i žilavog papira. <br>
      <br>
      Ravni slojevi su od papira velike vlačne čvrstoće. U postupku nastajanja valovite ljepenke, za lijepljenje ravnih i valovitih slojeva koriste se ljepila. Valovita ljepenka zbog svojih dobrih mehaničkih svojstava i pristupačne cijene ubraja se u jednu od najboljih ambalažnih materijala. <br>
      <br>
      Razlikuju se slijedeći tipovi vala: <br>
      <br>
      A val: 120 valova/metru (tzv. veliki val); <br>
      B val: 167 valova/metru (tzv. mali val); <br>
      C val: 140 valova/metru (tzv. srednji val); E val: 295 valova/metru (tzv. sitni val).<br>
      <br>
      U opisu vala koristi se i parametar visina vala koji je u razmjeru s parametrom korak vala: <br>
      <br>
      A val: 4,0 do 4,8 mm<br>
      B val: 2,1 do 3,0 mm<br>
      C val: 3,2 do 3,9 mm<br>
      E val: 1,0 do 1,8 mm<br>
      <br>
      <br>
    </div>


    <h3>Honeycomb ploče (Re-Board)</h3>
    <h4 class="mini_h4">Vrsta tiska: Latex, UV</h4>

    <div class="vel_info_text">
      Slične su valovitoj ljepenci, ali središnja struktura je papirnata, međusobno povezana saćasta struktura koja daje veliku čvrstoću uz malu težinu samog materijala. Materijal pokušava popuniti tržišni prostor između stalaka od valovite ljepenke i metalnih stalaka. Lagan, čvrst, otporan i lako sastavljiv stalak od Re- Boarda je puno jeftiniji od metalnog stalka a opet puno izdržljiviji od stalka izrađenog od valovite ljepenke.
    </div>

    <h3>ACM paneli (ACM - aluminijski kompozitni materijal), alubond,
      tubond, dibond
    </h3>
    <h4 class="mini_h4">Vrsta tiska: UV</h4>

    <div class="vel_info_text">
      ACM paneli su poznatiji pod nazivima Poznatiji je pod nazivima: alubond, dibond, tubond... Alubond je laki, kruti, izdržljiv i estetski kompozitni panel za univerzalnu upotrebu, koji se sastoji od slojeva polietilena niske gustoće (LDPE) u jezgri i vanjskih površina od lakiranog aluminija. Vezivanje slojeva je napravljeno mehaničkim i kemijskim metodama, što čini da je ploča izuzetno otporna na raslojavanje i delaminaciju. Alubond je materijal idealan za izradu svih vrsta dizajna koji zahtijevaju laganu, ravnu i krutu površinu. Koristi se za vanjsku trajnu signalizaciju, izložbene i maloprodajne štandove i pločeitd. UV plošni tisak omogućuju izravan digitalni tisak, a alternativa je prethodno ispisati samoljepljivi materijal i aplicirati na alubond.


    </div>

    <h3>Pjenaste ploče</h3>
    <h4 class="mini_h4">Vrsta tiska: Latex, UV</h4>

    <div class="vel_info_text">
      Lagane pjenaste ploče sa poliuretanskom ili polistirenskom jezgrom i gornjim slojevima od različitih materijala.<br>
      KAPAline - izuzetno lagan kompozitni materijal s poliuretanskom jezgrom i obložen papirom.
      KAPAmount - lagan kompozitni pjenasti materijal s aluminijskim slojem oko jezgre koji ploči daje čvrstoću. <br>
      Stadur (Tusand)- lagan kompozitni pjenasti sendvič materijal sastavljen od dvije pjenaste PVC ploče sa ispunom od polistirena.<br>
    </div>

    <h3>Akrilne ploče (plexiglass)</h3>
    <h4 class="mini_h4">Vrsta tiska: Latex, UV</h4>

    <div class="vel_info_text">
      Akrilne ploče su kruti, prozirni, termoplastični materijal od polimetilnog metakrilata (PMMA). Prirodno je bezbojan i izuzetno proziran, a može biti obojan u neograničenu paletu boja. Ne reagira s brojnim korozivnim kemikalijama, pa je s toga dobar za vanjsku primjenu (otporan je na UV zrake i vremenske utjecaje općenito). Ploče se mogu strojno obrađivati i oblikovati, tako da ima brojne mogućnosti industrijske, obrtničke ili umjetničke primjene. Razlika između ekstrudiranih i lijevanih ploča je u načinu obrade ostale namjene su iste.
    </div>

    <h3>Pjenasti PVC (Forex)</h3>
    <h4 class="mini_h4">Vrsta tiska: Latex, UV</h4>

    <div class="vel_info_text">
      Pjenasti PVC poznatiji kao Forex, lagan je, a ipak čvst materijal, raspoloživ u raznim debljinama (1-10 mm, a ima i debljih) Upotrebljava se za digitalni tisak, rezanje slova ili nekih drugih oblika itd.
    </div>


    <h3>Razni kruti materijali</h3>
    <h4 class="mini_h4">Vrsta tiska: UV</h4>

    <div class="vel_info_text">
      Razni ravni, debeli i teški pločasti materijali kao drvo, metal, staklo i debela plastika često se mogu odmah tiskati na UV printera, ali mogu zahtjevati i poseban proces pripreme podloge (pred-premaz, "primeriranje” ili drugim tehnikama (elektrostatska korona) zbog boljeg prijanjanja boje na podlogu. Alternativa
      je ispis na samoljepive PVC materijale i apliciranje na materijal.
    </div>

    <h3>Banner materijali</h3>
    <h4 class="mini_h4">Vrsta tiska: Latex, UV</h4>

    <div class="vel_info_text">

      Banner materijal je obično izrađen od jakog PVC-a. Najčešće su u mesh i frontlit izvedbi, premda postoje i druge opcije. Mesh - mrežasta struktura koja pomaže da materijal bude vjetrootporan i samim tim se koristi na otvorenim (vanjskim) prostorima.
      Frontlit - nema mrežastu strukturu pa
      je otisak kvalitetniji i ima više detalja. Uglavnom se koristi za izložbe. Zbog
      loše otpornosti na vjetar izbjegava se koristiti u vanjskim prostorima gdje postoji mogućnost da ga vjetar prevrne ili otpuše. Backlit - sličan je frontlit-u, ali je tvornički proizveden da propušta više svjetla tako da je primarni izvor svjetla postavljen iza bannera. Ovo je vrlo korisno kod manjka prostora, kada se zbog sigurnosti ili estetike želi sakriti izvor svjetla.
      Blockout - vrlo snažan i težak materijal
      koji ima svojstvo da ne propušta svjetlost (čak ni najjaču). Koristi se ispred prozora ili izvora svjetala koje ne možemo kontrolirati npr. na izložbama i sajmovima pa ih na taj način "sakrijemo”.
    </div>



    <h3>Mesh</h3>
    <h4 class="mini_h4">Vrsta tiska: Latex, UV</h4>

    <div class="vel_info_text">

      Perforirana mrežasta PVC struktura pomaže da materijal bude vjetrootporan i samim tim se koristi na otvorenim (vanjskim) prostorima i pogodan je za vrlo velike "display-e” kao što je npr. oslikavanje kompletnih zgrada.

    </div>

    <h3>Canvas (platno)</h3>
    <h4 class="mini_h4">Vrsta tiska: Latex, UV</h4>


    <div class="vel_info_text">
      Tekstilni materijal sa bijelim "printabilnim” premazom. Uglavnom se montira (napinje) na okvir. Platno može biti potpuno glatko što garantira vrhunsku kvalitetu ispisa, a može biti sa reljefnom teksturom koja podsjeća na slikarsko platno.

    </div>

    <h3>Samoljepive print folije</h3>
    <h4 class="mini_h4">Vrsta tiska: Latex, UV</h4>

    <div class="vel_info_text">

      Samoljepive print folije su uglavnom
      od PVC ili PE materijala, bijele ili transparentne. Uglavnom se koriste za naljepnice, konturno rezane naljepnice ili prilikom oslikavanja vozila.
      Koristimo i naljepnice za prozorsku grafiku "One Way Vision” (rupičasta, perforirana) koja se koristi za oslikavanje prozora, izloga ili stražnjih stakla na vozilima. Vanjska strana stakla postaje reklamni prostor, a unutrašnja strana je sakrivena vanjskim promatračima.
      Podne naljepnice odlikuje visoka otpornost na grebanje i habanje. Za dodatnu zaštitu mogu se i laminirati.
    </div>

    <h3>Samoprijanjajući film</h3>
    <h4 class="mini_h4">Vrsta tiska: Latex, UV</h4>


    <div class="vel_info_text">
      Printabilni film koji prijanja uz glatku površinu. Drži se čvrsto ali se vrlo jednostavno može odlijepiti od podloge.
    </div>

    <h3>Magnetna folija</h3>
    <h4 class="mini_h4">Vrsta tiska: Latex, UV</h4>


    <div class="vel_info_text">
      Magnetne folije se koriste za držanje tiskanog materijala i lakših predmeta na metalnim podlogama poput hladnjaka ili automobila.
      Magnetska folija može poslužiti osim ostaloga i kao vizualni nosač reklame. Tako izradjena reklama se jednostavno skida, ne oštećuje površinu ispod nje kao druge samoljepljive folije. Eventualna grafika može biti primjenjena na magnetskoj foliji sitotiskom, offsetom, ili samoljepljivom folijom.
    </div>



    <h3>Tekstil</h3>
    <h4 class="mini_h4">Vrsta tiska: Latex, UV</h4>

    <div class="vel_info_text">
      Ink: DS, L, SS, ES, UV, specialist Koristi se za tiskanje tekstilnih bannera, zastava, dekoriranje izloga i sl.
    </div>

    <h3>Lentikularna leća</h3>
    <h4 class="mini_h4">Vrsta tiska: UV</h4>

    <div class="vel_info_text">

      Lentikularna folija je list sa lećama. Sastoji od puno leća postavljenih na paralelne trake sa jednakim optičkim karakteristikama. <br>
      List sa trakama lentikularnih leća je optički prozirni supstrat, s ravnom poleđinom koja omogućuje izravan ispis, ili hladno ljepljenje na ispisani papir. Dvije ili više slika na poleđini folije prožimaju na taj način da se pod jednim kutom promatranja vidi jedna, a pod drugim kutom druga slika. Na taj način dobivamo optički efekt kao 3D, animacija ili sl. Obično se koristi kod backlit display-a. Zahtjeva specijalizirani software za pripremu.
    </div>

    <h3>PP ploče (polipropilen)</h3>
    <h4 class="mini_h4">Vrsta tiska: UV</h4>

    <div class="vel_info_text">
      PP (polipropilen) je termoplastični materijal s polukristalnom strukturom. Ima veću snagu i čvrstoću i višu točku topljenja nego PE. Ovaj materijal može lako zamijeniti drvo, karton, PVC ili polistirensku pjenastu ploču.
      Materijal postoji u dvije različite strukture: saćastoj (mjehurići).i strukturalnoj (prugastoj).

    </div>

    <h3>PET-G ploče</h3>
    <h4 class="mini_h4">Vrsta tiska: UV</h4>


    <div class="vel_info_text">

      PET-G (Polyethylene Terephthalate Glycol)
      Ove ploče nude izvrstan omjer čvrstoće i težine, izvanrednu optičku jasnoću, vrhunsku kemijsku otpornost, trajnost, otpornost na požar, te se može 100% reciklirati, a propusnost svjetla je 90% (1 mm). Ključna prednost je u izuzetno jednostavnoj obradi, te mogućnosti tiska i raznih vrsta dekoracije. Debljine: 0,5 - 3 mm
      
      
      <div class="no_col_break">
        <h4 style="margin-top: 0;">PET G (Vivak)</h4>
        0,50mm<br>
        0,70mm<br>
        0,75mm<br>
        1,00mm<br>
        1,50mm<br>
        2,00mm<br>
        3,00mm<br>
      </div> <!-- end no col break -->      

    </div>

    <h3>EPLAK ploče</h3>
    <h4 class="mini_h4">Vrsta tiska: UV</h4>

    <div class="vel_info_text">
      Ekstrudirani polipropilen koji je razvijen
      i proizveden da bi pružio izvrsne karakteristike u raznim primjenama. Proizvodnja ploča je "eco-friendly”, sam proizvod se može u potpunosti reciklirati a prilikom izgaranja ne otpušta toksične tvari u atmosferu.
    </div>
    <!-- end info text -->
    
    <h3>HIPS ploče</h3>
    
    <img src="/img/info/hips_all.png" alt=""  style="max-width: 70%; min-width: 300px; display: block; margin: 10px 10px 60px; float: left; padding: 0;" />

    
  
    <br>
    
    <div class="vel_info_text">
     
      <div class="no_col_break">

        <b>HIPS (High Impact Polystyrene) </b> je modificirana formula <b>polistirena (PS)</b> sa mješavinom <b>polibutadiena</b>. To je lagan materijal, male gustoće od 1.06 g/cm3, sa visokom otpornošću na udarce, izuzetno pogodan za termičko oblikovanje i ne zahtijeva presušenje. Ove karakteristike omogućuju ekstrurziju i najkompleksnijih prostornih oblika. U svakom slučaju polibutadien smanjuje otpornost na kidanje i povećava transparentnost, dugotrajnost i stabilnost oblika na visokim temperaturama. Stoga se HIPS preporučuje za upotrebu u interijeru. Ima široku upotrebu u industriji i reklamnoj industriji.  

      </div> <!-- end no col break -->


      <div class="no_col_break">

        <b>Polibutadien</b> [butadienska guma BR] je sintetička guma. Polibutadienska guma je polimer nastao polimerizacijom monomera 1,3-butadiena. Polibutadien ima visoku otpornost na habanje i koristi se posebno u proizvodnji guma, koje troše oko 70% proizvodnje. Još 25% koristi se kao dodatak za poboljšanje žilavosti (otpornosti na udarce) plastike kao što su polistiren i akril...
        
      </div>
      
      <div class="no_col_break">

        <b>Polistiren</b> je polimer stirena i jedan je od najčešćih plastičnih materijala. Značajna primjena polistirena je u obliku ekspandiranih i ekstrudiranih pjena poznatih pod jednim od trgovačkih naziva kao "stiropor".

      </div>

    </div>
    
    <h3>HIPS Upotreba</h3>
    
      
      
     <div class="vel_info_text">
     
      <div class="no_col_break">
       <h4 style="margin-top: 0;" >1. Industrija</h4>
       
        <ul>
        
          <li>Kućišta i unutrašnjost hladnjaka (rashladni HIPS)</li>
          <li>Navlake za solarije</li>
          <li>Lutke za izloge</li>
          <li>Namještaj (dekorativni HIPS)</li>
          <li>Kućišta za elektroniku</li>
          <li>Čvrsta pakiranja industrijskih proizvoda,npr. kazete, diskovi, kutije za nakit itd.</li>
          <li>Transportne palete</li>
          <li>Kutije za sadnice, tegle, eksluzivna pakiranja (Flock)</li>
          <li>Spremnici za otpad</li>
        </ul>
        
        
      </div>
      
      
      <div class="no_col_break">
      
       <h4 style="margin-top: 0;" >2. Oglašavanje</h4>
       
        <ul>
        
          <li>Promotivne i oglasne ploče, oscilirajući znakovi itd.</li>
          <li>Oglasni uređaji, igrice i igračke</li>
          <li>Izložbeni štandovi</li>
          <li>Dvodimenzionalne oglasne ploče</li>
          <li>Svjetlosne kutije-Light-boxes</li>

        </ul>
        
        
      </div>
      
      
      
    </div>
    
    
    <h3>HIPS Korištenje i prednosti</h3>
    
    <div class="vel_info_text">
     
      <div class="no_col_break">

        <ul>

          <li>Mala težina</li>
          <li>Oblikovanje zagrijavanjem</li>
          <li>Jednostavnost obrade</li>
          <li>Otpornost na kemikalije</li>
          <li>Dobra čvrstoća</li>
          <li>Visoka otpornost na udarce i lomljene</li>
          <li>Široka paleta boja</li>
          <li>Veliki izbor dizajna površine</li>

        </ul>

      </div>
      
      
    </div>
    <br>
    <br>
    
    
    
    
    
    <h3>Smart-X</h3>
    
    
    <img src="/img/info/smart_all.png" alt=""  style="max-width: 70%; min-width: 300px; display: block; margin: 10px 10px 60px; float: left; padding: 0;" />

    
    <div class="vel_info_text">
      
      <b>Smart-X</b> je polistirenska ploča koja je 100% reciklabilna (može se reciklirati), PVC-free.
<b>Dolaze u debljinama 5 i 10 mm,  a debljina 19 mm se može projektno naručiti.</b>
Materijal se koristi <b> za sve POP/POS aplikacije</b>, razne stalke, hangere i sl.
Može se glodati, rezati nožem, čak i laserom, direktno tiskati i sl.

Smart-X jedinstvena je I lagana pjenasta ploča koja se koristi za sofisticirane aplikacije.
To je lagana plastična ploča načinjena od materijala otpornog na UV zrake I vremenske uvjete: polistirena (HIPSA) s jezgrom od ekspandiranog polistirena koji je otporan na vlagu.
SMART-X je jedina lagana ploča koja se moze koristiti za vanjsko apliciranje, bez znatnije promjene boja u periodu do dvije godine. Zbog glatke površine i velikih formata ploča je idealna za digitalni print.
<b>SMART-X</b> izrađena je od <b>100% polistirena</b> te je stoga ekološki material koji je <b>100% reciklabilan</b>.

      
     
    </div>
    
    
    
    
    <div class="vel_info_text">
     
      <div class="no_col_break">
        <h4 style="margin-top: 0;" >Smart-X Karakteristike</h4>
        <ul>

          <li>Vrlo stabilan i lagan material</li>
          <li>Otporan na vremenske uvjete: otpornost na vlagu i UV zrake</li>
          <li>Glatka površinska struktura</li>
          <li>Material daje odlične rezultate kod digitalnog direktnog tiska</li>
          <li>Dostupan u širinama do 2 metra</li>
          <li>Otpornost na temperature od -10°C do +70°C</li>
          <li>Niska otpornost tijekom konturnog glodanja</li>

        </ul>
      
      </div>
     
      <div class="no_col_break">
       
        <h4 style="margin-top: 0;" >Smart-X Obrada rubova</h4>
        
        <ul>
          <li>plastični W profili – lijepiti s bilo kojim ljepilom za plastiku</li>
          <li>kant trake za iverale s nanesenim ljepilom</li>
        </ul>
        
      </div>
      
      
      <div class="no_col_break">
       
        <h4 style="margin-top: 0;" >Smart-X Direktni tisak</h4>
        
        <ul>
          <li>može se tiskati na bilo kojem flatbed printeru UV direktnim tiskom s nisko potrošnim UV lampe.– što je viša postavka potrošnje UV lampi, tisak izgleda tamnije, te je vidljiva bilo kakva površinska nesavršenost, a također može doći i do savijanja manjih formata (zbog visoke temperature).</li>
          <li>prilikom rukovanja pločama potrebno je nositi pamučne rukavice.</li>
          <li>ne koristiti tekuća sredstva za čišćenje, površinu čistiti pomoću spreja s ioniziranim zrakom.</li>
          <li>45% relativne vlaznosti zraka pokazalo se vrlo uspješnim u borbi sa statičkim nabojem, ne samo kod smart-x materijala.</li>
          <li>Zaštitnu foliju potrebno je ukloniti polako, ali čvrsto kako bi se izbjeglo stvaranje dodatnog statičkog naboja.</li>
        </ul>
        
      </div>
      
    </div>  
    
    <br>
      
    <div class="vel_info_text">
     
      <div class="no_col_break">
        <h4 style="margin-top: 0;" >Smart-X Bojanje</h4>
        Boja: <b>BIJELA</b> <br>
        <br>
        Ploče se mogu obojati bilo kojim akrilnim ljepilom (solventne boje ne odgovaraju)
      </div>
      
      <div class="no_col_break">
        <h4 style="margin-top: 0;" >Smart-X Korištenje</h4>
        <ul>
          <li>Sve vrste plakata, osobito velikih formata ili dugačkih i širokih formata (drugi materijali se obično deformiraju).</li>
          <li>Optimalno za samostojeće stalke: direktni tisak, glodanje, neformirano slanje, sastavljanje na lokaciji.</li>
          <li>POS/POP materijali.</li>
        </ul>
      </div>
      
      <div class="no_col_break">
        <h4 style="margin-top: 0;" >Smart-X Vrste obrade</h4>
        <ul>
          <li>Digitalni tisak</li>
          <li>Laminacija</li>
          <li>Lakiranje</li>
          <li>Piljenje</li>
          <li>Konturno rezanje</li>
          <li>Lasersko rezanje</li>
          <li>V-cut</li>
          <li>Lijepljenje</li>
        </ul>
      </div>
      
    
    </div>  
    
    <div class="vel_info_text">

      <div class="no_col_break">
      
      <h4 style="margin-top: 0;" >Smart-X Debljine:</h4>
      <ul>
        <li>5 mm</li>
        <li>10 mm</li>
        <li>19 mm – na upit</li>
      </ul>
      
      </div>
      
      <div class="no_col_break">
      
      <h4 style="margin-top: 0;" >Smart-X Standardni formati:</h4>
      <ul>      
        <li>1220x2440 mm</li>
        <li>1220x3050 mm</li>
        <li>2030x3050 mm</li>
      </ul>
      
      <h4 style="margin-top: 0;" >Smart-X Zaštitna folija:</h4>
      <ul>
        <li>Jednostrano za debljine 5 i 10 mm, obostrano za debljinu 19 mm.</li>
      </ul>
     </div> <!-- end no col break -->

      <div class="no_col_break">
        <h4 style="margin-top: 0;" >Smart-X Ekološki aspekti</h4>
        <ul>
          <li>Mono-materijal bez dodanih ljepila, 100% polistiren</li>
          <li>100% reciklabilan</li>
          <li>95% otpada SMART-X proizvodnje prikuplja se i reciklira u tvornicama</li>

        </ul>
      </div>
      
    </div> 
    
    
    
    
    <h3>Špaga</h3>

    <div class="vel_info_text">
      <div class="no_col_break">
        <img src="/img/info/spaga_1.jpg" alt="" style="max-width: 90%; padding: 0;">
      </div> <!-- end no col break --> 
     
      <div class="no_col_break">
        <h4 style="margin-top: 0;">Pamigo d.o.o.</h4>
        Ilica 510, 10090 Zagreb, Hrvatska<br>
        OIB: 75444587892<br>
        Tel: 00385 1 3465718<br>
        sandra@pamigo.net<br>
        www.pamigo.hr<br>
        <br>
        Promjer: 5 mm<br>
        VPC: 2,50 kn/metar + PDV<br>
      </div>
      
    </div>
    <!-- end info text -->
    <div class="vel_info_text">
      <div class="no_col_break">
        <img src="/img/info/spaga_2.jpg" alt="" style="max-width: 90%; padding: 0;">
      </div> <!-- end no col break --> 
     
      <div class="no_col_break">
        <h4 style="margin-top: 0;">ENDI LINE d.o.o.</h4>
        Zagrebačka 60<br>
        10380 Sveti Ivan Zelina<br>
        www.endi.hr<br>
        Tel: +385 1 2060 212<br>
        Endi Line d.o.o. <br>
        <br>
        endi.line@zg.t-com.hr<br>
        Tomislav Gorički <br>
        <br>
        Promjer: 3 mm<br>
        VPC: 0,64 kn/metar + PDV.<br>
        <br>
        Vrijeme izrade 5-7 radnih dana.<br>
      </div>
      
    </div>
    
    <div class="vel_info_text">

      <div class="no_col_break">
        <h4 style="margin-top: 0;">Univez Filipović d.o.o.</h4>
        Dobavljač za špage  sa plasticnim završetkom za vrećice, kutije i gumice sa metalnim završetkom. <br><br>
        10380 Sveti Ivan Zelina<br>
        Tel: 098 4710 92<br>
      </div>
      
    </div>
    
    
    <h3>Pjena za pakiranje</h3>
    <h4 class="mini_h4">Pjenasti uložak</h4>
    <div class="vel_info_text">
      <div class="no_col_break">
        Direktan dobavljač iz Njemačke:
        <br>
        <a href="ttps://www.koepp.de/">KOEPP.DE</a>
        <br>
        <b>Robert Galos</b> <br>
        Tel: +49 (0) 241 16 60 5-29 <br>
        Fax: +49 241 16605-55 <br>
        Mail:  r.galos@koepp.de <br>
        
      </div>
      <div class="no_col_break">
        Distributer iz Slovenije:
        <br>
        <b>Boris Lenko</b> <br>
        <b>Commerce trgovina d.o.o., Mencingerjeva 7, 1000 Ljubljana</b> <br>
        Tel.: +386(0)1 4741-104 <br>
        GSM: +386(0)31 614-223 <br>
        Fax: +386(0)1 4741 - 128 <br>
        E-mail: boris.lenko@commerce-lj.si <br>
      </div>
      
      
    </div>
    
    
    
  </div>
  <!-- end info page -->




  <h2 id="dimenzije_info">Dimenzije i Tok</h2>

  <div class="vel_info_page">
    <h3>Određivanje dimezija ploča</h3>
    <div class="vel_info_text">
      <img src="/img/info/Image350.jpg" alt="">
      Ploče označavamo <b style="color: red;">PO VALU X KONTRA VAL</b>, nebitno koja je dimenzija duža bitan je smjer vala. <br>
      (nabavljamo materijal od više dobavljača i svaki od njih ima svoje pravilo tako da vam se može dogoditi da na etiketi piše drugačije, ali <u>naše pravilo</u> je ovo <b>PO VALU X KONTRA VAL</b>, tako da mozete zanemariti napisano na cedulji dobavljača)
    </div>

  </div>
  
  <div class="vel_info_page">
    <h3>Određivanje dimenzija proizvoda</h3>
    <div class="vel_info_text">
      <img src="/img/info/Image343.jpg" alt="">
      <h4>Kutije</h4>

      <b>Kutije</b> imaju svoju <b style="color: red;">DUŽINU x ŠIRINU x VISINU</b> <br>
      i <u>uvijek</u> ih pišemo tim redom. <br><br>

      Voditi računa da s kupcima i interno uvijek razgovaramo o <b>unutarnjim dimenzijama kutija.</b><br>
      <br>
      (Ona stranica gdje kutiju otvarate smatra se DUŽINOM - UVIJEK UNUTARNJA DIMENZIJA !!! ).<br>
      Radi lakšeg razumjevanja crteža nisu označene unutarnje dimezije. <br>
      
      <!--
      <b>STALCI</b><br>
      ----- TODO ---- nije dovršeno
      -->
      
    </div>
  </div>  
  
  <div class="vel_info_page">
    <h3>Papir - dugi i kratki tok (smjer vlakanaca)</h3>
    <div class="vel_info_text">
     
     
     
      <div class="no_col_break">
        <img src="/img/info/papir_smjer_vlakanaca.png" alt="" style="padding-bottom: 5px;">
        <div style="font-size: 12px; text-align: center;">Slika 1. Tok papira označen crvenim strelicama (dugi tok)</div><br>
        <img src="/img/info/mokri_papir.png" alt="" style="padding-bottom: 5px;">
        <div style="font-size: 12px; text-align: center;">Slika 2. Tok papira označen žutim strelicama</div><br>
      </div> <!-- end no col break --> 
      
      <div class="no_col_break">
      
        <h4 style="margin-top: 0;">TOK PAPIRA</h4>
        Tok papira može se odrediti kidanjem (slika 1.) ili na način da namočimo papir i vidimo kako se isti savija (slika 2.)<br>
        Tok papira označava se suprotno od valovite ljepenke (npr. 700x1000 je dugi tok papira)<br>
      
      </div> <!-- end no col break -->
      
    </div>
    
    <br>
    
    <div class="vel_info_text">
     
     
      <div class="no_col_break">
        <img src="/img/info/smjer_vlakana_pillow_box.png" alt="" style="padding-bottom: 5px;" >
        <div style="font-size: 12px; text-align: center;" >Slika 3.</div><br>
      </div> <!-- end no col break --> 
      
      <div class="no_col_break">

        <h4 style="margin-top: 0;">Pillow box kutija</h4>
        Tok papira na pillow boxu paralelan je s klapnom jer u suprotnom dolazi do deformacije kutije (slika 3.)<br>
        Budući da tok papira paralelan s klapnom može uzrokovati probleme na ljepilici potrebno je pojačati big na klapni da ga ljepilica lakše prelomi <br>
        Iznimka može biti pillow box većih dimenzija kod kojeg nužno ne dolazi do pucanja papira na nepredviđenim mjestima (kao na slici 3) <br>

      </div> <!-- end no col break -->

    </div>
    
    <div class="vel_info_text">
     
     
      <div class="no_col_break">
        <img src="/img/info/kutija_215.png" alt="" style="padding-bottom: 5px;" >
        <div style="font-size: 12px; text-align: center;" >Slika 4.</div><br>
      </div> <!-- end no col break --> 
      
      <div class="no_col_break">

        <h4 style="margin-top: 0;">Kutija F215</h4>
         
          Ukoliko se kutija F215 od papira lijepi na ljepilici tok papira okomit je na klapnu kako bi se izbjegle nepravilnosti kod lijepljenja i kako bi kutija bila što urednije zalijepljena.<br>
          Problem koji se javlja kada je tok papira okomit na klapnu je oslabljeno dno budući da je velika vjerojatnost da će se dno bigati gdje ne bi trebalo (slika 4.)<br>
          Problem oslabljenog dna moguće je riješiti lijepljenjem prozirne naljepnice na dno ili preinakom samosloživog dna u automatsko dno.<br>


      </div> <!-- end no col break -->

    </div>
    
    
    
  </div>
  
  
  

  <div class="vel_info_page">

    <h3>Formati (dimenzije) ploča</h3>

    <img src="/img/info/1.jpg" alt="" style="float: left; max-width: 500px;">
    <img src="/img/info/2.jpg" alt="" style="float: left; max-width: 500px;">

    <div class="vel_info_text col_count_2">

      <div class="no_col_break">
        <div class="table_wrap">
          <table class="info_table" style="width: 100%;">
            <tbody>

              <tr>
                <td><b>Veličina</b></td>
                <td><b>Dimenzija u mm</b></td>
                <td><b>Veličina</b></td>
                <td><b>Dimenzija u mm</b></td>
              </tr>


              <tr>
                <td>B0</td>
                <td>1000 x 1414 mm</td>
                <td>A0</td>
                <td>841 x 1189 mm</td>
              </tr>

              <tr>
                <td>B1</td>
                <td>707 x 1000 mm</td>
                <td>A1</td>
                <td>594 x 841 mm</td>
              </tr>

              <tr>
                <td>B2</td>
                <td>500 x 707 mm</td>
                <td>A2</td>
                <td>420 x 594 mm</td>
              </tr>

              <tr>
                <td>B3</td>
                <td>353 x 500 mm</td>
                <td>A3</td>
                <td>297 x 420 mm</td>
              </tr>


              <tr>
                <td>B4</td>
                <td>250 x 353 mm</td>
                <td>A4</td>
                <td>210 x 297 mm</td>
              </tr>



              <tr>
                <td>B5</td>
                <td>176 x 250 mm</td>
                <td>A5</td>
                <td>148 x 210 mm</td>
              </tr>

              <tr>
                <td>B6</td>
                <td>125 x 176 mm</td>
                <td>A6</td>
                <td>105 x 148 mm</td>
              </tr>


              <tr>
                <td>B7</td>
                <td>88 x 125 mm</td>
                <td>A7</td>
                <td>74 x 105 mm</td>
              </tr>

              <!--
        <tr>
        <td>_______</td>
        <td>_______</td>
        <td>_______</td>
        <td>_______</td>
        </tr>
        -->

              <tr>
                <td>B8</td>
                <td>62 x 88 mm</td>
                <td>A8</td>
                <td>52 x 74 mm</td>
              </tr>



              <tr>
                <td>B9</td>
                <td>44 x 62 mm</td>
                <td>A9</td>
                <td>37 x 52 mm</td>
              </tr>

              <tr>
                <td>B10</td>
                <td>31 x 44 mm</td>
                <td>A10</td>
                <td>26 x 37 mm</td>
              </tr>
            </tbody>
          </table>
        </div>

      </div> <!-- end no col break -->

      Mi najčešće koristimo B formate papira i kartona. <br>
      Svaki format se prepolavlja na slijedeći. Pa je tako B1 polovica B0, B2 polovica B1... <br>
      Radi lakšeg komuniciranja kažemo da je B0 1000 X 1400 mm, a ne kako je navedeno u standardima (1000 X 1414 mm) <br>
      Za B1 koristimo 700 X 1000 mm

    </div>

  </div>
  
  <br>

  <div class="vel_info_page">

    <h3>"American" transportna kutija (FEFCO 201 ili skraćeno F201)</h3>
    
    <div class="vel_info_text">
      TRANSPORTNA KUTIJA "AMERICAN" prema FEFCO standardu označava se FEFCO 201 (skraćeno F201). Mi se u radu koristimo skaćenim oznakama FEFCO standarada F+broj standarda. Kao i kod svih ostalih proizvoda označavamo je <b>DUŽINA x ŠIRINA x VISINA</b>
      <br>
      Takve kutije mogu se proizvoditi iz ploče ili iz sloter elementa. <br>
      Na slici ispod vidljiva je rastvorena kutija sa svim dodacima i dimenzijama. <br>
      <br>
      <b style="color: darkred;">
        VAŽNA NAPOMENA:<br>
        Prilikom konstrukcije voditi računa da postoji razmak od 2-5 mm između poklopaca i "ušiju" (kada se gleda prirez kutije) kako bi se kutija lakše preklapala!
        <br>
        
      </b>

      <br>

      <div class="no_col_break">
        Kada F201 radimo iz ploče onda ploču računamo na slijedeći način:
        <br>
        <img src="/img/info/Image382.jpg" alt="">
      </div> <!-- end no col break -->

      PO VALU računamo: ŠIRINA(Š) + VISINA (V) + 2*DODATAK A
      KONTRA VAL: KLAPNA(25-30 mm)+ 2* ŠIRINA(Š)+ 2*DUŽINA(D)+3*DODATAK B
      Ili vizualno

      <img src="/img/info/Image389.jpg" alt="">

      DODATAK: <br>
      <ul>
        <li>3mm ako se radi o troslojnom materijalu</li>
        <li>5mm ako se radi o peteroslojnom materijalu</li>
      </ul>

    </div>
       
        
    <h4 style="color: darkred;">NARUČIVANJE SLOTERA OD DOBAVLJAČA</h4>
        
    <b style="color: darkred;">
      Sloter naručujemo u ovim slučajevima: <br>
      1) Ako je ukupna površina materijala za cijelu nakladu veća od 1000 kvadratnih metara. <br>
      2) Ako je rok isporuke dovoljno daleko tj. ako imamo cca 15 dana za isporuku (tako da nam naručeni sloter na vrijeme stigne od dobavljača).<br>
    </b>

    <br>
    
    <img src="/img/info/american_dimenzije_schema.jpg" alt="" style="min-width: 100%;">

    <br>
    
    <div style="width: 90%; max-width: 100%; text-align: center; margin: 0 auto;">
      Kada za F201 naručujemo TROSLOJNI sloter od dobavljača:
      <img src="/img/info/3_sloja_izracun_formata_po_dimenzijama_kutije.jpg" style="max-width: 800px; margin-top: 0;" />
    </div>

    <div style="width: 90%; max-width: 100%; text-align: center; margin: 0 auto;">
      Kada za F201 naručujemo PETEROSLOJNI sloter od dobavljača:
      <img src="/img/info/5_sloja_izracun_formata_po_dimenzijama_kutije.jpg" style="max-width: 800px; margin-top: 0;" />
    </div>
    


    <div class="vel_info_text">

      <div class="no_col_break">

        <h4 style="margin-top: 0;">Korisne napomene</h4>
        Svaki put dok radite, složite kutiju, pogledajte kakva je, kako se sastavlja i lakše će vam
        biti raditi. Provjerite prema radnom nalogu dimenzije, na radnom nalogu stoji i materijal, pa testirajte svoje znanje.
        <br>
        <br>
        
      </div>
      
      <div class="no_col_break">
        <h4 style="color: red;" >Varijacije veličina</h4> 
        <b style="color: red;">
          Kutije od BC vala bit će 2 mm manje unutarnje dimenzije od nazivne <br>
          Kutije od EB vala bit će 2 mm vece unutarnje dimenzije od nazivne <br>
          Kutije od B vala bit će točnih unutarnjih dimenzija <br>
          Kutije od mikrovala bit će 2 mm veće unutarnje dimenzije od nazivne <br>
        </b>
      </div>

    </div>
    
    
    <h3>Dimenzije ploča kod dobavljača</h3>

    <div class="vel_info_text">

      <div class="no_col_break">

        <h4 style="margin-top: 0;">Model d.o.o.</h4>

        <b>Rok isporuke:</b> do 10 radnih dana <br>
        <b>Minimalna širina ploča:</b> 300 mm <br>
        <b>Minimalna dužina ploča:</b> 700 mm <br>
        <b>Maksimalna širina ploča:</b> 2460 mm <br>
        <br>
        - za ploče širine između 1800-2200 mm zaračunavamo dodatno 10% na cijenu<br>
        <br>
        <b>Maksimalna dužina ploče:</b> 2800 mm <br>
        <br>
        - za ploče dužine između 2800-4000 mm zaračunavamo dodatno 10% na cijenu
        <br>
        <br>

        <b>Napomene vezane na narudžbu posebnih kvaliteta:</b><br>
        a) minimalna širina za narudžbu je puna radna širina, tj. 2320 mm i 2460 mm<br>
        b) minimalna količina za narudžbe navedene u tablici se odnose na svaku dimenziju, ne na ukupan zbroj m2 u kvaliteti<br>

      </div> <!-- end no col break -->

     <div class="no_col_break">

        <h4>Bilokalnik d.o.o.</h4>
        <b>Minimalna širina (na Velpapu):</b> 300 mm <br>
        <b>Minimalna dužina (na Velpapu):</b> 700 mm <br>
        <b>Maksimalne širina korugatora:</b> 2100 mm <br>
        <b>Maksimalna širina (na Velpapu):</b> <br>
        2200 (bez obreza), a 2160 mm sa obrezom. <br>

        <b>Maksimalna dužina:</b> 3500 mm <br>


        <h4>Jakšapack d.o.o.</h4>
        <b>Minimalna širina:</b> 300 mm<br> 
        <b>Minimalna dužina:</b> 650 mm<br>
        <b>Maksimalne širina neobrazeni karton:</b> 1800 mm<br>
        <b>Maksimalna širina obrezanog kartona:</b> 1750 mm<br>
        <b>Maksimalna dužina:</b> 3500 mm<br>
      </div>
      
      <div class="no_col_break">
        <h4>Dunapack d.o.o.</h4>
        <b>Format ploče:</b> min 250 x max 2465<br>
        <b>Širine na koje kombiniraju:</b> 2115, 2315 i 2465 mm<br>
      </div>
      
    </div> <!-- end info text -->
    
    <br>
    
    <h4 style="margin-top: 0;">Inpol Papier </h4>
    

    <div class="vel_info_text col_count_2">
      
      
      <div class="no_col_break">
        
        <div class="table_wrap">
         
          <table class="info_table" style="width: 100%;">
            <tbody>

              <tr>
                <td><b>Trade names</b></td>
                <td><b>Colour</b></td>
                <td><b>Bottom layer gsm</b></td>
                <td><b>Fluted paper gsm</b></td>
                <td><b>Total weight gsm</b></td>
                
              </tr>

              <tr>
                <td>2TEW</td>
                <td>White</td>
                <td style="text-align: center;" >120</td>
                <td style="text-align: center;" >100</td>
                <td style="text-align: center;" >241</td>
              </tr>
              
              <tr>
                <td>2TEBr</td>
                <td>Brown</td>
                <td style="text-align: center;" >120</td>
                <td style="text-align: center;" >100</td>
                <td style="text-align: center;" >241</td>
              </tr>
              

            </tbody>
          </table>
          
        </div>

        <br>
         
        Minimalna širina ploče: <b>500mm</b><br>
        Minimalna duljina ploče: <b>500mm</b><br>
        <br>
        Maksimalna širina ploče: <b>1650mm</b><br>
        Maksimalna duljina ploče: <b>2000mm</b><br>
        <br>
        GD liner, GC: (ovo je za GD I GC papire)<br>
        Minimalna širina ploče: <b>450mm</b><br>
        Minimalna duljina ploče: <b>450mm</b><br>
        <br>
        Maksimalna širina ploče: <b>2300mm</b><br>
        Maksimalna dužina ploče: <b>2100mm</b><br>
        <br>
        Minimalna količina za naručivanje je <b>1000m2</b><br>
        Maksimalna visina palete: <b>2m</b><br>
        <br>
        Zbog distance <b>rok isporuke su nam dali okviran 10 dana</b>, ali ako je nešto hitno možemo se probati dogovoriti da bude manji. Inače je do 5 dana.<br>
        Palete se omataju folijom (stretchaju) hladne kada materijal odstoji i to se radi samo kada su ploče u dva reda na paleti.<br>
        <b>Cijenik je u m2 za narudžbe iznad 1000m2 po proizvodu, doplata ispod 1000m2 je 0,07 EUR po kvadratu.</b><br>
        <b>Rokovi plaćanja 60 dana, plaćanje 14 dana 2% popusta.</b><br>
        <br>
        
      </div>
      <div class="no_col_break">
        
               

        Parametri:<br>
        Val: <b>E</b><br>
        Apsolutna vlažnost: <b>6 – 9%</b><br>
        Ukupna tolerancija u gsm: <b>± 5%</b><br>
        <br>
        <br>
        Pakiranje:<br>
        Pakirano na palete poprečno povezane sa 4 trake<br>
        <br>
        Rukovanje teretom:<br>
        Valoviti karton zahtijeva posebne uvjete temperature, vlage/vlage i eventualno ventilacije.<br>
        Najpovoljniji raspon temperature prilikom prijevoza: 10 - 25°C. <br>
        Optimalna relativna vlažnost je 50% u umjerenoj klimi.<br>
        <br>
        Robu je nužno zaštititi od vlage (kiše, snijega) tijekom rukovanja teretom, jer postoji opasnost od gubitaka uzrokovanih bubrenjem i kidanjem pojedinih slojeva.<br>
        <br>
        Nepravilno rukovanje tijekom utovara i istovara i skladištenja povlači za sobom opasnost od zapinjanja materijala. Kao posljedica toga, slojevi papira postaju neupotrebljivi. Tada su prikladni samo za korištenje kao otpadni papir.<br>
        <br>

        
        
        
      </div>
      
      
      
      
    </div>
  
  
<br>
<br>


<h2 id="vrste_tiska_info">Vrste tiska</h2>

  <div class="vel_info_page">

    <h3>Visoki tisak (knjigotisak, fleksotisak...)</h3>
    <div class="vel_info_text">
      Visoki tisak koristi uzdignutu površinu tiskovne forme za zadržavanje boje koja se na formu nanosi prolaskom valjka s bojom ili pritiskanjem forme preko tampona sa bojom. Jednostavan primjer visokog tiska je linorez na kojeg se nanosi boja i onda se pritiskom linoreza na papir otiskuje slika sa onih dijelova koji su izbočeni ili viši od ostalih. Drugi jednostavni primjer je uredski žig. Moderne tiskovne forme za visoki tisak se izrađuju uglavnom od fotopolimera, gume ili metala, a materijal izrade ovisi o primjeni same tiskovne forme. Tiskovne forme za visoki tisak nisu skupe i relativno se brzo izrađuju.
    </div>
    <h3>
      Duboki tisak (bakrotisak, tampon tisak,<br>
      čelični tisak tj. slijepi/reljefni tisak)
    </h3>
    
    <div class="vel_info_text">
      Duboki tisak, za razliku od visokog tiska, koristi udubljene dijelove tiskovne forme za nanos boje na tiskovnu površinu. Boja se nanosi na tiskovnu formu te se prešanjem dalje prenosi na tiskovnu površinu.
      Tehnikom slijepog tiska, tiskom bez boje se postiže reljefnost otiska i ta se tehnika koristi kod izrade ukrasnih detalja na svečanim tiskanicama.
      Bakrotisak ima mogućnost najveće kvalitete otiska, međutim izrada i priprema tiskovne forme je izrazito skupa i dugotrajna, tako da se bakrotisak danas koristi samo kod velikih naklada poput novina, rotoromana i džepnih knjiga.
    </div>
    
    <h3>Propusni tisak (sitotisak)</h3>
    <div class="vel_info_text">
      Sitotisak je tehnika propusnog tiska, koji kako i samo ime kaže propušta boju kroz tiskovnu formu. Tiskovna forma kod propusnog tiska predstavlja sito kod kojeg je mrežica napeta na okvir. Metalna ili plastična mrežica ostavlja otvorena područja na mjestu gdje se boja želi otisnuti, a zatvorena područja tamo gdje ne želi. Prednost sitotiska pred ostalim tehnikama je ta da je tiskovna forma, u ovom slučaju mrežica, jeftina i brzo se priprema, moguće je tiskati većom raznovrsnošću boja i to na bilo kojoj podlozi, a boje su visokog sjaja.
      Unatoč navedenim prednostima, brzina tiska ovom tehnikom je niska, deblji slojevi troše više boje i ne mogu se tiskati kvalitetni višetonski rasteri.
      Propusni tisak se jako rijetko koristi kao samostalno rješenje otiska nekog nakladničkog proizvoda te je više u službi dorade i oplemenjivanja tiska.
    </div>

    <h3>Plošni tisak (offset tisak)</h3>
    
    <div class="vel_info_text">
      Litografija (kamenotisak), plošni je tisak, što znači da su tiskajuće i netiskajuće površine na istoj ravnini. Za razliku od ploče za visoki tisak, ploča za litografiju je ravna i glatka. Troškovi ploča su manji, a vrijeme obrade je kratko.
      Plošni tisak se koristi za tisak većine grafičkih proizvoda. U plošni tisak ubrajamo offsetni tisak koji se vrši pomoću rotacije ili se tiska na arke. Offsetni tisak se vrši tako da se boja sa tiskovne forme transferira na gumeni plašt, a sa gumenog plašta na papir, odnosno na tiskovnu površinu. To je indirektni postupak umnožavanja tiskovne forme na tiskovnu podlogu. Osnovni princip offsetnog tiska je u međusobnoj odbojnosti ulja i vode.
      Najveća prednost offsetnog tiska je njegova vrhunska kvaliteta i vrlo povoljna cijena za veće naklade.
    </div>
    <h3>Digitalni tisak </h3>
    <div class="vel_info_text">
      Digitalni tisak je moderna tiskarska tehnika bez tiskovne forme i zbog toga sama izrada otiska je mnogo brža te se lakše može mijenjati. On znatno pojednostavljuje tiskarski poces, jer omogućuje otisak malih i srednjih naklada, bez izrade filmova i ploča kao što
      je slučaj u offsetnom tisku. Digitalni tisak je izuzetno pogodna tehnika za makete, pokazni marketinški primjerak i manje naklade uz mogućnost personalizacije tiska (svaki otisak može biti oslovljen na drugu osobu). Digitalnim tiskom možemo otiskivati na razne površine poput papira, foto papira, platna, stakla, metala, mramora te ostalih površina.
      Kod same tehnike digitalnog tiska, u većini slučajeva, tinta ili toner ne prodiru u sam materijal na kojeg se tiska nego se na površini materijala stvori tanki sloj otiska koji se dodatno može učvrstiti na površinu. Najčešće korištene tehnike digitalnog tiska su ink- jet digitalni tisak i elektrofotografski tisak koji je poznat pod nazivom laserski tisak. Kada tiskamo pomoću tinte, odnosno ink-jet tehnikom, to činimo pomoću UV postupka koji stvrdnjava i dodatno veže polimer za površinu, kod tonera, odnosno elektrofotografskog tiska, pri samom procesu grijanja koristi se posebna tekućina koja pomaže prianjanje samog otisnutog sloja.
      Ink jet tehnika tiska ima nekoliko inačica, no sve tehnike funkcioniraju na principu direktnog puštanja boje na tiskovnu površinu putem mlaznica, dok se kod elektrofotografije sam proces tiskanja može podijelit na 5 faza. To su slikanje pomoću LED-a ili laserskih
      zraka, bojanje odnosno tisak, prijenos tonera, stapanje tonera za bolje prianjanje na tiskovnu površinu te čišćenje ostataka tonera na cilindru koje se obavlja na samom fotokonduktorskom bubnju odnosno cilindru kako bi pripremili stroj za idući otisak.
      Navedene tehnologije digitalnog tiska su tehnike koje u potpunosti dominiraju unutar digitalnih tehnika tiska te zauzimaju sve veći značaj. Elektrofotograija i Ink-jet u sve većoj mjeri otimaju dio kolača koji pripada offsetnom tisku unutar industrije.
    </div>
  </div>

<h2 id="velprom_tisak_info">Tisak strojevi</h2>

  <div class="vel_info_page">
    
    <h3>HP Latex 365 (digitalni tisak, inkjet printer iz role)</h3>

    <div class="vel_info_text col_count_2">

      <img src="/img/info/3.jpg" alt="">

      <div class="no_col_break">
        Godina proizvodnje: 7/2016. <br>
        Dimenzija role: <b>254 do 1625 mm</b> <br>
        Rezolucija: do <b>1200x1200 dpi</b> (točaka po inču) <br>
        <br>

      </div>

      <div class="no_col_break">
        <!--Brzina ispisa: <br>-->
        <ul>
          <li>91 m2/hr - Max Speed (1 pass)</li>
          <li>31 m2/hr - Outdoor High Speed (4 pass)</li>
          <li>23 m2/hr - Outdoor Plus (6 pass)</li>
          <li>17 m2/hr - Indoor Quality (8 pass)</li>
          <li>14 m2/hr - Indoor High Quality (10 pass)</li>
          <li>6 m2/hr - Backlits, Textiles, and Canvas (16 pass)</li>
          <li>5 m2/hr - High Saturation Textiles (20 pass)</li>
        </ul>
      </div>
      Maksimalni otisak u širinu je 1600 mm, maksimalna širina role 1625 mm, najčešće širine rola su 1370 mm i 1520 mm, duljina nije strogo određena. Print se može protezati onoliko koliko je duljina role.
      Direktni tisak 4/0 tj. 6/0 (Black, cyan, light cyan, light magenta, magenta, yellow, HP Latex Optimizer).
      Mogućnost ispisa na medije za interijere i eksterijere, mat i sjajne papire, samoljepljive folije, mat i sjajne PVC folije, prozirne i mat filmove, platna, tekstile itd.
      Moguć i obostran tisak, ali samo na materijalu koji je obostrano obrađen za tisak (coated), prije drugog prolaza rola se treba premotati.
      Naš latex nema kadicu tako da ukoliko printamo na mrežaste, rupičaste materijale, onda ti materijali moraju imati liner/podlogu kako nam boja ne bi mazala stroj.

    </div>

    <br>

    <h3>Fuji Acuity LED 1600 II (UV printer)</h3>

    <div class="vel_info_text col_count_2">
      <img src="/img/info/4.jpg" alt="" style="width: 90%;">

      <div class="no_col_break">
        Godina proizvodnje: 2016.<br>
        Maksimalna širina ispisa:<br>
        1610 mm <br>
        <b style="color: red;">BITNO: Svaki dan promućkati kazete s bojom!</b><br>
        <br>
        Maksimalna debljina (čvrstih/pločastih) materijala:<br>
        13 mm (18 mm uz korištenje "trikova”)<br>
        <br>
      </div>
      <div class="no_col_break">
        Težina materijala za tisak:<br>
        <b>do 12 kg (za čvrste/pločaste materijale)</b> <br>
        <b>do 25 kg (za materijale na roli) <br></b>

        Vrsta tiska: <b>direktni UV tisak</b>
      </div>

      <div class="no_col_break">
        <div class="table_wrap">
          <table class="info_table" style="width: 100%;">
            <tbody>

              <tr>
                <td><b>Mode</b></td>
                <td><b>Broj prolaza</b></td>
                <td><b>Rezolucija (dpi)</b></td>
                <td><b>Max. brzna (m2/hr)</b></td>
              </tr>

              <tr>
                <td>Express</td>
                <td>6 pass bi-di</td>
                <td>600x300</td>
                <td>33</td>
              </tr>

              <!--
        <tr>
        <td>_______</td>
        <td>_______</td>
        <td>_______</td>
        <td>_______</td>
        </tr>
        -->

              <tr>
                <td>Production</td>
                <td>10 pass bi-di </td>
                <td>600x500</td>
                <td>20</td>
              </tr>

              <tr>
                <td>Standard</td>
                <td>16 pass bi-di </td>
                <td>900x800</td>
                <td>13</td>
              </tr>

              <tr>
                <td>Quality</td>
                <td>24 pass bi-di </td>
                <td>1200x1200</td>
                <td>8,3 </td>
              </tr>


              <tr>
                <td>High Quality</td>
                <td>48 pass bi-di </td>
                <td>1200x1200</td>
                <td>4,2 </td>
              </tr>


            </tbody>
          </table>
        </div>
      </div> <!-- end no col break -->

    </div>
    <!-- end info text -->
    <br>

    <h3 style="float: none; clear: both;">HP Scitex FB15500 (UV printer)</h3>

    <div class="vel_info_text col_count_2" style="float: none; clear: both;">

      <img src="/img/info/5.jpg" alt="">

      Godina proizvodnje: 2014. <br>
      Dimenzija materijala za tisak:<br>
      <b>1600x3200 mm (debljina do 25 mm, minimalna debljina papira 0,8 mm)</b> <br>
      Težina materijala za tisak:<br>
      <b>do 12 kg (za automatsko ulaganje) i do 40 kg (za ručno ulaganje)</b><br>
      Rezolucija: <b>do 1200 dpi (točaka po inču) - ???</b> <br>
      <br>
      <div class="no_col_break">
        Brzina ispisa:
        <ul>
          <li><b>Sample 164 m2/hr (32 stolova/sat)</b></li>
          <li>Text 297 m2/hr (58 stolova/sat)</li>
          <li>Fast sample 333 m2/hr (65 stolova/sat)</li>
          <li><b>High Quality POP 399 m2/h (78 stolova/sat)</b></li>
          <li><b>POP Production 492 m2/hr (96 stolova/sat)</b></li>
          <li><b>Production 579 m2/hr (113 stolova/sat)</b></li>
          <li>Fast production 650 m2/hr (127 stolova/sat)</li>
        </ul>
      </div>
      Vrsta tiska: <b>direktni UV tisak</b>
      <br>
      <br>
      Imamo mogućnost sjajnog efekt (glossy). Za postizanje sjajnog efekta segmentno (samo na jednom dijelu) moraju se pripremiti 2 PDF-a. Sjajni efekt može usporiti rad strojara/stroja, ali što se tiče potrošnje, ona je jednaka.
      <div class="no_col_break">
        Rukovanje materijalom: <br>
        <br>
        <ul>
          <li>manualno tj. ručno ulaganje i izlaganje (direktno na stol) + ljepljenje trakom što izrazito smanjuje brzinu tiska te 100 prolaza na stroju može trajati i do 8h,</li>
          <li>automatski - automatsko ulaganje, automatsko izlaganje</li>
          <li>više araka odjednom, do 4 komada (Multi Loader)</li>
        </ul>
      </div>
      Potrošnja boja za kalkulaciju je 7-10 kn/m2 ukoliko nemamo pripremu, stvaran trošak boje je 38 EUR po litri (po boji). Sa jednom litrom se pokrije 100 m2 površine. <br>
      U kalkulaciji računati da uvijek potrošimo 2-3 ploče više.<br>
    </div>
    <br>

  </div>

  <h2 id="info_teh_obrada">Obrada strojevi</h2>
  
  <div class="vel_info_page">
    
    <!-- end info text -->
    <h3>Sloter Gandossi & Fossati GFM 180 (s flexo tiskom)</h3>
    
    <div class="vel_info_text col_count_2">
      <img src="/img/info/IMG_20180709_121421.jpg" alt="">
      <img src="/img/info/IMG_20180709_095355.jpg" alt="">
      <img src="/img/info/IMG_20180709_095329.jpg" alt="">
      <img src="/img/info/Sloter.jpg" alt="">


      Godina proizvodnje: 1969. <br>
      Proizvođač: Gandossi & Fossati Model: <b>GFM 180</b>
      Maksimalni format (kartona): <br>
      <b>800 (širina) x 1760 (dužina) mm</b> <br>
      Minimalni format (kartona): <br>
      <b>200 (širina) x 600 (dužina) mm</b> <br>
      Maksimalni format površine/klišea za tisak: <br>
      <b>500 x 500 mm ? (ljepe se na valjak, segmentno)</b> <br>
      <br>
      Oprema: <br>
      <b>Sloter <br></b>
      <b>
        2 flexo tiskovne jedinice (2 boje)<br>
        Izlaganje na paletu <br>
      </b>
      <div class="no_col_break">
        Mogućnosti: <br>
        <ul>
          <li>tisak maksimalno 2 boje</li>
          <li>nema pasera</li>
          <li>biganje</li>
          <li>štancanje rukohvata</li>
          <li>najtanji materijal koji može u "sloter” je B val (ne možemo koristiti materijale tanje</li>
          <li>od 3 mm - mikroval, papir...)</li>
        </ul>
      </div>

    </div>
    <!-- end info text -->

   
    <h3>Sloter video obuka</h3>
         
    <div class="vel_info_text">
     
      <a class="video_link" href="https://youtu.be/Itzc0vWMTSk" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Kako se stavlja kliše na sloter</a><br>
      
    </div> <!-- end info text --> 
       
    <br>
    <br>

    <h3>Aristo Aristomat TL 1625 (ploter)</h3>

    <div class="vel_info_text col_count_2" style="float: none; clear: both;">
      <img src="/img/info/6.jpg" alt="">
      <div class="no_col_break">
        Godina proizvodnje: 2014. <br>
        Dimenzija radne površine: <b>1520x2500 mm</b> <br>
        Brzina: <b>1130 mm/s (maksimalna)</b>, <br>
        Akceleracija: <b>11,3 m/s2 (maksimalna)</b> <br>
        Debljina materijala za obradu: <b>do 46 mm</b> <br><br>
      </div>
      Plošni rezač za obradu (rezanje, presavijanje, V-cut...) pločastih materijala (valovita ljepenka, karton, polipropilen, forex, stadur, kapaline...)
      Koristimo ga za izrada uzoraka, prototyping, izrada manjih naklada
    </div>
    <!-- end info text -->
    <br>
    <br>
    
    <h3>Zund D3 L-3200</h3>

    <div class="vel_info_text col_count_2">
      
       <div class="no_col_break">
        
        <img src="/img/info/001_zund_D3.png" alt="">
        
         <div style="margin: 10px 0 6px; text-align: center; font-size: 14px;">Ispod: Ulagač i Izlagač</div>
        <img src="/img/info/002_zund_D3.png" alt="" style="padding-top: 0;">
      </div>
      
      <div class="no_col_break">
        Zund D3 ima 2 grede što mu povećava kapacitet proizvodnje. Učinkovitost dviju greda ovisi o pozicioniranju određenih elemenata na ploči. <br>
        Zund D3 ima automatski ulagač maksimalne visine ulaganja 540 mm i maksimalnog tereta od 800 kg. <br>
        Izlaganje je isto automatski na način da se prilikom ulaganja nove ploče izlaže stara, izrezana ploča. <br>
        <br>
        Širina reznog stola: <b>1800x3200</b> mm <br>
        Godina proizvodnje: 2022.g. <br> 
        <br>       
        Ostale karakteristike rezanja i biganja su identične kao i za Zund G3XL-3200	

          
      </div>
      
      
    </div><!-- end info text -->
    <br>    
    
    
    <h3>Zund G3 3XL-3200 (ploter)</h3>

    <div class="vel_info_text col_count_2">
      <img src="/img/info/7.jpg" alt="">

      <div class="no_col_break">
        Godina proizvodnje: 2019.<br>
        Dimenzija radne površine: <b>3210x3200 mm</b> <br>
        Brzina: <b>1414 mm/s (maksimalna),<br></b>
        Akceleracija: <b>9,3 m/s2 (maksimalna)<br></b>
        Maksimalna težina materijala za obradu: <b>do 30 kg/m2 <br></b>
      </div>
      Plošni rezač za obradu (rezanje, presavijanje, V-cut...) pločastih materijala (valovita ljepenka, karton, polipropilen, forex, stadur, kapaline...)
      Koristimo ga za izrada uzoraka, prototyping, izrada manjih naklada <br><br>
      
      <b style="color: darkred;">Kad se naručuju ploče na mjeru za rezanje na Zund potrebno je naručiti minimalno 30mm veću ploču od prireza (15 mm sa svake strane).</b>
      
    </div><!-- end info text -->
    <br>
    
    <h3>Upute za rad na Zund ploteru</h3>
    
Skripta je namjenjena primarno djelatnicima na ploteru i svima koji su barem djelomično upoznati sa radom plotera.<br>
Navedenih preporuka ne treba se držati isključivo, puno toga ovisi o samom nacrtu, vrsti materijala itd.<br>
Stavke nemaju kronologiju i nadopunjavat će se i izmjenjivati kako s vremenom iskustvo pokaže da je nešto bolje. 
<br>
<br>      
    
  <div class="vel_info_text">

    <h4 style="margin-top: 0;">Rez i big</h4>
    <div class="no_col_break">
    Pravilo kod plotanja gdje je prisutan rez i big (što je u većini slučajeva) je da se uvijek prvo odrađuje big, zatim rez - međutim, iskustvo je pokazalo da materijal gdje je prebigan i oslabljen, ako nož nije savršeno oštar, upravo na tom mjestu gdje rez prolazi blizu ili kroz big bude poderan, katkad pri velikim brzinama i pukne nož. Zato je preporuka u programu namjestiti da prvo odradi rez, zatim big - osim tankih materijala kao što su papir i PVC, u tom slučaju se izrezani elementi mogu pomaknuti prilikom biganja, stoga je tu nužno držati se pravila.
    <br>
    <br>
    </div> <!-- end no col break -->


    <div class="no_col_break">

      <h4>Optimizacija nacrta</h4>
      Kod učitavanja i pripreme nacrta treba gledati da ga se što bolje optimizira na način da ima što manje isprekidanih linija (npr. spajanje bigova gdje je moguće), uključe radijusi, optimizacija na programu i automatski redosljed (automatic order). Radijuse NE uključivati kod uzoraka i količine koja je trebala ići na štancanje pa naknadno data u rad na ploteru (ovaj dio sa radijusima odobrava poslovođa ili netko više rangiran). U nekim situacijama radijusi nisu preporučljivi: npr. na tvrdim materijalima kao što su polipropilen 5 mm, forex, zatim neki debeli materijali kao npr. kapaline, honeycomb. Uključeni radijusi uz zadanu brzinu i akceleraciju najviše utječu na potrebno vrijeme izrade - zato, kada se nacrt pušta u rad nakon što se podese svi parametri i broj ploča, potrebno je pregledati koliko  program predviđa potrebnog vremena da odradi ciklus i isprobati kombinacije sa brzinom/akceleracijom prije kretanja u rad serije.
      <br>
      <br>
    </div> <!-- end no col break -->

    <div class="no_col_break">
      <h4>Akceleracija kod kompliciranih nacrta</h4>
      Nacrti koji imaju jako puno krivulja tj. za stroj jako puno promjena smjera su opasni ako se podesi maksimalna akceleracija i velika brzina. Pri tome stroj neće javljati nikakvu grešku, ali se može čuti lupanje i osjetiti trešnja stola. Stoga je potrebno smanjiti parametre na optimalnu mjeru ( npr. dovoljno je akceleraciju sa max. vrijednosti 4 smanjiti na 3). Kod veće serije ako su potrebna dva alata, poželjno je skinuti treći modul radi rasterećenja težine na mostu.
      <br>
      <br>  
    </div>

  </div>

  <div class="vel_info_text">
   
   <div class="no_col_break">
   
  <h4 style="margin-top: 0;">Vrste noževa za rez</h4> 
   Rez izvodimo pomoću oscilirajućeg noža (EOT) i poteznog noža (PCT, UCT). PCT alat bi trebao biti primarni kod rezanja, iako je EOT lakši za podesiti i daje kvalitetniji rez - zbog pokretnih dijelova i prisutne elektronike ima kratak vijek trajanja i najskupli je od svih alata. Stoga rezanje poteznim nožem treba koristiti kad je god to moguće. UCT za razliku od PCT-a nije toliko precizan, ali zbog masivnog noža je izvrstan za rezanje plastike i svih ostalih materijala na velikim brzinama uz uvijet da nacrt nije kompleksan (krivulje, puno detalja). PCT radi na principu pritiskanja materijala dok ga reže - zato je važno podesiti jačinu pritiska kada se mijenja materijal (npr. prelazak sa EB vala na E val). Poželjna je kombinacija gdje ravne i dugačke linije radi PCT a EOT radi detalje. Izuzetak su jako kompleksni nacrti i uzorci gdje nije isplativo riskirati trganje ploče, ipak, kod velike serije je poželjno pustiti prvo na PCT i vidjeti može li se izvesti tim alatom.
    <br>
    
  </div>  
    
  <div class="no_col_break">
    
  <h4 style="margin-top: 0;">Vrste noževa za big</h4>
 Big imamo u dvije verzije: CTT3 (veliki kotač) i CTT2 (mali kotač). Svaka verzija ima po još dvije podverzije (više u iduća dva odlomka). Veliki kotač ima prednost što daje rezultat najsličniji štanci uz veliki nedostatak slabog efekta kod pojačavanja pritiska. Stoga npr. kod dvostrukih bigova kao na bokovima stalaka, policama, i nekim kutijama je jako teško dobiti pravi kut pri savijanju. Tu se koristi mali kotač koji je dosta agresivniji (ostavlja ispucan trag), ali je potrebna manja sila za iste ili bolje rezultate kod kasnije dorade. Ukoliko se pretjeruje sa količinom pritiska na bigu, dugoročno se skraćuje vijek trajanja tepihu (podlozi plotera), s time se mora i povećavati dubina noža da bi prorezao materijal i to lančano uvjetuje bržem trošenju tepiha.
  
  <br>
  <br>
    </div>
    
  <div class="no_col_break">  
  <h4 style="margin-top: 0;">CTT3</h4>
  CTT3 big imamo u dvije varijante: C303 (tupi big) i C302 (oštri big). C303 je idealan za sve valovite kartone (pogotovo B val i BC val) gdje nisu prisutni dvostruki bigovi. U tom slučaju se koristi kombinacija sa CTT2 bigom koji odrađije samo duple bigove, ostale se radi sa C303 bigom. C302 oštri big je idealan za papir i tanke materijale gdje nije potrebni ići u veliku dubinu, to je ujedno i njegova mana jer je konstrukcijski izveden da može ići 0,5 mm u materijal.
  <br>
  <br>
  </div>
    
    
    
    </div>
  
  <div class="vel_info_text">
  
  <div class="no_col_break">
  
  
  <h4 style="margin-top: 0;">CTT2</h4>
   CTT2 big imamo isto u dvije varijante: C212 (tupi big) i *C210 (oštri big). C212 je najćešće korišten (gotovo identičan kao na Aristo ploteru) i koristi se dok god nije važno da li pucaju bigovi. Kada je u nalogu navedeno "paziti da ne pucaju bigovi" - potrebno je koristiti veliki C303 big ili kombinaciju kao gore navedeno. Poželjno je kutije kao npr. f427 raditi u kobinaciji da po valu ide veliki C303, a kontra vala mali C212 big (kod mikrovala tako budu izvrsni rezultati). C210 je mali oštri big koji za razliku od velikog oštrog može ići u veću dubinu (5 mm), stoga je idealan za biganje polipropilena (isključivo kontra vala) i PVC-a: npr. paletni omotači, PVC kadice, PVC đepići od debljeg materijala, eplak, može se koristiti i za biganje papira jače gramature gdje je C302 preslab. <u>*C210 big je još u fazi pregovora za nabavu</u>
   <br>
   <br>
   
  </div> <!-- end no col break --> 
   
   <div class="no_col_break">
   <h4 style="margin-top: 0;">V Cut</h4>
V cut (rezanje pod kutom) gotovo isključivo koristimo kut 45° iako ima i podešavanje više kutova. Koristimo ga za zarezivanje tvrdih valovitih ili saćastih materijala gdje big nema efekta. Cilj je zarezati pod kutom i ukloniti višak materijala da bi se isti mogao presavinuti. Tu je najčešće korišten polipropilen 5 mm i honeycomb. Redoslijed radnji u programu mora biti da prvo radi ric (ako ga ima), zatim zarezivanje pod kutom i tek onda rezanje. Kod polipropilena je najkritičnije zarezivanje po valu jer vrh noža je odmaknut 1 mm od tepiha i svaka čestica na tepihu distancira ploču i dolazi do neželjenog lančanog prorezivanja cijelom dužinom. Stoga je vrlo važno očistiti stol prije svakog puštanja u rad. Do istog neželjenog efekta dolazi kada je nacrt smješten preblizu ruba ploče i ako ploča nije savršeno ravna - vrh noža zahvati dno ploče i prorezuje ga cijelom dužinom. Visina se može podesiti da bude veća ali onda se ne može dobiti ispravan kut za linije kontra vala. Kod velike serije sa polipropilenom zna se desiti da nož ne bude više u istoj poziciji i opet prorezuje materijal, tada je potrebno skinuti nož i ponovno ga montirati pomoću šablone i propisno stegnuti vijke.
  <br>
  <br>
   
  </div>
    
  <div class="no_col_break">
  
    <h4 style="margin-top: 0;">Registracija ploča</h4>   
    Registracija ploča je u početnim postavkama podešena na naprijed-desno (front-righr) - najčešće korištena opcija, druga opcija je naprijed-lijevo (front-left) - nužno unijeti dimenziju ploče. Kod ploča sa tiskom treba poštivati uložni kut i u većini slučajeva se koristi prva opcija. Kada se pušta više ploča odjednom (print na B0 ili B1 formatu) potrebno je okrenuti ploče da ih stane više u tandemu (okomita orijentacija umjesto vodoravne), pri tome posebno obratiti pažnju na uložni kut i sukladno tome podesiti u programu - u tom slučaju često se koristi druga opcija i potrebno je unijeti dimenziju ploče. Kod obostranog printa grafičari šalju dva nacrta. Prvi je za vizual gledan sa lica, drugi je gledan sa naličja (zrcaljen). Uglavnom se priprema radi kao i za jednosrani print (uzima se prvi nacrt), izuzetak je simetričan print gdje se može uzeti drugi već zrcaljen nacrt i podesi se da samo učitava markere i odmah kreće u rad (bez snimanja ruba). Simetričan print nije svaki obostrani print (npr. ako headeri imaju samo ton na naličju to nije simetričan print). Pripaziti kod materijala koji nemaju lice-naličje (forex, polipropilen, eplak, juvidur...) ako se isprintaju sa obostranim i simetričnim printom. To je najčešće kombinacija skupih materijala sa velikim utroškom boje i potrebna je maksimalna koncentracija kod pripreme nacrta.
    <br>
    <br>   

  </div> 
   
   
  </div>

  <div class="vel_info_text">
  
  
  <div class="no_col_break">
  
  <h4 style="margin-top: 0;">Način rada</h4> 
   Poželjno je steći naviku da se u radu što manje saginje prilikom uzimanja ploča i slaganja izrezanih elemenata. Pri tome se pomagati stolom na kotačima i električnim paletarom koliko je god to moguće. Idealna bi bila varijanta sa dva visokopodizna ručna viličara gdje bi se mogla podešavati visina na paleti isprintanih ploča (podizanje prilikom trošenja ploča) i s druge strane slaganje isplotanih elemenata (spuštanje prilikom punjenja palete).
  <br>
  <br>       
  </div> <!-- end no col break -->
         
                               
  <div class="no_col_break">
            
  <h4 style="margin-top: 0;">Optimizacija rada</h4> 
  Na ploteru, dok radi, uvijek ima posla. Najčešća je situacija: čim kompleksniji nacrt - tim treba više vremena da odradi plotanje, ali i duže vrijeme da se skloni sa stola i složi na paletu. Prazni hod (čekanje da bude gotova iduća proča) - ako ga ima - se koristi za čišćenje detalja kod isplotanih elemenata. Ako je situacija da dugo plota, a brzo se čisti: vrijeme je poželjno iskoristiti na provjeri šta je iduće na redu, učitavanju i pripremi nacrta, priprema ploča... S druge strane, bitno je da stroj ne čeka operatera, pogotovo kada se radi u tandemu, zato treba prioritet dati plotanju. Ako u tandemu nije moguće raditi, nikako ne čistiti ploču u zoni gdje se reže, već sa novom pločom izgurati izrezanu na drugi kraj plotera (po potrebi privući stol na toj strani) i čistiti u periodu dok se ploča reže. 
  
  </div> <!-- end no col break --> 
   
   
  </div> 

<br>
<br>  
  
   
    <h3>Zund video obuka</h3>
         
    <div class="vel_info_text">
     
      <a class="video_link" href="https://youtu.be/qRLW17TwT8E" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Kako ispravno raditi na Zund-u</a><br>
     
      <a class="video_link" href="https://youtu.be/-rywYb2zeA8" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Skidanje modula i podmazivanje grede</a><br>
      <a class="video_link" href="https://youtu.be/2vPdgIXtJms" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Podmazivanje modula</a><br>
      <a class="video_link" href="https://youtu.be/kE1nBogzBzA" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Zaštita preko svjetlosenzora</a><br>
      <a class="video_link" href="https://youtu.be/GCwVsxt_efs" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Razlika između Z10 i Z11 noževa za UCT alat</a><br>
      <a class="video_link" href="https://youtu.be/04m2aDz76Os" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Zamjena uloška čahure za UCT alat</a><br>
      <a class="video_link" href="https://youtu.be/Xklh-U9dFbE" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Primjer novog otvorenog noža i njegova zaštita</a><br>
      <a class="video_link" href="https://youtu.be/rICH-t3sQ3k" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Vađenje i stavljenje noža na UCT alatu</a><br>
      <a class="video_link" href="https://youtu.be/IArXvhYqixs" target="_blank"><i class="far fa-play-circle"></i>&nbsp;UCT alat - upotreba</a><br>
      <a class="video_link" href="https://youtu.be/WXS8EWnhfk4" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Alat za crtanje - kemijska</a><br>
      <a class="video_link" href="https://youtu.be/G9f-K7IpEU4" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Alat kiss cut - rezanje folije</a><br>
      <a class="video_link" href="https://youtu.be/SyQWdWZLqQk" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Alat kiss cut - podešavanje i izmjena noža</a><br>
      <a class="video_link" href="https://youtu.be/t25cOJxZeek" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Mehanički graničnik za kiss cut</a><br>
      <a class="video_link" href="https://youtu.be/qbGrQ9MbPIM" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Mehanički graničnik za kiss cut nastavak</a><br>
      <a class="video_link" href="https://youtu.be/5G-dAzdlL1w" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Noževi za kiss cut i opcije kod rezanja</a><br>
      <a class="video_link" href="https://youtu.be/cn3pdD2Q6Q0" target="_blank"><i class="far fa-play-circle"></i>&nbsp;V cut uvod</a><br>
      
      <a class="video_link" href="https://youtu.be/GyjKNCQ5vAQ" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Noževi za V cut</a><br>
      
      <a class="video_link" href="https://youtu.be/QrJLnJhN4zk" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Montaža noža na V cut</a><br>
<a class="video_link" href="https://youtu.be/MxcnBCsGMJ4" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Alat za perforaciju kartona - prvi dio</a><br>
<a class="video_link" href="https://youtu.be/qUAwERJiekI" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Alat za perforaciju kartona - drugi dio</a><br>

<a class="video_link" href="https://youtu.be/J_LlaaqxmjE" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Alat za perforaciju kartona - treći dio</a><br>
    
     <a class="video_link" href="https://youtu.be/3JgfBowyotM" target="_blank"><i class="far fa-play-circle"></i>&nbsp;CTT3 alat - veliki big</a><br>
     
     <a class="video_link" href="https://youtu.be/OhcgWILNBg8" target="_blank"><i class="far fa-play-circle"></i>&nbsp;CTT3 alat - veliki big nastavak</a><br>
     
     <a class="video_link" href="https://youtu.be/dc9g0TC3a8A" target="_blank"><i class="far fa-play-circle"></i>&nbsp;EOT alat oscilirajući - nož uvod i održavanje</a><br>
     
     <a class="video_link" href="https://youtu.be/yv1185_F9OY" target="_blank"><i class="far fa-play-circle"></i>&nbsp;EOT zaštitna karbonska osovina</a><br>
     <a class="video_link" href="https://youtu.be/Sg6qHMzy-Ao" target="_blank"><i class="far fa-play-circle"></i>&nbsp;EOT predviđen broj sati i montaža noža</a><br>
     <a class="video_link" href="https://youtu.be/FVhA9aybDkQ" target="_blank"><i class="far fa-play-circle"></i>&nbsp;EOT montiran nož i alat spreman za ugradnju u modul</a><br>
     <a class="video_link" href="https://youtu.be/rqIBJ7cifmI" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Ugradnja alata u modul i inicijalizacija</a><br>
     <a class="video_link" href="https://youtu.be/dbxUUCVAs2o" target="_blank"><i class="far fa-play-circle"></i>&nbsp;Opcije na konzoli</a><br>
      
 
    </div> <!-- end info text --> 
    
    <br>
    <br>
    
    <h3>Stroj za lasersko rezanje i graviranje FL1610 (FL-1390, 130 W)</h3>
    
    <div class="vel_info_text col_count_2">
      <img src="/img/info/Laser.jpg" alt="">
      <div class="no_col_break">
        Datum proizvodnje: 14. 12. 2018.
        Dimenzija radne površine: <b>1600x1000 mm <br></b>
        Maksimalna brzina lasera: <b>0-40000 mm/min <br></b>
        Minimalna veličina graviranja:<b> 1,5 x 1,5 mm <br></b>
        Rezolucija:<b>
          <= 1000 dpi <br>
        </b>
        Debljina za razrezivanje: <b>0,2 - 22 mm (ovisno o materijalu) <br></b>
      </div>
      Stroj za lasersko rezanje i graviranje koristimo za izradu uzoraka, prototyping, proizvodnju
      manjih naklada.
      Može se gravirati na drvo, staklo, gumu, keramiku, papir, karton, kožu, kamen. Rezati se može pleksiglas do 20 mm, drvo do 30mm, papir, prirodnu kožu, karton, gumu...
      Možete gravirat i na čaše, boce i slično.
    </div>
    <br>

    <h3>F1 CNC Router Servo</h3>

    <div class="vel_info_text col_count_2">

      <img src="/img/info/CNC.jpg" alt="">

      <div class="no_col_break">
        Datum proizvodnje: 27. 8. 2019.<br>
        Dimenzija radne površine:<b> 1500x3000x200 mm <br></b>
        Maksimalna radna brzina: <b>30 m/min <br></b>
        Maksimalna brzina slobodnog hoda: <b>50 m/min <br></b>
        Brzina vrtnje glodala: <b>6000-24000 okretaja/min <br></b>
      </div>

    </div>
    <br>

    <h3>Grafički nož Wohlenberg</h3>

    <div class="vel_info_text col_count_2">

      <img src="/img/info/Graficki_noz.jpg" alt="">

      <div class="no_col_break">
        Godina proizvodnje: 1993. <br>
        Maksimalna dužina: <br>
        <b>1150 mm <br></b>
        Minimalna širina: <br>
        <b>100 mm <br></b>
        Maksimalna.širina: <br>
        <b>1145 mm <br></b>
      </div>
      <!-- end info ul -->
    </div>
    
    <h3 id="graf_noz_video_info">Video upute za Grafički nož Wohlenberg</h3>

    <div class="vel_info_text">
       <a class="video_link" href="https://youtu.be/4fwqxGXzE7M" target="_blank" style="text-align: center; width: 100%;">
        <i class="far fa-play-circle"></i>&nbsp;Zamjena noža 1
      </a>
      <br>
      <a class="video_link" href="https://youtu.be/dSXQzxFhtIM" target="_blank" style="text-align: center; width: 100%;">
        <i class="far fa-play-circle"></i>&nbsp;Zamjena noža 2
      </a>
      <br>
      <a class="video_link" href="https://youtu.be/eHYuoFXvFns" target="_blank" style="text-align: center; width: 100%;">
        <i class="far fa-play-circle"></i>&nbsp;Mijenjanje letve na grafičkom nožu
      </a>
      <br>      
      <a class="video_link" href="https://youtu.be/eeIQS1LHwTA" target="_blank" style="text-align: center; width: 100%;">
        <i class="far fa-play-circle"></i>&nbsp;Mijenjanje letve na grafičkom nožu + kalibracija visine noža i zadnjeg graničnika
      </a>
      <br> 
      <a class="video_link" href="https://youtu.be/bxWpskExrgo" target="_blank" style="text-align: center; width: 100%;">
        <i class="far fa-play-circle"></i>&nbsp;Kalibracija zadnjeg graničnika 2
      </a>
      <br> 
      <a class="video_link" href="https://youtu.be/ClrqO-RhD4Q" target="_blank" style="text-align: center; width: 100%;">
        <i class="far fa-play-circle"></i>&nbsp;Grafički nož 6
      </a>
      <br> 
      <a class="video_link" href="https://youtu.be/CBzpyHAa51g" target="_blank" style="text-align: center; width: 100%;">
        <i class="far fa-play-circle"></i>&nbsp;Podmazivanje noža
      </a>
      <br> 

    </div>    
    <br>

    <h3>Potezni nož KeenCut SteelTraK 210</h3>
    <div class="vel_info_text col_count_2">
      <div class="no_col_break">
        <img src="/img/info/9.jpg" alt="" style="max-width: 350px;" >
      </div>
      <!-- end info ul -->
      
      
      Godina proizvodnje: 2019. <br>
      Maksimalna dužina rezanja (po visini): <br>
      <b>2100 mm <br></b>
      <br>
      <div class="no_col_break">
       
       Detaljne upute za rad možete pogledati ovdje:
       <br>
       <a href="/img/info/SteelTraK-165-210-Manual-English-R3-1.pdf" target="_blank" style="font-size: 20px;">
       SteelTraK Manual English &nbsp;<i class="fal fa-external-link"></i>
       </a>
       <br>
       <br>
       
       
       <br>
       
        <b>Semi-rigid materials <br></b>
        <ul>
          <li>PVC foam board up to 13 mm</li>
          <li>Corrugated/fluted plastic (Correx) up to 13 mm Foam centred board up to 13 mm</li>
          <li>Most other semi-rigid plastic materials</li>
          <li>Magnetic sheet up to 4 mm</li>
        </ul>
      </div>
      <!-- end info ul -->
      <div class="no_col_break">
        <b>Rigid materials <br> </b>
        <ul>
          <li>Aluminium composite (Dibond) up to 4 mm</li>
          <li>MDF up to 3 mm</li>
          <li>Cast acrylic or Plexiglas scoring up to 3 mm</li>
          <li>Glass scoring up to 3 mm</li>
        </ul>
      </div>
      <!-- end info ul -->
    </div>

      
    <h3>Potezni nož KeenCut Evolution E2 EV2310</h3>
    <div class="vel_info_text col_count_2">
     
    <div class="no_col_break">
      <img src="/img/info/8.jpg" alt="" style="max-width: 350px;" >
    </div>
      
      <div class="no_col_break">
      
      Godina proizvodnje: 2019.<br>
      Maksimalna dužina rezanja: <b>3100 mm</b> 
      <br>
      <br>
      
      Detaljne upute za rad možete pogledati ovdje:
       <br>
       <a href="/img/info/evolution_e2.pdf" target="_blank" style="font-size: 20px;">
       Evolution 2 Manual English&nbsp;<i class="fal fa-external-link"></i>
       </a>
       
       <br>
       <br>
        <b>Semi-rigid materials <br></b>
        <ul>
          <li>Cardboard, PVC foam board, foam-centred board, corrugated plastic up to</li>
          <li>13 mm</li>
          <li>Mountboard and conservation board up to 3.5 mm Flexible polycarbonate up to 3 mm</li>
        </ul>
        
      
       <b>Flexible materials <br></b>
        <ul>
          <li>Tough materials, banners, pop-ups, magnetic sheets</li>
          <li>Film and self-adhesive vinyls</li>
          <li>Delicate materials: tissue paper, fabrics, wallpaper & textiles</li>
        </ul>  
        
       
      <b>Rigid material <br></b>
        <ul>
          <li>Cast acrylic or Plexiglas up to 3 mm</li>
        </ul>  
          
      </div>

    </div>
    

    <br>  
  
  
    <h3>Krajšer krugorezač stari</h3>

    <div class="vel_info_text col_count_2">

      <div class="no_col_break">
        <img src="/img/info/Dodatna_skripta_-_slike1.jpg" alt="" style="padding: 20px;">
        <img src="/img/info/Krajser_-_stari.jpg" alt="" style="padding: 20px;">
      </div> <!-- end no col break -->

      <div class="no_col_break">
        Izvorni naziv: Combined Slitting & Line pressing machine <br>
        Godina proizvodnje: 2010. <br>
        Maksimalna dužina:<b> 2650 mm. <br></b>
        <br>
        Ukupno ima 2 noža i 4 biga, kako bi stroj bio efikasniji treba postaviti jedan nož i dva biga. Minimalni napust 10 mm za sve materijale.<br>
      </div>

    </div> <!-- end info text -->

    <br>


    <h3>Krajšer krugorezač novi</h3>

    <div class="vel_info_text col_count_2">
     
      <div class="no_col_break">
        <img src="/img/info/Krajser_-_novi.jpg" alt="">
      </div> <!-- end no col break -->
      
      
      <div class="no_col_break">
        Izvorni naziv: Thin Slitter Creaser Machine (Dongguang County Huanyu Carton Machinery)<br>
        Godina proizvodnje: 2019.<br>
        Maksimalna dužina: <b>3100</b> mm <br>
        Broj noževa: 5 noževa<br>
        Min. razmak između noževa: 120mm<br>
        (iznimka je min. razmak između 4. i 5. noža koji je 235mm)<br>
        <br>
        Minimalni razmak od graničnika do prvog noža: 10mm<br>
      </div>
      
      
    </div>
   
    <br>
    
    <h3>Usjecalica</h3>
    
    <div class="vel_info_text col_count_2">
    
      <div class="no_col_break">
        <img src="/img/info/Dodatna_skripta_-_slike2.jpg" alt="">
      </div> <!-- end no col break -->
      
      
      <div class="no_col_break">
        Godina proizvodnje: 1997.<br>
        Maksimalna dužina: 1880 mm (zadnji nož) - pripaziti posebno kod veliki "amerikanki” (F201). <br>
        Dimenzija kutija:<br>
        min.širina/dužina kutije je 100 mm,<br>
        min.visina kutije 50 mm, <br>
        u suprotnom dolazi do pucanja kutije<br>
        Ako je širina ili dužina manja od 100 mm, prvo se usjeca 1. i 3. big, a onda 2. i 4. big.<br>
        Maksimalna dužina usjecanja cca. 360 mm.
      </div> <!-- end no col break -->  
      
    </div>
    <!-- end info text -->
    <br>
    
    <h3>Šlic mašina  - Sloter novi</h3>

    <div class="vel_info_text col_count_2">
      <div class="no_col_break">
        <img src="/img/info/Sloter_-_novi.jpg" alt="">    
      </div>
      <div class="no_col_break">
        Izvorni naziv: Sloter Semi-Auto Rotary Slotting Machine <br>
        Proizvođač: Dongguang County Huanyu Carton Machinery - China <br>        
        Godina proizvodnje: <b>2019</b>. <br>
        Max dimenzija ploče koja moze uci u stroj: <b>1450 X 2520</b> mm <br>
        Max. visina kutije koja moze izaci iz stroja: <b>750</b> mm <br>
        Minimalni razmak izmedu nozeva: <b>140</b> mm
      </div>
    </div>
    
    <br>  
  
  

  
</div> <!-- end info page  kraj obrade -->
    
  
  

  <h2 id="info_teh_dorada">Dorada strojevi</h2>  


  <div class="vel_info_page">
    
    <h3>Automatska kaširka DX1415 i automatski Flip Flop Stacker</h3>

    <div class="vel_info_text col_count_2">
     
      <img src="/img/info/001_auto_kasirka_DX1415.png" alt="" style="margin-bottom: 0; margin-top: 0;">
      <br>
      <img src="/img/info/003_auto_kasirka_DX1415.png" alt="" style="margin-bottom: 0; margin-top: 0;">

      
      <div class="no_col_break">
        <img src="/img/info/002_auto_kasirka_DX1415.png" alt="" style="margin-bottom: 0; margin-top: 0;">
        <br>
        Karakteristike stroja: <br>
        <div class="table_wrap">

          <table class="info_table" style="width: 100%;">
            <tbody>

              <tr>
                <td>Maksimalna veličina ploče</td>
                <td>1400x1450 mm</td>

              </tr>

              <tr>
                <td>Minimalna veličina ploče</td>
                <td>450x450 mm</td>

              </tr>

              <tr>
                <td>Gramaža gornjeg kartona</td>
                <td>120-800 g/m2</td>

              </tr>



              <tr>
                <td>Gramaža donjeg kartona/valovite ljepenke</td>
                <td>0.5 - 10mm</td>

              </tr>

              <tr>
                <td>Maksimalna brzina stroja</td>
                <td>10.000 ploča/h</td>
              </tr>

              <tr>
                <td>Dimenzija stroja</td>
                <td>24500 mm x 2314 mm x 2845 mm</td>

              </tr>


              <tr>
                <td>Preciznost kaširanja</td>
                <td>± 1.5mm</td>

              </tr>


              <tr>
                <td>Godina proizvodnje</td>
                <td>9./2022.</td>

              </tr>


            </tbody>
          </table>
        </div> 
        <br>


        Kaširka ima mogućnost kaširanja na dvoslojnu, troslojnu, četveroslojnu i peteroslojnu valovitu ljepenku.<br>
        <br>
        Preferira se kaširanje na dvoslojac ili četverslojac radi lakšeg ulaganja ploča valovite ljepenke te samim time boljeg rada same kaširke. <br>
        U slučaju kaširanja kartona na dvoslojnu ili četveroslojnu valovitu ljepenku potrebna je gramaža kartona od 210-250 g/m2. Moguće je kaširati i karton na karton.<br>
        Kod jednostranog kaširanja potreban je karton 10 mm većih dimenzija od valovite ljepenke u obje dimenzije, npr. ako je dvoslojna valovita ljepenka 690x990mm, karton mora biti 700x1000mm.<br>
        U slučaju obostranog kaširanja:<br>
        <br>
        <ul>
        <li>Npr. Dvoslojna valovita ljepenka: 690x990 mm </li>
        <li>Prva se kašira na val, to je unutarnja strana kutije/proizvoda; dimenzija kartona je 695x995mm</li>
          <li>U drugom prolazu se kašira na ravni sloj, to je vanjska strana kutije/proizvoda; dimenzija kartona: 700x1000 mm</li>
            <li>Kod obostranog kaširanja tražiti u tiskari da razreže karton na dim: 695x995mm  za unutarnju stranu kutije i to SVAKAKO prije tiska.
              <ul>
                <li>Ako će se zbog nekog razloga arak obrezivati nakon tiska, arak ne smije biti razrezan po šuberu i marki</li>
              </ul>
            </li>
              
        </ul>

        <br>



        Detaljne upute za rad možete pogledati ovdje:
       <br>
       <br>
       <a href="/img/info/Manual_Laminator_HRV_Final.pdf" target="_blank" style="font-size: 20px; display: block; float: left; margin-right: 30px;" >
       Laminator - Upute za rad&nbsp;<i class="fal fa-external-link"></i>
       </a>

       <a href="/img/info/Manual_Flip_Flop_HRV_Final.pdf" target="_blank" style="font-size: 20px; display: block; float: left; margin-right: 30px; " >
       Flip Flop - Upute za rad&nbsp;<i class="fal fa-external-link"></i>
       </a>

       <br>
       <br>

       <a class="video_link" href="https://youtu.be/byyqLaEzJJE" target="_blank" style="font-size: 20px; display: block; float: left; margin-right: 30px; " >
        <i class="far fa-play-circle"></i>&nbsp;Video automatske kaširke 1
       </a>
       <a class="video_link" href="https://youtu.be/1iFjbggum9s" target="_blank" style="font-size: 20px; display: block; float: left; margin-right: 30px; " >
        <i class="far fa-play-circle"></i>&nbsp;Video automatske kaširke 2
       </a>
        
        
      </div>
    </div>
    
    
    <br>    
    
    
    
    <h3>Kaširka Lamina</h3>
    <div class="vel_info_text col_count_2">
     
      <img src="/img/info/Kasirka_Lamina.jpg" alt="" style="margin-bottom: 0; margin-top: 0;">
      
      <div class="no_col_break">
        Godina proizvodnje: 2005.  <br>
        <br>
        <b style="color: darkred;">Tok papira uvijek mora biti okomit na tok valovite ljepenke!</b>
        <br>
        <br>
        Detaljne upute za rad možete pogledati ovdje:
       <br>
       <br>
       <a href="/img/info/lamina_upute_za_rad.pdf" target="_blank" style="font-size: 30px;">
       Lamina - Upute za rad&nbsp;<i class="fal fa-external-link"></i>
       </a>
        
        
      </div>
    </div>
    
    
    <br>
    
    <h3>Ručna kaširka</h3>
    
    <div class="vel_info_text col_count_2">
      <img src="/img/info/Kasirka_rucna.jpg" alt="" style="margin-bottom: 0; margin-top: 0;">
      <div class="no_col_break">
        Godina proizvodnje: 2015.<br>
        <br>
        <b style="color: darkred;">Tok papira uvijek mora biti okomit na tok valovite ljepenke!</b>
        <br>
        
        Koristimo ako se radi o segmentnom kaširanju ili kada je gramatura materijala koji se kaširaju premala.
        <br>
        <b>
        Kada kaširamo obostrano uvijek kasiramo unutarnju stranu prvo jer prilikom drugog prolaza dolazi do mazanja ljepila na valjcima i trakama pa ako treba mazati bolje da maže po unutarnjoj strani.
        </b>
      </div>  
    </div>
    
    <br>
    
    
    <a href="/img/info/PERFORACIJE.pdf" target="_blank" style="font-size: 30px;">
      Upute za PERFORACIJE &nbsp;<i class="fal fa-external-link"></i>
    </a>
    
    <br>
    <br>
    <br>
    <br>

    
    <h3>Automatska štanca ALPES ST 1050EC</h3>
    <div class="vel_info_text col_count_2">
     
      <img src="/img/info/001_auto_stanca_1050CE.png" alt="" style="margin-bottom: 0; margin-top: 0;">
      
      
      <div class="no_col_break">

        <img src="/img/info/002_auto_stanca_1050CE.png" alt="" style="margin-bottom: 0; margin-top: 0;">
        <br>
        Svi alati koji su rađeni za zaklopne štance mogu se upotrijebiti i za automatsku štancu (ako se zadovoljavaju uvjeti minimalnih i maksimalnih prireza alata automatske štance).<br>
        Automatska štanca ima i sekciju u kojoj se stavljaju alati za izbijanje što olakšava proces čišćenja. <br>
        U pravilu svi elementi se mogu očistiti osim prednje strane ploče zbog same tehnologije izlaganja ploče u stroju.<br>
        Ako se rade alati za izbijanje potrebno je i raditi novi alat za rezanje i biganje (tzv. špera) <br>

        <br>
        <div class="table_wrap">
          
          <table class="info_table" style="width: 100%;">
            <tbody>

              <tr>
                <td>Maksimalna veličina ploče</td>
                <td>1050x750 mm</td>

              </tr>

              <tr>
                <td>Minimalna veličina ploče</td>
                <td>400x360 mm</td>

              </tr>

              <tr>
                <td>Maksimalna dimenzija prireza</td>
                <td>1040x720 mm</td>

              </tr>



              <tr>
                <td>Maksimalna brzina stroja</td>
                <td>7500 udaraca/h</td>

              </tr>

              <tr>
                <td>Snaga stroja</td>
                <td>16,9 kW</td>
              </tr>

              <tr>
                <td>Maksimalni radni pritisak</td>
                <td>300 t</td>

              </tr>


              <tr>
                <td>Raspon štancanja</td>
                <td>80-2000 g/m2 za karton, < 4mm valovita ljepenka</td>

              </tr>


              <tr>
                <td>Preciznost štancanja</td>
                <td>± 0.1 mm</td>

              </tr>

              <tr>
                <td>Godina proizvodnje</td>
                <td>10./2022.</td>

              </tr>


            </tbody>
          </table>
        </div> 
        <br>
       
        Detaljne upute za rad možete pogledati ovdje:
       <br>
       <br>
       <a href="/img/info/Manual_mala_stanca_105_HRV_Final.pdf" target="_blank" style="font-size: 30px;">
        ALPES ST 1050EC - Upute za rad&nbsp;<i class="fal fa-external-link"></i>
       </a>
        
        
      </div>
    </div>
    
    
    <br>            
    
    
    
    <h3>Automatska štanca ALPES ST 1650EC</h3>
    <div class="vel_info_text col_count_2">
      <img src="/img/info/001_automatska_stanca.png" alt="" style="margin-bottom: 0; margin-top: 0;"> 
      <img src="/img/info/002_automatska_stanca.png" alt="" style="margin-bottom: 0; margin-top: 0;"> 
      <img src="/img/info/003_automatska_stanca.png" alt="" style="margin-bottom: 0; margin-top: 0;"> 
      <div class="no_col_break">
        Svi alati koji su rađeni za zaklopne štance mogu se upotrijebiti i za automatsku štancu (ako se zadovoljavaju uvjeti minimalnih i maksimalnih prireza alata automatske štance). <br>
        Automatska štanca ima i sekciju u kojoj se stavljaju alati za izbijanje što olakšava proces čišćenja. U pravilu svi elementi se mogu očistiti osim prednje strane ploče zbog same tehnologije izlaganja ploče u stroju. <br>
        Ako se rade alati za izbijanje potrebno je i raditi novi alat za rezanje i biganje (tzv. špera) <br>
        <br>
        <div class="table_wrap">

          <table class="info_table" style="width: 100%;">
            <tbody>

              <tr>
                <td>Maksimalna veličina ploče</td>
                <td>1650x1200 mm</td>

              </tr>

              <tr>
                <td>Minimalna veličina ploče</td>
                <td>650x550 mm</td>

              </tr>

              <tr>
                <td>Maksimalna dimenzija prireza</td>
                <td>1620x1190 mm </td>

              </tr>

              <tr>
                <td>Maksimalna brzina stroja</td>
                <td>4000 udaraca/h</td>
              </tr>

              <tr>
                <td>Snaga stroja</td>
                <td>19,7 kW</td>

              </tr>


              <tr>
                <td>Maksimalni radni pritisak</td>
                <td>30 t</td>

              </tr>


              <tr>
                <td>Dimenzija stroja</td>
                <td>9000x4400x2300 mm</td>

              </tr>

              <tr>
                <td>Raspon štancanja</td>
                <td>Valovita ljepenka 1-7 mm</td>

              </tr>
              <tr>
                <td>Preciznost štancanja</td>
                <td>± 0.1 mm</td>

              </tr>
              <tr>
                <td>Godina proizvodnje</td>
                <td>2022.</td>

              </tr>


            </tbody>
          </table>
        </div>    

     
     
      </div>  
    </div>
    <br>
    
    <h3>Procedura za CURIHTUNG</h3>
    <div class="vel_info_text">


U pravilu svi alati koji su rađeni za zaklopne štance mogu se upotrijebiti i za automatsku štancu (ako se zadovoljavaju uvjeti minimalnih i maksimalnih prireza alata automatske štance). <br>
Referent prodaje je dužan navesti na nalogu da se naklada odrađuje na automatskoj štanci i da je potrebno napraviti nacrt na papiru (u daljnjem tekstu tzv. curihtung). Curithtung se radi na blueback papiru te mora biti zrcaljen s obzirom na originalan nacrt alata. Nacrt se zrcali s obzirom na dužu stranu ulaganja alata (1650mm max dužina papira). Ako je alat simetričan s obzirom na os koja ide po širini stroja, curihtung nije potrebno raditi. Idući put kad se bude puštao isti alat na automatskoj štanci nije potrebno raditi novi curihtung.<br>
Sam curihtung služi za podljepljivanje alata na automatskoj štanci. Podljepljivanje se radi posebnim cito trakama različitih debljina (na zaklopnoj stanci se alat podljepljuje pik trakom).<br>
     <br>
     Debljine i boje cito trake: <br>
    <ul>
       <li>Plava – 0.03 mm debljine</li>
       <li>Crvena – 0.05 mm debljine</li>
       <li>Žuta – 0.08 mm debljine</li>
    </ul>
U pravilu jednom kada se na curihtung naštima određeni pritisak stroja te poljepe cito trake, tokom idućeg stavljanja alata u stroj nisu potrebna dodatna podešavanja stroja. <br>
Ako se alat radi specifično za automatsku štancu, dobavljač alata sam radi curihtung te nam ga dostavlja ( LPF dostavlja curihtung na paus papiru)
Prije slanja naloga u radionu, referent prodaje je dužan proslijediti nalog CADu sa zahtjevom da napravi curihtung. Referent u CADu zrcali nacrt te šalje radioni kako je <b>nacrt i nalog </b>spreman za crtanje. Referent CADa nacrt smješta u folder ….. (po dogovoru)
Poslovođa prilikom printanja naloga jedan primjerak naloga odnosi radniku na ploteru kako bi izradio curihtung. Kada se curihtung izradi, radnik na ploteru ga dostavlja direktno poslovođi. <br>
Nakon što je curihtung napravljen referent u CADu je dužan ući u svaku šifru kojem taj alat pripada te navesti da imamo curihtung za navedeni alat. <br>


     
 
    </div>
    <br>    
    
    
    <h3>Cilindar štanca Heidelberg HB cylinder 54x77</h3>
    <div class="vel_info_text col_count_2">
      <img src="/img/info/Cilindar_Heidelberg.jpg" alt="" style="margin-bottom: 0; margin-top: 0;"> 
      <div class="no_col_break">
        Godina proizvodnje: 1965.<br>
        <br>
        Maksimalna dimenzija arka:<b> 540x770 mm <br></b>
        Od noža do noža (na alatu): <b>490x700 mm <br></b>
        Daska (za alat): <b>maksimalno 510 720 mm <br></b>
        Minimalna dimenzija arka: <b>115x160 mm <br></b>
        Maksimalna brzina:<b> 3500 araka/sat <br></b>
        Grajfer: <b>8-10 mm <br></b>
      </div>  
    </div>
    <br>    


    <h3>Zaklopna velika štanca</h3>
    <div class="vel_info_text col_count_2">
      <img src="/img/info/Velika_stanca.jpg" alt="" style="margin-bottom: 0; margin-top: 0;">
      <div class="no_col_break">
        Godina proizvodnje: 01/2017.<br>
        Dimenzija daske alata:<b> 1810x1280 mm<br></b>
      </div>  
    </div>    
    <br>
   
    <h3>Zaklopna štanca B0 (zelena)</h3>
    <div class="vel_info_text col_count_2">
      <img src="/img/info/Zelena_stanca.jpg" alt="" style="margin-bottom: 0; margin-top: 0;">
      <div class="no_col_break">
        Godina proizvodnje: 12/2007.<br>
        Dimenzija od noža do noža:<b> 1380x980 mm<br></b>
      </div>  
    </div>    
    <br>

    <h3>Zaklopna štanca B0 (siva)</h3>
    <div class="vel_info_text col_count_2">
      <img src="/img/info/Siva_stanca.jpg" alt="" style="margin-bottom: 0; margin-top: 0;">
      <div class="no_col_break">
        Godina proizvodnje: 6/2018.<br>
        Dimenzija od noža do noža:<b> 1380x980 mm <br></b>
      </div>  
    </div>    
    <br>
    
    <h3>Zaklopna štanca B1</h3>
    <div class="vel_info_text col_count_2">
      <img src="/img/info/Mala_stanca.jpg" alt="" style="margin-bottom: 0; margin-top: 0;">
      <div class="no_col_break">
        Godina proizvodnje: 1993. <br>
        Dimenzija od noža do noža:<b> 980x680 mm <br></b>
        <br>
        <h4>Korisne napomene</h4>
        Maksimalno do 20 m noža. Što ima više metara noža i što je veći alat to dulje traje priprema stroja 
        te može znatno usporiti sam proces štancanja. Ako je pultni s pregradama, ulošcima, najbolje
        je odvojiti na 2 alata iako nam stane u dimenziju stroja.
      </div>  
    </div>    
    <br>
  
    <h3>FQD X-1400 Šivačica (Stiching Machine)</h3>

    <div class="vel_info_text col_count_2">

      <div class="no_col_break">
        <img src="/img/info/Dodatna_skripta_-_slike.jpg" alt="" style="padding: 20px;">
        <img src="/img/info/Sivacica.jpg" alt="" style="padding: 20px;">
      </div>

      <div class="no_col_break">
        Godina proizvodnje: 1985. <br>
        Dubina 1400 mm <br>
      </div> <!-- end no col break -->

    </div>

    <h3>Siat - Stretch omatalica</h3>
    
    <div class="vel_info_text col_count_2">
      <div class="no_col_break">
        <img src="/img/info/Stretch_omatalica.jpg" alt="">    
      </div>
      <div class="no_col_break">
         Godina proizvodnje: 1998.
      </div>
    
    </div>
    
    <br> 
    <br>
    
    
    <h3>CNC Felix Gluer Combo XL</h3>

    <div class="vel_info_text col_count_2">
      
       <div class="no_col_break">
        
        <img src="/img/info/001_CNC_felix.png" alt="">
        <br>
        Broj stolova: <b>4</b><br>
        Dimenzija jednog stola: <b>1250x1800 mm</b><br>
        Mogućnost nezavisnog rada 4 stola ili 2 spojena stola. <br>
        U slučaju rada 2 spojena stola dimenzija stola je <b>2500x1800 mm.</b><br>
        Mogućnost apliciranja hladnog i vrućeg ljepila (2 glave vrućeg ljepila i 3 glave hladnog ljepila<br>
        mogućnost istovremenog nanošenja 2 linije hladnog ljepila u jednom potezu). <br>
        Mogućnost nanošenja ljepila točkasto ili u liniji (u slučaju točkastog nanošenja brzina nanošenja je sporija).<br>
        Brzina apliciranja: <b>2.5 m/s.</b><br>
        Potrošnja: <b>5.8 kW</b><br>
        Maksimalna novost jednog stola: <b>300 kg</b><br>
        Maksimalna visina slaganja: <b>500 mm</b><br>
        <br>
        
       

      </div>
      
      <div class="no_col_break">
       
        <img src="/img/info/002_CNC_felix.png" alt="">

        <b>Napomene za nabavu:</b><br>
        <br>
        <b>Vruće ljepilo</b>
        <ul>
          <li>Vruće ljepilo za CNC ljepilicu ne smije biti ljepilo na bazi smole, biljaka ili drugog organskog (EVA) podrijetla jer njihovom upotrebom se smanjuje životni vijek komponentni sustava vrućeg ljepila; preporuka dobavljača stroja je da se upotrebljava ljepilo na bazi plastike (Polyolefin ili PSA – pressure sensitive)</li>
          <li>Ljepilo ne smije imati veću viskoznost na izlazu od 4.000 mPas te vrijeme otvaranja mora biti minimalno 60 sekundi radi bolje učinkovitosti doradnog procesa.</li>
          <li>Maksimalna temperatura ljepila u samom stroju ne smije prekoračiti vrijednost od 195°C</li>
        </ul>

        <br>
        
        <b>Hladno ljepilo</b>
        <ul>
          <li>Hladno ljepilo za CNC ljepilicu mora biti što rjeđe (maksimalna viskoznost 2.500 mPas) te imat mogućnost lijepljenja premaznog kartona.</li>
          <li>Ljepilo mora biti obavezno na bazi vode tzv. drveno ljepilo/bijelo ljepilo</li>
           <li>Ljepilo se ne smije zgrušnjavati tokom eksploatacije jer će u suprotnom začepiti kompletni sustav hladnog ljepila</li>
        </ul>
        
          
      </div>
      
      
    </div><!-- end info text -->
    <br>
    <br>     
    
    <h3 id="semi_automatic_gluer_info">ZX-1300 Velika ljepilica (Semi automatic Gluer)</h3>
    
    <div class="vel_info_text col_count_2">
      <div class="no_col_break">
        <img src="/img/info/Velika_ljepilica.jpg" alt="" >
        <img src="/img/info/Dodatna_skripta_-_slike3.jpg" alt="" style="margin-top: 20px;">
      </div>
      
      <div class="no_col_break">
        Godina proizvodnje: 7/2015.<br>
        Maksimalna dužina: <b>1300 mm <br></b>
        Maksimalna širina: <b> 1000 mm <br></b>
        <br>
        <br>
        <h4>Video upute za veliku ljepilicu</h4>
        <a class="video_link" href="https://youtu.be/6jD6asMP6y8" target="_blank" style="text-align: center; width: 100%;">
          <i class="far fa-play-circle"></i>&nbsp;Rad na prihvatu ljepilice 1
        </a>
        <br>
        <a class="video_link" href="https://youtu.be/fZQfXZh8eMo" target="_blank" style="text-align: center; width: 100%;">
          <i class="far fa-play-circle"></i>&nbsp;Rad na prihvatu ljepilice 2
        </a>
        <br>
        
      </div>
    
    </div>

    
    <h3>Ljepilica - Box Plus 2 (APR solution) </h3>

    <div class="vel_info_text col_count_2">

      <div class="no_col_break">

        <div style="display: flex; align-items: flex-end; flex-wrap: wrap; justify-content: center;">
          <img src="/img/info/001_box_plus_2.jpg" alt="" class="image_float" style="width: 50%; max-width: 600px;">
          <img src="/img/info/002_box_plus_2.jpg" alt="" class="image_float" style="width: 50%; max-width: 600px;">
        </div>
        <img src="/img/info/003_box_plus_2.jpg" alt="" style="max-width: 640px;">


      </div> <!-- end no col break -->


      <div class="no_col_break">
        Ulazna dimenzija: <b>720x1020 mm <br></b>
        Materijal: <br>
        <b>
          - puni papiri od 200-600 g/m2 <br>
          - valovita ljepenka do 4 mm <br>
        </b>
        Maksimalna brzina: <b>150 m/min <br></b>

        Godina proizvodnje: 2020 <br>
        Proizvođač: APR Solution Srl<br>
        Serijski broj: BP2 933<br>
        Ljepilica ima mogućnost nanošenja vrućeg ljepila te obostrano ljepljive trake (duplofana) na mjesta
        lijepljenja.<br>
        Ljepilica može raditi s materijalima od punog kartona, mikrovala te kaširanog mikrovala<br>
        Na ljepilicu je moguće ljepljenje duplofana na juvidur materijal*<br>
        Maksimalni broj točki lijepljenja vrućim ljepilom: 3<br>
        Maksimalno broj točki lijepljenja duplofanom u jednom prolazu: 1<br>
        Minimalni format kutije: 120x120mm**<br>
        Maksimalni format kutije: 720x1020**<br>
        Minimalna težina punog kartona: 150g/m 2<br>
        Maksimalna težina punog kartona: 350g/m 2<br>
        Maksimalna brzina nanošenja ljepila:100 m/min<br>
        Minimalna dužina nanosa ljepila/duplofana: 35mm<br>
        Mogućnost nanošenja ljepila u prekidnoj liniji (minimalni razmak između dvije crte je 25mm)<br>
        <br>
        


        <b style="color: darkred; font-size: 13px;">&nbsp;* bilo kakve druge materijale potrebno je istestirati i u trenutku pisanja ove skripte nisu isprobani</b> <br>
        <b style="color: darkred; font-size: 13px;">** bilo kakva odstupanja od navedenih formata potrebno je provjeriti!</b> <br>


      </div>

    </div>


    <div class="vel_info_text col_count_2">

      <h4 style="margin-top: 0;">Specifikacija za Box Plus 2</h4>

      <div class="no_col_break">

        <img src="/img/info/OBA_SMJERA_LJEPILICA_SMJER_VLAKANA.jpg">
        <img src="/img/info/SAMO_PO_SIRINI_LJEPILICA_SMJER_VLAKANA.jpg">
        
               
        <div class="table_wrap">
         
          <table class="info_table no_min" style="width: 100%;">
            <tbody>

              <tr>
                <td><b>GRAMATURA</b></td>
                <td><b>SMJER VLAKANA</b></td>
              </tr>

              <tr>
                <td>DO 220 g/m2</td>
                <td>SMJER VLAKANA PO DUŽINI I ŠIRINI</td>
              </tr>

              <tr>
                <td>220 - 350 g/m2</td>
                <td>SMJER VLAKANA SAMO PO ŠIRINI</td>
              </tr>

            </tbody>
          </table>
        </div>
        

      </div> <!-- end no col break -->


      <div class="no_col_break">

        
        <h4 style="">Kutija F211</h4>
        
        <img src="/img/info/F211_box_plus.png" />
        

        <div class="table_wrap">

          <table class="info_table no_min" style="width: 100%;">

          <tbody>

           <tr>
              <td><b>DUŽINA</b></td>
              <td><b>MIN</b></td>
              <td><b>MAX</b></td> 
           </tr>

            <tr>
              <td>A</td>
              <td>130</td>
              <td>500</td> 
           </tr>  

            <tr>
              <td>B</td>
              <td>200</td>
              <td>700</td> 
           </tr>


           <tr>
              <td>C</td>
              <td>100</td>
              <td>380</td> 
           </tr>

            <tr>
              <td>D</td>
              <td>12</td>
              <td>20</td> 
           </tr>


          </tbody>
        </table>

        </div>


        <br>
               
        <h4 style="">Kutija F215</h4>
        
        <img src="/img/info/F215_box_plus.png" />
        
        <b>
        Prilikom konstrukcije voditi računa da postoji razmak od 2-5 mm između poklopaca i "ušiju" <br>
        (kada se gleda prirez kutije) kako bi se kutija lakše preklapala.
        </b>
           
           
        <div class="table_wrap">

          <table class="info_table no_min" style="width: 100%;">

          <tbody>

           <tr>
              <td><b>DUŽINA</b></td>
              <td><b>MIN</b></td>
              <td><b>MAX</b></td> 
           </tr>

            <tr>
              <td>A</td>
              <td>150</td>
              <td>500</td> 
           </tr>  

            <tr>
              <td>B</td>
              <td>200</td>
              <td>750</td> 
           </tr>


           <tr>
              <td>C</td>
              <td>100</td>
              <td>390</td> 
           </tr>

            <tr>
              <td>D</td>
              <td>15</td>
              <td>20</td> 
           </tr>


          </tbody>
        </table>

        </div>



      </div> <!-- end no col break -->


    </div> <!-- end info text -->

    <br>
    <br>

  
    <h3 id="nova_vezalica_info">Nova Vezalica</h3>

    <div class="vel_info_text col_count_2">
      
      <div class="no_col_break">
        <img src="/img/info/potrebno_slikati.png" alt="">    
      </div>
      <div class="no_col_break">
       
        XXXXXX: <b>XXXXXXX</b><br>
        XXXXXX: <b>XXXXXXX</b><br>
        <br>
        <br>
        <h4>Video upute za novu vezalicu</h4>
        <a class="video_link" href="https://youtu.be/tAKdAIgsadE" target="_blank" style="text-align: center; width: 100%;">
          <i class="far fa-play-circle"></i>&nbsp;Nova Vezalica 1
        </a>
        <br> 
        <a class="video_link" href="https://youtu.be/CMpVsGdLflA" target="_blank" style="text-align: center; width: 100%;">
          <i class="far fa-play-circle"></i>&nbsp;Nova Vezalica 2
        </a>
        <br> 
        
      </div>
      
    </div>  
  
   
   
   
   
    
    <h3>Velika vakumirka</h3>

    <div class="vel_info_text col_count_2">
      
      <div class="no_col_break">
        <img src="/img/info/potrebno_slikati.png" alt="">    
      </div>
      <div class="no_col_break">
        Dimenzije: <b>860x610 mm</b><br>
        Godina proizvodnje: <b>10/2018.</b><br>
      </div>
      
    </div>

    
    <h3>Mala vakumirka</h3>

    <div class="vel_info_text col_count_2">
      
      <div class="no_col_break">
        <img src="/img/info/potrebno_slikati.png" alt="">    
      </div>
      <div class="no_col_break">
        Dimenzije: <b>530x420 mm</b><br>
        Godina proizvodnje: <b>10/2018.</b><br>
      </div>
      
    </div>   
      
      
    <h3>Rezač duplofana, čičak trake...</h3>

    <div class="vel_info_text col_count_2">
      
      <div class="no_col_break">
        <img src="/img/info/potrebno_slikati.png" alt="">    
      </div>
      <div class="no_col_break">
        XXXXXX: <b>XXXXXXX</b><br>
        XXXXXXX: <b>XXXXXX</b><br>
      </div>
      
    </div>       

    <div class="vel_info_text">
     
      <div class="no_col_break">
        <h4 style="margin-top: 0;">Korisne napomene</h4>
        Na novom krajšeru se može bigati mikroval, na starom krajašeru to nije moguće.
        Na grafičkom nožu može se obrezivati mikroval te jednostrano i obostrano kaširani mikroval.
        Ukoliko se za štancanje uzima ploča koja je veća od dimenzije prireza alata, dodatno razrezivanje (na dimenziju daske) će se odraditi <b>samo pod uvjetom</b> da je odrezani višak upotrebljiv za neki drugi proizvod tj. da je iskoristiv u budućnosti i u tom slučaju višak treba vratiti na skladište.
      </div>
      
            
      <div class="no_col_break">
        <h4 style="margin-top: 0;">Ljepljenje čičaka</h4>
        <b style="color: red;">
          Muški čičak se UVIJEK lijepi na tijelo stalka <br>
          Ženski čičak se UVIJEK lijepi na ekstenzije <br>
        </b>
      </div> <!-- end no col break -->

      
    </div>
    <!-- end info text -->
    <br>
    <br>

  </div>
  <!-- end info page -->
  

  <h2 id="info_dorada_manual">Dorada ručno</h2>  

  <div class="vel_info_page">
    
    
    <div class="vel_info_text">
      <div class="no_col_break">
        <h4 style="margin-top: 0;">PVC prozori</h4>
        
         <a class="video_link" href="https://youtu.be/7izHHe0wZ6g" target="_blank" style="text-align: center; width: 100%;">
          <i class="far fa-play-circle"></i>&nbsp;Ljepljenje PVC prozora
        </a>
      <br>
        <a class="video_link" href="https://youtu.be/rSD-JxXZBZU" target="_blank" style="text-align: center; width: 100%;">
          <i class="far fa-play-circle"></i>&nbsp;Ljepljenje PVC prozora vrućim ljepilom na CNC ljepilici
        </a>
        
        <br> 
      </div>
      
      <div class="no_col_break">
        <h4 style="margin-top: 0;">Duplofan</h4>
        Za valovitu ljepenku obavezno mora koristiti<br>
        <b>bijeli spužvasti duplofan</b>.<br>
        <br>
        Žuti duplofan nikako se ne smije upotrebljavati na tom materijalu!
      </div> <!-- end no col break -->
      <div class="no_col_break">
        <h4 style="margin-top: 0;">Kant traka</h4>
        <a class="video_link" href="https://youtu.be/2sL9OvlHgU0" target="_blank" style="text-align: center; width: 100%;">
          <i class="far fa-play-circle"></i>&nbsp;Ljepljenje kant trake
        </a>
        <br> 
      </div>
      
    </div> <!-- end info text -->
    
    
    <h3>Segmentno kaširanje - problem s mjehurićima</h3>
    
    <div class="vel_info_text">
      <div class="no_col_break">
        <h4 style="margin-top: 0;">Kaširanje Castano Papira</h4>
        <b style="color: darkred;">
        Kod ovog papira pogotovo, ali i kod svih ostalih kaširanja gdje se pojavljuju mjehurići TREBA KORISTITI LJEPILO MEKOL 1428
        </b>
      </div>
    </div> <!-- end info text -->    
     
    <h3>Savjeti za ljepljenje</h3>

    <div class="vel_info_text">

      <div class="no_col_break">

        <h4 style="margin-top: 0;">Lijepljenje materijala sa tiskom</h4>
        Ako na materijalu A imamo tisak sa HP Scitex-a i pokušavamo ga zalijepiti za materijal B na kojem nema tiska, <b>spoj će se odlijepiti u roku 24 h.</b>
        Sa svih površina koje se trebaju lijepti, <b>mora se maknuti tisak</b> da bi proizvod bio kvalitetno zalijepljen.<br>
        <br>
        <b style="text-transform: uppercase;">Svaki put dok radite, složite kutiju, pogledajte kakva je, kako se sastavlja, PRIPADA LI KOJEM STANDARDU i lakše će vam biti raditi.</b>

      </div> <!-- end no col break -->


      <div class="no_col_break">
        
        Ljepilo u granulama <b>(Exomelt 3150)</b> koje ide na automat se može koristiti također za:
        <br>

        <ul>
          <li>karton/karton</li>
          <li>karton/plastika</li>
          <li>plastika/plastika</li>
          <li>PP bez tiska/PP bez tiska</li>
        </ul>
        
        <b>AG STICK 004</b> – ljepilo u patronama za karton

      </div> <!-- end no col break -->

      <div class="no_col_break">
        <h4>Faktori koji utječu na ljepljenje</h4>
        Koje ljepilo će se koristiti ovisi o više faktora:
        <br>

        <ul>
          <li>trenutno zauzeće automata za ljepilo u granulama</li>
          <li>površina koja se treba lijepiti – vremenski nije isto lijepiti klapnu od 60 cm sa specijal ljepilom ili sa gore navedenim ljepilima</li>
          <li>oblik kutije koje se lijepi – nije isto lijepiti kutiju F211 za lilicom ili pillow box</li>
        </ul>
        
        Za površine s lakom koristimo drugačije ljepilo u granulama, <b>Exomelt 3421</b>
        <br>
        <br>
        Bez obzira na sve, pravilo je da mičemo bilo kakav tisak s klapne i drugih površina na koje se nanosi ljepilo u bilo kojem obliku (specijal, vruće ljepilo, duplofan) ako kupcu to neće igrati ulogu u konačnom izgledu proizvoda. <br>
        <br>

      </div> <!-- end no col break -->
      
      
      <div class="no_col_break">
        <h4>Plastificikacija</h4>
        
        <b>AG STICK 008</b> – ljepilo u patronama za plastiku: to ljepilo se može koristiti za lijepljenje površina plastika-plastika i plastika-karton.
        <br>
        <br>
        Plastificikacija se ne mora uvijek lijepiti sa specijal ljepilom - imamo ljepilo u patronama za plastiku: <br>
        <b>BeA HM640-12</b> <br>
        (to ljepilo se može koristiti za lijepljenje površina plastika-plastika i plastika-karton).
        
      </div> <!-- end no col break -->
  
    </div> <!-- end info text -->
 
    <br>    
    <br>     
     
      
  </div>


  
  <h2 id="vanjske_usluge_info">Vanjske usluge tiska</h2>  
  
  <b style="color: red; text-align: left; font-size: 16px;">
   * Kod naručivanja tiska, uvijek se naručuje veća količina araka 5-10% <br>
   ovisno o nakladi i s obzirom kroz koliko procesa proizvod treba proći da bi se završio.
  </b>

  <div class="vel_info_page">
    
    <h3>Foliotisak</h3>
    
    <div class="vel_info_text">
     
      <h4>Kreativni tisak d.o.o.</h4>
      Zagrebačka cesta 18, 10000 Zagreb<br>
      Kontakt: Tomislav Štefanović<br>
      Telefon 1: +385 1 3703 600<br> 
      Telefon 2: +385 1 3750 404<br>
      Telefon/faks: +385 1 3750 405<br>
      Mobitel: +385 91 3703 600<br>
      E-mail: info@kreativni-tisak.hr<br>
      Web: http://www.kreativni-tisak.hr<br>
      Maksimalni format materijala: 900x640 mm<br> 
      Maksimalni format otiska: 710x530 mm<br> 
      Poslati minimalno 40-50 araka viška<br>
      <br>
      <b>U Kreativnom tisku - foliotisak se ne može raditi na kartonu, već se radi na papirima 120-400g tako da kutije možemo odštancati samo nakon što je foliotisak gotov.</b>
      
      <b>Moguća je izrada foliotiska u formatu materijala do B2</b><br>
      
      <h4>MP Graf/ Eko Aroma (Pero Karaklajić)</h4>
      Email: info@ekoaroma.hr<br>
      Mobitel: 099 / 199 1906 Miro<br>
      <br>
      <b style="color: darkred;">MP Graf može tiskati foliju na gotove kutije te da ne mora biti izričito tiskovni arak</b>, a arke i dalje šaljemo u svima poznatom formatu 900x640 mm. Rokovi su mu dosta kraći nego Kreativnom tisku!
      <br>
      <br>
      
      <b>Mi naručujemo kliše u Fibelu i kliše je na naš trošak!</b><br>
      premda je nekad zbog dostavljanja klišea iz Fibela u npr.<br>
      Kreativni tisak jednostavnije da Kreativni tisak daje kliše na izradu.<br>
      <br>
      
      <br>
      <br>
      
    </div>

    <h3>UV lakiranje (parcijalno i potpuno)</h3>

    <div class="vel_info_text">

      UV lak je atraktivna, profinjena i trenutno vrlo tražena tehnika tiska. Postiže se poseban kristalni efekt i nije ga moguće po svom izgledu ili svojstvima zamjeniti plastifikacijom.
      UV lakiranje najčešće podrazumijeva parcijalno lakiranje slika, slogan, logotipa ili određenog dijela teksta ili slike čak I tona boje, no moguće lakirati i cijele stranice.
      Efekt UV laka daje poseban izgleda raznim proizvodima a može biti u sjajnoj ili mat izvedbi. Najbolji efekt UV lakiranja postiže se na mat plastificiranoj podlozi sa sjajnim UV lakom.
      UV lak svojim efektom daje poseban izgled proizvoda, a može se aplicirati na papir sa i bez premaza, sintetski papir, specijalne – ekskluzivne, strukturne papire, plastificirane papire...
      Boja papira je vrlo bitna za što bolji efekt te se uvijek usklađuje sa grafičkim vizualom.
      UV lakiranje radimo u <b>BT Commerce.</b><br>
      Provjeriti može li raditi i Sitotisak Cvirn.<br>
      
    </div>

    <h3>Plastifikacija i laminacija</h3>

    <div class="vel_info_text">

        <h4>Kapital d.o.o.</h4>
        Vrapčanska 15, 10 090 Zagreb<br>
        HR Telefon: +385 1 29 89 714<br>
        Faks: +385 1 29 89 713<br>
        Mobitel 1: +385 98 365 788<br> 
        Mobitel 2: +385 95 2167 590<br>
        E-mail: kapitalzg@inet.hr<br>
        Web: https://kapital.hr/<br>
        Najmanji format 250x250 mm<br>
        Najveći format 1020x1040 mm<br>
        Gramature materijala od 90 do 600 g<br>
        Vrste plastike: mat, sjaj, linen, soft touch, anti scratch, koža, propilenske metalizirane folije, UV lak, glitter<br>
        Imaju i mogućnost laminacije<br> 
        Najmanji format: A8 (52x94 mm)<br> 
        Najveći format: A3 (297x420mm)<br>
        <br>
        <b>U Velpromu plastificiramo samo papire 120-400 g, ne plastificiramo karton</b>
        <br>
    
    </div>


    <h3>Sitotisak</h3>
    <div class="vel_info_text">

      <h4>Tiskara Fertis</h4>
      Stara cesta 50, 10 419 Vukovina<br>
      Kontakt: Srećko Ferjančić, Dubravko Štruklec<br>
      Telefon: +385 1 6253 067<br>
      Fax: +385 1 6253 067<br>
      E-mail: tiskara.fertis@gmail.com
      Web: https://print-centar.net<br>
      <br>
      Najčešće tisak 1 boje (uglavnom crna, zlatna, srebrna boja).<br>
      Može se raditi i višebojni tisak (do 8 boja).<br>
      Otisak se većinom radi na odštancanim kutijama.<br>
      Maksimalna dimenzija sita: 500x370mm <br>
      <br>
      
    </div>


    <h3>Digitalni tisak</h3>
    <div class="vel_info_text">

      <h4>Grafo-Jan d.o.o.</h4>
      Za manje količine tiskanih araka ili konturno rezanih naljepnica koristimo usluge Grafo-Jana.<br>
      <br>
      Medarska 56a, 10000 Zagreb<br>
      Telefon: +385 1 3817 491<br>
      Mobitel 1: +385 98 648 318<br> 
      Mobitel 2: +385 98 1667 666<br>
      E-mail: info@grafojan.com<br>
      Web: http://www.grafojan.com<br>
      <b>Xerox® Versant® 180 Press</b><br>
      Maksimalni format papira: 488x330 mm<br>
      Može koristiti i format 660x330 mm (iz posebne ladice) ali samo do 220 gr.<br>
      Nema duplex i u tom slučaju je lošiji paser<br>
      Iskoristivi dio za tisak: -10 mm sa svake strane (možda i više),<br>
      Dakle 468x310 mm Možda može i -5 mm sa svake strane (478x320 mm)<br>
      
    </div>
    
    <h3>Štancanje</h3>
    
    <div class="vel_info_text">
     
      <div class="no_col_break">
        <h4 style="margin-top: 0;">SAP AMBALAŽA d.o.o.</h4>

        Bobst štanca: 1550x1050 mm<br>
        Kaširaju na 230 g UMKU<br>
        U štancu se materijal uvlači PO VALU<br>
        Štancaju UVIJEK na NALIČJE.<br>
        Alat je dimenzije cijele štance i u ovisnosti o veličini nacrta koji se stavlja na ploču stavljaju se "balansni noževi"<br>
        U ovisnosti o tisku, klin tiska treba biti 15 mm od ruba daske ( može proći i 17 mm)<br>
        Nacrt treba biti centriran ( misli se lijevo -desno)<br>
        Noževi u Bobstu su sa 3 punkta.<br>
     
      </div>
     
      <div class="no_col_break">
        <img src="/img/info/alat_na_automatskoj_stanci.jpeg" alt="" style="padding: 0;">
        Alat za SAP na automatskoj stanci. <br>
        <br>
        Minimalna dimenzija ploče za SAP štancu: <b>400 mm</b>
        <br>
      </div>

      
    </div>    
    

    <h3>Offset tisak</h3>

    <div class="vel_info_text">

      <div class="no_col_break">

        <h4 style="margin-top: 0;"> Stega tisak d.o.o.</h4>
        Heinzelova 60/1, 10000 Zagreb, Croatia Telefon: +385 1 6197 633<br>
        Faks: +385 1 6197 430<br>
        E-mail: stega@stega-tisak.hr<br>
        E-mail: upit@stega-tisak.hr Web: http://www.stega-tisak.hr/<br>
        Napomene:
        Grajfer 11 mm, za klin treba 7 mm<br>
        Za KBA Rapidu treba 20 mm u grajferu za klin???<br>
        <br>

      </div> <!-- end no col break -->

      <div class="no_col_break">
        <h4>TVG - Tiskara d.o.o.</h4>
        Juraja Habdelića 70, Staro Čiče, 10419 Vukovina Telefon: +385 1 6230 033<br>
        Faks: +385 1 6230 034<br>
        Mobitel: +385 91 3333 460 (Krešimir Lieli)
        E-mail: kresimir@tvg.hr, priprema@tvg.hr <br>
        <br>


      </div> <!-- end no col break -->

        <div class="no_col_break">

          <h4>Kerschoffset d.o.o.</h4>
          Ježdovečka 112, 10250 Zagreb, Hrvatska <br>
          Telefon: +385 1 6560 222<br>
          Faks: +385 1 6560 223<br>
          E-mail: kerschoffset@kerschoffset.hr <br>
          Web: http://www.kerschoffset.hr/<br>
          Imaju stroj za digitalni tisak: Kodak NexPress SX2700<br>
          Digital Production Color Press<br>
          <br>
          <b>NAPOMENE:<br></b>
          <b>270g - 500g UMKA</b> diže se premaz! <br>
          Zamjeniti sa 300g - 550g  jer je najstabilniji za premaz. <br>
          Dimenzije: 710 X 1100 mm <br><br>

          <b>VD LAK</b> - ima prehrambeni certifikat <br>
          <b>OFFSETNI LAK</b> - smatra se elastičnijim <br>
          UV LAK:
          <ul> 
            <li>BC komerc  - B1 format</li>
            <li>CVIRN sitotisak - B2 format (rade s efektom i mirisom)</li>
            <li>MAR MAR - B1 format</li>
          </ul>  
          <br>

          Kersch koristi istu foliju kao Kapital<br>
          (ista brzina i zategnutost folije).<br>
          <br>
          <b>Dodatni nož za skretanje napetosti.</b> <br>
          Klin 16/17 mm od ruba i 9 mm na sredini <br>
          Bjelina 7-8 mm (plus-minus 1 mm) <br>
          Iznad 3 tone  - može se naručiti specijalni format! <br>
          <br>
          Kersch traži da u emailu uvijek odgovaramo sa <br>
          <b>REPLAY TO ALL !</b>
        </div>  
          
        <div class="no_col_break">
          <b>Dimenzije Vakumirke u Kerschoffset-u</b>
          <br>
          <div class="table_wrap">
            <table class="info_table no_min" style="width: 100%;">
              <tbody>

                <tr>
                  <td><b>VISINA / mm</b></td>
                  <td><b>DUŽINA / mm</b></td>
                  <td><b>ŠIRINA / mm</b></td>
                </tr>

                <tr>
                  <td>50</td>
                  <td>600</td>
                  <td>400</td>
                </tr>

                <tr>
                  <td>70</td>
                  <td>530</td>
                  <td>380</td>
                </tr>


                <tr>
                  <td>90</td>
                  <td>510</td>
                  <td>360</td>
                </tr>


                <tr>
                  <td>110</td>
                  <td>490</td>
                  <td>340</td>
                </tr>


                <tr>
                  <td>130</td>
                  <td>470</td>
                  <td>320</td>
                </tr>


                <tr>
                  <td>150</td>
                  <td>450</td>
                  <td>300</td>
                </tr>

                <tr>
                  <td>170</td>
                  <td>430</td>
                  <td>280</td>
                </tr>

                <tr>
                  <td>200</td>
                  <td>400</td>
                  <td>250</td>
                </tr>




              </tbody>
            </table>
          </div>
        </div> <!-- end no col break -->

      </div>

    </div>


  <div class="vel_info_page">

      <div class="vel_info_text">

        <div class="no_col_break">
          <h4 style="margin-top: 0;">Print Grupa</h4>
          Brezjanski put 33, 10431 Zagreb, Croatia<br>
          Telefon: +385 1 3371 336<br>
          Faks: +385 1 6404 351<br>
          Mobitel: +385 98 424 358 (Boris Fabek, boris.fabek@printgrupa.com)
          Web: http://www.printgrupa.com/ <br>

        </div>
          <br>
          <br>
          
          <div class="no_col_break">

          <h4>FED</h4>
          - podložna bijela<br>
          <br>
          
        </div>  
          

        <div class="no_col_break">
          
          <h4>Mediaprint Tiskara Hrastić d.o.o.</h4>
          Murati 16, 10000 Zagreb, Hrvatska<br>
          Telefon: +385 1 6609 641<br>
          Faks: +385 1 6609 412<br>
          <br>
          Mobitel: +385 91 6609 641 <br>
          Darko Hrastić, direktor,
          darko@mediaprint-tiskarahrastic.hr<br>
          <br>
          Mobitel: +385 91 5037 008 <br>
          Svjetlana Hrastić Jukić,
          svjetlana@mediaprint-tiskarahrastic.hr<br>
          <br>
          Web: https://mediaprint-tiskarahrastic.hr/<br>

        </div> <!-- end no col break -->


        <div class="no_col_break">
          Napomene:<br>
          Grajfer: 12 mm u grajferu, 5 mm kontra grajfera Maksimalna visina tiska na 720x1020 mm <br>je 706 mm Tiskaju na papir do 450 mikrona<br>
          Ne mogu nabaviti KD 200 g/m2 u formatu 720x1020 mm<br>
          Koriste: KD u formatu 700 x 1000 mm<br>
          <br>
          GC1 u formatu 710 x 1010 mm ili 720 x 1020 cm.<br>
          <br>
        </div>

      </div>



      <br>
      <br>

      <div class="vel_info_text">

        <h4>Naša djeca tiskara d.d.</h4>
        Remetinečka 135,10 020 Zagreb, Hrvatska<br>
        Telefon: +385 1 6539 688<br>
        Faks:+385 1 6539 697<br>
        E-mail: info@nasa-djeca-tiskara.hr<br>
        E-mail: priprema@nasa-djeca-tiskara.hr<br>
        Web: http://www.nasa-djeca-tiskara.hr/<br>
        Napomene:<br>
        Grajfer: 15 mm u grajferu 10 mm kontra grajfera<br>
        Označavanje toka papira:<br>
        kod Naše djece druga dimenzija je tok<br>
        npr 500x700 je dugi tok, 700x500 je kratki tok<br>
        Plastifikacija: ne prolazi im plastika ispod 300 grama<br>
        Maksimalna veličina daske (špere) za cilindar (za štancanje) 720x530 mm<br><br>


        <b>Format B1 (700x1000 mm)<br></b>
        Naručujemo većinom u Stega tisku, Kerschoffset, Mediaprint Hrastić jer imaju dug
        prema nama, mogućnost i TVG (ima puno niže cijene za 300-350 g materijale od ostalih dobavljača), većinom 200 gr. kunstdruck sa sjajnom/matt plastikom ili sjajnim/matt lakom, ali svi oni mogu raditi i 300 i 350 g materijale, postoji i mogućnost proširenog formata B1, odnosno 710x1010 mm (Umka) ili 720x1020 mm (Kunstdruck...).<br>
        <br>
        <b>Format B2 (500x700 mm)</b><br>
        Kerschoffset, ukoliko nemamo izbora, imamo opciju i Naša djeca (min. materijal koji mogu plastificirati je 270 g materijal)<br><br>
        <b>Format B0 (1000x1400 mm)</b><br>
        Samo Print grupa - izbjegavamo raditi u tom formatu jer su nam konkurencija, min. gramatura materijal je 150 g-provjeriti u mailu. <br><br>

      </div>
      <br>
      <br>


      <h3>Razne napomene za vanjske usluge</h3>

      <div class="vel_info_text">

        UMKA B1 dolazi u formatu 710x1010 mm
        Kunstdruck 720x1020 mm (kratki tok je manji problem, imaju ga manje više svi, a dugi tok Velpapir često nema pa je alternativa Gardapack iz Europapira)
        Minimalna dimenzija kartona kod Bilokalnika je 350x700 mm.
        <div class="no_col_break" style="color: red;">

          Štancanje: <br>
          <ol>
            <li>Puni kartoni (npr. High point 350 gr., Invercote Creatto 400 gr., triplex itd) uvijek se štancaju sa lica i stavljaju se kontra bigovi</li>
            <li>Jednostrano kaširani karton, sa lica</li>
            <li>Obostrano kaširani karton, sa naličja</li>
            <li>Direktni tisak (na HP Scitexu i sl.), sa naličja</li>
            <li>Tok papira za kaširanje i val kartona ne smiju se poklapati.</li>
          </ol>
          
        </div>
      </div>
      <!-- end info text -->


    </div>



  <div class="vel_info_page">


    <h2 id="fefco_anchor">FEFCO standardi</h2>

    <h3>FEFCO 201-2</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni.jpg" alt="" style="max-width: 800px;">

    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>SVE</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>Nije potrebno raditi alat, izradjuje se na krajšeru, usjecalica, ljepilica ili šivačica.
              Za manje kutije ovog standarda u dim. npr. 150x150x150 mm i manje od tih, izradjuje se alat te se štanca, optrgava i lijepi ili šiva.

            </td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Peteroslojne</td>
                    <td>Bunt po 10 komada</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>

    <h3>FEFCO 204-2</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni1.jpg" alt="" style="max-width: 800px;">

    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>A) Za boce/staklenke, ležeća 6/1 ili 12/1, najčešće potreban još
              horizontalni umetak( dxš)iz B vala, 1 kom za 6/1 i 2 kom za 12/1 B) sve ostalo
            </td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>Potrebna izrada alata te se štanca, optrgava i lijepi, rijetko se šiva.</td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Peteroslojne</td>
                    <td>Bunt po 10 komada</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>

    <h3>FEFCO 211-2</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni4.jpg" alt="" style="max-width: 800px;">
    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Puni karton:</td>
                    <td>od 250 g. - 600 g., kao što su: high point, tripleks/umka, popset itd.</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>SVE</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>Potrebna izrada alata te se štanca, optrgava i lijepi.</td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Peteroslojne</td>
                    <td>Bunt po 10 komada</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <br>
    <br>
    <h3>FEFCO 215-2</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni5.jpg" alt="" style="max-width: 800px;">

    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Puni karton:</td>
                    <td>od 250 g. - 600 g., kao što su: high point, tripleks/umka, popset itd.</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>SVE</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>Potrebna izrada alata te se štanca, optrgava i lijepi.</td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Peteroslojne</td>
                    <td>Bunt po 10 komada</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>

    <h3>FEFCO 216-2</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni6.jpg" alt="" style="max-width: 800px;">
    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB, BC</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>SVE</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>Potrebna izrada alata te se štanca, optrgava i lijepi.</td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Peteroslojne</td>
                    <td>Bunt po 10 komada</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>

    <h3>FEFCO 217-A2</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni7.jpg" alt="" style="max-width: 800px;">
    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>SVE, staklenke/boce, pokloni</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>Potrebna izrada alata te se štanca, optrgava i lijepi.
              Dno samosloživo, mogućnost propadanja proizvoda kroz dno, bitan omjer težine proizvoda i kvalitete ljepenke(E, B, C ili EB val)
            </td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Peteroslojne</td>
                    <td>Bunt po 10 komada</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>

    <h3>FEFCO 301</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni8.jpg" alt="" style="max-width: 800px;">

    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>SVE</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>Izradjuju se ovisno o količini i učestalosti naručene dimenzije bez alata na krajšer i usjecalicu.
              Ukoliko se radi veća količina učestalo, potrebna izrada alata te se štanca,optrgava i šiva u četri točke dno i četri točke poklopac. Najčešće se izradjuju i isporučuju neformirane, nezašivene.
            </td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Peteroslojne</td>
                    <td>Bunt po 10 komada</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <br>
    <br>

    <h3>FEFCO 304</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni9.jpg" alt="" style="max-width: 800px;">

    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>Beskonačni obrasci 234x12, 380x12 i slično</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>Dno + poklopac <br>
              Potrebna izrada alata te se štanca, optrgava i ljepi u četri točke dno i četri točke poklopac.<br>
              Imaju prednost jer im je visina podesiva
            </td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 15 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Dno i poklopci: </td>
                    <td>pakiraju se u odvojene bunteve</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>

    <h3>FEFCO 307</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni10.jpg" alt="" style="max-width: 800px;">

    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>SVE</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>Dno + poklopac <br>
              Potrebna izrada alata te se štanca i optrgava; kupac sam slaže/formira kutiju.<br>
              Imaju prednost jer im je visina podesiva
            </td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 ili 50 komada, ovisno o veličini</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Dno i poklopci: </td>
                    <td>pakiraju se u odvojene bunteve</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>

    <h3>FEFCO 330-1</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni11.jpg" alt="" style="max-width: 800px;">

    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Puni karton:</td>
                    <td>od 250 g. - 600 g., kao što su: high point, tripleks/umka, popset itd.</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>SVE</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>Dno + poklopac <br>
              Potrebna izrada alata te se štanca i optrgava; kupac sam slaže/formira kutiju.<br>
              Imaju prednost jer im je visina podesiva
            </td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 ili 50 komada, ovisno o veličini</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Dno i poklopci: </td>
                    <td>pakiraju se u odvojene bunteve</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>

    <h3>FEFCO 411-2</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni12.jpg" alt="" style="max-width: 800px;">

    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>SVE, pakiranje stalaka, kartonskih proizvoda, pločastih proizvoda</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>Kartonski plašt <br>
              Nije potrebna izrada alata, izrađuje se na krajšeru, usjecalici ili usjeca sa ručnom električnom pilom za karton
              Kupac sam slaže/formira kutiju <br>
              Alat se izrađuje ukoliko razvijeni plašt možemo štancati zbog formata i ukoliko su veće količine.
            </td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Pakiranje ovisi o </td>
                    <td>dimenziji plašta</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 ili 50 komada, ovisno o veličini</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>

    <h3>FEFCO 421</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni13.jpg" alt="" style="max-width: 800px;">

    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>
              Samosloživa kutija sa integriranim poklopcem, najbolje raditi zatvaranje sa lock buttonom <br>
              Potrebna izrada alata te se štanca i optrgava; kupac sam slaže/formira kutiju <br>
            </td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 ili 50 komada, ovisno o veličini</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>

    <h3>FEFCO 422</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni14.jpg" alt="" style="max-width: 800px;">
    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Puni karton:</td>
                    <td>od 250 g. - 600 g., kao što su: high point, tripleks/umka, popset itd.</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>SVE</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>Ovdje prikazano samo dno, ali najčešće se radi kao dno+poklopac (vidi standard FEFCO 330) <br>
              Potrebna izrada alata te se štanca i optrgava; kupac sam slaže/formira kutiju <br>
              Imaju prednost jer im je visina podesiva
            </td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 ili 50 komada, ovisno o veličini</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Dno i poklopci: </td>
                    <td>pakiraju se u odvojene bunteve</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>

    <h3>FEFCO 422-B</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni15.jpg" alt="" style="max-width: 800px;">
    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Puni karton:</td>
                    <td>od 250 g. - 600 g., kao što su: high point, tripleks/umka, popset itd.</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>SVE, pultni dislay+moguće dodati zaglavlje, shelf ready kutija+dodati pokopac do dna</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>Potrebna izrada alata te se štanca i optrgava; kupac sam slaže/formira kutiju</td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 ili 50 komada, ovisno o veličini</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>

    <h3>FEFCO 422-C</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni16.jpg" alt="" style="max-width: 800px;">

    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Puni karton:</td>
                    <td>od 250 g. - 600 g., kao što su: high point, tripleks/umka, popset itd.</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>SVE, pultni dislay+moguće dodati zaglavlje, shelf ready kutija+dodati pokopac do dna</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>Potrebna izrada alata te se štanca i optrgava; kupac sam slaže/formira kutiju</td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 ili 50 komada, ovisno o veličini</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>

    <h3>FEFCO 422-D</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni17.jpg" alt="" style="max-width: 800px;">

    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Puni karton:</td>
                    <td>od 250 g. - 600 g., kao što su: high point, tripleks/umka, popset itd.</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>SVE, pultni dislay+moguće dodati zaglavlje, shelf ready kutija+dodati pokopac do dna</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>Potrebna izrada alata te se štanca i optrgava; kupac sam slaže/formira kutiju</td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 ili 50 komada, ovisno o veličini</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>

    <h3>FEFCO 425</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni18.jpg" alt="" style="max-width: 800px;">

    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Puni karton:</td>
                    <td>od 250 g. - 600 g., kao što su: high point, tripleks/umka, popset itd.</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>SVE, poklon kutije</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>Ovdje prikazano samo dno, ali najčešće se radi kao dno+poklopac (vidi standard FEFCO 330) <br>
              Potrebna izrada alata te se štanca i optrgava; kupac sam slaže/formira kutiju <br>
              Dupli preklop sa svih strana, nevidi se prijesjek kartona tlocrtno gledano
            </td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 ili 50 komada, ovisno o veličini</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Dno i poklopci </td>
                    <td>pakiraju se u odvojene bunteve</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>

    <h3>FEFCO 426</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni19.jpg" alt="" style="max-width: 800px;">
    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Puni karton:</td>
                    <td>od 250 g. - 600 g., kao što su: high point, tripleks/umka, popset itd.</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>SVE</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>Samosloživa kutija sa integriranim poklopcem, najbolje raditi zatvaranje sa lock buttonom <br>
              Potrebna izrada alata te se štanca i optrgava; kupac sam slaže/formira kutiju
            </td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 ili 50 komada, ovisno o veličini</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <br>
    <br>

    <h3>FEFCO 427</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni20.jpg" alt="" style="max-width: 800px;">
    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Puni karton:</td>
                    <td>od 250 g. - 600 g., kao što su: high point, tripleks/umka, popset itd.</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>SVE</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>
              Samosloživa kutija sa integriranim poklopcem, najbolje raditi zatvaranje sa lock buttonom <br>
              Potrebna izrada alata te se štanca i optrgava; kupac sam slaže/formira kutiju
            </td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 ili 50 komada, ovisno o veličini</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Peteroslojne - EB val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>

    <h3>FEFCO 462</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni21.jpg" alt="" style="max-width: 800px;">
    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Puni karton:</td>
                    <td>od 250 g. - 600 g., kao što su: high point, tripleks/umka, popset itd.</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>Kao mapa, za pločaste materijale, za knjige, rokovnike, prospekte, kalendare</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>
              Samosloživa kutija <br>
              Potrebna izrada alata te se štanca i optrgava; kupac sam slaže/formira kutiju
            </td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 ili 50 komada, ovisno o veličini</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Peteroslojne - EB val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>

    <h3>FEFCO 470</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni22.jpg" alt="" style="max-width: 800px;">
    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>SVE</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>
              Samosloživa kutija sa integriranim poklopcem, najbolje raditi zatvaranje sa lock buttonom <br>
              Potrebna izrada alata te se štanca i optrgava; kupac sam slaže/formira kutiju <br>
            </td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 ili 50 komada, ovisno o veličini</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Peteroslojne - EB val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>

    <h3>FEFCO 471</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni23.jpg" alt="" style="max-width: 800px;">

    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>SVE</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>
              Samosloživa kutija sa integriranim poklopcem <br>
              Potrebna izrada alata te se štanca i optrgava; kupac sam slaže/formira kutiju <br>
            </td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 ili 50 komada, ovisno o veličini</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Peteroslojne - EB val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>

    <h3>FEFCO 820</h3>
    <img src="/img/info/FEFCO_katalog_super_-_djelomicni24.jpg" alt="" style="max-width: 800px;">

    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td><b>Materijal:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Valovita ljepenka:</td>
                    <td>E, B, C, EB</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Puni karton:</td>
                    <td>od 250 g. - 600 g., kao što su: high point, tripleks/umka, popset itd.</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Ostali materijali:</td>
                    <td>Saćasti polipropilen 3 mm</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><b>Mogućnost tiska:</b></td>
            <td>Direktni digitalni tisak, fleksotisak, sitotisak, offsetni tisak+kaširanje</td>
          </tr>
          <tr>
            <td><b>Namjena:</b></td>
            <td>Kao mapa, za pločaste materijale, za knjige, rokovnike, prospekte, kalendare</td>
          </tr>
          <tr>
            <td><b>Način izrade:</b></td>
            <td>
              Samosloživa kutija <br>
              Potrebna izrada alata te se štanca i optrgava; kupac sam slaže/formira kutiju <br>
            </td>
          </tr>
          <tr>
            <td><b>Pakiranje:</b></td>
            <td>
              <table class="sub_table">
                <tbody>
                  <tr>
                    <td class="sub_col_1">Troslojne - E val</td>
                    <td>Bunt po 25 ili 50 komada, ovisno o veličini</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Troslojne - B, C val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                  <tr>
                    <td class="sub_col_1">Peteroslojne - EB val</td>
                    <td>Bunt po 20 ili 25 komada</td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

  </div>
  <!-- end info page -->

  <h2 id="pos_pop_products_info">POS/POP Proizvodi</h2>

  <div class="vel_info_page">

    <h3>Samostojeći podni stalci</h3>
    <div style="display: flex; align-items: flex-end; flex-wrap: wrap; justify-content: center;">
      <img src="/img/info/cedevita_stalak_lijevo_final.jpg" alt="" class="image_float" style="width: 25%;">
      <img src="/img/info/kutjevo_stalak_lijevo_final.jpg" alt="" class="image_float" style="width: 25%;">
      <img src="/img/info/Shake_stalak_veliki_s_kukicama_lijevo_final.jpg" alt="" class="image_float" style="width: 25%;">
      <img src="/img/info/Orion_stalak_za_rakete_lijevo_final.jpg" alt="" class="image_float" style="width: 25%;">
    </div>
    <br style="clear: both;">
    <div class="vel_info_text">
      <div class="no_col_break">
        <ul>
          <li>standardne dimenzije stalka 600x400x1400+200mm header, nosivost 20-tak kg po polici</li>
          <li>većina trgovina ima limite što se tiče visine stalka, max. 1600 mm visine, obavezno provjeriti s kupcem ograničenja</li>
          <li>Dimenzije stalka za chep paletu su 590x390 mm + kutnici za stabilnost stalka. Iako se navode dimenzije chep palete 600x400, dimenzije stalka moraju biti malo manje</li>
          <li>Stalci se uvijek tiskaju na scitexu, osim kada kupac traži određenu panton boju tada kaširamo arke i to izrazito povećava cijenu stalka</li>
          <li>Mogući materijali: karton (standardno), pp, forex, drvo...</li>
        </ul>
      </div>
      <!-- end info ul -->
      <br>
      <br>
      <div class="no_col_break">
        <h4>Potrebne informacije za kalkulaciju stalaka:</h4>
        <ol>
          <li>Redni brojevi polica se <b>UVIJEK</b> broje od dna stalka prema gore! (1. polica je najniža)</li>
          <li>Dimenzije stalaka - navedene dimenzije su unutarnjih ili vanjskih mjera te visina headera?</li>
          <li>Police: a) broj polica, b) dubina polica, c) razmak između polica (od unutarnje donje police do gornje police), d) nosivost polica, e) shelf lineri ?</li>
          <li>Tisak: na bokove, leđa, header, footer - jednostran/obostran tisak, tisak na police - fronte ili i unutrašnjost polica?</li>
          <li>Količina određuje način izrade - štancanje/ploter (u dogovoru s razvojem)</li>
          <li>Načini pakiranja: a) formirano/neformirano, b) pvc crijevo/kartonski plašt/svaki element složen zasebno na svoju paletu (kod većih količina stalaka)</li>
          <li>Uz svaki stalak je obavezno pakirati upute za formiranje - 1 uputa po stalku ili barem po paleti</li>
          <li>Mogući dodatak uz stalak: pvc kadica - dodatna zaštita dna stalka (navoditi posebno u ponudi jer diže cijenu, a nije u standardnoj ponudi)</li>
        </ol>
      </div>
      <!-- end info ul -->
    </div>
    <!-- end info text -->
  </div>
  <!-- end info page -->

  <div class="vel_info_page">
    <h3>Paletni omotači</h3>
    <div style="display: flex; align-items: flex-end; flex-wrap: wrap; justify-content: center;">
      <img src="/img/info/Nestle_paletni_s_2_nadvoja_lijevo_final.jpg" alt="" class="image_float" style="width: 30%;">
      <img src="/img/info/Kala_paletni_desno_final.jpg" alt="" class="image_float" style="width: 30%;">
      <img src="/img/info/Gavrilovic_paletni_desno_final.jpg" alt="" class="image_float" style="width: 30%;">
    </div>
    <br style="clear: both;">
    <div class="vel_info_text">
      <div class="no_col_break">
        <ul>
          <li>Paletni omotači su savršeno rješenje za promociju paletiziranih proizvoda u trgovinama.</li>
          <li>Idealni su za palete s pićem, voćem i povrćem, a pružaju mogućnost promocije sa svih strana.</li>
          <li>Kreativni dizajn paletnih omotača omogućuje da na najjednostavniji način od transportne palete dobijete istaknuto promotivno mjesto za Vaše proizvode.</li>
          <li>Dimenzije, standardne 1200x800x700 i 1200x800x800mm</li>
          <li>Vrste: izrada 4 stranice odvojeno, izrada iz 2 dijela, spajanje stranica pvc čepićima ili</li>
          <li>spajanje čičak trakom</li>
          <li>Tisak: jednostran - scitex</li>
          <li>Ako radimo kontra vala tada obavezno uzorak</li>
          <li>Izrada: ploter ili štancanje ili kombinacija</li>
        </ul>
      </div>
      <!-- end info ul -->
      <div class="no_col_break">
        Mogući dodaci uz paletni omotač: <br>
        1. stupovi i nadvoj (karton) <br>
        2. topper <br>
      </div>
      <div class="no_col_break">
        <ul>
          <li>može biti bez nogica kada se umetne između proizvoda ili s nogicama kada ga se postavlja na proizvode unutar paletnog radi boljeg isticanja i privlačenja kupca)ili samostojeći (s nogicama)</li>
          <li>materijal: karton, kapaline, stadur</li>
          <li>Pakiranje: pvc crijevo ili uredno složeno na euro paletu bez zasebnog pakiranja, kod veće količine svaki element na zasebnoj paleti</li>
        </ul>
      </div>
      <!-- end info ul -->
    </div>
    <!-- end info text -->
  </div>
  <!-- end info page -->


  <div class="vel_info_page">
    <h3>Pultni display (stolni stalak)</h3>
    <div style="display: flex; align-items: flex-end; flex-wrap: wrap; justify-content: center;">
      <img src="/img/info/Fokus_pultni_sa_stepenicama_lijevo_final.jpg" alt="" class="image_float" style="width: 30%;">
      <img src="/img/info/bushman_pultni_lijevo_final.jpg" alt="" class="image_float" style="width: 30%;">
      <img src="/img/info/Henkel_Loctite_pultni_lijevo_final.jpg" alt="" class="image_float" style="width: 30%;">
    </div>
    <div class="vel_info_text">
      
        <ul>
          <li>Stolni displayi su mali i uvijek prisutni na prodajnim mjestima i promotivnim pultovima u cilju stimuliranja impulzivne kupnje.</li>
          <li>Mogu biti nosači proizvoda raznih oblika i dimenzija, kreativnog i privlačnog dizajna.</li>
          <li>Tisak: scitex/ offset/latex</li>
          <li>Pozicija tiska: najčešće bokovi,fronta,header (jednostrano/</li>
          <li>obostrano)</li>
          <li>Dodatak: stepenice (broj stepenica, razmak izmedu</li>
          <li>stepenica, visina fronte, visina leđa) i pregrade</li>
          <li>Izrada: Ploter/stancanje</li>
          <li>Pakiranje: iz jednog komada - pakiranje u transportnu</li>
          <li>kutiju, kada se sastoji od više elemenata tada se zastreča u foliju zajedno s uputama i stavlja u kutiju - cca 100 kom u kutiji ovisno o dimenziji i broju elementa</li>
        </ul>
  
      <!-- end info list -->
    </div>
    <!-- end info text -->
  </div>
  <!-- end info page -->

  <div class="vel_info_page">
    <h3>Viseći display (parazit)</h3>
    <div style="display: flex; align-items: flex-end; flex-wrap: wrap; justify-content: center;">
      <img src="/img/info/After_Eight_parazit_desno_final.jpg" alt="" class="image_float" style="width: 25%;">
      <img src="/img/info/Nesquik_zuti_parazit_desno_final.jpg" alt="" class="image_float" style="width: 25%;">
      <img src="/img/info/Thommy_parazit_2_police_desno_final.jpg" alt="" class="image_float" style="width: 25%;">
      <img src="/img/info/Ferrero_Pocket_Coffee_desno_final.jpg" alt="" class="image_float" style="width: 25%;">
    </div>
    <div class="vel_info_text">
        <ul>
          <li>To su police malih dimenzija koje imaju mogućnost vješanja ili podnog oslanjanja.</li>
          <li>bitne informacije: dimenzije parazita, broj polica,nosivost,razmak između polica,visina fronti, visina headera</li>
          <li>police možemo mi uljepljivati ili kačiti. Pokušati nuditi kačenje jer izbjegavamo dodatnu doradu</li>
          <li>Materijal: najčešće karton,ali moze i od drugih materijala</li>
          <li>Tisak: jednostran/obostran •</li>
          <li>Pakiranje: ako se sastoji iz jednog dijela,moze se uredno</li>
          <li>složiti u kartonsku kutiju, ako se sastoji od više elemenata strechati ili pvc crijevo + transportna kutija 50-100 kom po kutiji</li>
        </ul>
      <!-- end info list -->
    </div>
    <!-- end info text -->
  </div>
  <!-- end info page -->


<h2>Price i shelf lineri</h2>

  <div class="vel_info_page">

    <h3>PRICE LINER</h3>

    <div class="vel_info_text col_count_2">
    
    
    <div class="no_col_break">
      <img src="/img/info/001_price_liner.png" alt="" >
      <img src="/img/info/002_price_liner.png" alt="" >
    </div>
    
    
     
      <div class="no_col_break">
        <ul>
          <li>izrađuje se od PVC-a</li>
          <li>duplofanom se lijepi na frontu police</li>
          <li>u price liner se umeće shelf liner (trakica sa tiskom)</li>
          <li>dimenzije: 580 x 39 mm, 560 x 39 mm, 1200 x 39 mm</li>
          <li>za Barcafe “ECO” - izrađujemo price liner od kartona (e-val)</li>
        </ul>
      </div>
    </div>
    <!-- end info text -->
  </div>
  <!-- end info page -->

  <div class="vel_info_page">

    <h3>SHELF LINER (trakica sa tiskom)</h3>
    
    <div class="vel_info_text col_count_2">
     
      <div class="no_col_break">
        <img src="/img/info/001_shelf_liner.png" alt="" >
        <img src="/img/info/002_shelf_liner.png" alt="" >
      </div>
      
      <div class="no_col_break">
        <ul>
          <li>izrađuje se od papira veće gramature (npr. KD 250g/m2)</li>
          <li>sa tiskom</li>
          <li>umeće se u price liner</li>
        </ul>
      </div>

    </div>
    <!-- end info text -->
  </div>
  <!-- end info page -->
<div style="display: flex; align-items: flex-end; flex-wrap: wrap; justify-content: center;">
  <img src="/img/info/shelf_and_price_liners.png" alt="" class="image_float" style="width: 90%; max-width: 800px;">
</div>

<br>
<br>



<h2 id="package_products_info">Ambalažni proizvodi</h2>

  <div class="vel_info_page">

    <h3>Kartonske kutije</h3>
    <div style="display: flex; align-items: flex-end; flex-wrap: wrap; justify-content: center;">
      <img src="/img/info/Drniska_prsutana_kutija_2_final.jpg" alt="" class="image_float" style="width: 25%;">
      <img src="/img/info/Adrialab_djecja_kozmetika_final.jpg" alt="" class="image_float" style="width: 25%;">
      <img src="/img/info/Nesquik_SRP_lijevo_final.jpg" alt="" class="image_float" style="width: 25%;">
      <img src="/img/info/Fokus_likovni_set_kutija_dip_lijevo_final.jpg" alt="" class="image_float" style="width: 25%;">
    </div>
    <div style="display: flex; align-items: flex-end; flex-wrap: wrap; justify-content: center;">
      <img src="/img/info/InMusic_kutija_lijevo_final.jpg" alt="" class="image_float" style="width: 25%;">
      <img src="/img/info/Vincek_kutija_za_torte_final.jpg" alt="" class="image_float" style="width: 25%;">
      <img src="/img/info/fokus_ART_BOX_lijevo_final.jpg" alt="" class="image_float" style="width: 25%;">
    </div>
    <div class="vel_info_text">
      
        <ul>
          <li>Dimenzije: dužina x širina x visina - mi uvijek razgovaramo o unutarnjim dimenzijama kutija</li>
          <li>Vrsta kutija - transportne, arhivske, poklon kutije, kutije za prehranu (torte, kolače, vjenčanja), kutije za boce, staklenke, ekskluzivne kutije, glasačke kutije, kutije za nagradnu igru, shelf ready kutije - određujemo FEFCO standardima</li>
          <li>Mogući dodaci - kartonski umetci/ulošci</li>
          <li>Materijal: 250 g-400 g papir/karton/siva ljepenka -2 ili 3mm, PP</li>
          <li>obavezno pitati nosivost kutije kako bi se mogao odrediti materijal ili potrebno dodatno</li>
          <li> ojačanje</li>
          <li>mogućnost tiska: 1. Naš tisak, 2. vanjska usluga</li>
          <li>Izrada: krajšer (f201), štancanje, rezanje na cutteru (ploter)-manje količine Pakiranje: neformirano, složeno u bunteve te na paletu</li>
        </ul>

      <!-- end info list -->
      <div class="no_col_break">
        Količina po buntevima: troslojni materijal, peteroslojni, dno+poklopac <br>
        <br>
        <ul>
          <li>Posebne kutije: ekskluzivne kutije - ručno presvlačenje kutija papirima u boji 120-140g, isporučuju se formirane kutije, pakirane u kutiji, složene na paletu; kod velikih količina koristimo vanjsku uslugu Markek</li>
        </ul>
      </div>
      <!-- end info list -->
    </div>
    <!-- end info text -->
  </div>
  <!-- end info page -->

<h2 id="ostali_proizvodi_info">Ostali proizvodi</h2>

  <div class="vel_info_page">

    <div class="vel_info_text">
      <h4>1. Samostojeći totem (display)/reklamni pano (pancart)/banner</h4>
      Ravni display u prirodnoj veličini može biti uzbudljiv i efektan način skretanja pozornosti na proizvod, izložbu ili važan događaj. Često se koriste u kinima, političkim kampanjama ili prilikom promocije novih proizvoda ili ponuda.
      <br>
      <br>
      <div class="no_col_break">
        Vrste:

        <ol>
          <li>S 1 nogicom na stražnjoj strani po skoro cijeloj duljini panoa/totema čime ga čini veoma stabilnim, ali može biti samo s jednostranim tiskom</li>
          <li>S 2 nogice koje se nalaze na dnu panoa/totema, tisak
            može biti jednostrani ili obostrani, najčešće je obostrani, paziti u slučaju ako je jednostran tisak panoa/totema da li ide tisak na nogice. Ako ide tisak na nogice onda bez obzira što jedna strana nema tiska, mora ići obostrani tisak da ne bi jedna strana nogice bila bijela, a druga s tiskom
          </li>
        </ol>
        
      </div>
      <!-- end info list -->
      Materijal: karton, kapa line 10 mm, stadur <br>
      Tisak: scitex/latex, može biti jednostran/obostran Pakiranje: strečano u foliju ili pvc crijevo, složeno na paletu
      <br>
      <br>
      <h4>2. Promotivna kutija</h4>
      Promo/reklamne kutije su idealan, promotivni alat za brzu prodaju proizvoda široke potrošnje. Mogu biti različitih oblika i dimenzija tako da se savršeno prilagođavaju Vašem proizvodu i njegovom mjestu u trgovini. Mogu biti i glasačke kutije i kutije za nagradne igre.
      Karakteristične velike površine za tisak su idealne za promociju brenda ili informacije o akcijskoj prodaji.
      <h4>3. Promotivni zid</h4>
      Tisak: scitex ili latex
      Izrada: ploter
      Isporuka: mi vršimo montažu ukoliko to nije moguće tada se pakira u kartonske plašteve i šalje kupcu
      s detaljnim uputama slaganja
      <h4>4. Etikete</h4>
      Samoljepive i nesamoljepive <br>
      Tisak: latex 4/0 <br>
      Vrsta materijala: monomerne folije (rok trajanja 1-2 godine) i polimerne folije Možemo ih rezati, ali ne možemo ricati te je teško odljepiti naljepnicu s podloge <br>
      Izrađujemo manje količine, a za veće se koristi vanjska usluga - Grafojan (isporuka u arcima, tisak 4/0), moguć Sato i Agens, ali količine od par tisuća komada (isporuka je u rolama, moguć tisak u panton bojama, foliotisak, blindruck...) <br>
      Magnetne naljepnice - tisak latex, koristi se posebna folija, može se npr. staviti neka oznaka na auto <br>
      Naljepnice za table - obavezno pitati lokaciju i visinu postavljanja <br>
      Vizitke - Grafojan <br>
      <h4>5. Cerade</h4>
      Tisak latex, paziti na naše max. Dimenzije latexa <br>
      Potrebno umetanje ringova <br>
      Možemo i postaviti ceradu, ali je bitna lokacija te visina na koju stavljamo ceradu <br>
      <h4>6. Reklamni proizvodi s gotovom konstrukcijom</h4>
      Mi apliciramo foliju - latex Konstrukcije kupujemo u Fortuni
      <br>
      <h4>7. Sjenila za vjetrobranska stakla automobila</h4>
      <img src="/img/info/11.jpg" alt="" style="max-width: 200px;" />
      <br>
      Materijal: karton-mikroval
      Tisak: scitex, jednostran ili obostran Pakiranje: 30 kom u transportnu kutiju
      <h4>8. Zidna grafika</h4>
      Vrlo jednostavan i ekonomičan način za oslikavanje zidova, <br>
      nakon završetka kampanje, naljepnice se jednostavno odstrane
      <h4>9. Prozorska grafika</h4>
      One-way vision folija - samoljepivi PVC materijal specijalno izrađen za aplikacije na prozorima. Omogućava prodor svjetlosti, s vanjske strane se vidi aplikacija, a s unutrašnje strane je blago zatamnjeno staklo prozora.
      <h4>10. Roll up banner</h4>
      <img src="/img/info/roll_up_1000x2000_final.jpg" alt="" style="max-width: 200px;" />
      <br>
      
      <div class="no_col_break">
        <h4>8. Promotivni pult</h4>
        <img src="/img/info/promo_pult_Gavrilovic_final.jpg" alt="" style="max-width: 200px;" />
      </div> <!-- end no col break -->
      
      <br>
      
      Vrste : četvrtasti plastični bez table ili s tablom <br>
      POP UP COUNTER ovalni drveno-plasticni s i bez table,izrazito čvrst i vizualno ljepši, ali skuplji od plastičnog <br>
      Info pultevi na sajmovima ili pokretni pultevi su savršeni za promocije ili degustacije proizvoda u supermarketima <br>
      <h4>9. Reklamne table u obliku slova A s plocom pisi brisi (flomaster/ kreda) ili s tiskom - A board</h4>

      <img src="/img/info/a_stalak_zarotiran_final.jpg" alt="" style="max-width: 200px;" />
      <img src="/img/info/a_stalak_B2_final.jpg" alt="" style="max-width: 200px;" />
    </div>
    <!-- end info text -->
  </div>
  <!-- end info page -->

<h2 id="anchor_zastita_na_radu">Zaštita na radu</h2>


<div class="vel_info_page">

    <div class="vel_info_text">
      
      Detaljne informacije o Zaštiti na radu se nalaze u ovom PDF dokumentu:
       <br>
       <a href="/img/info/Osposobljavanje_radnika_za_rad_na_siguran_nacin.pdf" target="_blank" style="font-size: 20px;">
       Osposobljavanje radnika za rad na siguran način&nbsp;<i class="fal fa-external-link"></i>
       </a>  
      
      
    </div> <!-- end info text --> 
    
</div> <!-- end info page -->



<h2 id="anchor_info_piktogrami">Piktogrami opasnosti</h2>

  <div class="vel_info_page">


    <h4 style="margin-top:  0px;">GLOBALNO USKLAĐENI SUSTAV RAZVRSTAVANJA I OZNAČIVANJA KEMIKALIJA (GHS)</h4>
    <div class="vel_info_text">
      Da bi se olakšala trgovina na svjetskoj razini i istovremeno zaštitilo zdravlje ljudi i okoliš,
      u okviru Ujedinjenih naroda (UN) su tijekom 12 godina brižljivoga rada izrađeni usklađeni kriteriji za razvrstavanje i označivanje, koji su rezultirali Globalno usklađenim sustavom razvrstavanja i označivanja kemikalija (GHS).
      Opasnosti pri upotrebi opasnih tvari i smjesa prikazane su piktogramima u obliku dijamanta s crvenim obrubom.
    </div>
    <!-- end info text -->
    <h3>DIO 1: FIZIKALNE OPASNOSTI</h3>
    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td>
              <b>GHS01<br></b>
              Eksplodirajuća bomba
            </td>
            <td>
              <img src="/img/info/GHS01-explos.jpg" alt="" />
            </td>
            <td>
              <ul>
                <li>Nestabilni eksplozivi</li>
                <li>Eksplozivi iz odjeljaka 1.1., 1.2., 1.3. i 1.4.</li>
                <li>Samoreagirajuće tvari i smjese, tip A i B</li>
                <li>Organski peroksidi, tip A i B</li>
              </ul>
            </td>
          </tr>
          <tr>
            <td>
              <b>GHS02 <br></b>
              Plamen
            </td>
            <td>
              <img src="/img/info/GHS02-flamme.jpg" alt="" />
            </td>
            <td>
              <ul>
                <li>Zapaljivi plinovi, 1. kategorija opasnosti</li>
                <li>Zapaljivi aerosoli, 1. i 2. kategorija opasnosti</li>
                <li>Zapaljive tekućine, 1., 2. i 3. kategorija opasnosti</li>
                <li>Zapaljive krutine, 1. i 2. kategorija opasnosti</li>
                <li>Samoreagirajuće tvari i smjese, tip B, C, D, E, F</li>
                <li>Piroforne tekućine, 1. kategorija opasnosti</li>
                <li>Piroforne krutine, 1. kategorija opasnosti</li>
                <li>Samozagrijavajuće tvari i smjese, 1. i 2. kategorija opasnosti</li>
                <li>Tvari i smjese koje u dodiru s vodom oslobađaju zapaljive plinove, 1., 2. i 3. kategorija opasnosti</li>
                <li>Organski peroksidi, tip B, C, D, E, F</li>
              </ul>
            </td>
          </tr>
          <tr>
            <td>
              <b>GHS03 <br></b>
              Plamen iznad prstena
            </td>
            <td>
              <img src="/img/info/GHS03-rondflam.jpg" alt="" />
            </td>
            <td>
              <ul>
                <li>Oksidirajući plinovi, 1. kategorija opasnosti</li>
                <li>Oksidirajuće tekućine, 1., 2. i 3. kategorija opasnosti</li>
                <li>Oksidirajuće krutine, 1., 2. i 3. kategorija opasnosti</li>
              </ul>
            </td>
          </tr>
          <tr>
            <td>
              <b>GHS04 <br></b>
              Plinska boca
            </td>
            <td>
              <img src="/img/info/GHS04-bottle.jpg" alt="" />
            </td>
            <td>
              <ul>
                <li>Plinovi pod tlakom</li>
                <li>Stlačeni plinovi</li>
                <li>Ukapljeni plinovi</li>
                <li>Ohlađeno ukapljeni plinovi</li>
                <li> Otopljeni plinovi</li>
              </ul>
            </td>
          </tr>
          <tr>
            <td>
              <b>GHS05 <br></b>
              Nagrizanje
            </td>
            <td>
              <img src="/img/info/GHS05-acid_red.jpg" alt="" style="transform: rotate(-45deg) scale(0.75)" />
            </td>
            <td>
              <ul>
                <li>Nagrizajuće za metale, 1. kategorija opasnosti</li>
              </ul>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>


    <h3>DIO 2: OPASNOSTI ZA ZDRAVLJE</h3>
    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td>
              <b>GHS05 <br></b>
              Nagrizanje
            </td>
            <td>
              <img src="/img/info/GHS05-acid_red.jpg" alt="" style="transform: rotate(-45deg) scale(0.75)" />
            </td>
            <td>
              <ul>
                <li>Nagrizajuće za kožu, 1.A, 1.B i 1.C kategorija opasnosti</li>
                <li>Teška ozljeda oka, 1. kategorija opasnosti</li>
              </ul>
            </td>
          </tr>
          <tr>
            <td>
              <b>GHS06 <br></b>
              Mrtvačka glava s prekriženim kostima
            </td>
            <td>
              <img src="/img/info/GHS06-skull.jpg" alt="" />
            </td>
            <td>
              <ul>
                <li>Akutna toksičnost (gutanje, preko kože, udisanje), 1., 2. i 3. kategorija opasnosti</li>
              </ul>
            </td>
          </tr>
          <tr>
            <td>
              <b>GHS07 <br></b>
              Uskličnik
            </td>
            <td>
              <img src="/img/info/GHS07-exclam.jpg" alt="" />
            </td>
            <td>
              <ul>
                <li>Akutna toksičnost (gutanje, preko kože, udisanje), 4. kategorija opasnosti</li>
                <li>Nadražujuće za kožu, 2. kategorija opasnosti</li>
                <li>Nadražujuće za oko, 2. kategorija opasnosti</li>
                <li>Preosjetljivost kože, 1. kategorija opasnosti</li>
                <li>Toksičnost za ciljani organ – jednokratna izloženost, 3. kategorija opasnosti</li>
                <li>Nadraživanje dišnog sustava</li>
                <li>Narkotički efekti</li>
              </ul>
            </td>
          </tr>
          <tr>
            <td>
              <b>GHS08 <br></b>
              Opasnost za zdravlje
            </td>
            <td>
              <img src="/img/info/GHS08-silhouete.jpg" alt="" />
            </td>
            <td>
              <ul>
                <li>Preosjetljivost ako se udiše, 1. kategorija opasnosti</li>
                <li>Mutageni učinak na zametne stanice, 1.A, 1.B i 2. kategorija opasnosti</li>
                <li>Karcinogenost, 1.A, 1.B i 2. kategorija opasnosti</li>
                <li>Reproduktivna toksičnost, 1.A, 1.B i 2. kategorija opasnosti</li>
                <li>Specifična toksičnost za ciljani organ – jednokratno izlaganje, 1. i 2. kategorija opasnosti</li>
                <li>Specifična toksičnost za ciljani organ – ponavljano izlaganje, 1. i 2. kategorija opasnosti</li>
                <li>Opasnost od aspiracije, 1. kategorija opasnosti</li>
              </ul>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <br>
    <h3>DIO 3: OPASNOSTI ZA OKOLIŠ</h3>
    <div class="table_wrap">
      <table class="info_table">
        <tbody>
          <tr>
            <td>
              <b>GHS09 <br></b>
              Okoliš
            </td>
            <td>
              <img src="/img/info/GHS09-Aquatic-pollut-red.jpg" alt="" />
            </td>
            <td>
              <ul>
                <li>Nagrizajuće za kožu, 1.A, 1.B i 1.C kategorija opasnosti</li>
                <li>Teška ozljeda oka, 1. kategorija opasnosti</li>
              </ul>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>
  </div>
  <!-- end info page -->

  <h2 id="transport_info">Logistika</h2>
  

  
    
        
  
  <div class="vel_info_page">
   
   
    <h3>Označavanje broja komada u paketu i na otpremnici</h3>

   

    <div class="vel_info_text" style="margin-bottom: 10px;">
      <div class="no_col_break">
      Ukoliko kupac <b>NE dostavlja svoje otpremnice</b>, potrebno je naziv proizvoda navesti na stranom jeziku.<br>
      </div>
      <br>
      <div class="no_col_break">
        U slučaju kada <b>kupac DOSTAVLJA svoje otpremnice</b> (npr. Stemmler), na radnom nalogu potrebno je navesti koja se kratica za količinu koristi (npr. njem. stk, eng. pcs) ili već na paletnoj kartici imati integriranu oznaku komada pa samo brojčano upisati količinu.<br>
      </div>
      <br>
<div class="no_col_break">
Ukoliko se <b>na paletnom mjestu nalazi više paketa</b> koji su dio jedne otpreme, potrebno je na <b>svaki paket</b> napisati ispravnu <b>kraticu za komade</b>. Te pakete <b>skladištari su dužni pregledati</b> te vratiti nazad u proizvodnju ako oznake nisu ispravne.<br><br>
      </div>

      
    </div> <!-- end info text -->
 
<b>Prodaja je dužna navesti na kojem jeziku trebaju biti priložene upute za sklapanje proizvoda.<br></b>

Ukoliko se pokaže potreba za jos nekim jezikom  dodajte kako se piše. ( švedski, slovenski, francuski, grčki ...)<br>
<br>
<b>
  Hrvatski: KOM<br>
  Njemački: STK<br>
  Engleski: PCS<br>
</b>
<br>
<br>
<br>
  
 
   
   
   
   
    
    <h3>Euro paleta</h3>

    <b style="color: red;">* Broj paletnih mjesta za sva vozila odnosi se isključivo na standardne Euro palete.</b>

    <div class="vel_info_text col_count_2">
     
     <div class="no_col_break">
      <img src="/img/info/1200px-Plan_palette-europe.jpg" alt="">
     </div> <!-- end no col break --> 
      
      Euro paleta izrađena je od drveta s točno propisanim dimenzijama. <br>
      Dimenzija: 1200 x 800 x 144 mm <br>
      Težina: cca 25 kg <br>
      Nosivost: 1500 kg <br>
      Volumen: 0,96 m3 <br>
    </div> <!-- end info text -->


    
    <h3>Incoterms oznake (pariteti)</h3>

<br>     
       
<div class="table_wrap">               
  <table class="info_table" style="max-width: 500px; float: left;">
    <tr><td>ŠIFRA</td><td>NAZIV</td></tr>
    <tr><td>CFR</td><td>trošak i vozarina</td></tr>
    <tr><td>CIF</td><td>trošak, osiguranje i vozarina</td></tr>
    <tr><td>CIP</td><td>vozarina i osiguranje plaćeni do</td></tr>
    <tr><td>CPT</td><td>vozarina plaćena do</td></tr>
    <tr><td>DAP</td><td>predano na mjestu</td></tr>
    <tr><td>DDP</td><td>predano ocarinjeno</td></tr>
    <tr><td>DPU</td><td>predano na mjesto istovareno</td></tr>
    <tr><td>EXW</td><td>franko tvornica</td></tr>
    <tr><td>FAS</td><td>franko uz bok broda</td></tr>
    <tr><td>FCA</td><td>franko prijevoznik</td></tr>
    <tr><td>FOB</td><td>franko brod</td></tr>
    <tr><td>XXX</td><td>uvjeti isporuke drugačiji od gore navedenog</td></tr>
  </table>
</div>

<br>     
<img src="/img/info/Pariteti_slikovno_2020.png" alt="" style="min-width: 100%;">      
<br>          
        
<div class="table_wrap">   
     
  Kratice: <br>
  <b>P</b> ‐ prijevoz <br>
  <b>O</b> ‐ osiguranje <br>
  <b>n/a</b> ‐ ne primjenjuje se <br>
  <br>

       
  <b>         
   * u svim slučajevima ukoliko prodavatelj snosi trošak prijevoza i / ili osiguranja do mjesta isporuke u RH <br>
   dio troškova koji se odnosi na relaciju od u RH pa do mjesta isporuke može se isključiti iz carinske vrijednosti <br>
   ukoliko je iznos troška naznačen i razdvojen od ostalih troškova
  </b>
    <br>

    <table class="info_table incoterms_table" style="width: 100%; min-width: 1300px;">
    <tbody>
    <tr class="incoterms_header" style="background: #eaeaea;">
    <td><b>OZNAKA</b></td>
    <td><b>ZNAČENJE</b></td>
    <td><b>OPIS</b></td>
    <td style="  text-align: center;  width: 20%;  min-width: 200px;">
    <b>INCOTERMS</b><br>
    <span class="half_div" style="height: auto;"><b>2000</b></span>
    <span class="half_div" style="height: auto;"><b>2010</b></span>
    </td>
    <td><b>NAPOMENA *</b></td>
    </tr>
    
    
    
    <tr>
    <td>EXW</td>
    <td>franko tvornica (imenovano mjesto)</td>
    <td>Prodavatelj je izvršio svoju obvezu, te trošak i rizik nad robom prelazi na kupca kad prodavatelj stavlja robu na raspolaganje kupcu i to u svojim prostorijama ili na drugom imenovanom mjestu. Roba ne mora biti utovarena, niti obavljeno izvozno carinjenje.</td>
    <td style="text-align: center;">+ Utovar + P + O</td>
    <td>Od tvornice do ulaska u RH</td>
    </tr>
    
    
    
    
    <tr>
    <td>FCA</td>
    <td>franko prijevoznik (imenovano mjesto isporuke)</td>
    <td>Prodavatelj je izvršio svoju obvezu, te trošak i rizik nad robom prelazi na kupca kad prodavatelj stavlja robu na raspolaganje prijevozniku (ili drugoj osobi koju je imenovao kupac), izvozno ocarinjenu i to u prodavateljevim prostorijama (utovarena na prijevozno sredstvo kupca) ili na nekom drugom imenovanom mjestu na prijevoznom sredstvu prodavatelja.</td>
    <td style="text-align: center;">+ P + O + prekrcaj</td>
    <td>Od imenovanog mjesta do ulaska u RH. Ukoliko je imenovano mjesto prostorije prodavača nema prekrcaja</td>
    </tr>
    
        
    <tr>
    <td>FAS</td>
    <td>franko uz bok broda (imenovano mjesto otpreme)</td>
    <td>Prodavatelj je izvršio svoju obvezu kad je isporučio robu izvozno ocarinjenu uz bok broda (npr. dok luke ili teglenica) kojeg je imenovao kupac u imenovanoj luci otpreme.</td>
    <td style="text-align: center;">+ P + O + utovar</td>
    <td>Od imenovanog mjesta do ulaska u RH</td>
    </tr>

    <tr>
    <td>FOB</td>
    <td>franko brod (imenovano mjesto otpreme)</td>
    <td>Prodavatelj je izvršio svoju obvezu kad je isporučio robu izvozno ocarinjenu na brodu koji je imenovao kupac u imenovanoj luci otpreme.</td>
    <td style="text-align: center;">+ P + O</td>
    <td>Od imenovanog mjesta do ulaska u RH</td>
    </tr>
    
    <tr>
    <td>CFR</td>
    <td>Cijena sa vozarinom (imenovana luka odredišta)</td>
    <td>Prodavatelj je izvršio svoju obvezu kad je isporučio robu izvozno ocarinjenu na brodu koji je imenovao kupac u imenovanoj luci otpreme. U tom trenutku rizik nad robom prelazi sa prodavatelja na kupca. Međutim, prodavatelj je dužan ugovoriti i platiti troškove i vozarinu, te pristojbe za istovar, a potrebno kako bi robu dopremio do imenovane luke odredišta.</td>
    <td style="text-align: center;">+ O</td>
    <td>Ukoliko je odredište strana luka još + P od strane luke do ulaska u RH</td>
    </tr>
    
    <tr>
    <td>CIF</td>
    <td>Cijena sa osiguranjem i vozarinom (imenovana luka odredišta)</td>
    <td>Prodavatelj je izvršio svoju obvezu kad je isporučio robu izvozno ocarinjenu na brodu koji je imenovao kupac u imenovanoj luci otpreme. U tom trenutku rizik nad robom prelazi sa prodavatelja na kupca. Međutim, prodavatelj je dužan ugovoriti i platiti sve troškove, vozarinu i osiguranje (minimalno pokriće), te pristojbe za istovar u imenovanoj luci odredišta.</td>
    <td style="text-align: center;">+ (nula)</td>
    <td>Ukoliko je odredište strana luka još + P + O od strane luke do ulaska u RH</td>
    </tr>   

   
   
    <tr>
    <td>CPT</td>
    <td>Vozarina plaćena do... (imenovano mjesto odredišta)</td>
    <td>Prodavatelj je izvršio svoju obvezu kad je isporučio robu izvozno ocarinjenu prijevozniku (ili drugoj osobi koju je imenovao kupac) u ugovorenom mjestu. Prodavatelj je dužan ugovoriti i platiti troškove prijevoza do imenovanog odredišta. Rizik nad robom prelazi na kupca u prvoj točci gdje prodavatelj preda robu prijevozniku (ili drugdje ako se tako kupac i prodavatelj dogovore), ali prodavatelj snosi trošak prijevoza.</td>
    <td style="text-align: center;">+ O (odredište RH)</td>
    <td>Ukoliko je odredište u inozemstvu još + P od odredišta do ulaska u RH</td>
    </tr>
       
    
    <tr>
    <td>CIP</td>
    <td>Vozarina i osiguranje plaćeni do (imenovano mjesto odredišta)</td>
    <td>Prodavatelj je izvršio svoju obvezu kad je isporučio robu izvozno ocarinjenu prijevozniku (ili drugoj osobi koju je imenovao kupac) u ugovorenom mjestu. Prodavatelj je dužan ugovoriti i platiti troškove prijevoza i osiguranja (minimalno pokriće) do imenovanog odredišta. Rizik nad robom prelazi na kupca u prvoj točci gdje prodavatelj preda robu prijevozniku (ili drugdje ako se tako kupac i prodavatelj dogovore), ali prodavatelj snosi trošak prijevoza i osiguranja.</td>
    <td style="text-align: center;">+ (nula)</td>
    <td>Ukoliko je odredište u inozemstvu + P + O od odredišta do ulaska u RH</td>
    </tr>
    
    <tr>
    <td>DAF</td>
    <td>Isporučeno granica (imenovano mjesto)</td>
    <td>Prodavatelj je izvršio svoju obvezu kad je isporučio robu izvozno ocarinjenu, spremnu za istovar sa prijevoznog sredstva na imenovanoj granici, prije carinske crte RH. Izraz granica može se koristit za bilo koju granicu uključujući i zemlju izvoza.</td>
    <td>
      <div class="half_div">+ iskrcaj + O</div>
      <div class="half_div cit_grey">n/a</div>
    </td>
    <td>Ukoliko je granica mjesto ulaska u RH. Ako je druga granica još + P od granice do RH</td>
    </tr>
    
    <tr>
    <td>DES</td>
    <td>sporučeno franko brod (imenovana luka odredišta)</td>
    <td>Prodavatelj je izvršio svoju obvezu kad je isporučio robu izvozno ocarinjenu na brodu koji je imenovao kupac
u imenovanoj luci odredišta.</td>
    <td>
      <div class="half_div">+ iskrcaj + O</div>
      <div class="half_div cit_grey">n/a</div>
    </td>
    <td>RH luka</td>
    </tr>
    
    <tr>
    <td>DEQ</td>
    <td>Isporučeno franko obala (imenovana luka odredišta)</td>
    <td>Prodavatelj je izvršio svoju obvezu kad je isporučio robu izvozno ocarinjenu na obalu u imenovanoj luci
odredišta.</td>
    <td>
      <div class="half_div">+ O</div>
      <div class="half_div cit_grey">n/a</div>
    </td>
    <td>RH luka</td>
    </tr>
        
    <tr>
    <td>DDU</td>
    <td>Isporučeno neocarinjeno (imenovano mjesto odredišta)</td>
    <td>Prodavatelj je izvršio svoju obvezu kad je isporučio robu izvozno ocarinjenu, spremnu za istovar sa prijevoznog sredstva na imenovanom mjestu odredišta.</td>
    <td>
      <div class="half_div">+ O</div>
      <div class="half_div cit_grey">n/a</div>
    </td>
    <td>Odredište u RH i razdvojeni tuzemni troškovi P se mogu isključiti</td>
    </tr>
    

    <tr>
    <td>DDP</td>
    <td>Isporučeno ocarinjeno (imenovano mjesto odredišta)</td>
    <td>Prodavatelj je izvršio svoju obvezu kad je stavio robu na raspolaganje kupcu, izvozno i uvozno ocarinjenu na dolaznom prijevoznom sredstvu spremnu za istovar. Ovo pravilo predstavlja maksimalnu obvezu prodavatelja. Preporuka je da se ne ugovara DDP ukoliko prodavatelj ne može, izravno ili neizravno, obaviti uvozno carinjenje. Prodavatelj nema obvezu sklopiti ugovor o osiguranju. Sav PDV ili druge poreze snosi prodavatelj ukoliko nije izričito navedeno drugačije.</td>
    <td style="text-align: center;">+ O</td>
    <td>Odredište u RH i razdvojeni tuzemni troškovi P, carina i PDV se mogu isključiti</td>
    </tr>

    
        
            
    <tr>
    <td>DAT</td>
    <td>Isporučeno terminal (imenovano mjesto odredišta)</td>
    <td>Prodavatelj je izvršio svoju obvezu kad je isporučio robu izvozno ocarinjenu, istovarenu sa prijevoznog sredstva u imenovanom terminalu u imenovanoj luci ili mjestu odredišta (bilo koje mjesto, pokriveno ili ne, npr. luke, skladišta, željeznički ili zračni terminal i sl.).</td>
    <td>
      <div class="half_div cit_grey">n/a</div>
      <div class="half_div">+ O</div> 
    </td>
    <td>Ukoliko je mjesto odredišta terminal u RH</td>
    </tr>
    
    
    <tr>
    <td>DAP</td>
    <td>Isporučeno na mjestu (imenovano mjesto odredišta)</td>
    <td>Prodavatelj je izvršio svoju obvezu kad je isporučio robu izvozno ocarinjenu, spremnu za istovar sa prijevoznog sredstva u imenovanom mjestu odredišta.</td>
    <td>
      <div class="half_div cit_grey">n/a</div>
      <div class="half_div">+ O</div> 
    </td>
    <td>Ukoliko je mjesto odredišta u RH</td>
    </tr>    
    
   
    
    </tbody>
    </table>
    </div>     
       
    <br>
    <br>
    <div class="table_wrap">
      <h2>Kombinirana nomenklatura 2021</h2>
      <table class="info_table incoterms_table" style="width: 100%; min-width: 1300px;">
       <tbody>
          <tr class="incoterms_header" style="background: #496af9; color: #fff; font-weight: 700;">
            <td style="color: #fff;">KN OZNAKA<span style="mso-spacerun:yes">&nbsp;</span></td>
            <td style="color: #fff;">JEDINICA MJERE (Hrvatski)</td>
            <td style="color: #fff;">JEDINICA MJERE (Engleski)</td>
            <td style="color: #fff;">KN OPIS (Hrvatski)</td>
            <td style="color: #fff;">KN OPIS (Engleski)</td>
          </tr>
          <tr class="incoterms_header" style="background: #496af9; color: #fff; font-weight: 700;">
            <td style="color: #fff;">Commodity Code</td>
            <td style="color: #fff;">Supplementary Unit (Croatian)</td>
            <td style="color: #fff;">Supplementary Unit (English)</td>
            <td style="color: #fff;">Self Explanatory Text (Croatian)</td>
            <td style="color: #fff;">Self Explanatory Text (English)</td>
          </tr>
          <tr>
            <td>48</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>POGLAVLJE&nbsp;48 - PAPIR I KARTON; PROIZVODI OD PAPIRNE MASE, PAPIRA ILI KARTONA</td>
            <td>CHAPTER 48 - PAPER AND PAPERBOARD; ARTICLES OF PAPER PULP, OF PAPER OR OF PAPERBOARD</td>
          </tr>
          <tr>
            <td>4801 00 00</td>
            <td>-</td>
            <td>-</td>
            <td>Novinski papir, u rolama ili listovima</td>
            <td>Newsprint, in rolls or sheets</td>
          </tr>
          <tr>
            <td>4802</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Nepremazani papir i karton, vrsta koje se rabi za pisanje, tiskanje ili ostale grafičke namjene, neperforirani papir za bušene kartice i vrpce, u svitcima ili pravokutnim (uključujući kvadratnim) listovima, svih veličina, osim papira iz tarifnih brojeva&nbsp;4801 ili 4803; ručno rađeni papir i karton</td>
            <td>Uncoated paper and paperboard, of a kind used for writing, printing or other graphic purposes, and non-perforated punchcards and punch-tape paper, in rolls or rectangular (including square) sheets, of any size, other than paper of heading&nbsp;4801 or 4803; handmade paper and paperboard</td>
          </tr>
          <tr>
            <td>4802 10 00</td>
            <td>-</td>
            <td>-</td>
            <td>- ručno rađeni papir i karton</td>
            <td>- Handmade paper and paperboard</td>
          </tr>
          <tr>
            <td>4802 20 00</td>
            <td>-</td>
            <td>-</td>
            <td>- papir i karton vrsta koje se rabi kao podloge za fotoosjetljivi, termoosjetljivi ili elektroosjetljivi papir ili karton</td>
            <td>- Paper and paperboard of a kind used as a base for photosensitive, heat-sensitive or electrosensitive paper or paperboard</td>
          </tr>
          <tr>
            <td>4802 40</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- papirna podloga za tapete</td>
            <td>- Wallpaper base</td>
          </tr>
          <tr>
            <td>4802 40 10</td>
            <td>-</td>
            <td>-</td>
            <td>-- bez vlakana dobivenih mehaničkim postupkom ili s masenim udjelom tih vlakana ne većim od 10&nbsp;% u ukupnom sadržaju vlakana</td>
            <td>-- Not containing fibres obtained by a mechanical process or of which not more than 10&nbsp;% by weight of the total fibre content consists of such fibres</td>
          </tr>
          <tr>
            <td>4802 40 90</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostala</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- ostalo papir i karton, bez vlakna dobivenih mehaničkim ili kemijskomehaničkim postupkom ili s masenim udjelom tih vlakana ne većim od 10&nbsp;% u ukupnom sadržaju vlakana</td>
            <td>- Other paper and paperboard, not containing fibres obtained by a mechanical or chemi-mechanical process or of which not more than 10&nbsp;% by weight of the total fibre content consists of such fibres</td>
          </tr>
          <tr>
            <td>4802 54 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- mase manje od 40&nbsp;g/m²</td>
            <td>-- Weighing less than 40&nbsp;g/m²</td>
          </tr>
          <tr>
            <td>4802 55</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- mase 40&nbsp;g/m² ili veće, ali ne veće od 150&nbsp;g/m², u svitcima</td>
            <td>-- Weighing 40&nbsp;g/m² or more but not more than 150&nbsp;g/m², in rolls</td>
          </tr>
          <tr>
            <td>4802 55 15</td>
            <td>-</td>
            <td>-</td>
            <td>--- mase 40&nbsp;g/m² ili veće, ali manje od 60&nbsp;g/m²</td>
            <td>--- Weighing 40&nbsp;g/m² or more but less than 60&nbsp;g/m²</td>
          </tr>
          <tr>
            <td>4802 55 25</td>
            <td>-</td>
            <td>-</td>
            <td>--- mase 60&nbsp;g/m² ili veće, ali manje od 75&nbsp;g/m²</td>
            <td>--- Weighing 60&nbsp;g/m² or more but less than 75&nbsp;g/m²</td>
          </tr>
          <tr>
            <td>4802 55 30</td>
            <td>-</td>
            <td>-</td>
            <td>--- mase 75&nbsp;g/m² ili veće, ali manje od 80&nbsp;g/m²</td>
            <td>--- Weighing 75&nbsp;g/m² or more but less than 80&nbsp;g/m²</td>
          </tr>
          <tr>
            <td>4802 55 90</td>
            <td>-</td>
            <td>-</td>
            <td>--- mase 80&nbsp;g/m² ili veće</td>
            <td>--- Weighing 80&nbsp;g/m² or more</td>
          </tr>
          <tr>
            <td>4802 56</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- mase 40&nbsp;g/m² ili veće, ali ne veće od 150&nbsp;g/m², u listovima s jednom stranom ne većom od 435&nbsp;mm i drugom stranom ne većom od 297&nbsp;mm u nepresavijenom stanju</td>
            <td>-- Weighing 40&nbsp;g/m² or more but not more than 150&nbsp;g/m², in sheets with one side not exceeding 435&nbsp;mm and the other side not exceeding 297&nbsp;mm in the unfolded state</td>
          </tr>
          <tr>
            <td>4802 56 20</td>
            <td>-</td>
            <td>-</td>
            <td>--- s jednom stranom 297&nbsp;mm i drugom stranom 210&nbsp;mm (A4&nbsp;format)</td>
            <td>--- With one side measuring 297&nbsp;mm and the other side measuring 210&nbsp;mm (A4&nbsp;format)</td>
          </tr>
          <tr>
            <td>4802 56 80</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>4802 57 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostalo, mase 40&nbsp;g/m² ili veće, ali ne veće od 150&nbsp;g/m²</td>
            <td>-- Other, weighing 40&nbsp;g/m² or more but not more than 150&nbsp;g/m²</td>
          </tr>
          <tr>
            <td>4802 58</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- mase veće od 150&nbsp;g/m²</td>
            <td>-- Weighing more than 150&nbsp;g/m²</td>
          </tr>
          <tr>
            <td>4802 58 10</td>
            <td>-</td>
            <td>-</td>
            <td>--- u svitcima</td>
            <td>--- In rolls</td>
          </tr>
          <tr>
            <td>4802 58 90</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- ostalo papir i karton, s masenim udjelom vlakana dobivenih mehaničkim ili polukemijskim postupkom većim od 10&nbsp;% u ukupnom sadržaju vlakana</td>
            <td>- Other paper and paperboard, of which more than 10&nbsp;% by weight of the total fibre content consists of fibres obtained by a mechanical or chemi-mechanical process</td>
          </tr>
          <tr>
            <td>4802 61</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- u svitcima</td>
            <td>-- In rolls</td>
          </tr>
          <tr>
            <td>4802 61 15</td>
            <td>-</td>
            <td>-</td>
            <td>--- mase manje od 72&nbsp;g/m² i s masenim udjelom vlakana dobivenih mehaničkim postupkom većim od 50&nbsp;% u ukupnom sadržaju vlakana</td>
            <td>--- Weighing less than 72&nbsp;g/m² and of which more than 50&nbsp;% by weight of the total fibre content consists of fibres obtained by a mechanical process</td>
          </tr>
          <tr>
            <td>4802 61 80</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>4802 62 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- u listovima, s jednom stranom ne većom od 435&nbsp;mm i drugom stranom ne većom od 297&nbsp;mm u nepresavijenom stanju</td>
            <td>-- In sheets with one side not exceeding 435&nbsp;mm and the other side not exceeding 297&nbsp;mm in the unfolded state</td>
          </tr>
          <tr>
            <td>4802 69 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>4803 00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Papir za izradu toaletnog papira, papira za skidanje šminke, ručnika ili salveta te sličnog papira vrsta koje se rabi za kućanske ili za sanitarne svrhe, celulozna vata i koprene od celuloznih vlakana, neovisno jesu li nabrani, naborani, reljefirani, perforirani, površinski obojeni, površinski ukrašeni ili tiskani ili ne, u svitcima ili listovima</td>
            <td>Toilet or facial tissue stock, towel or napkin stock and similar paper of a kind used for household or sanitary purposes, cellulose wadding and webs of cellulose fibres, whether or not creped, crinkled, embossed, perforated, surface-coloured, surface-decorated or printed, in rolls or sheets</td>
          </tr>
          <tr>
            <td>4803 00 10</td>
            <td>-</td>
            <td>-</td>
            <td>- celulozna vata</td>
            <td>- Cellulose wadding</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- krep papir i koprene od celuloznih vlakana, mase pojedinačnog sloja</td>
            <td>- Creped paper and webs of cellulose fibres (tissues), weighing, per ply</td>
          </tr>
          <tr>
            <td>4803 00 31</td>
            <td>-</td>
            <td>-</td>
            <td>-- ne veće od 25&nbsp;g/m²</td>
            <td>-- Not more than 25&nbsp;g/m²</td>
          </tr>
          <tr>
            <td>4803 00 39</td>
            <td>-</td>
            <td>-</td>
            <td>-- veće od 25&nbsp;g/m²</td>
            <td>-- More than 25&nbsp;g/m²</td>
          </tr>
          <tr>
            <td>4803 00 90</td>
            <td>-</td>
            <td>-</td>
            <td>- ostalo</td>
            <td>- Other</td>
          </tr>
          <tr>
            <td>4804</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Nepremazani kraft papir i karton, u svitcima ili listovima, osim onog iz tarifnog broja&nbsp;4802 ili 4803</td>
            <td>Uncoated kraft paper and paperboard, in rolls or sheets, other than that of heading&nbsp;4802 or 4803</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- kraftlinear</td>
            <td>- Kraftliner</td>
          </tr>
          <tr>
            <td>4804 11</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- nebijeljeni</td>
            <td>-- Unbleached</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>--- s masenim udjelom vlakana od crnogoričnog drva, dobivenih kemijskim sulfatnim ili natronskim postupkom, ne manjim od 80&nbsp;% u ukupnom sadržaju vlakana</td>
            <td>--- Of which not less than 80&nbsp;% by weight of the total fibre content consists of coniferous fibres obtained by the chemical sulphate or soda process</td>
          </tr>
          <tr>
            <td>4804 11 11</td>
            <td>-</td>
            <td>-</td>
            <td>---- mase manje od 150&nbsp;g/m²</td>
            <td>---- Weighing less than 150&nbsp;g/m²</td>
          </tr>
          <tr>
            <td>4804 11 15</td>
            <td>-</td>
            <td>-</td>
            <td>---- mase 150&nbsp;g/m² ili veće, ali ne veće od 175&nbsp;g/m²</td>
            <td>---- Weighing 150&nbsp;g/m² or more but less than 175&nbsp;g/m²</td>
          </tr>
          <tr>
            <td>4804 11 19</td>
            <td>-</td>
            <td>-</td>
            <td>---- mase 175&nbsp;g/m² ili veće</td>
            <td>---- Weighing 175&nbsp;g/m² or more</td>
          </tr>
          <tr>
            <td>4804 11 90</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>4804 19</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>--- s masenim udjelom vlakana od crnogoričnog drva, dobivenih kemijskim sulfatnim ili natronskim postupkom, ne manjim od 80&nbsp;% u ukupnom sadržaju vlakana</td>
            <td>--- Of which not less than 80&nbsp;% by weight of the total fibre content consists of coniferous fibres obtained by the chemical sulphate or soda process</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>---- koji se sastoji od jednog ili više nebijeljenih listova i s jednim vanjskim listom bijeljenim, polubijeljenim ili obojenim, mase po m²</td>
            <td>---- Composed of one or more layers unbleached and an outside layer bleached, semi-bleached or coloured, weighing per m²</td>
          </tr>
          <tr>
            <td>4804 19 12</td>
            <td>-</td>
            <td>-</td>
            <td>----- manje od 175&nbsp;g</td>
            <td>----- Less than 175&nbsp;g</td>
          </tr>
          <tr>
            <td>4804 19 19</td>
            <td>-</td>
            <td>-</td>
            <td>----- 175&nbsp;g ili veće</td>
            <td>----- 175&nbsp;g or more</td>
          </tr>
          <tr>
            <td>4804 19 30</td>
            <td>-</td>
            <td>-</td>
            <td>---- ostalo</td>
            <td>---- Other</td>
          </tr>
          <tr>
            <td>4804 19 90</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- kraft papir za vreće</td>
            <td>- Sack kraft paper</td>
          </tr>
          <tr>
            <td>4804 21</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- nebijeljeni</td>
            <td>-- Unbleached</td>
          </tr>
          <tr>
            <td>4804 21 10</td>
            <td>-</td>
            <td>-</td>
            <td>--- s masenim udjelom vlakana od crnogoričnog drva, dobivenih kemijskim sulfatnim ili natronskim postupkom, ne manjim od 80&nbsp;% u ukupnom sadržaju vlakana</td>
            <td>--- Of which not less than 80&nbsp;% by weight of the total fibre content consists of coniferous fibres obtained by the chemical sulphate or soda process</td>
          </tr>
          <tr>
            <td>4804 21 90</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>4804 29</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>4804 29 10</td>
            <td>-</td>
            <td>-</td>
            <td>--- s masenim udjelom vlakana od crnogoričnog drva, dobivenih kemijskim sulfatnim ili natronskim postupkom, ne manjim od 80&nbsp;% u ukupnom sadržaju vlakana</td>
            <td>--- Of which not less than 80&nbsp;% by weight of the total fibre content consists of coniferous fibres obtained by the chemical sulphate or soda process</td>
          </tr>
          <tr>
            <td>4804 29 90</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- ostalo kraft papir i karton, mase 150&nbsp;g/m² ili manje</td>
            <td>- Other kraft paper and paperboard weighing 150&nbsp;g/m² or less</td>
          </tr>
          <tr>
            <td>4804 31</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- nebijeljeni</td>
            <td>-- Unbleached</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>--- s masenim udjelom vlakana od crnogoričnog drva, dobivenih kemijskim sulfatnim ili natronskim postupkom, ne manjim od 80&nbsp;% u ukupnom sadržaju vlakana</td>
            <td>--- Of which not less than 80&nbsp;% by weight of the total fibre content consists of coniferous fibres obtained by the chemical sulphate or soda process</td>
          </tr>
          <tr>
            <td>4804 31 51</td>
            <td>-</td>
            <td>-</td>
            <td>---- kraft papir za izolaciju u elektrotehnici</td>
            <td>---- Kraft electro-technical insulating paper</td>
          </tr>
          <tr>
            <td>4804 31 58</td>
            <td>-</td>
            <td>-</td>
            <td>---- ostalo</td>
            <td>---- Other</td>
          </tr>
          <tr>
            <td>4804 31 80</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>4804 39</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>--- s masenim udjelom vlakana od crnogoričnog drva, dobivenih kemijskim sulfatnim ili natronskim postupkom, ne manjim od 80&nbsp;% u ukupnom sadržaju vlakana</td>
            <td>--- Of which not less than 80&nbsp;% by weight of the total fibre content consists of coniferous fibres obtained by the chemical sulphate or soda process</td>
          </tr>
          <tr>
            <td>4804 39 51</td>
            <td>-</td>
            <td>-</td>
            <td>---- bijeljen u masi</td>
            <td>---- Bleached uniformly throughout the mass</td>
          </tr>
          <tr>
            <td>4804 39 58</td>
            <td>-</td>
            <td>-</td>
            <td>---- ostalo</td>
            <td>---- Other</td>
          </tr>
          <tr>
            <td>4804 39 80</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- ostalo kraft papir i karton, mase veće od 150&nbsp;g/m², ali manje od 225&nbsp;g/m²</td>
            <td>- Other kraft paper and paperboard weighing more than 150&nbsp;g/m² but less than 225&nbsp;g/m²</td>
          </tr>
          <tr>
            <td>4804 41</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- nebijeljeni</td>
            <td>-- Unbleached</td>
          </tr>
          <tr>
            <td>4804 41 91</td>
            <td>-</td>
            <td>-</td>
            <td>--- zasićeni (saturirani) kraft papir</td>
            <td>--- Saturating kraft</td>
          </tr>
          <tr>
            <td>4804 41 98</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>4804 42 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- jednolično bijeljeni u masi i s maseni, udjelom drvnih vlakana dobivenih kemijskim postupkom većim od 95&nbsp;% u ukupnom sadržaju vlakana</td>
            <td>-- Bleached uniformly throughout the mass and of which more than 95&nbsp;% by weight of the total fibre content consists of wood fibres obtained by a chemical process</td>
          </tr>
          <tr>
            <td>4804 49 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- ostalo kraft papir i karton, mase 225&nbsp;g/m² ili veće</td>
            <td>- Other kraft paper and paperboard weighing 225&nbsp;g/m² or more</td>
          </tr>
          <tr>
            <td>4804 51 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- nebijeljeni</td>
            <td>-- Unbleached</td>
          </tr>
          <tr>
            <td>4804 52 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- jednolično bijeljeni u masi i s maseni, udjelom drvnih vlakana dobivenih kemijskim postupkom većim od 95&nbsp;% u ukupnom sadržaju vlakana</td>
            <td>-- Bleached uniformly throughout the mass and of which more than 95&nbsp;% by weight of the total fibre content consists of wood fibres obtained by a chemical process</td>
          </tr>
          <tr>
            <td>4804 59</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>4804 59 10</td>
            <td>-</td>
            <td>-</td>
            <td>--- s masenim udjelom vlakana od crnogoričnog drva, dobivenih kemijskim sulfatnim ili natronskim postupkom, ne manjim od 80&nbsp;% u ukupnom sadržaju vlakana</td>
            <td>--- Of which not less than 80&nbsp;% by weight of the total fibre content consists of coniferous fibres obtained by the chemical sulphate or soda process</td>
          </tr>
          <tr>
            <td>4804 59 90</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>4805</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Ostali nepremazani papir i karton, u svitcima ili listovima, dalje neobrađen osim postupcima navedenima u napomeni&nbsp;3 uz ovo poglavlje</td>
            <td>Other uncoated paper and paperboard, in rolls or sheets, not further worked or processed than as specified in note&nbsp;3 to this chapter</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- fluting papir</td>
            <td>- Fluting paper</td>
          </tr>
          <tr>
            <td>4805 11 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- fluting papir od polukemijske celuloze (za valoviti sloj)</td>
            <td>-- Semi-chemical fluting paper</td>
          </tr>
          <tr>
            <td>4805 12 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- fluting papir od celuloze dobivene od slame</td>
            <td>-- Straw fluting paper</td>
          </tr>
          <tr>
            <td>4805 19</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>4805 19 10</td>
            <td>-</td>
            <td>-</td>
            <td>--- za valoviti karton</td>
            <td>--- Wellenstoff</td>
          </tr>
          <tr>
            <td>4805 19 90</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- testliner (reciklirani slojeviti karton)</td>
            <td>- Testliner (recycled liner board)</td>
          </tr>
          <tr>
            <td>4805 24 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- mase 150&nbsp;g/m² ili manje</td>
            <td>-- Weighing 150&nbsp;g/m² or less</td>
          </tr>
          <tr>
            <td>4805 25 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- mase veće od 150&nbsp;g/m²</td>
            <td>-- Weighing more than 150&nbsp;g/m²</td>
          </tr>
          <tr>
            <td>4805 30 00</td>
            <td>-</td>
            <td>-</td>
            <td>- sulfitni omotni papir</td>
            <td>- Sulphite wrapping paper</td>
          </tr>
          <tr>
            <td>4805 40 00</td>
            <td>-</td>
            <td>-</td>
            <td>- filtar papir i karton</td>
            <td>- Filter paper and paperboard</td>
          </tr>
          <tr>
            <td>4805 50 00</td>
            <td>-</td>
            <td>-</td>
            <td>- pust papir i pust karton</td>
            <td>- Felt paper and paperboard</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- ostalo</td>
            <td>- Other</td>
          </tr>
          <tr>
            <td>4805 91 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- mase 150&nbsp;g/m² ili manje</td>
            <td>-- Weighing 150&nbsp;g/m² or less</td>
          </tr>
          <tr>
            <td>4805 92 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- mase veće od 150&nbsp;g/m², ali manje od 225&nbsp;g/m²</td>
            <td>-- Weighing more than 150&nbsp;g/m² but less than 225&nbsp;g/m²</td>
          </tr>
          <tr>
            <td>4805 93</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- mase 225&nbsp;g/m² ili veće</td>
            <td>-- Weighing 225&nbsp;g/m² or more</td>
          </tr>
          <tr>
            <td>4805 93 20</td>
            <td>-</td>
            <td>-</td>
            <td>--- izrađen iz otpadnog papira</td>
            <td>--- Made from recovered paper</td>
          </tr>
          <tr>
            <td>4805 93 80</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>4806</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Biljni pergament papir, papir otporan na mast, paus papir, kristal papir i ostali satinirani prozirni ili providni papiri, u svitcima ili listovima</td>
            <td>Vegetable parchment, greaseproof papers, tracing papers and glassine and other glazed transparent or translucent papers, in rolls or sheets</td>
          </tr>
          <tr>
            <td>4806 10 00</td>
            <td>-</td>
            <td>-</td>
            <td>- biljni pergament papir</td>
            <td>- Vegetable parchment</td>
          </tr>
          <tr>
            <td>4806 20 00</td>
            <td>-</td>
            <td>-</td>
            <td>- papir otporan na mast</td>
            <td>- Greaseproof papers</td>
          </tr>
          <tr>
            <td>4806 30 00</td>
            <td>-</td>
            <td>-</td>
            <td>- paus papir</td>
            <td>- Tracing papers</td>
          </tr>
          <tr>
            <td>4806 40</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- kristal papir i ostali satinirani prozirni ili providni papiri</td>
            <td>- Glassine and other glazed transparent or translucent papers</td>
          </tr>
          <tr>
            <td>4806 40 10</td>
            <td>-</td>
            <td>-</td>
            <td>-- kristal papir</td>
            <td>-- Glassine papers</td>
          </tr>
          <tr>
            <td>4806 40 90</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>4807 00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Složeni papir i karton (izrađen međusobnim lijepljenjem ravnih slojeva papira ili kartona), površinski nepremazan niti impregniran, neovisno je li ojačan u unutrašnjosti ili ne, u svitcima ili listovima</td>
            <td>Composite paper and paperboard (made by sticking flat layers of paper or paperboard together with an adhesive), not surface-coated or impregnated, whether or not internally reinforced, in rolls or sheets</td>
          </tr>
          <tr>
            <td>4807 00 30</td>
            <td>-</td>
            <td>-</td>
            <td>- izrađen od otpadnog papira, neovisno je li prekriven papirom ili ne</td>
            <td>- Made from recovered paper, whether or not covered with paper</td>
          </tr>
          <tr>
            <td>4807 00 80</td>
            <td>-</td>
            <td>-</td>
            <td>- ostalo</td>
            <td>- Other</td>
          </tr>
          <tr>
            <td>4808</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Papir i karton, valoviti (sa ili bez lijepljenih ravnih površinskih listova), nabrani, naborani, reljefirani ili perforirani, u svitcima ili listovima, osim papira vrsta navedenih u tarifnom broju 4803</td>
            <td>Paper and paperboard, corrugated (with or without glued flat surface sheets), creped, crinkled, embossed or perforated, in rolls or sheets, other than paper of the kind described in heading&nbsp;4803</td>
          </tr>
          <tr>
            <td>4808 10 00</td>
            <td>-</td>
            <td>-</td>
            <td>- valoviti papir i karton, neovisno je li perforirani ili ne</td>
            <td>- Corrugated paper and paperboard, whether or not perforated</td>
          </tr>
          <tr>
            <td>4808 40 00</td>
            <td>-</td>
            <td>-</td>
            <td>- kraft papir, nabrani ili naboran (krep ili plisiran), neovisno je li reljefiran ili perforiran ili ne</td>
            <td>- Kraft paper, creped or crinkled, whether or not embossed or perforated</td>
          </tr>
          <tr>
            <td>4808 90 00</td>
            <td>-</td>
            <td>-</td>
            <td>- ostalo</td>
            <td>- Other</td>
          </tr>
          <tr>
            <td>4809</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Karbon papir, samokopirni papir i ostali papir za kopiranje ili prenošenje (uključujući premazani ili impregnirani papir za matrice za umnožavanje ili ofset-ploče), neovisno je li tiskan ili ne, u listovima ili svitcima</td>
            <td>Carbon paper, self-copy paper and other copying or transfer papers (including coated or impregnated paper for duplicator stencils or offset plates), whether or not printed, in rolls or sheets</td>
          </tr>
          <tr>
            <td>4809 20 00</td>
            <td>-</td>
            <td>-</td>
            <td>- samokopirni papir</td>
            <td>- Self-copy paper</td>
          </tr>
          <tr>
            <td>4809 90 00</td>
            <td>-</td>
            <td>-</td>
            <td>- ostalo</td>
            <td>- Other</td>
          </tr>
          <tr>
            <td>4810</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Papir i karton, premazan s jedne ili s obje strane kaolinom (china clay) ili drugim anorganskim tvarima, sa ili bez veziva, ali bez drugog premaza, neovisno je li površinski obojen, površinski ukrašen ili tiskan ili ne, u svitcima ili pravokutnim (uključujući kvadratnim) listovima, svih veličina</td>
            <td>Paper and paperboard, coated on one or both sides with kaolin (China clay) or other inorganic substances, with or without a binder, and with no other coating, whether or not surface-coloured, surface-decorated or printed, in rolls or rectangular (including square) sheets, of any size</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- papir i karton vrsta koje se rabi za pisanje, tiskanje ili ostale grafičke namjene, bez vlakna dobivena mehaničkim ili polukemijskim postupkom ili s masenim udjelom tih vlakana ne većim od 10&nbsp;% u ukupnom sadržaju vlakana</td>
            <td>- Paper and paperboard of a kind used for writing, printing or other graphic purposes, not containing fibres obtained by a mechanical or chemi-mechanical process or of which not more than 10&nbsp;% by weight of the total fibre content consists of such fibres</td>
          </tr>
          <tr>
            <td>4810 13 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- u svitcima</td>
            <td>-- In rolls</td>
          </tr>
          <tr>
            <td>4810 14 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- u listovima, s jednom stranom ne većom od 435&nbsp;mm i drugom stranom ne većom od 297&nbsp;mm u nepresavijenom stanju</td>
            <td>-- In sheets with one side not exceeding 435&nbsp;mm and the other side not exceeding 297&nbsp;mm in the unfolded state</td>
          </tr>
          <tr>
            <td>4810 19 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- papir i karton vrsta koje se rabi za pisanje, tiskanje ili ostale grafičke namjene, s masenim udjelom vlakana dobivenih mehaničkim ili polukemijskim postupkom većim od 10&nbsp;% u ukupnom sadržaju vlakana</td>
            <td>- Paper and paperboard of a kind used for writing, printing or other graphic purposes, of which more than 10&nbsp;% by weight of the total fibre content consists of fibres obtained by a mechanical or chemi-mechanical process</td>
          </tr>
          <tr>
            <td>4810 22 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- prevučeni papir male mase</td>
            <td>-- Lightweight coated paper</td>
          </tr>
          <tr>
            <td>4810 29</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>4810 29 30</td>
            <td>-</td>
            <td>-</td>
            <td>--- u svitcima</td>
            <td>--- In rolls</td>
          </tr>
          <tr>
            <td>4810 29 80</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- kraft papir i karton, osim vrsta koje se rabi za pisanje, tiskanje ili za ostale grafičke namjene</td>
            <td>- Kraft paper and paperboard, other than that of a kind used for writing, printing or other graphic purposes</td>
          </tr>
          <tr>
            <td>4810 31 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- jednolično bijeljen u masi i s masenim udjelom drvnih vlakana dobivenih kemijskim postupkom većim od 95&nbsp;% u ukupnom sadržaju vlakana, mase 150&nbsp;g/m² ili manje</td>
            <td>-- Bleached uniformly throughout the mass and of which more than 95&nbsp;% by weight of the total fibre content consists of wood fibres obtained by a chemical process, and weighing 150&nbsp;g/m² or less</td>
          </tr>
          <tr>
            <td>4810 32</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- jednolično bijeljen u masi i s masenim udjelom drvnih vlakana dobivenih kemijskim postupkom većim od 95&nbsp;% u ukupnom sadržaju vlakana, mase veće od 150&nbsp;g/m²</td>
            <td>-- Bleached uniformly throughout the mass and of which more than 95&nbsp;% by weight of the total fibre content consists of wood fibres obtained by a chemical process, and weighing more than 150&nbsp;g/m²</td>
          </tr>
          <tr>
            <td>4810 32 10</td>
            <td>-</td>
            <td>-</td>
            <td>--- prevučen kaolinom</td>
            <td>--- Coated with kaolin</td>
          </tr>
          <tr>
            <td>4810 32 90</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>4810 39 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- ostalo papir i karton</td>
            <td>- Other paper and paperboard</td>
          </tr>
          <tr>
            <td>4810 92</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- višeslojni</td>
            <td>-- Multi-ply</td>
          </tr>
          <tr>
            <td>4810 92 10</td>
            <td>-</td>
            <td>-</td>
            <td>--- sa svim bijeljenim listovima</td>
            <td>--- Each layer bleached</td>
          </tr>
          <tr>
            <td>4810 92 30</td>
            <td>-</td>
            <td>-</td>
            <td>--- samo s jednim bijeljenim vanjskim slojem</td>
            <td>--- With only one outer layer bleached</td>
          </tr>
          <tr>
            <td>4810 92 90</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>4810 99</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>4810 99 10</td>
            <td>-</td>
            <td>-</td>
            <td>--- bijeljeni papir i karton, prevučen kaolinom</td>
            <td>--- Bleached paper and paperboard, coated with kaolin</td>
          </tr>
          <tr>
            <td>4810 99 80</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>4811</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Papir, karton, celulozna vata i koprene od celuloznih vlakana, premazani, impregnirani, prevučeni, površinski obojeni, površinski ukrašeni ili tiskani, u svitcima ili pravokutnim (uključujući kvadratnim) listovima, svih veličina, osim onih navedenih u tarifnom broju 4803, 4809 ili 4810</td>
            <td>Paper, paperboard, cellulose wadding and webs of cellulose fibres, coated, impregnated, covered, surface-coloured, surface-decorated or printed, in rolls or rectangular (including square) sheets, of any size, other than goods of the kind described in heading&nbsp;4803, 4809 or 4810</td>
          </tr>
          <tr>
            <td>4811 10 00</td>
            <td>-</td>
            <td>-</td>
            <td>- katranirani, bitumenizirani ili asfaltirani papir i karton</td>
            <td>- Tarred, bituminised or asphalted paper and paperboard</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- gumirani ili ljepljivi papir i karton</td>
            <td>- Gummed or adhesive paper and paperboard</td>
          </tr>
          <tr>
            <td>4811 41</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- samoljepivi</td>
            <td>-- Self-adhesive</td>
          </tr>
          <tr>
            <td>4811 41 20</td>
            <td>-</td>
            <td>-</td>
            <td>--- širine ne veće od 10&nbsp;cm, premazani nevulkaniziranim prirodnim ili sintetičkim kaučukom</td>
            <td>--- Of a width not exceeding 10&nbsp;cm, the coating of which consists of unvulcanised natural or synthetic rubber</td>
          </tr>
          <tr>
            <td>4811 41 90</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>4811 49 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- papir i karton, premazani, impregnirani ili prevučeni plastičnim masama (osim ljepila)</td>
            <td>- Paper and paperboard, coated, impregnated or covered with plastics (excluding adhesives)</td>
          </tr>
          <tr>
            <td>4811 51 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- bijeljeni, mase veće od 150&nbsp;g/m²</td>
            <td>-- Bleached, weighing more than 150&nbsp;g/m²</td>
          </tr>
          <tr>
            <td>4811 59 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>4811 60 00</td>
            <td>-</td>
            <td>-</td>
            <td>- papir i karton, premazani, impregnirani ili prevučeni voskom, parafinskim voskom, stearinom, uljem ili glicerolom</td>
            <td>- Paper and paperboard, coated, impregnated or covered with wax, paraffin wax, stearin, oil or glycerol</td>
          </tr>
          <tr>
            <td>4811 90 00</td>
            <td>-</td>
            <td>-</td>
            <td>- ostalo papir, karton, celulozna vata i koprene od celuloznih vlakana</td>
            <td>- Other paper, paperboard, cellulose wadding and webs of cellulose fibres</td>
          </tr>
          <tr>
            <td>4812 00 00</td>
            <td>-</td>
            <td>-</td>
            <td>Filter blokovi i ploče, od papirne mase</td>
            <td>Filter blocks, slabs and plates, of paper pulp</td>
          </tr>
          <tr>
            <td>4813</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Cigaretni papir, neovisno je li izrezan na veličine ili je u obliku knjižica ili cjevčica ili ne</td>
            <td>Cigarette paper, whether or not cut to size or in the form of booklets or tubes</td>
          </tr>
          <tr>
            <td>4813 10 00</td>
            <td>-</td>
            <td>-</td>
            <td>- u obliku knjižica ili cjevčica</td>
            <td>- In the form of booklets or tubes</td>
          </tr>
          <tr>
            <td>4813 20 00</td>
            <td>-</td>
            <td>-</td>
            <td>- u svitcima širine ne veće od 5&nbsp;cm</td>
            <td>- In rolls of a width not exceeding 5&nbsp;cm</td>
          </tr>
          <tr>
            <td>4813 90</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- ostalo</td>
            <td>- Other</td>
          </tr>
          <tr>
            <td>4813 90 10</td>
            <td>-</td>
            <td>-</td>
            <td>-- u svitcima širine veće od 5&nbsp;cm, ali ne veće od 15&nbsp;cm</td>
            <td>-- In rolls of a width exceeding 5&nbsp;cm but not exceeding 15&nbsp;cm</td>
          </tr>
          <tr>
            <td>4813 90 90</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>4814</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Zidne tapete i slične zidne obloge; prozorske vitrofanije od papira</td>
            <td>Wallpaper and similar wallcoverings; window transparencies of paper</td>
          </tr>
          <tr>
            <td>4814 20 00</td>
            <td>-</td>
            <td>-</td>
            <td>- zidne tapete i slične zidne obloge, od papira čije je lice premazano ili prevučeno slojem plastične mase zrnaste, reljefirane, bojene, tiskane s uzorkom ili drukčije ukrašene površine</td>
            <td>- Wallpaper and similar wallcoverings, consisting of paper coated or covered, on the face side, with a grained, embossed, coloured, design-printed or otherwise decorated layer of plastics</td>
          </tr>
          <tr>
            <td>4814 90</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- ostale</td>
            <td>- Other</td>
          </tr>
          <tr>
            <td>4814 90 10</td>
            <td>-</td>
            <td>-</td>
            <td>-- zidne tapete i slične zidne obloge, od papira zrnaste, reljefirane, bojene, tiskane s uzorcima ili drukčije ukrašene površine, prevučenog ili prekrivenog prozirnom zaštitnom plastičnom masom</td>
            <td>-- Wallpaper and similar wallcoverings, consisting of grained, embossed, surface-coloured, design-printed or otherwise surface-decorated paper, coated or covered with transparent protective plastics</td>
          </tr>
          <tr>
            <td>4814 90 70</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostale</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>4816</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Karbonski papir, samokopirni papir i ostali papir za kopiranje i prenošenje (osim onih iz tarifnog broja&nbsp;4809), matrice za umnožavanje i ofset ploče, od papira, neovisno jesu li u kutijama ili ne</td>
            <td>Carbon paper, self-copy paper and other copying or transfer papers (other than those of heading&nbsp;4809), duplicator stencils and offset plates, of paper, whether or not put up in boxes</td>
          </tr>
          <tr>
            <td>4816 20 00</td>
            <td>-</td>
            <td>-</td>
            <td>- samokopirni papir</td>
            <td>- Self-copy paper</td>
          </tr>
          <tr>
            <td>4816 90 00</td>
            <td>-</td>
            <td>-</td>
            <td>- ostalo</td>
            <td>- Other</td>
          </tr>
          <tr>
            <td>4817</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Poštanske omotnice, pismovne dopisnice, dopisnice i kartice za dopisivanje, od papira ili kartona; kutije, vrećice, notesi i omoti, od papira ili kartona, sa zbirkama pribora za dopisivanje</td>
            <td>Envelopes, letter cards, plain postcards and correspondence cards, of paper or paperboard; boxes, pouches, wallets and writing compendiums, of paper or paperboard, containing an assortment of paper stationery</td>
          </tr>
          <tr>
            <td>4817 10 00</td>
            <td>-</td>
            <td>-</td>
            <td>- poštanske omotnice</td>
            <td>- Envelopes</td>
          </tr>
          <tr>
            <td>4817 20 00</td>
            <td>-</td>
            <td>-</td>
            <td>- pismovne dopisnice, dopisnice i kartice za dopisivanje</td>
            <td>- Letter cards, plain postcards and correspondence cards</td>
          </tr>
          <tr>
            <td>4817 30 00</td>
            <td>-</td>
            <td>-</td>
            <td>- kutije, vrećice, notesi i omoti, od papira ili kartona, sa zbirkama pribora za dopisivanje</td>
            <td>- Boxes, pouches, wallets and writing compendiums, of paper or paperboard, containing an assortment of paper stationery</td>
          </tr>
          <tr>
            <td>4818</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Toaletni i slični papir, celulozna vata ili od koprene od celuloznih vlakana, vrsta koje se rabi za kućanske ili za sanitarne svrhe, u svitcima širine ne veće od 36&nbsp;cm ili izrezani na određene veličine ili oblike; džepni rupčići, listići za uklanjanja šminke, ručnici, stolnjaci, salvete, plahte i slični proizvodi za kućansku, sanitarnu ili bolničku uporabu, odjeća i pribor za odjeću, od papirne mase, papira, celulozne vate ili od koprene od celuloznih vlakana</td>
            <td>Toilet paper and similar paper, cellulose wadding or webs of cellulose fibres, of a kind used for household or sanitary purposes, in rolls of a width not exceeding 36&nbsp;cm, or cut to size or shape; handkerchiefs, cleansing tissues, towels, tablecloths, serviettes, bedsheets and similar household, sanitary or hospital articles, articles of apparel and clothing accessories, of paper pulp, paper, cellulose wadding or webs of cellulose fibres</td>
          </tr>
          <tr>
            <td>4818 10</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- toaletni papir</td>
            <td>- Toilet paper</td>
          </tr>
          <tr>
            <td>4818 10 10</td>
            <td>-</td>
            <td>-</td>
            <td>-- mase pojedinačnog sloja 25&nbsp;g/m² ili manje</td>
            <td>-- Weighing, per ply, 25&nbsp;g/m² or less</td>
          </tr>
          <tr>
            <td>4818 10 90</td>
            <td>-</td>
            <td>-</td>
            <td>-- mase pojedinačnog sloja veće od 25&nbsp;g/m²</td>
            <td>-- Weighing, per ply, more than 25&nbsp;g/m²</td>
          </tr>
          <tr>
            <td>4818 20</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- džepni rupčići, listići za uklanjanja šminke i ručnici</td>
            <td>- Handkerchiefs, cleansing or facial tissues and towels</td>
          </tr>
          <tr>
            <td>4818 20 10</td>
            <td>-</td>
            <td>-</td>
            <td>-- džepni rupčići i listići za uklanjanja šminke</td>
            <td>-- Handkerchiefs and cleansing or facial tissues</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- ručnici</td>
            <td>-- Hand towels</td>
          </tr>
          <tr>
            <td>4818 20 91</td>
            <td>-</td>
            <td>-</td>
            <td>--- u svitcima</td>
            <td>--- In rolls</td>
          </tr>
          <tr>
            <td>4818 20 99</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>4818 30 00</td>
            <td>-</td>
            <td>-</td>
            <td>- stolnjaci i salvete</td>
            <td>- Tablecloths and serviettes</td>
          </tr>
          <tr>
            <td>4818 50 00</td>
            <td>-</td>
            <td>-</td>
            <td>- odjeća i pribor za odjeću</td>
            <td>- Articles of apparel and clothing accessories</td>
          </tr>
          <tr>
            <td>4818 90</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- ostalo</td>
            <td>- Other</td>
          </tr>
          <tr>
            <td>4818 90 10</td>
            <td>-</td>
            <td>-</td>
            <td>-- proizvodi vrsta koje se rabi za kirurške, medicinske ili higijenske svrhe, nepripremljeni u pakiranja za pojedinačnu prodaju</td>
            <td>-- Articles of a kind used for surgical, medical or hygienic purposes, not put up for retail sale</td>
          </tr>
          <tr>
            <td>4818 90 90</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>4819</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Kutije, kutijice, vreće i drugi spremnici za pakiranje, od papira, kartona, celulozne vate ili koprene od celuloznih vlakana; kutije za spise, ladice za spise i slični proizvodi, od papira ili kartona, vrsta koje se rabi u uredima, prodavaonicama ili slično</td>
            <td>Cartons, boxes, cases, bags and other packing containers, of paper, paperboard, cellulose wadding or webs of cellulose fibres; box files, letter trays, and similar articles, of paper or paperboard, of a kind used in offices, shops or the like</td>
          </tr>
          <tr>
            <td>4819 10 00</td>
            <td>-</td>
            <td>-</td>
            <td>- kutije i kutijice, od valovitog papira ili kartona</td>
            <td>- Cartons, boxes and cases, of corrugated paper or paperboard</td>
          </tr>
          <tr>
            <td>4819 20 00</td>
            <td>-</td>
            <td>-</td>
            <td>- složive kutije i kutijice, od nevalovitog papira ili kartona</td>
            <td>- Folding cartons, boxes and cases, of non-corrugated paper or paperboard</td>
          </tr>
          <tr>
            <td>4819 30 00</td>
            <td>-</td>
            <td>-</td>
            <td>- vreće i vrećice, širine osnove 40&nbsp;cm ili veće</td>
            <td>- Sacks and bags, having a base of a width of 40&nbsp;cm or more</td>
          </tr>
          <tr>
            <td>4819 40 00</td>
            <td>-</td>
            <td>-</td>
            <td>- ostale vreće i vrećice, uključujući stožaste</td>
            <td>- Other sacks and bags, including cones</td>
          </tr>
          <tr>
            <td>4819 50 00</td>
            <td>-</td>
            <td>-</td>
            <td>- ostalo spremnici za pakiranje, uključujući omote za gramofonske ploče</td>
            <td>- Other packing containers, including record sleeves</td>
          </tr>
          <tr>
            <td>4819 60 00</td>
            <td>-</td>
            <td>-</td>
            <td>- kutije za spise, ladice za spise, kutije za spremanje i slični proizvodi, vrsta koje se rabi u uredima, prodavaonicama i slično</td>
            <td>- Box files, letter trays, storage boxes and similar articles, of a kind used in offices, shops or the like</td>
          </tr>
          <tr>
            <td>4820</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Registri, knjigovodstvene knjige, podsjetnici, knjige narudžbi, priznanične knjige, blokovi za pisanje, memorandum blokovi, dnevnici i slični proizvodi, bilježnice, blokovi s upijajućim papirom, korice za uvezivanje (za slobodne listove ili druge), mape, košuljice za spise, poslovni obrasci u više primjeraka, setovi s umetnutim karbon papirom i drugi proizvodi za pisanje, od papira ili kartona; albumi za uzorke ili zbirke i omoti za knjige, od papira ili kartona</td>
            <td>Registers, account books, notebooks, order books, receipt books, letter pads, memorandum pads, diaries and similar articles, exercise books, blotting pads, binders (loose-leaf or other), folders, file covers, manifold business forms, interleaved carbon sets and other articles of stationery, of paper or paperboard; albums for samples or for collections and book covers, of paper or paperboard</td>
          </tr>
          <tr>
            <td>4820 10</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- registri, knjigovodstvene knjige, podsjetnici, knjige narudžbi, priznanične knjige, blokovi za pisanje, memorandum blokovi, dnevnici i slični proizvodi</td>
            <td>- Registers, account books, notebooks, order books, receipt books, letter pads, memorandum pads, diaries and similar articles</td>
          </tr>
          <tr>
            <td>4820 10 10</td>
            <td>-</td>
            <td>-</td>
            <td>-- registri, knjigovodstvene knjige, knjige narudžbi i priznanične knjige</td>
            <td>-- Registers, account books, order books and receipt books</td>
          </tr>
          <tr>
            <td>4820 10 30</td>
            <td>-</td>
            <td>-</td>
            <td>-- podsjetnici, blokovi za pisanje i memorandum blokovi</td>
            <td>-- Notebooks, letter pads and memorandum pads</td>
          </tr>
          <tr>
            <td>4820 10 50</td>
            <td>-</td>
            <td>-</td>
            <td>-- dnevnici</td>
            <td>-- Diaries</td>
          </tr>
          <tr>
            <td>4820 10 90</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>4820 20 00</td>
            <td>-</td>
            <td>-</td>
            <td>- bilježnice</td>
            <td>- Exercise books</td>
          </tr>
          <tr>
            <td>4820 30 00</td>
            <td>-</td>
            <td>-</td>
            <td>- korice za uvezivanje (osim omota za knjige), mape i košuljice za spise</td>
            <td>- Binders (other than book covers), folders and file covers</td>
          </tr>
          <tr>
            <td>4820 40 00</td>
            <td>-</td>
            <td>-</td>
            <td>- poslovni obrasci u više primjeraka i setovi s umetnutim karbon papirom</td>
            <td>- Manifold business forms and interleaved carbon sets</td>
          </tr>
          <tr>
            <td>4820 50 00</td>
            <td>-</td>
            <td>-</td>
            <td>- albumi za uzorke ili zbirke</td>
            <td>- Albums for samples or for collections</td>
          </tr>
          <tr>
            <td>4820 90 00</td>
            <td>-</td>
            <td>-</td>
            <td>- ostalo</td>
            <td>- Other</td>
          </tr>
          <tr>
            <td>4821</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Papirnate ili kartonske etikete svih vrsta, neovisno jesu li tiskane ili ne</td>
            <td>Paper or paperboard labels of all kinds, whether or not printed</td>
          </tr>
          <tr>
            <td>4821 10</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- tiskane</td>
            <td>- Printed</td>
          </tr>
          <tr>
            <td>4821 10 10</td>
            <td>-</td>
            <td>-</td>
            <td>-- samoljepive</td>
            <td>-- Self-adhesive</td>
          </tr>
          <tr>
            <td>4821 10 90</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostale</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>4821 90</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- ostale</td>
            <td>- Other</td>
          </tr>
          <tr>
            <td>4821 90 10</td>
            <td>-</td>
            <td>-</td>
            <td>-- samoljepive</td>
            <td>-- Self-adhesive</td>
          </tr>
          <tr>
            <td>4821 90 90</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostale</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>4822</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Vretena, koluti, cijevke i slične podloge, od papirne mase, papira ili kartona (neovisno jesu li perforirani ili pojačani ili ne)</td>
            <td>Bobbins, spools, cops and similar supports, of paper pulp, paper or paperboard (whether or not perforated or hardened)</td>
          </tr>
          <tr>
            <td>4822 10 00</td>
            <td>-</td>
            <td>-</td>
            <td>- vrsta koje se rabi za namatanje tekstilne pređe</td>
            <td>- Of a kind used for winding textile yarn</td>
          </tr>
          <tr>
            <td>4822 90 00</td>
            <td>-</td>
            <td>-</td>
            <td>- ostalo</td>
            <td>- Other</td>
          </tr>
          <tr>
            <td>4823</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Ostali papir, karton, celulozna vata i koprene od celuloznih vlakana, izrezani u određene veličine ili oblike; ostali proizvodi od papirne mase, papira, kartona, celulozne vate ili koprene od celuloznih vlakana</td>
            <td>Other paper, paperboard, cellulose wadding and webs of cellulose fibres, cut to size or shape; other articles of paper pulp, paper, paperboard, cellulose wadding or webs of cellulose fibres</td>
          </tr>
          <tr>
            <td>4823 20 00</td>
            <td>-</td>
            <td>-</td>
            <td>- filtar papir i karton</td>
            <td>- Filter paper and paperboard</td>
          </tr>
          <tr>
            <td>4823 40 00</td>
            <td>-</td>
            <td>-</td>
            <td>- svitci, listovi i brojčanici, tiskani za aparate za registraciju</td>
            <td>- Rolls, sheets and dials, printed for self-recording apparatus</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- poslužavnici, zdjele, tanjuri, šalice i slično, od papira ili kartona</td>
            <td>- Trays, dishes, plates, cups and the like, of paper or paperboard</td>
          </tr>
          <tr>
            <td>4823 61 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- od bambusa</td>
            <td>-- Of bamboo</td>
          </tr>
          <tr>
            <td>4823 69</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>4823 69 10</td>
            <td>-</td>
            <td>-</td>
            <td>--- poslužavnici, zdjele i tanjuri</td>
            <td>--- Trays, dishes and plates</td>
          </tr>
          <tr>
            <td>4823 69 90</td>
            <td>-</td>
            <td>-</td>
            <td>--- ostalo</td>
            <td>--- Other</td>
          </tr>
          <tr>
            <td>4823 70</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- lijevani ili prešani proizvodi, od papirne mase</td>
            <td>- Moulded or pressed articles of paper pulp</td>
          </tr>
          <tr>
            <td>4823 70 10</td>
            <td>-</td>
            <td>-</td>
            <td>-- lijevani podlošci i kutije za pakiranje jaja</td>
            <td>-- Moulded trays and boxes for packing eggs</td>
          </tr>
          <tr>
            <td>4823 70 90</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>4823 90</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- ostalo</td>
            <td>- Other</td>
          </tr>
          <tr>
            <td>4823 90 40</td>
            <td>-</td>
            <td>-</td>
            <td>-- papir i karton, vrsta koje se rabi za pisanje, tiskanje ili ostale grafičke namjene</td>
            <td>-- Paper and paperboard, of a kind used for writing, printing or other graphic purposes</td>
          </tr>
          <tr>
            <td>4823 90 85</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr class="incoterms_header" style="background: #496af9; font-weight: 700;">
            <td style="color: #fff;">49</td>
            <td style="color: #fff;">&nbsp;</td>
            <td style="color: #fff;">&nbsp;</td>
            <td style="color: #fff;">POGLAVLJE&nbsp;49 - TISKANE KNJIGE, NOVINE, SLIKE I OSTALI PROIZVODI GRAFIČKE INDUSTRIJE; RUKOPISI, TIPKANI TEKSTOVI I NACRTI</td>
            <td style="color: #fff;">CHAPTER&nbsp;49 - PRINTED BOOKS, NEWSPAPERS, PICTURES AND OTHER PRODUCTS OF THE PRINTING INDUSTRY; MANUSCRIPTS, TYPESCRIPTS AND PLANS</td>
          </tr>
          <tr>
            <td>4901</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Tiskane knjige, brošure, leci i slični tiskani materijal, neovisno jesu li u slobodnim listovima ili ne</td>
            <td>Printed books, brochures, leaflets and similar printed matter, whether or not in single sheets</td>
          </tr>
          <tr>
            <td>4901 10 00</td>
            <td>-</td>
            <td>-</td>
            <td>- u pojedinačnim listovima, neovisno jesu li presavijeni ili ne</td>
            <td>- In single sheets, whether or not folded</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- ostalo</td>
            <td>- Other</td>
          </tr>
          <tr>
            <td>4901 91 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- rječnici i enciklopedije i njihovi dijelovi koji izlaze u nastavcima</td>
            <td>-- Dictionaries and encyclopaedias, and serial instalments thereof</td>
          </tr>
          <tr>
            <td>4901 99 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>4902</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Novine, časopisi i ostale periodične publikacije, neovisno jesu li ilustrirani ili sadrže reklamni materijal ili ne</td>
            <td>Newspapers, journals and periodicals, whether or not illustrated or containing advertising material</td>
          </tr>
          <tr>
            <td>4902 10 00</td>
            <td>-</td>
            <td>-</td>
            <td>- koji izlaze najmanje četiri puta tjedno</td>
            <td>- Appearing at least four times a week</td>
          </tr>
          <tr>
            <td>4902 90 00</td>
            <td>-</td>
            <td>-</td>
            <td>- ostalo</td>
            <td>- Other</td>
          </tr>
          <tr>
            <td>4903 00 00</td>
            <td>-</td>
            <td>-</td>
            <td>Dječje slikovnice, knjige za crtanje ili bojanje</td>
            <td>Children's picture, drawing or colouring books</td>
          </tr>
          <tr>
            <td>4904 00 00</td>
            <td>-</td>
            <td>-</td>
            <td>Glazbene note, tiskane ili u rukopisu, neovisno jesu li uvezene ili ilustrirane ili ne</td>
            <td>Music, printed or in manuscript, whether or not bound or illustrated</td>
          </tr>
          <tr>
            <td>4905</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Zemljopisne karte i hidrografske ili slične karte svih vrsta, uključujući atlase, zidne karte, topografske karte i globuse, tiskane</td>
            <td>Maps and hydrographic or similar charts of all kinds, including atlases, wall maps, topographical plans and globes, printed</td>
          </tr>
          <tr>
            <td>4905 10 00</td>
            <td>-</td>
            <td>-</td>
            <td>- globusi</td>
            <td>- Globes</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- ostalo</td>
            <td>- Other</td>
          </tr>
          <tr>
            <td>4905 91 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- u obliku knjiga</td>
            <td>-- In book form</td>
          </tr>
          <tr>
            <td>4905 99 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>4906 00 00</td>
            <td>-</td>
            <td>-</td>
            <td>Originalni planovi i nacrti za arhitektonske, inženjerske, industrijske, komercijalne, topografske ili slične namjene, rađeni rukom; rukopisi; fotografske reprodukcije na osjetljivom papiru i karbonske kopije prethodno spomenutih proizvoda</td>
            <td>Plans and drawings for architectural, engineering, industrial, commercial, topographical or similar purposes, being originals drawn by hand; handwritten texts; photographic reproductions on sensitised paper and carbon copies of the foregoing</td>
          </tr>
          <tr>
            <td>4907 00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Neiskorištene poštanske marke, porezne ili slične markice, trenutačno u opticaju ili novoizdane u zemlji namjene u kojoj imaju, ili će imati, prepoznatljivu nominalnu vrijednost; papir s utisnutim žigom; banknote; čekovi; akcije, dionice ili obveznice i slični dokumenti</td>
            <td>Unused postage, revenue or similar stamps of current or new issue in the country in which they have, or will have, a recognised face value; stamp-impressed paper; banknotes; cheque forms; stock, share or bond certificates and similar documents of title</td>
          </tr>
          <tr>
            <td>4907 00 10</td>
            <td>-</td>
            <td>-</td>
            <td>- poštanske marke, porezne i slične markice</td>
            <td>- Postage, revenue and similar stamps</td>
          </tr>
          <tr>
            <td>4907 00 30</td>
            <td>-</td>
            <td>-</td>
            <td>- banknote</td>
            <td>- Banknotes</td>
          </tr>
          <tr>
            <td>4907 00 90</td>
            <td>-</td>
            <td>-</td>
            <td>- ostalo</td>
            <td>- Other</td>
          </tr>
          <tr>
            <td>4908</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Preslikači svih vrsta (dekalkomanije)</td>
            <td>Transfers (decalcomanias)</td>
          </tr>
          <tr>
            <td>4908 10 00</td>
            <td>-</td>
            <td>-</td>
            <td>- preslikači (dekalkomanije), za ostakljivanje</td>
            <td>- Transfers (decalcomanias), vitrifiable</td>
          </tr>
          <tr>
            <td>4908 90 00</td>
            <td>-</td>
            <td>-</td>
            <td>- ostalo</td>
            <td>- Other</td>
          </tr>
          <tr>
            <td>4909 00 00</td>
            <td>-</td>
            <td>-</td>
            <td>Tiskane ili ilustrirane razglednice; tiskane karte s osobnim čestitkama, porukama ili objavama, neovisno jesu li ilustrirane ili ne, sa ili bez omotnica ili ukrasa</td>
            <td>Printed or illustrated postcards; printed cards bearing personal greetings, messages or announcements, whether or not illustrated, with or without envelopes or trimmings</td>
          </tr>
          <tr>
            <td>4910 00 00</td>
            <td>-</td>
            <td>-</td>
            <td>Kalendari svih vrsta, tiskani, uključujući kalendare u blokovima</td>
            <td>Calendars of any kind, printed, including calendar blocks</td>
          </tr>
          <tr>
            <td>4911</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Ostali tiskani materijali, uključujući tiskane slike i fotografije</td>
            <td>Other printed matter, including printed pictures and photographs</td>
          </tr>
          <tr>
            <td>4911 10</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- trgovački reklamni materijal, trgovački katalozi i slično</td>
            <td>- Trade advertising material, commercial catalogues and the like</td>
          </tr>
          <tr>
            <td>4911 10 10</td>
            <td>-</td>
            <td>-</td>
            <td>-- trgovački katalozi</td>
            <td>-- Commercial catalogues</td>
          </tr>
          <tr>
            <td>4911 10 90</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>- ostalo</td>
            <td>- Other</td>
          </tr>
          <tr>
            <td>4911 91 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- slike, grafike i fotografije</td>
            <td>-- Pictures, designs and photographs</td>
          </tr>
          <tr>
            <td>4911 99 00</td>
            <td>-</td>
            <td>-</td>
            <td>-- ostalo</td>
            <td>-- Other</td>
          </tr>


        </tbody>
      </table>
    </div>
    <br>
    <br>
    
    
    <!--
    
    <div class="table_wrap">
      <h2>Jedinice mjere</h2>
      <table class="info_table incoterms_table" style="width: 100%; min-width: 1300px;">
        <tr class="incoterms_header" style="background: #eaeaea; font-weight: 700;">
          <td>JEDINICA MJERE</td>
          <td>NAZIV JEDINICE MJERE (Hrvatski)</td>
          <td>Objašnjenje jedinice mjere (Hrvatski)</td>
          <td>NAZIV JEDINICE MJERE (Engleski)</td>
          <td>Objašnjenje jedinice mjere (Engleski)</td>
        </tr>
        <tr class="incoterms_header" style="background: #eaeaea; font-weight: 700;">
          <td>Supplementary Unit</td>
          <td>Supplementary Unit Name (Croatian)</td>
          <td>Supplementary Unit Explanation (Croatian)</td>
          <td>Supplementary Unit Name (English)</td>
          <td>Supplementary Unit Explanation (English)</td>
        </tr>
        <tr>
          <td>11</td>
          <td>kom</td>
          <td>broj komada</td>
          <td>p/st</td>
          <td>Number of items</td>
        </tr>
        <tr>
          <td>12</td>
          <td>1000 kom</td>
          <td>tisuću komada</td>
          <td>1000 p/st</td>
          <td>Thousand items</td>
        </tr>
        <tr>
          <td>13</td>
          <td>100 kom</td>
          <td>sto komada</td>
          <td>100 p/st</td>
          <td>Hundred items</td>
        </tr>
        <tr>
          <td>20</td>
          <td>gi F/S</td>
          <td>gram fisilnog izotopa</td>
          <td>gi F/S</td>
          <td>Gram of fissile isotopes</td>
        </tr>
        <tr>
          <td>21</td>
          <td>g </td>
          <td>gram</td>
          <td>g</td>
          <td>Gram</td>
        </tr>
        <tr>
          <td>25</td>
          <td>nt</td>
          <td>nosivost u tonama</td>
          <td>ct/l</td>
          <td>Carrying capacity in tonnes<1>
          </td>
        </tr>
        <tr>
          <td>26</td>
          <td>m</td>
          <td>metar</td>
          <td>m</td>
          <td>Metre</td>
        </tr>
        <tr>
          <td>27</td>
          <td>m2</td>
          <td>četvorni (kvadratni) metar</td>
          <td>m2</td>
          <td>Square metre</td>
        </tr>
        <tr>
          <td>28</td>
          <td>m3</td>
          <td>kubični metar</td>
          <td> m3</td>
          <td>Cubic metre</td>
        </tr>
        <tr>
          <td>29</td>
          <td>1000 m3</td>
          <td>tisuću kubičnih metara</td>
          <td>1000 m3</td>
          <td>Thousand cubic metres </td>
        </tr>
        <tr>
          <td>31</td>
          <td>kg/net om</td>
          <td>kilogram neto ocijeđene mase</td>
          <td>kg/net eda</td>
          <td>Kilogram drained net weight</td>
        </tr>
        <tr>
          <td>32</td>
          <td>kg 90% st</td>
          <td>kilogram 90% suhe tvari</td>
          <td>kg 90 % sdt</td>
          <td>Kilogram of substance 90 % dry</td>
        </tr>
        <tr>
          <td>33</td>
          <td>l</td>
          <td>litra</td>
          <td>l </td>
          <td>Litre </td>
        </tr>
        <tr>
          <td>35</td>
          <td>k</td>
          <td>karat (1 metrički karat = 2 x 10-4 kg)</td>
          <td>c/k</td>
          <td>Carats (1 metric carat = 2 × 10-4 kg)</td>
        </tr>
        <tr>
          <td>36</td>
          <td>1000 l</td>
          <td>tisuću litara</td>
          <td>1000 l</td>
          <td>Thousand litres</td>
        </tr>
        <tr>
          <td>37</td>
          <td>l alk. 100%</td>
          <td>litra čistog (100%) alkohola</td>
          <td>l alc. 100%</td>
          <td>Litre pure (100 %) alcohol</td>
        </tr>
        <tr>
          <td>45</td>
          <td>par</td>
          <td>broj pari</td>
          <td>pa </td>
          <td>Number of pairs</td>
        </tr>
        <tr>
          <td>51</td>
          <td>kg C5H14ClNO</td>
          <td>kilogram kolin klorida</td>
          <td>kg C5H14ClNO</td>
          <td>Kilogram of choline chloride</td>
        </tr>
        <tr>
          <td>52</td>
          <td>kg H2O2</td>
          <td>kilogram vodikovog peroksida</td>
          <td>kg H2O2</td>
          <td>Kilogram of hydrogen peroxide </td>
        </tr>
        <tr>
          <td>53</td>
          <td>kg K2O</td>
          <td>kilogram kalijevog oksida</td>
          <td>kg K2O </td>
          <td>Kilogram of potassium oxide</td>
        </tr>
        <tr>
          <td>54</td>
          <td>kg KOH</td>
          <td>kilogram kalijevog hidroksida</td>
          <td>kg KOH</td>
          <td>Kilogram of potassium hydroxide (caustic potash)</td>
        </tr>
        <tr>
          <td>55</td>
          <td>kg met.am.</td>
          <td>kilogram metilamina</td>
          <td>kg met.am.</td>
          <td>Kilogram of methylamines</td>
        </tr>
        <tr>
          <td>56</td>
          <td>kg N</td>
          <td>kilogram dušika</td>
          <td>kg N</td>
          <td>Kilogram of nitrogen</td>
        </tr>
        <tr>
          <td>57</td>
          <td>kg NaOH</td>
          <td>kilogram natrijevog hidroksida</td>
          <td>kg NaOH</td>
          <td>Kilogram of sodium hydroxide (caustic soda)</td>
        </tr>
        <tr>
          <td>58</td>
          <td>kg P2O5</td>
          <td>kilogram difosfornog pentoksida</td>
          <td>kg P2O5 </td>
          <td>Kilogram of diphosphorus pentaoxide</td>
        </tr>
        <tr>
          <td>59</td>
          <td>kg U</td>
          <td>kilogram urana</td>
          <td>kg U</td>
          <td>Kilogram of uranium</td>
        </tr>
        <tr>
          <td>60</td>
          <td>će</td>
          <td>broj ćelija</td>
          <td>ce/el</td>
          <td>Number of cells</td>
        </tr>
        <tr>
          <td>61</td>
          <td>TJ</td>
          <td>teradžul (terajoule) (bruto kalorijska vrijednost)</td>
          <td>TJ</td>
          <td>Terajoule (gross calorific value)</td>
        </tr>
        <tr>
          <td>64</td>
          <td>1000 kWh</td>
          <td>tisuću kilovat sati</td>
          <td>1000 kWh</td>
          <td> Thousand kilowatt hours</td>
        </tr>
      </table>
      
      
    </div>
    <br>
    <br>
    
    -->
    

    <h3>Velprom vozila</h3>
    
    
      <div class="vel_info_text col_count_2">
        <div class="no_col_break">
          <img src="/img/info/velprom_kamion_palete.png" alt="" style="margin: 0;">
        </div>
        
      <div class="no_col_break">
        <h4>Velprom Kamion MAN</h4>
        Dužina: 6000 mm<br>
        Širina: 2400 mm<br>
        Visina: 2400 mm<br>
        Visina tovarnog prostora: 2300 mm<br>

        <br>
        Broj paletnih mjesta: 15<br>
        <br>

      </div> <!-- end no col break -->

    </div> <!-- end info text -->
    
    <br>
    <br>
        
      <div class="vel_info_text col_count_2">
        <div class="no_col_break">
          <img src="/img/info/velprom_kombi_palete.png" alt="" style="margin: 0;">
        </div>
        
      <div class="no_col_break">
        <h4>Velprom kombi Citroen Jumper 1, 2 i 3</h4>
        Dužina: 4000 mm<br>
        Širina: 1600 mm<br>
        <br>
        Visina kombi 1 i 2: 2000 mm<br>
        Visina kombi 3: 1750 mm<br>
        
        <br>
        Broj paletnih mjesta: 5<br>
        <br>

      </div> <!-- end no col break -->

    </div> <!-- end info text -->
    
    <br>
    <br>
    
    <div class="vel_info_text col_count_2">
        <div class="no_col_break">
          <img src="/img/info/velprom_prikolica_palete.png" alt="" style="margin: 0;">
        </div>
        
      <div class="no_col_break">
        <h4>Velprom Prikolica</h4>
        Dužina: 2500 mm<br>
        Širina: 1650 mm<br>
        Visina: 2000 mm<br>
        
        <br>
        Broj paletnih mjesta: 4<br>
        <br>

      </div> <!-- end no col break -->

    </div> <!-- end info text -->
    
    <br>
    <br>
    
    
    

    <div class="vel_info_text">
     
      <div class="no_col_break">
        <h4 class="go_left" style="margin-top: 0;">Citroen Berlingo 1</h4>
        Broj paletnih mjesta: 1<br>
        Visina tovarnog prostora: 1000 mm<br>
      </div>
      
      <div class="no_col_break">
        <h4 class="go_left" style="margin-top: 0;">Citroen Berlingo 2</h4>
        Broj paletnih mjesta: 1<br>
        Visina tovarnog prostora: 1000 mm<br>
      </div>
      
      <div class="no_col_break">
        <h4 style="margin-top: 0;">Citroen Berlingo 3</h4>
        Broj paletnih mjesta: 1<br>
        Visina tovarnog prostora: 1000 mm<br>
      </div>
      
    </div> <!-- end info text -->


    <h3>Vanjska usluga prijevoza</h3>


    <div class="vel_info_text">


      <img src="/img/info/12.jpg" alt="">


      <div class="no_col_break">


        <h4>Šleper</h4>
        
        Dužina šlepera: 13,60 m<br>
        Širina šlepera: 2,46 m<br>
        Visina šlepera: 2,65 – 2,80 m<br>
        Nosivost: do 24000 kg/90m3<br>
        <br>
        Broj paletnih mjesta: 33<br>
        <br>
        

        <h4>Šleper mega</h4>
        
        Dužina šlepera: 13,60 m<br>
        Širina šlepera: 2,46 m<br>
        Visina šlepera: 3,05 – 3,10 m<br>
        Nosivost: do 24000 kg/100m3<br>
        <br>
        Broj paletnih mjesta: 33<br>
        <br>

      </div> <!-- end no col break -->
    </div>

   <br>

    <div class="vel_info_text">


      <img src="/img/info/13.jpg" alt="">


      <div class="no_col_break">

        <h4>Prikoličar (Kamion + prikolica)</h4>
        Dužina kamiona 6,50 m + dužina prikolice 7,50 m<br>
        Širina kamiona i prikolice 2,46 m<br>
        Visina kamiona i prikolice 2,65 – 2,80 m<br>
        Nosivost: do 24000 kg/90 m3<br>
        <br>
        Broj paletnih mjesta: 33<br>
        <br>

        <h4>Tandem (Kamion + prikolica)</h4>
        Dužina kamiona 7,30 m + dužina prikolice 8,20 m<br>
        Širina kamiona i prikolice 2,46 m<br>
        Visina kamiona i prikolice 2,80 – 3,00 m<br>
        Nosivost: do 24000 kg/120 m3<br>
        <br>
        Broj paletnih mjesta: 38<br>
        <br>
        
      </div> <!-- end no col break -->


    </div> <!-- end info text -->
    
    <h3 style="margin: 0;">DUŽNI METRI (LDM) ZA KAMION</h3>
    <img src="/img/info/sleper_tandem_palete.png" alt="" style="max-width: 100%; width: 100%; margin: 0;" >  
    
    <br>
    <br>
    
    
    
    
    
  </div> <!-- end info page -->


  <div class="vel_info_page">

    <h2 id="anchor_info_skladiste">Skladište</h2>

    <h3>Viličari</h3>

    <div class="vel_info_text">

      <h4 style="margin-top: 0;">Viličar, čeoni, plinski 1 (RX70-20T)</h4>
      Nosivost: 1600 kg Visina dizanja: 8065 mm <br>

      <h4>Viličar, čeoni, plinski 2 (RX70-20T)</h4>
      Nosivost: 2000 kg Visina dizanja: 8065 mm<br>

      <h4>Viličar, čeoni, električni 1 (RX20-16P)</h4>
      Nosivost: 1600 kg Visina dizanja: 7870 mm<br>

      <h4>Ručno vođeni viličar, električni 1 (EXV10)</h4>
      Nosivost: 1000 kg Visina dizanja: 5466 mm<br>

      <h4>Ručni viličar (15 komada)</h4>
      Nosivost: 1200 kg Visina dizanja: 250 mm<br>

    </div> <!-- end info text -->

<br>
<br>

<h3>Šatori</h3>


<div class="vel_info_text">
 
  
 
  <div class="no_col_break">
   <h4 style="margin-top: 0;">ŠATORI 1, 2 i 3</h4>
   
    Dužina: 21,4 m<br>
    Širina: 12,2 m<br>
    Visina: 6,4 m<br>
  </div> <!-- end no col break -->

  <div class="no_col_break">
  
   <h4 style="margin-top: 0;">ŠATOR 4</h4>
    Dužina: 40 m<br>
    Širina: 25 m<br>
    Visina: 8 m<br>
  </div> <!-- end no col break -->
  
  
</div>



  </div> <!-- end info page -->


  

<h2 id="otpad_info">Otpad</h2>


<div class="vel_info_page">


  <div class="vel_info_text">
    <div class="no_col_break">
    Otpad, osim zbog okoliša, razvrstavamo jer postoje otkupljivači otpada koji će za određeni iznos platiti da se otpad odveze te reciklira. 
KLJUČNI BROJ otpada je jedinstvena oznaka vrste otpada koja se sastoji od šesteroznamenkastoga broja kojem je, u slučaju opasnog otpada, pridružen znak *

    </div>

  </div>

  <div class="vel_info_text col_count_2">

   <div class="no_col_break">
    <img src="/img/info/Picture1.png" alt="" style="max-width: 400px" >  
    <img src="/img/info/Picture2.png" alt="" style="max-width: 400px" > 
  </div>
    <div class="no_col_break">

      <h4>15 01 01</h4>
      PAPIRNA I KARTONSKA AMBALAŽA – karton, papir

    </div>

  </div>

  <div class="vel_info_text col_count_2">

   <div class="no_col_break">

   <img src="/img/info/Picture3.png" alt="">
    BANDAŽNA TRAKA
    <img src="/img/info/Picture4.png" alt="">
    STRETCH FOLIJA

    <img src="/img/info/poli.png" alt="">
    POLIPROPILEN
    <img src="/img/info/foreks.png" alt="">
    FOREKS

    <img src="/img/info/pvc_1.png" alt="">
    PVC
    <img src="/img/info/pvc_2.png" alt="">
    PVC

  </div>



    <div class="no_col_break">

      <h4>15 01 02 </h4>
      PLASTIČNA AMBALAŽA – polipropilen, foreks, bandažne trake, stretch folija, PVC

    </div>

  </div>    


  <div class="vel_info_text col_count_2">

  <div class="no_col_break">
    <img src="/img/info/drvo.png" alt="">
  </div>

    <div class="no_col_break">

      <h4>15 01 03  </h4>
      DRVENA AMBALAŽA – otpatci od drveta, paleta i dr.

    </div>

  </div>


  <div class="vel_info_text col_count_2">
  <div class="no_col_break">
    <img src="/img/info/kadica_1.png" alt="">
    <img src="/img/info/kadica_2.png" alt="">
  </div>

    <div class="no_col_break">

      <h4>15 01 10*  </h4>
      AMBALAŽA KOJA SADRŽI OSTATKE OPASNIH TVARI ILI JE ONEČIŠĆENA OPASNIM TVARIMA - prazne kante onečišćene bojom ili drugim opasnim tvarima (npr.nitro)

      <h4>08 01 11* </h4>
      OTPADNE BOJE I LAKOVI KOJI SADRŽE ORGANSKA OTAPALA ILI DRUGE OPASNE TVARI – kantice laka, boje ili stvrdnute kantice boje

      <h4>08 03 17* </h4>

      OTPADNI TISKARSKI TONERI KOJI SADRŽE OPASNE TVARI - kante pomiješane boje

      <br>
      <br>

      Ključni brojevi <b>15 01 10*</b>, <b>08 01 11*</b> i <b>08 03 17*</b> stavljamo u zajedničku kadicu jer tako lakše otkupljivaču tog otpada


    </div>

  </div>                


  <div class="vel_info_text col_count_2">
  <div class="no_col_break">
    <img src="/img/info/iron.png" alt="">
  </div>

    <div class="no_col_break">

      <h4>12 01 01 </h4>
      STRUGOTINE I OPILJCI KOJI SADRŽE ŽELJEZO

    </div>

  </div>    



  <div class="vel_info_text col_count_2">
  <div class="no_col_break">
    <img src="/img/info/color_metal.png" alt="">
  </div>

    <div class="no_col_break">

      <h4>12 01 03 </h4>
      STRUGOTINE I OPILJCI OBOJENIH METALA – otpatci od aluminija, bakra, bronce i drugih metala

    </div>

  </div>     



  <div class="vel_info_text col_count_2">
  <div class="no_col_break">
    <img src="/img/info/baterije.png" alt="">
  </div>

    <div class="no_col_break">

      <h4>20 01 33*</h4>
      BATERIJE I AKUMULATORI OBUHVAĆENI POD <b>16 06 01*</b>, <b>16 06 02*</b> ILI <b>16 06 03*</b> I NESORTIRANE BATERIJE I AKUMULATORI KOJI SADRŽE TE BATERIJE

    </div>

  </div>     


  <div class="vel_info_text col_count_2">
  <div class="no_col_break">
    <img src="/img/info/elektro.png" alt="">
  </div>

    <div class="no_col_break">

      <h4>20 01 35* </h4>
      ODBAČENA ELEKTRIČNA I ELEKTRONIČKA OPREMA KOJA NIJE NAVEDENA POD <b>20 01 21* </b>I <b>20 01 23* </b>KOJA SADRŽI OPASNE KOMPONENTE
    </div>

  </div>    



</div>


<h2 id="sloning">Upute za rad u Sloning aplikaciji</h2>

<div class="vel_info_page">

  <h3>Remote Desktop</h3>
  
  <div style="color: darkred; font-weight: 700; font-size: 20px;">
    Svi korisnici koji Sloningu pristupaju putem remote desktopa prilikom završetka rada u programu moraju <br>
    KLIKNUTI NA START, kao kad gasimo računalo, a zatim KLIKNUTI NA LOG OFF. <br>
    Na taj način će svi biti pravilno odlogirani!
    <br>
    <img src="/img/info/log_off.png" alt=""  style="width: 350px; display: block; float: left;"  />
    <br>
    <br style="clear: both;">
Ovo se NE odnosi na spajanje preko AnyDeska.<br>
Kad radite preko AnyDeska se ne trebate odlogirati.
 <br>
 <br>
 
  
  </div>
  <br>
  <br>
    
 
  <div class="vel_info_text">
    U Sloningu za SVE dokumente bitne su slijedeće ikonice:
  </div> 
  
  <img src="/img/info/slon_1.png" alt=""  style="max-width: 70%; min-width: 300px;" />
  
  <br>
  
  <div class="vel_info_text">
    Slijedeće ikonice su bitne da saznaš čega ima:
  </div> 
  
  <img src="/img/info/slon_2.png" alt=""  style="max-width: 70%;  min-width: 300px;" />
  

  <div class="vel_info_text">
    Bitno je znati da se za pretragu u Sloningu koristi <b style="color: red; font-size: 18px;">*</b>. Pojam koji zelimo pretrazivati upisemo na slijedeći način <b style="color: red; font-size: 18px;">*</b>traženi pojam<b style="color: red; font-size: 18px;">*</b> ENTER
    <br>
    Od pomoći su i tipke F4 i F5. Sloning na svakom mjestu izbaci skočni prozorčić koji uputi koja od te dvije tipke može pomoći. Svi koji rade na laptopima trebaju obratiti pažnju jer na laptopima najčešće ne funkcioniraju tipke F ukoliko se prije ne stisne i tipka Fn ( funkcija).

  </div> 
  
      
  <img src="/img/info/slon_3.png" alt=""  style="max-width: 80%; min-width: 300px;" />
      
      
  <h3>Dokumenti</h3>    
      
  <div class="vel_info_text">
  
Pod DOKUMENTI nalazi se slijedeće: <br>
<b>PRIMKA</b> ( skraćeno PR)- kroz ovaj dokument zaprima se sve što stigne u Velprom iz vana ( od dobavljača). ( 1. ikona na komandnoj listi )
<br>
<br>
<b>PONUDA</b> ( skraćeno PO  ako ikad vidite u listi korištenja ispod IN br. 11 onda se taj br. odnosi na ponudu prethodne godine) (2. ikona na komandnoj listi)- kroz ovo generiramo ponude kupcima. Unutar ovog dokumeta razlikujemo :
<br>
<br>

<ol type="a">
  <li><b>DEVIZNU PONUDU</b> koju šaljemo ino kupcima i po kojoj oni mogi uzvršiti uplatu</li>
  <li><b>REGULARNU PONUDU</b> koju saljemo tuzemnim kupcima i po kojoj kupci mogu izvršiti uplatu</li>
  <li><b>PROFORMA PONUDU</b> koju koristimo kad kupac za jedan artikl traži varijacije u količinama npr. ponuda za 100, 200 i 500 kom. Kad se kupac odluči za točnu količinu onda mu dodatno posaljemo REGULARNU PONUDU ( navedenu pod b) koja mu je onda zvanična i po kojoj može izvršiti uplatu</li>
</ol>

    <b style="color: red;">PREDRAČUN</b> ( skraćeno <b style="color: red;">PD</b>) ne koristimo jer umjesto njega saljemo PONUDU , ali ako kupac baš inzistira, onda mu se pošalje predračun, ali taj dio s kupcem izregulira naš odjel financija
<br>
<br>

    <b style="color: red;">RAČUN</b> ( skaraćeno <b style="color: red;">RA</b>)-  koristi samo odjel financija. Saljemo VELEPRODAJNI tuzemnim kupcima, a DEVIZNI ino kupcima (3. ikona na komandnoj listi)
<br>
<br>

    <b style="color: red;">OTPREMNICA</b>  ( skraćeno <b style="color: red;">OT</b> ako ikad vidite u listi korištenja ispod IN br. 10 onda se taj br. odnosi na otpremnicu prethodne godine)- putem nje otpremamo robu iz Velproma ( 4. ikona na komandnoj listi)
<br>
<br>

<b style="color: red;">MEĐUSKLADIŠNICA</b> ( skraćeno MS) premještamo robu sa jednog skladista na drugo. Ukoliko je netko ne pažljiv i prilikom spremanja dokumenta ne vodi računa na koje je skladište naveo da roba ide onda se međuskladisnicom toj robi promjeni lokacija. Isto tako ukoliko se roba fizički preseli sa jednog skladista na drugo, međuskladišnica će promjeniti njegovu staru lokaciju u novu ( 5. ikona na komandnoj listi)
<br>
<br>

    <b style="color: red;">INVENTURA</b> (skraćeno <b style="color: red;">IN</b>) koristi se u inventurama kao inventurna lista. Mi je vrlo rijetko koristimo
<br>
<br>

    <b style="color: red;">OTPIS</b> ( skraćeno <b style="color: red;">TP</b> ) koristi se za micanje robe koja vise nije u funkiji. Mi ga također nismo još koristili, ali je vrlo koristan za skidanje robe sa stanja skladista koja vise nije u funkciji. Recimo imamo tipkovnicu koja se pokvarila i bacamo je. Trebala bi ici na otpis da se vise ne tereti određenog zaposlenika za tu tipkovnicu. Ili recimo imamo uništenu robu , ploče, i bacamo ih isto se stavlja na otpis. Za sad ga ne koristimo.
<br>
<br>

    <b style="color: red;">STORNO ULAZA </b>( skraćeno <b style="color: red;">SU</b>) –Robu koja je došla i zaprimljena je  kod nas , vraćamo dobavljaču. Najčešće vraćamo palete.
<br>
<br>

    <b style="color: red;">STORNO IZLAZA</b> ( skraćeno  <b style="color: red;">SI</b>) – kad nama nešto vrate što smo izdali
<br>
<br>

    <b style="color: red;">NARUDŽBA</b> ( skraćeno <b style="color: red;">NA</b>)- naručujemo vanjsku uslugu. ( tiska, izrade alata isl) Točnije bi bilo sve naručivati kroz narudžebe dobavljačima, ali sad kad smo ovaj ispis prilagodili za naše potrebe naručivanja tiska i neka ostane do daljenjeg tako.
<br>
<br>

    <b style="color: red;">OBAVIJEST O KNJIŽENJU </b>( skraćeno <b style="color: red;">OK</b>)- koristi samo odjel financija, kad  na vec izdanu fakturu rade dodatni popust iz nekog razloga( reklamacije, vraćenih paleta isl)

<b style="color: red;">PREDUJAM</b> ( skraćeno <b style="color: red;">AV</b>)- financije obavijestavaju prodaju da je kupac platio ponudu koju je dobio od nas. To je prodaji znak da ponudu može u realizaciju.
 

  </div> 
  
  
  
  <h3>Narudžbe/Evidencije</h3>    
      
  <div class="vel_info_text">
    Pod NARUDŽBE/EVIDENCIJE imamo slijedeće dokumente:
    <br>
    <br>
    <b style="color: red;">NARUDŽBE DOBAVLJAČIMA </b>( skrećeno <b style="color: red;">ND</b>)- ovim dokumentom saljemo narudžbu dobavljaču
    <b style="color: red;">NARUDŽBE KUPACA</b> ( skraćeno <b style="color: red;">NK</b>) saljemo potvrdu narudžbe kupcu. Kad kupac kaže da bi nešto naručio , ili kad plati ponudu uvijek mu šaljemo potvrdu narudžbe kao dokaz da je ono sto namj je javio platio primljeno na znanje i da krećemo s izradom posla , te ga obavijestamo o točnom načinu isporuke i roku izrade tražene robe.
    
    U <b style="color: red;">EVIDENCIJAMA</b> se vidi cijelokupan popis Narudžbi po kupcu / dobavljaču. Za pregled Kupaca evidencije se nisu pokazale dobre, dok se za pregled dobavljača koriste. Kao se ode u evidencije i odabere željenog dobavljača onda se moze vidjeti da se od tog dobavljača očekuje naručena roba i provjeri se odmah da li je ta roba baš naručena. Iz tog razloga sve narudžbe prema dobavljačima, bilo tiskarama i slično  smatram da bi trebali voditi kroz narudžbe dobavljača iz razloga sto onda skladiste vidi da se od tog dobavljača očekuje neki posao i po tome odmah moze napraviti primku i dokumenti bi bili bolje povezani, a ne kao što sad radimo da financije dodatno istražuju vezu između naručenog i zaprimljenog računa. Jedini problem je sto se u Narudžbama dobljačima roba zaprima putem jasnih i točnih sifri , a za zaprimanje tiska, alata i slično koriste se univerzalene šifre, koje kad bi se samo jednoznačno zaprimile, ne bi znali za sta je navedeni ulaz vezan osim ako ne bi djelovali na šifriranje.
    
    
  </div>  


  <h3>Stari Dokumenti</h3> 
  <div class="vel_info_text">
    U <b style="color: #4d75b4;">STARI DOKUMENTI</b> nalaze se dokumeti prošle godine Samo njih par ( narudžbe, ponude , otpremnice). Ovim djelom se koristimo ponajvise po prelaski iz godine u godinu. Kako se u SLONINGU svaka godina mora otvoriti posebno, onda je velika tlaka stalno prelaziti iz godine u godinu. Za potrebe pogleda u proslu godinu za  ovih par izdvojenih dokumenata Sloning nam je kreirao " prozor za pogled unatrag".
  </div>
  
  <h3>Proizvodnja</h3> 
  <div class="vel_info_text">
    U <b style="color: #4d75b4;">PROIZVODNJA</b> nalaze se dokumeti koji su izravno vezani za izradu u proizvodnji, a kroz to djeluju i na stanje skladista.

    <b style="color: red;">RADNI NALOG</b> ( skraćeno <b style="color: red;">RN</b>)- naši radi nalozi ( 6. ikona na komandnoj listi)
    <br>
    <br>
    
    <b style="color: red;">UTROŠAK</b> ( skraćeno <b style="color: red;">UT</b>)- skida materijala( robu ( ploče, sitni repro isl.)) sa skladista. ( 7. ikona na komandnoj listi)
    <br>
    <br>
    
    <b style="color: red;">NORMATIV</b> – uopće ne koristimo jer kod SLONINGA tu ima neki problem. Zamišljeno je dobro, ali nije realizirano jer mi imamo više mogućnosti doći do cilja. To se koristi kad se točno zna da bi se neki proizvod napravio potrebno ti je točno to što je tu navedeno i nista drugo. Kako mi krojimo iz raznih ploča onda "bas to samo to " nam ne odgovara. Taj NORMATIV da možemo provesti onda bi odmah po nalogu skidao sto se za određeni proizvod mora uvijek utrošiti ( 8. ikona na komandnoj listi)
    <br>
    <br>
    
    <b style="color: red;">PRIMKA IZ PROIZVODNJE</b> ( skraćeno <b style="color: red;">PP</b>) sve sto je proizvedeno u proizvodnji i upisano u izvjestaj smjene zaprima se na skladiste putem ovog dokumenta i kasnije otprema otpremnicom. Otpremnica se ne moze proknjiziti ukoliko robe nama na skladistu, odnosno ukoliko ona nije proizvedena.
    <br>
    <br>
    
    <b style="color: red;">EVIDENCIJA RADNIH NALOGA</b> – ne koristi se 
    
  </div>
  
  
  <h3>Kartoteke</h3> 
  <div class="vel_info_text">
  
    U <b style="color: #4d75b4;">KARTOTEKAMA</b>  su mjesta gdje se strogo mora voditi računa što se mijenja i koji novi elemeti se dodaju. Svaki novi unos u bilo koju kartoteku treba se javno objaviti da drugi znaju sta se radilo i sta se planira. Kad bi u kartotekama svako provodio svoju volju nastala bi opća pomutnja. Zato je iznimno bitno da u kartotekama svi radimo na isti način.
    Imamo slijedeće baze/kartoteke:
    <br>
    <br>
    
    <b style="color: red;">STANJE SKLADIŠTA</b> - baza svega sto u ovom trenutku imamo evidentirano na našem skladistu ( 14. ikona na komandnoj listi) mogu 
    <br>
    <br>
    
    <b style="color: red;">KARTOTEKA MATERIJALI/USLUGE</b> – ( 15. ikona na komandnoj listi) u njoj su navedeni svi proizvodi koje smo proizvodili, kupovali. Ovdje se jos nalaze:
    <br>
    <br>
    
    <ol type="a">
<li><b>GRUPE MATERIJALA</b> - radi lakše pretrage materijale podjelimo u grupe pa onda pretrazujete samo po tome. Ovdje je sve grupe formirala Nabava prema svom nahođenju, ali grupe se u dogovoru sa svima mogu dodati i oduzeti ili promjeniti. Npr. imamo grupu RO- radna oprema i sve sto sluzi za opremanje radnika nalazi se u toj grupi, majce, cipele isl. Administracija kad zeli vidjeti kakvo joj je stanje grupe određenih artikala, ode u stanje skladista i odabere grupu i vidi pregled svih artikala zavednih u toj grupi. Tako kad bi Prodaja htjela vidjeti sta od akcijskih kutija ima na lageru ode u stanje skladista i odabere grupu AKC i izlistaju se sve akcijske kutije s količinama koje imamo na stanju. Na isti način se moze i staviti premazni materijal koji koriste na printeru i dalo bi joj bolji efekat i točno bi se znalo za sto se i kad utrošio materijal nego sad kad ne mozemo pratiti tko dodaje, a tko oduzima.</li>

<li><b>RABATI ZA GRUPE MATERIJALA</b>- nismo do sad nikad koristili</li>

<li><b>TARIFNE GRUPE</b>- odnose se na PDV, ali mozda mogu imati i drugu funkciju . Na znam treba se dodatno istražiti</li>

<li><b>KVALITETA MATERIJALA</b>- svi materijali koje koristimo. Trenutno je ta kartoteka bez ikakvog reda. Ukoliko je zelimo lijepo posloziti, možemo , jer njena izmjena njeće utjecati na do sad vec kreirane dokumente.</li>

<li><b>JEDINICA MJERE</b>- kad upisujemo neki proizvod i za njega nemamo adekvatnu jedinicu mjere, ovdje dodamo tu jedinicu i unesemo element normalno u kartoteku materijala. To su najčešće mjerne jedinice, ali mogu biti i neka druga obilježja kao  "set2", "arak" i sl.</li>

    </ol>
    
    
    <b style="color: red;">KARTOTEKA PARTNERA</b> - baza svih kupaca i dobavljača s kojima je Velprom ikad radio. Tu su zavedeni svi njihovi podaci o registracij, kontakt podaci, mjesta isporuke, dodatni popusti ili poskupljenja, da li su nam dali neki dokument kao zaduznicu is. Unutar kartoteke razlikujemo jos slijedeće mogućnosti:
    <br>
    <br>
    
    
<ol type="a">    

<li><b>GRUPE PARTNERA</b> -ovo koristimo ako zelimo pratiti koliko posla ostvarujemo s nekom grupacijom. Trenutno to koristimo za Atlantic, ali moze se koristiti za bilo koju drugu grupaciju i onda vidjeti koliko ukupno poslujete s tom grupacijom , nego da svaku poslovnicu promatrate zasebno i onda računate njihov rezultat da bi vidjeli kako posluje ta grupacija.
<br>
Ukoliko zelite uvesti  novu grupu morate voditi računa o 2 stvari ili da partnera uvrstite u tu grupu ili da na svaki od dokumenata koji kreirate za tog partnera ne zaboravite staviti da su dio grupacije. Ukoliko nista stavili Grupu na partneru svaki dikument koji budete kreirali za njega biti će bez grupe i vi ce te mu grupu morati dodati, ako ste zaborili taj dokument ce se tretirati kao svaki drugi. Zato vodite računa gdje dodajete grupu i čemu ona služi.</li>

<li><b>VRSTE PARTNERA</b>- ovo uopće ne koristimo,ali moze se koristiti da se odredi koji je partner kakav. Da li nam je taj A1 kupac ili je A2, ili je mozda B1 ili D kupac. Ovisno kako s njim poslujemo.</li>
<li><b>PODRUČJE UTJECAJA</b>- takodjer ne koristimo, a kroz ovo se moze vidjeti ako bi partnere dodavali određenim skupinama pa da vidimo s kojim partnerima iz tog podrucja utjecaja radimo. Recimo "vina" svakoj vinariji s kojom radimo dodjelimo područje utjecaja vino i vidimo koliko uopće radimo s vinarijama i kako surađujemo s i kojim tipom proizvoda za tu branšu. Ili "prehrambena industrija" odmah bi vidjeli koji je nas udio u takvim poslovima i s kojim proizvodima, "auto industrija", "kozmetika" isl.</li>

</ol>


    <b style="color: red;">KARTOTEKA SKLADISTA</b> - popis svih skladista koje imamo
    <br>
    <br>
    
    <b style="color: red;">KARTOTEKA VALUTA</b> sve valute s kojima poslujemo
    <br>
    <br>
    
    <b style="color: red;">KARTOTEKA VRSTI PLAĆANJA</b>  - navodimo sve moguće vrste plaćanja. Isto je neuredna, bez redoslijeda i mogla bi se urednije složiti
    <br>
    <br>
    <b style="color: red;">CARINSKE TARIFE</b> - mi nemamo niti jednu navedenu, a mogli bi samo treba vidjeti gdje se taj podatak povlači i kad je potreban
    <br>
    <br>
    <b style="color: red;">KARTOTEKA CIJENIKA DOBAVLJAČA</b> - želja ovoga je da našu šifru pretvara u sifru dodbavljača i da šifru kupca pretvori u našu šifru. Pojasnjenje: š001 vobler zvijezda , zapravo je kod kupca <b>Š7894678</b> viseća reklama star ikad kupac nama posalje tu svoju šifru i taj naziv sofrware bi to odmah trebao pretvoriti u našu šifru i naziv. Tako i za dobavljača. Imamo <b>Š0012</b>  pionirski plavi 250 g papir, a kod dobavljača je to <b>Š75974</b> midnight blue sky alian free 250 g
    <br>
    <br>

    <b style="color: red;">KARTOTEKE ODJELA</b>- ne znam njihovu primjenu, treba se istražiti
    <br>
    <br>

    <b style="color: red;">KARTOTEKA NAČINA ISPORUKE</b> - kao sto ime kaže, definira način isporuke i u popriličnom je neredu. Trebala bi se urediti da se lakse koristi.
    <br>
    <br>
    <b style="color: red;">KARTOTEKA OBJEKATA</b> - mi je ne koristimo , a pitala sam ih pa su objasnili ako imamo nekoliko radnih jedinica pa po njima pratimo radnje, dokumente i kostanja. Recimo imamo 3 gradilista i onda svakom gradilištu prododajemo dokumente koji idu tom gradilištu.
    <br>
    <br>
    
    <b style="color: red;">ZEMLJA PORIJEKLA</b> - ime kaze
    <br>
    <br>
    
    <b style="color: red;">KARTOTEKA ALATA</b> - baza svih alata koje smo ikad imali
    <br>
    <br>
    
    <b style="color: red;">KARTOTEKA RADNIH OPERACIJA</b> - nikad nismo koristili, a kako znamo kako sloningu izgleda RN onda mislim da neme svrhe ni koristiti.
    <br>
    <br>
  
  </div>
  

  <h3>Prozori i Info </h3> 
  
  <div class="vel_info_text">
    <b style="color: #4d75b4;">PROZORI i INFO</b> staviti ću ih ih skupa i pojasniti jer su bas korisni, a vidim da se od vas nitko ziv s tim ne koristi. Ovaj dio svakome preporučujem da vidi jer stvarno vam daje super mogućnosti. Svi se natezu s prozorima, pa ih slazu, smjestaju, ali ovdje vam se sve jednim klikom preslozi u zeljenom smjeru  
  </div>  
  
  Ovo je su moji prozor u ovom trenutku...
  <br>
  <img src="/img/info/slon_4.png" alt=""  style="max-width: 70%; min-width: 300px;" />
  <br>
  <br>
  Prozori --> <b style="color: red;">KASKADE</b> pretvara ih u ovo:
  <br>
  <img src="/img/info/slon_5.png" alt=""  style="max-width: 70%; min-width: 300px;" />
  <br>
  <br>
  Prozori --> <b style="color: red;">IZNAD/ PORED</b> pretvara ih u ovo: 
  <br>
  <img src="/img/info/slon_6.png" alt=""  style="max-width: 70%; min-width: 300px;" /> 
  <br>
  <br>
  
  A <b style="color: red;">ZATVARANJE SVIH PROZORA</b> - sve zatvori odjednom i ostavi vas u SLONINGU
  <br>
  <br>
  
  Ako pak ne zelite da se prozori spremaju po ovim ponuđenim opcijama, onda ih vi rasporedite i kliknite <b style="color: red;">SPREMANJE SVOJSTAVA PROZORA</b>.
  <br>
  <br>
  
  <div class="vel_info_text">  
    To takodjer postizemo kad si poslozimo stupce koje zelimo gledati svaki put kad otvorimo dokument. Recimo kod upisa u <b>KARTOTEKU</b> stupce koje nikad ne koristite umjesto da ih svaki put preskacete smo ih stavite na kraj i to tako spremite.
    Recimo u <b>KARTOTECI ALATA</b> vidim da je <b>VEZANI ALAT</b> skoro na kraju i prije njega ima puno praznih polja koji se ne unose. 
    <br>
    Samo si premjestite Vezane alate blize i kliknete na <b>SPREMANJE SVOJSTAVA PROZORA</b> i slijedeći put kad udjete u kartoteku, taj stupac ce biti na mjestu na kojem ste ga zadji put ostavili. Ili recimo stupac vam je preuzak i svaki put sirite da vidite sta pise u njemu. Rasirite jednom na zeljenu dimeziju i kliknete na SPREMANJE SVOJSTAVA PROZORA i svaki put ce stajati tako bez bespotrebnog širenja svaki put.
  </div>
  
  <h3>Info Setup</h3> 
  U <b style="color: #4d75b4;">INFO – SUTUP</b> ima dosta vama bitnih podešavanja koja neću dodatno pojašnjavati, smo ću neka najbitnija označiti žuto pa obratite pažnju
  <br>
  <br>
  
  <img src="/img/info/slon_7.png" alt=""  style="max-width: 40%; min-width: 300px;" /> 
  <br>
  <img src="/img/info/slon_8.png" alt=""  style="max-width: 40%; min-width: 300px;" /> 
  <br>
  <br>
  Ako ima netko da mu uvijek pokazuje stare dokumente, pa uvijek mora povlačiti kursor da dođe do zadnjih onda nije uključio ovu opciju:
  <br>
  <img src="/img/info/slon_9.png" alt=""  style="max-width: 40%; min-width: 300px;" /> 
  <br>
  <br>
  Ako vam u <b>PONUDAMA</b>, <b>OTPREMNICI</b>, <b>PRIMKI</b>, <b>UTROSKU</b> ili bilo kom dokumentu ne izbacuje svaki put kad otvorite dokument Skladište Poštanska 7 onda morate spremiti u žuto označeno polje <b>SKL POŠTAN</b> ( kopirajte odavde i ne zaboravite, nećete moći unijeti promjeni jer niste stisnuli <b>PROMJENA</b>:
  <br>
  <img src="/img/info/slon_10.png" alt=""  style="max-width: 40%; min-width: 300px;" /> 
  <br>
  <br>
  Ne možete vidjeti stare dokumente u ovoj godini. Vjerojatno imate ovdje kvačicu:
  <br>
  <img src="/img/info/slon_11.png" alt=""  style="max-width: 40%; min-width: 300px;" /> 
  <img src="/img/info/slon_12.png" alt=""  style="max-width: 40%; min-width: 300px;" /> 
  <br>
  <br>
  E Sad dalje sto je isto bitno...<br>
  Prozori su vam razbacani, ne znate šta imate otvoreno...odite na <b>DOKUMENTI</b> i vidjeti će te, i možete odmah odabrati ono sto tražite:
  <br>
  <img src="/img/info/slon_13.png" alt=""  style="max-width: 40%; min-width: 300px;" /> 
  
  <br>
  
  
<div class="vel_info_text"> 
  
  Dalje....<br>
  U svkom dokumentu na dnu s lijeve i desne strane nalaze se ovakve strelice  <img src="/img/info/slon_14.png" alt=""  style="width: 40px; height: auto; display: inline; padding: 0" />
  one šire i skupljaju dokument koji gledate i pomiču ga na lijevu odnosno desnu stranu koju gledate. To rade sve strelice na koje naiđete zuto/žute, plavo/plave ili plavo zute a da su pozicionirane u tom smjeru ( lijevo-desno)
  <br><br>
  Ove strelice daju dodatne opcije svakom dokumetu <img src="/img/info/slon_15.png" alt=""  style="width: 40px; height: auto; display: inline; padding: 0" />. U nekim dokumentima otvaraju nova polja moćih filtera, a u nekima nova polja upisa opaske koja je biti vidljiva samo na tom dokumentu. Recimo moze se na otpremnicu napisati nekia napomena, kao doslo je takvo tih registarskih oznaka po robu ili robu preuzeo netko treci u ime kupca. Ovo rade sve strelice koje upucuju na smjer gore-dolje.
  <br><br>
  U Sloningu je dovoljno u tekućem mjesecu upisati samo dan i pritisnite ENTER i program će sam ispisati ostatak cijelog datuma.
  Npr. napisete u polje datuma 9 i pritisnite <b>ENTER</b> i ispisati ce se 9.tekuci mjesec.tekuća godina.

</div>

<br>
<br>

Svi dokumeti imaju ista ova polja. Objašnjavati ću svako polje s lijeva na desno. Odozgora prema dolje.
<br>
  
<img src="/img/info/slon_13.png" alt=""  style="max-width: 40%; min-width: 300px;" />   
<br>
<br>
 
<div class="vel_info_text"> 
 
 
  <b style="color: red;">PRIKAZ:</b> Ovisno u kojem se dokumentu nalazite prikazati će sve te dokumente poredane po redu, po datumu i broju. Ukoliko vam se pokazuju dokumenti od 1. prema zadnjem odnda morate u OPCIJE- SETUP ( vec ranije spominjali) promjeniti prikaz
  <br>
  <br>
  
  <b style="color: red;">DATUM KNJIŽENJA:</b> automatski se ispisuje kad otvorimo novi zapis. To je datum kad smo otvorili taj dokumet. Ukoliko se otvori novi zapis, njega se ne moze izbrisati. Ostavljate ga praznog ili ispravljete. Ako vema ne odgovara smao napustite dokument i otvorite novi. Ovome je bitno samo iznose staviti na 0 da ne remete izračune.
  <br>
  <br>
  
  <b style="color: red;">DATUM DOKUMETA:</b> popunjavamo sami i tu se stavlja datum kreiranja dokumenta
  <br>
  <br>
  
  <b style="color: red;">DATUM PLAĆANJA:</b> nije potrebno ispisati
  <br>
  <br>
  
  <b style="color: red;">ODOBRENJE</b>  ( poziv na broj) se negdje ispisuje automatski, negdje popunjava od strane korisnika. Molim napomenite koji odjeli na kojim dokumentima trebaju popuniti odobrenje
  <br>
  <br>
  
  <b style="color: red;">SPAJALICA:</b> sluzi za neku napomenu kad sljedeći put dođete u taj dokument. Sluzi i kao poruka onome tko ce kasnije koristiti taj dokument. Kada u spajalicu upisemo podatak ona se pretvori u spajalicu s listom.
  <br>
  <br>
  <b style="color: red;">?:</b> ne znam čemu sluzi
  <br>
  <br>
  
  <b style="color: red;">KONTROLA:</b> sluzi da u dokumetu u kojem jeste vam se azurira najnovije stanje skladista i odmah na licu mjesta provjerite da li nekog artikla ima ili nema.
  <br>
  <br>
  
  <b style="color: red;">KUPAC:</b> partner kojem upućujete dokument. Krenete sa početnim slovima imena i F4 i otvoriti će se lista subjekata koja su to početna slova ili ako ne znate točan početak nego se sjećate nečeg iz naziva krećete sa * podatak koji znate*F4 i ponuditi će vam se lista mogućih partnera
  <br>
  <br>
  
  <b style="color: red;">SKLADISTE:</b> u padajućem izborniku odaberete SKLADISTE POSTANKA 7, često nam se otvaraju neka druga stara skladiste i ne nudi nam se <b>SKL POŠTAN</b> ranije je objašnjeno kako se mijenja i memorira skladiste
  <br>
  <br>
  
  <b style="color: red;">IZBOR:</b> dodatni filter koji olakšava pretragu dokumenta u kojem se trenutno nalazimo.   Kasnije ću detaljnije objasniti.
  <br>
  <br>
  
  <b style="color: red;">BROJ:</b> pretraga dokumenta pro broju, ako zanamo broj dokumenta lako ćemo pronaći traženi dokument
  <br>
  <br>
  
  <b>TAB</b>: <b style="color: red;">PODACI O DOKUMENTU</b>  sadrzi glavne elemente dokumenta koje ćemo kasnije pojasniti
  <br>
  <br>
  
  <b>TAB</b>: <b style="color: red;">PREBACIVANJE U DOKUMENTE</b> omogućuje nam prebacivanje postojećeg dokumenta u neki drugi , ali i takodjer praćenje porijekla postojećeg dokumenta. Lako se mogu vidjeti " majka i djeca" nekog dokumenta.
  <br>
  <br>
  
  <b>TAB</b>: <b style="color: red;">DEVIZNI OBRAČUN</b> omogućuje nam prekalkulaciju kuna u eure i obrnuto
  <br>
  <br>
  <b>TAB</b>: <b style="color: red;">PORIJEKLA I CJENICI</b> omogućuje  da za pojedinog kupca dogovorimo cijenu nekog proizvoda bez obzira na količinu i onda svaki put cijenu povlacimo iz tog dokumenta
  <br>
  <br>
  <b>TAB</b>: <b style="color: red;">ISPORUKA</b> omogućuje  ukoliko kupac ima vise mjesta isporuke koji su definirani u Kartoteci partnera da ih ovdje povučete.
  <br>
  <br>
  
<b>TAB</b>: <b style="color: red;">OPIS STAVKE</b> koristimo kao nadopuna univerzalnim šiframa, ono sto stavimo u opis pojaviti će se samo u tom dokumentu i samo na tom mjestu. To nije trajni podatak. Koristi se kao dodatno pojasnjenje koje samo u tom trenutku šaljes ili kolegi ili kupcu. Mi najčešće koristimo na univerzalnim šiframa kojima kroz opis dajemo dodatno pojasnjenje.  
<br>
<br>
 
U ponudama univerzalne šifre koristimo na jos ne definirani i ne naručeni proizvodima i onda ih pobliže opisujemo kroz TAB OPIS STAVKE
<br>
<br>
Ispod svega polje sa zvjezdicom omogućuje povlačenje artikla iz Kartoteke materijala. Kliknete u polje šifra artikla i utipkate šifru ako je znate, a ako ne onda <b>*</b> podatak koji znate*F5 ( npr.*smarti*F5 pokazati će sve šifre proizvoda kojima se u nazivu pojavljuje "smarti")

  
</div> 

 
<b style="color: red;">IZBOR</b> ovisi o vašoj umješnosti.
<br>
Ovo je filter koji omogućuje pretragu dokumenata po kojem ste odlučili pretraživati

<img src="/img/info/slon_17.png" alt=""  style="max-width: 30%; min-width: 300px;" />   

<br>
<br>
<div class="vel_info_text">
  Možete tako pretraživati po:
  <br>
  <br>

  Broju dokumenata tako da vam izbaci raspon od ovog do onog dokumenta. Po  vremenskom periodu knnjiženja, kreiranja ili roka isporuke. Po partneru, po skladistu, po referentu koji je radio na tom dokumentu( recimo da izbaci sve sto je radio taj referent za tog kupca), po odjelu ( spominjali ranije u kartotekama, po Objektu ( takodjer spominjali u kartotekama) po odobrenju ( vec ranije spominjali), po artiklu ( ako zelimo pretragu bas po nekom proizvodu).
  Tip stavke ne znam sta zanči.
  Poslovnu regiju isto ne upisujemo pa nam pretraga po tome ne  vazi. Kod nas se vrlo učestalo traži po <b>"Temeljem"</b> jer se u to polje upisuju podaci kao sto je mail ili neka druga veza koja omogućuje lakse povezivanje. Može se pretrazivati i po elementima koji imaju kvačicu, ali za to vem treba već dublje znanje vezana elemanat u Sloningu. Koliki vidim kolege najčešće koriste "Partner" i "Temeljem", ali to ne znači da se ne mogu početi koristiti i neka druga polje koja će nas lakse definirati.

</div>

     
  <b>TAB</b>: <b style="color: red;">PODACI O DOKUMENTU</b>
  <br>
  
  U <b style="color: red;"><u>svim Ponudama</u></b> TAB PODACI O DOKUMENTU izgleda ovako:
       
  <img src="/img/info/slon_18.png" alt=""  style="max-width: 40%; min-width: 300px;" />         
              
  <div class="vel_info_text">
   
    U <b>TEMELJEM</b> upisujemo neki element koji će ukazati na tog kupca. Najčešće je to mail. 
    <br>
    <br>
    
    <b>OD DATUMA</b> datum od kad želite da krene obračun roka plaćanja.<br>
    <br>
    <br>
    
    <b>VAL.PLAĆ</b>. valuta plaćanje povlači se automatski za tog subjekta ako u kartoteci partnera ima definiran rok plaćanja ili ga popunjavate ručno ovisno o tome koliki rok za plaćanje želite dati. 
    <br>
    <br>
    
    <b>NAČIN PLAĆANJA</b> bira se kao padajući izbornik, a podatke povlači iz <b>KARTOTEKE NAČINA PLAĆANJA</b> 
    <br>
    <br>
    
    <b>ROK</b> popunjavate samo ako znate točan datum kad ce nesto biti. Na nivou ponude nije preporučljivo pisati ovaj podatak. Radije ga ostavite praznog. <br>
    <br>
    
    <b>ISPORUKA</b>: ovdje je dobro opisati isporuku. Recimo "iporuka 6-10 radnih dana od dostavljene pripreme" ili "isporuka 3-5 radnih dana od uplate".
    <br>
    <br>
    
    <b>DAT. OTPREM.</b> Datum otpremnice ostavite prazno.
    <br>
    <br>
    
    <b>DAT. VALUTE</b> popuniti će se automatski ako imate unešenu <b>VAL.PLAĆANJA</b>.
    <br>
    <br>
    Software će izračunati koliko je to dana od početnog datuma
    <br>
    <br>
    <b>NAČIN ISPORUKE</b>: popunjava se po padajućem izborniku, a podatci se povlače iz <b>KARTOTEKE NAČINA ISPORUKE</b>
    <br>
    <br>
    
    <b>TROŠKOVI</b>: ne znam šta bi tu mogli upisati. Najčešće ne ispisujemo
    
  </div>    

            
                        
                                    
  U <u>Narudžbi kupca</u>:
 <br>
 <br>
 
  <img src="/img/info/slon_19.png" alt=""  style="max-width: 60%; min-width: 300px;" />  
  <br>
  <br>
  <div class="vel_info_text">
  
    Sve je isto samo sad pišemo točan <b>ROK</b>. 
    <br>
    <br>

    Baš datum koji je definiran. Moja preporuka je da prilikom prebacivanja u <b>LNK</b> stavite najraniji datum kad je to kupcu potrebno ili neki realan datum i to prebacite u RN i LNK, a kasnije kad kupcu budete slali Potvrdu narudžbe taj datum promjenite u najkasniji datum koji kupac treba. Ako se trebate s kupcem natezati oko datuma i on traži smanjenje uvijek si pravite lufta u odnosu na ono sto će izaći iz proizvodje u odnosu na ono što kupcu prikazujete. 
    <br>

    Morate moci ostaviti lufta ukoliko dođe do nekeih nepredviđenih radnji kao što je kvar stroja isl. da nemate problema neprestanog opravdavanja pred kupcem.
    <br>
    <br>

    <b>Br. OTPREMNICE</b> ovdje ne popunjavamo.
    <br>
    <br>

    U ostalim dokumentima Podaci o dokumentu nisu toliko bitni jer se koriste za internu komunikaciju. Ukoliko postoji dokument na kojem je bitno popratiti  i nadopisati.
    
  </div>
  
  
  <h3>TAB:PREBACIVANJE U DOKUMENTE:</h3>
  <br>
  <img src="/img/info/slon_20.png" alt=""  style="max-width: 60%; min-width: 300px;" />  
  <br>
  <br>
  Na primjer nalazite se u dokumentu <b>PONUDA</b> u koju je unešena točna šifra proizvoda i želite sad iz ponude napraviti <b>NARUDŽBU KUPCA</b> ili <b>RADNI NALOG</b>:
  <br>
  <img src="/img/info/slon_21.png" alt=""  style="max-width: 60%; min-width: 300px;" />  
  
  <div class="vel_info_text">
    <div class="no_col_break">
      Klikom odaberete u koji dokument želite prebaciti PONUDU i zatim stisnete crvene strlice. Program će vas pitati želite li kreirati iz ovog dokumenta taj dokument. Odgovorite potvrdno . U polju vezani dokument pojaviti ce se naziv odabranog dokumenta i uz njega broj. Dvoklikom na polje vezani dokument otvoriti će se traženi dokument.
      <br>
    
    </div>
    
    <div class="no_col_break">  
      U našem primjeru na slici iznad vidi se popunjeno polje Porijeklo dokumenta koje nam kaže iz kog dokumenta je nastao dokument koji trenutno koristite. Dvoklikom na taj podatak otvoriti će se traženi dokument.
      <br>
    </div>  
    
  </div>
  
  <h3>TAB:OPIS STAVKE:</h3>
  
  <img src="/img/info/slon_22.png" alt=""  style="max-width: 60%; min-width: 300px;" /> 
  
  <div class="vel_info_text">
    Recimo radite <b>PONUDU</b> za nekog kupca za artikal koji nemamo ušifriran u kartoteku.
    <br>

    Koristite iz kartoteke univerzalnu šifru pod nazivom <b>PROIZVOD</b> 1 i dodajete joj opis u <b>lijevo polje Opisa stavke</b>.
    <br>
    Tu jasno precizirate sve sto vam je kupac spomenuo i što želite vi kupcu napomenuti. Kad ste gotovi kliknite <b>SAVE</b>.
    <br>
    <br>
    Isto tako želite izradu uzorka pa vam je potreban RN. 
    <br>
    Navedete univerzalnu šifru naziva Izrada uzorka i popunite sve željene elemente u opis stavke.

  </div>
  
  <br>
  <br>
  

  <h2 id="storno_izlaza">Storno izlaza</h2>

  <h3>Što je storno izlaza i kada se upotrebljava?</h3>
  <div class="vel_info_text">
    STORNO IZLAZA – POVRATNICA OD KUPCA Kreira se u slučajevima kada se roba koja je isporučena kupcu tj. izašla je sa skladišta VRAĆA na skladište iz raznih razloga (povrat viška, povrat neodgovarajuće robe, povrat radi neodgovarajuće kvalitete, povrat paleta I sl.)
    <br>
    Poveznice povrata su NK/ OTP / RAČUN – kod kreiranja navesti podatke.
    <br>
    Na temelju dokumenta STORNO IZLAZA/POVRATNICA OD KUPCA ovjerenog na našem skladištu koje potvrđuje potpisom primitak navedene količine I ponovno ju zaprima na skladište, računovodstvo će izdati financijsko odobrenje (količina* cijena+ PDV) I financijski umanjiti terećenje kupca.
  </div> 
  
  <h3>Zašto nam je storno izlaza bitan?</h3>
  <div class="vel_info_text">
    Storno izlaza nam je bitan kako bi se kutije koje su putem proknjižene otpremnice skinute sa stanja ponovno vratile na stanje. Storno izlaza je vezan uz dokument uz koji je roba isporučena, a najčešće je to otpremnica.
    <br>
    Može se koristiti za povrat robe od strane kupca, ali i za povrat paleta koje su navedene na otpremnici.
    <br>
    Koristi se samo kod povrata kupaca. Za dobavljače, odnosno za robu zaprimljenu primkom, koristimo storno ulaza.
  </div> 

  <div class="vel_info_text">
   <div class="no_col_break">
      Situacija u kojoj se povrat vrši po isporučenoj robi (kad je kreirana otpremnica).
    </div> <!-- end no col break -->
  </div> 
  
  <h5>1) U tabu dokumenti odaberite storno izlaza</h5>
  <img src="/img/info/storno_1.png" alt=""  style="max-width: 70%; min-width: 300px;" />
    
  <h5>2) Odaberete novi</h5>
  <img src="/img/info/storno_2.png" alt=""  style="max-width: 70%; min-width: 300px;" />
  
  <h5>3) Pod kupac upišite velp i enter. </h5>
  <div class="vel_info_text" style="margin-top: 0; padding-top: 0;">
   <div class="no_col_break">
     
    Zatim kliknite na upitnik u desnom kutu. Otvorit će vam se sljedeći prozor i popunite ga na način da u prvi tab upišete OT, u drugi broj otpremnice, označite s kvačicama prve dvije tvrdnje i odaberite prebacivanje. 
     
   </div> <!-- end no col break -->
    
  </div> 
  
  <img src="/img/info/storno_3.png" alt=""  style="max-width: 70%; min-width: 300px;" />
  
  <h5>4) Prebacit će vam se sve što se nalazilo u toj otpremnici</h5>
  <div class="vel_info_text" style="margin-top: 0; padding-top: 0;">
   <div class="no_col_break">
    i ako ima više šifri, a kupac primjerice vraća samo jednu, obrišite ostatak. Pazite na pravo skladište, u odobrenje upišite broj otpremnice (možete i u tab br. Otpremnice).
    </div>
  </div> 
  
  <img src="/img/info/storno_4.png" alt=""  style="max-width: 70%; min-width: 300px;" />
  
  <h5>5) Storno izlaza je sad kreiran</h5>
  <div class="vel_info_text" style="margin-top: 0; padding-top: 0;">
   <div class="no_col_break">
      Printa se u dva primjerka, na oba stavljamo pečat i potpis i jedan dajemo kupcu, a drugi se vraća nama i odlazi u administraciju kod Marine kako bi se moglo napraviti Odobrenje.
    </div>
  </div>   
  
  <br>
  <br>
  <h3>UPUTE – OSNOVE SLONINGA ZA LAKŠE KORIŠTENJE I PROVJERU</h3>
  
  <h4>1. OSNOVE LNK I NK</h4>
  
  <div class="vel_info_text">
    Nakon što se roba unese u LNK, važno je obratiti pažnju na nekoliko stvari: odabrati način dostave koji mora biti jednak načinu dostave u NK. U NK također upisati točnu adresu za isporuku, kao i kontakt broj na koji vozači mogu kontaktirati kupca prije dostave.<br>
    <br>
    <b>Bez navedenih podataka dostava neće biti moguća.</b><br>
    <br>
    Ukoliko kupac ima posebne zahtjeve vezane uz pakiranje, način dostave (ako se primjerice radi o zgradi bez lifta), drugačije radno vrijeme od našeg – to obavezno naglasiti ili u NK ili u LNK pod napomenu.<br>
    <br>

    Ukoliko se radi o robi koju imamo na lageru, staviti status <b>"skladištari će mi javiti"</b> i ne mijenjati taj status. Ukoliko robu imamo na lageru, skladištari će upisati <b>"Rezervirano na skladištu"</b> ili ako nemamo cijelu količinu, upisat će status "Nema cijela količina na skladištu, mora se proizvoditi". U tom će slučaju skladištari u rubrici "koliko je odrađeno u proizvodnji" upisati "skl/količina", a za ostatak prodaja ili pušta radni nalog ili nabavi šalje narudžbu za preostalu količinu.<br>
    <br>
    Važno je da nitko ne mijenja i ne upisuje status "Rezervirano na skladištu" osim skladišta jer se taj status upisuje samo kad je količina u skladištu smanjena na temelju skladišnog naloga. Ukoliko netko drugi promijeni status u "rezervirano na skladištu", skladište će misliti da je ta količina već smanjena u skladištu i doći će do netočnog stanja u sloningu što nikada ne smijemo dopustiti.<br>
    <br>
  </div> 
  
  <h4>2. SLANJE SKLADIŠNOG NALOGA</h4>
  
  <div class="vel_info_text">
    Nakon što se u sloningu provjeri imamo li dovoljnu količinu određene robe na stanju, skladištu se šalje skladišni nalog ili broj narudžbe kupaca. Na temelju tog skladišnog naloga, skladištar iz sloninga smanjuje količinu navedenu na nalogu i upisuje status u LNK. Ukoliko sve što se nalazi u skladišnom nalogu imamo na lageru, odmah se izrađuje otpremnica i printaju se paletne kartice te skladištari slažu robu za isporuku. Nakon toga u LNK se upisuje broj otpremnice i "Spremno za isporuku", a prodaja kupcu može javiti da je roba spremna.
  </div>   

  <h4>3. PROVJERA STANJA U SLONINGU</h4>
  <div class="vel_info_text">
    Stanje skladišta – upisati šifru – enter – za kutije se točno stanje čita pod "napomenu", a za ploče pod "Stanje".<br>
    Ako sloning ne izbacuje robu koju tražimo po određenoj šifri, odznačiti kvačicu "samo artikli koje imamo na stanju" i još jednom kliknuti enter.<br>
  </div>

  <h4>4. SLANJE SPECIFIKACIJE ZA KURIRSKU SLUŽBU</h4>
  <div class="vel_info_text">
    Postoje dvije vrste kurirske službe – kad kupci sami organiziraju dostavu i kad mi organiziramo dostavu za njih. U slučaju kad mi organiziramo dostavu, skladište javlja specifikaciju paketa prodajnom referentu, a on javlja Ivanki da organizira dostavnu službu. Nakon toga Ivanka skladištu šalje adresnicu i skladište tu adresnicu lijepi na paket.<br>
    <br>
    U slučaju da kupac sam organizira dostavnu službu, skladište radi specifikaciju i automatski obavještava kupca o logističkim podacima, dok prodajnog referenta stavlja u cc.
    <br>
  </div>
   
  <h4>5. PROVJERA STATUSA ISPORUKE ROBE</h4>
  <div class="vel_info_text">
    Postoji više načina provjere statusa isporuke robe. Jedan je da je u LNK pod status navedeno "Isporučeno", a drugi je da se u sloningu upiše broj otpremnice i ukoliko piše da je dokument proknjižen, roba je isporučena.
  </div>
  
  
  
<!-- /////////////////////// KRAJ SLONING SEKCIJE /////////////////////// -->
</div>

<br>
<br>

  
  <h2 id="sustav_ovlaz_info">Upute za rad sa sustavom ovlaživanja</h2>

  <div class="vel_info_page">

    
    <h4>Sustav ovlaživanja sastoji se od 3 sustava:</h4>
    <br>
    

    <div class="vel_info_text ">

      <div class="no_col_break">
        <div style="font-size: 20px; text-align: center; font-weight: 700;">1. Omekšivač vode</div>
        <img src="/img/info/1_ovlaz.png" alt="" style="padding-bottom: 5px; padding-top: 5px;">
        
      </div> <!-- end no col break -->
     
      <div class="no_col_break">
        <div style="font-size: 20px; text-align: center; font-weight: 700;">2. Reverzibilna osmoza</div>
        <img src="/img/info/2_ovlaz.png" alt="" style="padding-bottom: 5px; padding-top: 5px;">      
        
        
      </div>
      
      <div class="no_col_break">
        <div style="font-size: 20px; text-align: center; font-weight: 700;">3. Pumpa sa sapnicama i vremenskim programatorom</div>
        <img src="/img/info/3_ovlaz.png" alt="" style="padding-bottom: 5px; padding-top: 5px;">      
        
        
      </div>

    </div>
    
    
    <div class="vel_info_text ">

       <div class="no_col_break">
       
        <img src="/img/info/4_ovlaz.png" alt="" style="padding-bottom: 5px; padding-top: 0;">
        <div style="font-size: 12px; text-align: center;">Slika 1. Spremnik demineralizirane i omekšane vode</div><br>
       <br>
        
      </div> <!-- end no col break --> 
     
     <div class="no_col_break">
       
        <img src="/img/info/5_ovlaz.png" alt="" style="padding-bottom: 5px; padding-top: 0;">
        <div style="font-size: 12px; text-align: center;">Slika 2. Sustav će u slučaju niske razine demineralizirane i omekšane vode na zaslonu javiti poruku <b>Low level</b></div><br>
        
      </div> <!-- end no col break --> 
     
     
      <div class="no_col_break">
              
        Kako bi sustav radio, potrebno je da u plavom spremniku vode postoji određena količina
        demineralizirane i omekšane vode. Ukoliko u spremniku nema dovoljna količina vode, sustav
        ovlaživanja neće raditi te će se na zaslonu izbornika reverzne osmoze pojaviti poruka:<br>
        <b>"Low level"</b>.<br>
        To je znak da je sustav potrebno napuniti s novom količinom demineralizirane i
        omekšane vode.
        
      </div>

    </div>
    
    
    <h4>Prije početka paljenja sustava potrebno je osigurati sljedeće:</h4>
    <br>
    <div class="vel_info_text ">
      
       <div class="no_col_break">
        <img src="/img/info/6_ovlaz.png" alt="" style="padding-bottom: 5px; padding-top: 0;">
      </div> <!-- end no col break --> 
      
      <div class="no_col_break">
        <img src="/img/info/7_ovlaz.png" alt="" style="padding-bottom: 5px; padding-top: 0;">
      </div> <!-- end no col break --> 
      
      <div class="no_col_break">
       
        <h4 style="margin-top: 0; color: black;">1. Da tank odvodne vode (tzv. kondenzata) nije prešao razinu od preko 500 L (polovica tanka)</h4>
        
        <h4 style="margin-top: 0; color: black;">2. Da su 2 crijeva (zelena i crna koji služe za odvoz kondenzata) pričvršćena pvc vezicom na tank</h4>
        
        
      </div>
    </div>
    
    
    <div class="vel_info_text ">
      <div class="no_col_break">
        <img src="/img/info/8_ovlaz.png" alt="" style="padding-bottom: 5px; padding-top: 0;">
        <div style="font-size: 12px; text-align: center;">Slika 3. Vertikalni (otvoreni) položaj ventila</div><br>
      </div> <!-- end no col break --> 
      <div class="no_col_break">
        Prilikom paljenja sustava potrebno je otvoriti ventil za dovod vode (iz horizontalnog u vertikalni položaj) kako bi se pustio protok vode u sustav
      </div>
    </div>
    
    
    
    
    <div class="vel_info_text ">
      <div class="no_col_break">
        <img src="/img/info/9_ovlaz.png" alt="" style="padding-bottom: 5px; padding-top: 0;">
        <div style="font-size: 12px; text-align: center;">Slika 4. izbornik omekšivača vode - vrijeme</div><br>
      </div> <!-- end no col break --> 
      
      <div class="no_col_break">
        <img src="/img/info/10_ovlaz.png" alt="" style="padding-bottom: 5px; padding-top: 0;">
        <div style="font-size: 12px; text-align: center;">Slika 5. izbornik omekšivača vode – količina omekšane vode</div><br>
      </div> <!-- end no col break --> 
      
      
      <div class="no_col_break">
        Na izborniku omekšivača vode (slika 4. i slika 5.) izmjenjuju se dva podatka:
        <ul>
          <li>vrijeme</li>
          <li>količina omekšane vode (maksimalna količina omekšane vode je 1280)</li>
        </ul>
      </div>
    </div>
    
    
    
    <div class="vel_info_text ">
      <div class="no_col_break">
        <img src="/img/info/11_ovlaz.png" alt="" style="padding-bottom: 5px; padding-top: 0;">
        <div style="font-size: 12px; text-align: center;">Slika 6. tipka za početak postupa omekšavanja vode</div><br>
      </div> <!-- end no col break --> 
      <div class="no_col_break">
        Prije pokretanja postupka reverzne osmoze potrebno je osigurati da količina omekšane vode ne bude
        ispod vrijednosti 600 (u prosjeku za punjenje punog plavog spremnika potroši se cca 600 količine
        omekšane vode).<br>
        Postupak omekšavanja vode pokreće se pritiskom tipke na izborniku otprilike 5 sekundi (zeleno označeno na slici 6.).
      </div>
    </div>
    
    <div class="vel_info_text ">
      <div class="no_col_break">
        <img src="/img/info/12_ovlaz.png" alt="" style="padding-bottom: 5px; padding-top: 0;">
        <div style="font-size: 12px; text-align: center;">Slika 7. unutrašnjost spremnika za omekšavanje vode</div><br>
      </div> <!-- end no col break --> 
      <div class="no_col_break">
        Postupak omekšavanja vode traje oko 1h i 10 minuta. Potrebno je osigurati da je tank IBC kontejnera minimalno poluprazan te da se u spremniku 
        omekšivača vode nalazi dovoljno tabletirane soli (slika 7.). Kada nema soli u spremniku sustav ne može omekšavati vodu. Voda koja se nalazi u spremniku
        uvijek može primiti samo određenu količinu soli pa se ne treba brinuti o tome da li je stavljeno previše soli.
      </div>
    </div>
    
    
    <div class="vel_info_text ">
      <div class="no_col_break">
        <img src="/img/info/13_ovlaz.png" alt="" style="padding-bottom: 5px; padding-top: 0;">
        <div style="font-size: 12px; text-align: center;">Slika 8. Izbornik reverzne osmoze</div><br>
      </div> <!-- end no col break --> 
      <div class="no_col_break">
                Nakon što imamo dovoljno omekšane vode potrebno je na izborniku reverzne osmoze <b>dva puta</b>
        stisnuti tipku <i class="fas fa-level-down" style="transform: rotate(90deg); font-size: 20px; margin: 0 10px; position: relative; top: 5px;"></i> (zeleno označeno na slici 8.) te <b>jednom</b> tipku <b style="font-size: 20px;">ESC</b> (crveno označeno na slici 8.). Nakon  toga pojavljuje se poruka <b>"Out Conductivity"</b>. Sustav se počne puniti demineraliziranom i omekšanom
        vodom. Nakon što plavi spremnik dosegne kritičnu visinu popunjenosti sustav se sam gasi kako se voda  ne bi prelijevala.
      </div>
    </div>
    
    
    <div class="vel_info_text ">
      <div class="no_col_break">
        <img src="/img/info/14_ovlaz.png" alt="" style="padding-bottom: 5px; padding-top: 0;">
        <div style="font-size: 12px; text-align: center;">Slika 9. paljenje i gašenje pumpe</div><br>
      </div> <!-- end no col break --> 
      <div class="no_col_break">
        Mlaznice se pale na način da se prvo upali pumpa pritiskom na tipku <b style="font-size: 20px;">ON/OFF</b>
        <br>
        <br>
        Zatim se okreće gumb u desnu stranu na poziciju <b style="font-size: 20px;">AUTO</b> (slika 10. zeleno označena tipka). 
        <br>
        Nakon toga sustav mlaznica se pokreće u programiranom intervalu. 
      </div>
    </div>
    
    <div class="vel_info_text ">
      <div class="no_col_break">
        <img src="/img/info/15_ovlaz.png" alt="" style="padding-bottom: 5px; padding-top: 0;">
        <div style="font-size: 12px; text-align: center;">Slika 10. sustav paljenja mlaznica</div><br>
      </div> <!-- end no col break --> 
      <div class="no_col_break">
        Sustav ovlaživanja se gasi vraćanjem tipke u položaj <b style="font-size: 20px;">0</b> (slika 10. zeleno označena tipka) te gašenje pumpe na tipku
        <b style="font-size: 20px;">ON/OFF</b> (slika 9.).
      </div>
    </div>
    
    
    <h3 style="color: red;">VAŽNE NAPOMENE:</h3>
    
    <div class="vel_info_text ">
      <div class="no_col_break">
        <img src="/img/info/16_ovlaz.png" alt="" style="padding-bottom: 5px; padding-top: 0;">
        <div style="font-size: 12px; text-align: center;">Slika 11. Higrometar</div><br>
      </div> <!-- end no col break --> 
      <div class="no_col_break">
        <ol>
          <li>U bilo kojem slučaju nakon omekšavanja vode i nakon postupka punjenja plavog spremnika potrebno je zavrnuti ventil dovoda vode kako bi se minimalizirala mogućnost nastanka poplave.</li>
          <li>Nakon što nestane vode u plavom spremniku prestat će raditi sustav međutim pumpa će i dalje raditi <b style="color: red;">i zagrijavati se što može dovesti do njenog smanjenog životnog vijeka. Kada nestane vode u plavom spremniku obavezno je potrebno ugasiti pumpu kao i mlaznice (slika 10.)</b></li>
          <li>Sustav posjeduje i higrometar koji mjeri vlažnost zraka te se prema njemu može podesiti rad sustava (slika 11.)</li>
        </ol>
      </div>
    </div>

  </div> <!-- end info page  KRAJ OVLAŽIVANJA -->
  
  
  

  <h2 id="anchor_info_razno">Razne napomene</h2>

  <div class="vel_info_page">

    

    <h3>Problemi u proizvodnji</h3>

    <div class="vel_info_text">

     <div class="no_col_break">
        <img src="/img/info/IMG-20200625-WA0005.jpg" alt="">
      </div> <!-- end no col break -->
     
      <div class="no_col_break">
        <h4 style="margin-top: 0;">Problem oksidacije foliotiska na crnom materijalu</h4>
        Neki crni papiri nemaju neki zaštitni pigment za zlatotisak (zlatni foliotisak).
        Jedini papir koji garantira da je dobar za zlatotisak je Sirio ultra black 460g.
        Za sve druge crne papire ne garantira se da su dobri za foliotisak.
        <br>
        
        <h4>Korona naboj</h4>
        Polipropilen zbog male površinske energije (nepolarne i kemijski inertne površine) na sebe loše prima tisak i ljepilo, zbog tog razloga mora se tretirati Koronom.<br>
        Korona izboj pri atmosferskom tlaku jedan je od načina za znatno povećanje površinske energije takvih i sličnih podloga.<br>
        Korona postupak se može ponavljati.<br>        
        
      </div>

      <div class="no_col_break">
        <b>
          Na polipropilenu Korona sloj traje od 6 mjeseci do godinu dana nakon čega velika je vjerojatnost da će se tisak ljuštiti, a ljepilo odljepljivati.<br>
        </b>
        <br>
      </div> <!-- end no col break -->

      Postoje i ljepila koja drže baš taj ne tretirani poliproplen, ali mi ga u radu ne koristimo.<br>

      Prilikom naručivanja PP ploča potrebno je provjeriti koliko dugo stoje na njihovom skladištu i voditi računa da ni kod nas ne stoje dugo na skladištu. Polipropilen koji je prvi ušao mora i prvi izaći i o tome se strogo mora voditi računa.<br>

      Za provjeru Korona naboja koriste se flomasteri koji se nalaze u frižideru na katu. Na način kakav trag ostavljaju, takav je Korona naboj.<br>

    </div>

    
   
    <h3>Razne upute za rad</h3>

    <div class="vel_info_text">
      
        <div class="no_col_break">
        
          <a class="video_link" href="https://youtu.be/dDy23py4tqE" target="_blank" style="text-align: center; width: 100%;">
            <i class="far fa-play-circle"></i>&nbsp;Kako promijeniti nož skalpela
          </a>
          <br>
          <a class="video_link" href="https://youtu.be/RqCyL0a0ezo" target="_blank" style="text-align: center; width: 100%;">
            <i class="far fa-play-circle"></i>&nbsp;Kako upaliti kompresor
          </a>
          <br>
          <a class="video_link" href="https://youtu.be/miLvzO4O754" target="_blank" style="text-align: center; width: 100%;">
            <i class="far fa-play-circle"></i>&nbsp;Odlaganje isplotanih elemenata na paletu
          </a>
          <br>
          <a class="video_link" href="https://youtu.be/3OO46ta_5D0" target="_blank" style="text-align: center; width: 100%;">
            <i class="far fa-play-circle"></i>&nbsp;Kako se spremaju alati
          </a>
          
        </div> <!-- end no col break -->
        
        <br>
        <h4>Lozinka za 7z enkritprirane datoteke</h4>
        wfCKNOpdMHkSFbgUF0Vly73GYBlZPyw8
        
      
    </div> <!-- end info text --> 



    <h3>Klik i navojni vijci</h3>

    <div class="vel_info_text">

      <div class="no_col_break">
        U radu koristimo sljedeće PVC vijke:<br>
        <br>

        <b>Klik PVC vijci:</b><br>
        - 10 mm<br>
        - 20 mm<br>
        - 35 mm<br>
        <br>
        <b>Navojni PVC vijci:</b><br>
        - 12 mm<br>
        - 16 mm<br>
        - 25 mm<br>

      </div> <!-- end no col break -->

      <div class="no_col_break">
        <br>
        Vijci se pakiraju u ZIP vrećice, pakiranje vrši Nada prema uputi poslovođa. Ukoliko je potrebno dostaviti vijaka da se nalaze kod nas na skladištu, onda je tu informaciju potrebno javiti poslovođama da od Nade dostave tražene vijeke.
        <br>

      </div> <!-- end no col break -->



    </div> <!-- end info text -->


    <h3>Aqua Slotter boje Tip 04 (Chromos)</h3>

    <h4 style="color: red; font-size: 12px;">
      * Zbog loše kvalitete slikanih uzoraka i različitih prikaza na različitim ekranima,<br>
      boje mogu znatno odstupati od stvarnih boja.
    </h4>


    <div style="display: flex; align-items: flex-end; flex-wrap: wrap; justify-content: center;">
      <img src="/img/info/Chromos_boje_1.jpg" alt="" class="image_float" style="width: 25%;">
      <img src="/img/info/Chromos_boje_11.jpg" alt="" class="image_float" style="width: 25%;">
      <img src="/img/info/Chromos_boje_2.jpg" alt="" class="image_float" style="width: 25%;">
    </div>

    <div style="display: flex; align-items: flex-end; flex-wrap: wrap; justify-content: center;">
      <img src="/img/info/Chromos_boje_21.jpg" alt="" class="image_float" style="width: 25%;">
      <img src="/img/info/Chromos_boje_3.jpg" alt="" class="image_float" style="width: 25%;">
      <img src="/img/info/Chromos_boje_31.jpg" alt="" class="image_float" style="width: 25%;">
    </div>




  </div> <!-- end info page -->


  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <!--<script src="js/scripts.js"></script>-->

</div>
 

`;

  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
    console.log(`history length UNUTAR INFO PLACE COMPONENT --------------->`, history.length);
  };
  
  wait_for( `$('#${data.id}').length > 0`, function() {
    
    window.change_hash = false;
    
    
    this_module.scroll_to_anchor(anchor);
    
    console.log(data.id + 'component injected into html');
    
    //if ( STORE.find(data.id, `all`) == null ) STORE.make(data.id, data);

    $(`#cit_page_title h3`).text(`Info`);
    document.title = "INFO VELPROM";
    
    
    var on_change = function() { 
      console.log(`promijeno sam data unutar ${data.id} `);
      // this_module.create( new_data, $(`#cit_root`) );      
      this_module.data_to_html(STORE.find(data.id, 'all' ));
    };
    
    // var component_listener_id = store_listener( data.id, 'all', on_change, data.id);

    /*    
    $(`#${data.id} input`).off('keyup');
    $(`#${data.id} input`).on('keyup', function() {
      var prop = $(this).attr('data-prop');
      var input_value = $(this).val();
      STORE.update(data.id, prop, input_value, `promjena input value ${prop} unutar #${data.id} componente` );
    });
    */
    
    toggle_global_progress_bar(true);
    
    $(document).off( 'scroll.info_js' );
    $(document).on( 'scroll.info_js', function(){
      
      if (
        window.change_hash             ==  true          &&
        $(`#info_module`).length       >   0             &&
        window.location.hash           !== `#info/null`
        ) {
        
        
        window.location.hash = `#info/null`;
        
        
      }
    });
    
    
    var last_find_time = 0;
    var curr_result_index = 0;
    var result_count = 0;
    var new_search = true;
      
    function goto_result(index) {

      window.change_hash = false;
      
      result_count = $(`span.shiny`).length;
      
      $(`span.shiny`).removeClass(`cit_focus`);

      $(`span.shiny`).eq(index).addClass(`cit_focus`);

      var scroll_top = $(`span.shiny`).eq(index).offset().top /* + $(`html`)[0].scrollTop */ - 200;
      $(`html`)[0].scrollTop = scroll_top;
      setTimeout( function() {  window.change_hash = true  }, 200);
      
      $(`#cit_find_count`).text( (index+1) + `/` + result_count );
      

    };  

    
    $(`#cit_find_all_btn`).off('click');
    $(`#cit_find_all_btn`).on('click', function() {
      
      
      if ( $(`#cit_find_all`).val() == "" ) return;
      
      new_search = true;
    
      if ( window.info_finder) window.info_finder.revert();

      let reg_1 = new RegExp( $(`#cit_find_all`).val(), 'ig');

      window.info_finder = findAndReplaceDOMText(document.getElementById('info_module'), {
       find: reg_1,
       wrap: 'span',
       wrapClass: 'shiny'
      });
      
      /*
      
      var no_cro_letters = $(`#cit_find_all`).val();
      no_cro_letters = no_cro_letters.replace(/č/ig, "c");
      no_cro_letters = no_cro_letters.replace(/ć/ig, "c");
      no_cro_letters = no_cro_letters.replace(/š/ig, "s");
      no_cro_letters = no_cro_letters.replace(/ž/ig, "z");
      no_cro_letters = no_cro_letters.replace(/đ/ig, "d");
      
      let reg_2 = new RegExp( no_cro_letters , 'ig');
      window.info_finder = findAndReplaceDOMText(document.getElementById('info_module'), {
       find: reg_2,
       wrap: 'span',
       wrapClass: 'shiny'
      });
      
      */
    
      result_count = $(`span.shiny`).length;
      
      if ( result_count > 0 ) {
        curr_result_index = 0;
        goto_result(curr_result_index);
      } else {
        $(`#cit_find_count`).text( `0/0` );
      };
      
    });
    
    
    
    $(`#cit_find_prev`).off('click');
    $(`#cit_find_prev`).on('click', function() {
      new_search = false;
      if ( curr_result_index == 0 ) {
        curr_result_index = $(`span.shiny`).length - 1;
      } else {
        curr_result_index -= 1;
      };
      
      
      goto_result(curr_result_index);
      
    });
    
    
    
    $(`#cit_find_next`).off('click');
    $(`#cit_find_next`).on('click', function() {
      new_search = false;
      if ( curr_result_index == $(`span.shiny`).length - 1 ) {
        curr_result_index = 0;
      } else {
        curr_result_index += 1;
      };
      
      goto_result(curr_result_index);
      
    });    
    

    $('#cit_find_all').off('keypress');
    $("#cit_find_all").on('keypress', function (e) {
      
      
      
      if (e.keyCode === 13) {
        
        if ( new_search == true ) {
          $(`#cit_find_all_btn`).trigger(`click`);
          new_search = false;
        } else {
           $(`#cit_find_next`).trigger('click');
        };
        
      } else {
        
        new_search = true;
        
      };
      
    });


    
  }, 500*1000 );

  return {
    html: component_html,
    id: data.id
  };

},
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};

  
  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
  };
  this_module.set_init_data = set_init_data;
  
  
  function scroll_to_anchor(anchor) {
    
    // stavim ovu varijablu na true da spriječim trigeriranje promjene hasha na scroll
    window.change_hash = false;
    
    var all_img_count = $(`#cont_main_box img`).length;
    var loaded_image_count = 0;
    
    var img_src_array = [];
    
    // uzmi sve src-ove od svih slika
    $(`#cont_main_box img`).each( function() {
      img_src_array.push(this.src);  
    });
    
    // console.log(img_src_array);
    
    // uzmi sve slike iz imag cache containera
    // to je div koji sam sakrio i služi mi samo da 
    // postavi sve slike unutara kako bi se mogle loadati
    var image_cache_count = $(`#image_cache_container img`).length;
    
    // ako broj slika u containeru za cache slika nije isti kao broj SRC-ova u arraju
    if ( image_cache_count !== img_src_array ) {
      
      var all_cached_img_html = "";
      // kreiraj sve img tagove 
      $.each(img_src_array, function(img_ind, img_src) {
        all_cached_img_html += `<img src="${img_src}" alt="" loading="eager">`;
      });
      // postavi sve img u cache container
      $(`#image_cache_container`).html(all_cached_img_html);
    };
    
    // napravilistenere za svaku sliku
    // i obriši iz arraja src od te slike kada se slika loada
    $(`#image_cache_container img`).each( function() {

      var this_image = this;
      
      
      // ako je slika dobila svoje dimenzije to znači da je loadana
      function image_loaded_check(this_image) {
        return function() {
          var image_loaded = false;
          if ( this_image.naturalHeight > 0 && this_image.naturalWidth > 0 ) {
            image_loaded = true;
          };
          return image_loaded;
        };
      };
      
      wait_for( image_loaded_check(this_image), function() {

        
        // ako je slika loadana onda ju obriši iz arraja
        var delete_index = null;
        $.each(img_src_array, function(ind, src) {
          if ( src == this_image.src ) {
            delete_index = ind;
          };
        });
        
        if ( delete_index !== null ) {
          img_src_array.splice(delete_index, 1);
          // console.log(`img array length:  `, img_src_array.length);
        };
        
      }, 6*1000);
      
      
      /*
      $(this_image).off('load');
      $(this_image).on('load', function() {
        
        var delete_index = null;
        $.each(img_src_array, function(ind, src) {
          if ( src == this_image.src ) {
            delete_index = ind;
          };
        });
        
        if ( delete_index !== null ) {
          img_src_array.splice(delete_index, 1);
          console.log(`img array length:  `, img_src_array.length);
        };
          
        // loaded_image_count += 1;
        // console.log(loaded_image_count);
      });
      */
      
      

      // važno ---- svaki put moram obrisati src i ponovo ga ubaciti
      // jer inače load event neće biti trigeriran
      
      // VAZNO !!!!! - kako bih izbjegao da svaki put brisem i postavljam src ponovo 
      // napravio sam container koji ima nula visinu i sirinu 
      // i u njega sam stavio sve slike i taj container zapravo koristim kao cache
      
      /*
      var image_src = $(this)[0].src;
      $(this)[0].src = "";
      $(this)[0].src = image_src;
      */

    });

    
    // sada provjeri jel array svih SRC-ova prazan
    let check_all_loaded = function() {
      let images_are_loaded = false;
      if ( img_src_array.length == 0 ) {
       images_are_loaded = true;
      };
      return images_are_loaded;
    };

    // postavi scroll na nula
    // $(`html`)[0].scrollTop = 0;
    
    toggle_global_progress_bar(true);
    
    var scroll_for_top_menu = 100;
    
    // ako je na mobitelu onda spusti scroll top menu
    if ( $(window).width() < 600 ) scroll_for_top_menu = 160;
    
    wait_for( check_all_loaded, function() {

      var scroll_top = 0;
      if ( anchor ) scroll_top = $(`#`+anchor).offset().top  /* + $(`html`)[0].scrollTop */ - scroll_for_top_menu;
      $(`html`)[0].scrollTop = scroll_top;
      setTimeout( function() {  toggle_global_progress_bar(false); window.change_hash = true  }, 200);

    }, 
    5*1000,
    function () {
      
      // console.log(img_src_array);
      // čekam 5 sekundi i onda bez obzira jesu li slike loadane ili ne  - prikazujem info page !!!
      var scroll_top = 0;
      if ( anchor ) scroll_top = $(`#`+anchor).offset().top /* + $(`html`)[0].scrollTop */ - scroll_for_top_menu;
      $(`html`)[0].scrollTop = scroll_top;
      setTimeout( function() {  toggle_global_progress_bar(false); window.change_hash = true  }, 200);

    });
   
    
  };
  this_module.scroll_to_anchor = scroll_to_anchor;
  
  
  this_module.cit_loaded = true;
 
} // end of module scripts
};

