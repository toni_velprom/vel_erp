var module_object = {
  
create: function( data, parent_element, placement ) {

  
  var { id } = data;
  
  this_module.set_init_data(data);
  var rand_id = `docpreview`+cit_rand();
  this_module.cit_data[id].rand_id = rand_id;
  
  
  var component_id = data.id;

  var w = window;


  var component_html =
`

<div id="${data.id}" class="cit_comp">
   
  <div id="generate_docs_buttons_box">
    <div class="preview_buttons" id="close_cit_preview"><i class="fal fa-times"></i></div>
    <div class="preview_buttons" id="print_cit_preview"><i class="fal fa-print"></i></div>
    <div class="preview_buttons" id="gen_pdf" style="background-color: #ba0909; color: #fff; font-size: 11px;">PDF</div>
    <div class="preview_buttons" id="make_record_btn" style="background-color: #3f6ad8; color: #fff; font-size: 11px;">
      <i class="fas fa-check"></i>
    </div>
    
    
  </div>

  <div id="printContainerIFRAME" class="${ data.orient == 'landscape' ? 'cit_landscape' : '' }">
    <div id="temp_preview_box"></div>
    <iframe id="printiFrame" frameborder="0" scrolling="no" name="printiFrame">
      <!-- OVDJE UBACUJEM GENERIRANI DOC ZA PREVIEW -->
      
    </iframe>
  </div>

  <div id="measurement" style="width: 100mm;
            height: 100mm;
            pointer-events: none;
            opacity: 0;
            position: absolute;
            z-index: 99;
            top: 0;
            left: 0;"></div>

</div>

`;


  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };


  wait_for( `$('#cit_preview_modal').length > 0`, function() {

    $(`#close_cit_preview`).off(`click`);
    $(`#close_cit_preview`).on(`click`, function(e) {
      
      e.preventDefault();
      e.stopPropagation();
           
      // id je uvijek isti ( cit_preview_modal) pa odmah znam property zato što je UVIJEK JEDNA JEDINA INSTANCA !!!
      var data = this_module.cit_data.cit_preview_modal;

      if ( data.doc_type == `radni_nalog` ) {
        
        // data je u ovom slučaju DATA FOR DOC koji sam predao ovom modulu
        if ( data.pro_kalk?.kalk_sifra )  {
          
          
          var switch_elem = $(`#` + data.pro_kalk.kalk_sifra + `_kalk_radni_nalog` )[0];
          
          var kalk_comp_id = $(switch_elem).closest('.kalk_comp')[0].id;
          var kalk_data = this_module.kalk_module.cit_data[kalk_comp_id];
          var rand_id =  this_module.kalk_module.cit_data[kalk_comp_id].rand_id;

          var product_id = $(switch_elem).closest('.product_comp')[0].id;
          var product_data =  this_module.product_module.cit_data[product_id];

          
          var kalk_box = $(switch_elem).closest('.kalk_box');
          var kalk_type = kalk_box.attr(`data-kalk_type`);
          var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);

          var curr_kalk = this_module.kalk_module.kalk_search(kalk_data, kalk_type, kalk_sifra, null, null, null, null );
          
          
          // VRATI NAZAD DA RADNI NALOG BUDE FALSE u podacima
          curr_kalk.kalk_radni_nalog = false;
          // VRATI SWITCH U HTML-u na false
          $(`#` + data.pro_kalk.kalk_sifra + `_kalk_radni_nalog` ).removeClass(`on`).addClass(`off`);
          
          
          
          // obriši post kalk !!!! AKO JE USER ODUSTAO !!!!!!
          kalk_data.post_kalk = [];
          
          
        }; // kraj jel imam pro_kalk sifru u data for doc !!!
        
        
      }; // kraj jel radni nalog 
      
      
      $(`#cit_preview_modal`).removeClass(`cit_show`);
      
    });
    
    
    $(`#print_cit_preview`).off(`click`);
    $(`#print_cit_preview`).on(`click`, function(e) {
      
      e.preventDefault();
      e.stopPropagation();
      
      var printBOX = document.getElementById("printiFrame").contentWindow;
      
      printBOX.focus();
      printBOX.print(); 
      
    });
    

    $(`#gen_pdf`).off(`click`);
    $(`#gen_pdf`).on(`click`, async function(e) {
      
      e.preventDefault();
      e.stopPropagation();
      

      var this_button = this;
      var pop_text = `Jeste li sigurni da želite kreirati novi PDF dokument?`;

      function new_doc_yes() {

        this_module.save_pdf_and_record(this_button, null);

      };

      function new_doc_no() {
        show_popup_modal(false, pop_text, null );
      };

      
      var show_popup_modal = this_module.pop_mod.show_popup_modal;
      show_popup_modal(`warning`, pop_text, null, 'yes/no', new_doc_yes, new_doc_no, null);
      
      
    });
    
    
    $(`#make_record_btn`).off(`click`);
    $(`#make_record_btn`).on(`click`, async function(e) {
      
      e.preventDefault();
      e.stopPropagation();
      

      var this_button = this;
      var pop_text = `Jeste li sigurni da želite kreirati novi zapis?`;

      function new_doc_yes() {

        this_module.save_pdf_and_record( this_button, `make_record`);

      };

      function new_doc_no() {
        show_popup_modal(false, pop_text, null );
      };

      var show_popup_modal = this_module.pop_mod.show_popup_modal;
      show_popup_modal(`warning`, pop_text, null, 'yes/no', new_doc_yes, new_doc_no, null);
      
      
      
    });
    
    
    
    
    
    
  }, 5*1000 );


  
  

  return {
    html: component_html,
    id: component_id
  } 
  


},
scripts: function () {
  
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};

  
  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
  };
  this_module.set_init_data = set_init_data;

  
async function generate_cit_doc(data, lang) {
  
  var this_component_data = this_module.cit_data.cit_preview_modal;
  
  
  if ( data.doc_type == `sklad_kartica` ) {
    $(`#gen_pdf`).css(`display`, `none`);
    $(`#make_record_btn`).css(`display`, `none`);
  } 
  else {
    $(`#gen_pdf`).css(`display`, `flex`);
    $(`#make_record_btn`).css(`display`, `flex`);
  };
  
  // ovo je jako bitno - ovdje PREPIŠEM sve propertije iz data_for_doc koji su prije bili
  // data je zapravo ------> data_for_doc u partner/kalk/sirov/project modulu (zavisi odakle poziva generiranje dokumenta)
  // ostavljam samo id modula i rand id od modula !!!
  this_module.cit_data.cit_preview_modal = { 
    
    ...data,
    
    id: this_component_data.id,
    rand_id: this_component_data.rand_id
    
  };
  
  

  data.ex_rate = 1;
  
  
  // ------------------------ HNB TEČAJNA LISTA ------------------------
  if ( data.doc_valuta && data.doc_valuta !== "HRK" ) {
    
    var hnb = await get_ex_rates();
  
    if ( hnb.success == true ) {

      data.ex_rate = hnb.result.find( (tecaj_obj) => { return tecaj_obj.valuta === data.doc_valuta } );
      data.ex_rate = cit_number( data.ex_rate.srednji_tecaj );
      console.log(data.ex_rate);

    }
    else {

      console.log(`GREŠKA PRILIKOM GET-a on HNB tečaj !!!!!!`);
      console.log(hnb);

    };
    
  };
  // ------------------------ HNB TEČAJNA LISTA ------------------------
  
  
  // AKO JE USER RUČNO UPISAO NEKI TEČAJ !!!!!!!!!!!
  if ( data.doc_valuta_manual ) data.ex_rate = data.doc_valuta_manual;
  
  var doc_type_text = {
    hr: ``,
    en: ``,
  };
  
  var partner_tip_text = {
    hr: ``,
    en: ``,
  };
  
  var doc_adresa_title = {
    hr: ``,
    en: ``,
  };
  
  var ship_adresa_title = {
    hr: ``,
    en: ``,
  };
  
  var payment_title = {
    hr: ``,
    en: ``,
  };  
  
  
  var rok_title = {
    hr: ``,
    en: ``,
  };  
  
    
  if ( data.doc_type == `offer_dobav` ) { 
    
    doc_type_text.hr = `UPIT`;
    doc_type_text.en = `RfQ`; 
    
    partner_tip_text.hr = `Dobavljač`;
    partner_tip_text.en = `Supplier`;
    
    doc_adresa_title.hr = `Adresa Primatelja:`;
    doc_adresa_title.en = `Recipient Address:`;
    
    ship_adresa_title.hr = `Adresa Dostave:`;
    ship_adresa_title.en = `Shipment Address:`;
    
    payment_title.hr = `Način plaćanja:`;
    payment_title.en = `Payment method:`;
    
    rok_title.hr = `Molim odgovor do:`;
    rok_title.en = `Please response before:`;
    
    
  };
  
  
  if ( data.doc_type == `sklad_kartica` ) { 
    
    doc_type_text.hr = `SKL. KARTICA`;
    doc_type_text.en = `STORAGE CARD`; 
    
    partner_tip_text.hr = `Dobavljač`;
    partner_tip_text.en = `Supplier`;
    
    doc_adresa_title.hr = `Adresa Primatelja:`;
    doc_adresa_title.en = `Recipient Address:`;
    
    ship_adresa_title.hr = `Adresa Dostave:`;
    ship_adresa_title.en = `Shipment Address:`;
    
    payment_title.hr = `Način plaćanja:`;
    payment_title.en = `Payment method:`;
    
    rok_title.hr = `Molim odgovor do:`;
    rok_title.en = `Please response before:`;
    
    
  };
  
  
  if ( data.doc_type == `offer_reserv` ) { 
    
    doc_type_text.hr = `PONUDA`;
    doc_type_text.en = `OFFER`; 
    
    partner_tip_text.hr = `Kupac`;
    partner_tip_text.en = `Customer`;
    
    doc_adresa_title.hr = `Adresa Ponude:`;
    doc_adresa_title.en = `Offer Address:`;
    
    ship_adresa_title.hr = `Adresa Dostave:`;
    ship_adresa_title.en = `Shipment Address:`;
    
    payment_title.hr = `Način plaćanja:`;
    payment_title.en = `Payment method:`;
    
    rok_title.hr = `Molim odgovor do:`;
    rok_title.en = `Please response before:`;
    
    
  };
  
  
  if ( data.doc_type == `radni_nalog` ) { 
    
    doc_type_text.hr = `RADNI NALOG`;
    doc_type_text.en = `WORK ORDER`; 
    
    partner_tip_text.hr = `Kupac`;
    partner_tip_text.en = `Customer`;
    
    doc_adresa_title.hr = `Adresa Primatelja:`;
    doc_adresa_title.en = `Recipient Address:`;
    
    ship_adresa_title.hr = `Adresa Dostave:`;
    ship_adresa_title.en = `Shipment Address:`;
    
    payment_title.hr = `Način plaćanja:`;
    payment_title.en = `Payment method:`;
    
    rok_title.hr = `Molim odgovor do:`;
    rok_title.en = `Please response before:`;
    
  };
  
  
  if ( data.doc_type == `order_reserv` ) { 
    
    doc_type_text.hr = `POTVRDA NARUDŽBE`;
    doc_type_text.en = `ORDER`; 
    
    partner_tip_text.hr = `Kupac`;
    partner_tip_text.en = `Customer`;
    
    doc_adresa_title.hr = `Adresa Narudžbe:`;
    doc_adresa_title.en = `Order Address:`;
    
    ship_adresa_title.hr = `Adresa Dostave:`;
    ship_adresa_title.en = `Shipment Address:`;
    
    payment_title.hr = `Način plaćanja:`;
    payment_title.en = `Payment method:`;
    
    rok_title.hr = `Molim potvrdu prije:`;
    rok_title.en = `Please confirm before:`;
    
  };
  
  
  if ( data.doc_type == `in_record` ) { 
    
    doc_type_text.hr = `PRIMKA`;
    doc_type_text.en = `RECPT`; 
    
    partner_tip_text.hr = `Dobavljač`;
    partner_tip_text.en = `Supplier`;
    
    doc_adresa_title.hr = `Adresa Dobavljača:`;
    doc_adresa_title.en = `Supplier Address:`;
    
    ship_adresa_title.hr = `Adresa Dostave:`;
    ship_adresa_title.en = `Shipment Address:`;
    
    payment_title.hr = `Način plaćanja:`;
    payment_title.en = `Payment method:`;
    
    rok_title.hr = `Datum dostave:`;
    rok_title.en = `Receipt Date:`;
    
    
  }; 
  
  
  
  if ( data.doc_type == `in_pro_record` ) { 
    
    doc_type_text.hr = `PRIMKA.PRO`;
    doc_type_text.en = `RECPT`; 
    
    partner_tip_text.hr = `Dobavljač`;
    partner_tip_text.en = `Supplier`;
    
    doc_adresa_title.hr = `Adresa Dobavljača:`;
    doc_adresa_title.en = `Supplier Address:`;
    
    ship_adresa_title.hr = `Adresa Dostave:`;
    ship_adresa_title.en = `Shipment Address:`;
    
    payment_title.hr = `Način plaćanja:`;
    payment_title.en = `Payment method:`;
    
    rok_title.hr = `Datum dostave:`;
    rok_title.en = `Receipt Date:`;
    
    
  }; 
    
  
  
  if ( data.doc_type == `order_dobav` ) { 

    doc_type_text.hr = `NARUDŽBENICA`;
    doc_type_text.en = `ORDER`; 

    partner_tip_text.hr = `Dobavljač`;
    partner_tip_text.en = `Supplier`;

    doc_adresa_title.hr = `Adresa Primatelja:`;
    doc_adresa_title.en = `Recipient Address:`;

    ship_adresa_title.hr = `Adresa Dostave:`;
    ship_adresa_title.en = `Shipment Address:`;

    payment_title.hr = `Način plaćanja:`;
    payment_title.en = `Payment method:`;

    rok_title.hr = `Molim dostaviti do:`;
    rok_title.en = `Please deliver before:`;


  }; 

  
  if ( data.doc_type == `bill` ) { 
    
    doc_type_text.hr = `RAČUN`;
    doc_type_text.en = `RECEIPT`; 
    
    partner_tip_text.hr = `Kupac`;
    partner_tip_text.en = `Customer`;
    
    doc_adresa_title.hr = `Adresa Primatelja:`;
    doc_adresa_title.en = `Recipient Address:`;
    
    ship_adresa_title.hr = `Adresa Dostave:`;
    ship_adresa_title.en = `Shipment Address:`;
    
    payment_title.hr = `Način plaćanja:`;
    payment_title.en = `Payment method:`;
    
    rok_title.hr = `Molim odgovor do:`;
    rok_title.en = `Please response before:`;
    
  };  

  
  if ( data.doc_type == `offer` ) { 
    
    doc_type_text.hr = `PONUDA`;
    doc_type_text.en = `OFFER`; 
    
    partner_tip_text.hr = `Kupac`;
    partner_tip_text.en = `Customer`;
    
    doc_adresa_title.hr = `Adresa Primatelja:`;
    doc_adresa_title.en = `Recipient Address:`;
    
    ship_adresa_title.hr = `Adresa Dostave:`;
    ship_adresa_title.en = `Shipment Address:`;
    
    payment_title.hr = `Način plaćanja:`;
    payment_title.en = `Payment method:`;
    
    rok_title.hr = `Molim odgovor do:`;
    rok_title.en = `Please response before:`;
    
  };  

  
  if ( data.doc_type == `out_record` ) { 
    
    doc_type_text.hr = `OTPREMNICA`;
    doc_type_text.en = `SHIPMENT`; 
    
    partner_tip_text.hr = `Kupac`;
    partner_tip_text.en = `Customer`;
    
    doc_adresa_title.hr = `Adresa Primatelja:`;
    doc_adresa_title.en = `Recipient Address:`;
    
    ship_adresa_title.hr = `Adresa Dostave:`;
    ship_adresa_title.en = `Shipment Address:`;
    
    payment_title.hr = `Način plaćanja:`;
    payment_title.en = `Payment method:`;
    
    rok_title.hr = `Molim odgovor do:`;
    rok_title.en = `Please response before:`;
    
  }; 
  
  
  this_module.temp_vars = {
    
    
    
    hr: {
      oib: `OIB`,
      datum: `Datum`,
      doc_type: doc_type_text.hr,
      referent: `Referent`,
      place: `Mjesto izdavanja`,
      partner_tip: partner_tip_text.hr,
      pay_data: ``,
      doc_adresa_title: doc_adresa_title.hr,
      ship_adresa_title: ship_adresa_title.hr,
      payment_title: payment_title.hr,
      rok_title: rok_title.hr,

    },
    
    
    en: {
      oib: `VAT No`,
      datum: `Date`,
      doc_type: doc_type_text.en,
      referent: `Assignee`,
      place: `Place`,
      partner_tip: partner_tip_text.en,
      pay_data: 
`
<br>
<br>
<br>
VAT has not been charged according to Article  45.Section 1.Point.1 of Croatian VAT Law. <br>
Please execute this payment until 21. 11.2021 <br>
Bank name: <b>Addiko Bank  d.d., Zagreb, Croatia </b><br>
Beneficiary: <b>VELPROM d.o.o.</b> <br>
Account No : <b>2500009-1101346755</b> <br>
IBAN: <b>HR7225000091101346755</b> <br>
SWIFT-CODE/BIC: <b>HAABHR22</b> <br>
Reference: <b>Offer No: ${ data.doc_num }</b> <br>

`,
      doc_adresa_title: doc_adresa_title.en,
      ship_adresa_title: ship_adresa_title.en,
      
      payment_title: payment_title.en,
      rok_title: rok_title.en,
      
    },


  };
  
  $(`#cit_preview_modal`).addClass(`cit_show`);
  
  
  var podaci_dokumenta = this_module.napravi_podatke_dokumenta(data, lang);
  
  
  var html = 
`
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>VELPROM DOKUMENT</title>

${style_html_doc}

</head>

<body>
<table border="0" cellspacing="0" cellpadding="0" style="width: 100%;">
 
  <thead>
    <tr>
      <td>
        <div class="header-space">&nbsp;</div>
      </td>
    </tr>
  </thead>
  
  <tbody>
    <tr>
      <td>
        <div class="page" contentEditable="true" >

          ${ podaci_dokumenta.doc_start }
          <br>
          ${ data.doc_type == `offer_dobav` ? this_module.make_table_offer_dobav(data, lang) : "" }
          
          ${ data.doc_type == `order_dobav` ? this_module.make_table_order_dobav(data, lang) : "" }
          
          ${ data.doc_type == `offer_reserv` ? this_module.make_table_offer_reserv(data, lang) : "" }
          
          ${ data.doc_type == `order_reserv` ? this_module.make_table_order_reserv(data, lang) : "" }
          
          
          ${ data.doc_type == `in_record` ? this_module.make_table_in_record(data, lang) : "" }
          ${ data.doc_type == `in_pro_record` ? this_module.make_table_in_pro_record(data, lang) : "" }
          
          ${ data.doc_type == `radni_nalog` ? this_module.make_table_radni_nalog(data, lang) : "" }
          
          ${ data.doc_type == `sklad_kartica` ? this_module.make_table_sklad_kartica(data, lang) : "" }
          
          ${ data.doc_type == `exit_kartica` ? this_module.make_table_exit_kartica(data, lang) : "" }
          
          <!--
          ---------------------------------------------------------------------
          OVO JE PRIMJER TABLICE
          
          <div class="divTable greyGridTable">
           
            <div class="divTableHeading">
              <div class="divTableRow">
                <div class="divTableHead">head1</div>
                <div class="divTableHead">head2</div>
                <div class="divTableHead">head3</div>
                <div class="divTableHead">head4</div>
              </div>
            </div>
            
            <div class="divTableBody">
             
              <div class="divTableRow">
                <div class="divTableCell">cell1_1</div>
                <div class="divTableCell">cell2_1</div>
                <div class="divTableCell">cell3_1</div>
                <div class="divTableCell">cell4_1</div>
              </div>
              
              <div class="divTableRow">
                <div class="divTableCell">cell1_2</div>
                <div class="divTableCell">cell2_2</div>
                <div class="divTableCell">cell3_2</div>
                <div class="divTableCell">cell4_2</div>
              </div>
              <div class="divTableRow">
                <div class="divTableCell">cell1_3</div>
                <div class="divTableCell">cell2_3</div>
                <div class="divTableCell">cell3_3</div>
                <div class="divTableCell">cell4_3</div>
              </div>
              <div class="divTableRow">
                <div class="divTableCell">cell1_4</div>
                <div class="divTableCell">cell2_4</div>
                <div class="divTableCell">cell3_4</div>
                <div class="divTableCell">cell4_4</div>
              </div>
              <div class="divTableRow">
                <div class="divTableCell">cell1_5</div>
                <div class="divTableCell">cell2_5</div>
                <div class="divTableCell">cell3_5</div>
                <div class="divTableCell">cell4_5</div>
              </div>
              <div class="divTableRow">
                <div class="divTableCell">cell1_6</div>
                <div class="divTableCell">cell2_6</div>
                <div class="divTableCell">cell3_6</div>
                <div class="divTableCell">cell4_6</div>
              </div>
              <div class="divTableRow">
                <div class="divTableCell">cell1_7</div>
                <div class="divTableCell">cell2_7</div>
                <div class="divTableCell">cell3_7</div>
                <div class="divTableCell">cell4_7</div>
              </div>
              <div class="divTableRow">
                <div class="divTableCell">cell1_8</div>
                <div class="divTableCell">cell2_8</div>
                <div class="divTableCell">cell3_8</div>
                <div class="divTableCell">cell4_8</div>
              </div>
              <div class="divTableRow">
                <div class="divTableCell">cell1_9</div>
                <div class="divTableCell">cell2_9</div>
                <div class="divTableCell">cell3_9</div>
                <div class="divTableCell">cell4_9</div>
              </div>
              <div class="divTableRow">
                <div class="divTableCell">cell1_10</div>
                <div class="divTableCell">cell2_10</div>
                <div class="divTableCell">cell3_10</div>
                <div class="divTableCell">cell4_10</div>
              </div>
              <div class="divTableRow">
                <div class="divTableCell">cell1_11</div>
                <div class="divTableCell">cell2_11</div>
                <div class="divTableCell">cell3_11</div>
                <div class="divTableCell">cell4_11</div>
              </div>
              
            </div>
            
            <div class="divTableFoot tableFootStyle">
              <div class="divTableRow">
                <div class="divTableCell">foot1</div>
                <div class="divTableCell">foot2</div>
                <div class="divTableCell">foot3</div>
                <div class="divTableCell">foot4</div>
              </div>
            </div>
            
          </div>
          
          ---------------------------------------------------------------------
          -->
       
          ${ podaci_dokumenta.doc_end }
       
        </div>
      </td>
    </tr>
  </tbody>
  
  
  <tfoot>
    <tr>
      <td>
        <div class="footer-space">&nbsp;</div>
      </td>
    </tr>
  </tfoot>
  
</table>


${ this_module.napravi_page_header() }


<div class="footer">
  <div style="width: 100%; height: 5pt; font-size: 4pt;">&nbsp;</div>
  Tvrtka je registrirana na Trgovačkom sudu u Zagrebu sa sjedištem: Dalmatinska 27, Gradići; 10410 Velika Gorica, Hrvatska<br>
  IBAN : HR7225000091101346755 Addiko Bank d.d., Iznos temeljnog kapitala: 20.000,00<br>
 <!--  <div class="footerTemplate"><span class="pageNumber">2</span>&nbsp;/&nbsp;<span class="totalPages">3</span> </div> -->
</div>


</body>
</html>

`;
  
  var printBOX = document.getElementById("printiFrame").contentWindow;

  printBOX.document.open();
  printBOX.document.write( html );
  printBOX.document.close(); 



  if (data.orient == "landscape") {
    $(`#printContainerIFRAME`).addClass(`cit_landscape`);
  } else {
    $(`#printContainerIFRAME`).removeClass(`cit_landscape`);
  };

  
  setTimeout( function() {

    var iframe_reference = document.getElementById("printiFrame");
    var iframe_content = (iframe_reference.contentWindow || iframe_reference.contentDocument);
    var gen_doc_body = $(iframe_content.document.documentElement).find(`body`);
    
    var visina_gen_doc = gen_doc_body.height();
    $(iframe_reference).height(visina_gen_doc);
    
    var page = gen_doc_body.find(`.page`);
    
    // ovo je listener ako user nešto počne pisati u pdf preview jer sam napravio da preview bude EDITABILAN !!!
    page.off("input");
    page.on("input", function() {
      
      console.log("input event fired");
      // pogledaj koja je visina html-a koji je user upravo editirao 
      visina_gen_doc = gen_doc_body.height();
      
      // naštima da visina iframe-a bude ista tako da se sve vidi :-) 
      $(iframe_reference).height(visina_gen_doc);  
      
    });
    
    var qrcode = null;
    
    if ( data.doc_type == `sklad_kartica` ) {
      
      
      $.each(data.palet_sifre, function(p_ind, p_sifra) {      
      
        /*
        
        if ( qrcode ) {
          qrcode.clear(); // clear the code.
          // qrcode.makeCode("http://naver.com"); // make another code.
        };
        
        */
 
        qrcode = new QRCode( page.find(`#doc_qrcode_${p_sifra}`)[0], {
        
          text: 
  `http://192.168.88.193:8080/#sirov/${data.sirov_id}/sklad/${data.sklad?.short || null }/sector/${data.sector?.sifra || null }/level/${data.level?.sifra || null }/shelf/${ data.shelf?.naziv || null }`,
          /*text: `http://192.168.88.112:9000/#sirov/61bc4e42fe6d06b00c3fd16e/sklad/SKL1/sector/S1/level/L1`,*/
          width: 256,
          height: 256,
          colorDark : "#000000",
          colorLight : "#ffffff",
          correctLevel : QRCode.CorrectLevel.H

        });


        
      
      });  // kraj loopa po palet sifre
      
    
      var visina_gen_doc = gen_doc_body.height();
      $(iframe_reference).height(visina_gen_doc);
      
      
    }; // kraj ako je ovo sklad kartica
             
          
    
    
  }, 100 );

  
}; 
this_module.generate_cit_doc = generate_cit_doc;  
  

function napravi_page_header() {

  var page_header_HTML =

`
<div class="header">
  <img class="logo" src="../../velprom_logo.png" />
  <div class="generalije">
    <b style="color: #5a79a4;">VELPROM d.o.o.</b> <br>
    <b>Poštanska 7, 10410 VELIKA GORICA, HRVATSKA</b> <br>
    <b>OIB:</b> 33428207264<br>
    <b>Tel:</b> +385 1 62 15529,+385 1 621 5541<br>
    <b>Fax:</b> +385 1 621 5529,+385 1 621 5541<br>
    <b>e-mail:</b> sale@velprom.hr<br>
    <b>web:</b> www.velprom.hr<br>
  </div>
</div>
`;

  return page_header_HTML;

}; 
this_module.napravi_page_header = napravi_page_header;  
  
  
function napravi_podatke_dokumenta(data, lang) {

  
  var temp_vars = this_module.temp_vars;
  
  var multiline_adresa = ``;
  
  if (data.partner_adresa) {
    var split_adresa = data.partner_adresa.split(`,`);
    
    $.each( split_adresa, function(a_ind, adres) {
      // nemoj dodavati <br> na zadnjoj liniji adrese
      if( a_ind !== split_adresa.length-1 ) split_adresa[a_ind] = adres.trim() + `<br>`;
    });
    
    multiline_adresa = split_adresa.join('');
    
  };
  
  
  
  var doc_start =

`
<!--
<div class="mjesto_vrijeme_dokumenta">
${data.mjesto_izrade}, ${data.datum_izrade}. Vrijeme izrade: ${data.vrijeme_izrade}
</div>
-->


<div class="podaci_dokumenta_i_primatelja">

<div class="podaci_dokumenta" style="${ data.doc_type == 'in_record' || data.doc_type == 'in_pro_record' ? 'display: none;' : '' }" >

  ${ temp_vars[lang].datum }: ${ cit_dt(null, `y-m-d`).date_time }<br> 
  ${ temp_vars[lang].referent }: ${ data.referent || "" }<br>  
  ${ temp_vars[lang].place }: ${ data.place ? data.place : "" }<br>
  
  ${ data.doc_adresa ? (`<br><span style="font-size: 9pt; line-height: 15pt;" >` + temp_vars[lang].doc_adresa_title + `</span><br>`) : "" }
  ${ data.doc_adresa ? data.partner_name : "" } ${ data.doc_adresa ? data.doc_adresa : "" }
  
  ${ data.dostav_adresa ? (`<br><span style="font-size: 9pt; line-height: 15pt;" >` + temp_vars[lang].ship_adresa_title + `</span><br>`) : "" }
  ${ data.dostav_adresa ? data.partner_name : "" } ${ data.dostav_adresa || "" }
 
</div>

<div class="podaci_primatelja" style="${ data.doc_type == 'in_record' || data.doc_type == 'in_pro_record' ? 'display: none;' : '' }" >

  <div class="vrsta_i_broj_dokumenta">
    ${ temp_vars[lang].doc_type }:
    <div class="broj_dokumenta">${data.doc_num}</div>
  </div>


<span style="font-size: 12pt; line-height: 22pt;" >${ temp_vars[lang].partner_tip }</span>
<br/>
${ temp_vars[lang].oib }: ${ data.oib ? data.oib.trim() : "" }<br> 
${ data.partner_name ? data.partner_name.trim() : "" }<br> 
${ multiline_adresa }


<!--

${ data.partner_place || "" }<br> 
${ data.partner_country || "" }<br> 

-->

</div>

<div class="doc_pay" style="${ data.doc_type == 'in_record' || data.doc_type == 'in_pro_record' ? 'display: none;' : '' }" >
  ${ data.doc_pay ? (`<br><span style="font-size: 9pt; line-height: 15pt; " >` + temp_vars[lang].payment_title + `</span>`) : "" }<br>
  ${ data.doc_pay ? data.doc_pay.def_rows : "" }
</div>

</div>

`;    

  return {
    doc_start: doc_start,
    doc_end: temp_vars[lang].pay_data,
  }

};  
this_module.napravi_podatke_dokumenta = napravi_podatke_dokumenta;
  
  
function make_table_offer_dobav(data, lang) {
  
  
  var iznad_tablice = ``;
  var table_header = ``;
  var table_body = ``;
  var table_footer = ``;
  var doc_table = ``;

    
iznad_tablice =   
    
`
<b>${ this_module.temp_vars[lang].rok_title } ${ data.rok ? cit_dt(data.rok, `y-m-d`).date : "" }</b><br>
<br>

`;    

table_header =
`
<div class="divTableHeading">
  <div class="divTableRow">
    <div class="divTableHead" style="width: 10%;">R. br</div>
    <div class="divTableHead" style="width: 50%; text-align: left;">Naziv artikla</div>
    <div class="divTableHead" style="width: 10%;">J.M. VELPROM</div>
    <div class="divTableHead" style="width: 10%;">J.M. DOB.</div>
    <div class="divTableHead" style="width: 10%;">Količina VELPROM</div>
    <div class="divTableHead" style="width: 10%;">Količina DOB</div>
  </div>
</div>        
`;   
   
  
  var table_rows = ``;
    
    
    // kada se radi o cijenama obavezno  cijena / data.ex_rate !!!!
    // kada se radi o cijenama obavezno  cijena / data.ex_rate !!!!
    // kada se radi o cijenama obavezno  cijena / data.ex_rate !!!!
    
  $.each( data.items, function(ind, item) {
    
    table_rows += 
`
<div class="divTableRow">
  <div class="divTableCell" style="text-align: center;">${ind+1}.</div>
  <div class="divTableCell" style="text-align: left;">${item.naziv}</div>
  <div class="divTableCell" style="text-align: center;">${item.jedinica_in}</div>
  <div class="divTableCell" style="text-align: center;">${item.jedinica_out}</div>
  <div class="divTableCell" style="text-align: right;">${ cit_format( item.count , item.decimals) }</div>
  <div class="divTableCell" style="text-align: right;">${ cit_format( item.count / item.koef , item.decimals) }</div>
</div>

`;
    
  });
  
    
table_body = `<div class="divTableBody">${ table_rows }</div>`;
    
    
table_footer = 
  
`
<div class="divTableFoot tableFootStyle">
  <div class="divTableRow">
    <div class="divTableCell">&nbsp;</div>
    <div class="divTableCell">&nbsp;</div>
    <div class="divTableCell">&nbsp;</div>
    <div class="divTableCell">&nbsp;</div>
    <div class="divTableCell">&nbsp;</div>
    <div class="divTableCell">&nbsp;</div>
  </div>
</div>


`;  
    
     
doc_table = 
`
<div class="divTable greyGridTable">
  ${ table_header }
  ${ table_body }
  ${ table_footer }
</div>
`;     

  
  var komentar = data.doc_komentar ? `<br>Napomena:<br>${ data.doc_komentar.replace(/\n/g, "<br />") }` : "";
  
  return (iznad_tablice + doc_table + komentar );
  
}; 
this_module.make_table_offer_dobav = make_table_offer_dobav;
  
  
function make_table_order_dobav(data, lang) {
  
  
  var iznad_tablice = ``;
  var table_header = ``;
  var table_body = ``;
  var table_footer = ``;
  var doc_table = ``;
  
    
    
iznad_tablice =   
    
`
<b>${ this_module.temp_vars[lang].rok_title } ${ data.rok ? cit_dt(data.rok, `y-m-d`).date : "" }</b><br>
Po ponudi: ${ data.ref_doc_sifra || "" }<br>
<br>

`;    

table_header =
`
<div class="divTableHeading">
  <div class="divTableRow">
    <div class="divTableHead" style="width: 10%;">R. br</div>
    <div class="divTableHead" style="width: 40%; text-align: left;">Naziv artikla</div>
    <div class="divTableHead" style="width: 10%;">Kol. VEL</div>
    <div class="divTableHead" style="width: 10%;">Kol. DOB</div>
    <div class="divTableHead" style="width: 10%;">J.C. VEL/DOB</div>
    <div class="divTableHead" style="width: 20%;">Ukupna cijena</div>
  </div>
</div>        
`;   
   
  
  var table_rows = ``;
  var sum_price = 0;
    
    
    // kada se radi o cijenama obavezno  cijena / data.ex_rate !!!!
    // kada se radi o cijenama obavezno  cijena / data.ex_rate !!!!
    // kada se radi o cijenama obavezno  cijena / data.ex_rate !!!!
    
  $.each( data.items, function(ind, item) {
    
    
    sum_price += item.count * item.price;
    
    table_rows += 
`
<div class="divTableRow" >

  <div class="divTableCell" style="text-align: center;">${ind+1}.</div>
  <div class="divTableCell" style="text-align: left;">${item.naziv}</div>
  <div class="divTableCell" style="text-align: right;">${ cit_format( item.count , item.decimals) } ${item.jedinica_in}</div>
  <div class="divTableCell" style="text-align: right;">${ cit_format( item.count / item.koef , item.decimals)} ${item.jedinica_out}</div>
  <div class="divTableCell" style="text-align: right;">${ cit_format( item.price , item.decimals)} / ${ cit_format( item.price / item.koef, item.decimals)}</div>
  <div class="divTableCell" style="text-align: right;">${ cit_format( item.count*item.price , item.decimals) }</div>
  
</div>

`;
    
  });
  
    
table_body = `<div class="divTableBody">${ table_rows }</div>`;


table_footer = 
  
`
<div class="divTableFoot tableFootStyle">

  <div class="divTableRow">
    <div class="divTableCell">&nbsp;</div>
    <div class="divTableCell">&nbsp;</div>
    <div class="divTableCell">&nbsp;</div>
    <div class="divTableCell">&nbsp;</div>
    <div class="divTableCell">&nbsp;</div>
    
    <div class="divTableCell" style="text-align: right; font-size: 14px; font-weight: 700;" >${ cit_format( sum_price, 2 ) }kn</div>
    
  </div>
</div>


`;  
    
     
doc_table = 
`
<div class="divTable greyGridTable">
  ${ table_header }
  ${ table_body }
  ${ table_footer }
</div>
`;     

  
  var komentar = data.doc_komentar ? `<br>Napomena:<br>${ data.doc_komentar.replace(/\n/g, "<br />") }` : "";
  
  return (iznad_tablice + doc_table + komentar );
  
}; 
this_module.make_table_order_dobav = make_table_order_dobav;

  
function make_table_offer_reserv(data, lang ) {
  
  var iznad_tablice = ``;
  var table_header = ``;
  var table_body = ``;
  var table_footer = ``;
  var doc_table = ``;
    
iznad_tablice =   
    
`
<b>${ this_module.temp_vars[lang].rok_title } ${ data.rok ? cit_dt(data.rok, `y-m-d`).date : "" }</b><br>
<br>

`;    
  
  
  var ima_popust = false;  
  var popust_stupac = ``;
  
  // ako bilo koji item ima popust onda moram ubaciti stupac s popustom  
  $.each( data.items, function(i_ind, item) {
    if( item.discount ) ima_popust = true;
  });
  
  
  
if ( ima_popust == true )  {
  popust_stupac = `<div class="divTableHead" style="width: 7%;">Popust</div>`
};

table_header =
`
<div class="divTableHeading">
  <div class="divTableRow">
    <div class="divTableHead" style="width: 10%;">R. br</div>
    <div class="divTableHead" style="width: 43%; text-align: left;">Naziv artikla</div>
    <div class="divTableHead" style="width: 10%;">J. mjere</div>
    <div class="divTableHead" style="width: 10%;">J. cijena</div>
    ${popust_stupac}
    <div class="divTableHead" style="width: 10%;">Količina</div>
    <div class="divTableHead" style="width: 10%;">Cijena</div>
  </div>
</div>        
`;
  
  var table_rows = ``;
  var dis_stupac = ``;
  
    
    // kada se radi o cijenama obavezno  cijena / data.ex_rate !!!!
    // kada se radi o cijenama obavezno  cijena / data.ex_rate !!!!
    // kada se radi o cijenama obavezno  cijena / data.ex_rate !!!!
    
  $.each( data.items, function(ind, item) {
    
    dis_stupac = ima_popust ? `<div class="divTableCell" style="width: 10%;">${ cit_format(item.discount, 2)+"%" || "" }</div>`: "";
    
    var item_dostav_adresa = "";
    if ( item.dostav_adresa ) {
      item_dostav_adresa = `<b>` + this_module.temp_vars[lang].ship_adresa_title + `</b><br> ` + item.dostav_adresa; 
    };
    
    
    
    table_rows += 
`
<div class="divTableRow">

    <div class="divTableCell" style="text-align: center;" >${ind+1}.</div>
    <div class="divTableCell" style="text-align: left;" >${ item.naziv + ` ` + item_dostav_adresa }</div>
    <div class="divTableCell" style="text-align: center;" >${item.jedinica}</div>
    <div class="divTableCell" style="text-align: right;" >${ cit_format(item.unit_price, 2) }kn</div>
    ${dis_stupac}
    <div class="divTableCell" style="text-align: right;" >${ cit_format(item.count, 0) }</div>
    <div class="divTableCell" style="text-align: right;" >${ cit_format(item.dis_price, 2) }kn</div>

</div>

`;
    
  });
  
    
table_body = `<div class="divTableBody">${ table_rows }</div>`;
    
    
table_footer = 
  
`
<div class="divTableFoot tableFootStyle">
  <div class="divTableRow">
  
    <div class="divTableCell" style="text-align: center;" >&nbsp;</div>
    <div class="divTableCell" style="text-align: left;" >&nbsp;</div>
    <div class="divTableCell" style="text-align: center;" >&nbsp;</div>
    <div class="divTableCell" style="text-align: right;" >&nbsp;</div>
    <div class="divTableCell" style="text-align: right;" >&nbsp;</div>
    ${ dis_stupac ? `<div class="divTableCell" style="text-align: right;" > </div>` : "" }
    <div class="divTableCell" style="text-align: right;" ></div>
    
  </div>
</div>


`;  
    
     
doc_table = 
`
<div class="divTable greyGridTable">
  ${ table_header }
  ${ table_body }
  ${ table_footer }
</div>
`;     

  var komentar = data.doc_komentar ? `<br>Napomena:<br>${ data.doc_komentar.replace(/\n/g, "<br />") }` : "";
  
  return (iznad_tablice + doc_table + komentar );
  
}; 
this_module.make_table_offer_reserv = make_table_offer_reserv;
  
  
function make_table_order_reserv(data, lang) {
  
  
  var iznad_tablice = ``;
  var table_header = ``;
  var table_body = ``;
  var table_footer = ``;
  var doc_table = ``;

    
iznad_tablice =   
    
`
<b>${ this_module.temp_vars[lang].rok_title } ${ data.rok ? cit_dt(data.rok, `y-m-d`).date : "" }</b><br>
<br>

`;    
  
  
  var ima_popust = false;  
  var popust_stupac = ``;
  
  // ako bilo koji item ima popust onda moram ubaciti stupac s popustom  
  $.each( data.items, function(i_ind, item) {
    if( item.discount ) ima_popust = true;
  });
  
  
  
if ( ima_popust == true )  {
  popust_stupac = `<div class="divTableHead" style="width: 7%;">Popust</div>`
};

table_header =
`
<div class="divTableHeading">
  <div class="divTableRow">
    <div class="divTableHead" style="width: 10%;">R. br</div>
    <div class="divTableHead" style="width: 43%; text-align: left;">Naziv artikla</div>
    <div class="divTableHead" style="width: 10%;">J. mjere</div>
    <div class="divTableHead" style="width: 10%;">J. cijena</div>
    ${popust_stupac}
    <div class="divTableHead" style="width: 10%;">Količina</div>
    <div class="divTableHead" style="width: 10%;">Cijena</div>
  </div>
</div>        
`;   
   
  
  var table_rows = ``;
    
    
    // kada se radi o cijenama obavezno  cijena / data.ex_rate !!!!
    // kada se radi o cijenama obavezno  cijena / data.ex_rate !!!!
    // kada se radi o cijenama obavezno  cijena / data.ex_rate !!!!
    
  var sum_price = 0;
  var dis_stupac = ``;
  
  
  // --------------------------------------------------------------------------
  $.each( data.items, function(ind, item) {
    
    sum_price += item.dis_price;
    
    
    dis_stupac = ima_popust ? `<div class="divTableCell" style="width: 10%;">${ cit_format(item.discount, 2)+"%" || "" }</div>`: "";
    
    
    var item_dostav_adresa = "";
    if ( item.dostav_adresa ) {
      item_dostav_adresa = `<b>` + this_module.temp_vars[lang].ship_adresa_title + `</b><br> ` + item.dostav_adresa; 
    };
    
    
    
    table_rows += 
`
<div class="divTableRow">

  <div class="divTableCell" style="text-align: center;" >${ind+1}.</div>
  <div class="divTableCell" style="text-align: left;" >${ item.naziv + ` ` + item_dostav_adresa }</div>
  <div class="divTableCell" style="text-align: center;" >${item.jedinica}</div>
  <div class="divTableCell" style="text-align: right;" >${ cit_format(item.unit_price, 2) }kn</div>
  ${dis_stupac}
  <div class="divTableCell" style="text-align: right;" >${ cit_format(item.count, 0) }</div>
  <div class="divTableCell" style="text-align: right;" >${ cit_format(item.dis_price, 2) }kn</div>

</div>

`;
    
  });
  // --------------------------------------------------------------------------
  
    
table_body = `<div class="divTableBody">${ table_rows }</div>`;
    
    
table_footer = 
  
`
<div class="divTableFoot tableFootStyle">
  <div class="divTableRow">
  
    <div class="divTableCell" style="text-align: center;" >${ " " }</div>
    <div class="divTableCell" style="text-align: left;" >${ " " }</div>
    <div class="divTableCell" style="text-align: center;" >${ " " }</div>
    <div class="divTableCell" style="text-align: right;" >${ " " }</div>
    <div class="divTableCell" style="text-align: right;" >${ " " }</div>
    ${ dis_stupac ? `<div class="divTableCell" style="text-align: right;" > </div>` : "" }
    <div class="divTableCell" style="text-align: right; font-size: 14px; font-weight: 700;" >${ cit_format( sum_price, 2 ) }kn</div>
    
  </div>
</div>


`;  
    
     
doc_table = 
`
<div class="divTable greyGridTable">
  ${ table_header }
  ${ table_body }
  ${ table_footer }
</div>
`;     

  var komentar = data.doc_komentar ? `<br>Napomena:<br>${ data.doc_komentar.replace(/\n/g, "<br />") }` : "";
  
  return (iznad_tablice + doc_table + komentar );
  
}; 
this_module.make_table_order_reserv = make_table_order_reserv;

  
function make_table_in_record(data, lang) {
  
  
  var iznad_tablice = ``;
  var table_header = ``;
  var table_body = ``;
  var table_footer = ``;

    
iznad_tablice =   
    
`
  
<b>${ data.partner_name ? data.partner_name.trim() : "" }</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>${ this_module.temp_vars[lang].rok_title } ${ data.rok ? cit_dt(data.rok, `y-m-d`).date : "" }</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>OT. DOBAV.:</b> ${ data.ref_doc_sifra || "_______________________" }&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>Datum printa: ${ cit_dt(null, `y-m-d`).date_time }</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>Printao/la: ${ data.referent || "" }</b>
<br>
<br>
<div style="margin-bottom: 3mm;" >IME I POTPIS RADNIKA 1 ____________________________________________________________________________________</div>
<div style="margin-bottom: 3mm;" >IME I POTPIS RADNIKA 2 ____________________________________________________________________________________</div>
<div style="margin-bottom: 3mm;" >IME I POTPIS RADNIKA 3 ____________________________________________________________________________________</div>

`;    
  
  
  var ima_popust = false;  
  var popust_stupac = ``;
  
  // ako bilo koji item ima popust onda moram ubaciti stupac s popustom  
  $.each( data.items, function(i_ind, item) {
    if( item.discount ) ima_popust = true;
  });
  
table_header =
`
<div class="divTableHeading">
  <div class="divTableRow">
    <div class="divTableHead" style="width: 10%;">R. br</div>
    <div class="divTableHead" style="width: 30%; text-align: left;">Naziv artikla</div>
    <div class="divTableHead" style="width: 10%;">J.M.</div>
    <div class="divTableHead" style="width: 10%;">ND Kol.</div>
    <div class="divTableHead" style="width: 10%;">OT Kol.</div>
    <div class="divTableHead" style="width: 10%;">UK. KOL.</div>
    <div class="divTableHead" style="width: 10%;">UK. PALETA</div>
    <div class="divTableHead" style="width: 10%;">UK. PAKIR.</div>
  </div>
</div>        
`;   
   
  
  var just_items_rows = ``;
    
  
  // --------------------------------------------------------------------------
  $.each( data.items, function(ind, item) {

    just_items_rows += 
`


  
  
    <div class="divTableRow">

        <div class="divTableCell" style="text-align: center; width: 10%;" >${ind+1}.</div>
        <div class="divTableCell" style="text-align: left; width: 30%;" >${item.naziv}</div>
        <div class="divTableCell" style="text-align: center; width: 10%;" >${item.jedinica_in}</div>
        <div class="divTableCell" style="text-align: center; width: 10%;" >${ cit_format(item.nd_count, 0) }</div>
        <div class="divTableCell" style="text-align: center; width: 10%;" >${ cit_format(item.count, 0) }</div>
        <div class="divTableCell realni_count" style="text-align: center; width: 10%;" >${ "" }</div>
        <div class="divTableCell realni_count" style="text-align: center; width: 10%;" >${ "" }</div>
        <div class="divTableCell realni_count" style="text-align: center; width: 10%;" >${ "" }</div>

    </div>
    

`;
    
  });
  
  // --------------------------------------------------------------------------
  
  
  var first_primka_table =
  
`
<div class="divTable greyGridTable cit_break_page">
  ${table_header}
  <div class="divTableBody">
    ${just_items_rows}
  </div>
</div>  
  

`;  
  
  
  var first_part = iznad_tablice + first_primka_table;
  
  

  table_body = ``;
  table_footer = ``;
  
  
  var location_rows = ``;
  
  
  // --------------------------------------------------------------------------
  $.each( data.items, function(ind, item) {
   
   
    var loc_last_row =  `cit_break_page`;
    
    // ako je zadnji item onda nemoj stavljati ovu klasu koja označava page break u printu (ako to ostavim onda printa zadnji page prazan !!!!!)
    if ( ind == data.items.length - 1 ) loc_last_row = ``;
    
    dis_stupac = ima_popust ? `<div class="divTableCell" style="width: 10%;">${ cit_format(item.discount, 2)+"%" || "" }</div>`: "";
    
    location_rows += 
`

${iznad_tablice}
<div class="divTable greyGridTable">
  ${ table_header }
  <div class="divTableBody">
    <div class="divTableRow">

        <div class="divTableCell" style="text-align: center; width: 10%;" >${ind+1}.</div>
        <div class="divTableCell" style="text-align: left; width: 30%;" >${item.naziv}</div>
        <div class="divTableCell" style="text-align: center; width: 10%;" >${item.jedinica_in}</div>
        <div class="divTableCell" style="text-align: center; width: 10%;" >${ cit_format(item.nd_count, 0) }</div>
        <div class="divTableCell" style="text-align: center; width: 10%;" >${ cit_format(item.count, 0) }</div>
        <div class="divTableCell realni_count" style="text-align: center; width: 10%;" >${ "" }</div>
        <div class="divTableCell realni_count" style="text-align: center; width: 10%;" >${ "" }</div>
        <div class="divTableCell realni_count" style="text-align: center; width: 10%;" >${ "" }</div>

    </div>
  </div>
</div>

  
<!-- NASLOVNA TABLICA TJ. RED -->  
<div class="divTable greyGridTable title_location_row" style="background: #e2fafd">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 2px solid #333; border-bottom: 2px solid #333 !important;" >


      <div class="divTableCell" style="width: 25%; border-right: 2px solid transparent;">LOKACIJA</div>
      <div class="divTableCell" style="width: 30%; border-right: 2px solid transparent;">PALETA/PAKIRANJA</div>
      <div class="divTableCell" style="width: 10%; border-right: 2px solid transparent;">KOM</div>
      <div class="divTableCell" style="width: 20%; border-right: 2px solid transparent;">ZAUZIMA PROSTOR D/Š/V u mm</div>
      <div class="divTableCell" style="width: 15%;">UK. KOM</div>


    </div>

  </div>
</div> 



<div class="divTable greyGridTable location_row" style="background: #fff; border: none;">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 0px solid #333; border-bottom: 1px solid #333 !important;" >

      <div class="divTableCell" style="width: 25%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 30%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 10%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 20%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 15%;">&nbsp;</div>


    </div>

  </div>
</div>

<div class="divTable greyGridTable location_row" style="background: #fff; border: none;">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 0px solid #333; border-bottom: 1px solid #333 !important;" >

      <div class="divTableCell" style="width: 25%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 30%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 10%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 20%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 15%;">&nbsp;</div>


    </div>

  </div>
</div>

<div class="divTable greyGridTable location_row" style="background: #fff; border: none;">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 0px solid #333; border-bottom: 1px solid #333 !important;" >

      <div class="divTableCell" style="width: 25%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 30%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 10%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 20%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 15%;">&nbsp;</div>


    </div>

  </div>
</div>

<div class="divTable greyGridTable location_row" style="background: #fff; border: none;">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 0px solid #333; border-bottom: 1px solid #333 !important;" >

      <div class="divTableCell" style="width: 25%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 30%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 10%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 20%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 15%;">&nbsp;</div>


    </div>

  </div>
</div>

<div class="divTable greyGridTable location_row" style="background: #fff; border: none;">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 0px solid #333; border-bottom: 1px solid #333 !important;" >

      <div class="divTableCell" style="width: 25%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 30%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 10%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 20%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 15%;">&nbsp;</div>


    </div>

  </div>
</div>

<div class="divTable greyGridTable location_row" style="background: #fff; border: none;">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 0px solid #333; border-bottom: 1px solid #333 !important;" >

      <div class="divTableCell" style="width: 25%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 30%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 10%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 20%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 15%;">&nbsp;</div>


    </div>

  </div>
</div>

<div class="divTable greyGridTable location_row" style="background: #fff; border: none;">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 0px solid #333; border-bottom: 1px solid #333 !important;" >

      <div class="divTableCell" style="width: 25%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 30%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 10%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 20%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 15%;">&nbsp;</div>


    </div>

  </div>
</div>

<div class="divTable greyGridTable location_row" style="background: #fff; border: none;">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 0px solid #333; border-bottom: 1px solid #333 !important;" >

      <div class="divTableCell" style="width: 25%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 30%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 10%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 20%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 15%;">&nbsp;</div>


    </div>

  </div>
</div>


<div class="divTable greyGridTable location_row ${loc_last_row}" style="background: #fff; border: none;">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 0px solid #333; border-bottom: 1px solid #333 !important;" >


      <div class="divTableCell" style="width: 25%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 30%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 10%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 20%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 15%;">&nbsp;</div>


    </div>

  </div>
</div>
  
   

`;
    
  });
  
  // --------------------------------------------------------------------------
    
  
  
  var komentar = data.doc_komentar ? `<br>Napomena:<br>${ data.doc_komentar.replace(/\n/g, "<br />") }` : "";
  
  return ( first_part + location_rows + komentar );
  
}; 
this_module.make_table_in_record = make_table_in_record;
  
  
  
  
function make_table_in_pro_record(data, lang) {
  
  
  var iznad_tablice = ``;
  var table_header = ``;
  var table_body = ``;
  var table_footer = ``;

    
iznad_tablice =   
    
`
  
<b>${ data.partner_name ? data.partner_name.trim() : "" }</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>${ this_module.temp_vars[lang].rok_title } ${ data.rok ? cit_dt(data.rok, `y-m-d`).date : "" }</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>OT. DOBAV.:</b> ${ data.ref_doc_sifra || "_______________________" }&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>Datum printa: ${ cit_dt(null, `y-m-d`).date_time }</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>Printao/la: ${ data.referent || "" }</b>
<br>
<br>
<div style="margin-bottom: 3mm;" >IME I POTPIS RADNIKA 1 ____________________________________________________________________________________</div>
<div style="margin-bottom: 3mm;" >IME I POTPIS RADNIKA 2 ____________________________________________________________________________________</div>
<div style="margin-bottom: 3mm;" >IME I POTPIS RADNIKA 3 ____________________________________________________________________________________</div>

`;    
  
  
  var ima_popust = false;  
  var popust_stupac = ``;
  
  // ako bilo koji item ima popust onda moram ubaciti stupac s popustom  
  $.each( data.items, function(i_ind, item) {
    if( item.discount ) ima_popust = true;
  });
  
table_header =
`
<div class="divTableHeading">
  <div class="divTableRow">
    <div class="divTableHead" style="width: 10%;">R. br</div>
    <div class="divTableHead" style="width: 30%; text-align: left;">Naziv artikla</div>
    <div class="divTableHead" style="width: 10%;">J.M.</div>
    <div class="divTableHead" style="width: 10%;">ND Kol.</div>
    <div class="divTableHead" style="width: 10%;">OT Kol.</div>
    <div class="divTableHead" style="width: 10%;">UK. KOL.</div>
    <div class="divTableHead" style="width: 10%;">UK. PALETA</div>
    <div class="divTableHead" style="width: 10%;">UK. PAKIR.</div>
  </div>
</div>        
`;   
   
  
  var just_items_rows = ``;
    
  
  // --------------------------------------------------------------------------
  $.each( data.items, function(ind, item) {

    just_items_rows += 
`


  
  
    <div class="divTableRow">

        <div class="divTableCell" style="text-align: center; width: 10%;" >${ind+1}.</div>
        <div class="divTableCell" style="text-align: left; width: 30%;" >${item.naziv}</div>
        <div class="divTableCell" style="text-align: center; width: 10%;" >${item.jedinica}</div>
        <div class="divTableCell" style="text-align: center; width: 10%;" >${ cit_format(item.order_count, 0) }</div>
        <div class="divTableCell" style="text-align: center; width: 10%;" >${ cit_format(item.primka_count, 0) }</div>
        <div class="divTableCell realni_count" style="text-align: center; width: 10%;" >${ "" }</div>
        <div class="divTableCell realni_count" style="text-align: center; width: 10%;" >${ "" }</div>
        <div class="divTableCell realni_count" style="text-align: center; width: 10%;" >${ "" }</div>

    </div>
    

`;
    
  });
  
  // --------------------------------------------------------------------------
  
  
  var first_primka_table =
  
`
<div class="divTable greyGridTable cit_break_page">
  ${table_header}
  <div class="divTableBody">
    ${just_items_rows}
  </div>
</div>  
  

`;  
  
  
  var first_part = iznad_tablice + first_primka_table;
  
  

  table_body = ``;
  table_footer = ``;
  
  
  var location_rows = ``;
  
  
  // --------------------------------------------------------------------------
  $.each( data.items, function(ind, item) {
   
   
    var loc_last_row =  `cit_break_page`;
    
    // ako je zadnji item onda nemoj stavljati ovu klasu koja označava page break u printu (ako to ostavim onda printa zadnji page prazan !!!!!)
    if ( ind == data.items.length - 1 ) loc_last_row = ``;
    
    dis_stupac = ima_popust ? `<div class="divTableCell" style="width: 10%;">${ cit_format(item.discount, 2)+"%" || "" }</div>`: "";
    
    location_rows += 
`

${iznad_tablice}
<div class="divTable greyGridTable">
  ${ table_header }
  <div class="divTableBody">
    <div class="divTableRow">

        <div class="divTableCell" style="text-align: center; width: 10%;" >${ind+1}.</div>
        <div class="divTableCell" style="text-align: left; width: 30%;" >${item.naziv}</div>
        <div class="divTableCell" style="text-align: center; width: 10%;" >${item.jedinica}</div>
        <div class="divTableCell" style="text-align: center; width: 10%;" >${ cit_format(item.order_count, 0) }</div>
        <div class="divTableCell" style="text-align: center; width: 10%;" >${ cit_format(item.primka_count, 0) }</div>
        <div class="divTableCell realni_count" style="text-align: center; width: 10%;" >${ "" }</div>
        <div class="divTableCell realni_count" style="text-align: center; width: 10%;" >${ "" }</div>
        <div class="divTableCell realni_count" style="text-align: center; width: 10%;" >${ "" }</div>

    </div>
  </div>
</div>

  
<!-- NASLOVNA TABLICA TJ. RED -->  
<div class="divTable greyGridTable title_location_row" style="background: #e2fafd">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 2px solid #333; border-bottom: 2px solid #333 !important;" >


      <div class="divTableCell" style="width: 25%; border-right: 2px solid transparent;">LOKACIJA</div>
      <div class="divTableCell" style="width: 30%; border-right: 2px solid transparent;">PALETA/PAKIRANJA</div>
      <div class="divTableCell" style="width: 10%; border-right: 2px solid transparent;">KOM</div>
      <div class="divTableCell" style="width: 20%; border-right: 2px solid transparent;">ZAUZIMA PROSTOR D/Š/V u mm</div>
      <div class="divTableCell" style="width: 15%;">UK. KOM</div>


    </div>

  </div>
</div> 



<div class="divTable greyGridTable location_row" style="background: #fff; border: none;">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 0px solid #333; border-bottom: 1px solid #333 !important;" >

      <div class="divTableCell" style="width: 25%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 30%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 10%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 20%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 15%;">&nbsp;</div>


    </div>

  </div>
</div>

<div class="divTable greyGridTable location_row" style="background: #fff; border: none;">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 0px solid #333; border-bottom: 1px solid #333 !important;" >

      <div class="divTableCell" style="width: 25%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 30%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 10%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 20%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 15%;">&nbsp;</div>


    </div>

  </div>
</div>

<div class="divTable greyGridTable location_row" style="background: #fff; border: none;">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 0px solid #333; border-bottom: 1px solid #333 !important;" >

      <div class="divTableCell" style="width: 25%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 30%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 10%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 20%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 15%;">&nbsp;</div>


    </div>

  </div>
</div>

<div class="divTable greyGridTable location_row" style="background: #fff; border: none;">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 0px solid #333; border-bottom: 1px solid #333 !important;" >

      <div class="divTableCell" style="width: 25%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 30%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 10%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 20%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 15%;">&nbsp;</div>


    </div>

  </div>
</div>

<div class="divTable greyGridTable location_row" style="background: #fff; border: none;">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 0px solid #333; border-bottom: 1px solid #333 !important;" >

      <div class="divTableCell" style="width: 25%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 30%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 10%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 20%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 15%;">&nbsp;</div>


    </div>

  </div>
</div>

<div class="divTable greyGridTable location_row" style="background: #fff; border: none;">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 0px solid #333; border-bottom: 1px solid #333 !important;" >

      <div class="divTableCell" style="width: 25%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 30%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 10%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 20%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 15%;">&nbsp;</div>


    </div>

  </div>
</div>

<div class="divTable greyGridTable location_row" style="background: #fff; border: none;">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 0px solid #333; border-bottom: 1px solid #333 !important;" >

      <div class="divTableCell" style="width: 25%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 30%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 10%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 20%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 15%;">&nbsp;</div>


    </div>

  </div>
</div>

<div class="divTable greyGridTable location_row" style="background: #fff; border: none;">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 0px solid #333; border-bottom: 1px solid #333 !important;" >

      <div class="divTableCell" style="width: 25%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 30%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 10%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 20%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 15%;">&nbsp;</div>


    </div>

  </div>
</div>


<div class="divTable greyGridTable location_row ${loc_last_row}" style="background: #fff; border: none;">  
  <div class="divTableBody">  
    
    <div class="divTableRow" style="border-top: 0px solid #333; border-bottom: 1px solid #333 !important;" >


      <div class="divTableCell" style="width: 25%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 30%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 10%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 20%; border-right: 1px solid #333;">&nbsp;</div>
      <div class="divTableCell" style="width: 15%;">&nbsp;</div>


    </div>

  </div>
</div>
  
   

`;
    
  });
  
  // --------------------------------------------------------------------------
    
  
  
  var komentar = data.doc_komentar ? `<br>Napomena:<br>${ data.doc_komentar.replace(/\n/g, "<br />") }` : "";
  
  return (first_part + location_rows + komentar);
  
}; 
this_module.make_table_in_pro_record = make_table_in_pro_record;
    
  
  
function make_table_radni_nalog(data, lang) {
  
  
  var iznad_tablice = ``;
  var table_header = ``;
  var table_body = ``;
  var table_footer = ``;
  var doc_table = ``;

    
iznad_tablice =   
    
`
<b>
ROK ZA PROIZVODNJU: ${ data.rok ? cit_dt(data.rok, `y-m-d`).date : "" }<br>
<br>
PRIREZ VAL: ${data.prirez_val}<br>
PRIREZ KONTRAVAL: ${data.prirez_kontra}<br>
POLOVANJKA: ${data.polov == true ? "DA" : "NE" }<br>
BROJ KLAPNI: ${data.broj_klapni}<br>
</b>
`;    

table_header =
`
<div class="divTableHeading">
  <div class="divTableRow">
  
    <div class="divTableHead" style="width: 40%;">ULAZI</div>
    <div class="divTableHead" style="width: 20%;">PROCES</div>
    <div class="divTableHead" style="width: 40%;">IZLAZI</div>
    
  </div>
</div>        
`;   
   
  
  var table_rows = ``;
    
    
    // kada se radi o cijenama obavezno  cijena / data.ex_rate !!!!
    // kada se radi o cijenama obavezno  cijena / data.ex_rate !!!!
    // kada se radi o cijenama obavezno  cijena / data.ex_rate !!!!
    
  $.each( data.rn_items, function(ind, item) {

    table_rows += 
`
<div class="divTableRow">

    <div class="divTableCell" style="text-align: left;" >${item.ulazi}</div>
    <div class="divTableCell" style="text-align: left;" >${item.proces}</div>
    <div class="divTableCell" style="text-align: left;" >${item.izlazi}</div>

</div>

`;
    
  });
  
    
table_body = `<div class="divTableBody">${ table_rows }</div>`;
    
    
table_footer = 
  
`
<div class="divTableFoot tableFootStyle">
  <div class="divTableRow">
  
    <div class="divTableCell" style="text-align: center;" >&nbsp;</div>
    <div class="divTableCell" style="text-align: center;" >&nbsp;</div>
    <div class="divTableCell" style="text-align: center;" >&nbsp;</div>
    
  </div>
</div>


`;  
    
     
doc_table = 
`
<div class="divTable greyGridTable">
  ${ table_header }
  ${ table_body }
  ${ table_footer }
</div>
`;     

  
  var komentar = data.doc_komentar ? `<br>Napomena:<br>${ data.doc_komentar.replace(/\n/g, "<br>") }<br><br> ` : "";
  // u nalog stavljam komentar prvi pa onda sve ostalo
  return ( komentar + iznad_tablice + doc_table);
  
}; 
this_module.make_table_radni_nalog = make_table_radni_nalog;
  
  
function create_sklad_card(data, lang, page_index) {
  
  var sklad_card_html = ``;
  var table_header = ``;
  var table_body = ``;
  var table_footer = ``;
  var doc_table = ``;

  // upiši odgovorajajuću šifru u doc num !!!
  data.doc_num = data.palet_sifre[page_index];
  
  
  var kolicina = 0;
  if ( data.palet_unit_count ) kolicina = data.palet_unit_count;
  if ( data.box_unit_count ) kolicina = data.box_unit_count;
  
  
  var page_break_class = `cit_break_page`;
  // nemoj stavljati page break na zadnjem papiru
  if ( page_index == data.palet_sifre.length - 1 ) page_break_class = ``;

        
  var dummy_header = ``;
  // na svakom page-u osim prvom dodaj ovaj div da pogura skladišnu karticu na pravo mjesto na dno
  // dummy header mora u sebi imati visinu prethodnog footera, prethodnog headera i plus 122 px za opće podatke
  if ( page_index > 0 ) dummy_header = `<div style="height: calc(37mm + 25mm + 122px); width: 100%;"></div>`;
  
  
sklad_card_html =   
    
`
  
  ${dummy_header}
  <!-- OVO JE SAMO DIV DA BUDE GORNJA STRANA PAPIRA POSVE PRAZNA TAKO DA SE MOŽE UKAČITI IZMEĐU PLOČA NA PALETI -->
  <div style="height: 50mm; width: 100%;"></div>

<b style="margin-left: 20px;" >DATUM KREIRANJA: ${ data.rok ? cit_dt(data.rok, `y-m-d`).date_time : "" }</b><br>
<b style="margin-left: 20px;" >KREIRAO: ${ data.referent ? data.referent : "" }</b><br>
<br>

<div  class="${page_break_class}" style=" width: calc(100% - 40px); display: flex; margin: 0 20px;">

  <div style="width: 280px;">


    <div id="doc_qrcode_${data.palet_sifre[page_index]}" style="width: 100%;">
      <!-- OVDJE GENERIRAM QR CODE  -->
    </div>
    <img src="../../velprom_logo.png" style="width: 256px; height: auto; display: block; margin-top: 3mm;" />
    <div style="width: 100%; font-size: 3.4mm; line-height: 5mm; margin-top: 1mm;">
      <b> 
      Poštanska 7, 10410 V. GORICA, HRVATSKA<br>
      OIB: 33428207264&nbsp;&nbsp;Tel: +385 1 62 15529
      </b>
    </div>

  </div>

  <div style="width: calc(100% - 280px);">
    
    <div style="display: flex">

      <div class="primka_num" 
          style=" font-size: 50px; font-weight: 700; line-height: 12mm; color: #000;" >
          
        ${ data.primka_record?.doc_sifra || data.primka_pro_record?.doc_sifra || "" }
      </div>

    </div>
    

    <div style="display: flex">

      <div class="palet_sifra" >${ data.palet_sifre[page_index] }</div>

    </div>

    <div class="palet_kolicina" >${ kolicina || "2.500.000" } <span class="jedinica_kolicine_small">${ data.jedinica }</span></div>

    <h3>${ data.full_record_name } </h3>

    <div class="location_name">
      <div style="width: 50%">SKLADIŠTE:</div>
      <div style="width: 50%; text-align: right;">${ data.sklad?.short || `` }</div>
    </div>

    <div class="location_name">
      <div style="width: 50%">SEKTOR:</div>
      <div style="width: 50%; text-align: right;">${ data.sector?.sifra || `` }</div>
    </div>

    <div class="location_name">
      <div style="width: 50%">RAZINA:</div>
      <div style="width: 50%; text-align: right;">${ data.level?.sifra || `` }</div>
    </div>

    <div class="location_name">
      <div style="width: 50%">POLICA:</div>
      <div style="width: 50%; text-align: right;">${ data.shelf?.naziv || `` }</div>
    </div>

  </div>

</div>


`;   
  
  
  return sklad_card_html;
  
  
}; 
  
  
function make_table_sklad_kartica(data, lang) {
  
  var all_sklad_card_pages = ``;
  
  $.each(data.palet_sifre, function(p_ind, p_sifra) {
    all_sklad_card_pages += create_sklad_card(data, lang, p_ind);
  });

  return all_sklad_card_pages;
  
}; 
this_module.make_table_sklad_kartica = make_table_sklad_kartica;
 
  
  
  
  
  
function create_exit_card(data, lang, page_index) {
  
  var sklad_card_html = ``;
  var table_header = ``;
  var table_body = ``;
  var table_footer = ``;
  var doc_table = ``;

  // upiši odgovorajajuću šifru u doc num !!!
  data.doc_num = data.palet_sifre[page_index];
  
  
  var kolicina = 0;
  if ( data.palet_unit_count ) kolicina = data.palet_unit_count;
  if ( data.box_unit_count ) kolicina = data.box_unit_count;
  
  
  var page_break_class = `cit_break_page`;
  // nemoj stavljati page break na zadnjem papiru
  if ( page_index == data.palet_sifre.length - 1 ) page_break_class = ``;

        
  var dummy_header = ``;
  // na svakom page-u osim prvom dodaj ovaj div da pogura skladišnu karticu na pravo mjesto na dno
  // dummy header mora u sebi imati visinu prethodnog footera, prethodnog headera i plus 122 px za opće podatke
  if ( page_index > 0 ) dummy_header = `<div style="height: calc(37mm + 25mm + 122px); width: 100%;"></div>`;
  
  
sklad_card_html =   
    
`
  
  ${dummy_header}
  <!-- OVO JE SAMO DIV DA BUDE GORNJA STRANA PAPIRA POSVE PRAZNA TAKO DA SE MOŽE UKAČITI IZMEĐU PLOČA NA PALETI -->
  <div style="height: 50mm; width: 100%;"></div>

<b style="margin-left: 20px;" >DATUM KREIRANJA: ${ data.rok ? cit_dt(data.rok, `y-m-d`).date_time : "" }</b><br>
<b style="margin-left: 20px;" >KREIRAO: ${ data.referent ? data.referent : "" }</b><br>
<br>

<div  class="${page_break_class}" style=" width: calc(100% - 40px); display: flex; margin: 0 20px;">
  
    <div style="display: flex">

      <div  class="otp_num" 
            style=" font-size: 50px; font-weight: 700; line-height: 12mm; color: #000;" >
          
        ${ data.otp_record?.doc_sifra || "" }
        
      </div>

    </div>
    

    <div style="display: flex">

      <div class="palet_sifra" >${ data.palet_sifre[page_index] }</div>

    </div>

    <div class="palet_kolicina" >${ kolicina || "" } <span class="jedinica_kolicine_small">${ data.jedinica }</span></div>

    <h3>${ data.full_record_name } </h3>

    <div class="location_name">
     
    </div>

    <div class="location_name">
      <div style="width: 50%">SEKTOR:</div>
      <div style="width: 50%; text-align: right;">${ data.sector?.sifra || `` }</div>
    </div>

    <div class="location_name">
      <div style="width: 50%">RAZINA:</div>
      <div style="width: 50%; text-align: right;">${ data.level?.sifra || `` }</div>
    </div>

    <div class="location_name">
      <div style="width: 50%">POLICA:</div>
      <div style="width: 50%; text-align: right;">${ data.shelf?.naziv || `` }</div>
    </div>


</div>


`;   
  
  
  return sklad_card_html;
  
  
}; 
  
    
  
  
  
  
function make_table_exit_kartica(data, lang) {
  
  var all_exit_card_pages = ``;
  
  $.each(data.palet_sifre, function(p_ind, p_sifra) {
    all_exit_card_pages += create_exit_card(data, lang, p_ind);
  });

  return all_exit_card_pages;
  
}; 
this_module.make_table_exit_kartica = make_table_exit_kartica;
   
  
  

function update_doc_sifra_in_sirov_records(data, arg_pdf ) {
  
  
  
  var data = cit_deep(data);
  
  if ( data.doc_type == "radni_nalog" ) {

    if ( data.switch_elem ) delete data.switch_elem;
    if ( data.kalk ) delete data.kalk;
    if ( data.pro_kalk ) delete data.pro_kalk;
    
  };

  return new Promise(function(resolve, reject) {

    $.ajax({
      headers: {
        'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
      },
      type: "POST",
      cache: false,
      url: `/update_doc_sifra_in_sirov_records`,
      data: JSON.stringify({
        
        data_for_doc: data,
        
        doc_sifra: data.doc_sifra,
        doc_type: data.doc_type,
        proj_sifra: data.proj_sifra,
        pdf_name: arg_pdf.ime_filea,
        pdf_time: arg_pdf.time,

      }),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done(function (result) {

      console.log(result);

      if ( result.success == true ) {
        
        resolve(true);

      } else {
        if ( result.msg ) popup_error( `Greška prilikom spremanja šifre dokumenta u povijest sirovine!<br>` + result.msg );
        if ( !result.msg ) popup_error( `Greška prilikom spremanja šifre dokumenta u povijest sirovine!` );
      };

    })
    .fail(function (error) {
      console.log(error);
      resolve(null);
    })
    .always(function() {

    });
    
  }); // kraj promisa

  
  
}; 
this_module.update_doc_sifra_in_sirov_records = update_doc_sifra_in_sirov_records;  

  
  
  
async function save_pdf_and_record(arg_button, arg_make_record) {

  
  // id je uvijek isti ( cit_preview_modal) pa odmah znam property zato što je UVIJEK JEDNA JEDINA INSTANCA !!!
  var data = this_module.cit_data.cit_preview_modal;

 
  if ( !window.cit_user )  {
    popup_warn( `Niste ulogirani!`);
    return;
  };
  

  var counter_result = await get_new_counter(data.doc_type, false);
  
  
  var iframe_reference = document.getElementById("printiFrame");
  var iframe_content = (iframe_reference.contentWindow || iframe_reference.contentDocument);
  var broj_dokumenta_box = $(iframe_content.document.documentElement).find(`.broj_dokumenta`);

  var doc_prefix = ``;


  if ( data.doc_type == `offer_dobav` ) doc_prefix = `RFQ`; // zahtjev za ponudu prema dobavljaču
  if ( data.doc_type == `order_dobav` ) doc_prefix = `ND`; // narudžba dobavljaču
  if ( data.doc_type == `sklad_kartica` ) doc_prefix = `SKL`;
  
  if ( data.doc_type == `in_record` ) doc_prefix = `PR`; // primka
  if ( data.doc_type == `in_pro_record` ) doc_prefix = `PP`; // primka iz proizvodnje
  

  if ( data.doc_type == `offer_reserv` ) doc_prefix = `PK`;
  if ( data.doc_type == `order_reserv` ) doc_prefix = `NK`;

  if ( data.doc_type == `radni_nalog` ) doc_prefix = `RN`;

  var doc_sifra = doc_prefix + counter_result.year + counter_result.month + `-` + String(window.cit_user.user_number).padStart(2, "0") + `-`+ counter_result.number;

  broj_dokumenta_box.text(doc_sifra);
  
  data.doc_sifra = doc_sifra;

  if ( !doc_sifra ) {
    return;
  };

  $(arg_button).css("pointer-events", "none");
  
  var show_popup_modal = this_module.pop_mod.show_popup_modal;
  show_popup_modal(true, `Spremanje PDF dokumenta u toku !!!`, null, false, null, null, null);


  var iframe_reference = document.getElementById("printiFrame");
  var iframe_content_reference = (iframe_reference.contentWindow || iframe_reference.contentDocument);
  var html_sadrzaj = iframe_content_reference.document.documentElement.outerHTML

  // var cont = document.getElementById('printiFrame').contentWindow.document;
  // console.log(cont.documentElement.outerHTML, cont.doctype);


  $.ajax({
    headers: {
      'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
    },
    type: "POST",
    cache: false,
    url: `/save_pdf`,
    data: JSON.stringify({

      html: html_sadrzaj,
      user: { 
        user_number: window.cit_user.user_number,
        email: window.cit_user.email,
        full_name: window.cit_user.full_name
      },
      doc_type: data.doc_type,
      orient: data.orient || "portrait",

    }),
    contentType: "application/json; charset=utf-8",
    dataType: "json"
  })
  .done( async function (result) {

    console.log(result);

    if ( result.success == true ) {

      cit_toast(`PDF DOKUMNET JE SPREMLJENI`);


      // ako user NE ŽELI zapisati novi record onda samo spremi PDF u sirov specs
      if ( 
        ( 
          data.doc_type == `offer_dobav`   || 
          data.doc_type == `order_dobav`   ||
          data.doc_type == `in_record`     ||
          data.doc_type == `in_pro_record`             
        ) 
        &&
        arg_make_record
      ) {
        
        if ( data.partner_data ) { 
          
          
          // ---------------------START--------------------------- PARTNER NABAVA ------------------------------------------------
          async function on_doc_loop_finish(rec_result) {
            
            var partner_module = await get_cit_module(`/modules/partners/partner_module.js`, `load_css`);
            var this_comp_id = $(`#save_partner_btn`).closest('.cit_comp')[0].id;
            var partner_data = partner_module.cit_data[this_comp_id];
            var rand_id = partner_module.cit_data[this_comp_id].rand_id;
 
            
            // -------------------------- ZA PARTNER MODULE --------------------------
            partner_data.docs.push( rec_result.data_for_doc ); // uzimam zadnji data for doc jer je uvijek isti za bilo koju sirovinu

            partner_module.make_partner_doc_list(partner_data);
            
            $(`#save_partner_btn`).trigger(`click`);
            
            setTimeout ( function() { 
  
              $(`#cit_preview_modal`).removeClass(`cit_show`);

              // anuliraj listu sirovina u partneru i removaj listu sirovina !!!!
              partner_data.partner_doc_sirovs = null;
              $(`#doc_sirovs_list_box`).html(``);
              
            }, 300);
            
          }; // kraj doc loop finish
          
          function run_partner_doc_loop(data, arg_pdf, arg_index) {
            
            this_module.sirov_module.sirov_status_and_record( data, arg_pdf, data.doc_sirovs, arg_index )
            .then(
            function(rec_result) {
              console.log(`-- loop --- za sirovinu ---- ` + data.doc_sirovs[arg_index].sirovina_sifra );

              if ( arg_index == data.doc_sirovs.length - 1 ) {
                // spremi dokument unutar partnera
                on_doc_loop_finish(rec_result);
              } else {
                // ponovo pozovi ovu func sa sljedećim indexom !!!!
                run_partner_doc_loop(data, arg_pdf, arg_index+1);
              };
              
            })
            .catch( function(error) {
              console.error(` ------------------------------ ERROR U LOOP ZA KREIRANJE DOCS U PARTNERU ------------------------------ `);
              console.log(error);
            }); 

          }; // END run partner doc loop 
          
          
          run_partner_doc_loop(data, result, 0);
          
          // --------------------END---------------------------- PARTNER NABAVA ------------------------------------------------
          
        }
        else {
          // ------------------------------------------------ SIROV NABAVA  OVO NE KORISTIM VIŠE ------------------------------------------------
           var new_record = await this_module.sirov_module.sirov_status_and_record( data, result );
          
          
          setTimeout ( function() { 
            $(`#cit_preview_modal`).removeClass(`cit_show`);
          }, 300);
          
          
        };
        
      };
      
      
      if ( 
        (
          data.doc_type == `offer_reserv` ||
          data.doc_type == `order_reserv` || 
          data.doc_type == `radni_nalog` 
        ) 
        &&
        arg_make_record
      ) {
         
        
        if ( data.doc_type == `radni_nalog` ) {
          
          var saved_rn_records = await this_module.kalk_module.save_RN_records(this_module.kalk_module, data);
          
        };
        
        // ovo se sada događa na razini PROJECTA i zato trebam napraviti update recorda
        // u svakoj sirovini i u svakom recordu koji ima u sebi trenutni proj_sifra !!!!!
        
        var records_updated = await this_module.update_doc_sifra_in_sirov_records(data, result ); // result je ovdje PDF podaci tje pdf ime i vrjeme spremanja
        
        var status_result = null;
        if ( data.doc_type == `radni_nalog` ) status_result = await this_module.kalk_module.make_radni_nalog_statuses(data, result );
        
        this_module.proj_module.save_doc_in_proj_specs( data, result );
        
        setTimeout ( function() { 
          $(`#cit_preview_modal`).removeClass(`cit_show`);
        }, 300);
        
      };
   
    } else {
      if ( result.msg ) popup_error( `Greška prilikom spremanja PDF dokumenta!<br>` + result.msg );
      if ( !result.msg ) popup_error( `Greška prilikom spremanja PDF dokumenta!` );
    };

  })
  .fail(function (error) {

    console.log(error);
    popup_error(`Došlo je do greške na serveru prilikom spremanja ovog zapisa za Sirovinu!`);
  })
  .always(function() {

    toggle_global_progress_bar(false);
    $(arg_button).css("pointer-events", "all");
    
    // ugasi popup za spremanje pdf
    show_popup_modal(false, `Spremanje PDF dokumenta u toku !!!`, null, false, null, null, null);
  

  });

};  
this_module.save_pdf_and_record = save_pdf_and_record; 

  
  
  
  async function get_modules() {
    this_module.sirov_module = await get_cit_module(`/modules/sirov/sirov_module.js`, `load_css`);
    this_module.pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
    this_module.kalk_module = await get_cit_module(`/modules/kalk/kalk_module.js`, `load_css`);
    this_module.product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
    this_module.proj_module = await get_cit_module(`/modules/project/project_module.js`, `load_css`);
  };
  get_modules();
  
  
  
  
  
  
  
  
  this_module.cit_loaded = true;  
 
} /* kraj od module scripts  */
  
  
};