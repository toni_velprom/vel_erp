async function alert_deficit(this_elem) {
  
  
  var this_comp_id = $(this_elem).closest('.cit_comp')[0].id;
  var data = this_module.cit_data[this_comp_id];
  var rand_id =  this_module.cit_data[this_comp_id].rand_id;
  
  if ( !data.current_status ) {
    console.error(`NEDOSTAJE data.current_status UNUTAR FUNC alert deficit !!!!`);
    return;
  };
  
  if ( !data.current_proces ) {
    console.error(`NEDOSTAJE data.current_proces UNUTAR FUNC alert deficit !!!!`);
    return;
  };
  
  
  /*
  var curr_pro_ulaz = get_pro_kalk_val( data.current_status.kalk, data.current_proces.row_sifra, `ulazi`, data.current_prodon_ulaz.sifra );
  var curr_pro_izlaz = get_pro_kalk_val( data.current_status.kalk, data.current_proces.row_sifra, `izlazi`, data.current_prodon_izlaz.sifra );

  var curr_post_ulaz = get_post_kalk_val( data.current_status.kalk, data.current_proces.row_sifra, `ulazi`, data.current_prodon_ulaz.sifra  );
  var curr_post_izlaz = get_post_kalk_val( data.current_status.kalk, data.current_proces.row_sifra, `izlazi`, data.current_prodon_izlaz.sifra );
  */

  var izlaz_deficit = ``;

  $.each( data.current_status.kalk.post_kalk[0].procesi, function(p_ind, post_proc) {

    if ( izlaz_deficit == `` && proc.row_sifra == data.current_proces.row_sifra ) {

      $.each( post_proc.izlazi, function(izlazi_ind, post_izlaz) {

        if ( izlaz_deficit == `` ) {
          // ////////////////////////////////////////////////////////////////////////

          // iste je sifra za proces i za izlaz u POST KAO I U PRO
          var pro_izlaz = get_pro_kalk_val( data.current_status.kalk, post_proc.row_sifra, `izlazi`, post_izlaz.sifra );
          // izlaz kalk sum sirovina je uvijek nastao kao zbroj  kalk sum sirovina  + extra !!!
          // DAKLE AKO JE IZLAZ MANJI OD KALK SUM SIROVINA ONDA NEMAM DOVOLJNO
          var post_izlaz_kom = pro_izlaz.kalk_sum_sirovina + pro_izlaz.extra_kom - (post_izlaz.kalk_skart || 0);

          final_izlazi_poruka += izlaz.prodon_name + ` GOTOVO: ` + post_izlaz_kom + ` / ŠKART: ` + (post_izlaz.kalk_skart || 0) + `<br>`;

          if ( pro_izlaz.extra_kom < (post_izlaz.kalk_skart || 0) ) {
            izlaz_deficit = `NEMA DOVOLJNO ` + post_izlaz.prodon_name + `!!!`;
          };

          // ////////////////////////////////////////////////////////////////////////
        }; // jel izlaz deficit == false


      }); // loop po izlazima current procesa

    }; // jel našao current proces row sifra

  }); // loop po post kalk



    var popup_text = `PROVJERI PODATKE !!!<br><br>` + final_izlazi_poruka;
    
    async function poruka_yes() {
      
      var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;


      var work_cat = { sifra: "WCAT10", naziv: "RN GOTOV" };

      if ( !data.current_status.work_times ) data.current_status.work_times = [];


      // ovo nije bitno jer proces gotov znači da svi prestaju s radom
      // var found_unfinished_work = false;  

      $.each( data.current_status.work_times, async function( w_ind, work_obj) {

        if ( 

          /* SADA NIJE BITNO KOJA JE KATEGORIJA SAMO DA NIJE DOVRŠENO  */
          // work_obj.worker.user_number == window.cit_user.user_number // traži samo trenutno ulogiranog usera
          // &&
          // NIJE BITNO KOJI JE USER  ---- > SVAKOM USER-u NAPIŠI DA JE GOTOV S RADOM JE SADA PROCES GOTOV !!!

          work_obj.production 
          &&
          work_obj.work_from_time 
          &&
          !work_obj.work_to_time

        ) {

          data.current_status.work_times[w_ind].work_to_time = curr_time;

          if ( work_obj.work_from_time ) {
            data.current_status.work_times[w_ind].duration = ( curr_time - work_obj.work_from_time ) / (1000*60);
          };

          // var saved_status = await this_module.save_work(null, this_button, work_obj);

        };

      }); // kraj loopa po work times
      

      var work_desc = $('#'+rand_id+`_prodon_work_desc`).val();

      // za sve ove kategorije vrijeme početno i završno je zapravo isto vrijeme
      var end_time = null;
      if ( work_cat?.sifra == `WCAT6`) end_time = curr_time; // kraj radnog vremena
      if ( work_cat?.sifra == `WCAT7`) end_time = curr_time; // dobio drugi zadatak
      if ( work_cat?.sifra == `WCAT10`) end_time = curr_time; // gotov RN proces !!!!

      var arg_work = {

        sifra: `work` + cit_rand(),

        worker: {
          _id: window.cit_user._id, 
          user_number: window.cit_user.user_number,
          email: window.cit_user.email,
          full_name: window.cit_user.full_name,
          name: window.cit_user.name,
        },

        time: curr_time,
        work_from_time: curr_time,
        work_to_time: end_time, 
        duration: null,
        desc: work_desc || "",

        work_cat: work_cat,
        production: true,

      };
      
      

      data.current_status.kalk = run_prodon_update_kalk( 

        data.current_status.kalk,
        data.current_proces.row_sifra,
        `izlazi`,
        data.current_prodon_izlaz.sifra,
        curr_post_izlaz
      );
      
      

      data.statuses = upsert_item( data.statuses, data.current_status, `sifra` );
      var saved_status = await this_module.save_work( null, this_button, arg_work);
      
      var kalk_mod = await get_cit_module(`/modules/kalk/kalk_module.js`, `load_css`);
      var rn_updated_statuses = await kalk_mod.ajax_proces_rn_update_statuses( data.current_status.kalk._id, data.proces_id, `rn_finished` );
      
      
    }; // kraj poruka_yes
    
    function poruka_no() {
      
      show_popup_modal(false, popup_text, null );

      // resetiraj za sljedeći STOP !!!!
      data.temp_work_cat = null;
      // resetiraj sve check boxove u HTML-u
      $(`#prodon_check_boxes .cit_check_box`).each( function() {
        $(this).addClass(`off`).removeClass(`on`);
      });
      
    };
    
    
    var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
    var show_popup_modal = pop_mod.show_popup_modal;
    show_popup_modal(`warning`, popup_text, null, 'yes/no', poruka_yes, poruka_no, null);
  
  


  
  
  
  
};




async function update_utrosak_in_roba( this_module, data, curr_ulaz, input_value ) {
  
  return new Promise( async function (resolve, reject) {

    if ( curr_ulaz._id ) {

      var all_sirov_records = [];


      var new_record = {

        type: `utrosak`,


        sirov_id: sirov._id,

        count: sirov.sum_kolicina,

        record_parent: null,

        time: Date.now(),

        sifra: `record`+cit_rand(),

        doc_sifra: null,
        pdf_name: null,
        doc_link: null,


        record_statuses: null,

        record_kalk: {
          _id: data._id || null,
          proj_sifra: data.proj_sifra,
          item_sifra: data.item_sifra,
          variant: data.variant,
          kalk_sifra: curr_kalk.kalk_sifra, // samo uzmi sifru
          full_product_name: product_data.full_product_name,

        },

        record_est_time: product_data?.rok_za_def || null, // procjena vremena kada će doći do promjene

        storno_time: null,
        storno_kolicina: null,

        pk_kolicina: sirov.sum_kolicina,
        nk_kolicina: null,
        rn_kolicina: null,
        order_kolicina: null,
        sklad_kolicina: null,

        partner_name: project_data.kupac.naziv,
        partner_adresa: project_data.doc_adresa,
        partner_id: project_data.kupac._id,

      };

      all_sirov_records.push(new_record);

      // ------------ END ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )
      // ------------ END ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )
      // ------------ END ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )

      var records_result = await this_module.save_sirov_records(all_sirov_records);


    }; // kraj ako ulaz ima _id

    
  }); // kraj promisa  
    
};



async function check_skl_card(arg_ulaz) {
  
  return new Promise( function(resolve, reject) {
  
    var skl_sifra = $(`#${rand_id}_prodon_ulaz_palet_sifra`).val();

    if ( arg_ulaz._id ) {

      if ( skl_sifra == "" ) {
        popup_warn(`OBAVEZNO UPISATI SKLADIŠNU ŠIFRU (4 ZNAKA) !!!`);
        return;
      };
       
       

    } else {
      
      resolve(true);
      
      
    };
  
  }); // kraj promisa
  
};



async function write_work_efect( this_module, data, arg_ulaz, arg_ulaz_skart, arg_izlaz, arg_izlaz_skart ) {


  $.each( data.current_status.work_times, function( w_ind, work_obj) {

    if ( 

      // ovdje ne tražim specifično user nego upisujem svim userima koji trenutno rade

      work_obj.work_cat.sifra == `WCAT1` // ovo je kategorija RAD
      &&
      work_obj.production
      &&
      work_obj.work_from_time // ima početak
      &&
      !work_obj.work_to_time // nema kraj
    ) {

      if ( arg_ulaz !== null )        data.current_status.work_times[w_ind].ulaz = arg_ulaz;
      if ( arg_ulaz_skart !== null )  data.current_status.work_times[w_ind].ulaz_skart = arg_ulaz_skart;

      if ( arg_izlaz !== null )       data.current_status.work_times[w_ind].izlaz = arg_izlaz;
      if ( arg_izlaz_skart !== null ) data.current_status.work_times[w_ind].izlaz_skart = arg_izlaz_skart;

    };

  });


  data.statuses = upsert_item( data.statuses, data.current_status, `sifra` );


  var result = await this_module.ajax_save_status(data, `Product`, data.current_status?.product_id || null );

  if ( result?.success == true ) {
    cit_toast(`ZAPIS JE SPREMLJEN!`);
    this_module.make_status_list(data);
  };
  
  
  return result;

};


async function save_status_in_all_modules(this_button, result, Model_name) {

  
  return new Promise(async function(resolve, reject) {

    var status_module = await get_cit_module(`/modules/status/status_module.js`, `load_css`);


    // ------------------------------------------ PARTNER STATUSI ------------------------------------------
    // ------------------------------------------ PARTNER STATUSI ------------------------------------------

    var partner_comp = $("#save_partner_btn").closest('.partner_comp.cit_comp');

    if (Model_name == `Partner` && partner_comp.length > 0) {

      var partner_module = await get_cit_module(`/modules/partners/partner_module.js`, `load_css`);
      partner_comp_id = partner_comp[0].id;
      var partner_data = partner_module.cit_data[partner_comp_id];


      if ( result?.status ) {

        partner_data.statuses = upsert_item(partner_data.statuses, result.status, `sifra`);

        // status_module.make_status_list(partner_data);

        var status_data = {
          id: `sirovstatus`+cit_rand(),
          statuses: partner_data.statuses,
          goto_status: null,
          for_module: `partner`,

        };

        status_module.create( status_data, $(`#${partner_data.id}_statuses_box`) );

      };
      
      
      
      resolve(true);
      return;

    };

    // ------------------------------------------ PRODUCT STATUSI ------------------------------------------
    // ------------------------------------------ PRODUCT STATUSI ------------------------------------------

    // ubaci ovaj status također i u product data !!! kad budem radio save product da bude i taj novi status !!!
    var product_comp = $(this_button).closest('.product_comp');

    if ( window.location.hash.indexOf(`#production`) > -1 ) {
      // ako se nalazi na production page onda product component nije parent nego je zapravo sibiling
      // I JEDAN JEDINI PRODUCT JE NA TOJ STRANICI ZA PRODUCTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      product_comp = $(`body .product_comp`);
    };

    var product_comp_id = null;


    if ( (!Model_name || Model_name == `Product`) && product_comp.length > 0 ) {

      var save_product_btn = product_comp.find(`.save_product_btn`);

      product_comp_id = product_comp[0].id;
      var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
      var product_data = product_module.cit_data[product_comp_id];

      if ( result?.status ) {
        product_data.statuses = upsert_item(product_data.statuses, result.status, `sifra`);
      };

      setTimeout(function () {
        save_product_btn.trigger(`click`);
        resolve(true);
      }, 200);
      
      
      return;

    };

    // ------------------------------------------ SIROV STATUSI ------------------------------------------
    // ------------------------------------------ SIROV STATUSI ------------------------------------------

    var sirov_component = $(`#save_sirov_btn`).closest('.sirovine_body.cit_comp');
    var sirov_comp_id = null;
    var sirov_data = null;

    // ako je pronašao komponentu sirov tj ako se user nalazi u sirov modulu
    // i ako status ima sirovinu u sebi
    // onda ubaci ovaj premljeni status u sirov data u array statuses
    // to isto napravim server side !!!! ali ovo je samo da napravim update lokalno
    if (Model_name == `Sirov` && sirov_component.length > 0 && result.status.sirov?._id) {

      var sirov_module = await get_cit_module(`/modules/sirov/sirov_module.js`, `load_css`);
      sirov_comp_id = sirov_component[0].id;
      sirov_data = sirov_module.cit_data[sirov_comp_id];


      if ( result?.status ) {
        sirov_data.statuses = upsert_item(sirov_data.statuses, result.status, `sifra`);
      };

      // status_module.make_status_list(data);

      setTimeout(function () {
        $(`#save_sirov_btn`).trigger(`click`);
        resolve(true);
      }, 300);
      
      
      
      return;

    };
    
    
  }); // kraj promisa


};


async function get_sirov_status_parent(arg_sirov, data) {

  // ------------------------------------------------ FIND CURR SIROVINU i NJENE STATUSE iz baze ------------------------------------------------
  // ------------------------------------------------ FIND CURR SIROVINU i NJENE STATUSE iz baze ------------------------------------------------
  var db_sirovina = null;

  var props = {
    filter: null,
    url: '/find_sirov',
    query: {
      _id: arg_sirov._id || data._id
    },
    desc: `pretraga unutar kreiranja statusa za nabavu sirovine - trebam sve statuse sirovine  -------- : )`,
  };
  try {
    db_sirovina = await search_database(null, props);
  } catch (err) {
    console.log(err);
  };

  // ako je našao nešto onda uzmi samo prvi član u arraju
  if (db_sirovina && $.isArray(db_sirovina) && db_sirovina.length > 0) {
    db_sirovina = db_sirovina[0];
  };

  console.log(db_sirovina);

  // ------------------------------------------------ FIND CURR SIROVINU i NJENE STATUSE iz baze ------------------------------------------------
  // ------------------------------------------------ FIND CURR SIROVINU i NJENE STATUSE iz baze ------------------------------------------------



  var order_parent_status = null;

  if (db_sirovina.statuses?.length > 0) {
    // loop po svim statusima od ove sirovine !!!
    $.each(db_sirovina.statuses, function (s_ind, status) {
      // AKO JE  -----> "NABAVA - ZAHTJEV ZA PONUDU DOBAVLJAČA",
      // i nije gotov branch
      if (status.status_tip?.sifra == "IS1635256284641925" && !status.branch_done) {
        order_parent_status = cit_deep(status);
      };
    });
  };


};


// ----------------------------------- !!!! ZA SADA NE KORISTIM OVO !!!! -----------------------------------
function ajax_prodon_save_new_sirov( this_module, this_button ) {


  var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
  var data = this_module.cit_data[this_comp_id];
  var rand_id =  this_module.cit_data[this_comp_id].rand_id;


  return new Promise( function( resolve, reject) {


    if ( !data.current_prodon_izlaz ) {
      popup_warn(`Potrebno izabrati jedan od izlaza na listi !!!`);

      resolve(null);

      return;
    };

    var curr_izlaz = get_post_kalk_val(

      data.current_status.kalk,
      data.current_proces.row_sifra,
      `izlazi`,
      data.current_prodon_izlaz.sifra,

    );


    if ( curr_izlaz.prodon_sirov_id ) {
      resolve(curr_izlaz.prodon_sirov_id);
      return;
    };


    var new_sirov = cit_deep( this_module.sirov_module.new_sirov );

    var sirov_data = {

      naziv: curr_izlaz.prodon_name,
      cijena: 0,
      alt_cijena: 0,
      is_ostatak: true,
      osnovna_jedinica: { "sifra": "MJ4", "jedinica": "kom", "naziv": "Komada" }, 
      osnovna_jedinica_out: { "sifra": "MJ4", "jedinica": "kom", "naziv": "Komada" },
      sklad_kolicina: curr_izlaz.kalk_sum_sirovina || 0,
      dobavljac: velprom_firma._id,
      dobavljac_flat: JSON.stringify(velprom_firma),
      tip_sirovine: { sifra: `TSIR9PROC`, naziv: `PROCESNI ELEMENT` },

      kalk: data.current_status.kalk._id,
      proces_id: data.current_proces.row_sifra, // ovo je specifično kada sirovina nastaje kao procesni element u proizvodnji !!!

      ulaz_id: null,
      izlaz_id: curr_izlaz.sifra,

      locations: [ ],
      

    };

    new_sirov = { ...new_sirov, ...sirov_data };



    toggle_global_progress_bar(true);
    $(this_button).css("pointer-events", "none");


    $.ajax({
      headers: {
        'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
      },
      type: "POST",
      cache: false,
      url: `/save_sirov`,
      data: JSON.stringify( new_sirov ),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done( function (result) {

      console.log(result);

      if ( result.success == true ) {

        resolve( result.sirov._id );

      } else {
        if ( result.msg ) popup_error(result.msg);
        if ( !result.msg ) popup_error(`Greška prilikom spremanja novog PROCESNOG ELEMENTA KAO SIROVINE!`);
      };

    })
    .fail(function (error) {

      console.log(error);
      popup_error(`Došlo je do greške na serveru prilikom spremanja!`);
    })
    .always(function() {

      toggle_global_progress_bar(false);
      $(`#save_sirov_btn`).css("pointer-events", "all");


    });


  }); // kraj promisa

};


function get_pro_kalk_val( clean_kalk, proces_sifra, key, key_sifra ) {

  var curr_val = null;

  $.each( clean_kalk.pro_kalk[0].procesi, function(p_ind, proces) {


    if ( proces.row_sifra == proces_sifra ) {

      if ( $.isArray( proces[key] ) ) {

        $.each( proces[key], function(prop_ind, item) {
          if ( item.sifra == key_sifra ) {
            curr_val = clean_kalk.pro_kalk[0].procesi[p_ind][key][prop_ind];
          };

        });

      }; // kraj ako je proces[key] array


      // ako je ovo objekt onda je key_sifra zapravo pod propery

      if ( $.isPlainObject( proces[key] ) ) { 

        $.each( proces[key], function(prop_key, prop) {

          if ( prop_key == key_sifra ) {
            curr_val = clean_kalk.pro_kalk[0].procesi[p_ind][key][prop_key];
          };

        });

      };

    }; // kraj ako je nasao proces 

  }); // loop po PRO procesima !!!

  return curr_val;

};


function get_post_kalk_val( clean_kalk, proces_sifra, key, key_sifra ) {

  
  var curr_val = null;

  $.each( clean_kalk.post_kalk[0].procesi, function(p_ind, proces) {


    if ( proces.row_sifra == proces_sifra ) {

      if ( $.isArray( proces[key] ) ) {

        $.each( proces[key], function(prop_ind, item) {

          if ( item.sifra == key_sifra ) {
            curr_val = clean_kalk.post_kalk[0].procesi[p_ind][key][prop_ind];
          };

        });

      }; // kraj ako je proces[key] array


      // ako je ovo objekt onda je key_sifra zapravo pod propery

      if ( $.isPlainObject( proces[key] ) ) { 

        $.each( proces[key], function(prop_key, prop) {

          if ( prop_key == key_sifra ) {
            curr_val = clean_kalk.post_kalk[0].procesi[p_ind][key][prop_key];
          };

        });

      };

    }; // kraj ako je nasao proces 

  }); // loop po post procesima !!!

  return curr_val;

};


function run_prodon_update_kalk( clean_kalk, proces_sifra, key, key_sifra, val ) {

  /*
  PRIMJER:
  
      data.current_status.kalk = run_prodon_update_kalk( 

        data.current_status.kalk,
        data.current_proces.row_sifra,
        `ulazi`,
        data.current_prodon_ulaz.sifra,
        curr_ulaz
        
      );
  
  
  */
  
  

  $.each( clean_kalk.post_kalk[0].procesi, function(p_ind, proces) {


    if ( proces.row_sifra == proces_sifra ) {

      if ( $.isArray( proces[key] ) ) { // key često može biti "ulazi" ili "izlazi"

        $.each( proces[key], function(prop_ind, item) {
          if ( item.sifra == key_sifra ) {
            clean_kalk.post_kalk[0].procesi[p_ind][key][prop_ind] = val;
          };

        });

      }; // kraj ako je proces[key] array


      // ako je ovo objekt onda je key_sifra zapravo pod propery

      if ( $.isPlainObject( proces[key] ) ) {  // key često može biti "ulazi" ili "izlazi"

        $.each( proces[key], function(prop_key, prop) {

          if ( prop_key == key_sifra ) {
            clean_kalk.post_kalk[0].procesi[p_ind][key][prop_key] = val;
          };

        });

      };

    }; // kraj ako je nasao proces 

  }); // loop po post procesima !!!


  var clean_kalk = update_prodon_sirov_id_in_ulazi( clean_kalk, proces_sifra, key, key_sifra, val );

  
  return clean_kalk;

};


function update_prodon_sirov_id_in_ulazi( clean_kalk, proces_sifra, key, key_sifra, val  ) {
  
  
  // traži po svim procesima
  $.each( clean_kalk.post_kalk[0].procesi, function(proc_ind, find_proces) {


    $.each( find_proces.izlazi, function(izlaz_ind, find_izlaz) {

      var curr_izlaz = find_izlaz;


      // ------------------------------------------------- SAMO AKO IZLAZ IMA PRODON SIROV ID -------------------------------------------------
      if ( curr_izlaz.prodon_sirov_id ) {

        // ponovo loop po svim procesima
        $.each( clean_kalk.post_kalk[0].procesi, function(proc_ind_2, find_proces_2) {

          // -------------------------------------------po svim ulazima -------------------------------------
          $.each( find_proces_2.ulazi, function(ulaz_ind_2, find_ulaz_2) {

            // ako u procesu imam ULAZ koji je isti kao current izlaz  -----> DODAJ MU PRODON SIROV ID !!!!!!!!!!
            if ( find_ulaz_2.izlaz_sifra == curr_izlaz.sifra ) {

              clean_kalk.post_kalk[0].procesi[proc_ind_2].ulazi[ulaz_ind_2].prodon_sirov_id = curr_izlaz.prodon_sirov_id;

            };

          }); // loop po svim ulazima u procesu
          // --------------------------------------------po svim ulazima ------------------------------------


        });  // KRAJ ponovo loop po svim procesima

      }; // ako izlaz ima prodon sirov ID

    }); // loop po svim izlazima  

  }); // loop po svim procesima
  
  
  return clean_kalk;
  
};


function update_post_kalk( arg_id, arg_proces_sifra, arg_key, arg_key_sifra, arg_val ) {

  /*
  // MOŽE BITI BILO KOJI BUTTON NIJE BITNO !!!
  var this_button = $("#save_ulaz_utrosak")[0];
  var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
  var data = this_module.cit_data[this_comp_id];
  var rand_id =  this_module.cit_data[this_comp_id].rand_id;
  */


  toggle_global_progress_bar(true);

  return new Promise( function(resolve, reject) {

    // ----------------------------------------------------------------------------------------------------------------
    // START SAVE TO DB
    // ----------------------------------------------------------------------------------------------------------------

    $.ajax({
      headers: {
        'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
      },
      type: "POST",
      cache: false,
      url: `/update_post_kalk`,
      data: JSON.stringify({

        kalk_id: arg_id, 
        proces_sifra: arg_proces_sifra, 

        key: arg_key,
        key_sifra: arg_key_sifra,
        val: arg_val,

      }),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done(function (result) {


      if ( !result.success ) console.error(`SUCCESS JE FALSE ZA SPREMAJE POST KALKULACIJE !!!!`);

      resolve(result);



    })
    .fail(function (error) {

      console.log(error);
      popup_error(`Došlo je do greške na serveru prilikom spremanja!`);
      resolve(null);

    })
    .always(function() {

      toggle_global_progress_bar(false);

    });

    // ----------------------------------------------------------------------------------------------------------------
    // END SAVE TO DB
    // ----------------------------------------------------------------------------------------------------------------


  }); // kraj promisa

};


function register_prodon_events( this_module, data) {
  
  
  this_module.prodon_percent_setup(data);

  var rand_id = data.rand_id;


  /*

  $('#'+rand_id+'_velprom_smjena').data('cit_props', {

  desc: `odaberi tip unutar status modula u statusu ${rand_id}`,
  local: true,
  show_cols: ['naziv'],
  return: {},
  show_on_click: true,

  list: [

    { sifra: "SHIFT1", naziv: "1" },
    { sifra: "SHIFT2", naziv: "2" },
    { sifra: "SHIFT3", naziv: "3" },
    { sifra: "SHIFT4", naziv: "4" },


  ],

  filter: null,
  cit_run: function (state) {

    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;



    return;


    if ( state == null ) {
      $('#'+current_input_id).val(``);
      data.current_prodon_ulaz = null;
      $(`#`+ rand_id + `_prodon_utrosak_ukupno`).val(``);
      $(`#`+ rand_id + `_prodon_skart_ulaz_ukupno`).val(``);

    } 
    else {
      $('#'+current_input_id).val( state.prodon_name );
      data.current_prodon_ulaz = state;

      var curr_ulaz = this_module.get_post_kalk_val(

        data.current_status.kalk,
        data.current_proces.row_sifra,
        `ulazi`,
        data.current_prodon_ulaz.sifra,

      );



      var curr_pro_ulaz = this_module.get_pro_kalk_val(

        data.current_status.kalk,
        data.current_proces.row_sifra,
        `ulazi`,
        data.current_prodon_ulaz.sifra,

      );

      $(`#prodon_ulaz_title_row h4`).text(`POTREBNO: ${curr_pro_ulaz.kalk_sum_sirovina} kom`);


      format_number_input(curr_ulaz.kalk_sum_sirovina, $(`#`+ rand_id + `_prodon_utrosak_ukupno`)[0], null);
      format_number_input(curr_ulaz.kalk_skart, $(`#`+ rand_id + `_prodon_skart_ulaz_ukupno`)[0], null);


    };


  },

}); 

  */


    var check_box_array = [ 

    `prodon_drugi_zad`,
    `prodon_problem_nalog`,
    `prodon_problem_rad`,
    `prodon_problem_stroj`,
    `prodon_problem_worker`,
    `prodon_kraj_rad_vrem`,

  ];  


  $.each( check_box_array, function( generate_ch_ind, check_box_name ) {

    $('#' + data.rand_id + '_' + check_box_name ).data(`cit_run`, function(state, this_input) {


      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;


      if ( state == true ) {

        $.each( check_box_array, function( ch_ind, check_box_prop ) {
          var check_box_id = rand_id +'_' + check_box_prop;
          // ugasi sve osim ove kliknute
          if ( check_box_id !== this_input.id ) $('#' + check_box_id).addClass(`off`).removeClass(`on`);
        });

      } 
      else {
        // -------------------------------- AKO JE OD-ČEKIRANA TJ FALSE
        $.each( check_box_array, function( ch_ind, check_box_prop ) {
          var check_box_id = rand_id +'_' + check_box_prop;
          // ugasi sve
          $('#' + check_box_id).addClass(`off`).removeClass(`on`);
        });

      };



      /*

      work_cat: [
        { sifra: "WCAT1", naziv: "Rad" },
        { sifra: "WCAT2", naziv: "Pauza" },

        { sifra: "WCAT3", naziv: "Problem u radu" },
        { sifra: "WCAT4", naziv: "Pokvaren stroj" },
        { sifra: "WCAT5", naziv: "Problem osoblja" },
        { sifra: "WCAT6", naziv: "Gotovo radno vrijeme" },
        { sifra: "WCAT7", naziv: "Dobio drugi zadatak" },
        { sifra: "WCAT8", naziv: "Problem u radnom nalogu" },

        { sifra: "WCAT9", naziv: "Greška radnika" },

        { sifra: "WCAT10", naziv: "RN GOTOV" },
        { sifra: "WCAT11", naziv: "RN STORNO" },

        { sifra: "WCAT1B", naziv: "Priprema" },

      ]

      */


      var work_cat = null;


      if ( state == true ) {

        if ( this_input.id.indexOf( `prodon_drugi_zad` ) > -1 )      work_cat = { sifra: "WCAT7", naziv: "Dobio drugi zadatak" };
        if ( this_input.id.indexOf( `prodon_problem_nalog` ) > -1 )  work_cat = { sifra: "WCAT8", naziv: "Problem u radnom nalogu" };
        if ( this_input.id.indexOf( `prodon_problem_rad` ) > -1 )    work_cat = { sifra: "WCAT3", naziv: "Problem u radu" };
        if ( this_input.id.indexOf( `prodon_problem_stroj` ) > -1 )  work_cat = { sifra: "WCAT4", naziv: "Pokvaren stroj" };
        if ( this_input.id.indexOf( `prodon_problem_worker` ) > -1 ) work_cat = { sifra: "WCAT5", naziv: "Problem osoblja" };
        if ( this_input.id.indexOf( `prodon_kraj_rad_vrem` ) > -1 )  work_cat = { sifra: "WCAT6", naziv: "Gotovo radno vrijeme" };


        data.temp_work_cat = work_cat;

        console.log(data);
        console.log( data.temp_work_cat );

      };


    }); // kraj cit run za check boxove za proizvodnju

  });

  


  $(`#`+ rand_id + `_prodon_select_ulaz`).data('cit_props', {

    desc: `odaberi ulaz za proces u prodon ${rand_id}`,
    local: true,
    show_cols: ['prodon_name'],
    return: {},
    show_on_click: true,
    list: data.post_ulazi_list,

    cit_run: async function(state) {

      var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      /*
      if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
        popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
        return;
      };
      */
      
        // OBAVEZNO UVIJEK !!!
        // resetiraj i primku ako je neka već izabrana !!!
        $('#' + rand_id + `_prodon_select_ulaz_primka`).val(``);
        data.selected_primka = null;

      if ( state == null ) {
        $('#'+current_input_id).val(``);
        data.current_prodon_ulaz = null;
        $(`#`+ rand_id + `_prodon_utrosak_ukupno`).val(``);
        $(`#`+ rand_id + `_prodon_skart_ulaz_ukupno`).val(``);

      } 
      else {

        $('#'+current_input_id).val( state.prodon_name );

        data.current_prodon_ulaz = cit_deep(state);
        
        var find_sirov = await get_cit_module(`/modules/sirov/find_sirov.js`, null);
        var sirovine_ids = [];
        if ( state._id || state.prodon_sirov_id ) sirovine_ids.push( state._id || state.prodon_sirov_id );
        
        
        if ( sirovine_ids.length > 0 ) {

          var DB_sirovina = await ajax_find_query( { _id: { $in: sirovine_ids } }, `/find_sirov`, find_sirov.find_sirov_filter );

          if ( !DB_sirovina || DB_sirovina == 0 ) {
            popup_warn(`Ne mogu pristupiti bazi sirovina za ovaj ulaz !!!!`);
            return;
          };
          
          // pošto sam dao samo jedan specifični _id onda mora biti jedan jedini item u arrayu
          DB_sirovina = DB_sirovina[0];

          var sir_locations = null;
          
          if ( DB_sirovina.locations?.length > 0 ) {
            
            sir_locations = cit_deep(DB_sirovina.locations);
            multisort( sir_locations, [ '!time' ] );
            
          };

        };

        var curr_ulaz = get_post_kalk_val(

          data.current_status.kalk,
          data.current_proces.row_sifra,
          `ulazi`,
          data.current_prodon_ulaz.sifra,

        );


        var curr_pro_ulaz = get_pro_kalk_val(

          data.current_status.kalk,
          data.current_proces.row_sifra,
          `ulazi`,
          data.current_prodon_ulaz.sifra,

        );

        $(`#prodon_ulaz_title_row h4`).text(`POTREBNO: ${curr_pro_ulaz.kalk_sum_sirovina} kom`);


        format_number_input(curr_ulaz.kalk_sum_sirovina, $(`#`+ rand_id + `_prodon_utrosak_ukupno`)[0], null);
        format_number_input(curr_ulaz.kalk_skart, $(`#`+ rand_id + `_prodon_skart_ulaz_ukupno`)[0], null);


        if ( state._id ) state.prodon_sirov_id = state._id;

        if ( state.prodon_sirov_id ) {
          
          $(`#prodon_ulaz_link`).attr(`href`, `#sirov/${state.prodon_sirov_id}/status/null`);
          $(`#prodon_ulaz_link`).removeClass(`cit_disable`);
        }
        else {
          $(`#prodon_ulaz_link`).attr(`href`, `#`);
          $(`#prodon_ulaz_link`).addClass(`cit_disable`);
        };


      };

    },

  }); // kraj propsa za drop listu svih ulaza



  $('#'+rand_id+'_prodon_select_ulaz_primka').data('cit_props', {
    // !!!findprimka!!!
    desc: 'za odabir PRIMKE u modulu za PRODON ' + rand_id,
    local: false,
    url: '/find_docs',
    find_in: [ 'docs.doc_sifra' ],
    col_widths: [ 1, 1 ],
    custom_headers: [
      "PRIMKA",
      "DATUM",
    ],
    format_cols: {
      doc_sifra: "center",
      rok: "date_time",
    },
    show_cols: [ 'doc_sifra', 'rok' ], // rok u ovom slučaju znači kad je došla roba !!!!!!!
    query: function() {

      var req = {};

      var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      
      if ( !data.current_prodon_ulaz ) {
        req = `abort_req`; // ovo mi služi da abortam sve u search_database unutar global_func.js
        return req;
      };
      
      
      /*  
      
      var find_sirov = await get_cit_module(`/modules/sirov/find_sirov.js`, null);
      var sirovine_ids = [];
      if ( state._id || state.prodon_sirov_id ) sirovine_ids.push( state._id || state.prodon_sirov_id );
      var DB_sirovina = await ajax_find_query( { _id: { $in: sirovine_ids } }, `/find_sirov`, find_sirov.find_sirov_filter );
      
      if ( !DB_sirovina || DB_sirovina == 0 ) {
        popup_warn(`Ne mogu pristupiti bazi sirovina za ovaj ulaz !!!!`);
        req = `abort_req`; // ovo mi služi da abortam sve u search_database unutar global_func.js
        return req;
      };
      
      // pošto sam dao samo jedan specifični _id onda mora biti jedan jedini item u arrayu
      DB_sirovina = DB_sirovina[0];
      
      */
      
      var dobavljac_ulaza = data.current_prodon_ulaz?.original_sirov?.dobavljac?._id;
      var selected_sirov_id = data.current_prodon_ulaz._id || data.current_prodon_ulaz.prodon_sirov_id || null;
      
      if ( !dobavljac_ulaza || !selected_sirov_id ) {

        popup_warn(`Nedostaju podaci dobavljača za ovu sirovinu !!!!`);
        req = `abort_req`; // ovo mi služi da abortam sve u search_database unutar global_func.js

      } else {

        req = { 
          _id: data.current_prodon_ulaz.original_sirov.dobavljac._id,
          get_doc_type: `in_record`,
          get_doc_sirov_id: data.current_prodon_ulaz._id || data.current_prodon_ulaz.prodon_sirov_id || null,
        };

      };

      return req;

    },
    show_on_click: true,
    list_width: 700,
    cit_run: function(state) {


      /*

      --------- OVO JE PRIMJER OBJEKTA / STATE-a ---------

      sifra: doc.sifra,
      doc_type: doc.doc_type,
      doc_sifra: doc.doc_sifra,
      time: doc.time,
      doc_link: doc.doc_link,
      doc_sirovs: doc.doc_sirovs,
      rok: doc.rok,


      */



      var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      if ( state == null ) {
        $('#'+current_input_id).val(``);
        data.selected_primka = null;
      } else {
        $('#'+current_input_id).val(state.doc_sifra);
        data.selected_primka = state;
      };
      
    },

  });


  /*
  
  $('#'+rand_id+'prodon_select_ulaz_palet_sifra').data('cit_props', {
    // !!!findprimka!!!
    desc: 'za odabir SKLADIŠNE KARTICE TJ PALET SIFRA u modulu za PRODON ' + rand_id,
    local: false,
    url: '/find_docs',
    find_in: [ 'docs.doc_sifra' ],
    col_widths: [ 1, 1 ],
    custom_headers: [
      "PRIMKA",
      "DATUM",
    ],
    format_cols: {
      doc_sifra: "center",
      rok: "date_time",
    },
    show_cols: [ 'doc_sifra', 'rok' ], // rok u ovom slučaju znači kad je došla roba !!!!!!!
    query: function() {

      var req = {};

      var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      
      if ( !data.current_prodon_ulaz ) {
        req = `abort_req`; // ovo mi služi da abortam sve u search_database unutar global_func.js
        return req;
      };
      
      
      // --------------------------------------------------------------------------- 
      
      // var find_sirov = await get_cit_module(`/modules/sirov/find_sirov.js`, null);
      // var sirovine_ids = [];
      // if ( state._id || state.prodon_sirov_id ) sirovine_ids.push( state._id || state.prodon_sirov_id );
      // var DB_sirovina = await ajax_find_query( { _id: { $in: sirovine_ids } }, `/find_sirov`, find_sirov.find_sirov_filter );
      
      // if ( !DB_sirovina || DB_sirovina == 0 ) {
      //   popup_warn(`Ne mogu pristupiti bazi sirovina za ovaj ulaz !!!!`);
      //   req = `abort_req`; // ovo mi služi da abortam sve u search_database unutar global_func.js
      //   return req;
      // };
      
      // ----------------------------------- pošto sam dao samo jedan specifični _id onda mora biti jedan jedini item u arrayu
      // DB_sirovina = DB_sirovina[0];
      
      // --------------------------------------------------------------------------- 
      
      
      var dobavljac_ulaza = data.current_prodon_ulaz?.original_sirov?.dobavljac?._id;
      var selected_sirov_id = data.current_prodon_ulaz._id || data.current_prodon_ulaz.prodon_sirov_id || null;
      
      if ( !dobavljac_ulaza || !selected_sirov_id ) {

        popup_warn(`Nedostaju podaci dobavljača za ovu sirovinu !!!!`);
        req = `abort_req`; // ovo mi služi da abortam sve u search_database unutar global_func.js

      } else {

        req = { 
          _id: data.current_prodon_ulaz.original_sirov.dobavljac._id,
          get_doc_type: `in_record`,
          get_doc_sirov_id: data.current_prodon_ulaz._id || data.current_prodon_ulaz.prodon_sirov_id || null,
        };

      };

      return req;

    },
    show_on_click: true,
    list_width: 700,
    cit_run: function(state) {


      

      // ------------------ OVO JE PRIMJER OBJEKTA / STATE-a ---------

      // sifra: doc.sifra,
      // doc_type: doc.doc_type,
      // doc_sifra: doc.doc_sifra,
      // time: doc.time,
      // doc_link: doc.doc_link,
      // doc_sirovs: doc.doc_sirovs,
      // rok: doc.rok,


      



      var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      if ( state == null ) {
        $('#'+current_input_id).val(``);
        data.selected_primka = null;
      } else {
        $('#'+current_input_id).val(state.doc_sifra);
        data.selected_primka = state;
      };
    },

  });
  
  
*/
  
  
  $(`#`+ rand_id + `_prodon_select_izlaz`).data('cit_props', {

    desc: `odaberi IZLAZ za proces u prodon ${rand_id}`,
    local: true,
    show_cols: ['prodon_name'],
    return: {},
    show_on_click: true,
    list: data.post_izlazi_list,

    cit_run: function(state) {

      var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      /*
      if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
        popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
        return;
      };
      */


      if ( state == null ) {
        
        $('#'+current_input_id).val(``);
        data.current_prodon_izlaz = null;

        $(`#`+ rand_id + `_prodon_izlaz_ukupno`).val(``);
        $(`#`+ rand_id + `_prodon_skart_ulaz_ukupno`).val(``);

      } else {

        $('#'+current_input_id).val( state.prodon_name );

        data.current_prodon_izlaz = state;

        var curr_post_izlaz = get_post_kalk_val(

          data.current_status.kalk,
          data.current_proces.row_sifra,
          `izlazi`,
          data.current_prodon_izlaz.sifra,

        );
        
        format_number_input(curr_post_izlaz.kalk_sum_sirovina, $(`#`+ rand_id + `_prodon_izlaz_ukupno`)[0], null);
        format_number_input(curr_post_izlaz.kalk_skart, $(`#`+ rand_id + `_prodon_skart_izlaz_ukupno`)[0], null);
        

        var curr_pro_izlaz = get_pro_kalk_val(

          data.current_status.kalk,
          data.current_proces.row_sifra,
          `izlazi`,
          data.current_prodon_izlaz.sifra,

        );
        
        $(`#prodon_izlaz_title_row h4`).text(`POTREBNO: ${curr_pro_izlaz.kalk_sum_sirovina} kom`);


        if ( state._id ) state.prodon_sirov_id = state._id;

        if ( state.prodon_sirov_id ) {
          $(`#prodon_izlaz_link`).attr(`href`, `#sirov/${state.prodon_sirov_id}/status/null`);
          $(`#prodon_izlaz_link`).removeClass(`cit_disable`);

        }
        else {
          $(`#prodon_izlaz_link`).attr(`href`, `#`);
          $(`#prodon_izlaz_link`).addClass(`cit_disable`);
        };

      };

    },

  }); // kraj propsa za drop listu svih ulaza


  $(`#prep_work_btn`).off(`click`);
  $(`#prep_work_btn`).on(`click`, async function() {


  if ( !window.cit_user ) {
    popup_warn(`User nije ulogiran !!!!`);
    return;
  };

  /*
  if ( !window.velprom_smjena ) {
    popup_warn(`Nije odabrana smjena !!!!`);
    return;
  };
  */


  var this_button = this;

  var curr_time = Date.now();

  var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
  var data = this_module.cit_data[this_comp_id];
  var rand_id =  this_module.cit_data[this_comp_id].rand_id;


  if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
    popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
    return;
  };


  if ( !data.current_status.work_times ) data.current_status.work_times = [];

  var found_unfinished_prep = false;  

  $.each( data.current_status.work_times, async function( w_ind, work_obj) {

    if ( 
      work_obj.worker.user_number == window.cit_user.user_number // traži samo trenutno ulogiranog usera
      &&
      work_obj.work_cat.sifra == `WCAT1B` // ovo je kategorija { sifra: "WCAT1B", naziv: "Priprema" },
      &&
      work_obj.production
      &&
      work_obj.work_from_time 
      &&
      !work_obj.work_to_time
    ) {

      if ( found_unfinished_prep == false ) {
        found_unfinished_prep = true;
        popup_warn(`Već je upisan POČETAK PRIPREME od: ` + cit_dt( work_obj.work_from_time).date_time );
      };

    };

  });

  if ( found_unfinished_prep == true ) return;


  var found_unfinished_previous = false;  
  $.each( data.current_status.work_times, async function( w_ind, work_obj) {

    if ( 

      /* NIJE BITNO KOJA KATEGORIJA POSLA*/

      work_obj.worker.user_number == window.cit_user.user_number // traži samo trenutno ulogiranog usera
      &&
      work_obj.production 
      &&
      work_obj.work_from_time 
      &&
      !work_obj.work_to_time
    ) {

      if ( found_unfinished_previous == false ) {

        found_unfinished_previous = true;

        data.current_status.work_times[w_ind].work_to_time = curr_time;

        if ( data.current_status.work_times[w_ind].work_from_time ) {
          data.current_status.work_times[w_ind].duration = ( curr_time - data.current_status.work_times[w_ind].work_from_time)/(1000*60);
        };

        data.statuses = upsert_item( data.statuses, data.current_status, `sifra` );

        // var saved_status = await this_module.save_work(null, this_button, work_obj);


      };

    };

  }); // kraj loopa po svim work times



  var work_desc = $('#'+rand_id+`_prodon_work_desc`).val();
  work_desc = esc_html(work_desc);
  

  var arg_work = {
    sifra: `work` + cit_rand(),

    worker: {
      _id: window.cit_user._id, 
      user_number: window.cit_user.user_number,
      email: window.cit_user.email,
      full_name: window.cit_user.full_name,
      name: window.cit_user.name,
    },

    time: curr_time,
    work_from_time: curr_time,
    work_to_time: null,
    duration: null,
    desc: work_desc || "",

    work_cat: { sifra: "WCAT1B", naziv: "Priprema" },
    production: true,

  };

  /*smjena: window.velprom_smjena,*/

  var saved_status = await this_module.save_work(null, this_button, arg_work);


  });


  $(`#start_work_btn`).off(`click`);
  $(`#start_work_btn`).on(`click`, async function() {


    if ( !window.cit_user ) {
      popup_warn(`User nije ulogiran !!!!`);
      return;
    };

    /*
    if ( !window.velprom_smjena ) {
      popup_warn(`Nije odabrana smjena !!!!`);
      return;
    };
    */


    var this_button = this;

    var curr_time = Date.now();

    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
      popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
      return;
    };


    if ( !data.current_status.work_times ) data.current_status.work_times = [];

    var found_unfinished_work = false;  

    // PROVJERI JEL VEĆ IMA AKTIVNI RAD
    $.each( data.current_status.work_times, async function( w_ind, work_obj) {

      if ( 
        work_obj.worker.user_number == window.cit_user.user_number // traži samo trenutno ulogiranog usera
        &&
        work_obj.work_cat.sifra == `WCAT1` // ovo je kategorija RAD
        &&
        work_obj.production
        &&
        work_obj.work_from_time // ima početak
        &&
        !work_obj.work_to_time // ali nema kraj
      ) {

        if ( found_unfinished_work == false ) {
          found_unfinished_work = true;
          popup_warn(`Već je upisan START RADA od: ` + cit_dt( work_obj.work_from_time).date_time );
        };

      };

    }); // kraj loopa po svim work times

    if ( found_unfinished_work == true ) return;


    // PREKINI PRIJAŠNJI POSAO
    var found_unfinished_previous = false;  
    $.each( data.current_status.work_times, async function( w_ind, work_obj) {

      if ( 

        /* NIJE BITNO KOJA KATEGORIJA POSLA ------> NE SAMO RAD ( WCAT1 ) */

        work_obj.worker.user_number == window.cit_user.user_number // traži samo trenutno ulogiranog usera
        &&
        work_obj.production 
        &&
        work_obj.work_from_time 
        &&
        !work_obj.work_to_time
      ) {

        if ( found_unfinished_previous == false ) {

          found_unfinished_previous = true;

          data.current_status.work_times[w_ind].work_to_time = curr_time;
          data.current_status.work_times[w_ind].duration = ( curr_time - data.current_status.work_times[w_ind].work_from_time)/(1000*60);
          data.statuses = upsert_item( data.statuses, data.current_status, `sifra` );

          // var saved_status = await this_module.save_work(null, this_button, work_obj);


        };

      };

    });



    var work_desc = $('#'+rand_id+`_prodon_work_desc`).val();

    var arg_work = {
      sifra: `work` + cit_rand(),

      worker: {
        _id: window.cit_user._id, 
        user_number: window.cit_user.user_number,
        email: window.cit_user.email,
        full_name: window.cit_user.full_name,
        name: window.cit_user.name,
      },

      time: curr_time,
      work_from_time: curr_time,
      work_to_time: null,
      duration: null,
      desc: work_desc || "",

      work_cat: { sifra: "WCAT1", naziv: "Rad" },
      production: true,

    };

    /*smjena: window.velprom_smjena,*/

    var saved_status = await this_module.save_work(null, this_button, arg_work);


  });


  $(`#pauza_work_btn`).off(`click`);
  $(`#pauza_work_btn`).on(`click`, async function() {


    if ( !window.cit_user ) {
      popup_warn(`User nije ulogiran !!!!`);
      return;
    };


    var this_button = this;
    var curr_time = Date.now();


    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
      popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
      return;
    };


    if ( !data.current_status.work_times ) data.current_status.work_times = [];


    var found_unfinished_pauza = false;  

    $.each( data.current_status.work_times, function( w_ind, work_obj) {

      if ( 
        work_obj.worker.user_number == window.cit_user.user_number // traži samo trenutno ulogiranog usera
        &&
        work_obj.work_cat.sifra == `WCAT2` // ovo je categorija pauza
        &&
        work_obj.production 
        &&
        work_obj.work_from_time 
        &&
        !work_obj.work_to_time
      ) {

        if ( found_unfinished_pauza == false ) {
          found_unfinished_pauza = true;
          popup_warn(`Već je upisana PAUZA od: ` + cit_dt( work_obj.work_from_time).date_time );
        };

      };

    });

    if ( found_unfinished_pauza == true ) return;


    var found_unfinished_work = false;  
    $.each( data.current_status.work_times, async function( w_ind, work_obj) {

      if ( 

        /* MOŽE BITI BILO KOJA KATEGORIJA POSLA */

        work_obj.worker.user_number == window.cit_user.user_number // traži samo trenutno ulogiranog usera
        &&
        work_obj.production 
        &&
        work_obj.work_from_time 
        &&
        !work_obj.work_to_time

      ) {

        if ( found_unfinished_work == false ) {

          found_unfinished_work = true;

          data.current_status.work_times[w_ind].work_to_time = curr_time;

          if ( data.current_status.work_times[w_ind].work_from_time ) {
            data.current_status.work_times[w_ind].duration = ( curr_time - data.current_status.work_times[w_ind].work_from_time)/(1000*60);
          };

          data.statuses = upsert_item( data.statuses, data.current_status, `sifra` );

          // var saved_status = await this_module.save_work(null, this_button, work_obj);

        };

      };

    }); // kraj loopa po work times


    var work_desc = $('#'+rand_id+`_prodon_work_desc`).val();

    var arg_work = {

      sifra: `work` + cit_rand(),

      worker: {
        _id: window.cit_user._id, 
        user_number: window.cit_user.user_number,
        email: window.cit_user.email,
        full_name: window.cit_user.full_name,
        name: window.cit_user.name,
      },

      time: curr_time,
      work_from_time: curr_time,
      work_to_time: null,
      duration: null,
      desc: work_desc || "",

      work_cat:  { sifra: "WCAT2", naziv: "Pauza" },
      production: true,

    };



    var saved_status = await this_module.save_work(null, this_button, arg_work);

  }); // kraj pauza button


  $(`#stop_work_btn`).off(`click`);
  $(`#stop_work_btn`).on(`click`, async function() {


    if ( !window.cit_user ) {
      popup_warn(`User nije ulogiran !!!!`);
      return;
    };


    var this_button = this;
    var curr_time = Date.now();


    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
      popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
      return;
    };

    if ( !data.temp_work_cat ) {
      popup_warn(`Potrebno kliknuti na jedan od razloga stopiranja procesa!!!`);
      return;
    };


    var work_cat = cit_deep( data.temp_work_cat );


    if ( !data.current_status.work_times ) data.current_status.work_times = [];

    var found_unfinished_work = false;  
    $.each( data.current_status.work_times, async function( w_ind, work_obj) {

      if ( 

        /* SADA NIJE BITNO KOJA JE KATEGORIJA SAMO DA NIJE DOVRŠENO  */

        work_obj.worker.user_number == window.cit_user.user_number // traži samo trenutno ulogiranog usera
        &&
        work_obj.production 
        &&
        work_obj.work_from_time 
        &&
        !work_obj.work_to_time

      ) {

        if ( found_unfinished_work == false ) { 

          found_unfinished_work = true;

          data.current_status.work_times[w_ind].work_to_time = curr_time;

          if ( data.current_status.work_times[w_ind].work_from_time ) {
            data.current_status.work_times[w_ind].duration = ( curr_time - data.current_status.work_times[w_ind].work_from_time)/(1000*60);
          };

          data.statuses = upsert_item( data.statuses, data.current_status, `sifra` );

          // var saved_status = await this_module.save_work(null, this_button, work_obj);

        };

      };

    }); // kraj loopa po work times


    var work_desc = $('#'+rand_id+`_prodon_work_desc`).val();


    var end_time = null;
    if ( work_cat?.sifra == `WCAT6`) end_time = curr_time; // kraj radnog vremena
    if ( work_cat?.sifra == `WCAT7`) end_time = curr_time; // dobio drugi zadatak

    var arg_work = {

      sifra: `work` + cit_rand(),

      worker: {
        _id: window.cit_user._id, 
        user_number: window.cit_user.user_number,
        email: window.cit_user.email,
        full_name: window.cit_user.full_name,
        name: window.cit_user.name,
      },

      time: curr_time,
      work_from_time: curr_time,
      work_to_time: end_time, 
      duration: null,
      desc: work_desc || "",

      work_cat: work_cat,
      production: true,

    };


    var saved_status = await this_module.save_work( null, this_button, arg_work);



  }); // kraj STOP button


  $(`#done_work_btn`).off(`click`);
  $(`#done_work_btn`).on(`click`, async function() {
    
    
    var this_button = this;
    var curr_time = Date.now();

    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
      popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
      return;
    };
    

    if ( !window.cit_user ) {
      popup_warn(`User nije ulogiran !!!!`);
      return;
    };
    

    var curr_pro_ulaz = get_pro_kalk_val( data.current_status.kalk, data.current_proces.row_sifra, `ulazi`, data.current_prodon_ulaz.sifra );
    var curr_pro_izlaz = get_pro_kalk_val( data.current_status.kalk, data.current_proces.row_sifra, `izlazi`, data.current_prodon_izlaz.sifra );

    var curr_post_ulaz = get_post_kalk_val( data.current_status.kalk, data.current_proces.row_sifra, `ulazi`, data.current_prodon_ulaz.sifra  );
    var curr_post_izlaz = get_post_kalk_val( data.current_status.kalk, data.current_proces.row_sifra, `izlazi`, data.current_prodon_izlaz.sifra );

    
    var final_izlazi_poruka = ``;
    var izlaz_deficit = ``;

    $.each( data.current_status.kalk.post_kalk[0].procesi, function(p_ind, post_proc) {
      
      if ( izlaz_deficit == `` && proc.row_sifra == data.current_proces.row_sifra ) {

        $.each( post_proc.izlazi, function(izlazi_ind, post_izlaz) {
          
          final_izlazi_poruka += izlaz.prodon_name + ` GOTOVO: ` + post_izlaz_kom + ` / ŠKART: ` + (post_izlaz.kalk_skart || 0) + `<br>`;
            
        }); // loop po izlazima current procesa

      }; // jel našao current proces row sifra
      
    }); // loop po post kalk

    
    var popup_text = `PROVJERI PODATKE !!!<br><br>` + final_izlazi_poruka;
    
    async function poruka_yes() {
      
      var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;


      var work_cat = { sifra: "WCAT10", naziv: "RN GOTOV" };

      if ( !data.current_status.work_times ) data.current_status.work_times = [];


      // ovo nije bitno jer proces gotov znači da svi prestaju s radom
      // var found_unfinished_work = false;  

      $.each( data.current_status.work_times, async function( w_ind, work_obj) {

        if ( 

          /* SADA NIJE BITNO KOJA JE KATEGORIJA SAMO DA NIJE DOVRŠENO  */
          // work_obj.worker.user_number == window.cit_user.user_number // traži samo trenutno ulogiranog usera
          // &&
          // NIJE BITNO KOJI JE USER  ---- > SVAKOM USER-u NAPIŠI DA JE GOTOV S RADOM JE SADA PROCES GOTOV !!!

          work_obj.production 
          &&
          work_obj.work_from_time 
          &&
          !work_obj.work_to_time

        ) {

          data.current_status.work_times[w_ind].work_to_time = curr_time;

          if ( work_obj.work_from_time ) {
            data.current_status.work_times[w_ind].duration = ( curr_time - work_obj.work_from_time ) / (1000*60);
          };

          // var saved_status = await this_module.save_work(null, this_button, work_obj);

        };

      }); // kraj loopa po work times
      

      var work_desc = $('#'+rand_id+`_prodon_work_desc`).val();

      // za sve ove kategorije vrijeme početno i završno je zapravo isto vrijeme
      var end_time = null;
      if ( work_cat?.sifra == `WCAT6`) end_time = curr_time; // kraj radnog vremena
      if ( work_cat?.sifra == `WCAT7`) end_time = curr_time; // dobio drugi zadatak
      if ( work_cat?.sifra == `WCAT10`) end_time = curr_time; // gotov RN proces !!!!

      var arg_work = {

        sifra: `work` + cit_rand(),

        worker: {
          _id: window.cit_user._id, 
          user_number: window.cit_user.user_number,
          email: window.cit_user.email,
          full_name: window.cit_user.full_name,
          name: window.cit_user.name,
        },

        time: curr_time,
        work_from_time: curr_time,
        work_to_time: end_time, 
        duration: null,
        desc: work_desc || "",

        work_cat: work_cat,
        production: true,

      };
      
      

      data.current_status.kalk = run_prodon_update_kalk( 

        data.current_status.kalk,
        data.current_proces.row_sifra,
        `izlazi`,
        data.current_prodon_izlaz.sifra,
        curr_post_izlaz
      );
      
      

      data.statuses = upsert_item( data.statuses, data.current_status, `sifra` );
      var saved_status = await this_module.save_work( null, this_button, arg_work);
      
      var kalk_mod = await get_cit_module(`/modules/kalk/kalk_module.js`, `load_css`);
      var rn_updated_statuses = await kalk_mod.ajax_proces_rn_update_statuses( data.current_status.kalk._id, data.proces_id, `rn_finished` );
      
      
    }; // kraj poruka_yes
    
    function poruka_no() {
      
      show_popup_modal(false, popup_text, null );

      // resetiraj za sljedeći STOP !!!!
      data.temp_work_cat = null;
      // resetiraj sve check boxove u HTML-u
      $(`#prodon_check_boxes .cit_check_box`).each( function() {
        $(this).addClass(`off`).removeClass(`on`);
      });
      
    };
    
    
    var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
    var show_popup_modal = pop_mod.show_popup_modal;
    show_popup_modal(`warning`, popup_text, null, 'yes/no', poruka_yes, poruka_no, null);

    
    
    

  }); // kraj GOTOV PROCES button



  // -------------------------- ULAZ UTROŠAK I ULAZ ŠKART --------------------------
  // -------------------------- ULAZ UTROŠAK I ULAZ ŠKART --------------------------
  // -------------------------- ULAZ UTROŠAK I ULAZ ŠKART --------------------------


  $(`#${rand_id}_prodon_utrosak_ukupno`).off(`blur`);
  $(`#${rand_id}_prodon_utrosak_ukupno`).on(`blur`, async function(e) {

    e.stopPropagation(); 
    e.preventDefault();


    var this_input = this;
    var curr_time = Date.now();

    var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    
    if ( !data.current_status?.work_times ) {
      popup_warn(`Prije upisivanja ulaza i izlaza morate kliknuti na START !!!`);
      return;
    };
    
    
    if ( !data.current_prodon_ulaz ) {
      popup_warn(`Potrebno je izabrati jedan od ulaza za ovaj proces !!!!`);
      $(this_input).val(``);
      return;
    };
    
    
    if ( !data.current_prodon_ulaz ) {
      popup_warn(`Potrebno je izabrati jedan od ulaza za ovaj proces !!!!`);
      $(this_input).val(``);
      return;
    };


    if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
      popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
      return;
    };

    var curr_ulaz = get_post_kalk_val(

      data.current_status.kalk,
      data.current_proces.row_sifra,
      `ulazi`,
      data.current_prodon_ulaz.sifra,

    );

    // ---------------------- UPIS NEW VALUE U HTML I U DATA ----------------------
    var input_value = cit_number( $(this).val() );
    
    if ( $(this).val() !== `` && input_value === null ) {
      popup_warn(`Nije numerička vrijednost !!!`)
      return;
    };
    
    $(`#${rand_id}_prodon_utrosak_ukupno`).val( cit_format(input_value, 2) );
    // ---------------------- UPIS NEW VALUE U HTML I U DATA ----------------------



    var popup_text = 
    `Jeste li sigurni da želite promijeniti utrošak ulaza sa:
    <br>
    <br>
    <span style="font-size: 40px;">${ cit_format(curr_ulaz.kalk_sum_sirovina || 0, 2) }</span>
    <br>
    na
    <br>
    <span style="font-size: 40px;">${ cit_format(input_value, 2) }</span>

    `;

    async function utrosak_yes() {

      curr_ulaz.kalk_sum_sirovina = input_value;

      data.current_status.kalk = run_prodon_update_kalk( 

        data.current_status.kalk,
        data.current_proces.row_sifra,
        `ulazi`,
        data.current_prodon_ulaz.sifra,
        curr_ulaz
      );


      var saved_status = await write_work_efect( this_module, data, curr_ulaz.kalk_sum_sirovina, curr_ulaz.kalk_skart, null, null );
      
      
      var saved_record = await update_utrosak_in_roba( this_module, data, curr_ulaz, input_value );


      $(`#save_ulaz_utrosak`).trigger(`click`);


    };

    function utrosak_no() {

      show_popup_modal(false, popup_text, null );
      format_number_input( curr_ulaz.kalk_sum_sirovina , this_input, null, null);

    };


    if ( curr_ulaz.kalk_sum_sirovina !==  cit_number(  $(this_input).val() ) ) {

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', utrosak_yes, utrosak_no, null);

    };


  });


  $(`#${rand_id}_prodon_utrosak_dodaj`).off(`blur`);
  $(`#${rand_id}_prodon_utrosak_dodaj`).on(`blur`, async function(e) {


    e.stopPropagation(); 
    e.preventDefault();

    var this_input = this;
    var curr_time = Date.now();

    var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    if ( !data.current_status?.work_times ) {
      popup_warn(`Prije upisivanja ulaza i izlaza morate kliknuti na START !!!`);
      return;
    };
    

    if ( !data.current_prodon_ulaz ) {
      popup_warn(`Potrebno je izabrati jedan od ulaza za ovaj proces !!!!`);
      $(this_input).val(``);
      return;
    };

    if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
      popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
      return;
    };

    var curr_ulaz = get_post_kalk_val(

      data.current_status.kalk,
      data.current_proces.row_sifra,
      `ulazi`,
      data.current_prodon_ulaz.sifra,

    );

    // ---------------------- UPIS NEW VALUE U HTML I U DATA ----------------------
    var input_value = cit_number( $(this).val() );
    if ( $(this).val() !== `` && input_value === null ) {
      popup_warn(`Nije numerička vrijednost !!!`)
      return;
    };

    $(`#${rand_id}_prodon_utrosak_dodaj`).val( cit_format(input_value, 2) );
    // ---------------------- UPIS NEW VALUE U HTML I U DATA ----------------------




    var popup_text = 
`Jeste li sigurni da želite dodati UTROŠAK:
<br>
<br>
<span style="font-size: 40px;">${ cit_format(input_value, 2) }</span>

`;


    async function dodaj_yes() {


      // PAZI OVDJE JE DODAVANJE +=
      // PAZI OVDJE JE DODAVANJE +=
      // PAZI OVDJE JE DODAVANJE +=
      curr_ulaz.kalk_sum_sirovina = (curr_ulaz.kalk_sum_sirovina || 0 ) + input_value;

      data.current_status.kalk = run_prodon_update_kalk( 

        data.current_status.kalk,
        data.current_proces.row_sifra,
        `ulazi`,
        data.current_prodon_ulaz.sifra,
        curr_ulaz
      );

      var saved_status = await write_work_efect( this_module, data, curr_ulaz.kalk_sum_sirovina, curr_ulaz.kalk_skart, null, null );

      $(this_input).val(``);

      format_number_input( curr_ulaz.kalk_sum_sirovina ,  $(`#${rand_id}_prodon_utrosak_ukupno`)[0], null, null );


      var comfirm_msg = `<span style="font-size: 40px;"> DODALI STE ${text_value} !!!</span>`;

      show_popup_modal(true, comfirm_msg, null );

      setTimeout( function() { show_popup_modal(false, comfirm_msg, null );  }, 2000 );

      $(`#save_ulaz_utrosak`).trigger(`click`);

    };

    function dodaj_no() {

      show_popup_modal(false, popup_text, null );
      $(this_input).val(``);

    };


    if ( $(this_input).val() !== "" ) {

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', dodaj_yes, dodaj_no, null);

    };


  });


  $(`#${rand_id}_prodon_skart_ulaz_ukupno`).off(`blur`);
  $(`#${rand_id}_prodon_skart_ulaz_ukupno`).on(`blur`, async function(e) {


    var this_input = this;
    var curr_time = Date.now();

    var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    if ( !data.current_status?.work_times ) {
      popup_warn(`Prije upisivanja ulaza i izlaza morate kliknuti na START !!!`);
      return;
    };

    if ( !data.current_prodon_ulaz ) {
      popup_warn(`Potrebno je izabrati jedan od ulaza za ovaj proces !!!!`);
      $(this_input).val(``);
      return;
    };


    if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
      popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
      return;
    };

    var curr_ulaz = get_post_kalk_val(

      data.current_status.kalk,
      data.current_proces.row_sifra,
      `ulazi`,
      data.current_prodon_ulaz.sifra,

    );

    // ---------------------- UPIS NEW VALUE U HTML I U DATA ----------------------
    var input_value = cit_number( $(this).val() );
    if ( $(this).val() !== `` && input_value === null ) {
      popup_warn(`Nije numerička vrijednost !!!`)
      return;
    };
    
    $(`#${rand_id}_prodon_skart_ulaz_ukupno`).val( cit_format(input_value, 2) );
    // ---------------------- UPIS NEW VALUE U HTML I U DATA ----------------------

    var popup_text = 
  `Jeste li sigurni da želite promijeniti ŠKART ulaza sa:
  <br>
  <br>
  <span style="font-size: 40px;">${ cit_format( curr_ulaz.kalk_skart || 0, 2) }</span>
  <br>
  na
  <br>
  <span style="font-size: 40px;">${ cit_format(input_value, 2) }</span>

  `;

    async function ulaz_skart_yes() {

      curr_ulaz.kalk_skart = input_value;

      data.current_status.kalk = run_prodon_update_kalk( 

        data.current_status.kalk,
        data.current_proces.row_sifra,
        `ulazi`,
        data.current_prodon_ulaz.sifra,
        curr_ulaz
      );

      var saved_status = await write_work_efect( this_module, data, curr_ulaz.kalk_sum_sirovina, curr_ulaz.kalk_skart, null, null );


      $(`#save_ulaz_skart`).trigger(`click`);


    };

    function ulaz_skart_no() {

      show_popup_modal(false, popup_text, null );
      format_number_input( curr_ulaz.kalk_skart , this_input, null, null);

    };


    if ( curr_ulaz.kalk_skart !== cit_number( $(this_input).val() ) ) {

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', ulaz_skart_yes, ulaz_skart_no, null);

    };

  });


  $(`#${rand_id}_prodon_skart_ulaz_dodaj`).off(`blur`);
  $(`#${rand_id}_prodon_skart_ulaz_dodaj`).on(`blur`, async function(e) {


    var this_input = this;
    var curr_time = Date.now();

    var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    if ( !data.current_status?.work_times ) {
      popup_warn(`Prije upisivanja ulaza i izlaza morate kliknuti na START !!!`);
      return;
    };

    if ( !data.current_prodon_ulaz ) {
      popup_warn(`Potrebno je izabrati jedan od ulaza za ovaj proces !!!!`);
      $(this_input).val(``);
      return;
    };

    if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
      popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
      return;
    };

    var curr_ulaz = get_post_kalk_val(

      data.current_status.kalk,
      data.current_proces.row_sifra,
      `ulazi`,
      data.current_prodon_ulaz.sifra,

    );

    // ---------------------- UPIS NEW VALUE U HTML I U DATA ----------------------
    var input_value = cit_number( $(this).val() );
    if ( $(this).val() !== `` && input_value === null ) {
      popup_warn(`Nije numerička vrijednost !!!`)
      return;
    };
    
    $(`#${rand_id}_prodon_skart_ulaz_dodaj`).val( cit_format(input_value, 2) );
    // ---------------------- UPIS NEW VALUE U HTML I U DATA ----------------------

    var popup_text = 
`Jeste li sigurni da želite dodati ŠKART ULAZA:
<br>
<br>
<span style="font-size: 40px;">${ cit_format( input_value, 2) }</span>

`;


    async function dodaj_yes() {


      // PAZI OVDJE JE DODAVANJE +=
      // PAZI OVDJE JE DODAVANJE +=
      // PAZI OVDJE JE DODAVANJE +=
      curr_ulaz.kalk_skart = (curr_ulaz.kalk_skart || 0 ) + input_value;

      data.current_status.kalk = run_prodon_update_kalk( 

        data.current_status.kalk,
        data.current_proces.row_sifra,
        `ulazi`,
        data.current_prodon_ulaz.sifra,
        curr_ulaz
      );

      var saved_status = await write_work_efect( this_module, data, curr_ulaz.kalk_sum_sirovina, curr_ulaz.kalk_skart, null, null );

      $(this_input).val(``);

      format_number_input( curr_ulaz.kalk_skart ,  $(`#${rand_id}_prodon_skart_ulaz_ukupno`)[0], null, null );


      var comfirm_msg = `<span style="font-size: 40px;"> DODALI STE ${text_value} !!!</span>`;

      show_popup_modal(true, comfirm_msg, null );

      setTimeout( function() { show_popup_modal(false, comfirm_msg, null );  }, 2000 );

      $(`#save_ulaz_skart`).trigger(`click`);



    };

    function dodaj_no() {

      show_popup_modal(false, popup_text, null );
      $(this_input).val(``);

    };


    if ( $(this_input).val() !== ``) {

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', dodaj_yes, dodaj_no, null);
    };


  });

  
  $(`#${rand_id}_prodon_ulaz_palet_sifra`).off(`blur`);
  $(`#${rand_id}_prodon_ulaz_palet_sifra`).on(`blur`, async function(e) {


    var this_input = this;
    var curr_time = Date.now();

    var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    if ( !data.current_status?.work_times ) {
      popup_warn(`Prije upisivanja ulaza i izlaza morate kliknuti na START !!!`);
      return;
    };

    if ( !data.current_prodon_ulaz ) {
      popup_warn(`Potrebno je izabrati jedan od ulaza za ovaj proces !!!!`);
      $(this_input).val(``);
      return;
    };

    if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
      popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
      return;
    };
    

    
    var curr_ulaz = get_post_kalk_val(

      data.current_status.kalk,
      data.current_proces.row_sifra,
      `ulazi`,
      data.current_prodon_ulaz.sifra,

    );



  });
  
  
  
  
  $(`#save_ulaz_utrosak`).off(`click`);
  $(`#save_ulaz_utrosak`).on(`click`, async function(e) {


    var this_button = this;
    var curr_time = Date.now();

    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    if ( !data.current_prodon_ulaz ) {
      popup_warn(`Potrebno je izabrati jedan od ulaza za ovaj proces !!!!`);
      return;
    };

    if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
      popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
      return;
    };

    var curr_ulaz = get_post_kalk_val(

      data.current_status.kalk,
      data.current_proces.row_sifra,
      `ulazi`,
      data.current_prodon_ulaz.sifra,

    );


    var result = await update_post_kalk( 

      data.current_status.kalk._id, // arg_id ---> id od kalkulacije
      data.current_proces.row_sifra, // sifra procesa
      `ulazi`, // property na kojem radim update ----> zapravo ovo je array
      data.current_prodon_ulaz.sifra, //  sifra od itema u array-u,
      curr_ulaz

    );


    if ( result.success == true ) cit_toast(`PODATAK SPREMLJEN U BAZU!`);
    console.log(result.kalk);

    // ako ima više ulaza onda obriši sve ulaze tako da ga natjeram da specifično izabere ulaz koji treba !!!
    setTimeout( function() {
      if ( data.current_proces.ulazi.length > 1 ) {
        data.current_prodon_ulaz = null;
        $(`#`+ rand_id + `_prodon_select_ulaz`).val(``);
      };
    }, 300);


  }); // kraj save ulaz utrošak

  
  $(`#save_ulaz_skart`).off(`click`);
  $(`#save_ulaz_skart`).on(`click`, async function(e) {


    var this_button = this;
    var curr_time = Date.now();

    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    if ( !data.current_prodon_ulaz ) {
      popup_warn(`Potrebno je izabrati jedan od ulaza za ovaj proces !!!!`);
      return;
    };

    if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
      popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
      return;
    };

    var curr_ulaz = get_post_kalk_val(

      data.current_status.kalk,
      data.current_proces.row_sifra,
      `ulazi`,
      data.current_prodon_ulaz.sifra,

    );


    var result = await update_post_kalk( 

      data.current_status.kalk._id, // arg_id ---> id od kalkulacije
      data.current_proces.row_sifra, // sifra procesa
      `ulazi`, // property na kojem radim update ----> zapravo ovo je array
      data.current_prodon_ulaz.sifra, //  sifra od itema u array-u,
      curr_ulaz

    );

    if ( result.success == true ) cit_toast(`PODATAK SPREMLJEN U BAZU!`);
    console.log(result.kalk);


    setTimeout( function() {
      if ( data.current_proces.ulazi.length > 1 ) {
        data.current_prodon_ulaz = null;
        $(`#`+ rand_id + `_prodon_select_ulaz`).val(``);
      };
    }, 300);





  }); // kraj save ulaz utrošak


  // -------------------------- IZLAZ UTROŠAK I IZLAZ ŠKART --------------------------
  // -------------------------- IZLAZ UTROŠAK I IZLAZ ŠKART --------------------------
  // -------------------------- IZLAZ UTROŠAK I IZLAZ ŠKART --------------------------

  $(`#${rand_id}_prodon_izlaz_ukupno`).off(`blur`);
  $(`#${rand_id}_prodon_izlaz_ukupno`).on(`blur`, async function(e) {

    e.stopPropagation(); 
    e.preventDefault();

    var this_input = this;
    var curr_time = Date.now();

    var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    
    if ( !data.current_status?.work_times ) {
      popup_warn(`Prije upisivanja ulaza i izlaza morate kliknuti na START !!!`);
      return;
    };

    if ( !data.current_prodon_izlaz ) {
      popup_warn(`Potrebno je izabrati jedan od izlaza za ovaj proces !!!!`);
      $(this_input).val(``);
      return;
    };

    
    if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
      popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
      return;
    };


    var curr_izlaz = get_post_kalk_val(

      data.current_status.kalk,
      data.current_proces.row_sifra,
      `izlazi`,
      data.current_prodon_izlaz.sifra,

    );

    
    
    // var skl_sifra_OK = await check_skl_card(curr_ulaz);
    
    
    
    // ---------------------- UPIS NEW VALUE U HTML I U DATA ----------------------
    var input_value = cit_number( $(this).val() );
    
    if ( $(this).val() !== `` && input_value === null ) {
      popup_warn(`Nije numerička vrijednost !!!`)
      return;
    };
    
    $(`#${rand_id}_prodon_izlaz_ukupno`).val( cit_format(input_value, 2) );
    // ---------------------- UPIS NEW VALUE U HTML I U DATA ----------------------


    var popup_text = 
  `Jeste li sigurni da želite promijeniti količinu izlaza sa:
  <br>
  <br>
  <span style="font-size: 40px;">${ cit_format( curr_izlaz.kalk_sum_sirovina || 0, 2) }</span>
  <br>
  na
  <br>
  <span style="font-size: 40px;">${ cit_format(input_value, 2) }</span>

  `;

    async function izlaz_yes() {

      
      curr_izlaz.kalk_sum_sirovina = input_value;

      data.current_status.kalk = run_prodon_update_kalk( 

        data.current_status.kalk,
        data.current_proces.row_sifra,
        `izlazi`,
        data.current_prodon_izlaz.sifra,
        curr_izlaz
      );

      var saved_status = await  write_work_efect( this_module, data, null, null, curr_izlaz.kalk_sum_sirovina, curr_izlaz.kalk_skart );
      
      // this_module.make_post_sums(data);
      // this_module.prodon_percent_setup(data);

      $(`#prodon_save_izlaz`).trigger(`click`);


    };

    function izlaz_no() {

      show_popup_modal(false, popup_text, null );
      format_number_input( curr_izlaz.kalk_sum_sirovina , this_input, null, null);

    };



    if ( curr_izlaz.kalk_sum_sirovina !== cit_number( $(this_input).val() ) ) {

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', izlaz_yes, izlaz_no, null);
    };

  });


  $(`#${rand_id}_prodon_izlaz_dodaj`).off(`blur`);
  $(`#${rand_id}_prodon_izlaz_dodaj`).on(`blur`, async function(e) {

    e.stopPropagation(); 
    e.preventDefault();

    var this_input = this;
    var curr_time = Date.now();

    var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    if ( !data.current_status?.work_times ) {
      popup_warn(`Prije upisivanja ulaza i izlaza morate kliknuti na START !!!`);
      return;
    };
    
    if ( !data.current_prodon_izlaz ) {
      popup_warn(`Potrebno je izabrati jedan od izlaza za ovaj proces !!!!`);
      $(this_input).val(``);
      return;
    };


    if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
      popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
      return;
    };


    var curr_izlaz = get_post_kalk_val(

      data.current_status.kalk,
      data.current_proces.row_sifra,
      `izlazi`,
      data.current_prodon_izlaz.sifra,

    );

    // ---------------------- UPIS NEW VALUE U HTML I U DATA ----------------------
    var input_value = cit_number( $(this).val() );
    
    if ( $(this).val() !== `` && input_value === null ) {
      popup_warn(`Nije numerička vrijednost !!!`)
      return;
    };
    
    $(`#${rand_id}_prodon_izlaz_dodaj`).val( cit_format(input_value, 2) );
    // ---------------------- UPIS NEW VALUE U HTML I U DATA ----------------------


    var popup_text = 
  `Jeste li sigurni da želite dodati PROIZVEDENI IZLAZ:
  <br>
  <br>
  <span style="font-size: 40px;">${ cit_format(input_value, 2) }</span>

  `;


    async function dodaj_yes() {


      // PAZI OVDJE JE DODAVANJE +=
      // PAZI OVDJE JE DODAVANJE +=
      // PAZI OVDJE JE DODAVANJE +=
      curr_izlaz.kalk_sum_sirovina = (curr_izlaz.kalk_sum_sirovina || 0 ) + input_value;

      data.current_status.kalk = run_prodon_update_kalk( 

        data.current_status.kalk,
        data.current_proces.row_sifra,
        `izlazi`,
        data.current_prodon_izlaz.sifra,
        curr_izlaz
      );


      var saved_status = await write_work_efect( this_module, data, null, null, curr_izlaz.kalk_sum_sirovina, curr_izlaz.kalk_skart );


      $(this_input).val(``);

      format_number_input( curr_izlaz.kalk_sum_sirovina ,  $(`#${rand_id}_prodon_izlaz_ukupno`)[0], null, null );


      var comfirm_msg = `<span style="font-size: 40px;"> DODALI STE ${text_value} !!!</span>`;
      show_popup_modal(true, comfirm_msg, null );
      setTimeout( function() { show_popup_modal(false, comfirm_msg, null );  }, 2000 );

      
      // this_module.make_post_sums(data);
      // this_module.prodon_percent_setup(data); 

      $(`#prodon_save_izlaz`).trigger(`click`);


    };

    function dodaj_no() {

      show_popup_modal(false, popup_text, null );
      $(this_input).val(``);

    };


    if ( $(this_input).val() !== `` ) {

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', dodaj_yes, dodaj_no, null);
    };


  });


  $(`#${rand_id}_prodon_skart_izlaz_ukupno`).off(`blur`);
  $(`#${rand_id}_prodon_skart_izlaz_ukupno`).on(`blur`, async function(e) {


    e.stopPropagation(); 
    e.preventDefault();

    var this_input = this;
    var curr_time = Date.now();

    var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    if ( !data.current_status?.work_times ) {
      popup_warn(`Prije upisivanja ulaza i izlaza morate kliknuti na START !!!`);
      return;
    };

    if ( !data.current_prodon_izlaz ) {
      popup_warn(`Potrebno je izabrati jedan od izlaza za ovaj proces !!!!`);
      $(this_input).val(``);
      return;
    };

    var curr_izlaz = get_post_kalk_val(

      data.current_status.kalk,
      data.current_proces.row_sifra,
      `izlazi`,
      data.current_prodon_izlaz.sifra,

    );

    // ---------------------- UPIS NEW VALUE U HTML I U DATA ----------------------
    var input_value = cit_number( $(this).val() );
    
    if ( $(this).val() !== `` && input_value === null ) {
      popup_warn(`Nije numerička vrijednost !!!`)
      return;
    };
    
    $(`#${rand_id}_prodon_skart_izlaz_ukupno`).val( cit_format(input_value, 2) );
    // ---------------------- UPIS NEW VALUE U HTML I U DATA ----------------------



    var popup_text = 
  `Jeste li sigurni da želite promijeniti ŠKART izlaza sa:
  <br>
  <br>
  <span style="font-size: 40px;">${ cit_format( curr_izlaz.kalk_skart || 0, 2) }</span>
  <br>
  na
  <br>
  <span style="font-size: 40px;">${ cit_format(input_value, 2) }</span>

  `;

    async function izlaz_skart_yes() {

      curr_izlaz.kalk_skart = input_value;

      data.current_status.kalk = run_prodon_update_kalk( 

        data.current_status.kalk,
        data.current_proces.row_sifra,
        `izlazi`,
        data.current_prodon_izlaz.sifra,
        curr_izlaz
      );


      var saved_status = await write_work_efect( this_module, data, null, null, curr_izlaz.kalk_sum_sirovina, curr_izlaz.kalk_skart );


      $(`#save_izlaz_skart`).trigger(`click`);


    };

    function izlaz_skart_no() {

      show_popup_modal(false, popup_text, null );
      format_number_input( curr_izlaz.kalk_skart , this_input, null, null);

    };


    if ( curr_izlaz.kalk_skart !== cit_number( $(this_input).val() ) ) {

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', izlaz_skart_yes, izlaz_skart_no, null);
    };

  });


  $(`#${rand_id}_prodon_skart_izlaz_dodaj`).off(`blur`);
  $(`#${rand_id}_prodon_skart_izlaz_dodaj`).on(`blur`, async function(e) {


    e.stopPropagation(); 
    e.preventDefault();


    var this_input = this;
    var curr_time = Date.now();

    var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    if ( !data.current_status?.work_times ) {
      popup_warn(`Prije upisivanja ulaza i izlaza morate kliknuti na START !!!`);
      return;
    };

    if ( !data.current_prodon_izlaz ) {
      popup_warn(`Potrebno je izabrati jedan od izlaza za ovaj proces !!!!`);
      $(this_input).val(``);
      return;
    };


    if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
      popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
      return;
    };


    var curr_izlaz = get_post_kalk_val(

      data.current_status.kalk,
      data.current_proces.row_sifra,
      `izlazi`,
      data.current_prodon_izlaz.sifra,

    );

    // ---------------------- UPIS NEW VALUE U HTML I U DATA ----------------------
    var input_value = cit_number( $(this).val() );
    
    if ( $(this).val() !== `` && input_value === null ) {
      popup_warn(`Nije numerička vrijednost !!!`)
      return;
    };
    
    $(`#${rand_id}_prodon_skart_izlaz_dodaj`).val( cit_format(input_value, 2) );
    // ---------------------- UPIS NEW VALUE U HTML I U DATA ----------------------


    var popup_text = 
  `Jeste li sigurni da želite dodati ŠKART IZLAZA:
  <br>
  <br>
  <span style="font-size: 40px;">${ cit_format(input_value, 2) }</span>

  `;


    async function dodaj_yes() {


      // PAZI OVDJE JE DODAVANJE +=
      // PAZI OVDJE JE DODAVANJE +=
      // PAZI OVDJE JE DODAVANJE +=
      curr_izlaz.kalk_skart = (curr_izlaz.kalk_skart || 0 ) + input_value;

      data.current_status.kalk = run_prodon_update_kalk( 

        data.current_status.kalk,
        data.current_proces.row_sifra,
        `izlazi`,
        data.current_prodon_izlaz.sifra,
        curr_izlaz
      );


      $(this_input).val(``);

      format_number_input( curr_izlaz.kalk_skart ,  $(`#${rand_id}_prodon_skart_izlaz_ukupno`)[0], null, null );


      var saved_status = await write_work_efect( this_module, data, null, null, curr_izlaz.kalk_sum_sirovina, curr_izlaz.kalk_skart );



      var comfirm_msg = `<span style="font-size: 40px;"> DODALI STE ${text_value} !!!</span>`;

      show_popup_modal(true, comfirm_msg, null );

      setTimeout( function() { show_popup_modal(false, comfirm_msg, null );  }, 2000 );


      $(`#save_izlaz_skart`).trigger(`click`);


    };

    function dodaj_no() {

      show_popup_modal(false, popup_text, null );
      $(this_input).val(``);

    };


    if ( $(this_input).val() !== `` ) {

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', dodaj_yes, dodaj_no, null);
    };


  });


  $(`#prodon_save_izlaz`).off(`click`);
  $(`#prodon_save_izlaz`).on(`click`, async function(e) {


    var this_button = this;
    var curr_time = Date.now();

    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    if ( !data.current_prodon_izlaz ) {
      popup_warn(`Potrebno je izabrati jedan od izlaza za ovaj proces !!!!`);
      return;
    };

    if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
      popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
      return;
    };


    var curr_izlaz = get_post_kalk_val(

      data.current_status.kalk,
      data.current_proces.row_sifra,
      `izlazi`,
      data.current_prodon_izlaz.sifra,

    );



    // ------------------------------ AKO PRVI PUT SPREMA IZLAZ ------------------------------
    // ------------------------------ AKO PRVI PUT SPREMA IZLAZ ------------------------------
    // ------------------------------ AKO PRVI PUT SPREMA IZLAZ ------------------------------
    // u tom slučaju nema property prodon_sirov_id


    // kod ulaznih sirovina koje imaju _id ------> taj _id prepišem u prodon_sirov_id 
    // tako da sve PRAVE sirovine kao npr ploče imaju i _id i prodon_sirov_id !!!!!!!!!!!!!!!!!!
    
    
    // ------------------------------ ZA  SADA NE SPREMAM PROCESNE ELEMENTE ------------------------------
    // ------------------------------ ZA  SADA NE SPREMAM PROCESNE ELEMENTE ------------------------------
    // ------------------------------ ZA  SADA NE SPREMAM PROCESNE ELEMENTE ------------------------------
    
    /*
    if ( !curr_izlaz.prodon_sirov_id ) {

      var prodon_sirov_id = await ajax_prodon_save_new_sirov( this_module, this_button);

      // ako je dobio nazad sirov id 
      if ( prodon_sirov_id ) {

        cit_toast(`IZLAZ JE SPREMLJENI KAO SIROVINA!`);
        
        curr_izlaz.prodon_sirov_id = prodon_sirov_id;
        
        $(`#prodon_izlaz_link`).attr(`href`, `#sirov/${prodon_sirov_id}/sklad/${ "SKL1" }/sector/${ "S20" }/level/${ "L1" }` );
        $(`#prodon_izlaz_link`).removeClass(`cit_disable`);
      };

    };
    */
    
    
    data.current_status.kalk = run_prodon_update_kalk( 

      data.current_status.kalk,
      data.current_proces.row_sifra,
      `izlazi`,
      data.current_prodon_izlaz.sifra,
      curr_izlaz
    );


    var result = await update_post_kalk( 

      data.current_status.kalk._id, // arg_id ---> id od kalkulacije
      data.current_proces.row_sifra, // sifra procesa
      `izlazi`, // property na kojem radim update ----> zapravo ovo je array
      data.current_prodon_izlaz.sifra, //  sifra od itema u array-u,
      curr_izlaz

    );


    if ( result.success == true ) cit_toast(`PODATAK SPREMLJEN U BAZU!`);
    
    this_module.make_post_sums(data);
    this_module.prodon_percent_setup(data);
    
    
    console.log(result.kalk);
    
    // ako može biti više izlaza onda resetiraj tako da user ne napravi grešku sljedeći put  -----> mora svaki put izabrati izlaz
    setTimeout( function() {
      if ( data.current_proces.izlazi.length > 1 ) {
        data.current_prodon_izlaz = null;
        $(`#`+ rand_id + `_prodon_select_izlaz`).val(``);
      };
      
    }, 300);



  }); // kraj save izlaz količina


  $(`#save_izlaz_skart`).off(`click`);
  $(`#save_izlaz_skart`).on(`click`, async function(e) {


    var this_button = this;
    var curr_time = Date.now();

    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    if ( !data.current_prodon_izlaz ) {
      popup_warn(`Potrebno je izabrati jedan od izlaza za ovaj proces !!!!`);
      return;
    };


    if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
      popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
      return;
    };


    var curr_izlaz = get_post_kalk_val(

      data.current_status.kalk,
      data.current_proces.row_sifra,
      `izlazi`,
      data.current_prodon_izlaz.sifra,

    );


    var result = await update_post_kalk( 

      data.current_status.kalk._id, // arg_id ---> id od kalkulacije
      data.current_proces.row_sifra, // sifra procesa
      `izlazi`, // property na kojem radim update ----> zapravo ovo je array
      data.current_prodon_izlaz.sifra, //  sifra od itema u array-u,
      curr_izlaz

    );

    if ( result.success == true ) cit_toast(`PODATAK SPREMLJEN U BAZU!`);
    console.log(result.kalk);


    setTimeout( function() {
      if ( data.current_proces.izlazi.length > 1 ) {
        data.current_prodon_izlaz = null;
        $(`#`+ rand_id + `_prodon_select_izlaz`).val(``);
      };
    }, 300);


  }); // kraj save izlaz utrošak



  // -------------------------- OSTATAK  --------------------------
  // -------------------------- OSTATAK  --------------------------
  // -------------------------- OSTATAK  --------------------------


  $(`#save_new_ostatak`).off(`click`);
  $(`#save_new_ostatak`).on(`click`, async function(e) {


    var this_button = this;
    var curr_time = Date.now();

    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    if ( !data.current_prodon_ulaz ) {
      popup_warn(`Potrebno je izabrati ploču za ulaz u proces !!!!`);
      return;
    };

    if ( data.current_status.rn_droped || data.current_status.rn_finished ) {
      popup_warn(`Ne možete upisivati podatke u STORNIRANI ILI ZAVRŠENI RADNI NALOG!!!`);
      return;
    };

    var curr_ulaz = get_post_kalk_val(

      data.current_status.kalk,
      data.current_proces.row_sifra,
      `ulazi`,
      data.current_prodon_ulaz.sifra,

    );


    if ( !curr_ulaz._id ) {
      popup_warn(`Ulaz koji ste izabrali nije ploča ( nema svoj ID sirovine ) !!!!`);
      return;
    };

    
    // if ( result.success == true ) cit_toast(`PODATAK SPREMLJEN U BAZU!`);
    // console.log(result.kalk);


  }); // kraj save ulaz utrošak


  
  
  
  
};