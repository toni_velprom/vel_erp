var module_object = {
create: (data, parent_element, placement ) => {
  var this_module = window[module_url]; 
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };
  
  var { id } = data;
  
  var valid = {
    
    
    filter_status_cat: { element:"single_select", type:"same_width", lock:false, visible:true, label:"Dovršen / NEdovršen / Oboje"},
    
    filter_tip_proizvoda: { element:"multi_select", type:"same_width", lock:false, visible:true, label:"Filter po tipu proizvoda"},

    filter_rok_isporuke_from: { element: "simple_calendar", type: "date", lock: false, visible: true, label: "Rok isporuke OD" }, 
    filter_rok_isporuke_to: { element: "simple_calendar", type: "date", lock: false, visible: true, label: "Rok isporuke DO" }, 
    
    
    filter_product: { element:"single_select", type:"same_width", lock:false, visible:true, label:"Filter po proizvodu"},
    filter_status_tip: { element:"single_select", type:"simple", lock:false, visible:true, label:"Filter po tipu statusa"},
    
    filter_proces_tip_1: { element:"single_select", type:"same_width", lock:false, visible:true, label:"Tip procesa 1"},
    filter_machine_1: { element:"single_select", type:"same_width", lock:false, visible:true, label:"Stroj 1"},
    filter_radne_stanice_1: { element:"single_select", type:"same_width", lock:false, visible:true, label:"Radna stanica 1"},
    
    
    filter_proces_tip_2: { element:"single_select", type:"same_width", lock:false, visible:true, label:"Tip procesa 2"},
    filter_machine_2: { element:"single_select", type:"same_width", lock:false, visible:true, label:"Stroj 2"},
    filter_radne_stanice_2: { element:"single_select", type:"same_width", lock:false, visible:true, label:"Radna stanica 2"},
    
    
    filter_dep_from: { element:"single_select", type:"same_width", lock:false, visible:true, label:"Filter po odjelu pošiljatelja"},
    filter_dep_to: { element:"single_select", type:"same_width", lock:false, visible:true, label:"Filter po odjelu primatelja"},
    
    filter_from: { element:"single_select", type:"", lock:false, visible:true, label:"Filter po pošiljatelju"},
    filter_to: { element:"single_select", type:"", lock:false, visible:true, label:"Filter po primatelju"},
    
    
  };
  
  
  data.filter_rok_isporuke_from = null;
  data.filter_rok_isporuke_to = null;
  

  this_module.set_init_data(data);
  
  var rand_id = `filterstatus`+cit_rand();
  this_module.cit_data[id].rand_id = rand_id;
  
  
  
  var component_html =
`

<div id="${id}" class="filter_status_search_body cit_comp cit_closed" >
  <section>
    <div class="container-fluid">
    
    <i id="filter_statusa_arrow" class="fal fa-angle-down" style="pointer-events: all;" ></i>
    <h4>Filteri statusa:</h4>
    
      <div class="row" style="margin-bottom: 5px;" >
      
        <div class="col-md-2 col-sm-12">
          ${cit_comp(rand_id+`_filter_status_cat`, valid.filter_status_cat, null, "" )}
        </div>
      
      
        <div class="col-md-2 col-sm-12">
          ${cit_comp(rand_id+`_filter_rok_isporuke_from`, valid.filter_rok_isporuke_from, null )}
        </div>
        
        <div class="col-md-2 col-sm-12">
         ${cit_comp(rand_id+`_filter_rok_isporuke_to`, valid.filter_rok_isporuke_to, null )}
        </div>
        
        
      </div>  
      
      <div class="row" style="margin-top: 5px; margin-bottom: 5px;" >
      
        <div class="col-md-3 col-sm-12">

          <div class="row">
            <div class="col-md-12 col-sm-12">
              ${ cit_comp(rand_id+`_filter_tip_proizvoda`, valid.filter_tip_proizvoda, null, "") }
            </div>
          </div>

          <div class="row">
            <div  class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui"
                  id="${rand_id}_filter_tip_proizvoda_box" style="padding-top: 0;">
              <!-- OVDJE IDE LISTA SVIH TIPOVA PROIZVODA ZA FILTER -->
            </div>
          </div>
          
        </div>      
      
      
        <div class="col-md-6 col-sm-12">
          ${ cit_comp(rand_id+`_filter_product`, valid.filter_product, null, "") }
        </div>

      </div>
      
        
      <div class="row" style="margin-top: 5px;" >
        
        
        <div class="col-md-4 col-sm-12">
          ${ cit_comp(rand_id+`_filter_proces_tip_1`, valid.filter_proces_tip_1, null, "") }
        </div>
        
        <div class="col-md-4 col-sm-12">
          ${ cit_comp(rand_id+`_filter_machine_1`, valid.filter_machine_1, null, "") }
        </div>
        
        <div class="col-md-4 col-sm-12">
          ${ cit_comp(rand_id+`_filter_radne_stanice_1`, valid.filter_radne_stanice_1, null, "") }
        </div>
        
        
      </div>
      
      
      <div class="row" style="margin-bottom: 5px;" > 
       
        <div class="col-md-4 col-sm-12">
          ${ cit_comp(rand_id+`_filter_proces_tip_2`, valid.filter_proces_tip_2, null, "") }
        </div>
        
        <div class="col-md-4 col-sm-12">
          ${ cit_comp(rand_id+`_filter_machine_2`, valid.filter_machine_2, null, "") }
        </div>
        
        <div class="col-md-4 col-sm-12">
          ${ cit_comp(rand_id+`_filter_radne_stanice_2`, valid.filter_radne_stanice_2, null, "") }
        </div>
        
      </div>
      
      
      <div class="row" style="margin-top: 5px; margin-bottom: 5px;" >
      
        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_filter_dep_from`, valid.filter_dep_from, null, "") }
        </div>
        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_filter_dep_to`, valid.filter_dep_to, null, "") }
        </div>
        
       
       
        <div class="col-md-3 col-sm-12">
          ${cit_comp(rand_id+`_filter_from`, valid.filter_from, null, "" )}
        </div>
        <div class="col-md-3 col-sm-12">
          ${ cit_comp(rand_id+`_filter_to`, valid.filter_to, null, "") }
        </div>
        
        
      </div>
      

      <div class="row" style="margin-bottom: 15px;" >
       
        <div class="col-md-4 col-sm-12">
          ${ cit_comp(rand_id+`_filter_status_tip`, valid.filter_status_tip, null, "") }
        </div>
        
        <div class="col-md-6 col-sm-12">
          &nbsp;
        </div>
        
        <div class="col-md-2 col-sm-12">
          <button class="blue_btn btn_small_text" id="filter_statuses_btn" style="margin: 20px 0 0 auto; box-shadow: none;">
            FILTRIRAJ
          </button>
        </div>
        
        
      </div>
   
      
    </div>  
  </section>
</div>

`;

  


  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };
  
  wait_for( `$('#${data.id}').length > 0`, async function() {
    
    
    console.log(data.id + 'component injected into html');
    
    // odmah ubaci odjel trenutnog korisnika
    // data.filter_dep_to = window.cit_user?.dep || null;
    // data.filter_status_query.filter_dep_to = window.cit_user?.dep || null;
    
    
    this_module.project_module = await get_cit_module(`/modules/project/project_module.js`, 'load_css');
    this_module.status_module = await get_cit_module(`/modules/status/status_module.js`, 'load_css');
    this_module.product_module = await get_cit_module(`/modules/products/product_module.js`, 'load_css');
    
    
    $(`#filter_statusa_arrow`).off(`click`);
    $(`#filter_statusa_arrow`).on(`click`, function() {
      
      var parent_div = $(`#`+data.id);
      
      console.log(data.statuses);
      
      if ( parent_div.hasClass(`cit_closed`) == true ) {
        parent_div.removeClass(`cit_closed`);
        $(this).css(`transform`, `rotate(180deg)`);
      } else {
        parent_div.addClass(`cit_closed`);
        $(this).css(`transform`, `rotate(0deg)`);
      };
      
    });
    
    
        
    $('#'+rand_id+'_filter_status_cat').data('cit_props', {
     
      desc: `odaberi Dovršen / NEdovršen / Oboje STATUS unutar filtera statusa ${rand_id}`,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'filter_status_cat',
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        
        /*
        MOGUĆA STANJA:
        { "sifra": "SFC1", "naziv": "Nedovršeni statusi" },
        { "sifra": "SFC2", "naziv": "Dovršeni (stari) statusi" },
        { "sifra": "SFC3", "naziv": "Oboje" },
        */
        
        // rasetiraj na početku na null
        data.filter_status_query.filter_status_cat = null;
        
        if ( state?.sifra ==  "SFC1" ) data.filter_status_query.filter_status_cat = { branch_done: false };
        if ( state?.sifra ==  "SFC2" ) data.filter_status_query.filter_status_cat = { branch_done: true };
        
        if ( state?.sifra ==  "SFC3" ) {
          data.filter_status_query.filter_status_cat = 
          { $or: [ { branch_done: true }, { branch_done: false }, { branch_done: {$exists: false } } ] };
        };
        
        data.filter_status_cat = state;
        
        
        // ako sam izabrao bilo što od ovih polja: 
        // filter_status_cat
        // filter_tip_proizvoda
        // filter_rok_isporuke_from
        // filter_rok_isporuke_to
        // ------> moram obrisati filter po specifičnom proizvodu
        
        $('#'+rand_id+'_filter_product').val(``);
        data.filter_product = null;
        data.filter_status_query.filter_product = null;
        
        
        if ( state ) {
          $('#'+current_input_id).val(state.naziv);
        } else {
          $('#'+current_input_id).val(``);
        };
        
        // console.log(`filter_status_cat`);
        // console.log(state);

        
      },
      
    });    
    
    
    $('#'+rand_id+'_filter_rok_isporuke_from').data(`cit_run`, function (state, this_input ) {

      var this_comp_id = this_input.closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;


      if ( state == null || this_input.val() == ``) {
        this_input.val(``);
        data.filter_rok_isporuke_from = null;
        data.filter_status_query.filter_rok_isporuke_from = null;
        
        console.log(`filter_rok_isporuke_from: `, null );
        return;
      };

      data.filter_rok_isporuke_from = state;
      
      // ako sam izabrao bilo što od ovih polja: 
      // filter_status_cat
      // filter_tip_proizvoda
      // filter_rok_isporuke_from
      // filter_rok_isporuke_to
      // ------> moram obrisati filter po specifičnom proizvodu

      $('#'+rand_id+'_filter_product').val(``);
      data.filter_product = null;
      data.filter_status_query.filter_product = null;
      
      data.filter_status_query.filter_rok_isporuke_from = { rok_isporuke: { $gte: state } };
      
      
      console.log("filter_rok_isporuke_from = ", state ? new Date(state) : null );

    });
    
    $('#'+rand_id+'_filter_rok_isporuke_to').data(`cit_run`, function (state, this_input ) {
      
      var this_comp_id = this_input.closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;


      if ( state == null || this_input.val() == ``) {
        this_input.val(``);
        data.filter_rok_isporuke_to = null;
        data.filter_status_query.filter_rok_isporuke_to = null;
        console.log(`filter_rok_isporuke_to: `, null );
        return;
      };

      data.filter_rok_isporuke_to = state;
      
      
      // ako sam izabrao bilo što od ovih polja: 
      // filter_status_cat
      // filter_tip_proizvoda
      // filter_rok_isporuke_from
      // filter_rok_isporuke_to
      // ------> moram obrisati filter po specifičnom proizvodu

      $('#'+rand_id+'_filter_product').val(``);
      data.filter_product = null;
      data.filter_status_query.filter_product = null;
      
      
      data.filter_status_query.filter_rok_isporuke_to = { rok_isporuke: { $lte: state } };
      
      console.log("filter_rok_isporuke_to = ", state ? new Date(state) : null );

    });
    
    

    
    $(`#` + rand_id + `_filter_tip_proizvoda`).data('cit_props', {

      desc: 'odabir multiple vrste tipova proizvoda tako da napravim filter za status listu ' + rand_id,
      local: true,
      show_cols: [ 'naziv' ],
      return: {},
      show_on_click: true,
      list: `tip_proizvoda`,
      cit_run: function(state) {

        var state = cit_deep(state);

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          return;
        };
        

        if ( !data.filter_tip_produkta_table ) data.filter_tip_produkta_table = [];
        data.filter_tip_produkta_table = upsert_item( data.filter_tip_produkta_table, state , `sifra` );
        
        // resetiraj ovaj prop na početku na null
        data.filter_tip_proizvoda = null;
        
        // ako ima nešto u table onda postavi prop da bude lista tj table
        if ( data.filter_tip_produkta_table.length > 0  ) data.filter_tip_proizvoda = data.filter_tip_produkta_table;
        
        // ako je user odabrao samo jedan tip prozivoda
        if ( data.filter_tip_produkta_table.length == 1 ) data.filter_status_query.filter_tip_proizvoda = { "product_tip.sifra" : state.sifra };
        
        // ako je user izabrao više tipova proizvoda
        if ( data.filter_tip_produkta_table.length > 1 ) {
          
          // pretvori query u $or 
          data.filter_status_query.filter_tip_proizvoda = { $or: [] };
          
          $.each( data.filter_tip_produkta_table, function(tip_ind, tip) {
            data.filter_status_query.filter_tip_proizvoda.$or.push({ "product_tip.sifra" : state.sifra });
          });  
          
        };
        
        
        // moram obrisati filter po točnom proizvodu jer nema smisla tražiti tip proizvoda i ona točan proizvod !!!!
        data.filter_product = null;
        data.filter_status_query.filter_product = null;
        
        
        console.log(state);
        this_module.make_tip_proizvoda_list(data);

      },

    });
    
    $('#'+rand_id+'_filter_product').data('cit_props', {
     
      desc: `filter status po proizvodu uniq  ${rand_id}`,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: function() {

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        // uvijek resetiraj uniq_products
        data.uniq_products = [];

        $.each( data.statuses, function(s_ind, status) {
          

          var curr_status = status;
          
          var uniq_product_status = false;
          
          $.each( data.uniq_products, function(up_ind, u_prod) {
            if ( u_prod.item_sifra == curr_status.item_sifra && u_prod.proj_sifra == curr_status.proj_sifra ) uniq_product_status = true;
          });

          // ako nije ponavljanje i samo ako statuds ima product_id  -----> to znači da je povazan s productom !!!
          if ( uniq_product_status == false && curr_status.product_id  ) {

            data.uniq_products.push({ 
              sifra: curr_status.sifra,
              naziv: curr_status.full_product_name,
              item_sifra: curr_status.item_sifra,
              proj_sifra: curr_status.proj_sifra,
              produc_id: curr_status.product_id,
            });
          }

        });
        
        
        return data.uniq_products;


      },
      
      list_width: 800,
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        console.log(state);
        
        
        // BITNO !!!!!
        // BITNO !!!!! ----> ako sam izabrao specifičan proizvod sa liste onda trebam obrisati ove filtere
        // jer nema smisla imat sve te filtere kad user traži specifičan proizvod kojeg je izabrao liste !!!:
        

        
        $('#'+rand_id+'_filter_status_cat').val(``);
        data.filter_status_cat = null;
        data.filter_status_query.filter_status_cat = null;

        $('#'+rand_id+'_filter_rok_isporuke_from').val(``);
        data.filter_rok_isporuke_from = null;
        data.filter_status_query.filter_rok_isporuke_from = null;

        $('#'+rand_id+'_filter_rok_isporuke_to').val(``);
        data.filter_rok_isporuke_to = null;
        data.filter_status_query.filter_rok_isporuke_to = null;
        
        $('#'+rand_id+'_filter_tip_proizvoda').val(``);
        data.filter_tip_proizvoda = null;
        data.filter_status_query.filter_tip_proizvoda = null;

        

        if ( state ) {
          data.filter_product = state;
          data.filter_status_query.filter_product = { product_id: state.produc_id };
          $('#'+current_input_id).val(state.naziv);
        } else {
          data.filter_product = null;
          data.filter_status_query.filter_product = null;
          $('#'+current_input_id).val(``);
        };
        
        
        
  
      },
      
    });    

    
    
    $('#'+rand_id+'_filter_dep_from').data('cit_props', {
     
      desc: `odaberi ODJEL FROM ZA STATUSE unutar filtera statusa ${rand_id}`,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'deps',
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        
        if ( state ) {
          data.filter_dep_from = state;
          data.filter_status_query.filter_dep_from = { "dep_from.sifra" : state.sifra };
        
          $('#'+current_input_id).val(state.naziv);
        } else {
          data.filter_dep_from = null;
          data.filter_status_query.filter_dep_from = null;
        
          $('#'+current_input_id).val(``);
        };
        
        
        
      },
      
    });    
    
    $('#'+rand_id+'_filter_dep_to').data('cit_props', {
     
      desc: `odaberi ODJEL TO ZA STATUSE unutar filtera statusa ${rand_id}`,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'deps',
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;


        if ( state ) {
          $('#'+current_input_id).val(state.naziv);
          data.filter_dep_to = state;
          data.filter_status_query.filter_dep_to = { "dep_to.sifra" : state.sifra };
        } else {
          $('#'+current_input_id).val(``);
          data.filter_dep_to = null;
          data.filter_status_query.filter_dep_to = null;
        };
        
      },
      
    });    
    
    
    $('#'+rand_id+'_filter_from').data('cit_props', {
      desc: 'za odabir _filter_from u modulu FILTER STATUSA ',
      local: false,
      url: '/search_kal_users',
      find_in: [ 'email', 'full_name', 'name' ],
      return: { _id: 1, user_number: 1, email: 1, full_name: 1, name: 1 },
      col_widths: [ 2, 3, 3, 2 ],
      show_cols: ['user_number', 'email', 'full_name', 'name' ],
      query: {},
      show_on_click: false,
      list_width: 700,
      cit_run: function (state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        
        
        
        if ( state ) {
          data.filter_from = state;
          data.filter_status_query.filter_from = { "from.user_number" : state.user_number };
          $('#'+current_input_id).val(state.full_name);
        } else {
          data.filter_from = null;
          data.filter_status_query.filter_from = null;
          $('#'+current_input_id).val(``);
        };
        
        
        
      },
      
      
      
    });  
    
    $('#'+rand_id+'_filter_to').data('cit_props', {
      desc: 'za odabir _filter_to u modulu FILTER STATUSA ',
      local: false,
      url: '/search_kal_users',
      find_in: [ 'email', 'full_name', 'name' ],
      return: { _id: 1, user_number: 1, email: 1, full_name: 1, name: 1 },
      col_widths: [ 2, 3, 3, 2 ],
      show_cols: ['user_number', 'email', 'full_name', 'name' ],
      query: {},
      show_on_click: false,
      list_width: 700,
      cit_run: function (state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        
        
        if ( state ) {
          data.filter_to = state;
          data.filter_status_query.filter_to = { "to.user_number" : state.user_number };
        
          $('#'+current_input_id).val(state.full_name);
        } else {
          data.filter_to = null;
          data.filter_status_query.filter_to = null;
        
          $('#'+current_input_id).val(``);
        };
        
        
      },
      
    });  
    
    
    

    $(`#`+rand_id+`_filter_proces_tip_1`).data('cit_props', {

      desc: 'odabir _filter_proces_tip_1 unutar filtera za statuse  ' + rand_id,
      local: true,
      show_cols: [
        'proces_type_naziv',
      ],
      custom_headers: [
        'NAZIV',
      ],


      return: {},

      show_on_click: true,

      list: function () {

        var unique_list =  [];

        $.each( window.cit_local_list.radne_stanice, function( rs_ind, radna_stanica ) {
          unique_list = insert_uniq( unique_list, radna_stanica, `type` );
        });

        return unique_list;

      },


      cit_run: function(state) {


        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        


        if ( state ) {
          data.filter_proces_tip_1 = state;
          
          $('#'+current_input_id).val(state.proces_type_naziv);
          
          // moram obrisati sljedeća dva polja jer MOŽE BITI BILO KOJI !!!!
          // moram obrisati sljedeća dva polja jer MOŽE BITI BILO KOJI !!!!
          // moram obrisati sljedeća dva polja jer MOŽE BITI BILO KOJI !!!!
          data.filter_machine_1 = null;
          $(`#`+rand_id+`_filter_machine_1`).val(``);
          
          data.filter_radne_stanice_1 = null;
          $(`#`+rand_id+`_filter_radne_stanice_1`).val(``);
          
          
        } else {
          // ako je state null
          data.filter_proces_tip_1 = null;
          $('#'+current_input_id).val(``);
        };



      },

    });

    $(`#`+rand_id+`_filter_machine_1`).data('cit_props', {

      desc: 'odabir _filter_machine_1 unutar filtera za statuse  ' + rand_id,
      local: true,
      show_cols: [
        'machine',
      ],
      custom_headers: [
        'NAZIV',
      ],


      return: {},

      show_on_click: true,

      list: function () {

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        var unique_list =  [];

        $.each( window.cit_local_list.radne_stanice, function( rs_ind, radna_stanica ) {
          
          if ( this_module.prev_selection_rn_filter( data, $('#'+current_input_id)[0], 1, radna_stanica ) ) {
            unique_list = insert_uniq( unique_list, radna_stanica, `machine_sifra` );
          };
        });

        return unique_list;
        

      },

      cit_run: function(state) {


        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        


        if ( state ) {
          
          data.filter_machine_1 = state;
          
          $('#'+current_input_id).val(state.machine);
          
          // MORAM OBRISATI SLJEDEĆE POLJE JER MOŽE BITI BILO KOJE !!!
          // MORAM OBRISATI SLJEDEĆE POLJE JER MOŽE BITI BILO KOJE !!!
          // MORAM OBRISATI SLJEDEĆE POLJE JER MOŽE BITI BILO KOJE !!!


          data.filter_radne_stanice_1 = null;
          $(`#`+rand_id+`_filter_radne_stanice_1`).val(``);

          
          
        } else {
          
          data.filter_machine_1 = null;
          $('#'+current_input_id).val(``);
        };



      },

    });

    $(`#`+rand_id+`_filter_radne_stanice_1`).data('cit_props', {

      desc: 'odabir filter_radne_stanice_1 unutar filtera za statuse  ' + rand_id,
      local: true,
      show_cols: [
        'naziv',
      ],
      custom_headers: [
        'NAZIV',
      ],


      return: {},

      show_on_click: true,

      list: function () {
        
        var list = [];
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        $.each( window.cit_local_list.radne_stanice, function( rs_ind, radna_stanica ) {
          if ( this_module.prev_selection_rn_filter( data, $('#'+current_input_id)[0], 1, radna_stanica ) ) {
            list.push(radna_stanica);
          };
        });
        
        return list
      },


      cit_run: function(state) {


        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        

        if ( state ) {
          data.filter_radne_stanice_1 = state;
          $('#'+current_input_id).val(state.naziv);
        } else {
          data.filter_radne_stanice_1 = null;
          $('#'+current_input_id).val(``);
        };



      },

    });
    
    
    
    
    $(`#`+rand_id+`_filter_proces_tip_2`).data('cit_props', {

      desc: 'odabir _filter_proces_tip_2 unutar filtera za statuse  ' + rand_id,
      local: true,
      show_cols: [
        'proces_type_naziv',
      ],
      custom_headers: [
        'NAZIV',
      ],


      return: {},

      show_on_click: true,

      list: function () {


        var unique_list =  [];

        $.each( window.cit_local_list.radne_stanice, function( rs_ind, radna_stanica ) {
          unique_list = insert_uniq( unique_list, radna_stanica, `type` );
        });

        return unique_list;

      },


      cit_run: function(state) {


        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        if ( 
          !data.filter_proces_tip_1 
          &&
          !data.filter_machine_1 
          &&
          !data.filter_radne_stanice_1 
        ) {
          popup_warn(`Nije moguće izabrati SLJEDEĆI radni nalog ako niste izabrali niti jedno polje za PRETHODNI radni nalog :) !!!`);
         $('#'+current_input_id).val(``);
          data.filter_proces_tip_2 = null;
          return;
        };
        
        
        

        


        if ( state ) {
          
          data.filter_proces_tip_2 = state;
          $('#'+current_input_id).val(state.proces_type_naziv);
          
          
          // moram obrisati sljedeća dva polja jer MOŽE BITI BILO KOJI !!!!
          // moram obrisati sljedeća dva polja jer MOŽE BITI BILO KOJI !!!!
          // moram obrisati sljedeća dva polja jer MOŽE BITI BILO KOJI !!!!
          data.filter_machine_2 = null;
          $(`#`+rand_id+`_filter_machine_2`).val(``);
          
          data.filter_radne_stanice_2 = null;
          $(`#`+rand_id+`_filter_radne_stanice_2`).val(``);
          
          
        } else {
          data.filter_proces_tip_2 = null;
          $('#'+current_input_id).val(``);
        };



      },

    });

    $(`#`+rand_id+`_filter_machine_2`).data('cit_props', {

      desc: 'odabir _filter_machine_2 unutar filtera za statuse  ' + rand_id,
      local: true,
      show_cols: [
        'machine',
      ],
      custom_headers: [
        'NAZIV',
      ],


      return: {},

      show_on_click: true,

      list: function () {
        
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        

        var unique_list =  [];

        $.each( window.cit_local_list.radne_stanice, function( rs_ind, radna_stanica ) {
          
          if ( this_module.prev_selection_rn_filter( data, $('#'+current_input_id)[0], 2, radna_stanica ) ) {
            unique_list = insert_uniq( unique_list, radna_stanica, `machine_sifra` );
          };
        });

        return unique_list;

      },


      cit_run: function(state) {


        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        
        if ( 
          !data.filter_proces_tip_1 
          &&
          !data.filter_machine_1 
          &&
          !data.filter_radne_stanice_1 
        ) {
          popup_warn(`Nije moguće izabrati SLJEDEĆI radni nalog ako niste izabrali niti jedno polje za PRETHODNI radni nalog :) !!!`);
         $('#'+current_input_id).val(``);
          data.filter_machine_2 = null;
          return;
        };
        
        

        

        if ( state ) {
          data.filter_machine_2 = state;
        
          // MORAM OBRISATI SLJEDEĆE POLJE JER MOŽE BITI BILO KOJE !!!
          // MORAM OBRISATI SLJEDEĆE POLJE JER MOŽE BITI BILO KOJE !!!
          // MORAM OBRISATI SLJEDEĆE POLJE JER MOŽE BITI BILO KOJE !!!

          data.filter_radne_stanice_2 = null;
          $(`#`+rand_id+`_filter_radne_stanice_2`).val(``);

          $('#'+current_input_id).val(state.machine);
        } else {
          data.filter_machine_2 = null;
          $('#'+current_input_id).val(``);
        };



      },

    });

    $(`#`+rand_id+`_filter_radne_stanice_2`).data('cit_props', {

      desc: 'odabir filter_radne_stanice_2 unutar filtera za statuse  ' + rand_id,
      local: true,
      show_cols: [
        'naziv',
      ],
      custom_headers: [
        'NAZIV',
      ],


      return: {},

      show_on_click: true,

      list: function () {
        
        var list = [];
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        $.each( window.cit_local_list.radne_stanice, function( rs_ind, radna_stanica ) {
          if ( this_module.prev_selection_rn_filter( data, $('#'+current_input_id)[0], 2, radna_stanica ) ) {
            list.push(radna_stanica);
          };
        });
        
        return list
        
      },


      cit_run: function(state) {


        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        
        if ( 
          !data.filter_proces_tip_1 
          &&
          !data.filter_machine_1 
          &&
          !data.filter_radne_stanice_1 
        ) {
          popup_warn(`Nije moguće izabrati SLJEDEĆI radni nalog ako niste izabrali niti jedno polje za PRETHODNI radni nalog :) !!!`);
         $('#'+current_input_id).val(``);
          data.filter_radne_stanice_2 = null;
          return;
        };
        

        

        if ( state ) {
          data.filter_radne_stanice_2 = state;
          $('#'+current_input_id).val(state.naziv);
        } else {
          data.filter_radne_stanice_2 = null;
          $('#'+current_input_id).val(``);
        };
        

      },

    });

    
    wait_for( `typeof window.cit_local_list.status_conf !== "undefined"`, function() {
      

      $('#'+rand_id+'_filter_status_tip').data('cit_props', {
        
        desc: `odaberi tip statusa unutar FILTER status modula ${rand_id}`,
        local: true,
        show_cols: ['naziv'],
        return: {},
        show_on_click: true,

        list: window.cit_local_list.status_conf,

        filter: null,
        
        cit_run: function(state) {


          console.log(` ------------------------ ovo je odabrani status tip ------------------------ `);
          console.log(state);

          var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

 
          if ( state == null ) {

            $('#'+current_input_id).val(``);
            
            data.filter_status_tip = null;
            data.filter_status_query.filter_status_tip = null;

          } else {

            $('#'+current_input_id).val(state.naziv);
            
            data.filter_status_tip = state;
            data.filter_status_query.filter_status_tip = { "status_tip.sifra" : state.sifra };

          };

        },

      });    /// kraj cit props za status tip

      
    }, 5*1000 );     
    
    

    $(`#filter_statuses_btn`).off(`click`);
    $(`#filter_statuses_btn`).on(`click`, async function() {
      
      
      var this_button = this;
      
      var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      
      
      if ( 
      data.filter_proces_tip_1 
      ||
      data.filter_machine_1
      ||  
      data.filter_radne_stanice_1
      ||  
      data.filter_proces_tip_2 
      ||  
      data.filter_machine_2 
      ||
      data.filter_radne_stanice_2 
      
    ) {
        
        // ovo je opći query koji napravim za bilo koji filter proces/machine/radna stanica
        data.filter_status_query.find_rn_statuses = { "kalk" : { $exists: true  } , "proces_id" : { $exists: true  } };

      };
      
      var query = { $and: [] };
      
      $.each(data.filter_status_query, function( filter_key, filter_obj ) {
        
        if ( filter_obj ) query.$and.push( filter_obj );
        
      });
      
      
      if ( data.filter_proces_tip_1 ) query.filter_proces_tip_1 = data.filter_proces_tip_1.type;
      if ( data.filter_machine_1 ) query.filter_machine_1 = data.filter_machine_1.machine_sifra;
      if ( data.filter_radne_stanice_1 ) query.filter_radne_stanice_1 = data.filter_radne_stanice_1.sifra;
      
      if ( data.filter_proces_tip_2 ) query.filter_proces_tip_2 = data.filter_proces_tip_2.type;
      if ( data.filter_machine_2 ) query.filter_machine_2 = data.filter_machine_2.machine_sifra;
      if ( data.filter_radne_stanice_2 ) query.filter_radne_stanice_2 = data.filter_radne_stanice_2.sifra;
      
    
      var db_result = [];

      var props = {
        filter: null, // status_mod.statuses_filter,
        url: `/filter_statuses`, // /${ hash_data.statuses_page || "none" }`,
        query: query,
        desc: `pretraga kada kliknem na filter button na stranici sa listom statusa !!!!!`,
        
      };

      try {
        db_result = await search_database(null, props );
      } catch (err) {
        console.log(err);
      }

      var status_data = {
        /* ovo su podaci od modula status listu ispod filtera */
        for_module: data.for_module,
        status_type_title: data.status_type_title,
        id: data.status_page_id,
        /* ovo su podaci od modula status listu ispod filtera */
        
        statuses: db_result,
      };

      this_module.status_module.create( status_data, $(`#cont_main_box`) );

      
      // BITNO !!!!!!!!!!!!!
      // također je jako bitno da obnovim data set sa novim statusima u OVOM MODULU
      data.statuses = cit_deep(db_result);
      
      
    });
    

    

  }, 500*1000 );

  return {
    html: component_html,
    id: data.id
  };

},
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};

  
  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
  };
  this_module.set_init_data = set_init_data;
  

  
  
  function prev_selection_rn_filter( data, this_input, rs_num, radna_stanica ) {
    
    
    var is_ok = false;
    // upisujem machine
    if ( this_input.id.indexOf(`_filter_machine_${rs_num}`) > -1 ) {
      // postoji tip procesa
      if ( data[`filter_proces_tip_${rs_num}`] ) {
        if ( radna_stanica.type == data[`filter_proces_tip_${rs_num}`].type ) is_ok = true;
      } else {
        // ako nema tip procesa onda je auto ok
        is_ok = true;
      };
      
    };
      
      
    if ( this_input.id.indexOf(`_filter_radne_stanice_${rs_num}`) > -1 ) { 
      
      
      if ( data[`filter_proces_tip_${rs_num}`] && !data[`filter_machine_${rs_num}`] ) { // proces IMA - machine NEMA

        if ( radna_stanica.type == data[`_filter_proces_tip_${rs_num}`].type ) is_ok = true;
        
        
      } 
      else if ( !data[`filter_proces_tip_${rs_num}`] && data[`filter_machine_${rs_num}`] ) { // proces tip NEMA - machine IMA
        
       if ( radna_stanica.machine_sifra == data[`filter_machine_${rs_num}`].machine_sifra ) is_ok = true;
        
        // ako ima i proces i machine
      } 
      else if ( data[`filter_proces_tip_${rs_num}`] && data[`filter_machine_${rs_num}`] ) { // proces IMA - machine IMA
      
      if ( 
        radna_stanica.type == data[`filter_proces_tip_${rs_num}`].type
        &&
        radna_stanica.machine_sifra == data[`filter_machine_${rs_num}`].machine_sifra
      ) is_ok = true;
        
      }
      else {
        // ako nema niti proces niti machine
        is_ok = true;
        
      };
      
      
    };  
      
    return is_ok;

    
  };
  this_module.prev_selection_rn_filter = prev_selection_rn_filter;
  
  
  function make_tip_proizvoda_list(data) {
    
    // filter po miultiple tipu proizvoda list 

    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    if ( !data.filter_tip_produkta_table || data.filter_tip_produkta_table?.length == 0 ) {
      $(`#${rand_id}_filter_tip_proizvoda_box`).html(``);
      return;
    };
    
    
    var tip_prod_props = {
      desc: 'samo za kreiranje tablice svih tipova prozivoda po kojima želim filtrirati statuse na listi statusa ',
      local: true,
      
      list: data.filter_tip_produkta_table,
      
      show_cols: ["naziv", 'button_delete' ],
      col_widths: [3, 1],
      parent: `#${rand_id}_filter_tip_proizvoda_box`,
      return: {},
      show_on_click: false,
      
      cit_run: function(state) { console.log(state); },
      
      button_delete: async function(e) {
        
        e.stopPropagation();
        e.preventDefault();
        
        var this_button = this;

        var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        data.filter_tip_produkta_table = delete_item(data.filter_tip_produkta_table, sifra , `sifra` );

        $(`#`+parent_row_id).remove();
        
        // ponovo napravi ovu istu tablicu nakon brisanja ovog itema 
        this_module.make_tip_proizvoda_list(data);
        
      },
      
    };
    if (data.filter_tip_produkta_table.length > 0 ) create_cit_result_list( data.filter_tip_produkta_table, tip_prod_props );
    
    
    
  };
  this_module.make_tip_proizvoda_list = make_tip_proizvoda_list;
  

  
  this_module.cit_loaded = true;
 
} // end of module scripts
};

