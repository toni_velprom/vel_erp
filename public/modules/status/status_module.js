var module_object = {
create: ( data, parent_element, placement ) => {
  
  var this_module = window[module_url]; 
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };
  
  
  // VAŽNO -----> data u sebi sadrži:
  /*
  ---------------------------------------
  data.proj_sifra
  data.item_sifra
  data.variant
  ---------------------------------------
  */
  
  var { id } = data;

  var valid = this_module.valid;
  
  // ----> jer NEMA samo jedne project sifre pošto je lista statusa iz raznih projekata na status page
  /*
  if (  
    data.for_module !== `statuses_page` // FOR MODULE == statuses_page onda ne diraj nista  tj ne provjeravaj jel ima projekt sifru !!!! 
    &&
    data.for_module !== `sirov` // FOR MODULE == sirov onda ne diraj nista  tj ne provjeravaj jel ima projekt sifru !!!!  !!!! 
  ) {
  */  
    
  
  var is_visible = `display: block;`;
  
  if ( data.for_module == `product` ) {
    // AKO NEMA PROJECT SIFRE TO ZNAČI DA JE PRAZAN STATUS TJ FRESH TEMPLATE
    if ( !data.proj_sifra ) data = { ...data, statuses: [] };
  };
  
  
  // Ako je statuses page sakrij formu za unos statusa !!!
  //
  if ( data.for_module == `statuses_page` ) {
    is_visible = `display: none;`;
  };
  
  
  // samo kada je na page MENE ČEKAJU i ako je user iz nabave onda prikaži input formu od statusa !!!!
  
  if ( 
    window.location.hash.indexOf(`cit_my_tasks`) > -1
    &&
    data.for_module == `statuses_page`
    &&
    window.cit_user?.dep?.sifra == "NAB" 
  ) {
    // predomislio sam se  ---- ipak ne prikazuj formu na page-u MENE ČEKAJU
    // is_visible = `display: block;`;
  };
  
  
  data.current_status = {};
  
  if ( data.for_module == `production` ) {
    // pošto je u production ionako samo jedan status  !!!!!
    // taj jedan jedini status će postati aktivan status za editiranje
    
    data.current_status = {};
    
    
    $.each( data.statuses, function(s_ind, find_status) {
      
      if ( 
        find_status.status_tip.sifra == `IS16430378755748118` /* ovo je sifra za PROIZVODNI PROCES - RADNI NALOG */ 
        &&
        !find_status.elem_parent // ovo znači da nije child !!!
      ) {
        data.current_status = data.statuses[s_ind];
      };
    });
    
  };
  
  
  if ( window.cit_user ) {

    data.current_status.from = {

      _id: window.cit_user._id,
      name: window.cit_user.name,
      user_number: window.cit_user.user_number,
      full_name: window.cit_user.full_name,
      email: window.cit_user.email,
    };
  
  } else {
    
    popup_warn(`Nedostaje korisnik.<br>Molim vas ulogirajte se i osvježite prozor!!!`);
    return;
    
  };
  
    
  console.log(data);

  
  // prepiši sve _id propse u status_id !!!! jer mi treba za tablicu tj listu statusa
  // prepiši sve _id propse u status_id !!!! jer mi treba za tablicu tj listu statusa
  // prepiši sve _id propse u status_id !!!! jer mi treba za tablicu tj listu statusa
  
  $.each(data.statuses, function(s_ind, stat) {
    if( data.statuses[s_ind]._id ) data.statuses[s_ind].status_id = data.statuses[s_ind]._id;
  });
  // prepiši sve _id propse u status_id !!!! jer mi treba za tablicu tj listu statusa
  // prepiši sve _id propse u status_id !!!! jer mi treba za tablicu tj listu statusa
  // prepiši sve _id propse u status_id !!!! jer mi treba za tablicu tj listu statusa
  
  
  
  this_module.set_init_data(data);
  var rand_id = `status`+cit_rand();
  this_module.cit_data[id].rand_id = rand_id;
  
  
  
  var product_mod_html = ``;
  
  
  
  // ---------------------------------------BITNO-----------------------------------------
  // ---------------------------------------BITNO-----------------------------------------
  // ---------------------------------------BITNO-----------------------------------------
  // ovdje postavljam product module u html ali nije vidljiv - to mi samo služi da mogu pokupiti product data kad mi bude trebalo !!!!
  
  
  // data će imati prop product SAMO SAMO kada se učitava unutar production page-a
  if ( data.product ) {
    
    var production_product_data = data.product;
    
    // direktno upisujem podatke od trenutnog producta u CIT DATA MODULE
    // direktno upisujem podatke od trenutnog producta u CIT DATA MODULE
    // TO JE ISTO KAO DA SAM NAPRAVIO set_init_data unutar product modula !!!!
    
    this_module.product_module.cit_data[production_product_data.id] = production_product_data;
    
    var link = `#project/${production_product_data.proj_sifra || null }/item/${production_product_data.item_sifra || null }/variant/${ production_product_data.variant || null }/status/null`;
    
    product_mod_html = 
  `
  <div  id="${production_product_data.id}" class="container-fluid cit_comp product_comp"  
        style="margin-bottom: 5px; display: ${ window.location.hash.indexOf(`#production`) > -1 ?  "flex" : "none" } "
      
      data-link="${link}" 
      data-product_id="${ production_product_data._id || '' }" 
      data-product_tip="${ production_product_data.tip.sifra || '' }" 
      data-proj_sifra="${production_product_data.proj_sifra || '' }"
      data-item_sifra="${production_product_data.item_sifra || '' }"
      data-variant="${ production_product_data.variant || '' }"
      data-sifra="${ production_product_data.sifra || '' }" >
  
  <div class="row" >
  
    <div  class="col-md-12 col-sm-12 cit_result_table statuses_box result_table_inside_gui" 
          id="${production_product_data.id}_kalkulacija_box" style="padding-top: 0; background: #f1f4f6;">
          
      <!-- OVDJE IDU SVE KALKULACIJE ZA TAJ PROIZVOD -->
      
    </div> 
    
  </div>


</div>`;
  
  };
  
  // ---------------------------------------BITNO-----------------------------------------
  // ------------------END------------------BITNO-----------------------------------------
  // ---------------------------------------BITNO-----------------------------------------
  
  
  
  
  
  
  var component_html =
`
<div id="${id}" class="cit_comp status_comp">

  <div style="  ${is_visible}
                height: auto;
                background: #d6eaf7;
                border-radius: 6px;
                margin-top: 5px;
                margin-bottom: 5px;
                padding: 0 10px;">
                
    
    <div id="production_status_box" 
     style="display: ${ window.location.hash.indexOf(`#production`) > -1 ? "block" : "none" }; 
            width: 100%; 
            overflow: hidden;
            float: left;
            clear: both;"> 

      <div class="row">
        <div class="col-md-12 col-sm-12" id="prodon_user_title" style="font-size: 56px; font-weight: 700; text-align: center;">
         ${ window.cit_user?.full_name || "KORISNIK NIJE ULOGIRAN !!!!" }
        </div>
      </div>
     
      <div class="row">
        <div  class="col-md-12 col-sm-12" id="prodon_product_title" 
              style="font-size: 26px; font-weight: 700; text-align: center; margin: 10px 0;">
         <!-- 485-1 Podni stalak 530x536x536 / Super akcija / FLOOR DISPLAY STAND / SLO-NESQUIK -->
         <br>
         <!-- REZANJE ZUND PROCES 1. -->
        </div>
      </div> 
      
      <div class="row">
        
        <div class="col-md-9 col-sm-12">
          <div class="prodon_perc_box">
            <div class="prodon_perc_val" style="width: 0%">&nbsp;</div> 
          </div>
        </div>
        
        <div  class="col-md-3 col-sm-12 prodon_perc_ratio" 
              style="font-size: 30px; font-weight: 700; text-align: center;" >
          0 / 0
        </div>
        
        
      </div>
      
      
      
      <div class="row" id="prodon_file_list_box" >
    
        <!-- OVDJE IDU RN FILOVI ZA OVA RADNI NALOG -->
        
      </div>
      
      
      <div class="row" id="prodon_check_boxes">
      
        <div class="col-md-2 col-sm-12">
          ${ cit_comp( rand_id + `_prodon_drugi_zad`, valid.prodon_drugi_zad, false ) }
        </div>
        
        <div class="col-md-2 col-sm-12">
          ${ cit_comp( rand_id + `_prodon_problem_nalog`, valid.prodon_problem_nalog, false ) }
        </div>
        
        <div class="col-md-2 col-sm-12">
          ${ cit_comp( rand_id + `_prodon_problem_rad`, valid.prodon_problem_rad, false ) }
        </div>
        
        <div class="col-md-2 col-sm-12">
          ${ cit_comp( rand_id + `_prodon_problem_stroj`, valid.prodon_problem_stroj, false ) }
        </div>
        
        <div class="col-md-2 col-sm-12">
          ${ cit_comp( rand_id + `_prodon_problem_worker`, valid.prodon_problem_worker, false ) }
        </div>
        
        <div class="col-md-2 col-sm-12">
          ${ cit_comp( rand_id + `_prodon_kraj_rad_vrem`, valid.prodon_kraj_rad_vrem, false ) }
        </div>
        
      </div>
      
      <div class="row prodon_work_desc_box" >
      
        <div class="col-md-12 col-sm-12">
          ${ cit_comp(
            rand_id+`_prodon_work_desc`, 
            valid.prodon_work_desc, 
            "",
            null,
            `min-height: 66px; height: 66px; font-size: 17px;  font-weight: 700; letter-spacing: 0.05rem;` 
          ) }
        </div> 

      </div>
      
      <div class="row" style="padding-top: 10px; padding-bottom: 10px;">
       
       
        <div class="col-md-3 col-sm-12">
          <button class="production_btn" id="prep_work_btn" style="background: #0fb7ea; color: #fff;">
            PRIPREMA
          </button>
        </div> 
       
       
        <div class="col-md-3 col-sm-12">
          <button class="production_btn" id="start_work_btn" style="background: #bf0060; color: #fff;">
            RAD
          </button>
        </div>
      
        <div class="col-md-3 col-sm-12">
          <button class="production_btn" id="pauza_work_btn" style="background: #e5a71a; color: #000;">
            PAUZA
          </button>
        </div>
      
        <div class="col-md-3 col-sm-12">
          <button class="production_btn" id="stop_work_btn">
            STOP
          </button>
        </div>
      
      </div> <!--KRAJ ROW-->
      
      
      <div class="row">
       
        <div class="col-md-6 col-sm-12">
          <button class="production_btn" id="done_work_btn" style="width: 300px; background: #28517a; margin-left: auto; margin-right: auto;" >
            PROCES GOTOV
          </button>
        </div>
        
        
        <div class="col-md-6 col-sm-12">
          <button class="production_btn" id="odustali_work_btn" style="width: 300px; background: #bf0060; margin-left: auto; margin-right: auto;" >
            ODUSTALI OD PROCESA
          </button>
        </div>
        
      </div>
      
      <div class="row" id="prodon_ulaz_title_row" >
        <div class="col-md-12 col-sm-12">
          <h4>POTREBNO ---- kom</h4>
        </div>
      </div>
      
      
      <div class="row prodon_input_box">
        
        <div class="col-md-10 col-sm-12">
          ${ cit_comp(rand_id+`_prodon_select_ulaz`, valid.prodon_select_ulaz, null, "", `width: calc(100% - 50px);` ) }
          
          <a  href="#" target="_blank" onclick="event.stopPropagation();"
              class="cit_disable blue_btn btn_small_text small_btn_link_left prodon_link_to_sirov"
              id="prodon_ulaz_link" >
              <i style="font-size: 18px;" class="far fa-external-link-square-alt"></i>
          </a>
          
        </div>
        
        <div class="col-md-2 col-sm-12">
          <button class="production_btn" id="find_ulaz_qr">
            TRAŽI 
          </button>
        </div>
        
      </div>
      
      <div class="row prodon_input_box" id="prodon_ulaz_box">
       
        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_prodon_utrosak_ukupno`, valid.prodon_utrosak_ukupno, "") }
        </div>
        
        
        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_prodon_utrosak_dodaj`, valid.prodon_utrosak_dodaj, "") }
        </div>
        
        
        <div class="col-md-2 col-sm-12">
          <button class="production_btn" id="save_ulaz_utrosak">
            SPREMI UTROŠAK 
          </button>
        </div>
        
        
        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_prodon_skart_ulaz_ukupno`, valid.prodon_skart_ulaz_ukupno, "") }
        </div>
        
        
        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_prodon_skart_ulaz_dodaj`, valid.prodon_skart_ulaz_dodaj, "") }
        </div>
        
        
        <div class="col-md-2 col-sm-12">
          <button class="production_btn" id="save_ulaz_skart">
            ŠKART ULAZ
          </button>
        </div>
        
        
      </div>
      
      
      
      
      <div class="row prodon_input_box" id="prodon_ulaz_korekcija_box" style="display: flex; justify-content: center; background: #f3e6e9; margin: 0;">
       
        <!--
        <div class="col-md-2 col-sm-12" style="margin-top: 20px;" >
          ${ cit_comp(rand_id+`_prodon_select_ulaz_primka`, valid.prodon_select_ulaz_primka, null, "") }
        </div>
        -->
        
        <div class="col-md-2 col-sm-12" style="margin-top: 20px;" >
        

          ${ cit_comp(
            rand_id+`_prodon_ulaz_palet_doc`,
            valid.prodon_ulaz_palet_doc,
            "",
            null,
            `font-size: 32px; font-weight: 700; text-transform: uppercase;`
          )}
        
        </div>
        
        
        <div class="col-md-2 col-sm-12" style="margin-top: 20px;" >
        

          ${ cit_comp(
            rand_id+`_prodon_ulaz_palet_sifra`,
            valid.prodon_ulaz_palet_sifra,
            "",
            null,
            `font-size: 32px; font-weight: 700; text-transform: uppercase; letter-spacing: 0.1rem;`
          )}
        
        </div>
       
       
        <div class="col-md-2 col-sm-12" style="margin-top: 20px;" >
          ${ cit_comp(rand_id+`_prodon_ulaz_palet_count`, valid.prodon_ulaz_palet_count, "") }
        </div>
        
        <div class="col-md-2 col-sm-12" style="margin-top: 20px;">
          <button class="production_btn" id="print_palet_card">
            PRINT PALET. KARTICE
          </button>
        </div>
       
       
        <div class="col-md-2 col-sm-12" style="margin-top: 20px;">
          ${ cit_comp(rand_id+`_prodon_ulaz_korekcija`, valid.prodon_ulaz_korekcija, "") }
        </div>
        
        
        <div class="col-md-2 col-sm-12" style="margin-top: 20px;">
          <button class="production_btn" id="save_ulaz_korekcija">
            SPREMI KOREKCIJU 
          </button>
        </div>
        
        
        
      </div>
      
            
      
      
      
      
      <div class="row" id="prodon_izlaz_title_row" >
        <div class="col-md-12 col-sm-12">
          <h4>POTREBNO ---- kom</h4>
        </div>
      </div>
      
      
      <div class="row prodon_input_box">
      
        <div class="col-md-10 col-sm-12">
          ${ cit_comp(rand_id+`_prodon_select_izlaz`, valid.prodon_select_izlaz, null, "", `width: calc(100% - 50px);` ) }
          
          <a  href="#" target="_blank" onclick="event.stopPropagation();"
              class="cit_disable blue_btn btn_small_text small_btn_link_left prodon_link_to_sirov" 
              id="prodon_izlaz_link" >
              <i style="font-size: 18px;" class="far fa-external-link-square-alt"></i>
          </a>
          
          
        </div>
        
        <div class="col-md-2 col-sm-12">
          <button class="production_btn" id="print_izlaz_qr">
            PRINT KARTICE
          </button>
        </div>
        
        
      </div>
      
      
      <div class="row prodon_input_box" id="prodon_izlaz_box"> 
       
        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_prodon_izlaz_ukupno`, valid.prodon_izlaz_ukupno, "") }
        </div>
        
        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_prodon_izlaz_dodaj`, valid.prodon_izlaz_dodaj, "") }
        </div>
        
        <div class="col-md-2 col-sm-12">
          <button class="production_btn" id="prodon_save_izlaz">
            SPREMI IZLAZ
          </button>
        </div>
        
        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_prodon_skart_izlaz_ukupno`, valid.prodon_skart_izlaz_ukupno, "") }
        </div>
        
        
        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_prodon_skart_izlaz_dodaj`, valid.prodon_skart_izlaz_dodaj, "") }
        </div>
        
        
        <div class="col-md-2 col-sm-12">
          <button class="production_btn" id="save_izlaz_skart">
            ŠKART IZLAZ
          </button>
        </div>
        
      </div>
      
      <div class="row prodon_input_box" id="ostatak_def_box">
       
        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_prodon_ostatak_val`, valid.prodon_ostatak_val, "") }
        </div>
        
        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_prodon_ostatak_kontra`, valid.prodon_ostatak_kontra, "") }
        </div>
        
        
        <div class="col-md-2 col-sm-12">
          <button class="production_btn" id="save_new_ostatak">
            SPREMI OSTATAK
          </button>
        </div>
        
      </div>
      
      
      <div class="row prodon_input_box" id="select_ostatak_box">
      
        <div class="col-md-10 col-sm-12">
          ${ cit_comp(rand_id+`_prodon_select_ostatak`, valid.prodon_select_ostatak, null, "", `width: calc(100% - 50px);`  ) }
          
          
          <a  href="#" target="_blank" onclick="event.stopPropagation();"
              class="cit_disable blue_btn btn_small_text small_btn_link_left prodon_link_to_sirov"
              id="prodon_ostatak_link" >
              <i style="font-size: 18px;" class="far fa-external-link-square-alt"></i>
          </a>
          
        </div>
        
        
        <div class="col-md-2 col-sm-12">
          <button class="production_btn" id="print_ostatak_qr">
            PRINT KARTICE 
          </button>
        </div>
        
      </div>
      
      
      
      <div class="row prodon_input_box" id="prodon_izlaz_box"> 
       
        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_prodon_ostatak_ukupno`, valid.prodon_ostatak_ukupno, "") }
        </div>
        
        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_prodon_ostatak_dodaj`, valid.prodon_ostatak_dodaj, "") }
        </div>
        
        <div class="col-md-2 col-sm-12">
          <button class="production_btn" id="prodon_save_ostatak">
            SPREMI OSTATAK
          </button>
        </div>
        
        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_prodon_skart_ostatak_ukupno`, valid.prodon_skart_ostatak_ukupno, "") }
        </div>
        
        
        <div class="col-md-2 col-sm-12">
          ${ cit_comp(rand_id+`_prodon_skart_ostatak_dodaj`, valid.prodon_skart_ostatak_dodaj, "") }
        </div>
        
        
        <div class="col-md-2 col-sm-12">
          <button class="production_btn" id="save_ostatak_skart">
            ŠKART OSTATKA
          </button>
        </div>
        
        
      </div>
      
      
      
      
      
      
      

    </div> <!--KRAJ PROD STATUS BOX-->
    
    <br style="display: block; clear: both; line-height: 3px; width: 100%;" >
    
    
    <div class="row" id="${rand_id}_standardna_forma_za_slanje_statusa" 
      style=" padding-bottom: 10px;
              margin-top: ${ window.location.hash.indexOf('#production') > -1 ? '50px' : '10px' }; 
              background: ${ window.location.hash.indexOf('#production') > -1 ? '#fbdcba' : 'none' };" >
      <!-- 
      display: ${ window.location.hash.indexOf(`#production`) > -1 ? "none" : "flex" };
      -->
      

      <div class="col-md-6 col-sm-12"> 
          <!--LIJEVA STRANA-->
       
        <div class="row">
         
          <div class="col-md-6 col-sm-12 status_from_box">
            ${ cit_comp(rand_id+`_status_from`, valid.status_from, data.current_status.from, data.current_status.from.full_name ) }
          </div>

          <div class="col-md-6 col-sm-12">
            ${ cit_comp(rand_id+`_status_tip`, valid.status_tip, null, '') }
          </div>
        </div>
        
        <div class="row">
          <div class="col-md-6 col-sm-12">
            ${ cit_comp(rand_id+`_dep_to`, valid.dep_to, null, '') }
          </div>

          <div class="col-md-6 col-sm-12">
            ${ cit_comp(rand_id+`_status_to`, valid.status_to, null, "" ) }
          </div>
        </div>
        
        
      <!-- KRAJ LIJEVA STRANA-->  
      </div>
      
      <div class="col-md-6 col-sm-12"> 
       
        <!--DESNA STRANA-->
        
        <div class="row">
          <div class="col-md-12 col-sm-12" id="status_komentar_box">
            ${ cit_comp(rand_id+`_status_komentar`, valid.status_komentar, null, "", `min-height: 97px;` ) }
          </div>
        </div>
        
        <!-- KRAJ DESNA STRANA -->
      </div>

    </div> 


    <!-- style="display: ${ window.location.hash.indexOf(`#production`) > -1 ? "none" : "flex" };" -->

   <div class="row" id="${rand_id}_worker_error_box" style="display: none;" >

      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_worker_error_cost`, valid.worker_error_cost, "") }
        ${ cit_comp(rand_id+`_worker_error_users`, valid.worker_error_users, null, "" ) }
      </div>
      
      <div class="col-md-5 col-sm-12" >
        ${ cit_comp(rand_id+`_worker_error_possible_solution`, valid.worker_error_possible_solution, null, "", `min-height: 97px;` ) }
      </div>
      
      <div class="col-md-5 col-sm-12" >
        ${ cit_comp(rand_id+`_worker_error_solution`, valid.worker_error_solution, null, "", `min-height: 97px;` ) }
      </div>

    </div> 
    
    <div class="row" id="${rand_id}_worker_error_users_row" style="display: none;" >
     
      <div  class="col-md-12 col-sm-12 cit_result_table statuses_box result_table_inside_gui" 
            id="${rand_id}_worker_error_users_box" style="padding: 10px 15px 0; background: transparent;">
        <!-- OVDJE IDE LISTA SVIH USER KOJI SU ODGOVORNI ZA NEKU GREŠKU   -->    
      </div> 
      
    </div>
    
   
   
   
    <div class="row" id="${rand_id}_rn_files_select_box" style="display: none;" >

      <div class="col-md-12 col-sm-12">
        ${ cit_comp(rand_id+`_rn_files`, valid.rn_files, null, "" ) }
      </div>

    </div> 
    
    
    <div class="row">
      <div  class="col-md-12 col-sm-12 cit_result_table statuses_box result_table_inside_gui" 
            id="${rand_id}_rn_files_list_box" style="padding: 10px 15px 0; background: transparent;">
        <!-- OVDJE IDE LISTA SVIH FILOVA ZA RADNI NALOG  -->    
      </div> 
    </div>
    
    
    <div class="row" id="${rand_id}_rn_alat_select_box" style="display: none;" >

      <div class="col-md-11 col-sm-12">
        ${ cit_comp(rand_id+`_rn_alat`, valid.rn_alat, null, "" ) }
      </div>
      
      <div class="col-md-1 col-sm-12" >
        <a  id="${rand_id}_link_to_alat" href="#" target="_blank" onclick="event.stopPropagation();"
            class="blue_btn btn_small_text small_btn_link_left" 
            style="box-shadow: none; margin: 22px auto 0 0; width: 28px; height: 28px;" >

          <i style="font-size: 18px;" class="far fa-external-link-square-alt"></i>

        </a>
      </div>
      

    </div>
    
    
    <div class="row" id="${rand_id}_pre_production_tezina_i_procjena_box" >

      <div class="col-md-1 col-sm-12">
        ${ cit_comp(rand_id+`_tezina`, valid.tezina, "") }
      </div>

      <div class="col-md-1 col-sm-12">
        ${ cit_comp(rand_id+`_est_deadline_hours`, valid.est_deadline_hours, "") }
      </div>

      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_est_deadline_date`, valid.est_deadline_date, null, "") }
      </div>


      <div class="col-md-1 col-sm-12">
        ${ cit_comp(rand_id+`_work_time_sum`, valid.work_time_sum, "") }
      </div>

    </div>  
    
    
    <div class="row"  id="${rand_id}_choose_sirovina_box" style="display: ${ ( window.cit_user?.dep?.sifra  == "NAB" ) ? "flex" : "none" };" > 

      <div class="col-md-7 col-sm-12" id="${rand_id}_find_sirov_in_status">
        <!-- OVDJE UBACUJEM FIND SIROV MODULE UNUTAR STATUSA -->
      </div>
      
      <div class="col-md-1 col-sm-12">
      
        <a  id="${rand_id}_link_to_sirov" href="#" target="_blank" onclick="event.stopPropagation();"
            class="blue_btn btn_small_text small_btn_link_left" 
            style="box-shadow: none; margin: 20px auto 0 0; width: 28px; height: 28px;" >
            
          <i style="font-size: 18px;" class="far fa-external-link-square-alt"></i>
          
        </a>
        
      </div>

      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_order_sirov_count`, valid.order_sirov_count, "") }
      </div>
      

    </div>
    
    <!--  KONTAKTI DOBAVLJAČA OD OVE SIROVINE KOJU USER MOŽE ZAHTJEVATI U STATUSU-->
    <div class="row" style="padding: 5px 15px 0;">
      <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui svi_kontakti_dobavljac_box" id="${rand_id}_svi_kontakti_dobavljac_box" style="padding: 2px;" >

        <!-- OVDJE JE POPIS SVIH KONTAKTA OD OVOG DOBAVLJAČA -->
      </div>
    </div>
    
    
    <!-- style="display: ${ window.location.hash.indexOf(`#production`) > -1 ? "none" : "flex" };" -->
    
    <div class="row for_pre_production" id="${rand_id}_add_status_work_box" >
      
      <div class="col-md-1 col-sm-12">
        ${ cit_comp(rand_id+`_worker`, valid.worker, data.current_status.from || null, data.current_status.from?.full_name || "" ) }
      </div>
      
      <div class="col-md-1 col-sm-12">
        ${ cit_comp(rand_id+`_add_work_time`, valid.add_work_time, "") }
      </div>
      
      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_add_work_from_time`, valid.add_work_from_time, null, "") }
      </div>
      
      
      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_add_work_to_time`, valid.add_work_to_time, null, "") }
      </div>
      
      
      <div class="col-md-1 col-sm-12">
        ${ cit_comp(rand_id+`_work_cat`, valid.work_cat, null, "" ) }
      </div>
      
      <div class="col-md-4 col-sm-12">
      
        ${ cit_comp(
          rand_id+`_work_desc`,
          valid.work_desc,
          "",
          null,
          `height: 68px; min-height: 0; pointer-events: all;` 
        )}
        
      </div>
      
      
      <div class="col-md-1 col-sm-12">
        <button class="blue_btn btn_small_text" id="${rand_id}_add_work_btn" style="margin: 20px 0 0 auto; box-shadow: none;">
            DODAJ RAD
        </button>
      </div>
      
    </div>
    
    <!-- LISTA SVIH ZAPISA RADA ZA PRED_PRODUKCIJU TJ. ZA CAD  i PRODAJU !!!! -->
    <div  class="row">
      <div  class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" 
            id="${rand_id}_status_work_list_box" style="padding: 10px 15px 0; background: transparent;">
        <!-- OVDJE IDE LISTA WORK TIMES KOJE JE ZAPISUJU USERI UNUTAR NEKOG STATUSA -->    
      </div> 
    </div>
  
    <div  class="row" style=" margin-top: 10px; margin-bottom: 10px;" >
     
      <div class="col-md-6 col-sm-12" >
        ${ cit_comp(rand_id+`_status_docs`, valid.status_docs, "", "", `margin: 10px auto 10px; max-width: 300px;` ) }
      </div>
      
      <div class="col-md-3 col-sm-12">
      
        <button class="blue_btn btn_small_text" id="${rand_id}_quit_status" style="margin: 10px 0 0 auto; box-shadow: none; display: none;">
          ODUSTANI
        </button>
      
      </div>

      <div class="col-md-3 col-sm-12">

        <button class="blue_btn btn_small_text" id="${rand_id}_save_status" style="margin: 10px 0 0 auto; box-shadow: none;">
          KREIRAJ STATUS
        </button>

        <button class="violet_btn btn_small_text" id="${rand_id}_update_status" style="margin: 10px 0 0 auto; box-shadow: none; display: none;">
          AŽURIRAJ STATUS
        </button>

        <button class="violet_btn btn_small_text" id="${rand_id}_reply_status" style=" background: #207133; margin: 10px 0 0 auto; box-shadow: none; display: none;">
          ODGOVORI
        </button>

      </div>

    </div>
    
    
  
  </div>  <!-- KRAJ PLAVE FORME ZA UNOS STATUSA -->
  
  
  
  <!--
  <div class="row" style="display: ${ window.location.hash.indexOf(`#statuses_page`) == -1 ? "flex" : "none" };" >
    <div class="col-md-12 col-sm-12">
      PRETRAGA STATUSA !!!!
    </div>
  </div>
  -->
  
  
<div  class="page_row" id="${ cit_rand() + "_page_row" }" data-id="${ "_________" }">
 
  <div class="page_num_arrows_box">
   
    <div class="page_arrow_prev"><i class="fas fa-chevron-circle-left"></i></div>
    
    <div class="page_num_box">
      <div class="page_num_curr">${ 1 }</div>
      <div class="page_num_slash">/</div>
      <div class="page_num_max">${ 1 + ( data.statuses_count ? " (" + data.statuses_count + ")" : "" )  }</div>
    </div>
    
    <div class="page_arrow_next"><i class="fas fa-chevron-circle-right"></i></div>
    
  </div>
</div>
  
  
  
  <div class="row" >
    <div  class="col-md-12 col-sm-12 cit_result_table statuses_box result_table_inside_gui" 
          id="${rand_id}_statuses_box" style="padding-top: 0; background: #f1f4f6;">
      <!-- OVDJE IDE LISTA STATUSA OD OVOG PROIZVODA TVRTKE -->    
    </div> 
  </div>


</div> 

${product_mod_html}


`;

// `  
// ovaj backtick iznad služi samo da popravim krive boje jer bracket ne prepoznaje OPTIONAL CHAINING !!!!!!!

  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };
  
  wait_for( `$('#${data.id}').length > 0`, async function() {
    
    console.log(data.id + 'component injected into html');
    toggle_global_progress_bar(false);
    
    
    if ( data.for_module == `statuses_page` ) {
      
      $(`html`)[0].scrollTop = 0;
      
      $(`#cit_page_title h3`).text(data.status_type_title);
      document.title = data.status_type_title.toUpperCase() + " VELPROM";
      
      // pričekaj malo i onda napravi update brojača statusa !!!!
      // setTimeout(function() { get_statuses_count(); }, 500 );
      
      get_statuses_count();
      
    };
    
    
    // ppppppp
    // ------------------------------------------------------- PRODUCTION -------------------------------------------------------
    // ----------------------START---------------------------- PRODUCTION -------------------------------------------------------
    // ------------------------------------------------------- PRODUCTION -------------------------------------------------------
    
    if ( data.for_module == `production` ) {
      
      var prodon_setup = await this_module.production_setup(data);
      
      
    };
    
    // ------------------------------------------------------- PRODUCTION -------------------------------------------------------
    // ----------------------END------------------------------ PRODUCTION -------------------------------------------------------
    // ------------------------------------------------------- PRODUCTION -------------------------------------------------------
    
    
    $('.cit_tooltip').tooltip();
    
    // this_module.preview_module = await get_cit_module(`/modules/previews/cit_previews.js`, `load_css`);
    
    this_module.sirov_module = await get_cit_module(`/modules/sirov/sirov_module.js`, `load_css`);
    
    var rand_id = this_module.cit_data[data.id].rand_id;
    
    // ----------------------------------------------------------------------
    var find_sirov = await get_cit_module(`/modules/sirov/find_sirov.js`, null);
    this_module.find_sirov = find_sirov;
    
    if ( !find_sirov ) {
      toggle_global_progress_bar(false);
      return;
    };
    
    var find_sirov_data = {
      id: rand_id+`_find_sirov`,
      for_module: "status",
      pipe: this_module.make_sirov_in_status,
    };
    find_sirov.create( find_sirov_data, $(`#${data.rand_id}_find_sirov_in_status`) );    
    
    
    wait_for(`$("#${data.rand_id}_find_sirov_in_status").find(".single_select").length > 0`, function() {
      this_module.make_sirov_in_status(data.sirov, data);
    }, 5*1000);

    // ----------------------------------------------------------------------
    
    
    
    
    // nemoj scrollati na početak kad ubacuješ statuse
    // $(`html`)[0].scrollTop = 0;

    
    initial_get_admin_conf();
    
    
    function next_i_finish_statuses_from_parent( data, curr_status, status_tipovi, filtered_list ) {


      var parent_status = null;

      // --------------------------- AKO STATUS IMA PARENT ---------------------------

      // prvo nađi parent status object 
      // unutar SVIH STATUSA ZA OVAJ PRODUCT !!!!
      $.each(data.statuses, function(ind, parent) {
        if ( curr_status.elem_parent == parent.sifra ) parent_status = parent;
      });


      // ----------------- AKO JE MOŽDA STARA VERZIJA STATUS TIPA SVAKAKO NAPRAVI UPDATE !!!
      $.each( window.cit_local_list.status_conf, function(fresh_ind, fresh_tip ) {
        if ( fresh_tip.sifra == parent_status.status_tip.sifra ) {
          parent_status.status_tip = cit_deep(fresh_tip);
        };
      });


      // ----------------- TRAŽI NEXT STATUSE UNUTAR PARENTA
      $.each( parent_status.status_tip.next_statuses, function(n_ind, next ) {
        $.each( status_tipovi, function(t_ind, tip ) {
          if ( tip.sifra  == next.sifra ) filtered_list.push(next);
        });  
      });

      // ----------------- TRAŽI FINISH STATUSE UNUTAR PARENTA
      $.each( parent_status.status_tip.grana_finish, function( f_ind, finish ) {
        $.each( status_tipovi, function(t_ind, tip ) {
        if ( tip.sifra == finish.sifra ) filtered_list.push(finish);
        });  
      });


      if ( filtered_list.length > 0  ) {
        var criteria = [ 'next_status_num' ];
        multisort( filtered_list, criteria );
      };  

      return filtered_list;

    };
    
    
    function next_i_finish_statuses_from_old_status( data, curr_status, status_tipovi, filtered_list) {
      
      
      // --------------------------- STATUS NEMA PARENT ---------------------------
      
      if ( curr_status.old_status_tip ) {
      
        // ----------------- AKO JE STARA VERZIJA STATUS TIPA SVAKAKO NAPRAVI UPDATE !!!
        $.each( window.cit_local_list.status_conf, function(fresh_ind, fresh_tip ) {
          if ( fresh_tip.sifra == curr_status.old_status_tip.sifra  ) {
            curr_status.old_status_tip = cit_deep(fresh_tip);
          };
        });


        // ----------------- TRAŽI NEXT STATUSE UNUTAR CURRENT OOOLLLD STATUSA
        $.each( curr_status.old_status_tip.next_statuses, function(n_ind, next ) {
          $.each( status_tipovi, function(t_ind, tip ) {
          if ( next.sifra == tip.sifra ) filtered_list.push(next);
          });  
        });
        
        // ----------------- TRAŽI FINISH STATUSE UNUTAR OOOLLLD STATUSA

        $.each( curr_status.old_status_tip.grana_finish, function( f_ind, finish ) {
          $.each( status_tipovi, function(t_ind, tip ) {
          if ( finish.sifra == tip.sifra ) filtered_list.push(finish);
          });  
        });
        

      }; // kraj ako ima old status tip !!!
      

      if ( filtered_list.length > 0  ) {
        var criteria = [ 'next_status_num' ];
        multisort( filtered_list, criteria );
      };
      
      
      return filtered_list;
      
    };
    
    
    wait_for( `typeof window.cit_local_list.status_conf !== "undefined"`, function() {
      

      $('#'+rand_id+'_status_tip').data('cit_props', {
        
        desc: `odaberi tip unutar status modula u statusu ${rand_id}`,
        local: true,
        show_cols: ['naziv'],
        return: {},
        show_on_click: true,

        list: window.cit_local_list.status_conf,

        filter: function(status_tipovi) {

          var filtered_list = [];

          var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];

          if ( !data.current_status ) data.current_status = {};
          if ( !data.current_status.status_tip ) data.current_status.status_tip = {};

          // ako nema next statuses
          if ( !data.current_status.status_tip?.next_statuses ) data.current_status.status_tip.next_statuses = [];
          // ako nema grana finish
          if ( !data.current_status.status_tip?.grana_finish ) data.current_status.status_tip.grana_finish = [];

          var curr_status = null;
          var status_sifra = data.current_status.sifra || null;

          var parent_status = null;

          // ako već postoji current status
          if ( status_sifra ) {

            // --------------------------- AKO JE USER OTVORIO NEKI STATUS ---------------------------
            curr_status = cit_deep(data.current_status);


            if ( curr_status.elem_parent ) {
              filtered_list = next_i_finish_statuses_from_parent( data, curr_status, status_tipovi, filtered_list );
            } 
            else {
              filtered_list = next_i_finish_statuses_from_old_status( data, curr_status, status_tipovi, filtered_list )
            }; // ako status nema parent

          } 
          else {

            // --------------------------- AKO JE NOVI STATUS ---------------------------
            $.each(status_tipovi, function(ind, tip) {

              var status_place = check_status_place(status_tipovi, tip);
              var is_next_status = status_place.next;
              var is_finish_status = status_place.finish;
              var has_children = status_place.has_children;

              function filter_dep_statuses(tip) {

                var push_ok = true;

                if ( tip.cat.sifra == `nabava` || tip.cat.sifra == `financije` ) {
                  // ako nije standardni pre-productiuon  tj ako je status samo za odjel nabave ili samo za odjel financija 
                  // status onda NEMOJ UBACITI u listu !!
                  push_ok = false;
                  
                  // IPAK ubaci u listu pod dolje navedenim uvijetima
                  // tj ako je user baš u specifičnom odjelu pa može vidjeti specifične statuse za taj odjel
                  if ( tip.cat.sifra == `nabava` && window.cit_user?.dep?.sifra == "NAB" ) push_ok = true;
                  
                  
                  // TODO
                  // trenutno ne postoji cat statusa financije !!!!!!!!
                  // trenutno ne postoji cat statusa financije !!!!!!!!
                  // trenutno ne postoji cat statusa financije !!!!!!!!
                  // iskreno ne znam zašto sam to napravio :( -----> ovaj uvije nikada neće biti true jer nema te kategorije !!!!!! ?????
                  
                  if ( tip.cat.sifra == `financije` && window.cit_user?.dep?.sifra == "FIN" ) push_ok = true;
                };

                return push_ok;

              };

              // ubaci samo statuse koji imaju djecu
              // ali nisu sami djeca i nisu krajnji statusi
              if ( 
                has_children            && 
                !is_next_status         &&
                !is_finish_status       &&

                filter_dep_statuses(tip) 
              ) {
                filtered_list.push(tip);
              };

            });


            // ------------------------------------ AKO JE OVO ZA STATUSE UNUTAR MODULA SIROVINA ILI MODULA PARTNER ------------------------------------
            // onda samo prikazuj specijalne statuse za sirovinu !!!!

            
            
            if ( data.for_module == `sirov` || data.for_module == `partner` ) {
              
              filtered_list = [];
              
              $.each(status_tipovi, function(ind, tip) {
                
                var status_place = check_status_place(status_tipovi, tip);
                var is_next_status = status_place.next;
                var is_finish_status = status_place.finish;
                var has_children = status_place.has_children;

                
                if ( 
                  
                  has_children            && 
                  !is_next_status         &&
                  !is_finish_status       &&
                  tip.cat.sifra == `nabava`
                   
                   ) {
                  filtered_list.push(tip);
                };
              });
              
              
              
            }; // kraj ako je na page-u sirovina ili partnera
            
            

            
            // ------------------------------------ AKO JE OVO ZA STATUSE UNUTAR MODULA PRODUCTION ------------------------------------
            // onda samo prikazuj specijalne statuse za proizvodnju !!!!

            if ( data.for_module == `production` ) {
              
              filtered_list = [];
              
              $.each(status_tipovi, function(ind, tip) {
                
                if ( 
                  has_children            && 
                  !is_next_status         &&
                  !is_finish_status       &&
                  tip.cat.sifra == `prod` 
                ) {
                  filtered_list.push(tip);
                };
                
              });
              
            };


            if (filtered_list.length > 0 ) {
              var criteria = [ 'status_num' ];
              multisort( filtered_list, criteria );
            };

          }; // kraj od else ( ako trenutno nema current status tj ako user treba izabrati posve novi status )


          return filtered_list;
        },

        cit_run: function(state) {


          console.log(` ------------------------ ovo je odabrani status tip ------------------------ `);
          console.log(state);

          var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          if ( !data.current_status ) data.current_status = {};

          if ( state == null ) {

            $('#'+current_input_id).val(``);

            // RESETIRAJ SAMO AKO OVO NIJE REPLY !!!
            // RESETIRAJ SAMO AKO OVO NIJE REPLY !!!
            if ( !data.current_status.reply_for ) data.current_status.status_tip = null;

          } else {

            $('#'+current_input_id).val(state.naziv);


            data.current_status.status_tip = state;
            
            console.log( `state.sifra --------------------> ` + state.sifra );

            // ovo radi samo ako se NE nalazimo na production stranici
            // ovo radi samo ako se NE nalazimo na production stranici
            if ( window.location.hash.indexOf(`#production`) == -1 ) {

              if ( data.current_status.status_tip.is_work ) {
                $('#' + data.rand_id + 'add_status_work_box').css(`display`, `flex`);
                // $('#'+rand_id+'_est_deadline_hours').prop( "disabled", false );
              } else {
                $('#' + data.rand_id + 'add_status_work_box').css(`display`, `none`);
                // $('#'+rand_id+'_est_deadline_hours').prop( "disabled", true );
              };


            }; // kraj da nije production


            if ( data.for_module == `sirov` ) this_module.auto_select_sirov(data);  

            this_module.show_choose_sirov_box(data);
            
            this_module.show_worker_error_box(data);

          };

        },

      });    /// kraj cit props za status tip

      
    }, 5*1000 );  
    
    $(`#`+rand_id+`_status_from`).data('cit_props', {
      
      desc: `izaberi tko šalje ovaj status - svima osim Radmile i mene je to predefinirano na njihovog usera u statusu ${rand_id}`,
      local: false,
      url: '/search_kal_users',
      find_in: [ 'email', 'full_name', 'name' ],
      return: { _id: 1, user_number: 1, email: 1, full_name: 1, name: 1 },
      col_widths: [ 20, 30, 30, 20 ],
      show_cols: ['user_number', 'email', 'full_name', 'name' ],
      query: {},
      show_on_click: true,
      list_width: 700,
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( !data.current_status ) data.current_status = {};
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.current_status.from = null;
        } else {
          $('#'+current_input_id).val(state.full_name);
          data.current_status.from = state;
        };
        
      },
    });  
    
    
    $('#'+rand_id+'_dep_to').data('cit_props', {
     
      desc: `odaberi ODJEL PRIMATELJA unutar status modula  u statusu ${rand_id}`,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'deps',
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( !data.current_status ) data.current_status = {};
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.current_status.dep_to = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.current_status.dep_to = state;
        };
        
        // update query u props za find primatelja statusa
        $(`#`+rand_id+`_status_to`).data('cit_props').query = state?.sifra ? { "dep.sifra" : state.sifra } : {};
        
      },
      
    });    
    
    
    $(`#`+rand_id+`_status_to`).data('cit_props', {
      desc: `izaberi kojem useru ide status - to bi trebao program sam procjeniti i slati onome s najmanje posla u statusu ${rand_id}`,
      local: false,
      url: '/search_kal_users',
      find_in: [ 'email', 'full_name', 'name' ],
      return: { _id: 1, user_number: 1, email: 1, full_name: 1, name: 1 },
      col_widths: [ 20, 30, 30, 20 ],
      show_cols: ['user_number', 'email', 'full_name', 'name' ],
      query: {},
      show_on_click: true,
      list_width: 700,
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( !data.current_status ) data.current_status = {};
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.current_status.to = null;
        } else {
          $('#'+current_input_id).val(state.full_name);
          data.current_status.to = state;
        };
        
      },
    });  

     
    $(`#`+rand_id+`_worker`).data('cit_props', {
      desc: `izaberi radnika koji odrađuje proizvodni status ${rand_id}`,
      local: false,
      url: '/search_kal_users',
      find_in: [ 'email', 'full_name', 'name' ],
      return: { _id: 1, user_number: 1, email: 1, full_name: 1, name: 1 },
      col_widths: [ 20, 30, 30, 20 ],
      show_cols: ['user_number', 'email', 'full_name', 'name' ],
      query: {},
      show_on_click: true,
      list_width: 700,
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.current_worker = state;
        } else {
          $('#'+current_input_id).val(state.full_name);
          data.current_worker = state;
        };
        
      },
    });  
    
    
    $('#'+rand_id+'_est_deadline_hours').off(`blur.status_est_deadline`);
    $('#'+rand_id+'_est_deadline_hours').on(`blur.status_est_deadline`, function() {
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      
      var new_value = set_input_data(
        this, // input elem
        data, // data parent
        null, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
        "dont_update" // ovako određujem  jel treba napraviti update dataseta ili samo vratiti value
      );
      
      if ( !data.current_status ) data.current_status = {};
      data.current_status.est_deadline_hours = new_value;
      
      
    });
    
    
    $('#'+rand_id+'_est_deadline_date').data(`cit_run`, function(state, this_input) {
      
      
      var this_comp_id = this_input.closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_comp = this_input.closest('.product_comp');
      
      if ( window.location.hash.indexOf(`#production`) > -1 ) {
        // ako se nalazi na production page onda product component nije parent nego je zapravo sibiling
        // I JEDAN JEDINI PRODUCT JE NA TOJ STRANICI ZA PRODUCTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        product_comp = $(`body .product_comp`);
      };
      
      var product_comp_id  = null
      var product_data = null;
      
      if ( product_comp.length > 0 ) {
        product_comp_id = product_comp[0].id;
        product_data = this_module.product_module.cit_data[product_comp_id];
      };
      
      if ( !data.current_status ) data.current_status = {};

      var this_comp_id = this_input.closest('.cit_comp')[0].id;

      if ( state == null || this_input.val() == ``) {
        this_input.val(``);
        data.current_status.est_deadline_date = null;
        console.log(`current_status est_deadline_date: `, null );
        return;
      };
      
      // ako je STATUS PRED - PROIZVODNJE
      if ( 
        data.current_status.status_tip?.cat?.sifra == `preprod` // provjerava rok za def samo ako je ovo status za PRED-PROIZVODNJU
        &&
        product_data?.rok_za_def // samo ako postoji rok za def u podacima proizvoda
        &&
        state > product_data.rok_za_def // samo ako je odabrani datum veći od roka za definiciju
      ) {
        popup_warn(`Rok za definiciju proizvoda je:<br><br>${ cit_dt( product_data.rok_za_def).date } !!!<br><br>Ne možeš izabrati datum iza tog datuma :P`);
        this_input.val(``);
        data.current_status.est_deadline_date = null;
        return;
        
      };
      
      
      // ako je STATUS PROIZVODNJE
      if ( 
        data.current_status.status_tip?.cat?.sifra == `prod` // provjerava rok_proiz samo ako je ovo status za PROIZVODNJU
        &&
        product_data?.rok_proiz // samo ako postoji rok za proiz u podacima proizvoda
        &&
        state > product_data.rok_proiz // samo ako je odabrani datum veći od roka za proiz
      ) {
        popup_warn(`Rok za proizvodnju je:<br><br>${ cit_dt( product_data.rok_proiz).date } !!!<br><br>Ne možeš izabrati datum iza tog datuma :P`);
        this_input.val(``);
        data.current_status.est_deadline_date = null;
        return;
        
      };
      
      
      // ako status ima PARENT onda EST date ne smije biti veći od tog datuma !!!!!!
      // ako status ima PARENT onda EST date ne smije biti veći od tog datuma !!!!!!
      // ako status ima PARENT onda EST date ne smije biti veći od tog datuma !!!!!!
      
      var parent_est_date = null;
      if ( data.current_status.elem_parent_object.est_deadline_date ) {
        parent_est_date = data.current_status.elem_parent_object.est_deadline_date
      };
      
      if ( 
        parent_est_date
        &&
        data.current_status.elem_parent // ako ovaj status ima elem parent !!
        &&
        state > parent_est_date // samo ako je odabrani datum veći od datuma parent statusa !!!!
      ) {
        popup_warn(`Rok početnog statusa od ove grane je je:<br><br>${ cit_dt( parent_est_date ).date } !!!<br><br>Ne možeš izabrati datum iza tog datuma :P`);
        this_input.val(``);
        data.current_status.est_deadline_date = null;
        return;
        
      };
      
      
      if ( state < Date.now() ) {
        popup_warn(`Izaberite Datum i Vrijeme u budućnosti !!!`);
        this_input.val(``);
        data.current_status.est_deadline_date = null;
        return;
      };
      
      data.current_status.est_deadline_date = state;
      console.log(`current_status est_deadline_date: `, new Date( state ) );
      
      /*
      
      ------------------------------------------------------------------------------------------
      SADA VIŠE NE POVEZUJEM PROCJENJEN DATUM KRAJA POSLA I PROCJENU KOLIKO SATI TREBA RADITI JER TO NEMA VEZE  
      datum roka može biti za 10 dana, a može raditi samo 1 sat vremena 
      ------------------------------------------------------------------------------------------
      
      var est_hours = work_minutes_forward( Date.now(), state, { in: 6, out: 21, sub: false, ned: false } );
      $('#'+rand_id+`_est_deadline_hours`).val( cit_format(est_hours, 2) );
      data.current_status.est_deadline_hours = est_hours;
      
      ------------------------------------------------------------------------------------------
      */
      
      
    });
    
    
    $('#'+rand_id+'_add_work_from_time').data(`cit_run`, function(state, this_input) {
      
      
      var this_comp_id = this_input.closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      if ( !data.current_work ) data.current_work = {};

      
      if ( state == null || this_input.val() == ``) {
        this_input.val(``);
        data.work_from_time = null;
        console.log(`current_status work_from_time: `, null );
        return;
      };

      data.work_from_time = state;
      
      var curr_work_duration = 0;
      if ( data.work_from_time && data.work_to_time ) {
        
        
        if ( data.work_to_time < data.work_from_time ) {
          
          popup_warn(`Kraj rada ne može biti prije početka rada :)`);
          
          $('#'+rand_id+'_add_work_from_time').val(``);
          $('#'+rand_id+'_add_work_to_time').val(``);
          
          data.work_from_time = null;
          data.work_to_time = null;
          
          return;
          
        };
        
        curr_work_duration = (data.work_to_time - data.work_from_time) / (1000*60);
        $('#'+rand_id+'_add_work_time').val( cit_round( curr_work_duration, 2 ) );
      };
      
      
      console.log(`current_status work_from_time: `, new Date(state));
      
    });
    
    $('#'+rand_id+'_add_work_to_time').data(`cit_run`, function(state, this_input) {
      
      
      var this_comp_id = this_input.closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      

      if ( !data.current_work ) data.current_work = {};

      
      if ( state == null || this_input.val() == ``) {
        this_input.val(``);
        data.work_to_time = null;
        console.log(`current_status work_to_time: `, null );
        return;
      };

      data.work_to_time = state;
      
      var curr_work_duration = 0;
      
      if ( data.work_from_time && data.work_to_time ) {
        
        if ( data.work_to_time < data.work_from_time ) {
          
          popup_warn(`Kraj rada ne može biti prije početka rada :)`);
          
          $('#'+rand_id+'_add_work_from_time').val(``);
          $('#'+rand_id+'_add_work_to_time').val(``);
          
          data.work_from_time = null;
          data.work_to_time = null;
          
          return;
          
        };
        
        
        
        curr_work_duration = (data.work_to_time - data.work_from_time) / (1000*60);
        $('#'+rand_id+'_add_work_time').val( cit_round( curr_work_duration, 2 ) );
      };
      
      
      console.log(`current_status work_to_time: `, new Date(state));
      
    });

    
    $('#'+rand_id+'_tezina').off(`blur.tezina`);
    $('#'+rand_id+'_tezina').on(`blur.tezina`, function() {
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var new_value = set_input_data(
        this, // input elem
        data, // data parent
        null, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
        "dont_update" // određujem ovako jel treba napraviti update dataseta ili samo vratiti value
      );
      
      if ( !data.current_status ) data.current_status = {};
      data.current_status.tezina = new_value;
      
      console.log( data.current_status );
      
    });
    
    
    $('#'+rand_id+'_order_sirov_count').off(`blur.order_sirov_count`);
    $('#'+rand_id+'_order_sirov_count').on(`blur.order_sirov_count`, function() {
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var new_value = set_input_data(
        this, // input elem
        data.current_status, // data parent
        null, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
        "dont_update" // određujem ovako jel treba napraviti update dataseta ili samo vratiti value
      );
      
      if ( !data.current_status ) data.current_status = {};
      data.current_status.order_sirov_count = new_value;
      
      console.log( data.current_status );
      
    });
    
    
    $('#'+rand_id+'_worker_error_cost').off(`blur.worker_error_cost`);
    $('#'+rand_id+'_worker_error_cost').on(`blur.worker_error_cost`, function() {
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var new_value = set_input_data(
        this, // input elem
        data.current_status, // data parent
        null, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
        "dont_update" // određujem ovako jel treba napraviti update data seta ili samo vratiti value
      );
      
      if ( !data.current_status ) data.current_status = {};
      data.current_status.worker_error_cost = new_value;
      
      console.log( data.current_status );
      
    });
    
    
    $(`#`+rand_id+`_worker_error_users`).off(`click`);
    $(`#`+rand_id+`_worker_error_users`).on(`click`, function() {
      $(this).data('cit_props').list = data.statuses; // refresh listu
    });
    
    $(`#`+rand_id+`_worker_error_users`).data('cit_props', {
      
      desc: `izaberi radnika koji su odgovorni za grešku ${rand_id}`,
      local: true,
      show_cols: ['full_name'],
      return: {},
      show_on_click: true,
      list: function() { 
        
        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        return data.statuses 
      
      },
      filter: function(items) {

        var all_users_in_product = [];
        
        $.each( items, function(s_ind, status) {
          
          all_users_in_product = upsert_item(all_users_in_product, status.from, `_id`);
          all_users_in_product = upsert_item(all_users_in_product, status.to, `_id`);
          
          $.each( status.work_times, function( w_ind, work) {
            all_users_in_product = upsert_item( all_users_in_product, work.worker, `_id`);
          });
          
        });
        
        return all_users_in_product;
        
      },
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
        } else {
          
          if ( !data.current_status ) data.current_status = {};
          if ( !data.current_status.worker_error_users ) data.current_status.worker_error_users = [];
          data.current_status.worker_error_users = upsert_item( data.current_status.worker_error_users, state, `_id` );
          
          this_module.make_worker_error_users_list(data);
          
        };
        
      },
      
    });  
    
    
    $('#'+rand_id+'_worker_error_possible_solution').off(`blur.worker_error_possible_solution`);
    $('#'+rand_id+'_worker_error_possible_solution').on(`blur.worker_error_possible_solution`, function() {
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var new_value = set_input_data(
        this, // input elem
        data.current_status, // data parent
        null, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
        "dont_update" // određujem ovako jel treba napraviti update data seta ili samo vratiti value
      );
      
      if ( !data.current_status ) data.current_status = {};
      data.current_status.worker_error_possible_solution = new_value;
      
      console.log( data.current_status );
      
    });
    
    
    
    $('#'+rand_id+'_worker_error_solution').off(`blur.worker_error_solution`);
    $('#'+rand_id+'_worker_error_solution').on(`blur.worker_error_solution`, function() {
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var new_value = set_input_data(
        this, // input elem
        data.current_status, // data parent
        null, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
        "dont_update" // određujem ovako jel treba napraviti update data seta ili samo vratiti value
      );
      
      if ( !data.current_status ) data.current_status = {};
      data.current_status.worker_error_solution = new_value;
      
      console.log( data.current_status );
      
    });
    
    
    
    
    

    $(`#${rand_id}_add_work_time`).off('blur');
    $(`#${rand_id}_add_work_time`).on('blur', function() {
      
      var new_value = set_input_data(
        this, // input elem
        data, // data parent
        null, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
        "dont_update" // određujem ovako jel treba napraviti update dataseta ili samo vratiti value
      );
     
    });
    
    
    $('#'+rand_id+'_work_cat').data('cit_props', {
      desc: `odaberi katergoriju posla u statusu ${rand_id}`,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'work_cat',
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( !data.current_work ) data.current_work = {};
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.current_work.work_cat = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.current_work.work_cat = state;
        };
        
      },
      
    });    
    
    
        
    $('#'+rand_id+'_add_work_btn').off(`click`);
    $('#'+rand_id+'_add_work_btn').on(`click`, this_module.save_work );
    
    
    $('#'+rand_id+'_status_docs').data(`this_module`, this_module );
    $('#'+rand_id+'_status_docs').data(`cit_run`, this_module.update_status_docs );
    
    
    this_module.make_status_list(data);
    this_module.remove_header_arrows_and_blink(data);
    
    
    $(`#${rand_id}_quit_status`).off('click');
    $(`#${rand_id}_quit_status`).on('click', this_module.reset_status_GUI );
    

    $(`#${rand_id}_save_status`).off('click');
    $(`#${rand_id}_save_status`).on('click', this_module.save_current_status );

    $(`#${rand_id}_update_status`).off('click');
    $(`#${rand_id}_update_status`).on('click', this_module.save_current_status );

   
    $(`#${rand_id}_reply_status`).off('click');
    $(`#${rand_id}_reply_status`).on('click', this_module.save_current_status );

    
    $('#'+rand_id+'_rn_files').data('cit_props', {

      desc: `odaberi filove za radni proces ${rand_id}`,
      local: true,
      show_cols: [ 
        
        "tip_naziv",
        "time",
        
        "template",
        "graf_sifra",
        
        "doc",
        "graf_doc",
      
      ],
      custom_headers: [
        
        "TIP NACRTA", // "tip_naziv",
        "TIME", // "time",
        
        "ŠIFRA NACRTA" , // "template",
        "ŠIFRA GP", // "graf_sifra",
        
        
        "NACRT DOC.", // "docs",
        "GP DOC.", // "graf_docs",
        
      ],
      col_widths: [
        
        1, // "tip_naziv",
        1, // "time",
        
        1, // "template",
        1, // "graf_sifra",
        
        5, // "doc",
        5, // "graf_doc",
        
      ],
      
      format_cols: { 
        
        time: "date_time",
        template: "center",
        graf_sifra: "center",
        
      },
      
      return: {},
      show_on_click: true,

      list: function() {
        
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        
        var product_comp = $('#'+current_input_id).closest('.product_comp');

        if ( window.location.hash.indexOf(`#production`) > -1 ) {
          // ako se nalazi na production page onda product component nije parent nego je zapravo sibiling
          // I JEDAN JEDINI PRODUCT JE NA TOJ STRANICI ZA PRODUCTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          product_comp = $(`body .product_comp`);
        };

        var product_comp_id  = null
        var product_data = null;

        if ( product_comp.length > 0 ) {
          
          product_comp_id = product_comp[0].id;
          product_data = this_module.product_module.cit_data[product_comp_id];
          
          return product_data.nacrt_specs;
          
        } else {
          
          return [];
          
        };
        
        
      },

      filter: function(items) {

        var filtered_list = [];

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        
        $.each( items, function( n_ind, spec_obj ) {
 
        // ------------------------------ NACTI DOCS ------------------------------
        var all_docs = "";
        
        if ( spec_obj.docs && $.isArray(spec_obj.docs) ) {
          var criteria = [ '!time' ];
          multisort( spec_obj.docs, criteria );

          $.each(spec_obj.docs, function(doc_index, doc) {
            var doc_html = 
            `
            <div class="docs_row">
              <div class="docs_date">${ cit_dt(doc.time).date_time }</div>
              <div class="docs_link">${ doc.link }</div>
            </div>
            `;

            filtered_list.push( { 
              
              sifra: doc.sifra,
              
              tip_naziv: spec_obj.tip?.naziv || "",
              time: spec_obj.time,
              doc_time: doc.time,
              
              template: spec_obj.template,
              graf_sifra: spec_obj.graf_sifra || "",
              
              doc: doc_html,
              graf_doc: "",
              
            });

          }); // kraj loopa po docs

        }; // kraj ako ima docs
        
        // ------------------------------ NACTI DOCS ------------------------------
        
        
        
        
        // ------------------------------ GRAF DOCS ------------------------------
        var graf_docs = "";
        
        if ( spec_obj.graf_docs && $.isArray(spec_obj.graf_docs) ) {
          
          var criteria = [ '!time' ];
          multisort( spec_obj.graf_docs, criteria );

          $.each(spec_obj.graf_docs, function(doc_index, doc) {
            
            var graf_doc_html = 
            `
            <div class="docs_row">
              <div class="docs_date">${ cit_dt(doc.time).date_time }</div>
              <div class="docs_link">${ doc.link }</div>
            </div>
            `;

            filtered_list.push( { 
              
              sifra: doc.sifra,
              
              tip_naziv: spec_obj.tip?.naziv || "",
              time: spec_obj.time,
              doc_time: doc.time,
              
              template: spec_obj.template,
              graf_sifra: spec_obj.graf_sifra || "",
              
              doc: "",
              graf_doc: graf_doc_html,
              
            });

          }); // kraj loopa po graf docs

        }; // kraj ako ima graf docs
        
        // ------------------------------ GRAF DOCS ------------------------------
        
        
          
        }); // kraj loopa po nacrt specs

        
        var criteria = [ '!time', 'template', '!doc_time' ];
        
        multisort( filtered_list, criteria );
        

        return filtered_list;
      },

      cit_run: function(state) {


        console.log(` ------------------------ ovo je odabrani RN file za status ------------------------ `);
        console.log(state);

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( !data.current_status ) data.current_status = {};

        if ( !data.current_status.rn_files ) data.current_status.rn_files = [];
        
        data.current_status.rn_files = upsert_item( data.current_status.rn_files, state, `sifra` );
       
        console.log(data.current_status);
        
        this_module.make_rn_files_list(data);
        
        
        
      },

    });    /// kraj cit props za status tip
    

    
    this_module.sirov_valid = this_module.sirov_module.valid;
    $(`#` + rand_id + `_rn_alat`).data('cit_props', {
      // !!!findsirov!!! ----> isto kao _elem_choose_alat iznad osim što ima query da traži samo alate
      
      
      show_on_click: true,
      
      desc: 'pretraga za odabir _rn_alat unutar pojedinog elementa ' + rand_id ,
      local: false,
      url: '/find_sirov',
      find_in: [
        "sirovina_sifra", 
        "naziv",
        "full_naziv",
        "stari_naziv",
        "povezani_nazivi",
        "detaljan_opis",
        "grupa_flat",  

        "kvaliteta_1",
        "kvaliteta_2", 
        "kvaliteta_mix",

        "kontra_tok_x_tok",
        "val_x_kontraval",
        "alat_prirez",

        "dobavljac_flat", 
        "dobav_naziv", 
        "dobav_sifra", 
        "fsc_flat", 
        "povezani_kupci_flat", 
        "boja",   
        "zemlja_pod_flat",

        "specs_flat",

      ],
      query: function() { 
        
        
        var req = {};
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        var product_comp = $('#'+current_input_id).closest('.product_comp');

        if ( window.location.hash.indexOf(`#production`) > -1 ) {
          // ako se nalazi na production page onda product component nije parent nego je zapravo sibiling
          // I JEDAN JEDINI PRODUCT JE NA TOJ STRANICI ZA PRODUCTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          product_comp = $(`body .product_comp`);
        };

        var product_comp_id  = null
        var product_data = null;

        if ( product_comp.length > 0 ) {
          product_comp_id = product_comp[0].id;
          product_data = this_module.product_module.cit_data[product_comp_id];
        };

        
        var alat_ids = [];
        // ako product IMA elemente
        if ( product_data.elements?.length > 0 ) {
          $.each( product_data.elements, function(e_ind, element) {
            if ( element.elem_alat?.length > 0 ) {
              $.each( element.elem_alat, function(ala_ind, alat) {
                alat_ids.push(alat._id);
              });
            };
          }); // loop po elementima
        }; // ako ima elemente
        
        
        if ( alat_ids.length == 0 ) {
          
          popup_warn(`Nedostaju podaci alata za ovaj proizvod<br>Potrebno je dodati alate na odgovarajuće elemente unuter proizvoda !!!!`);
          req = `abort_req`; // ovo mi služi da abortam sve u search_database unutar global_func.js
          
        } else {
          
          req = { _id: {$in : alat_ids },  "klasa_sirovine.sifra": 'KS5' };
          
        };
        
        return req;
        

      }, // traži samo alate koji su definirani u elementima !!!!!!!  
      
      return: {},

      /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */

      col_widths: [
        1, // "sirovina_sifra",
        2, // "full_naziv",
        2, // "full_dobavljac",
        1, // grupa_naziv
        1, // "boja",
        1, // "cijena",
        1, // "kontra_tok",
        1, // "tok",
        1, // "po_valu",
        1, // "po_kontravalu",
        2, // "povezani_nazivi",

        1, // "pk_kolicina",
        1, // "nk_kolicina",
        1, // "order_kolicina",
        1, // "sklad_kolicina"
        1, // last_status

        4, // spec_docs

      ],
      show_cols: [
        "sifra_link",
        "full_naziv",
        "full_dobavljac",
        "grupa_naziv",
        "boja",
        "cijena",
        "kontra_tok",
        "tok",
        "po_valu",
        "po_kontravalu",
        "povezani_nazivi",

        `pk_kolicina`,
        `nk_kolicina`,
        `order_kolicina`,
        `sklad_kolicina`,
        `last_status`,

        `spec_docs`, 

      ],
      custom_headers: [
        `ŠIFRA`,  // "sirovina_sifra",
        `NAZIV`,  // "full_naziv",
        `DOBAV.`,  // "full_dobavljac",
        `GRUPA`,  // "grupa_naziv",
        `BOJA`,  // "boja",
        `CIJENA`,  // "cijena",
        `KONTRA TOK`,  // "kontra_tok",
        `TOK`,  // "tok",
        `VAL`,  // "po_valu",
        `KONTRA VAL`,  // "po_kontravalu",
        `POVEZANO`,  // "povezani_nazivi",

        `PK RESERV`,  // "pk_kolicina",
        `NK RESERV`,  // "nk_kolicina",
        `NARUČENO`,  // "order_kolicina",
        `SKLADIŠTE`,  // "sklad_kolicina",
        `STATUS`,

        `DOCS`, 

      ],

      format_cols: {

        "sifra_link": "center",
        "grupa_naziv": "center",
        "boja": "center",
        "cijena": this_module.sirov_valid.cijena.decimals,
        "kontra_tok": this_module.sirov_valid.kontra_tok.decimals,
        "tok": this_module.sirov_valid.tok.decimals,
        "po_valu": this_module.sirov_valid.po_valu.decimals,
        "po_kontravalu": this_module.sirov_valid.po_kontravalu.decimals,

        "pk_kolicina": this_module.sirov_valid.pk_kolicina.decimals,
        "nk_kolicina": this_module.sirov_valid.nk_kolicina.decimals,
        "order_kolicina": this_module.sirov_valid.order_kolicina.decimals,
        "sklad_kolicina": this_module.sirov_valid.sklad_kolicina.decimals,

      },

      filter: this_module.find_sirov.find_sirov_filter,

      cit_run: async function(state) {

        console.log(` ------------------------ ovo je odabrani RN ALAT za RADNI NALOG status ------------------------ `);
        console.log(state);

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( !data.current_status ) data.current_status = {};
        
        if ( state == null ) {
          data.current_status.rn_alat = null;
           $('#'+current_input_id).val(``);
          return;
        };
        
        
        $('#'+current_input_id).val( state.sirovina_sifra + `--` + state.full_naziv );
        
        data.current_status.rn_alat = {
          
          _id: state._id || null,
          
          time: Date.now(),
          
          user: {
            _id: window.cit_user._id,
            name: window.cit_user.name,
            user_number: window.cit_user.user_number,
            full_name: window.cit_user.full_name,
            email: window.cit_user.email,
          },
          
          sirovina_sifra: state.sirovina_sifra || null,
          full_naziv: state.full_naziv || null,
          val_x_kontraval: state.val_x_kontraval || null,
          po_valu: state.po_valu || null,
          po_kontravalu: state.po_kontravalu || null,
          specs: state.specs || null,
          
        };
        
        console.log(data.current_status);


      },
      
    });
    
    
    
    
  }, 50*1000 );

  return {
    html: component_html,
    id: data.id
  };

},
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};

  
  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
  };
  this_module.set_init_data = set_init_data;
  
  
  
  this_module.valid = {
    
    velprom_smjena: { element: "single_select", type: "simple", lock: false, visible: true, label: "SMJENA" },
    
    status_tip: { element: "single_select", type: "simple", lock: false, visible: true, label: "Novi status" },
    
    tezina: { element: "input", "type": "number", decimals: 2, lock: (window.cit_admin ? false : true), visible: true, label: "Težina zadatka" },
    
    
    work_time_sum: { element: "input", "type": "number", decimals: 2, lock: true, visible: true, label: "Ukupno rad / h" },
    add_work_time: { element: "input", "type": "number", decimals: 2, lock: false, visible: true, label: "Trajanje / minuta" },
    
    add_work_from_time: {  element: "simple_calendar", type: "date_time", lock: false, visible: true, "label": "Početak rada" }, 
    add_work_to_time: {  element: "simple_calendar", type: "date_time", lock: false, visible: true, "label": "Kraj rada" }, 
    
    work_cat: { element: "single_select", type: "simple", lock: false, visible: true, label: "Kategorija" },
    work_desc: { element: "text_area", type: "string", lock: false, visible: true, label: "Komentar posla" },
    
    worker: { element: "single_select", type: "simple", lock: false, visible: true, label: "Izaberi radnika" },
    
    worker_error_cost: { element: "input", "type": "number", decimals: 2, lock: false, visible: true, label: "Trošak greške" },
    worker_error_possible_solution: { element: "text_area", type: "string", lock: false, visible: true, label: "Moguće rješenje" },
    worker_error_solution: { element: "text_area", type: "string", lock: false, visible: true, label: "Kako je rješeno" },
    
    worker_error_users: { element: "multi_select", type: "simple", lock: false, visible: true, label: "Odgovorni radnici" },
    
    order_sirov_count: { element: "input", "type": "number", decimals: 2, lock: false, visible: true, label: "Količina" },
    
    ask_kolicina_1: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label:"Količina 1"},
    ask_kolicina_2: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label:"Količina 2"},
    ask_kolicina_3: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label:"Količina 3"},
    
    lang_order_dobav: { element:"single_select", type:"simple", lock:false, visible:true, label:"Jezik dokumenta" },
    
    doc_valuta: { element:"single_select", type:"simple", lock:false, visible:true, label:"Valuta" },
    doc_valuta_manual: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label:"Upis Valute"},
    
    doc_adresa: { element:"single_select", type:"same_width", lock:false, visible:true, label:"Adresa na dokumentu" },  
    

    status_from: { element: "single_select", type: "simple", lock: false, visible: true, label: "Od koga" },
    
    dep_to: { element: "single_select", type: "simple", lock: false, visible: true, label: "Prema kojem odjelu" },
    status_to: { element: "single_select", type: "simple", lock: false, visible: true, label: "Prema kome je status"},
    
    status_komentar: { element: "text_area", type: "string", lock: false, visible: true, label: "Komentar statusa" },
    
    status_docs: { "element": "upload", "type": "multiple", "lock": false, "visible": true, "label": "Dodaj Dokumente" },
    rn_files: { element: "single_select", type: "", lock: false, visible: true, label: "Spoji dokumente s RADNIM PROCESOM" },
    rn_alat: { element: "single_select", type: "", lock: false, visible: true, label: "Spoji ALAT S RADNIM PROCESOM" },
    
    
    est_deadline_hours: { "element": "input", "type": "number", decimals: 2,  "lock": false, "visible": true, "label": "Procjena sati" },
    est_deadline_date: {  element: "simple_calendar", type: "date_time", lock: false, visible: true, "label": "Procjena datum" }, 
    
    
    // ispod su production input elementi
    
    prodon_est_start: {  element: "simple_calendar", type: "date_time", lock: false, visible: true, "label": "Procjena START proiz." }, 
    prodon_est_end: {  element: "simple_calendar", type: "date_time", lock: false, visible: true, "label": "Procjena END proiz." }, 
    
    prodon_real_start: {  element: "simple_calendar", type: "date_time", lock: false, visible: true, "label": "Realni START proiz." }, 
    prodon_real_end: {  element: "simple_calendar", type: "date_time", lock: false, visible: true, "label": "Realni END proiz." }, 
    
    
    prodon_select_ulaz: { element: "single_select", type: "", lock: false, visible: true, label: "ULAZ"},
    prodon_utrosak_ukupno: { "element": "input", "type": "number", decimals: 2,  "lock": false, "visible": true, "label": "UROŠAK UKUPNO" },
    prodon_utrosak_dodaj: { "element": "input", "type": "number", decimals: 2,  "lock": false, "visible": true, "label": "DODAJ UTROŠAK" },
    
    prodon_skart_ulaz_ukupno:  { "element": "input", "type": "number", decimals: 2,  "lock": false, "visible": true, "label": "ŠKART ULAZA UKUPNO" },
    prodon_skart_ulaz_dodaj:  { "element": "input", "type": "number", decimals: 2,  "lock": false, "visible": true, "label": "DODAJ ŠAKRT ULAZA" },
    
    prodon_select_ulaz_primka: { element: "single_select", type: "simple", lock: false, visible: true, label: "PO KOJOJ PRIMKI?"},
    
    prodon_ulaz_palet_sifra: { element:"input", type:"string", lock:false, visible:true, label:"ŠIFRA PALETE/PAKIRANJA?", required: false},
    
    prodon_ulaz_palet_doc: { element:"input", type:"string", lock:false, visible:true, label:"DOKUMENT PALETE/PAKIRANJA?", required: false},
    
    
    // prodon_ulaz_palet_sifra: { element: "single_select", type: "same_width", lock: false, visible: true, label: "ŠIFRA PALETE/PAKIRANJA?"},
    
    prodon_ulaz_palet_count: { "element": "input", "type": "number", decimals: 2,  "lock": false, "visible": true, "label": "KOLIKO JE OSTALO NA PALETI" },
    prodon_ulaz_korekcija:  { "element": "input", "type": "number", decimals: 2,  "lock": false, "visible": true, "label": "KOREKCIJA ULAZA" },
    
    
    prodon_select_izlaz: { element: "single_select", type: "", lock: false, visible: true, label: "IZLAZ"},
    prodon_izlaz_ukupno: { "element": "input", "type": "number", decimals: 2,  "lock": false, "visible": true, "label": "IZLAZ UKUPNO" },
    prodon_izlaz_dodaj: { "element": "input", "type": "number", decimals: 2,  "lock": false, "visible": true, "label": "DODAJ NA IZLAZ" },
    
    prodon_skart_izlaz_ukupno: { "element": "input", "type": "number", decimals: 2,  "lock": false, "visible": true, "label": "ŠKART IZLAZA UKUPNO" },
    prodon_skart_izlaz_dodaj: { "element": "input", "type": "number", decimals: 2,  "lock": false, "visible": true, "label": "DODAJ ŠKART IZLAZA" },
        
    
    prodon_ostatak_val: { "element": "input", "type": "number", decimals: 2,  "lock": false, "visible": true, "label": "OSTATAK PO VALU" },
    prodon_ostatak_kontra: { "element": "input", "type": "number", decimals: 2,  "lock": false, "visible": true, "label": "OSTATAK PO KONTRA VALU" },
    
    
    prodon_select_ostatak: { element: "single_select", type: "", lock: false, visible: true, label: "ODABERI OSTATAK"},
    
    prodon_ostatak_ukupno: { "element": "input", "type": "number", decimals: 2,  "lock": false, "visible": true, "label": "OSTATAK UKUPNO" },
    prodon_ostatak_dodaj: { "element": "input", "type": "number", decimals: 2,  "lock": false, "visible": true, "label": "OSTATAK DODAJ" },
    prodon_skart_ostatak_ukupno: { "element": "input", "type": "number", decimals: 2,  "lock": false, "visible": true, "label": "OSTATAK ŠKART UKUPNO" },
    prodon_skart_ostatak_dodaj: { "element": "input", "type": "number", decimals: 2,  "lock": false, "visible": true, "label": "OSTATAK ŠKART DODAJ" },
    
    prodon_work_desc: { element: "text_area", type: "string", lock: false, visible: true, label: "Komentar rada" },
    
    
    prodon_drugi_zad: { element:"check_box", type:"bool", lock:false, visible:true, label:"Drugi zadatak" },
    prodon_problem_nalog: { element:"check_box", type:"bool", lock:false, visible:true, label:"Greška u nalogu" },
    prodon_problem_rad: { element:"check_box", type:"bool", lock:false, visible:true, label:"Problem u radu" },
    prodon_problem_stroj: { element:"check_box", type:"bool", lock:false, visible:true, label:"Problem stroj" },
    prodon_problem_worker: { element:"check_box", type:"bool", lock:false, visible:true, label:"Problem osoblja" },
    prodon_kraj_rad_vrem: { element:"check_box", type:"bool", lock:false, visible:true, label:"Kraj radnog vremena" },
    
    
    prio_input: { "element": "input", "type": "number", decimals: 0,  "lock": false, "visible": true, "label": "Prioritet" },

    
  };

  
  
  
  function update_status_docs(files, button, arg_time) {

    var new_docs_arr = [];
    $.each(files, function(f_index, file) {

      var file_obj = {
        sifra: `doc`+cit_rand(),
        time: arg_time,
        link: `<a href="/docs/${time_path(arg_time)}/${file.filename}" target="_blank" onClick="event.stopPropagation();" >${ file.originalname }</a>`,
      };
      new_docs_arr.push( file_obj );
    });


    var this_comp_id = button.closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    console.log( new_docs_arr );
    // napravi novi objekt ako current ne postoji
    if ( !data.current_status ) data.current_status = {};


    if ( $.isArray(data.current_status.docs) ) {
      // ako postoji docs array onda dodaj nove filove
      data.current_status.docs = [ ...data.current_status.docs, ...new_docs_arr ];
    } else {
      // ako ne postoji onda stavi ove nove filove
      data.current_status.docs = new_docs_arr;
    };    


  };
  this_module.update_status_docs = update_status_docs;

  
  function statuses_filter(items, jQuery) {

    // ------------------------------------------------------------------------------------------
    // SVE FUNC POTREBNE ZA OVAJ FILTER !!!
    // ------------------------------------------------------------------------------------------
    var $ = jQuery;
    function cit_round(broj, brojZnamenki) {

      var broj = parseFloat(broj);

      if ( typeof brojZnamenki == 'undefined' ) {
       brojZnamenki = 0;
      };
      var multiplicator = Math.pow(10, brojZnamenki);
      broj = parseFloat((broj * multiplicator).toFixed(11));
      var test =(Math.round(broj) / multiplicator);
      // + znak ispred je samo js trik da automatski pretvori string u broj
      return +(test.toFixed(brojZnamenki));
    };
    function cit_deep(data) {

      try {
        data = JSON.stringify(data);
        data = JSON.parse(data);
      }
      catch(err) {
        console.error(err);
      }

      return data;
    };
    function sort_parents(elements, parent, direction) {

      var sorted = [];

      function loop_elements(elems, arg_parent) {


        // var elems = cit_deep(elements);
        if (!arg_parent) {

          $.each(elems, function (ind, element) {
            // samo ako nema parenta
            if (!element.elem_parent && !element.sorted ) {

              elems[ind].level = 0;

              elems[ind].sorted = true;
              sorted.push(element);
              var parent = element;
              // provjeri jel ovaj elem parent nekome
              loop_elements(elems, parent);

            } else if ( !element.sorted ) {
              // ----------------------------------------------------------------
              // OVAJ DIO VRIJEDI SAMO ZA SERVER DIO U FIND CONTROLER
              // ...ili ako je za listu na statuses page !!!!!

              // ----------------------------------------------------------------

              if ( 

                typeof window == "undefined" // na serveru !!

                ||

                ( 
                  typeof window !== "undefined" && 
                  ( window.location.hash.indexOf('#statuses_page') > -1 || window.location.hash.indexOf('#sirov/') > -1 || window.location.hash.indexOf('#partner/') > -1 ) 
                )

              ) {

                elems[ind].level = (element.level || 0 ) + 10 ;

                elems[ind].sorted = true;
                sorted.push(element);
                var parent = element;
                loop_elements(elems, parent);

              };

              // --- KRAJ -------------------------------------------------------------
              // OVAJ DIO VRIJEDI SAMO ZA SERVER DIO U FIND CONTROLER
              // --- KRAJ -------------------------------------------------------------


            };

          }); // kraj loopa po svim elementima

        } 
        else {

          $.each(elems, function (ind, element) {
            // provjeri jel ovaj elem djete od arg parenta
            if (element.elem_parent == arg_parent.sifra && !element.sorted ) {

              elems[ind].level = arg_parent.level + 10;

              elems[ind].sorted = true;
              sorted.push(element);
              var parent = element;
              loop_elements(elems, parent);

            };


          });

        };
      }; // kraj loop func

      loop_elements(elements, parent);



      $.each(sorted, function(s_ind, sorted_elem) {
        delete sorted_elem.sorted;
      });

      
      // samo ako nisam explicitno rekao da treba biti prema dolje
      if ( direction !== `down` && sorted.length > 0 ) {
        // I  SAMO AKO VEĆ NIJE SORTIRAN OD NAJNOVIJEG PREMA NAJSTRIJEM !!!
        // I  SAMO AKO VEĆ NIJE SORTIRAN OD NAJNOVIJEG PREMA NAJSTRIJEM !!!
        if ( sorted[0].time < sorted[sorted.length-1].time ) sorted = sorted.reverse();
      };

      return sorted;

    }; // kraj sort parents
    
    function allow_weekend_days(date, config) {

      if ( typeof date == 'number' ) date = new Date(date);

      if ( config.sub == false && config.ned == false ) {
        return ( date.getDay() !== 0 && date.getDay() !== 6 );
      } 
      else if ( config.sub == true && config.ned == false ) {
         return ( date.getDay() !== 0 );
      } 
      else if ( config.sub == false && config.ned == true ) {
         return ( date.getDay() !== 6 );
      } 
      else if ( config.sub == true && config.ned == true ) {
         return true;
      }     

    };
    function work_minutes_forward( start_time, end_time, arg_work_options ) {


      if ( start_time > end_time ) {
        popup_warn(`Početno vrijeme ne smije biti VEĆE od krajnjeg vremena!!!`);
      };


      var radne_min_sum = 0;
      var minute_counter;
      var new_time;

      var work_options = null;

      if ( !arg_work_options ) {

         work_options = {
          in: 6,
          out: 21,
          sub: false,
          ned: false
        };

      } else {

        work_options = arg_work_options;

      }


      for( minute_counter = 0 ; minute_counter <= 1000*60*60*24*90; minute_counter += (1000*60) ) {

        new_time = start_time + minute_counter;
        var curr_date = new Date(new_time);

        var curr_yyyy = curr_date.getFullYear();
        var curr_mnt = curr_date.getMonth();
        var curr_day = curr_date.getDate();

        // var curr_hours   = curr_date.getHours();
        // var curr_minutes = curr_date.getMinutes();
        // var curr_seconds = curr_date.getSeconds();


        var curr_date_zero = new Date( curr_yyyy, curr_mnt, curr_day, 0 );
        curr_date_zero = Number(curr_date_zero);

        var start = curr_date_zero + ( 1000*60*60 *  work_options.in ) ; // brojim da se počine raditi u 8h
        var end = curr_date_zero + ( 1000*60*60 *  work_options.out ) ; // brojim da se počine raditi u 18h

        if ( 
          minute_counter
          &&
          allow_weekend_days(new_time, work_options)
          &&
          new_time >= start && new_time <= end
          // curr_date.getHours() >= 8 && curr_date.getHours() <= 18 // radni dan između 8 - 18
        ) {

          radne_min_sum += 1; 
          // pošto idem UNAPRIJED vrijeme ne smije biti VEĆE od krajnog vremena
          if ( new_time >= end_time ) { break; };

        };

      }; // kraj for loopa

      return radne_min_sum;

    };
    function work_minutes_reverse( start_time, end_time, arg_work_options ) {

      if ( start_time < end_time ) {
        popup_warn(`Početno vrijeme ne smije biti MANJE od krajnjeg vremena jer work_minutes_reverse funkcija broji vrijeme unazad !!!`);
      };

      var radne_min_sum = 0;
      var minute_counter;
      var new_time;

      var work_options = null;

      if ( !arg_work_options ) {

         work_options = {
          in: 6,
          out: 21,
          sub: false,
          ned: false
        };

      } else {

        work_options = arg_work_options;
      };

      for( minute_counter = 0 ; minute_counter <= 1000*60*60*24*90; minute_counter += (1000*60) ) {

        new_time = start_time - minute_counter;
        var curr_date = new Date(new_time);

        var curr_yyyy = curr_date.getFullYear();
        var curr_mnt = curr_date.getMonth();
        var curr_day = curr_date.getDate();

        // var curr_hours   = curr_date.getHours();
        // var curr_minutes = curr_date.getMinutes();
        // var curr_seconds = curr_date.getSeconds();


        var curr_date_zero = new Date( curr_yyyy, curr_mnt, curr_day, 0 );
        curr_date_zero = Number(curr_date_zero);

        var start = curr_date_zero + ( 1000*60*60 *  work_options.in ) ; // brojim da se počine raditi u 8h
        var end = curr_date_zero + ( 1000*60*60 *  work_options.out ) ; // brojim da se počine raditi u 18h

        if ( 
          minute_counter
          &&
          allow_weekend_days(new_time, work_options)
          &&
          new_time >= start && new_time <= end
          // curr_date.getHours() >= 8 && curr_date.getHours() <= 18 // radni dan između 8 - 18
        ) {

          radne_min_sum += 1; 
          // pošto idem unazad vrijeme ne smije biti manje od krajnog vremena
          if ( new_time <= end_time ) { break; };

        };

      };

      return radne_min_sum;

    };
    function cit_dt(number, date_format) {

      var date = null;

      // ako nije undefined ili null onda napravi datum od broja 
      if ( number || number == 0 ) {
      date = new Date( number );  
      } else {
      date = new Date();  
      };

      var sek = date.getSeconds();
      var min = date.getMinutes();
      var hr = date.getHours();
      var dd = date.getDate();
      var mm = date.getMonth() + 1; // January is 0!
      var yyyy = date.getFullYear();

      // ako je samo jedna znamenka
      if (dd < 10) {
      dd = '0' + dd;
      };
      // ako je samo jedna znamenka
      if (mm < 10) {
      mm = '0' + mm;
      };

      // ako je samo jedna znamenka
      if (sek < 10) {
      sek = '0' + sek;
      };


      // ako je samo jedna znamenka
      if (min < 10) {
      min = '0' + min;
      };

      // ako je samo jedna znamenka
      if (hr < 10) {
      hr = '0' + hr;
      };

      if ( !date_format || date_format == 'd.m.y' ) {
      date =  dd + '.' + mm + '.' + yyyy;
      };


      // ako nije neveden format ili ako je sa crticama
      if ( date_format == 'y_m_d' ) {
      date = yyyy + '_' + mm + '_' + dd;
      };  

      // ako nije neveden format ili ako je sa crticama
      if ( date_format == 'y-m-d' ) {
      date = yyyy + '-' + mm + '-' + dd;
      };


      time = hr + ':' + min + ':' + sek;


      return {
      date: date,
      time: time,
      date_time: date + " " + time,
      }

    };
    function get_super_editors() {

      var super_editori = [

        67, // Bruno Žgela
        24, // Stjepan Gašljević
        30, // Anel
        10, // Toni
        9,  // Radmila
        42, // Alex
        62, // Zoran

      ];

      return super_editori;      
    };

    // ------------------------------------------------------------------------------------------
    // SVE FUNC POTREBNE ZA OVAJ FILTER !!!
    // ------------------------------------------------------------------------------------------

    $.each( items, function( ind, elem ) {
      if ( items[ind].level ) delete items[ind].level;
    });

    var statuses = sort_parents(items);

    // ako nema global znači da je lokalno i ako se nalazi na statuses page
    if ( typeof global == `undefined` && window.location.hash.indexOf(`#statuses_page`) > - 1 ) {

      var criteria = [ 'rok_isporuke', 'time' ];

      if ( typeof window == "undefined" ) {
        global.multisort( items, criteria );
      } else {
        multisort( items, criteria );
      };

    };


    /*
    var uniq_statuses = [];
    $.each( statuses, function( s_ind, status ) {

      var curr_status_sifra = status.sifra;
      var curr_product_id = status.product_id;
      var is_copy = false;

      $.each( uniq_statuses, function( uniq_ind, uniq_status ) {
        if ( uniq_status.sifra == curr_status_sifra ) is_copy = true;
      });

      if ( is_copy == false ) uniq_statuses.push(status);
    });

    var criteria = [ 'time' ];

    if ( typeof window == "undefined" ) {
      global.multisort( uniq_statuses, criteria );
    } else {
      multisort( uniq_statuses, criteria );
    };

    var statuses = uniq_statuses;

    */

    var statuses_for_table = [];

    if ( statuses && statuses.length > 0 ) {

      $.each(statuses, function(index, status_obj ) {


        var { 

          _id,
          
          rok_isporuke,
          order_sirov_count,
          sirov,
          
          kupac_id,
          kupac_naziv,

          product_id,
          product_tip,
          proj_sifra,
          item_sifra,
          variant,
          status,  /* status je zapravo isto što i sifra ali mi je lakše tako da me ne zbunjuje kasnije kad radim pretragu */
          sifra,


          full_product_name,

          link,
          elem_parent,
          cat,
          time,
          est_deadline_hours,
          est_deadline_date,
          start,
          end,
          status_tip,
          dep_from,
          from,
          dep_to,
          to,
          komentar,
          prio,


          worker_error_cost,
          worker_error_users,
          worker_error_possible_solution,
          worker_error_solution,



        } = status_obj;


        // STATUS DOCS
        var all_docs = "";

        if ( status_obj.docs && $.isArray(status_obj.docs) ) {

          var criteria = [ '~time' ];

          if ( typeof window == "undefined" ) {
            global.multisort( status_obj.docs, criteria );
          } else {
            multisort( status_obj.docs, criteria );
          };

          $.each(status_obj.docs, function(doc_index, doc) {
            var doc_html = 
            `
            <div class="docs_row">
              <div class="docs_date">${ cit_dt(doc.time).date_time }</div>
              <div class="docs_link">${ doc.link }</div>
            </div>
            `;

            all_docs += doc_html

          }); // kraj loopa po docs

        }; // kraj ako ima docs


        var work_hours = null;
        if ( status_obj.work_times && status_obj.work_times.length > 0  ) {
          $.each( status_obj.work_times, function(ind, work_obj) {
            work_hours += work_obj.duration;
          });
        };

        var duration = null;
        // ----------------------------------------------------------------------------------------
        // prije sam samo uzeo razliku ali to ne valja jer trebam brojati samo radne sate !!!
        // duration = duration ? cit_round( duration/(1000*60*60), 2 ) : null; 
        // ----------------------------------------------------------------------------------------

        if ( status_obj.start && status_obj.end ) {

          // bilo koji odjel tj kategorija statusa  -----> OSIM proizvodnja
          if (
            status_obj.status_tip               &&
            status_obj.status_tip.cat           &&
            status_obj.status_tip.cat.sifra !== `prod` 
          ) {
            // obično radno vrijeme od 8 - 16
            duration = work_minutes_forward( status_obj.start, status_obj.end, {in: 8, out: 16, sub: false, ned: false } );
          } else {
            // za proizvodnju je radno vrijeme puno veće
            duration = work_minutes_forward( status_obj.start, status_obj.end, {in: 6, out: 21, sub: true, ned: false } );
          };

        };

        if ( status_obj.sifra == `status16469254461367456` ) {
          console.log(`stop`);
        };


        var late = null;
        var diff = null;

        if ( est_deadline_date ) {

          diff = ( status_obj.end || Date.now() ) - est_deadline_date;

          if ( diff !== null && diff > 0 ) {

            // bilo koji odjel tj kategorija statusa  -----> OSIM proizvodnja
            if (
              status_obj.status_tip               &&
              status_obj.status_tip.cat           &&
              status_obj.status_tip.cat.sifra !== `prod` 
            ) {
              late = work_minutes_reverse( ( status_obj.end || Date.now() ), est_deadline_date, {in: 8, out: 16, sub: false, ned: false } );

            }
            else {
              late = work_minutes_reverse( ( status_obj.end || Date.now() ), est_deadline_date, {in: 6, out: 21, sub: true, ned: false } );
            };

            late = late/(60);

          };

        };

        var hide_reply = false;
        var hide_edit = false;
        var hide_delete = false;
        var hide_seen = true;

        // ako koristim ovaj filter lokalno onda window postoji
        if ( typeof window == "undefined" ) {

          // ovo znači da je na serveru 
          hide_reply = true;
          hide_edit = true;
          hide_delete = true;
          hide_seen = true;

        } else {

          // ako sam ja poslao ovaj status onda ne mogu odgovoriti na njega !!! 

          // VAŽNO SAD SAM OSTAVIO DA MOŽEŠ SAM SEBI ODGOVORITI
          // VAŽNO SAD SAM OSTAVIO DA MOŽEŠ SAM SEBI ODGOVORITI    !!!!!!!!!!
          // VAŽNO SAD SAM OSTAVIO DA MOŽEŠ SAM SEBI ODGOVORITI

          // if ( status_obj.from.user_number == window.cit_user.user_number ) hide_reply = true;

          // ako ovo nije moj status ga ne mogu editirati
          if ( 
            typeof window !== "undefined" &&
            window &&
            window.cit_user &&
            window.cit_user.user_number &&
            status_obj.from &&
            status_obj.from.user_number &&
            status_obj.from.user_number !== window.cit_user.user_number ) {

            hide_edit = true;

          }

        };


        var parent_status = null;
        var branch_done = false;
        var indent_div = "";


        function is_branch_done(parent_status, statuses) {

          var branch_done = false;

          var all_childern = [];
          $.each(statuses, function( s_ind, check_status) {
            if( check_status.elem_parent == parent_status.sifra) all_childern.push(check_status);
          });

          // onda loopaj sve finish statuse u parentu


          $.each( parent_status.status_tip.grana_finish, function(f_index, finish) {
            $.each( all_childern, function(c_index, child_status) {
              // ako BILO KOJI CHILD STATUS IMA STATUS TIP KOJI JE  JEDAN OD FINISH OD PARENTA 
              // -----------> onda je grana gotova !!!!
              if ( child_status.status_tip.sifra == finish.sifra ) {
                branch_done = true;
              };
            });
          });

          return branch_done;

        };


        // ------------START------------- CLINET SIDE I NIJE STATUS PAGE
        // ------------START------------- CLINET SIDE I NIJE STATUS PAGE
        // ------------START------------- CLINET SIDE I NIJE STATUS PAGE

        if ( 
          typeof window !== 'undefined'
          &&
          window.location.hash.indexOf(`#statuses_page/`) == -1
        ) { 

          if (status_obj.elem_parent) {

            // prvo nađi parent status object 
            $.each(statuses, function(ind, parent) {
              if ( status_obj.elem_parent == parent.sifra ) parent_status = parent;
            });


            if ( parent_status ) { 

              branch_done = is_branch_done( parent_status, statuses);

              if ( branch_done == false ) {
                indent_div = `<div style="width: 10px; background: red;"></div>`;
              }
              else {
                // ako je status kraj jedne grane onda nema reply !!!!
                hide_reply = true;
                indent_div = `<div style="width: 10px; background: #89b0d4;"></div>`;
              };


              // PONOVO pretraži sve finish statuse u parentu

              $.each( parent_status.status_tip.grana_finish, function(f_index, finish) {
                  // ako je trenutni status je zapravo jedan od finish statusa za taj parent
                  if ( status_obj.status_tip.sifra == finish.sifra ) {
                      // SAMO AKO USER   !!  NIJE  !!   KLIKNUO NA OKO ( i ako je finish status upućen njemu ) !!!
                      if ( 
                        typeof global == "undefined" &&
                        status_obj.seen !== true &&

                        window &&
                        window.cit_user &&
                        window.cit_user.user_number &&

                        status_obj.to &&
                        status_obj.to.user_number &&
                        status_obj.to.user_number == window.cit_user.user_number 
                      ) {

                        hide_seen = false;

                      };

                  }; // kraj ako je trenutni status zapravo JEDAN OD finish statusa od parenta

              }); // kraj loopa po svim finish statusima u parentu

            }; // kraj ako je našao parenta od trenutnog statusa koji je child

          }
          else {
            // ako je ovo parent
            branch_done = is_branch_done(status_obj, statuses);
            if ( branch_done ) hide_reply = true;
          };

        };
        // ------------END------------- CLINET SIDE I NIJE STATUS PAGE
        // ------------END------------- CLINET SIDE I NIJE STATUS PAGE
        // ------------END------------- CLINET SIDE I NIJE STATUS PAGE


        var sirov_input_link = ``;

        if ( status_obj.sirov ) {

          var sirov_input_text = ( 
            status_obj.sirov.sirovina_sifra + "--" + 
            status_obj.sirov.full_naziv + "--" +
            (status_obj.sirov.dobavljac ? status_obj.sirov.dobavljac.naziv : "") +
            " KOLIČINA: " + (status_obj.order_sirov_count || 0)
          );

          sirov_input_link = 
          `<br>
            <a href="#sirov/${status_obj.sirov._id}/status/${status_obj.sifra}" 
                target="_blank"
                onClick="event.stopPropagation();"> 
                ${sirov_input_text} 
          </a>`

        };


        var proces_drop_pill = ``;
        
        if ( status_obj.rn_droped ) {
          proces_drop_pill = 
            `<span class="cit_storno_pill">
              ${ `ODUSTAO OD RN ` + status_obj.rn_droped.full_name + ` ` + cit_dt( status_obj.rn_droped.time).date_time }
            </span>`;
        };


        var proces_extra_pill = ``;
        if ( status_obj.rn_extra ) {
          proces_extra_pill = 
            `<span class="cit_extra_rn_pill">
              ${ `EXTRA RN ` + status_obj.rn_extra.full_name + ` ` + cit_dt( status_obj.rn_extra.time).date_time }
            </span>`;
          
        };
        
        
        var proces_finished_pill = ``;
        if ( status_obj.rn_finished ) {
          proces_finished_pill = 
            `<span class="cit_extra_rn_pill">
              ${ `GOTOV RN ` + status_obj.rn_finished.full_name + ` ` + cit_dt( status_obj.rn_finished.time).date_time }
            </span>`;
        };
        


        var worker_error_text = ``;
        if ( worker_error_possible_solution || worker_error_solution || worker_error_cost ) {

          worker_error_text += worker_error_possible_solution ? ("<br>MOGUĆE RJEŠENJE:<br>" + worker_error_possible_solution) : "";
          worker_error_text += worker_error_solution ? ("<br>RJEŠENJE:<br>" + worker_error_solution) : "";

        };


        var prio_input = null;

        // ----------START---------- LOKALNO SAM I NA STATUS PAGE-u
        // ----------START---------- LOKALNO SAM I NA STATUS PAGE-u
        // ----------START---------- LOKALNO SAM I NA STATUS PAGE-u
        if ( 
          typeof global == 'undefined'
          &&
          window.location.hash.indexOf(`#statuses_page/`) > -1 
        ) {

          var super_editori = get_super_editors();
          var prio_valid = cit_deep( this_module.valid.prio_input );
          // ako nije NITI JEDAN OD EDITORA onda zaklučaj
          if ( super_editori.indexOf( window.cit_user.user_number ) == -1 ) prio_valid.lock = true;

          prio_input = cit_comp( cit_rand() + '_' + status_obj.sifra + `_prio_input`, prio_valid , status_obj.prio || null, null, `text-align: center !important;` );

        };


        var list_status_object = {
          
          /* kopiraj sve propertije od status objekata */
          ...status_obj,

          /* zatim prepiši sve propertije koje sam promjenio  */
          status_id: status_obj.status_id || status_obj._id, // probaj jel već ima status_id ----> ako nema onda probaj _id !!!!!
          
          rok_isporuke,
          order_sirov_count,
          sirov,
          
          kupac_id,
          kupac_naziv,

          product_id,
          product_tip,
          proj_sifra,
          item_sifra,
          variant,
          status, /* status je zapravo isto što i sifra ali mi je lakše tako da me ne zbunjuje kasnje kad radim pretragu */
          sifra,

          full_product_name,
          link, /* može biti link koji vodi na product u projektu ali može biti i link koji vodi na sirovinu */
          elem_parent,
          cat,
          docs: all_docs,
          time,
          est_deadline_hours,
          work_hours,
          est_deadline_date,
          start,
          end,
          duration,
          late,
          product_link: `<a href="${link}" target="_blank" onClick="event.stopPropagation();" >${full_product_name}</a>`, /* može biti i ime sirovine */
          status_tip,
          status_tip_naziv: 
          `${indent_div}<div class="custom_result_box" style="background: transparent;">${ status_tip ? status_tip.naziv : "" }</div>`,
          dep_from,
          dep_from_naziv: (dep_from ? dep_from.naziv : null),
          from,
          from_full_name: (from ? from.full_name : null),
          dep_to,
          dep_to_naziv: (dep_to ? dep_to.naziv: null),
          to,
          to_full_name: (to ? to.full_name : null),

          komentar: ( proces_finished_pill + proces_extra_pill + proces_drop_pill + komentar + sirov_input_link + worker_error_text),

          hide_reply,
          hide_edit,
          hide_seen,
          hide_delete,

          prio_input,
          prio,


        };
        
        if ( list_status_object._id ) delete list_status_object._id;
        
        statuses_for_table.push( list_status_object );
        
        

      }); // kraj loopa svih statusa


    }; // kraj jel postoji array statusa

    return statuses_for_table;

  };
  this_module.statuses_filter = statuses_filter;
  
  
  function show_cols_arr() {

    var show_cols = [ 
      "product_link",
      "status_tip_naziv",
      "komentar",
      "from_full_name",
      "to_full_name",
      "est_deadline_hours",
      "work_hours",
      "est_deadline_date",
      "rok_isporuke",
      "start",
      "end",
      "duration",
      "late",
      "docs",
      
      'button_edit',
      'button_reply',
      'button_seen',

    ];
    
    if ( typeof global == 'undefined' && window.location.hash.indexOf(`#statuses_page/`) > -1 ) {
      
      // obriši zadnja tri itema
      show_cols.pop();
      show_cols.pop();
      show_cols.pop();
      
      // uguraj novi item na kraju
      show_cols.push(`prio_input`);
      
      
    };
    
    return show_cols;
    
  };
  this_module.show_cols_arr = show_cols_arr;
  

  function custom_headers_arr() {
  
    var custom_headers = [
        "Stavka",
        "Status",
        "Komentar",
        "Od",
        "Prema",
        "Procjena sati",
        "Upisano sati",
        "Procjena datum",
        "Rok isporuke",
        "Start",
        "End",
        "Trajanje minuta",
        "Kasni sati",
        "Docs",
      
        'UREDI',
        'ODG',
        'OK'
      ];

    if ( typeof global == 'undefined' && window.location.hash.indexOf(`#statuses_page/`) > -1 ) {
      
      // obriši zadnja tri itema
      custom_headers.pop();
      custom_headers.pop();
      custom_headers.pop();
      
      // uguraj novi item na kraju
      custom_headers.push(`PRIORITET`);
      
      
    };

    
    return custom_headers;
  
  };
  this_module.custom_headers_arr = custom_headers_arr;
  
  
  function col_widths_arr() {
  
    var col_widths = [
        1.7, // "Stavka"
        1.5, // "Status",
        3, //"Komentar",
        1, //"Od",
        1, //"Prema",
        0.7, //"Procjena sati",
        0.7, // "Upisano sati",
        1, // "Procjena datum",
        1, // "Rok isporuke",
        1, //"Start",
        1, //"End",
        0.7, //"Trajanje",
        0.7, //"Kasni",
        3.8, //"Docs",
      
        0.5, //'UREDI,
        0.5, //'REPLY,
        0.5, //'SEEN,
      ];
    
    if ( typeof global == 'undefined' && window.location.hash.indexOf(`#statuses_page/`) > -1 ) {
      
      // obriši zadnja tri itema
      col_widths.pop();
      col_widths.pop();
      col_widths.pop();
      
      // uguraj novi item na kraju
      col_widths.push(1.5);
      
  
      
    };
    
    return col_widths;
    
  };
  this_module.col_widths_arr = col_widths_arr;
  
  
  this_module.format_cols_obj = { 
    est_deadline_date: "date_time",
    rok_isporuke: "date_time",
    start: "date_time",
    end: "date_time",
    work_hours: 2,
    duration: 0,
    late: 2,
    est_deadline_hours: 2
  };

  
  function make_status_list(data, prio_sort) {
    
    
    var product_comp = null;
    var product_comp_id  = null;
    var product_data = null;
    
    if ( data.for_module == "product" ) {
      
      product_comp = $(`#${data.rand_id}_save_status`).closest('.product_comp');
    
      if ( product_comp.length > 0 ) {
        product_comp_id = product_comp[0].id;
        product_data = this_module.product_module.cit_data[product_comp_id];
      };

    };
    
    
    // ubaci u sve statuse koji imaju string u propertiju kalk ( tj. id od kalk) ----> ubaci cijeli objekt od kalkulacije
    // POŠTO U PRODUCTU NE RADIM POPULATE KALK ZA STATUSE (na serveru)
    // to moram napraviti na client side 
    
    $.each( data.statuses, function(s_ind, status) {
      
      if ( 
        product_data?.kalk
        &&
        status.kalk
        && 
        typeof status.kalk == "string" 
        &&
        status.kalk == product_data?.kalk?._id
      ) {
        
        data.statuses[s_ind].kalk = product_data.kalk;
        
      };
      
    });
    
    
    // koristim istu funkciju za filter u listi koju prikazujem i drop listi kada pretražujem statuse
    // naravno postoje razlike u izgledu kao na primjer NE prikazujem gumbe za edit u drop listi !!!
    data.statuses_for_table = this_module.statuses_filter(data.statuses, $);
    
    
    // ako nema statusa
    if ( data.statuses_for_table.length == 0 ) {
      $(`#${data.rand_id}_statuses_box`).html('');
      return;
    };
    
    
    
    if ( 
      typeof global == `undefined` 
      &&
      window.location.hash.indexOf(`#statuses_page/`) > - 1 // ako je je lokalno na status page-u
      &&
      ( prio_sort || window.cit_user?.dep.sifra == "PRO" ) // ako je user iz proizvodnje on uvijek gleda SORT po prioritetu !!!!
      
    ) {
      
      // ------------------------------------------------------------------------

        // ako nema prio stavi da je prio = 0
        $.each( data.statuses_for_table, function(s_ind, table_status) {
          if ( !table_status.prio ) data.statuses_for_table[s_ind].prio = 0;
        });

        var criteria = [ 'prio', 'rok_isporuke' ];
        multisort( data.statuses_for_table, criteria );
        
          
      // ------------------------------------------------------------------------
    }; // kraj ako je ovo statuses page
  
    


    
    // ------------------------------------------------ START STATUSES PROPS ZA GENERIRANJE LISTE STATUSA
    // ------------------------------------------------ START STATUSES PROPS ZA GENERIRANJE LISTE STATUSA
    var statuses_props = {
      
      hide_sort_arrows: true,
      
      desc: `samo za kreiranje tablice svih STATUSA u productu ${data.item_sifra} `,
      local: true,
      
      list: data.statuses_for_table,
      
      show_cols: this_module.show_cols_arr(),
      custom_headers: this_module.custom_headers_arr(),
      col_widths: this_module.col_widths_arr(),
      format_cols: this_module.format_cols_obj,
      
      parent: `#${data.rand_id}_statuses_box`,
      return: {},
      show_on_click: false,
      cit_run: function(state) { console.log(state); },
      
      button_edit: function(e) { 
        
        e.stopPropagation(); 
        e.preventDefault();
        
        console.log("kliknuo edit za STATUS");
        
        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        // spremi postojeći current status  - NPR KADA SE NALAZIM U PRODUCTION PAGE-u !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        window.curr_prodon_status_temp = null;
        if ( data.current_status && window.location.hash.indexOf(`#production/`) > -1 ) {
          window.curr_prodon_status_temp = cit_deep(data.current_status);
        };
        
        
        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        
        
        var index = find_index(data.statuses, sifra , `sifra` );
        data.current_status = cit_deep( data.statuses[index] );
        
        
        if ( data.current_status.status_tip?.sifra == `IS16430378755748118` /* AKO JE CURRENT STATUS RADNI NALOG !!! */  ) {
          $(`#${rand_id}_rn_files_select_box`).css(`display`, `flex`);
          $(`#${rand_id}_rn_alat_select_box`).css(`display`, `flex`);
        } else {
          $(`#${rand_id}_rn_files_select_box`).css(`display`, `none`);
          $(`#${rand_id}_rn_alat_select_box`).css(`display`, `none`);
        };
        
        
        // prvo enable sve inpute u status sučelju
        $('#'+data.id+` input`).each( function() {
          $(this).prop( "disabled", false );
        });
        
        // nemoj više dati nikakve uploade
        
        // -------------- IPAK SAM ODLUČIO DA DODAJEM DOKUMENTE KOD EDITA STATUSA !!!!  ( prije je to bilo ondemogućeno !!! ) --------------
        
        // $(`label[for='${rand_id}_status_docs']`).addClass(`cit_disable`);
        
      
        $('#'+rand_id+`_status_from`).val(data.current_status.from?.full_name || "");
        $('#'+rand_id+`_status_from`).prop( "disabled", true );
        
        $('#'+rand_id+`_status_tip`).val(data.current_status.status_tip?.naziv || "");
        $('#'+rand_id+`_status_tip`).prop( "disabled", true );
        
        $('#'+rand_id+`_dep_to`).val(data.current_status.dep_to?.naziv || "");
        $('#'+rand_id+`_dep_to`).prop( "disabled", true );
        
        $('#'+rand_id+`_status_to`).val(data.current_status.to?.full_name || "");
        $('#'+rand_id+`_status_to`).prop( "disabled", true );
        
        $('#'+rand_id+`_tezina`).val(data.current_status.tezina || "");
        $('#'+rand_id+`_tezina`).prop( "disabled", true );
        
        
        format_number_input(
          data.current_status?.est_deadline_hours || null, // value from object
          $('#'+rand_id+`_est_deadline_hours`)[0], // input element
          2, // na koliko decimala
          null
        );
        
        // ako sam već jednom upisao procjenu sati  - onda više ne mogu to editirati
        if ( data.current_status?.est_deadline_hours )  {
          $('#'+rand_id+`_est_deadline_hours`).prop( "disabled", true );
        } else {
          $('#'+rand_id+`_est_deadline_hours`).prop( "disabled", false );
        };
        
        // ----------------------- SUMA SVIH WORK VERMENA -----------------------
        var work_time_sum = 0;
        $.each( data.current_status.work_times, function(ind, work_obj) {
          work_time_sum += work_obj.duration || 0;
        });
        
        format_number_input(
          work_time_sum, 
          $('#'+rand_id+`_work_time_sum`)[0],
          null,
          null,
        );
        
        $('#'+rand_id+`_work_time_sum`).prop( "disabled", true );
        // ----------------------- SUMA SVIH WORK VERMENA -----------------------
        
        $('#'+rand_id+`_add_work_time`).val("");
        
        $(`#${rand_id}_quit_status`).css(`display`, `flex`);
        $(`#${rand_id}_save_status`).css(`display`, `none`);
        $(`#${rand_id}_reply_status`).css(`display`, `none`);
        $(`#${rand_id}_update_status`).css(`display`, `flex`);
        
        var deadline_date = data.current_status.est_deadline_date ? cit_dt(data.current_status.est_deadline_date).date_time : null;
        
        $('#'+rand_id+`_est_deadline_date`).val( deadline_date || "" );
        $('#'+rand_id+`_est_deadline_date`).prop( "disabled", true );
        
        $('#'+rand_id+`_status_komentar`).val( data.current_status?.komentar || "" );
        $('#'+rand_id+`_status_komentar`).prop( "disabled", true );
        
        
        
        this_module.show_worker_error_box(data);
        this_module.show_choose_sirov_box(data);
        
        
        this_module.make_sirov_in_status(data.current_status.sirov, data);
        
        this_module.make_work_list(data);
        
        
        $(`#${data.rand_id}_rn_files_list_box`).html(``);
        
        this_module.make_rn_files_list(data);
        
        
        
        $(`#${data.rand_id}_link_to_alat`).attr(`href`, `#`);
        
        if ( data.current_status?.rn_alat ) {
          $('#'+rand_id+`_rn_alat`).val( data.current_status.rn_alat.full_naziv || "" );
          
          $(`#${data.rand_id}_link_to_alat`).attr(`href`, `#sirov/${data.current_status.rn_alat._id}/status/null`);
          
          
        };
        
        // scrollaj do forme za upisivanje
        setTimeout( function() {
          cit_scroll( $("#"+this_comp_id), -200, 300);
        }, 100 );
        
        
      },

      button_reply: function(e) { 
        
        e.stopPropagation(); 
        e.preventDefault();

        console.log("kliknuo REPLY za STATUS");


        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        
        setTimeout( function() {
          var scroll_top = $(`#${rand_id}_standardna_forma_za_slanje_statusa`).offset().top;
          $(`html`)[0].scrollTop = scroll_top - 200;
        }, 200);
        
        
        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);

        var index = find_index(data.statuses, sifra , `sifra` );
        var status_copy = cit_deep( data.statuses[index] );
        
        
        this_module.setup_status_for_reply(data, status_copy );
        
        
        // može ponovo dati da uploada jer je ovo replay
        $(`label[for='${rand_id}_status_docs']`).removeClass(`cit_disable`);
        
        // prvo enable sve inpute u status sučelju
        $('#'+data.id+` input`).each( function() {
          $(this).prop( "disabled", false );
        });
        
        
        
        $('#'+rand_id+`_status_from`).val(data.current_status.from?.full_name || "");
        $('#'+rand_id+`_status_from`).prop( "disabled", true );
        
        $('#'+rand_id+`_status_tip`).val(data.current_status.status_tip?.naziv || "");
        
        
        $('#'+rand_id+`_dep_to`).val(data.current_status.dep_to?.naziv || "");
        // $('#'+rand_id+`_dep_to`).prop( "disabled", true );
        
        $('#'+rand_id+`_status_to`).val(data.current_status.to?.full_name || "");
        // $('#'+rand_id+`_status_to`).prop( "disabled", true );
        
        $('#'+rand_id+`_tezina`).val(data.current_status.tezina || "");
        $('#'+rand_id+`_tezina`).prop( "disabled", true );
        
        data.current_status.est_deadline_hours = null;
        $('#'+rand_id+`_est_deadline_hours`).val("");
        
        // ----------------------- SUMA SVIH WORK VERMENA -----------------------
        data.current_status.work_times = null;
        $('#'+rand_id+`_work_time_sum`).val("")
        $('#'+rand_id+`_work_time_sum`).prop( "disabled", true );
        // ----------------------- SUMA SVIH WORK VERMENA -----------------------
        
        $('#'+rand_id+`_add_work_time`).val("");
        
        
        $(`#${rand_id}_quit_status`).css(`display`, `flex`);
        $(`#${rand_id}_save_status`).css(`display`, `none`);
        $(`#${rand_id}_reply_status`).css(`display`, `flex`);
        $(`#${rand_id}_update_status`).css(`display`, `none`);
        
        
        data.current_status.est_deadline_date = null;
        $('#'+rand_id+`_est_deadline_date`).val( "" );
        $('#'+rand_id+`_est_deadline_date`).prop( "disabled", false );
        
        
        data.current_status.old_komentar = data.current_status.komentar;
        $('#'+rand_id+`_status_komentar`).val("");
        $('#'+rand_id+`_status_komentar`).prop( "disabled", false );
        
        
        this_module.show_worker_error_box(data);
        
        // prvo obriši input za sirovinu 
        $("#"+data.rand_id+"_find_sirov_in_status").find(".single_select").val(``);
        
        
        // ali ako postoji sirovina u statusu onda upiši u input !!!
        if ( data.current_status.sirov ) {
          
          this_module.show_choose_sirov_box(data);
          this_module.make_sirov_in_status(data.current_status.sirov, data);
          
          $('#'+rand_id+`_order_sirov_count`).prop( "disabled", false );
          $("#"+data.rand_id+"_find_sirov_in_status").find(".single_select").prop( "disabled", true );
          
          this_module.make_dobav_kontaki_in_status( data.current_status.sirov, data );
          
        } 
        else {
          
          $("#"+data.rand_id+"_choose_sirovina_box").css(`display`, `none`);
          $("#"+data.rand_id+"_find_sirov_in_status").find(".single_select").prop( "disabled", false ); // iako je sakriveno otključaj
          $('#'+data.rand_id+`_order_sirov_count`).prop( "disabled", false ); // iako je sakriveno otključaj
          
          $("#"+rand_id+"_link_to_sirov").attr(`href`, `#`);
          
        };
        
        if ( data.for_module !== `production` ) {
        
          // scrollaj do forme za upisivanje
          setTimeout( function() {
            cit_scroll( $("#"+this_comp_id + " .status_from_box"), -200, 300);
          }, 100 );
          
        };
        
      },
      
      button_seen: function(e) { 
        
        e.stopPropagation(); 
        e.preventDefault();
        
        console.log("kliknuo SEEN za STATUS");
        
        
        var this_button = this;
        
        var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        // spremi postojeći current status  - NPR KADA SE NALAZIM U PRODUCTION PAGE-u !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        window.curr_prodon_status_temp = null;
        if ( data.current_status && window.location.hash.indexOf(`#production/`) > -1 ) {
          window.curr_prodon_status_temp = cit_deep(data.current_status);
        };
        
        
        var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var index = find_index(data.statuses, sifra , `sifra` );
        
        
        data.statuses[index].seen = true;
        
        data.statuses[index].end = Date.now();
        
        data.current_status = cit_deep( data.statuses[index] );        
        
        
        // ----------------------------------------------------------------------------------------------------------------
        // START SAVE TO DB
        // ----------------------------------------------------------------------------------------------------------------


        $.each( data.current_status, function( key, value ) {
          // ako je objekt ili array onda ga flataj
          if ( $.isPlainObject(value) || $.isArray(value) ) {
            data.current_status[key+'_flat'] = JSON.stringify(value);
          };
        });

        $.ajax({
          headers: {
            'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
          },
          type: "POST",
          cache: false,
          url: `/save_status`,
          data: JSON.stringify( data.current_status ),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
        .done(function (result) {

          console.log(result);
          if ( result.success == true ) {

            cit_toast(`POTVRDILI STE DA STE VIDJELI STATUS!`);
            
            animate_badge( $(`#cit_my_tasks`), -1 );

            // obriši gumb da da di vidio finish status
            this_button.remove();

            data.current_status = null;
            
            
            this_module.make_status_list(data);
            
        
          } else {

            // ako result NIJE SUCCESS = true

            if ( result.msg ) popup_error(result.msg);
            if ( !result.msg ) popup_error(`Greška prilikom UPDATE vidljivosti statusa!`);
          };

        })
        .fail(function (error) {

          console.log(error);
          popup_error(`Došlo je do greške na serveru prilikom UPDATE vidljivosti statusa!`);
        })
        .always(function() {

          toggle_global_progress_bar(false);
          
        });

        // ----------------------------------------------------------------------------------------------------------------
        // END SAVE TO DB
        // ----------------------------------------------------------------------------------------------------------------
        
      },
      
      button_delete: async function(e) {
        
        e.stopPropagation();
        e.preventDefault();
        
        var this_button = this;
        
        console.log("kliknuo DELETE za STATUS");
        
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;
          
          
          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          data.statuses = delete_item(data.statuses, sifra , `sifra` );
          
          $(`#`+parent_row_id).remove();
        };
        
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj Status?`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj Status?`, null, 'yes/no', delete_yes, delete_no, null);
        
      },
      
    };
    
    // ------------------------------------------------ KRAJ STATUSES PROPS ZA GENERIRANJE LISTE STATUSA
    // ------------------------------------------------ KRAJ STATUSES PROPS ZA GENERIRANJE LISTE STATUSA
    
    
    
    // obriši sve od prije
    $(`#${data.rand_id}_statuses_box`).html('');
    
    
    if ( data.statuses_for_table.length > 0 ) {
      
      create_cit_result_list(data.statuses_for_table, statuses_props );
      
      
      wait_for(`$("#${data.rand_id}_statuses_box .search_result_row").length > 0`, function() {
        
        // prvo obriši sve work/files tablice ako postoje
        $(`#${data.rand_id}_statuses_box .search_result_table .search_result_table`).remove();
        // i header od svake sve work/files tablice
        $(`#${data.rand_id}_statuses_box .search_result_table .search_result_row.header`).remove();
        
        
        $.each( data.statuses, function(s_ind, status) {
          
          var all_extra_tables = ``;
         
          var perc_bar = this_module.make_percent_bar(status);
          if ( perc_bar ) all_extra_tables += perc_bar;
          
          var work_error_table = this_module.make_worker_error_users_list(status, 'return_html');
          if ( work_error_table ) all_extra_tables += work_error_table;
          
          var work_table = this_module.make_work_list(status, 'return_html');
          if ( work_table ) all_extra_tables += work_table;
          
          var rn_files_table = this_module.make_rn_files_list(status, 'return_html');
          if ( rn_files_table ) all_extra_tables += rn_files_table;
          
          var rn_alat_table = this_module.make_rn_alat_list(status, 'return_html');
          if ( rn_alat_table ) all_extra_tables += rn_alat_table;
          
          
       
          $(`[data-id="${ status._id || status.status_id }"]`).after( all_extra_tables );
          
          
        });
        
        $(`#${data.rand_id}_statuses_box .prio_input .cit_input`).off(`blur`);
        $(`#${data.rand_id}_statuses_box .prio_input .cit_input`).on(`blur`, function () {
          
          
          var prio_value = cit_number( $(this).val() );
          
          // ako nije prazno i ako cit number daje null !!!
          if ( $(this).val() !== `` && prio_value == null ) {
            popup_warn(`Upis mora biti broj`);
            return;
          };
          
          // spremi postojeći current status  - NPR KADA SE NALAZIM U PRODUCTION PAGE-u !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          window.curr_prodon_status_temp = null;
          if ( data.current_status && window.location.hash.indexOf(`#production/`) > -1 ) {
            window.curr_prodon_status_temp = cit_deep(data.current_status);
          };
          
          
          // PRIMJER IDja
          // 16655884921040056_status1664443533147109_prio_input
          var status_sifra = this.id.split(`_`)[1];
          
          $.each( data.statuses, async function(s_ind, status) {
            
            if ( status.sifra == status_sifra ) {
              
              data.current_status = status;
              data.current_status.prio = prio_value;
              
              // napravi update statusa unutar arraja
              data.statuses = upsert_item( data.statuses, data.current_status, `sifra` );
              
              this_module.make_status_list( data, `prio_sort`);
              

              var Model_name = null;
              var Model_id = null;
              
              
              if ( data.current_status.partner_model_id ) { Model_name = `Partner`; Model_id = data.current_status.partner_model_id; };
              if ( data.current_status.sirov_model_id )   { Model_name = `Sirov`;   Model_id = data.current_status.sirov_model_id;   };
              if ( data.current_status.product_model_id ) { Model_name = `Product`; Model_id = data.current_status.product_model_id; };
              
              
              // ako ima partner link onda uzmi _id od partnera iz linka
              if ( !Model_name && !Model_id && data.current_status.link.indexOf(`#partner/`) > -1 ) {
                Model_name = `Partner`;
                Model_id = data.current_status.link.split(`/`)[1];
                data.current_status.partner_model_id = Model_id; // upiši partner _id
              };
              
              
              // ako ima SIROV link onda uzmi _id od SIROV iz linka
              if ( !Model_name && !Model_id && data.current_status.link.indexOf(`#sirov/`) > -1 ) {
                Model_name = `Sirov`;
                Model_id = data.current_status.link.split(`/`)[1];
                data.current_status.sirov_model_id = Model_id; // upiši sirov _id
              };
              
              
              // ako ima PRODUCT link onda uzmi _id od partnera iz linka
              if ( data.current_status?.product_id ) {
                Model_name = `Product`;
                Model_id = data.current_status.product_id;
                data.current_status.product_model_id = Model_id; // upiši product _id
              };

              
              if ( !Model_name && !Model_id && !data.current_status?.product_id ) {
                popup_warn(`Ovom statusu nedostaje podatak da li je nastao unutar kartice RESURSA, PRODUCTA ili PARTNERA(dobavljača) !!!`);
                if ( window.curr_prodon_status_temp !== null ) data.current_status = cit_deep(window.curr_prodon_status_temp);
                return;
              };
              
              // ako nema gore navedenih model idjeva onda stavi po defaultu da bude PRODUCT MODEL !!!!
              var result = await this_module.ajax_save_status(data, Model_name || `Product`, Model_id || data.current_status?.product_id || null );
              if ( result.success == true ) cit_toast(`PRIORITET JE SPREMLJENI!`);
              if ( window.curr_prodon_status_temp !== null ) data.current_status = cit_deep(window.curr_prodon_status_temp);
              
            };
              
          }); // loop po svim statusima
          
        }); // kraj listenera za upis u prio
        
        
        jQuery('.statuses_box .scrollbar-macosx').scrollbar( { autoUpdate: true });
        
        
      }, 5*1000); // kraj čekanja da se pojavi status tablica u html-u
      
    }; // kraj ako ima nešto u arraju za status table
    
  };
  this_module.make_status_list = make_status_list;
  
  
  
  
  function setup_status_for_reply( data, status_copy ) {
    
    
    
    window.curr_prodon_status_temp = null;
    
    if ( data.current_status && window.location.hash.indexOf(`#production/`) > -1 ) {
      // spremi postojeći current status  - NPR KADA SE NALAZIM U PRODUCTION PAGE-u !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      window.curr_prodon_status_temp = cit_deep(data.current_status);
    };


    var elem_parent = null;
    var elem_parent_object = null;

    // ako je trenutni status child
    if ( status_copy.elem_parent ) {
      
      elem_parent = status_copy.elem_parent;
      elem_parent_object = status_copy.elem_parent_object;
    };

    // ako je trenutni status parent
    if ( status_copy.elem_parent == null ) {

      elem_parent = status_copy.sifra;
      elem_parent_object = status_copy;
      
      // napravi deep copy za svaki slučaj
      elem_parent_object = cit_deep(elem_parent_object);

    };

    // u current status OVERRIDE elem parent i parent objekt
    data.current_status = { ...status_copy, elem_parent, elem_parent_object };

    data.current_status.start = Date.now();
    data.current_status.end = null;
    
    data.current_status.reply_for = data.current_status.sifra;
    
    data.current_status.responded = null;
    data.current_status.seen = null;
    data.current_status.done_for_status = null;
    data.current_status.work_finished = null;
    
    data.current_status.records = null;

    data.current_status.docs = null;
    data.current_status.rn_files = null;
    
    data.current_status.est_deadline_hours = null;
    data.current_status.est_deadline_date = null;
    
    data.current_status.work_times = null;
    data.current_status.worker_error_users = null;

    
    // ako je from user različit od logiranog usera
    // DAKLE AKO NE ODGOVARAM SAMOM SEBI !!!!
    
    if ( data.current_status.from.user_number !== window.cit_user.user_number ) { 

      // FROM postaje TO
      data.current_status.to = {
        _id: data.current_status.from._id,
        name: data.current_status.from.name,
        user_number: data.current_status.from.user_number,
        full_name: data.current_status.from.full_name,
        email: data.current_status.from.email,
      };
      
      data.current_status.dep_to = cit_deep( data.current_status.dep_from );

    };
    
    // POŠILJATELJ MORA UVIJEK BITI ULOGIRANI USER !!!
    data.current_status.from = {
      _id: window.cit_user._id,
      name: window.cit_user.name,
      user_number: window.cit_user.user_number,
      full_name: window.cit_user.full_name,
      email: window.cit_user.email,
    };
    
    data.current_status.dep_from = cit_deep( window.cit_user.dep );
    
    // status tip od curr postaje old status tip
    data.current_status.old_status_tip = cit_deep( data.current_status.status_tip );
    
  };
  this_module.setup_status_for_reply = setup_status_for_reply;
  
  
  
  async function make_dobav_kontaki_in_status(state, data) {
    
    var this_comp_id = $(`#${data.rand_id}_svi_kontakti_dobavljac_box`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    
    if ( !data.current_status ) data.current_status = {};
    if ( !data.current_status.sirov ) data.current_status.sirov = {};
    
   
    // trebam uzeti iz baze cijeli objekt od sirovine
    var db_sirovina = null;

    var props = {
      filter: null,
      url: '/find_sirov',
      query: { _id: state._id },
      desc: `pretraga unutar statusa kada je u projectu da dobijem sirovinu ali zapravo mi treba dobavljač i njegovi kontakti : )`,
    };
    try {
      db_sirovina = await search_database(null, props );
    } catch (err) {
      console.log(err);
    };

    // ako je našao nešto onda uzmi samo prvi član u arraju
    if ( db_sirovina?.length > 0 ) {
      db_sirovina = db_sirovina[0];
    }; 

    console.log( db_sirovina );
    

    $(`#`+rand_id+`_doc_adresa`).html("");
    
    if ( db_sirovina == null || !db_sirovina.dobavljac ) {
      data.current_status.sirov.dobavljac = null;
      $(`#${data.rand_id}_svi_kontakti_dobavljac_box`).html("");
      $(`#`+rand_id+`_doc_adresa`).html("");
      return;
    };

/*
    var full_partner = 
        db_sirovina.dobavljac.naziv + 
        ( db_sirovina.dobavljac.grupacija_naziv ? (", " + db_sirovina.dobavljac.grupacija_naziv) : "" ) + 
        ( db_sirovina.dobavljac.sjediste ? ", " + db_sirovina.dobavljac.sjediste : "" );
    
    
    data.current_status.sirov.dobavljac = {
      _id: db_sirovina.dobavljac._id,
      naziv: db_sirovina.dobavljac.naziv,
      sjediste: db_sirovina.dobavljac.sjediste,
      grupacija_naziv:  db_sirovina.dobavljac.grupacija_naziv,
      status_naziv: db_sirovina.dobavljac.status_naziv
    };
*/

    console.log(data.current_status.sirov);

    // na silu obriši sve prethodne kontakte (samo HTML)
    $(`#${data.rand_id}_svi_kontakti_dobavljac_box`).html("");

    this_module.partner_module = await get_cit_module(`/modules/partners/partner_module.js`, `load_css`);
    
    this_module.partner_module.make_kontakt_list(db_sirovina.dobavljac, `#${data.rand_id}_svi_kontakti_dobavljac_box`);

    // čekaj da se pojave gumbi u listi kontakata za edit i delete
    // pa ih onda obriši :)
    // jer u projektima nema smisla editirati kontakte partnera !!!!
    wait_for(`$("#${data.rand_id}_svi_kontakti_dobavljac_box .result_row_edit_btn").length > 0`, function() {
      
      $(`${data.rand_id}_svi_kontakti_dobavljac_box .result_row_edit_btn`).remove();
      $(`#${data.rand_id}_svi_kontakti_dobavljac_box .result_row_delete_btn`).remove();
      
      /*
      var product_comp = $(`#${data.rand_id}_svi_kontakti_dobavljac_box`).closest('.product_comp');
      var product_comp_id  = null
      var product_data = null;
      
      if ( product_comp.length > 0 ) {
        product_comp_id = product_comp[0].id;
        product_data = this_module.product_module.cit_data[product_comp_id];
      };
      
      */
      
      // $.each()
      // $(`#`+rand_id+`_doc_adresa`).data('cit_props').list = $(`#${data.rand_id}_svi_kontakti_dobavljac_box`).data(`cit_result`);
      
    }, 5*1000); 

    
  };
  this_module.make_dobav_kontaki_in_status = make_dobav_kontaki_in_status;
  
  
  function remove_header_arrows_and_blink(data) {
    
    function condition_wait_list_to_generate() {
      var list_generated = false;
      if ( 
        $(`#${data.rand_id}_statuses_box .header_column`).length > 0 // ako je project/product page
        ||
        $(`#sirov_module_statuses_box .header_column`).length > 0 // ako je sirov page
        ||
        $(`#partner_module_statuses_box .header_column`).length > 0 // ako je partner page
      ) { 
        
        list_generated = true; 
        
      };
      
      return list_generated;
    };
    
    // čekaj da se pojavi tablica  -- ovdje sam arbitratno uzeo da se pojavi header tako da znam ne je se nesto pojavilo :) 
    wait_for(condition_wait_list_to_generate, function() {

      // remove_sort_arrows( $(`#${data.rand_id}_statuses_box`) );
      
      if ( data.for_module == `statuses_page` ) remove_list_buttons( $(`#${data.rand_id}_statuses_box`) );

      // ALI POSTOJI GOTO STATUS
      if ( data.goto_status ) {

        var status_row = null;
        var cit_panel = null;
        var status_tab = null;
        
        // ovaj loop je kada se status nalazi u productu / projectu
        $(`#${data.rand_id}_statuses_box .search_result_table .search_result_row`).each( function() {
          if ( this.id.indexOf( data.goto_status ) > -1 ) {
            
            status_row = $(this);
            cit_panel = status_row.closest(`.cit_panel`);
            
            // status_tab = cit_panel.find(`.cit_tab_btn`).eq(2);
            
            cit_panel.find(`.cit_tab_btn`).each( function() {
              if ( this.id.indexOf(`cit_tab_btn_statusi`) > -1 ) status_tab = $(this);
            });
            
            
          };
        });
        
        
        // ovaj loop je kada se status nalazi u SIROVINI !!!!
        $(`#sirov_module_statuses_box .search_result_table .search_result_row`).each( function() {
          if ( this.id.indexOf( data.goto_status ) > -1 ) {
            status_row = $(this);
            cit_panel = status_row.closest(`#sirov_module`)
            // status_tab = cit_panel.find(`.cit_tab_btn`).eq(2);
            status_tab = $(`#sirov_tab_statusi`);
          };
        });
        
        
        // ovaj loop je kada se status nalazi PARTNERU !!!!
        $(`#partner_module_statuses_box .search_result_table .search_result_row`).each( function() {
          if ( this.id.indexOf( data.goto_status ) > -1 ) {
            status_row = $(this);
            cit_panel = status_row.closest(`#partner_module`)
            // status_tab = cit_panel.find(`.cit_tab_btn`).eq(3);
            
            status_tab = $(`#partner_tab_statusi`);
          };
        });
        
        
        
        

        if ( status_row ) {
          
          // otvori tab za statuse !!!!
          status_tab.trigger(`click`);
          
          setTimeout( function() {
            var scroll_top = status_row.offset().top;
            $(`html`)[0].scrollTop = scroll_top - 200;
          }, 200);
          
          setTimeout( function() { status_row.addClass(`blink_bg`);  }, 500);
          setTimeout( function() { status_row.removeClass(`blink_bg`);  }, 2500);
        };
        
        
        

      }; // kraj ako postoji goto status


    }, 5*1000); // kraj wait if da da li je lista statusa generirana u html-u
    
    
  };
  this_module.remove_header_arrows_and_blink = remove_header_arrows_and_blink;


  function make_work_list(data, return_html) {

    var new_work_list = [];
    
    var curr_status = return_html ? data : data.current_status;

    if ( curr_status.work_times && curr_status.work_times.length > 0 ) {

      $.each( curr_status.work_times, function(index, work) {

        var { 
          worker,
          work_cat,
          duration,
          work_from_time,
          work_to_time,
          desc,
          time,
          
        } = work;

        var worker_naziv = worker?.full_name || "";
        var posao_naziv = work_cat.naziv;
        var time_string = time ? cit_dt( time ).date_time : null;
        var from_string = work_from_time ? cit_dt( work_from_time ).date_time : null;
        var to_string = work_to_time ? cit_dt( work_to_time ).date_time : null;
        
        var duration_string = duration ? cit_round(duration , 2) : null;
        
        new_work_list.push( { worker_naziv, posao_naziv, duration: duration_string, desc, time, time_string, from_string, to_string });

      });
    };

    var work_props = {
      desc: 'samo za kreiranje tablice svih zapisa rada tj work times unutar current statusa',
      local: true,
      list: new_work_list,
      show_cols: [ 'worker_naziv', 'time_string', 'posao_naziv', 'from_string', 'to_string', 'duration', 'desc' ],
      custom_headers: [ 'Radnik', 'Vrijeme upisa', 'Vrsta', 'OD', 'DO', 'Trajanje / min', 'Opis' ],
      format_cols: {
        worker_naziv: "center",
        time_string: "center",
        from_string: "center",
        to_string: "center",
        posao_naziv: "center",
        duration: "center",
      },
      col_widths: [1, 1, 1, 1, 1, 1, 4],
      
      parent: return_html || `#${data.rand_id}_status_work_list_box`,
      return: {},
      show_on_click: false,
      cit_run: function(state) { console.log(state); },

    };
    
    
    
    if ( new_work_list.length > 0 ) {
      
      if ( return_html ) {
        return create_cit_result_list(new_work_list, work_props );
      } else {
        create_cit_result_list(new_work_list, work_props );
      };
      
    };
   
  };
  this_module.make_work_list = make_work_list;
  
  

  
  async function save_current_status() {

    var this_button = this;

    return new Promise( async function( resolve, reject ) {


      var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      
      if ( !data.current_status?.status_tip ) {
        popup_warn(`Niste izabrali niti jedan status!!!`);
        return;
      };
      

      var comp_id = rand_id+`_status_docs`;

      toggle_global_progress_bar(true);

      $(`#${rand_id}_save_status`).css("pointer-events", "none");

      var product_comp = $(this_button).closest('.product_comp');

      if ( window.location.hash.indexOf(`#production`) > -1 ) {
        // ako se nalazi na production page onda product component nije parent nego je zapravo sibiling
        // I JEDAN JEDINI PRODUCT JE NA TOJ STRANICI ZA PRODUCTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        product_comp = $(`body .product_comp`);
      };

      var product_comp_id  = null
      var product_data = null;

      if ( product_comp.length > 0 ) {
        product_comp_id = product_comp[0].id;
        product_data = this_module.product_module.cit_data[product_comp_id];
      };

      
      var proj_comp = $(this_button).closest('.cit_comp.project_comp');
      var proj_comp_id = null
      var proj_data = null;

      if ( proj_comp.length > 0 ) {
        proj_comp_id = proj_comp[0].id;
        proj_data = this_module.product_module.cit_data[proj_comp_id];
      };
      

      // ako ovaj status VEĆ IMA SVE OVE PODATKE ONDA IH SAMO PREPIŠI !!!!!
      // ako ih nema onda ih zapiši kao NULL
      data.current_status.rok_isporuke = data.current_status.rok_isporuke || null;
      data.current_status.potrebno_dana_proiz = data.current_status.potrebno_dana_proiz || null;
      data.current_status.potrebno_dana_isporuka = data.current_status.potrebno_dana_isporuka || null;
      data.current_status.rok_proiz = data.current_status.rok_proiz || null;
      data.current_status.rok_za_def = data.current_status.rok_za_def || null;
      

      // ako status već ima sve ove podatke onda ih uzmi !!!
      // ako nema te podatke onda ih probaj prekopirati iz HTML-a tj iz data- atributa !!
      var product_id = data.current_status?.product_id || product_comp?.attr('data-product_id');
      var product_tip = data.current_status?.product_tip || product_comp?.attr('data-product_tip');
      var proj_sifra = data.current_status?.proj_sifra || product_comp?.attr('data-proj_sifra');
      var item_sifra = data.current_status?.item_sifra || product_comp?.attr('data-item_sifra');
      var variant = data.current_status?.variant || product_comp?.attr('data-variant');

      
      if ( data.for_module == `product` ) {

        if ( !product_id ) {
          popup_warn(`Prvo morate spremiti ovaj proizvod !!!`);
          toggle_global_progress_bar(false);

          $(`#${rand_id}_save_status`).css("pointer-events", "all");
          reject(null);
          return;
          
        };

        data.current_status.rok_isporuke = product_data.rok_isporuke || null;
        data.current_status.potrebno_dana_proiz = product_data.potrebno_dana_proiz || null;
        data.current_status.potrebno_dana_isporuka = product_data.potrebno_dana_isporuka || null;
        data.current_status.rok_proiz = product_data.rok_proiz || null;
        data.current_status.rok_za_def = product_data.rok_za_def || null;
        
      }; // kraj ako je je ovo u modulu product

      
      
      data.current_status.product_id = product_id || null;
      data.current_status.product_tip = product_tip || null;
      data.current_status.proj_sifra = proj_sifra || null;
      data.current_status.item_sifra = item_sifra || null;
      data.current_status.variant = variant || null;


      // ---------------------------------------- TRAŽI KUPCA OD OVOG STATUSA / PROJEKTA ----------------------------------------
      var kupac_statusa = null;
      if ( data.current_status.proj_sifra && this_button.id.indexOf(`save_status`) > -1 ) {

        kupac_statusa = await this_module.get_status_kupac(data.current_status.proj_sifra);

        if ( kupac_statusa  ) {
          data.current_status.kupac_id = kupac_statusa._id;
          data.current_status.kupac_naziv = kupac_statusa.naziv;
        };
      };
      // ----------------------------------------  TRAŽI KUPCA OD OVOG STATUSA / PROJEKTA ----------------------------------------
      

      // vrati nazad da gumb za save dokumenata  bude vidljiv
      // ionako ću sakriti parent i cijelu listu obrisati
      $(`#${comp_id}_upload_btn`).css(`display`, `flex`);

      // obriši listu uploadanih dokumenata
      $(`#${comp_id}_list_items`).html( "" );
      // sakrij listu i button
      $(`#${comp_id}_list_box`).css( `display`, `none` );


      if ( !data.current_status ) data.current_status = {};

      data.current_status.product_id = product_id || null;
      data.current_status.product_tip = product_tip || null;
      data.current_status.proj_sifra = proj_sifra || null;
      data.current_status.item_sifra = item_sifra || null;
      data.current_status.variant = variant || null;


      // POŠILJATELJ MORA UVIJEK BITI ULOGIRANI USER !!!
      data.current_status.from = {
        _id: window.cit_user._id,
        name: window.cit_user.name,
        user_number: window.cit_user.user_number,
        full_name: window.cit_user.full_name,
        email: window.cit_user.email,
      };

      data.current_status.dep_from = cit_deep( window.cit_user.dep );


      // stavi SEEN = null i stavi REPLY FOR = null  samo ako je novi status !!!
      if ( this_button.id.indexOf(`save_status`) > -1 ) {

        data.current_status.seen = null;
        data.current_status.reply_for = null;
        data.current_status.responded = null;

        data.current_status.work_finished = null;

        if ( !data.current_status.elem_parent )  {
          data.current_status.elem_parent = null;
          data.current_status.elem_parent_object = null;
        };

        // ako je user izabrao sirovinu za naručiti ----->  ONDA MORA UPISATI I KOLIČINU !!!!

        // ZA SADA ODUSTAJEM OD OVOG UVIJETA 

        /*
        if ( data.current_status.sirov && !data.current_status.order_sirov_count ) {
          popup_warn(`Ako je izabrana sirovina - obavezno upisati željenu količinu!`);

          toggle_global_progress_bar(false);

          $(`#${rand_id}_save_status`).css("pointer-events", "all");

          reject(null);
          return;
        };
        */

        if ( !data.current_status.sirov ) data.current_status.sirov = null;

        if ( !data.current_status.order_sirov_count ) data.current_status.order_sirov_count = null;


      };

      // ako je REPLY kopiraj parent od trenutnog status:
      if ( this_button.id.indexOf(`reply`) > -1 ) {

        //  VAŽNO !!!!! ------>  ako trenutni status nema parent stavi parent da bude poruka na koju odgovaram
        // na taj način uvijek početni status od neke grane će biti kopiran na svaki sub-status


        /*
        if ( !data.current_status.elem_parent ) {

          data.current_status.elem_parent = data.current_status.sifra;
          data.current_status.elem_parent_object = cit_deep(data.current_status);

        };
        */

        // ako je ovo odgovor na radni status onda NEMOJ !!!! upisati property reply_for
        // umjesto toga upiši done_for_status property

        if ( data.current_status.status_tip.work_done_for !== null ) {

          // ------------------------------------------------------------------------------------------
          //       ODGOVOR NA RADNI STATUS MOŽE SAMO ODGOVORITI USER KOJI GA JE NAPRAVIO !!!
          // ------------------------------------------------------------------------------------------


          data.current_status.reply_for = null;

          // prvo nađi parent status object 
          $.each(data.statuses, function(p_ind, parent) {
            if ( data.current_status.elem_parent == parent.sifra ) parent_status = parent;
          });

          // vrti po svim statusima u trenutnom productu
          $.each(data.statuses, function( c_ind, child_status) {

              if ( 

                /*mora biti child od tog parenta */
                child_status.elem_parent == parent_status.sifra 
                &&
                /* i ako je tip statusa od child jednak završnom poslu (work_done_for) u trenutnom statusu */
                data.current_status.status_tip.work_done_for == child_status.status_tip.sifra
                /* ALI SAMO AKO DO SADA NIJE UPISANO DA JE TAJ POSAO GOTOV tj nije upisano vrijeme kraja !!! */
                &&
                !data.statuses[c_ind].end

              ) {

                data.current_status.done_for_status = child_status.sifra;
                data.statuses[c_ind].end = Date.now();

              };

          });


          // kraj ako se radi o radnom statusu
        } 
        else {

          // ------------------------------------------------------------------------------------------
          //       ODGOVOR NA OBIČNI STATUS
          // ------------------------------------------------------------------------------------------

          // ako nije radni status onda nemoj upisivati bilo koju šifru
          data.current_status.done_for_status = null;

          // ako je replay onda uvijek zapiši na sifru na koji to status odgovaram
          data.current_status.reply_for = data.current_status.sifra;

          // samo lokalno upiši update na statusu na koji odgovoram
          // IONAKO ĆE SE TO NA SERVERU UPISATI U BAZU ----> ovo je samo da se lokalno vidi odmah ----> razlika će biti mala u par milisekundi :)
          $.each(data.statuses, function( s_ind, status ) {
            if ( status.sifra == data.current_status.reply_for ) {
              data.statuses[s_ind].end = Date.now();
            };
          });

        };
        // kraj else tj. ako NIJE RADNI STATUS !!!


      }; // kraj ako je kliknut gumb REPLY


      // ako ne postoji sifra statusa onda je napravi !!!!
      if ( !data.current_status.sifra ) {

        data.current_status.sifra = `status`+cit_rand();
        // prekopiraj sifru u status jer je to isto, ali kasnije mi treba da mi bude lakše s routerom !!!!
        // prekopiraj sifru u status jer je to isto, ali kasnije mi treba da mi bude lakše s routerom !!!!
        // prekopiraj sifru u status jer je to isto, ali kasnije mi treba da mi bude lakše s routerom !!!!
        data.current_status.status = data.current_status.sifra;

      };

      // ako već postoji time onda ga nemoj više dirati
      data.current_status.time = data.current_status.time || Date.now(); 

      // TODO KASNIJE DODATI I DRUGE KATEGORIJE kao npr "sale" ----> to znači aktivna prodaja - kao npr cold calls, newsletter follow up  i slično

      // ---------------------- REPLY ------------------------
      if ( this_button.id.indexOf(`reply`) > -1 ) {


        // ako je REPLY onda svakako stavi novu sifru !!!!
        // ako je REPLY onda svakako stavi novu sifru !!!!
        // ako je REPLY onda svakako stavi novu sifru !!!!


        data.current_status.sifra = `status`+cit_rand();
        data.current_status.status = data.current_status.sifra;


        // REPLAY OBAVEZNO NOVO VRIJEME !!!!!
        data.current_status.time = Date.now(); 
        // VAŽNO !!!!!! potrebno je delete postojeci _id ako je REPLY
        // VAŽNO !!!!!! potrebno je delete postojeci _id ako je REPLY
        // TAKO DA NAPRAVI NOVI STATUS
        if ( data.current_status._id ) delete data.current_status._id;
        if ( data.current_status.status_id ) delete data.current_status.status_id;

        if ( data.current_status.status_tip.sifra == data.current_status.old_status_tip.sifra ) {
          popup_warn(`Status odgovora je ili prazan ili je ostao isti !!!`);
          toggle_global_progress_bar(false);
          $(`#${rand_id}_save_status`).css("pointer-events", "all");

          reject(null);
          return;
        };

      };
      // ------------------------ REPLY ------------------------

  
      /*
        { "sifra": "preprod", "naziv": "PRED-PROIZVODNJA" },
        { "sifra": "nabava", "naziv": "REGULARNA NABAVA" },
        { "sifra": "prod", "naziv": "PROIZVODNJA" },
      */
      if ( data.current_status.status_tip?.cat?.sifra == `nabava` && window.location.hash.indexOf(`#sirov/`) > -1 ) {

        if ( !data.current_status.sirov ) {
          popup_warn(`Ako birate status unutar modula SIROVINE, a ne za pred-proizvodnju onda OBAVEZNO morate izabrati sirovinu!`);
          reject(null);
          return;
        };

        var sirov_id = data.current_status?.sirov ? data.current_status.sirov._id : "";
        // ako enma sirovine u trenutnom statusu onda pogledaj glavni id od sirovina page
        if ( !sirov_id ) sirov_id = data._id || null;
        
        if ( !sirov_id ) {
          popup_warn(`Nedostaje ID od sirovine / robe !`);
          reject(null);
          return;
        };

        data.current_status.link = sirov_id ? `#sirov/${sirov_id}/status/${data.current_status.sifra}` : `#`; // ako je reply onda sam malo iznad upisao novu sifru

        // VAŽNO ----> U OVOM SLUČAJU TO NIJE PRODUCT VEĆ SIROVINA !!!!!
        data.current_status.full_product_name = `SIROVINA`;
        // ako imam full naziv sirovine onda upiši taj full naziv

        if ( data.current_status.sirov?.full_naziv ) {

          data.current_status.full_product_name = 
            ( data.current_status.sirov.sirovina_sifra + "--" + data.current_status.sirov.full_naziv  + "--" + (data.current_status.sirov.dobavljac?.naziv || "")   );
        };

      };

      // ako nije update 
      // tj ako je reply ili ako je new
      if ( this_button.id.indexOf(`update_`) == -1 ) {

        // ako se nalazim na prodon page onda obavezno uzmi
        if ( 

          window.location.hash.indexOf(`#production/`) > -1 // ako sam na page PROIZVODNJA
          ||
          ( data.current_status.elem_parent_object && data.current_status.elem_parent_object.link.indexOf(`#production/`) > -1 ) // ILI AKO PARENT OD STATUSA IMA LINK OD PROIZVODNJE
          ||
          ( data.current_status.link && data.current_status.link.indexOf(`#production/`) > -1 ) // ILI AKO LINK OD CURRENT STATUS VODI NA PROIZVODNJU

        ) {

          // ako je production uvijek uzmi link od radnog procesa odnosno parenta
          // ako je production uvijek uzmi link od radnog procesa odnosno parenta !!!!
          var sifra_statusa = data.current_status.elem_parent ? data.current_status.elem_parent : data.current_status.sifra;
          data.current_status.link = `#production/${ sifra_statusa }/proces/${ data.current_status.proces_id || null }`;

        } else if (  window.location.hash.indexOf(`#project/`) > -1  ) {

          data.current_status.link =  `#project/${data.proj_sifra}/item/${data.item_sifra}/variant/${data.variant}/status/${data.current_status.sifra}`; 

        } else if ( window.location.hash.indexOf(`#partner/`) > -1 ) {
          
          
          var partner_comp_id = $("#save_partner_btn").closest('.partner_comp.cit_comp')[0].id;
          var partner_module = await get_cit_module(`/modules/partners/partner_module.js`, `load_css`);
          var partner_data = partner_module.cit_data[partner_comp_id];
          
          data.current_status.link =  `#partner/${partner_data._id}/status/${data.current_status.sifra}`; 
          
        };

      };

      // ako nije prozivodnja onda je start ODMAH  !!!!
      if ( 
        data.current_status.status_tip.cat?.sifra !== "prod"
        ||
        data.current_status.old_status_tip?.cat?.sifra !== "prod"
        
      ) data.current_status.start = data.current_status.time;

      data.current_status.komentar = $('#'+rand_id+`_status_komentar`).val();
      
      data.current_status.komentar = data.current_status.komentar || "";

      if ( 
        !data.current_status.status_tip    
        ||
        !data.current_status.dep_to 
        ||
        !data.current_status.to 
      ) {

        var popup_text = `Potrebno je upisati minimalno:<br>novi status, primatelja i odjel primatelja.<br>Ako je radni status morate upisati procjenu sati rada !!!`;

        popup_warn(popup_text);
        toggle_global_progress_bar(false);
        $(`#${rand_id}_save_status`).css("pointer-events", "all");

        reject(null);
        return;

      };


      // ako je is work onda dodaj na kraju text da mora upisati procjenu vremena !!!

      if ( data.current_status.status_tip?.is_work && !data.current_status.est_deadline_hours ) {

        popup_warn(`Za radne statuse potrebno je upisati i procjenu vremena !!!`);
        toggle_global_progress_bar(false);

        $(`#${rand_id}_save_status`).css("pointer-events", "all");

        reject(null);
        return;

      };


      var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);

      var db_product = null;

      if ( product_id && data.for_module == `product` ) {
        var db_product = await this_module.get_db_product(product_id);
        if ( db_product ) data.current_status.full_product_name = product_module.generate_full_product_name(db_product);
      };


      // ako fale ovi propertiji tj ako su undefined  -----> onda ih DODAJ DA BUDU NULL
      // ako fale ovi propertiji tj ako su undefined  -----> onda ih DODAJ DA BUDU NULL
      if ( !data.current_status.docs ) data.current_status.docs = null;
      if ( !data.current_status.work_times ) data.current_status.work_times = null;
      if ( !data.current_status.est_deadline_hours ) data.current_status.est_deadline_hours = null;
      if ( !data.current_status.est_deadline_date ) data.current_status.est_deadline_date = null;
      if ( !data.current_status.end ) data.current_status.end = null;


      // ako uopće nema propertija elem parent onda postavi oba propsa na null
      if ( !data.current_status.elem_parent ) {
        data.current_status.elem_parent = null;
        data.current_status.elem_parent_object = null;
      };


      $.each( data.current_status, function( key, value ) {
        // ako je objekt ili array onda ga flataj
        if ( $.isPlainObject(value) || $.isArray(value) ) {
          data.current_status[key+'_flat'] = JSON.stringify(value);
        };
      });

      var result = null;
      var Model_name = null;

      
      
      // ------------------------------ STATUS ZA PRODUCT ILI PRODUCTION  ------------------------------
      if ( window.location.hash.indexOf(`#production/`) > -1 ||  window.location.hash.indexOf(`#project/`) > -1) {
        data.current_status.product_model_id = product_id; 
        Model_name = "Product";
        result = await this_module.ajax_save_status( data, "Product", data.current_status?.product_id || null );
      };
      
      // ------------------------------ STATUS ZA SIROV ------------------------------
      if ( window.location.hash.indexOf(`#sirov/`) > -1 ) {
        data.current_status.sirov_model_id = data._id; 
        Model_name = "Sirov";
        result = await this_module.ajax_save_status(data, "Sirov", data._id || null );
      };
      
      
       // ------------------------------ STATUS ZA PARTNER  ------------------------------
      if ( window.location.hash.indexOf(`#partner/`) > -1 ) {
        data.current_status.partner_model_id = data._id; 
        Model_name = "Partner";
        result = await this_module.ajax_save_status(data, "Partner", data._id || null );
      };
      
      

      console.log(` ------------------------- RESULT SPREMANJA STATUSA :  ------------------------------`);
      console.log(result);

      if ( result?.success == true ) {

        
        data.current_status._id = result.status._id;
        data.current_status.status_id = result.status._id;
        
        
        data.current_status.branch_done = result.status.branch_done;
        data.statuses = upsert_item(data.statuses, data.current_status, `sifra`);
        
        
        var saved_status = await save_status_in_all_modules(this_button, result, Model_name );
        
        
        resolve(result.status);

        cit_toast(`STATUS JE SPREMLJENI!`);


        // ---------------------------------------- samo da ovo NIJE production page !!!!!!!
        // ---------------------------------------- samo da ovo NIJE production page !!!!!!!
        
        if ( window.location.hash.indexOf(`#production/`) == -1 ) {

          // kada NISAM NA PRODUCTION -----> promijeni hash tj url !!!
          var hash_data = {
            project: data.current_status.proj_sifra,
            item: (data.current_status.item_sifra || null),
            variant: (data.current_status.variant || null),
            status: (data.current_status.status || null),
          };
          

          // ali ako je ovo status unutar sirovine 
          if ( window.location.hash.indexOf(`#sirov/`) > - 1 ) {

            hash_data = {
              sirov: data._id,
              status: data.current_status.status,
            };

          };
          
          
          // ali ako je ovo status unutar sirovine 
          if ( window.location.hash.indexOf(`#partner/`) > - 1 ) {

            hash_data = {
              partner: data._id,
              status: data.current_status.status,
            };
            
            
            setTimeout(function () {
              $(`#save_partner_btn`).trigger(`click`);
            }, 300);
            

          };
          
          update_hash(hash_data);

        }; // kraj ako nije production page !!!!
        
        

        // ako je update_ NEMOJ SLATI SOCKET jer update služi samo za editiranje STATUSA
        // ako je update_ NEMOJ SLATI SOCKET jer update služi samo za editiranje STATUSA
        // ako je update_ NEMOJ SLATI SOCKET jer update služi samo za editiranje STATUSA

        // dakle ako je NEW ili ako je REPLY ---> onda šalji socket !!!!!
        if ( this_button.id.indexOf(`update_`) == -1 ) {

          cit_toast(`Poslan status prema ${data.current_status.to.full_name}`, `background: #3f6ad8;`);
          cit_socket.emit('new_status', data.current_status );
          animate_badge( $(`#cit_task_from_me`), 1 );

        };

        this_module.make_status_list(data);
        this_module.remove_header_arrows_and_blink(data);

      } 
      else if ( !result || result?.success ) {

        // ako result NIJE SUCCESS = true

        if ( result.msg ) popup_error(result.msg);
        if ( !result.msg ) popup_error(`Greška prilikom spremanja STATUSA!`);

      };

      this_module.reset_status_GUI(null, this_button);


    }); // KRAJ PROMISA !!!!

  };
  this_module.save_current_status = save_current_status;


  
  function reset_status_GUI(e, arg_button) {
    
    var this_button = arg_button || this;

    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    
    toggle_global_progress_bar(false);

    // ------------!!!!---------------- ako current status ima parent koji je radni nalog ---------------------------- 
    // ------------!!!!---------------- ili ako je current status zapravo taj parent radni nalog ----------------------------
    if (

      (
        data.current_status.elem_parent_object?.status_tip?.sifra == `IS16430378755748118` /* AKO JE PARENT RADNI NALOG !!! */ 
        ||
        data.current_status.status_tip?.sifra == `IS16430378755748118` /* AKO JE OVAJ SAM STATUS RADNI NALOG !!! */ 
      )
      &&
      window.location.hash.indexOf(`#production`) > -1

    ) {
      // vrati nazad da je status inicijalni STATUS OD RADNOG NALOGA
      data.current_status = window.curr_prodon_status_temp;
    } 
    else {
      data.current_status = null;
    };
    

    $('#'+rand_id+`_status_tip`).val("");
    $('#'+rand_id+`_dep_to`).val("");
    $('#'+rand_id+`_status_to`).val("");
    $('#'+rand_id+`_tezina`).val("");

    $('#'+rand_id+`_est_deadline_hours`).val("");
    $('#'+rand_id+`_est_deadline_date`).val("");
    $('#'+rand_id+`_work_time_sum`).val("");
    $('#'+rand_id+`_add_work_time`).val("");
    $('#'+rand_id+`_status_komentar`).val("");

    // ako je bilo koji gumb osim SAVE ----> SAKRIJ TAJ GUMB I PRIKAŽI SAVE
    
    $(`#${rand_id}_quit_status`).css(`display`, `none`);
    $(`#${rand_id}_reply_status`).css(`display`, `none`);
    $(`#${rand_id}_update_status`).css(`display`, `none`);
    $(`#${rand_id}_save_status`).css(`display`, `flex`);

    $(`#${rand_id}_save_status`).css("pointer-events", "all");
    
    function enable_input(input_id) {

      var dont_enable = [
          "_work_time_sum",
          "_tezina",
          "_est_deadline_date",
        ];

      var remove_disabled = true;
      // ako se input id nalazi u dont_enable arraju onda ga nemoj omogućiti
      $.each(dont_enable, function (ind, array_id) {
        if (input_id.indexOf(array_id) > -1) remove_disabled = false;
      });

      // OVERRIDE !!!!!!
      // ako nije RADMILA ILI TONI (super admini) onda NE MOŽEŠ upisivati težinu 
      if ( !window.cit_admin && input_id.indexOf(`_tezina`) > -1 ) {
        remove_disabled = false;
      };

      return remove_disabled;
    };

    // prvo enable sve inpute u status sučelju
    $('#'+data.id+` input`).each( function() {
      // neke inpute nemoj enable
      if ( enable_input( this.id ) ) $(this).prop( "disabled", false ); // ovo je zapravo enable
    });

    // osposobi komentar
    $('#'+data.id+` textarea`).prop( "disabled", false );

    // osposbi upload docs
    $(`label[for='${rand_id}_status_docs']`).removeClass(`cit_disable`);


    $('#'+rand_id+`_prodon_work_desc`).val(``);
    
    $(`#${data.rand_id}_status_work_list_box`).html(``);
    $(`#${data.rand_id}_rn_files_list_box`).html(``);
    
    
  };
  this_module.reset_status_GUI = reset_status_GUI;
  
  function ajax_save_status(data, Model_name, Model_id ) {
  
    
    toggle_global_progress_bar(true);
    
    var current_status_copy = cit_deep(data.current_status);
    // spremi samo _id od kalkulacije !!!! A  NE CIJELU KAKULACIJU !!!!!
    if ( current_status_copy.kalk ) current_status_copy.kalk = current_status_copy.kalk._id;
    
    
    return new Promise( function(resolve, reject) {
    
      // ----------------------------------------------------------------------------------------------------------------
      // START SAVE TO DB
      // ----------------------------------------------------------------------------------------------------------------

      $.ajax({
        headers: {
          'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
        },
        type: "POST",
        cache: false,
        url: `/save_status`,
        data: JSON.stringify( {
          ...current_status_copy,
          Model_name: Model_name || null,
          Model_id: Model_id || null,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done(function (result) {
        resolve(result);
        
      })
      .fail(function (error) {

        console.log(error);
        popup_error(`Došlo je do greške na serveru prilikom spremanja!`);
        resolve(null);

      })
      .always(function() {

        toggle_global_progress_bar(false);

      });

      // ----------------------------------------------------------------------------------------------------------------
      // END SAVE TO DB
      // ----------------------------------------------------------------------------------------------------------------

    
    }); // kraj promisa
    
    
  };
  this_module.ajax_save_status = ajax_save_status;
  
  
  
  
  
  async function make_sirov_in_status(state, arg_data) {
    
    console.log(state);
   
    var this_comp_id = null;
    var data = null;
    var rand_id = null;
    
    if ( !arg_data ) {
      this_comp_id = $('#'+current_input_id).closest('.cit_comp.status_comp')[0].id;
      data = this_module.cit_data[this_comp_id];
      rand_id =  this_module.cit_data[this_comp_id].rand_id;
    } else {
    
      this_comp_id = $(`#${arg_data.rand_id}_find_sirov_in_status`).closest('.cit_comp')[0].id;
      data = this_module.cit_data[this_comp_id];
      rand_id = data.rand_id;
    };
    
    // resetiraj link za sirovinu
    $("#"+data.rand_id+"_link_to_sirov").attr(`href`, `#`);

    if ( state == null || typeof state == `undefined` ) {
      
      if ( !data.current_status ) data.current_status = {};
      data.current_status.sirov = null;
      
      $("#"+data.rand_id+"_find_sirov_in_status").find(".single_select").val("");
      
      return;
    };

    var new_sirovina_input_text = ( state.sirovina_sifra + "--" + state.full_naziv + "--" + state.dobavljac?.naziv );
    
    $("#"+data.rand_id+"_find_sirov_in_status").find(".single_select").val( new_sirovina_input_text );

  
    if ( !data.current_status ) data.current_status = {};
    
    data.current_status.sirov = {
      _id: state._id,
      sirovina_sifra: state.sirovina_sifra,
      naziv: state.naziv,
      full_naziv: state.full_naziv,
      dobavljac: {
        
         _id: state.dobavljac._id || null,
        partner_sifra: state.dobavljac.partner_sifra || null,
        naziv: state.dobavljac.naziv || null, 
        kontakti: state.dobavljac.kontakti || null,

        
      },
      
    };

      
    $('#'+rand_id+`_order_sirov_count`).val( 
      data.current_status.order_sirov_count ? cit_format( data.current_status.order_sirov_count , this_module.valid.order_sirov_count.decimals ) : null
    );

    
    if ( data.for_module == `product` ) {
      this_module.make_dobav_kontaki_in_status(state, data);
    };    
    
    if ( state._id ) $("#"+rand_id+"_link_to_sirov").attr(`href`, `#sirov/${state._id}/status/null`);
    
    console.log(data);
    
  };
  this_module.make_sirov_in_status = make_sirov_in_status;
  

  async function get_db_product(product_id) {
    
    return new Promise( async function(resolve, reject) {

      var props = {
        filter: null,
        url: '/find_product',
        query: { _id: product_id },
        desc: `UNUTAR STATUS MODULA ------> pronadji u bazi product u kojem se nalazi ovaj status !!!`,
      };
      
      var db_product = null;
      
      try {
        db_product = await search_database(null, props );
      } catch (err) {
        console.log(err);
        reject(null);
      }; 

      // ako je našao nešto onda uzmi samo prvi član u arraju
      if ( db_product?.length > 0 ) {
        db_product = db_product[0];
      };

      resolve(db_product);
      
    }) // kraj promisa
    
    
    
  };
  this_module.get_db_product = get_db_product;



  async function get_product_module() {
    this_module.product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
  };
  this_module.get_product_module = get_product_module;

  get_product_module();
  
  
  
  function auto_select_sirov(data) {
    
    // ako koristim status modul unutar SIROV modula onda odmah na raspolaganju imam podatke dobavljača !!!!!!
    // ako koristim status modul unutar SIROV modula onda odmah na raspolaganju imam podatke dobavljača !!!!!!

    var sirov_comp_id = $('#'+current_input_id).closest('.sirovine_body.cit_comp')[0].id;
    var sirov_data = this_module.sirov_module.cit_data[sirov_comp_id];

    if ( !data.current_status ) data.current_status = {};

    // ako postoji neki _id to jest ako sam otvorio neku od sirovina
    // ( može se dogoditi da user odabere status tip na praznj kartici sirov modula )

    if ( sirov_data?._id ) {

      data.current_status.sirov = {
        
        _id: sirov_data._id,
        sirovina_sifra: sirov_data.sirovina_sifra,
        naziv: sirov_data.naziv,
        full_naziv: sirov_data.full_naziv,
        dobavljac: {
          
          _id: sirov_data.dobavljac._id || null,
          partner_sifra: sirov_data.dobavljac.partner_sifra || null,
          naziv: sirov_data.dobavljac.naziv || null, 
          kontakti: sirov_data.dobavljac.kontakti || null,
          
        },
        
        
      };

      var sirov_naziv_for_input = sirov_data.sirovina_sifra + "--" + sirov_data.full_naziv + "--" + sirov_data.dobavljac?.naziv;
      $("#"+data.rand_id+"_find_sirov_in_status").find(".single_select").val(sirov_naziv_for_input);

      this_module.make_dobav_kontaki_in_status(data.current_status.sirov, data);

    };

    
  };
  this_module.auto_select_sirov = auto_select_sirov;
  
  
  function show_choose_sirov_box(data) {
    
    // IS1634121913167369 -----> ZAHTJEV NABAVI ZA SIROVINU !!!

    // ako je izabrani status zahtjev za nabavu
    // ili ako je prethodni status zahjev za nabavu !!!!
    // ili ako je PARENT STATUS imao tip ZAHTJEV ZA NABAVU

    // ili ako je status unutar modula sirovina

    var parent_status = null; 

    // prvo nađi parent status object 
    $.each(data.statuses, function(ind, parent) {
      if ( data.current_status.elem_parent == parent.sifra ) parent_status = parent;
    });


    
    function status_tip_for_show_sirovina_box(data, parent_status) {
      
      var status_tipovi_for_sirovina_box = [
        `IS1634121913167369`, // zahtjev za nabavu sirovine
        `IS16408640642602544`, // ZAHTJEV ZA IZRADU ALATA
        `IS16408758411607424`, // VANJSKI TISAK NA PROIZVODU - POTREBNO NARUČITI
      ];
      
      
      var show_sirovina_box = false;
      
      $.each( status_tipovi_for_sirovina_box, function( st_ind, status_tip_sifra ) {
        
        
        if ( show_sirovina_box == false ) {
          // vrti ovaj each samo dok show bude false  -----> čim je true stani sa loop

          if ( 
            data.current_status.status_tip.sifra == status_tip_sifra // AKO JE CURRENT IZABRANI STATUS == status tip sifra
            ||
            data.current_status.old_status_tip?.sifra == status_tip_sifra // AKO JE STARI IZABRANI STATUS == status tip sifra
            ||
            ( parent_status && parent_status.status_tip?.sifra == status_tip_sifra ) // ili ako je parent imao status == status tip sifra
          ) {
            show_sirovina_box = true;
          };
        
        };
        
      });
      
      
      
      return show_sirovina_box;
      
    };
    

    if ( 

     status_tip_for_show_sirovina_box(data, parent_status) == true
      
      ||
      
      data.for_module == `sirov` // -----> ako je ovo unutar modula za sirovine ONDA UVIJEK PRIKAŽI POLJE ZA SIROVINE !!!!
      ||
      
      data.for_module == `partner` // -----> ako je ovo unutar modula za PARTNER ONDA UVIJEK PRIKAŽI POLJE ZA SIROVINE !!!!
      

    ) {

      $('#'+data.rand_id+'_choose_sirovina_box').css(`display`, `flex`);

    } else {

      $('#'+data.rand_id+'_choose_sirovina_box').css(`display`, `none`);
    };
    
    
  };
  this_module.show_choose_sirov_box = show_choose_sirov_box;
  
  
  function show_worker_error_box(data) {
    
    var worker_errors_arr = [
      `IS16456245409441008`, // LJUDSKA GREŠKA - POTREBNA DODATNA EDUKACIJA
      `IS16456245528614177`, // SISTEMSKA GREŠKA
      `IS1645624568705983`, // LJUDSKA GREŠKA KOJA SE NE MOZE RIJEŠITI DODATNOM PROCEDUROM
      `IS1645624588033259`, // GREŠKA DOBAVLJAČA
      `IS1645624601434254`, // GREŠKA KUPCA
    ];
    

    if ( 
      ( data.current_status.status_tip?.sifra && worker_errors_arr.indexOf( data.current_status.status_tip.sifra) > -1 )
      ||
      ( data.current_status.old_status_tip?.sifra && worker_errors_arr.indexOf( data.current_status.old_status_tip.sifra ) > -1 )
      
    ) {
      $('#'+data.rand_id+'_worker_error_box').css(`display`, `flex`);
      $('#'+data.rand_id+'_worker_error_users_row').css(`display`, `flex`);
      
      
      format_number_input( data.current_status.worker_error_cost || null , $('#'+data.rand_id+'_worker_error_cost')[0], null );
      
      // $('#'+data.rand_id+'_worker_error_cost').val( data.current_status.worker_error_cost );
      
      $('#'+data.rand_id+'_worker_error_possible_solution').val( data.current_status.worker_error_possible_solution );
      $('#'+data.rand_id+'_worker_error_solution').val( data.current_status.worker_error_solution );
      
      
      
      this_module.make_worker_error_users_list(data);
      
      
    } else {
      $('#'+data.rand_id+'_worker_error_box').css(`display`, `none`);
      $('#'+data.rand_id+'_worker_error_users_row').css(`display`, `none`);
      
      
      $('#'+data.rand_id+'_worker_error_cost').val(``);
      $('#'+data.rand_id+'_worker_error_possible_solution').val(``);
      $('#'+data.rand_id+'_worker_error_solution').val(``);
      
      $(`#${data.rand_id}_worker_error_users_box`).html(``);
      
    };


  };
  this_module.show_worker_error_box = show_worker_error_box;

  
  
  
  async function get_status_kupac( proj_sifra ) {
    
    toggle_global_progress_bar(true);
    
    return new Promise( async function(resolve, reject) {

      $.ajax({
        headers: {
          'X-Auth-Token': window.cit_user.token
        },
        type: "POST",
        cache: false,
        url: `/get_status_kupac`,
        data: JSON.stringify( { proj_sifra: proj_sifra } ),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done( async function (result) {

        console.log(result);

        if ( result.success == true ) {

          resolve(result.partner);


        } else {

          // ako result NIJE SUCCESS = true

          if ( result.msg ) console.error(result.msg);
          if ( !result.msg ) console.error(`Greška prilikom dobivanja podataka KUPCA od ovog STATUSA!`);
          resolve(null);

        };

      })
      .fail(function (error) {
        console.error(`Došlo je do greške na serveru prilikom dobivanja podataka KUPCA od ovog  STATUSA!`);
        console.log(error);
        resolve(null);
        
      })
      .always(function() {
        toggle_global_progress_bar(false);
      });

    
    }); // kraj promisa
    
    
  };
  this_module.get_status_kupac = get_status_kupac;

  
  
  
  // ako imam button elem onda moram imati i treći argument a to je cijeli objekt arg work !!!!!
  async function save_work( e, button_elem, arg_work) {
      

    var this_comp_id = null
    var data = null;
    var rand_id = null;
    
    var work_desc = null;
    var work_dur_time = null;
    
    
    if ( e ) {
    
      // OVO JE KAD UPISUJEM WORK UNUTAR PROJEKTA U NEKOM STATUSU
      // OVO JE KAD UPISUJEM WORK UNUTAR PROJEKTA U NEKOM STATUSU
      // OVO JE KAD UPISUJEM WORK UNUTAR PROJEKTA U NEKOM STATUSU
      
      this_comp_id = $(this).closest('.cit_comp')[0].id;
      data = this_module.cit_data[this_comp_id];
      rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      //ako nije izabran neki worker onda uzmi onog koji je ulogiran !!!

      if ( !data.current_worker ) {

        data.current_worker = {
          _id: window.cit_user._id, 
          user_number: window.cit_user.user_number,
          email: window.cit_user.email,
          full_name: window.cit_user.full_name,
          name: window.cit_user.name,
        };

      };
      
      // ako ne postoji current work objekt
      if ( !data.current_work ) data.current_work = {};

      refresh_simple_cal( $('#'+rand_id+`_add_work_from_time`) );
      refresh_simple_cal( $('#'+rand_id+`_add_work_to_time`) );
      
      work_desc = $('#'+rand_id+`_work_desc`).val();
      
      if ( work_desc == "" ) {
        popup_warn(`Potrebno upisati opis posla !!!`);
        return;
      };
      
      
      data.current_work.desc = work_desc;
      
      // ako nisam upisao trajanje rada i ONDA MORAM IMATI POČETAK I KRAJ RADA 
      if ( $('#'+rand_id+`_add_work_time`).val() == "" && (!data.work_from_time || !data.work_to_time) ) {
        
        popup_warn(`Potrebno upisati trajanje<br>ili<br>oba vremena Početak rada - Kraj rada !!!`);
        return;
        
      } else if ( $('#'+rand_id+`_add_work_time`).val() !== "" && data.work_from_time && data.work_to_time ) {
        
        
        data.current_work.work_from_time = data.work_from_time || null;
        data.current_work.work_to_time = data.work_to_time || null;

        // ako nisam upisao duration time
        if ( data.work_from_time && data.work_to_time ) {
          data.current_work.duration = (data.work_to_time - data.work_from_time)/(1000*60);
        };
        
        
      
      }
      else if ( $('#'+rand_id+`_add_work_time`).val() !== "" ) {
        
        // AKO IMAM TRAJANJE RADA ( to je u MINUTAMA )  -----> ne moram imati početak i kraj rada
        work_dur_time = cit_number( $('#'+rand_id+`_add_work_time`).val() );

        if ( work_dur_time == null ) {
          popup_warn(`Trajanje rada nije brojčana vrijednost ili je nula !!!`);
          return;
        };
        
        data.current_work.duration = work_dur_time; // to je u MINUTAMA
        
        
      };
      

      data.current_work.worker = data.current_worker;
      data.current_work.time = Date.now();
      data.current_work.sifra = `work` + cit_rand();


    } 
    else if ( button_elem ) {
        
      // ovo koristim kada je production jer onda izpod haube upisujem work data !!
      // ovo koristim kada je production jer onda izpod haube upisujem work data !!
      // ovo koristim kada je production jer onda izpod haube upisujem work data !!

      this_comp_id = $(button_elem).closest('.cit_comp')[0].id;
      data = this_module.cit_data[this_comp_id];
      rand_id =  this_module.cit_data[this_comp_id].rand_id;
  
      data.current_work = arg_work;
      
      
    };
    
    if ( !data.current_work?.work_cat ) {
      popup_warn(`Potrebno izabrati kategoriju posla !!!`);
      return;
    };
    
    // ------------------------------------------ UBACIVANJE U CURRENT STATUS ------------------------------------------
    // ------------------------------------------ UBACIVANJE U CURRENT STATUS ------------------------------------------
    // ------------------------------------------ UBACIVANJE U CURRENT STATUS ------------------------------------------
    

    if ( !data.current_status )  data.current_status = {};
    if ( !data.current_status.work_times ) data.current_status.work_times = [];
    
    // ubaci novi posao u status
    data.current_status.work_times = upsert_item( data.current_status.work_times, data.current_work, `sifra`);
    // ubaci update-an status u array
    data.statuses = upsert_item( data.statuses, data.current_status, `sifra` );
    

    var work_time_sum = 0;
    $.each( data.current_status.work_times, function(ind, work_obj) {
      work_time_sum += work_obj.duration;
    });
    
    // ------------------------------------------ UBACIVANJE U CURRENT STATUS ------------------------------------------
    // ------------------END--------------------- UBACIVANJE U CURRENT STATUS ------------------------------------------
    // ------------------------------------------ UBACIVANJE U CURRENT STATUS ------------------------------------------
    
    return new Promise( async function(resolve, reject) {
    
      if (e) {
        // ako je user kliknuo na gumb dodaj work unutar producta
        this_module.make_work_list(data);

        // upisuj ukupno vrijeme kao sate a ne minute
        format_number_input( work_time_sum/60, $('#'+rand_id+`_work_time_sum`)[0], null );

        $('#'+rand_id+`_worker`).val("");
        data.current_worker = null;

        $('#'+rand_id+`_add_work_time`).val("");
        $('#'+rand_id+`_work_cat`).val("");
        $('#'+rand_id+`_work_desc`).val("");

        $('#'+rand_id+`_add_work_from_time`).val("");
        $('#'+rand_id+`_add_work_to_time`).val("");
        
        resolve(true);

      }
      else if ( button_elem ) {

        // ovo je samo za spremanje novog zapisa posla unutar glavnog statusa za radni proces !!
        // ovo je samo za spremanje novog zapisa posla unutar glavnog statusa za radni proces !!
        var result = await this_module.ajax_save_status( data, `Product`, data.current_status?.product_id || null );

        if ( result?.success == true ) {
          
          cit_toast(`ZAPIS JE SPREMLJEN!`);
          
          this_module.make_status_list(data);
          
          if ( 
            window.location.hash.indexOf(`#production/`) > -1  // mora biti production page
            &&
            data.temp_work_cat
            &&
            data.temp_work_cat?.sifra !== "WCAT6" // naziv: "Gotovo radno vrijeme" ---- za ovo ne treba status
          ) {
            
            var popup_text = `Želite li poslati poruku POSLOVOĐI ?`;

            function poruka_yes() {
              this_module.make_prodon_status(button_elem);  
            };

            function poruka_no() {
              show_popup_modal(false, popup_text, null );
              
              
              // resetiraj za sljedeći STOP !!!!
              data.temp_work_cat = null;
              // resetiraj sve check boxove u HTML-u
              $(`#prodon_check_boxes .cit_check_box`).each( function() {
                $(this).addClass(`off`).removeClass(`on`);
              });
              
            };

            var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
            var show_popup_modal = pop_mod.show_popup_modal;
            show_popup_modal(`warning`, popup_text, null, 'yes/no', poruka_yes, poruka_no, null);
            
          };
          
          resolve(result);

        } else {
          
          popup_error(`Došlo je do greške prilikom upisivanja rada!!!`);
          resolve(result);
          
        };

      };


      // resetiraj
      data.current_work = null;
      data.work_from_time = null;
      data.work_to_time = null;
    
    }); // kraj promisa  
      

  };
  this_module.save_work = save_work;
  
  
  function make_prodon_status(button_elem) {

    var this_comp_id = $(button_elem).closest('.cit_comp.status_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    var statuses_copy = cit_deep(data.statuses);
    var criteria = [ 'time' ];
    multisort( statuses_copy, criteria );
    // pošto je poredano po time desc prvi status je najnoviji
    var glavni_status = statuses_copy[0];


    var reply_button = null;

    // pronalazim ---  gumb za replay na GLAVNI PARENT STATUS  koji je kronološki PRVI u listi statusa
    $(`#${data.id} .statuses_box .search_result_table .search_result_row`).each( function() {
      
      if ( 
        
        $(this).attr("data-id") == glavni_status._id
        || 
        $(this).attr("data-id") == glavni_status.status_id 
        ||
        $(this).attr("data-id") == glavni_status.sifra 
        
      ) {
        
        reply_button = $(this).find(`.result_row_reply_btn`);
        
        reply_button.trigger(`click`); // kliknio sam na reply gumb na tom statusu
        
        
        // ---------------- kad kliknem na reply button onda se pokrene funkcija 
        // setup_status_for_reply 
        // koja pripremi STATUS za reply
        
      };
      
      
    });



    if ( 
      data.current_status?.status_tip?.sifra == `IS16430378755748118` /* SAMO AKO JE OVO RADNI proces */
    ) {

      var status_tip = null;

      // , naziv: "Dobio drugi zadatak" };
      if ( data.temp_work_cat.sifra == "WCAT7" ) {  
        status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS16450938913273772`, `sifra`) ); 
      };

      // , naziv: "Greška u radnom nalogu" };
      if ( data.temp_work_cat.sifra == "WCAT8" ) {  
        status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS1641804940312613`, `sifra`) ); 
      };

      // , naziv: "Problem u radu" };
      if ( data.temp_work_cat.sifra == "WCAT3" ) {  
        status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS16451097477878032`, `sifra`) ); 
      };

      // , naziv: "Pokvaren stroj" };
      if ( data.temp_work_cat.sifra == "WCAT4" ) {  
        status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS1641804595136842`, `sifra`) ); 
      };

      // , naziv: "Problem osoblja" };
      if ( data.temp_work_cat.sifra == "WCAT5" ) {  
        status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS16418047377375376`, `sifra`) ); 
      };    
      
      
      // { sifra: "WCAT10", naziv: "RN GOTOV" },
      if ( data.temp_work_cat.sifra == "WCAT10" ) {  
        status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS1643038121299174`, `sifra`) ); 
      };       
      

      data.current_status.status_tip = status_tip;

      data.current_status.dep_to = {
          "sifra" : "PRO2",
          "naziv" : "Proizvodnja/Office"
      };

      data.current_status.to =  {
          "_id" : "60cc9208f0b3e71b940aa88d",
          "name" : "bruno",
          "user_number" : 67,
          "full_name" : "Žgela Bruno",
          "email" : "bruno.zgela@velprom.hr"
      };

      
      
      var prodon_komentar = $(`#${rand_id}_prodon_work_desc`).val() || `NEMA KOMENTARA`;
      prodon_komentar = esc_html(prodon_komentar);
      
      $(`#${rand_id}_status_komentar`).val( prodon_komentar );

      if ( reply_button ) {
        
        setTimeout( function() { 
          // SEND REPLY // SEND REPLY // SEND REPLY // SEND REPLY // SEND REPLY // SEND REPLY // SEND REPLY // SEND REPLY 
          $(`#${rand_id}_reply_status`).trigger(`click`); 
          
          // resetiraj za sljedeći put kad user klikne ne neki  PRODUCTION GUMB !!!!
          data.temp_work_cat = null;
          
          // resetiraj sve check boxove u HTML-u
          $(`#prodon_check_boxes .cit_check_box`).each( function() {
            $(this).addClass(`off`).removeClass(`on`);
          });
          
        }, 300 );
        
      }; // kraj ako je našao reply gumb od last statusa

    }; // kraj ako postoji curr status i ako ima parent koji je STATUS TIP == RADNI PROCES 


  };
  this_module.make_prodon_status = make_prodon_status;

  
  function make_worker_error_users_list(data, return_html) {
    
    $(`#${data.rand_id}_worker_error_users_box`).html(``);

    var new_error_users_list = [];

    var curr_status = return_html ? data : data.current_status;

    if ( curr_status.worker_error_users && curr_status.worker_error_users.length > 0 ) {

      $.each( curr_status.worker_error_users, function(index, error_user) {

        var { 
          _id,
          name,
          user_number,
          full_name,
          email,
        } = error_user;


        new_error_users_list.push({
          _id,
          name,
          user_number,
          full_name,
          email,
        });

      });

    };


    var show_cols = [ 
        `user_number`,
        `full_name`,
        `email`,
        `button_delete`,

      ];


    var col_widths = [ 

      1, //`user_number`,
      3, // `full_name`,
      4, // `email`,
      1, // `button_delete`,

      ];


    var custom_headers = [

        `USER BROJ`, // `tip_naziv`,
        `IME / PREZIME`, // `oznaka`,
        `EMAIL`, // `doc_time`,
        `IZBACI`, // `button_delete`,

      ];

    
if ( return_html ) {

      show_cols = [ 
        `user_number`,
        `full_name`,
        `email`,
      ];

      col_widths = [ 

        1, //`user_number`,
        3, // `full_name`,
        4, // `email`,

      ];

      custom_headers = [

      `USER BROJ`, // `tip_naziv`,
      `IME / PREZIME`, // `oznaka`,
      `EMAIL`, // `doc_time`,
      
      ];


    };    
    
    
    

    var error_users_props = {

      desc: `samo za kreiranje tablice ZA ERROR USER TJ usere koji su povezani s ovom greškom !!! ${data.rand_id}`,
      local: true,
      list: new_error_users_list,

      show_cols: show_cols,
      col_widths: col_widths,
      custom_headers: custom_headers,

      format_cols: {
        user_number: "center",
      },

      parent: return_html || `#${data.rand_id}_worker_error_users_box`,
      return: {},
      show_on_click: false,


      cit_run: function(state) { console.log(state); },

      button_delete: async function(e) {

        e.stopPropagation();
        e.preventDefault();

        var this_button = this;

        console.log("kliknuo DELETE za ERROR USERS ");


        var popup_text = `Jeste li sigurni da želite IZBACIT ovog KORISNIKA s liste?`

        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var user_id = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);

          data.current_status.worker_error_users = delete_item( data.current_status.worker_error_users, user_id , `_id` );

          $(`#`+parent_row_id).remove();

        };




        function delete_no() {
          show_popup_modal(false, popup_text, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, popup_text, null, 'yes/no', delete_yes, delete_no, null);

      },

    };

    if ( new_error_users_list.length > 0 ) {

      if ( return_html ) {
        return create_cit_result_list(new_error_users_list, error_users_props );
      } else {
        create_cit_result_list(new_error_users_list, error_users_props );
      };

    } else {

      return ``;

    };

  };
  this_module.make_worker_error_users_list = make_worker_error_users_list;

  
  function make_rn_files_list(data, return_html) {

    var new_rn_list = [];

    var curr_status = return_html ? data : data.current_status;

    if (  curr_status.rn_files &&  curr_status.rn_files.length > 0 ) {

      $.each(  curr_status.rn_files, function(index, rn_file) {

        var { 

          doc,
          doc_time,
          graf_doc,
          graf_sifra,
          sifra,
          template,
          time,
          tip_naziv,

        } = rn_file;


        var curr_doc = doc || graf_doc;
        var oznaka = template + ( graf_sifra ? `-`+graf_sifra : "" );

        new_rn_list.push({

          sifra,
          tip_naziv,
          oznaka,
          curr_doc,
          doc_time,

        });

      });
    };


    var show_cols = [ 
        `tip_naziv`,
        `oznaka`,
        `doc_time`,
        `curr_doc`,

        `button_delete`,

      ];


    var col_widths = [ 

        2, // `tip_naziv`,
        2, // `oznaka`,
        2, // `doc_time`,
        8, // `curr_doc`,
        1, // `button_delete`,

      ];


    var custom_headers = [

        `TIP`, // `tip_naziv`,
        `ŠIFRA`, // `oznaka`,
        `VRIJEME`, // `doc_time`,
        `DOKUMENT`, // `curr_doc`,
        `IZBACI`, // `button_delete`,

      ];


    if ( return_html ) {

      show_cols = [ 
        `tip_naziv`,
        `oznaka`,
        `doc_time`,
        `curr_doc`,
      ];

      col_widths = [ 

        2, // `tip_naziv`,
        2, // `oznaka`,
        2, // `doc_time`,
        8, // `curr_doc`,

      ];

      custom_headers = [

        `TIP`, // `tip_naziv`,
        `ŠIFRA`, // `oznaka`,
        `VRIJEME`, // `doc_time`,
        `DOKUMENT`, // `curr_doc`,
      ];


    };


    var rn_files_props = {

      desc: 'samo za kreiranje tablice RN FILES unuter radni proces status',
      local: true,
      list: new_rn_list,

      show_cols: show_cols,
      col_widths: col_widths,
      custom_headers: custom_headers,

      format_cols: {
        tip_naziv: "center",
        oznaka: "center",
        doc_time: "date_time",
      },

      parent: return_html || `#${data.rand_id}_rn_files_list_box`,
      return: {},
      show_on_click: false,


      cit_run: function(state) { console.log(state); },

      button_delete: async function(e) {

        e.stopPropagation();
        e.preventDefault();

        var this_button = this;

        console.log("kliknuo DELETE za RN FILES ");


        var popup_text = `Jeste li sigurni da želite IZBACIT ovaj DOKUMENT s liste?`

        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);

          data.current_status.rn_files = delete_item( data.current_status.rn_files, sifra , `sifra` );

          $(`#`+parent_row_id).remove();
        };




        function delete_no() {
          show_popup_modal(false, popup_text, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, popup_text, null, 'yes/no', delete_yes, delete_no, null);

      },

    };

    if ( new_rn_list.length > 0 ) {

      if ( return_html ) {
        return create_cit_result_list(new_rn_list, rn_files_props );
      } else {
        create_cit_result_list(new_rn_list, rn_files_props );
      };

    } else {
      
      return ``;
      
    };

  };
  this_module.make_rn_files_list = make_rn_files_list;
  
  
  
  function make_rn_alat_list(data, return_html) {

    var new_rn_list = [];

    var curr_status = return_html ? data : data.current_status;

    
    if ( $.isArray(curr_status.rn_alat) && curr_status.rn_alat.length == 0 )  curr_status.rn_alat = null;
    
    if ( curr_status.rn_alat ) {

        var { 

          _id,
          time,
          user,
          sirovina_sifra,
          full_naziv,
          val_x_kontraval,
          po_valu,
          po_kontravalu,
          specs
          
          
        } = curr_status.rn_alat;

      
      var file_list = ``;
      
      if ( specs?.length > 0 ) {
        
        $.each( specs, function(spec_ind, spec) {
          
          if ( spec.docs?.length > 0 ) {
            
            $.each( spec.docs, function(doc_ind, doc) {
              var file_link = user.full_name + ` ` + doc.link + `<br>`;
              file_list +=  file_link ;  
            });
            
          };
        });
        
      }; // kraj ako je ubačen alat
      
      
      
      

        new_rn_list.push({

          _id,
          tip_naziv: `ALAT`,
          oznaka: sirovina_sifra,
          curr_doc: file_list,
          doc_time: time,

        });

    }; // kraj ako u statusu postoji rn alat


    var show_cols = [ 
        `tip_naziv`,
        `oznaka`,
        `doc_time`,
        `curr_doc`,

        `button_delete`,

      ];


    var col_widths = [ 

        2, // `tip_naziv`,
        2, // `oznaka`,
        2, // `doc_time`,
        8, // `curr_doc`,
        1, // `button_delete`,

      ];


    var custom_headers = [

        `TIP`, // `tip_naziv`,
        `ŠIFRA`, // `oznaka`,
        `VRIJEME`, // `doc_time`,
        `DOKUMENT`, // `curr_doc`,
        `IZBACI`, // `button_delete`,

      ];


    if ( return_html ) {

      show_cols = [ 
        `tip_naziv`,
        `oznaka`,
        `doc_time`,
        `curr_doc`,
      ];

      col_widths = [ 

        2, // `tip_naziv`,
        2, // `oznaka`,
        2, // `doc_time`,
        8, // `curr_doc`,

      ];

      custom_headers = [

        `TIP`, // `tip_naziv`,
        `ŠIFRA`, // `oznaka`,
        `VRIJEME`, // `doc_time`,
        `DOKUMENT`, // `curr_doc`,
      ];


    };


    var rn_files_props = {

      desc: 'samo za kreiranje tablice RN ALAT unutar radni proces status',
      local: true,
      list: new_rn_list,

      show_cols: show_cols,
      col_widths: col_widths,
      custom_headers: custom_headers,

      format_cols: {
        tip_naziv: "center",
        oznaka: "center",
        doc_time: "date_time",
      },

      parent: return_html || `#${data.rand_id}_rn_alat_list_box`,
      return: {},
      show_on_click: false,


      cit_run: function(state) { console.log(state); },

      button_delete: async function(e) {

        e.stopPropagation();
        e.preventDefault();

        var this_button = this;
        
        
        // ---------------------------------------- ZA SADA NE KORISTIM OVO JER ALAT MOGU OBRISATI KADA EDITIRAM STATUS ----------------------------------------
        // ---------------------------------------- ZA SADA NE KORISTIM OVO JER ALAT MOGU OBRISATI KADA EDITIRAM STATUS ----------------------------------------
        
        return;
        

        console.log("kliknuo DELETE za RN ALAT ");


        var popup_text = `Jeste li sigurni da želite IZBACIT ovaj DOKUMENT s liste?`

        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);

          data.current_status.rn_files = delete_item( data.current_status.rn_files, sifra , `sifra` );

          $(`#`+parent_row_id).remove();
        };




        function delete_no() {
          show_popup_modal(false, popup_text, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, popup_text, null, 'yes/no', delete_yes, delete_no, null);

      },

    };

    if ( new_rn_list.length > 0 ) {

      if ( return_html ) {
        return create_cit_result_list(new_rn_list, rn_files_props );
      } else {
        create_cit_result_list(new_rn_list, rn_files_props );
      };

    } else {
      
      return ``;
      
    };

  };
  this_module.make_rn_alat_list = make_rn_alat_list;
    
  

  


  
  function prodon_percent_setup(data) {
    
    var proces_ind = null;
    $.each( data.current_status.kalk.post_kalk[0].procesi, function(p_index, proces ) {
      if ( proces.row_sifra == data.proces_id ) proces_ind = p_index;
    });
    

    $(`#prodon_product_title`).html(
    `
    ${ data.product.full_product_name || "???" }
    <br>
    ${ `RADNI PROCES: ` + (proces_ind+1) + `. ` + data.current_proces.action.action_radna_stn.naziv || "???" }

    `);


    $(`.prodon_perc_ratio`).html(`${ data.post_izlazi_sum }&nbsp;&nbsp;/&nbsp;&nbsp;${ data.pro_izlazi_sum }`);

    var proces_ratio = data.post_izlazi_sum / data.pro_izlazi_sum * 100;

    proces_ratio = cit_round(proces_ratio, 0);

    $(`.prodon_perc_val`).css(`width`, proces_ratio + `%`);
    $(`.prodon_perc_val`).text(proces_ratio + `%`);
   
    
  };
  this_module.prodon_percent_setup = prodon_percent_setup; 
  
  
  function make_pro_sums(data) {
    
    
    $.each( data.current_status.kalk.pro_kalk[0].procesi, function(pro_index, pro_proces ) {

      if ( pro_proces.row_sifra == data.proces_id ) { // proces id dobije iz URL -----> u router.js

        data.pro_proces = pro_proces;
        data.pro_ulazi_sum = 0;
        
        $.each( data.pro_proces.ulazi, function(u_ind, ulaz) {
          data.pro_ulazi_sum += sum_plus_extra(ulaz); // ulaz.kalk_sum_sirovina;
        });

        data.pro_izlazi_sum = 0;
        $.each( data.pro_proces.izlazi, function(i_ind, izlaz) {
          
          // ako ovaj proces ima izlaz_kom u EXTRA DATA onda 
          // i ako taj izlaz kom ima sifru kao ovaj trenutni izlaz !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          if ( pro_proces.extra_data?.izlaz_kom?.sifra == izlaz.sifra ) {
            data.pro_izlazi_sum += pro_proces.extra_data.izlaz_kom.kom;
          }
          else {
            data.pro_izlazi_sum += izlaz.kalk_sum_sirovina;
          };
          
        });

      };

    }); // kraj loopa po PRO procesima
    
  };
  this_module.make_pro_sums = make_pro_sums
  
  
  
  function make_post_sums(data) {
    
    
    $.each( data.current_status.kalk.post_kalk[0].procesi, function(post_index, post_proces ) {

      if ( post_proces.row_sifra == data.proces_id ) { // proces id dobije iz URL -----> u router.js

        data.post_proces = post_proces;
        data.post_ulazi_sum = 0;
        
        $.each( data.post_proces.ulazi, function(u_ind, ulaz) {
          data.post_ulazi_sum += sum_plus_extra(ulaz); // ulaz.kalk_sum_sirovina;
        });

        data.post_izlazi_sum = 0;
        $.each( data.post_proces.izlazi, function(i_ind, izlaz) {
          
          // OVDJE NE UZIMAM IZLAZ KOMADA IZ EXTRA DATA !!!!!
          // OVDJE NE UZIMAM IZLAZ KOMADA IZ EXTRA DATA !!!!! ------> samo uzimam direktno iz izlaza tj prop kalk sum sirovina
          data.post_izlazi_sum += izlaz.kalk_sum_sirovina;
          
          
        });

      };

    }); // kraj loopa po POST procesima
    
  };
  this_module.make_post_sums = make_post_sums;
  
    
  function make_prodon_drop_lists(data) {
    
    data.post_ulazi_list = [];
    $.each( data.current_proces.ulazi, function(u_ind, ulaz) {

      var naziv_ulaza = ulaz.kalk_full_naziv ? ulaz.kalk_full_naziv : "";
      // ako nema full naziv onda je vjerojatno MONTAŽNA PLOČA koja ima prop plate naziv
      if (!naziv_ulaza ) naziv_ulaza = ulaz.kalk_plate_naziv ? ("FORMA " + ulaz.kalk_plate_naziv) : "";

      data.current_proces.ulazi[u_ind].prodon_name = 
        naziv_ulaza + ` ` +
        (ulaz.kalk_element?.elem_tip?.naziv || ``) + ` -- ` +
        (ulaz.kalk_element?.elem_opis || ``);

      data.post_ulazi_list.push(ulaz);

    });


    data.post_izlazi_list = [];
    $.each( data.current_proces.izlazi, function(i_ind, izlaz) {

      data.current_proces.izlazi[i_ind].prodon_name = 
        /* (izlaz.kalk_full_naziv ? izlaz.kalk_full_naziv + ` ` : ``) + */
        (izlaz.kalk_element?.elem_tip?.naziv || ``) + ` -- ` +
        (izlaz.kalk_element?.elem_opis || ``);

      data.post_izlazi_list.push(izlaz);

    });
      
    
    
  };
  this_module.make_prodon_drop_lists = make_prodon_drop_lists;
  
  
  
  
  async function production_setup(data) {

    if ( !data.current_status?.kalk ) {
      popup_error(`NEDOSTAJE PROIZVODNI PROCES !!!`);
      return;
    };


    $(`html`)[0].scrollTop = 0;

    $(`#cit_page_title h3`).text(data.status_type_title);
    document.title = data.status_type_title.toUpperCase() + " VELPROM";
    
    
    ask_before_close_register_event();
    

    // ako postoji rn files ili je odabran alat unutar RN STATUSA
    if ( data.current_status.rn_files?.length > 0 || data.current_status.rn_alat ) {

      var file_list = ``;

      $.each( data.current_status.rn_files, function( rn_ind, rn_file ) {
        var file_link = `<div class="col-md-12 col-sm-12">${ rn_file.doc || rn_file.graf_doc } </div>`;
        file_list +=  file_link;
      });
      
      
      if ( data.current_status.rn_alat?.specs?.length > 0 ) {
        
        $.each( data.current_status.rn_alat.specs, function(spec_ind, spec) {
          
          if ( spec.docs?.length > 0 ) {
            
            $.each( spec.docs, function(doc_ind, doc) {
              var file_link = `<div class="col-md-12 col-sm-12">${ doc.link } </div>`;
              file_list +=  file_link;  
            });
            
          };
        });
        
      }; // kraj ako je ubačen alat
      
      
      

      $(`#prodon_file_list_box`).html(file_list);
      
      
    }; // kraj ako ima rn file unutar statusa 

    
    // umjesto _idjeva od sirovine postavi prave objekte iz baze !!!
    // umjesto _idjeva od sirovine postavi prave objekte iz baze !!!
    if ( data.current_status.kalk ) data.current_status.kalk = await populate_kalk_original_sirovs(data.current_status.kalk );
    
    
    
    this_module.make_pro_sums(data);
    
    this_module.make_post_sums(data);
    
    
    $.each( data.current_status.kalk.post_kalk[0].procesi, function (p_index, proces ) {

      
      if ( proces.row_sifra == data.proces_id ) {
        
        
        data.current_proces = proces;
        
        
        // ako se radi od optrgavanju nakon štance ili zunda !!!
        // onda napravi poseban element koji se zove KOMPLET
        // if ( data.current_proces.action_radna_stn?.sifra == `RADS1CLEAN` ) data.post_ulazi_list.push(ulaz);        
        
        
        
        this_module.make_prodon_drop_lists(data);
        
        register_prodon_events(this_module, data); // ova func se nalazi u status_util.js
        
        
        
        

      }; // kraj ifa - našao je odgovarajući proces

    }); // kraj loopa po post procesima

    
    
  };
  this_module.production_setup = production_setup;
  
 

  function make_percent_bar(curr_status) {

    var pro_ulazi = 0;
    var pro_izlazi = 0;

    var proces_ind = null;

    var post_ulazi = 0;
    var post_izlazi = 0;

    var perc_html = ``;

    // var curr_status = return_html ? data : data.current_status;

    if ( 
      window.location.hash.indexOf(`#production/`) == -1 
      &&
      curr_status.kalk 
      &&
      curr_status.kalk.pro_kalk?.length > 0
      &&
      curr_status.kalk.post_kalk?.length > 0
      &&
      curr_status.status_tip?.sifra == `IS16430378755748118` /* ovo je sifra za PROIZVODNI PROCES - RADNI NALOG */ 
    ) {

      $.each( curr_status.kalk.pro_kalk[0].procesi, function(pro_index, pro_proces ) {

        if ( pro_proces.row_sifra == curr_status.proces_id ) {

          $.each( pro_proces.ulazi, function(u_ind, ulaz) {
            pro_ulazi += ulaz.kalk_sum_sirovina;
          });

          $.each( pro_proces.izlazi, function(i_ind, izlaz) {
            
            
            // ako ovaj proces ima izlaz_kom u EXTRA DATA onda 
            // i ako taj izlaz kom ima sifru kao ovaj trenutni izlaz !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if ( pro_proces.extra_data?.izlaz_kom?.sifra == izlaz.sifra ) {
              pro_izlazi += pro_proces.extra_data.izlaz_kom.kom;
            }
            else {
              pro_izlazi += izlaz.kalk_sum_sirovina;
            };
            
            
          });
          

        };

      }); // kraj loopa po pro procesima


      $.each( curr_status.kalk.post_kalk[0].procesi, function(post_index, post_proces ) {

        if ( post_proces.row_sifra == curr_status.proces_id ) {

          proces_ind = post_index;

          $.each( post_proces.ulazi, function(u_ind, ulaz) {
            post_ulazi += ulaz.kalk_sum_sirovina;
          });

          $.each( post_proces.izlazi, function(i_ind, izlaz) {
            
            // OVDJE NE UZIMAM EXTRA DATA KOMADA -----> KOMADE TJ KOLIČINU UZIMAM SAMO DIREKTNO IZ IZLAZA
            post_izlazi += izlaz.kalk_sum_sirovina;
            
          });
          

        };

      }); // kraj loopa po POST procesima


      var proces_ratio = post_izlazi / pro_izlazi * 100;
      proces_ratio = cit_round(proces_ratio, 0);


      perc_html = 
  `
  <div class="row perc_inside_list" style="margin: 0;" >
   
   
    <div class="col-md-9 col-sm-12" style="margin: 0; padding: 0;">
      <div class="prodon_perc_box">
        <div class="prodon_perc_val" style="width: ${proces_ratio}%">${proces_ratio}%</div> 
      </div>
    </div>

    <div class="col-md-3 col-sm-12 prodon_perc_ratio" >
      ${ post_izlazi }&nbsp;&nbsp;/&nbsp;&nbsp;${ pro_izlazi }
    </div>
    
  </div>
  `;


    }; // kraj ako staus ima kalk i ako je radni proces

    return perc_html;


  };
  this_module.make_percent_bar = make_percent_bar;

  

  
  
  
  this_module.cit_loaded = true;
 
} // end of module scripts
  
  
};

