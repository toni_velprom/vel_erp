        
/*
OVO JE HTML ZA LINK

<div class="col-md-1 col-sm-12" >
  <a  id="link_to_roba_kupac" href="#" target="_blank" onclick="event.stopPropagation();"
      class="blue_btn btn_small_text small_btn_link_left" 
      style="box-shadow: none; margin: 22px auto 0 0; width: 28px; height: 28px;" >

    <i style="font-size: 18px;" class="far fa-external-link-square-alt"></i>

  </a>
</div>

*/          


  // ---------------- NE KORISTIM OVO ZA SADA ----------------
  // ---------------- NE KORISTIM OVO ZA SADA ----------------
  // ---------------- NE KORISTIM OVO ZA SADA ----------------
  function fill_roba_kupac( data ) {
    
    $("#link_to_roba_kupac").attr(`href`, `#`);
    console.log(data.roba_kupac);

    var this_comp_id = $(`#`+data.rand_id+`_roba_kupac`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    if ( !data.roba_kupac ) return;
    
    // provlačim ovo kroz filter za pretragu partnera tako da mi uredi podatke da budu točno kako trebaju izgledati
    var filtered_kupac_array = this_module.find_partner.find_partner_filter( [ data.roba_kupac ], jQuery );
    data.roba_kupac = filtered_kupac_array[0]; // uzmi samo prvi item jer mi vraća array !!!!
  
    
    
    var full_partner = 
        (data.roba_kupac?.naziv || "") + 
        ( data.roba_kupac?.grupacija_naziv ? (", " + data.roba_kupac?.grupacija_naziv) : "" ) + 
        ( data.roba_kupac?.sjediste ? ", "+data.roba_kupac?.sjediste : "" );

    $(`#`+data.rand_id+`_roba_kupac`).val(full_partner);
    
    $("#link_to_roba_kupac").attr(`href`, `#partner/${data.roba_kupac._id}/status/null`); 
    
    
  };
  this_module.fill_roba_kupac = fill_roba_kupac;
  