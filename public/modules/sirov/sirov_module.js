var module_object = {
  
create: ( data, parent_element, placement ) => {
  
  var this_module = window[module_url]; 
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };

  var { id } = data;

  if ( !data._id ) data = { ...data, ...this_module.new_sirov };
  
  
  // dodajem propse kojem možda nemam zapisane u bazi pa su undefined tj uopće ne postoje !!!!
  // dodajem propse kojem možda nemam zapisane u bazi pa su undefined tj uopće ne postoje !!!!
  // dodajem propse kojem možda nemam zapisane u bazi pa su undefined tj uopće ne postoje !!!!
  
  
  if ( !data.doc_valuta_manual ) data.doc_valuta_manual = null;
  
  if ( !data.pro_cijena ) data.pro_cijena = null;
  if ( !data.m2_cijena ) data.m2_cijena = null;

  
  
  
  // --------------------------- LOGISTIČKI PODACI ---------------------------
  // --------------------------- LOGISTIČKI PODACI ---------------------------
  
  
  if ( !data.write_skl_card_num ) data.write_skl_card_num = null;
  if ( !data.card_num_list ) data.card_num_list = [];
  
  if ( !data.palet_count ) data.palet_count = null;
  if ( !data.box_count_on_palet ) data.box_count_on_palet = null;
  if ( !data.red_count_on_palet ) data.red_count_on_palet = null;
  if ( !data.box_count_in_red ) data.box_count_in_red = null;
  if ( !data.palet_unit_count ) data.palet_unit_count = null;
  
  if ( !data.paleta_x ) data.paleta_x = null;
  if ( !data.paleta_y ) data.paleta_y = null;
  if ( !data.paleta_z ) data.paleta_z = null;
  
  if ( !data.box_count ) data.box_count = null;
  if ( !data.box_unit_count ) data.box_unit_count = null;
  if ( !data.box_x ) data.box_x = null;
  if ( !data.box_y ) data.box_y = null;
  if ( !data.box_z ) data.box_z = null;
  if ( !data.loc_komentar ) data.loc_komentar = null;
  
  if ( !data.ms_time ) data.ms_time = null;
  
  
  
  
  if ( !data.neto_masa_paketa_kg ) data.neto_masa_paketa_kg = null;
  if ( !data.bruto_masa_paketa_kg ) data.bruto_masa_paketa_kg = null;
  if ( !data.neto_masa_palete_kg ) data.neto_masa_palete_kg = null;
  if ( !data.bruto_masa_palete_kg ) data.bruto_masa_palete_kg = null;
  // --------------------------- LOGISTIČKI PODACI ---------------------------
  // --------------------------- LOGISTIČKI PODACI ---------------------------
  
  
  
  
  
  if ( !data.masa_kg ) data.masa_kg = null;
  
  if ( !data.alat_shelf ) data.alat_shelf = null;
  
  if ( !data.alat_stamp_num ) data.alat_stamp_num = null;
  if ( !data.alat_max_stamp ) data.alat_max_stamp = null;
  
  if ( !data.alat_nest_count ) data.alat_nest_count = null;
  
  
  if ( !data.alat_owner ) data.alat_owner = null;
  
  if ( !data.alat_fi ) data.alat_fi = null;
  if ( !data.alat_val ) data.alat_val = null;
  if ( !data.alat_kontra ) data.alat_kontra = null;

  
  
  
  if ( !data.rok_dostave ) data.rok_dostave = null;
  if ( !data.exp_month ) data.exp_month = null;
  
  
  if ( !data.ask_kolicina_1 ) data.ask_kolicina_1 = null;
  if ( !data.ask_kolicina_2 ) data.ask_kolicina_2 = null;
  if ( !data.ask_kolicina_3 ) data.ask_kolicina_3 = null;
  if ( !data.ask_kolicina_4 ) data.ask_kolicina_4 = null;
  if ( !data.ask_kolicina_5 ) data.ask_kolicina_5 = null;
  if ( !data.ask_kolicina_6 ) data.ask_kolicina_6 = null;
  
  if ( !data.sirov_pics ) data.sirov_pics = [];
  
  
  if ( !data.list_kupaca ) data.list_kupaca = [];
  
  if ( !data.price_hist_date ) data.price_hist_date = null; 
  
  if ( !data.premaz ) data.premaz = null; 
  
  if ( !data.premaz_papira ) data.premaz_papira = null; 
  if ( !data.vrsta_papira ) data.vrsta_papira = null; 
  
  
  if ( !data.voz_user ) data.voz_user = null; 
  if ( !data.voz_marka ) data.voz_marka = null; 
  if ( !data.voz_reg ) data.voz_reg = null; 
  if ( !data.voz_sas ) data.voz_sas = null; 
  if ( !data.voz_buy_date ) data.voz_buy_date = null; 
  
  if ( !data.voz_leasing ) data.voz_leasing = null; 
  if ( !data.voz_leasing_house ) data.voz_leasing_house = false; 
  if ( !data.voz_leasing_exp_date ) data.voz_leasing_exp_date = false; 
  
  if ( !data.voz_last_servis_date ) data.voz_last_servis_date = false; 
  if ( !data.voz_reg_exp_date ) data.voz_reg_exp_date = false; 
  
  
  if ( !data.palete_free ) data.palete_free = null; 
  if ( !data.palete_taken ) data.palete_taken = null;
  if ( !data.palete_broken ) data.palete_broken = null;
  
  
  if ( !data.sloj_1 ) data.sloj_1 = null;
  if (  !data.val_1 )  data.val_1 = null;
  if ( !data.sloj_2 ) data.sloj_2 = null;
  if (  !data.val_2 )  data.val_2 = null;
  if ( !data.sloj_3 ) data.sloj_3 = null;
  
  
  if ( !data.stroj_kw ) data.stroj_kw = null;
  if ( !data.stroj_god ) data.stroj_god = null;
  

  
  var curr_time = new Date();
  var curr_time_units = get_date_units(curr_time);
  
  curr_time = new Date( curr_time_units.yyyy, curr_time_units.mnt, curr_time_units.day, 0, 0, 0);
  curr_time = Number(curr_time);
  
  var curr_record = null;
  
  if ( data.records?.length > 0 ) {
    
    // neka najnoviji datum bude prvi  ----> ovo je descending sort
    var criteria = [ '!record_est_time' ];
    multisort( data.records, criteria );
    
    $.each(data.records, function(r_ind, record) {
      // pošto idem od najnovijeg prema najstarijem 
      // uzimam record koji je veći ili jednak današnjem danu od 00:00
      if ( record.record_est_time >= curr_time ) curr_record = record;
      
    });
    
    // SADA PONOVO SORTIRAJ ALI OD NAJSTARIJEG DATUMA DO NAJNOVIJEG
    // SADA PONOVO SORTIRAJ ALI OD NAJSTARIJEG DATUMA DO NAJNOVIJEG
    
  };
  
  

  
  this_module.set_init_data(data);
  
  // this_module.valid = valid;
  
  var valid = this_module.valid;
  
  var rand_id = `sir`+cit_rand();
  this_module.cit_data[id].rand_id = rand_id;
  
  
  var sirov_find_mode_title = ``;
  
  if ( window.sirov_find_mode ) sirov_find_mode_title = 
`
<div class="row">
  <h4 class="sirov_find_mode_title" >UKLJUČENA DETALJNA PRETRAGA !!!</h4>
</div>
`

  
  var component_html =
`

<div id="${id}" class="sirovine_body cit_comp" style="${ window.sirov_find_mode ? `background: #f3e6e9`: `` }"  >
 
  <section>
    <div class="container-fluid" style="padding-left: 0; padding-right: 0;" >
      
      ${sirov_find_mode_title}
      
      <div class="row" id="show_scaner_box" >
       
        <div class="col-md-12 col-sm-12" style="display: flex; justify-content: center;">
         
          <button id="show_sirov_scan_btn"
                  class="blue_btn btn_small_text cit_tooltip" >
                    
            <i class="far fa-qrcode" style="font-size: 36px; margin-right: 15px;"></i>
            
            SKENIRANJE
            
          </button>
          
          
          <div id="scaner_popup">
           
            <div id="close_scaner"> <i class="fas fa-times"></i> </div>
           
            
            <div id="scaner_screen" >
              <!-- OVDJE SE PRIKAŽE FEED OD KAMERE -->
         
            </div>  

            <div id="text_out_box">
              
              
            </div>

            
            
          </div>
          
          
          
        </div>
          
      </div>
      
      <div class="row">
       
        <div class="col-md-4 col-sm-12">
          <b>SPREMLJENO:</b> <span id="${rand_id}_user_saved">${data.user_saved?.full_name || "" }</span>
          &nbsp;&nbsp;
          <span id="${rand_id}_saved">${ data.saved ? cit_dt(data.saved).date_time : "" }</span>
        </div>
        <div class="col-md-4 col-sm-12">
          <b>EDITIRANO:</b> <span id="${rand_id}_user_edited">${data.user_edited?.full_name || "" }</span>
          &nbsp;&nbsp;
          <span id="${rand_id}_edited">${ data.edited ? cit_dt(data.edited).date_time : "" }</span>
        </div>
        
        <div class="col-md-2 col-sm-12" style="margin-bottom: 20px;" >
          
           <button  id="copy_sirov_btn"
                    class="blue_btn btn_small_text cit_tooltip"
                    data-toggle="tooltip" data-placement="top" data-html="true" title="Kopiraj ovu sirovinu!">
                    
            <i class="far fa-copy" style="font-size: 16px; margin-right: 7px;"></i>
            KOPIRAJ
            
          </button>
          
          
        
        </div>
        
        
        <div class="col-md-2 col-sm-12" style="margin-bottom: 20px;" >
          

          
          
        
        </div>        
        
        
        
        <div class="col-md-2 col-sm-12" style="margin-bottom: 20px;" >
         
          <button class="blue_btn btn_small_text" id="save_sirov_btn" >
            SPREMI RESURS
          </button>
        </div>
        
      </div>
      
      
      <div class="cit_tab_strip">
        <div id="sirov_tab_podaci" class="cit_tab_btn cit_active">PODACI</div>
        <div id="sirov_tab_povijest" class="cit_tab_btn">POVIJEST</div>
        <div id="sirov_tab_statusi" class="cit_tab_btn">STATUSI</div>
        <div id="sirov_tab_skladiste" class="cit_tab_btn">SKLADIŠTE</div>
        <div id="sirov_tab_proizvodi" class="cit_tab_btn">PROIZVODI</div>
        <div id="tab_police_alata" class="cit_tab_btn" >POLICE ALATA</div>
        <div id="tab_vozilo" class="cit_tab_btn" >VOZILO</div>
      </div>      
      
      <div class="cit_tab_box podaci">
        

        <div class="row">
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_sirovina_sifra`, valid.sirovina_sifra, data.sirovina_sifra) }
          </div>

          <div class="col-md-4 col-sm-12">
            ${ cit_comp(rand_id+`_naziv`, valid.naziv, data.naziv) }
          </div>    

          <div class="col-md-4 col-sm-12">
            ${ cit_comp(rand_id+`_stari_naziv`, valid.stari_naziv, data.stari_naziv) }
          </div>

          <div class="col-md-1 col-sm-12">
            ${ cit_comp(rand_id+`_for_search`, valid.for_search, data.for_search) }
          </div>


          <div class="col-md-1 col-sm-12">
            ${ cit_comp(rand_id+`_not_active`, valid.not_active, data.not_active) }
          </div>

        </div>
        

        <div class="row">


          <!--
          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_dobavljac`, valid.dobavljac, data.dobavljac,  data.dobavljac?.naziv || "") }
          </div>

          -->

          <div class="col-md-6 col-sm-12">
            ${ cit_comp(rand_id+`_dobav_naziv`, valid.dobav_naziv, data.dobav_naziv) }
          </div>

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_dobav_sifra`, valid.dobav_sifra, data.dobav_sifra) }
          </div>

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_fsc`, valid.fsc, data.fsc,  data.fsc?.naziv || "") }
          </div>

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_fsc_perc`, valid.fsc_perc, data.fsc_perc) }
          </div>

        </div>        
        
        <div class="row">
         
          <div class="col-md-11 col-sm-12">
            ${ cit_comp(rand_id+`_find_sirov_original`, valid.find_sirov_original, data.find_sirov_original,  data.original?.full_product_name || "") }
          </div>
          
          
          <div class="col-md-1 col-sm-12" >
           
            <a  href="#" target="_blank" onclick="event.stopPropagation();"
                class="blue_btn btn_small_text small_btn_link_left link_to_original" 
                style="box-shadow: none; margin: 22px auto 0 0; width: 28px; height: 28px;" >

              <i style="font-size: 18px;" class="far fa-external-link-square-alt"></i>

            </a>
            
          </div>
        
        
        </div>
                
        

        <div class="row">

          <div class="col-md-11 col-sm-12" id="find_partner_in_sirov">
            <!-- OVDJE UBACUJEM FIND PARTNER MODULE ZA DOBAVLJAČ ODABIR -->
          </div>
          
          <div class="col-md-1 col-sm-12" >
            <a  id="link_to_partner" href="#" target="_blank" onclick="event.stopPropagation();"
                class="blue_btn btn_small_text small_btn_link_left" 
                style="box-shadow: none; margin: 22px auto 0 0; width: 28px; height: 28px;" >

              <i style="font-size: 18px;" class="far fa-external-link-square-alt"></i>

            </a>
          </div>
          
        </div>
        
        <div class="row" style="padding: 0 15px;">
          <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="svi_kontakti_box" style="padding: 2px;" >

            <!-- OVDJE JE POPIS SVIH KONTAKTA OD OVE TVRTKE ID JE ISTI KAO NA PAGE-u OD PARTNERA -->
            <!-- ali pošto nikada te dvije stranice nisu otvorane u isto vrijeme onda je ok -->

          </div>
        </div>
        
       
        
        <div class="row" >
         
          <div class="col-md-12 col-sm-12" >
            ${ cit_comp(rand_id+`_roba_kupac`, valid.roba_kupac, data.roba_kupac, "" ) }
          </div>
          
        </div>  


        <div class="row" style="margin-top: 0; margin-bottom: 0; ">
          <h4 style="font-size: 15px; margin: 14px 0 7px 30px; font-weight: 700;">LISTA KUPACA</h4>
        </div>

        <div  class="row" 
              style="border-top: 5px solid #a4dafc; margin-top: -5px; border-bottom: 5px solid #a4dafc;" >
          <div  class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" 
                id="list_kupaca_box" style="padding-top: 20px;">
            <!-- OVDJE IDE LISTA SVIH KUPACA OVOG PROIZVODA -->    
          </div>
        </div>
                

        
        <div class="row" style="margin-top: 0; margin-bottom: 0;">
          <h4 style="font-size: 15px; margin: 14px 0 7px 30px; font-weight: 700;">O MATERIJALU</h4>
        </div>


        <div class="row spec_row" style="margin-top: -5px;" >
                  
          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_tip_sirovine`, valid.tip_sirovine, data.tip_sirovine, data.tip_sirovine?.naziv || "") }
          </div>
          
          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_klasa_sirovine`, valid.klasa_sirovine, data.klasa_sirovine,  data.klasa_sirovine?.naziv || "") }
          </div>
          
          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_vrsta_materijala`, valid.vrsta_materijala, data.vrsta_materijala,  data.vrsta_materijala?.naziv || "") }
          </div>
          
          
        </div>
        
        
    <!--  --------------------- VALOVITA LJEPENKA ---------------------  -->

        <div class="row" style="margin-top: 0; margin-bottom: 0;">
          <h4 style="font-size: 15px; margin: 14px 0 7px 30px; font-weight: 700;">VALOVITA LJEPENKA</h4>
        </div>
        
        <div class="row spec_row"  style="border-bottom: none; margin-top: -5px; margin-bottom: 0; padding-bottom: 0;" >
        
        
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_kvaliteta_1`, valid.kvaliteta_1, data.kvaliteta_1,  data.kvaliteta_1 || "") }
          </div>

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_kvaliteta_2`, valid.kvaliteta_2, data.kvaliteta_2,  data.kvaliteta_2 || "") }
          </div> 
 
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_po_valu`, valid.po_valu, data.po_valu) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_po_kontravalu`, valid.po_kontravalu, data.po_kontravalu) }
          </div>
          
          
        </div>
       
        <div class="row"  style="border-bottom: none; margin-top: 0; margin-bottom: 0; padding-bottom: 0;" >
         
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_sloj_1`, valid.sloj_1, data.sloj_1) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_val_1`, valid.val_1, data.val_1) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_sloj_2`, valid.sloj_2, data.sloj_2) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_val_2`, valid.val_2, data.val_2) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_sloj_3`, valid.sloj_3, data.sloj_3) }
          </div>
          
          
        </div>
       

       
              
                     
        <!--  --------------------- PAPIR ---------------------  -->                           
       
        <div class="row" style="margin-top: 0; margin-bottom: 0;">
          <h4 style="font-size: 15px; margin: 14px 0 7px 30px; font-weight: 700;">PAPIR</h4>
        </div>
        
        <div class="row spec_row" style="border-bottom: none; margin-top: -5px; margin-bottom: 0; padding-bottom: 0;" >
        
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_vrsta_papira`, valid.vrsta_papira, data.vrsta_papira,  data.vrsta_papira?.naziv || "") }
          </div>
          
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_premaz_papira`, valid.premaz_papira, data.premaz_papira,  data.premaz_papira?.naziv || "") }
          </div>
         
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_premaz`, valid.premaz, data.premaz) }
          </div>
         
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_kontra_tok`, valid.kontra_tok, data.kontra_tok) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_tok`, valid.tok, data.tok) }
          </div>
          

        </div>  
        
        
        <!--  --------------------- ALAT ---------------------  -->  
        
        <div class="row" style="margin-top: 0; margin-bottom: 0;">
          <h4 style="font-size: 15px; margin: 14px 0 7px 30px; font-weight: 700;">ALAT</h4>
        </div>
        
        <div class="row" style="margin-bottom: 20px; border-top: 5px solid #a4dafc; margin-top: -5px; padding-top: 13px;">
         
          <div class="col-md-11 col-sm-12" >
            ${ cit_comp(rand_id+`_alat_owner`, valid.alat_owner, data.alat_owner, "" ) }
          </div>
          
          <div class="col-md-1 col-sm-12" >
            <a  id="link_to_alat_owner" href="#" target="_blank" onclick="event.stopPropagation();"
                class="blue_btn btn_small_text small_btn_link_left" 
                style="box-shadow: none; margin: 22px auto 0 0; width: 28px; height: 28px;" >

              <i style="font-size: 18px;" class="far fa-external-link-square-alt"></i>

            </a>
          </div>
          
 
          
          
        </div>
        
        <div class="row spec_row" style="border-top: none; padding-top: 0; margin-top: 0;" >
        
        
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_alat_val`, valid.alat_val, data.alat_val ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_alat_kontra`, valid.alat_kontra, data.alat_kontra ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_alat_nest_count`, valid.alat_nest_count, data.alat_nest_count ) }
          </div>
          
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_alat_stamp_num`, valid.alat_stamp_num, data.alat_stamp_num ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_alat_max_stamp`, valid.alat_max_stamp, data.alat_max_stamp ) }
          </div>
          
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_alat_fi`, valid.alat_fi, data.alat_fi ) }
          </div>
          
          

        </div>
      
      
      
      
        
        
        <!--  --------------------- STROJ ---------------------  -->  
        
        <div class="row" style="margin-top: 0; margin-bottom: 0;">
          <h4 style="font-size: 15px; margin: 14px 0 7px 30px; font-weight: 700;">STROJ</h4>
        </div>        
      
        <div class="row" style="margin-bottom: 20px; border-top: 5px solid #a4dafc; margin-top: -5px; padding-top: 13px;">
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_stroj_kw`, valid.stroj_kw, data.stroj_kw ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_stroj_god`, valid.stroj_god, data.stroj_god ) }
          </div>
          
          
        </div>  
        
        
        <div class="row" style="margin-top: 0; margin-bottom: 0;">
          <h4 style="font-size: 15px; margin: 14px 0 7px 30px; font-weight: 700;">KUKICA</h4>
        </div> 
        
        <div class="row" style="margin-bottom: 20px; border-top: 5px solid #a4dafc; margin-top: -5px; padding-top: 13px;">

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_nosac_cijena`, valid.nosac_cijena, data.nosac_cijena ) }
          </div>

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_kapacitet_kukice`, valid.kapacitet_kukice, data.kapacitet_kukice ) }
          </div>
          
        </div>
        
        
        
        
        <div class="row" style="margin-top: 0; margin-bottom: 0;"><h4 style="font-size: 15px; margin: 14px 0 7px 30px; font-weight: 700;">JEDINICE</h4></div>
        
        
        <div class="row" style="margin-bottom: 10px; border-top: 5px solid #a4dafc; margin-top: -5px; padding-top: 13px;">

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_zemlja_pod`, valid.zemlja_pod, data.zemlja_pod,  data.zemlja_pod?.naziv || "") }
          </div>

          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_grupa`, valid.grupa, data.grupa,  data.grupa?.name || "" ) }
          </div>

          <div class="col-md-2 col-sm-12">
            <button class="blue_btn btn_small_text" id="edit_grupa_name" style="margin: 20px auto 0 0; box-shadow: none;">
              PROMJENI IME GRUPE
            </button>
          </div>

          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_new_grupa_naziv`, valid.new_grupa_naziv, "") }
          </div>

          <div class="col-md-1 col-sm-12">
              <button class="blue_btn btn_small_text" id="new_grupa_btn" style="margin: 20px auto 0 0; box-shadow: none;">
                NOVA GRUPA
              </button>
          </div>
          
          <div class="col-md-1 col-sm-12">
              <button class="blue_btn btn_small_text" id="remove_from_grupa" style="margin: 20px auto 0 0; box-shadow: none; background: #bf0060;">
                IZBACI IZ GRUPE
              </button>
          </div>

        </div>   

        <div class="row" style="margin-bottom: 20px; border-bottom: 5px solid #a4dafc; " >
          <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="grupa_box" style="padding-top: 20px;">
            <!-- OVDJE IDE LISTA SVIH SIROVINA UNUTAR OVE GRUPE -->    
          </div>
        </div>         



        <div class="row">

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_boja`, valid.boja, data.boja) }
          </div>

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_debljina_mm`, valid.debljina_mm, data.debljina_mm) }
          </div>
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_gramaza_g`, valid.gramaza_g, data.gramaza_g) }
          </div>
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_duzina`, valid.duzina, data.duzina) }
          </div>
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_sirina`, valid.sirina, data.sirina) }
          </div>
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_visina`, valid.visina, data.visina) }
          </div>

        </div>  

        <div class="row">

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_povrsina_m2`, valid.povrsina_m2, data.povrsina_m2) }
          </div>
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_volumen_m3`, valid.volumen_m3, data.volumen_m3) }
          </div>
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_volumen_l`, valid.volumen_l, data.volumen_l) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_masa_kg`, valid.masa_kg, data.masa_kg ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_promjer`, valid.promjer, data.promjer) }
          </div>

        </div>
        
        
          <div class="row" style="margin-bottom: 50px;">
            
            
            <div class="col-md-2 col-sm-12">
              ${ cit_comp(rand_id+`_min_order`, valid.min_order, data.min_order) }
            </div>
            

            <div class="col-md-2 col-sm-12">
              ${ cit_comp(rand_id+`_rok_dostave`, valid.rok_dostave, data.rok_dostave) }
            </div>

            <div class="col-md-2 col-sm-12">
              ${ cit_comp(rand_id+`_exp_month`, valid.exp_month, data.exp_month) }
            </div>
            
            <div class="col-md-2 col-sm-12">
              ${ cit_comp(rand_id+`_average_skart`, valid.average_skart, data.average_skart) }
            </div>


          </div>
        
        


        <div class="row">
          <div class="col-md-12 col-sm-12">
            ${ cit_comp(rand_id+`_povezani_nazivi`, valid.povezani_nazivi, data.povezani_nazivi) }
          </div>
        </div>

        <div class="row">
          <div class="col-md-12 col-sm-12">
            ${ cit_comp(rand_id+`_detaljan_opis`, valid.detaljan_opis, data.detaljan_opis) }
          </div>
        </div>
        

        <div class="row" style="margin-top: 20px; margin-bottom: 20px;" >
          <div class="col-md-6 col-sm-12">
           <div id="web_pristupi_box" style="float: left; overflow: hidden;">

           </div>
            <!-- OVDJE STAVLJAM SADRŽAJ IZ TEXT AREA KOJI PRETVORIM U HTML -->
          </div>    

          <div class="col-md-6 col-sm-12">
            ${ cit_comp(rand_id+`_web_pristupi`, valid.web_pristupi, data.web_pristupi) }
          </div>
        </div>      

        <div class="row">

          <div class="col-md-6 col-sm-12">
            ${ cit_comp(rand_id+`_specs_komentar`, valid.specs_komentar, "" ) }
          </div>

          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_sirov_docs`, valid.sirov_docs, "", "", `margin: 20px auto 0 0;` ) }
          </div>

          <div class="col-md-3 col-sm-12">

            <button class="blue_btn btn_small_text" id="save_specs" style="margin: 20px auto 0 0; box-shadow: none;">
              SPREMI SPECIFIKACIJE
            </button>

            <button class="violet_btn btn_small_text" id="update_specs" style="margin: 20px auto 0 0; box-shadow: none; display: none;">
              AŽURIRAJ SPECIFIKACIJE
            </button>

          </div>

        </div>

        <div class="row">
          <div  class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" 
                id="sirov_specs_box" style="padding-top: 20px;">
              <!-- OVDJE IDE LISTA SVIH SPECIFIKACIJA ZA OVU SIROVINU -->    
          </div> 
        </div>



        <div class="row" >

          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_rabat_min`, valid.rabat_min, "") }
          </div>

          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_rabat_max`, valid.rabat_max, "") }
          </div>


          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_rabat_perc`, valid.rabat_perc, "") }
          </div>

          <div class="col-md-3 col-sm-12">

            <button class="blue_btn btn_small_text" id="save_rabat" style="margin: 20px auto 0 0; box-shadow: none;">
              SPREMI RABAT
            </button>

            <button class="violet_btn btn_small_text" id="update_rabat" style="margin: 20px auto 0 0; box-shadow: none; display: none;">
              AŽURIRAJ RABAT
            </button>

          </div>

        </div>

        <div class="row">
          <div  class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" 
                id="sirov_rabats_box" style="padding-top: 20px;">
            <!-- OVDJE IDE LISTA RABATA TVRTKE -->    
          </div> 
        </div>
        
        
        
        <div class="row" style="margin-top: 30px; margin-bottom: 0;">
          <h4 style="font-size: 15px; margin: 14px 0 7px 30px; font-weight: 700;">TRENUTNA CIJENA</h4>
        </div> 
        
        
        <div class="row spec_row" style="margin-top: -5px; margin-bottom: 0; border-bottom: none; ">

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_cijena`, valid.cijena, data.cijena) }
          </div> 

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_alt_cijena`, valid.alt_cijena, data.alt_cijena) }
          </div> 
          
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_pro_cijena`, valid.pro_cijena, data.pro_cijena) }
          </div> 
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_m2_cijena`, valid.m2_cijena, data.m2_cijena) }
          </div>
          
          
          <div class="col-md-2 col-sm-12">
            <button class="blue_btn btn_small_text" id="update_all_m2" style="margin: 20px auto 0 0; box-shadow: none; height: 30px;">
              AŽURIRAJ SVE CIJENE U m<sup>2</sup>
            </button>
          </div>
          
       
        </div>
        
        
                
        <div class="row" >


          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_osnovna_jedinica`, valid.osnovna_jedinica, data.osnovna_jedinica,  data.osnovna_jedinica?.naziv || "") }
          </div>   

          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_osnovna_jedinica_out`, valid.osnovna_jedinica_out, data.osnovna_jedinica_out,  data.osnovna_jedinica_out?.naziv || "") }
          </div>  


          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_normativ_omjer`, valid.normativ_omjer, data.normativ_omjer) }
          </div> 
          

          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_min_order_unit`, valid.min_order_unit, data.min_order_unit,  data.min_order_unit?.naziv || "") }
          </div>


          

        </div>        
        
        
        

      
      
        

        
        <div class="row" style="margin-top: 0; margin-bottom: 0;">
          <h4 style="font-size: 15px; margin: 14px 0 7px 30px; font-weight: 700;">POVIJEST CIJENA</h4>
        </div> 
        
        
        <div class="row spec_row" style="margin-top: -5px; margin-bottom: 20px; border-bottom: none; ">
        
        
         
          <div class="col-md-6 col-sm-12">
            ${ cit_comp(rand_id+`_choose_spec`, valid.choose_spec, null,  "") }
          </div>
          
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_price_hist_date`, valid.price_hist_date, null,  "") }
          </div>
          

          <div class="col-md-2 col-sm-12">
            <button class="blue_btn btn_small_text" id="save_price_hist_btn" style="margin: 20px 0 0 auto; box-shadow: none;">
              SPREMI CIJENU U POVIJEST
            </button>
          </div>
          
        </div>        
        
        
        <div class="row spec_row" style="margin-top: 20px;  border-top: none;  padding-left: 15px;  padding-right: 15px;" >
         
          <div  id="price_hist_box"  
                class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" 
                style="padding: 2px; max-height: 500px; overflow-y: auto;" >

            <!-- OVDJE IDE TABLICA POVIJEST CIJENA -->

          </div>

          
        </div>
              
      
      
      
      

      
      <div class="row" style="margin-top: 20px; margin-bottom: 20px;">
        
        <div class="col-md-12 col-sm-12 cit_gallery_box" id="${ rand_id + `_gallery_box` }">
         
        
        </div>
        
      </div>
      
      <div class="row">
        <div class="col-md-12 col-sm-12" style="justify-content: center;">
          ${ cit_comp(rand_id+`_sirov_pics`, valid.sirov_pics, "", "", `margin: 10px auto; max-width: 200px;`, `for_gallery` ) }
        </div>
      </div>
      

      </div>       
     
      <div class="cit_tab_box povijest" style="display: none;" >
        



       
             
        <!--    
        <div class="row" style="margin-top: 50px;">
         
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_ask_kolicina_1`, valid.ask_kolicina_1, "") }
          </div>

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_ask_kolicina_2`, valid.ask_kolicina_2, "") }
          </div>

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_ask_kolicina_3`, valid.ask_kolicina_3, "") }
          </div>

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_ask_kolicina_4`, valid.ask_kolicina_4, "") }
          </div>

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_ask_kolicina_5`, valid.ask_kolicina_5, "") }
          </div>

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_ask_kolicina_6`, valid.ask_kolicina_6, "") }
          </div>
          
        </div>                    
        <div class="row" >
        
          <div class="col-md-6 col-sm-12">
            <div class="row">
             
              <div class="col-md-4 col-sm-12">
                ${ cit_comp(rand_id+`_doc_lang`, valid.doc_lang, { sifra: `hr`, naziv: `HRVATSKI` },  "HRVATSKI" ) }
              </div>

              <div class="col-md-4 col-sm-12">
                ${ cit_comp(rand_id+`_doc_valuta`, valid.doc_valuta, { "sifra": "DEFV1", "naziv": "Hrvatska", "valuta": "HRK" },  "HRK") }
              </div>

              <div class="col-md-4 col-sm-12">
                ${ cit_comp(rand_id+`_doc_valuta_manual`, valid.doc_valuta_manual, "") }
              </div>
            
            </div>
          
          </div> 
          
          <div class="col-md-6 col-sm-12">
            <div class="row">
             
              <div class="col-md-12 col-sm-12">
                ${ cit_comp(rand_id+`_doc_adresa`, valid.doc_adresa, null,  "" ) }
              </div>
              
            </div>
          </div> 
          
        </div> 
        <div class="row">
        
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_record_est_time`, valid.record_est_time, null, "") }
          </div>
         
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_order_price`, valid.order_price, "") }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_doc_ponuda_num`, valid.doc_ponuda_num, "") }
          </div>
          
          <div class="col-md-6 col-sm-12">
            ${ cit_comp(rand_id+`_find_status_for_sirov_record`, valid.find_status_for_sirov_record, null, "") }
          </div>
          
        </div>
        -->
        
        
        <div class="row">
          <div  class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" 
                id="selected_statuses_box" style="padding-top: 20px;">
              <!-- OVDJE IDE LISTA SVIH STATUSA NA KOJE ŽELIM POSLATI STATUS ODGOVOR -->    
          </div> 
        </div>  
                
                                
     
      <div class="row" style="justify-content: center;">
      
        <!--
        <div class="col-md-2 col-sm-12">
          <button class="blue_btn btn_small_text" id="sirov_offer" style="margin: 20px auto; box-shadow: none; min-width: 130px;">
            TRAŽI PONUDU
          </button>
        </div> 
        -->
        
        <!--
        <div class="col-md-2 col-sm-12">
          <button class="blue_btn btn_small_text" id="sirov_order" style="margin: 20px auto; box-shadow: none; min-width: 130px;">
            NARUČI
          </button>
        </div>
        -->
        

        
      </div>     
      
      
      <div class="row sirov_records_graf_box cit_closed">
      
        <i class="fal fa-angle-down graf_arrow"></i>
        
        <h5 class="graf_naslov" >
          Projekcije količina
        </h5>
        
        <div class="legend_box">
         
          <div class="legend_color cit_color_1"></div> REZERV. PO PK
          <div class="legend_color cit_color_2"></div> REZERV. PO NK
          <div class="legend_color cit_color_3"></div> RADNI NALOG
          <div class="legend_color cit_color_4"></div> NA SKLADIŠTU
          <div class="legend_color cit_color_5"></div> PRIMKA / PROIZ.
        </div>
        
        
        <div class="blueberryChart cit_graf" style="height: 500px; overflow: visible;">
           
          <!-- OVDJE IDE GRAF ZA PROJEKCIJU SIROVINE -->
      
        </div>
        
      </div>   
       
       
      <div class="row">
        <div  class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" 
              id="sirov_records" style="padding-top: 20px;">
          <!-- OVDJE IDE LISTA SVIH RECORDA ZA OVU SIROVINU-->    
        </div>
      </div>

      </div>

      <div class="cit_tab_box statusi" style="display: none;" >

        <div class="row" id="${data.id}_statuses_box" style="padding: 0; margin: 0;" >
          <!--OVDJE UBACUJEM STATUSE-->
        </div> 

      </div>
     
      <div class="cit_tab_box skladiste" style="display: none;" >
        
        <!-- SKLADIŠTE TAB CONT -->
        
        
        
        <div class="row" style="margin-top: 20px; padding: 0 15px;" >
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_min_kolicina`, valid.min_kolicina, data.min_kolicina) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_max_kolicina`, valid.max_kolicina, data.max_kolicina) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_palete_free`, valid.palete_free, data.palete_free) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_palete_taken`, valid.palete_taken, data.palete_taken) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_palete_broken`, valid.palete_broken, data.palete_broken) }
          </div>
          
          
        </div>
      
        <div class="row" style="padding: 0 15px;" >
        
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_sklad_kolicina`, valid.sklad_kolicina, data.sklad_kolicina || "" ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_order_kolicina`, valid.order_kolicina, data.order_kolicina || "" ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_pk_kolicina`, valid.pk_kolicina, data.pk_kolicina || "" ) }
          </div>
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_nk_kolicina`, valid.nk_kolicina, data.nk_kolicina || "" ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_rn_kolicina`, valid.rn_kolicina, data.rn_kolicina || "" ) }
          </div>
          
          
        </div>          
         
        <div class="row" style="padding: 0 15px;" >
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_osnovna_neto_masa_kg`, valid.osnovna_neto_masa_kg, data.osnovna_neto_masa_kg) }
          </div>
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_osnovna_bruto_masa_kg`, valid.osnovna_bruto_masa_kg, data.osnovna_bruto_masa_kg) }
          </div> 

          

        </div>
        
        
        <h4 style="margin-top: 30px; padding-left: 30px; ">Među-skladišnice:</h4>
        
        
        <div class="row" style="background: antiquewhite; padding: 20px 10px;" >
        
        
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_choose_primka`, valid.choose_primka, data.choose_primka || null,  data.choose_primka?.naziv || "") }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_choose_location_from`, valid.choose_location_from, null, "") }
          </div>

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_choose_pro_primka`, valid.choose_pro_primka, data.choose_pro_primka || null,  data.choose_pro_primka?.naziv || "") }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_choose_rn`, valid.choose_rn, data.choose_rn || null,  data.choose_rn?.doc_sifra || "") }
          </div>
          
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_empty_cards_count`, valid.empty_cards_count, "") }
          </div>
          
          <div class="col-md-2 col-sm-12">
            <button class="blue_btn btn_small_text" 
                    id="empty_QR_cards_btn" 
                    style="margin: 20px 0 0 0; box-shadow: none; width: 200px;" >
              NAPRAVI PRAZNE KARTICE
            </button>
          </div>
          
  
        </div>
        
        
        <div class="row" style="background: antiquewhite; padding: 0 10px 20px;" >
         
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(
              rand_id+`_write_skl_card_num`,
              valid.write_skl_card_num,
              data.write_skl_card_num,
              "",
              `text-transform: uppercase; font-size: 23px; letter-spacing: 0.08rem; font-weight: 700; padding: 0; text-align: center;` 
            )}
          </div>
          
          <div class="col-md-3 col-sm-12">
            <button class="blue_btn btn_small_text" 
                    id="card_num_btn" 
                    style="margin: 20px 0 0 0; box-shadow: none; width: 150px;" >
              UBACI SKL. ŠIFRU
            </button>
          </div>
          
        </div>
        
        <div class="row" style="background: antiquewhite; padding: 0 10px 20px; " >
          <div class="col-md-12 col-sm-12" id="card_num_list_box">
            <!--          
            <div  class="skl_card_pill">86F2<i class="far fa-times"></i></div>                
            <div  class="skl_card_pill">55DC<i class="far fa-times"></i></div>
            <div  class="skl_card_pill">86F2<i class="far fa-times"></i></div>
            <div  class="skl_card_pill">55DC<i class="far fa-times"></i></div>
            -->
          </div>
        </div>
        
        
        
        <div class="row" style="margin-top: 0; margin-bottom: 0;"><h4 style="font-size: 15px; margin: 14px 0 7px 30px; font-weight: 700;">OSNOVNI PODACI</h4></div> 
        
        <div class="row" style="background: antiquewhite; border-top: 5px solid #e0cbaf; padding: 20px 10px 20px;" >
        
        
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_palet_count`, valid.palet_count, data.palet_count || null ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_palet_unit_count`, valid.palet_unit_count, data.palet_unit_count || null ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_box_count_on_palet`, valid.box_count_on_palet, data.box_count_on_palet || null ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_box_count`, valid.box_count, data.box_count || null ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_box_unit_count`, valid.box_unit_count, data.box_unit_count || null ) }
          </div>
          
          
        </div>  
          
          
          
        <div class="row" style="margin-top: 0; margin-bottom: 0;">
          <h4 style="font-size: 15px; margin: 14px 0 7px 30px; font-weight: 700;">ROBA U PAKIRANJU NA PALETI</h4>
        </div>   
        
        
        <div class="row" style="background: antiquewhite; border-top: 5px solid #e0cbaf; padding: 20px 10px 20px;" >
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_red_count_on_palet`, valid.red_count_on_palet, data.red_count_on_palet || null ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_box_count_in_red`, valid.box_count_in_red, data.box_count_in_red || null ) }
          </div>
          
        
        </div>
        
        
        <div class="row" style="background: antiquewhite; padding: 0 10px 20px;" >
           
        
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_paleta_x`, valid.paleta_x, data.paleta_x ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_paleta_y`, valid.paleta_y, data.paleta_y) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_paleta_z`, valid.paleta_z, data.paleta_z) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_neto_masa_palete_kg`, valid.neto_masa_palete_kg, data.neto_masa_palete_kg) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_bruto_masa_palete_kg`, valid.bruto_masa_palete_kg, data.bruto_masa_palete_kg) }
          </div>
          
          
          
        </div>
        
        
        <div class="row" style="margin-top: 0; margin-bottom: 0;">
          <h4 style="font-size: 15px; margin: 14px 0 7px 30px; font-weight: 700;">ROBA U PAKIRANJU BEZ PALETE</h4>
        </div>   
        
        <div class="row" style="background: antiquewhite; padding: 20px 10px 20px; border-top: 5px solid #e0cbaf;" >
        
                  

          
        
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_box_x`, valid.box_x, data.box_x ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_box_y`, valid.box_y, data.box_y) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_box_z`, valid.box_z, data.box_z) }
          </div>
        
        </div>
        
        
        <div class="row" style="background: antiquewhite; padding: 0 10px 20px;" >
        
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_neto_masa_paketa_kg`, valid.neto_masa_paketa_kg, data.neto_masa_paketa_kg) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_bruto_masa_paketa_kg`, valid.bruto_masa_paketa_kg, data.bruto_masa_paketa_kg) }
          </div> 
          
        </div>  
        
        
        <div class="row" style="background: antiquewhite; padding: 20px 10px 20px; border-top: 5px solid #e0cbaf;" >
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_sklad`, valid.sklad, data.sklad,  data.sklad?.naziv || "") }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_sector`, valid.sector, data.sector,  data.sector?.naziv || "") }
          </div>
                
                
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_level`, valid.level, data.level,  data.level?.naziv || "") }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_alat_shelf`, valid.alat_shelf, data.alat_shelf,  data.alat_shelf?.naziv || "") }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_loc_est_date`, valid.loc_est_date, null, "") }
          </div>

          
          
        </div>
        
        <div class="row" style="background: antiquewhite; padding: 0px 10px 20px;"  >
        
          <div class="col-md-12 col-sm-12" >
            ${ cit_comp(rand_id+`_loc_komentar`, valid.loc_komentar, data.loc_komentar  ) }
          </div>
        
        </div>
        
        
        <div class="row" style="background: antiquewhite; padding: 0px 10px 20px;" >

          
          <!--
          <div class="col-md-2 col-sm-12">
            <button class="blue_btn btn_small_text" 
                    id="sirov_primka_prodon_btn" 
                    style="margin: 20px auto; box-shadow: none; min-width: 130px; background: #bf0060; width: 150px;" >
              PRIMKA IZ PROIZ.
            </button>
          </div>
          -->

          <div class="col-md-2 col-sm-12">
          
          
            <button class="blue_btn btn_small_text" 
                    id="save_sirov_location_btn" 
                    style="margin: 20px auto; 
                    box-shadow: none; min-width: 130px; width: 150px;" >
              KREIRAJ MS
            </button>
            
            <button class="violet_btn btn_small_text" 
                    id="update_sirov_location_btn" 
                    style="margin: 20px auto; box-shadow: none; min-width: 130px; display: none; width: 150px;" >
              AŽURIRAJ OVU MS
            </button>
            
            
            <button class="violet_btn btn_small_text" 
                    id="save_variant_sirov_location_btn" 
                    style="margin: 20px auto; box-shadow: none; min-width: 130px; display: none; background: #bf0060;  width: 150px;" >
              VARIJANTA MS
            </button>
          
          
            
          </div>

          <div class="col-md-2 col-sm-12">
          
            <button class="violet_btn btn_small_text" 
                    id="new_skl_card_btn" 
                    style="margin: 20px auto; box-shadow: none; min-width: 130px; background: #bf0060; width: 150px;" >
              KREIRAJ SKL. KART.
            </button>
          
           
            <button class="blue_btn btn_small_text" 
                    id="print_skl_card_btn" 
                    style="margin: 20px auto; box-shadow: none; min-width: 130px; width: 150px;" >
              PRINT SKL. KART.
            </button>
            
            
          </div>
          
          <div class="col-md-2 col-sm-12">
          
          
            <button class="violet_btn btn_small_text" 
                    id="new_exit_card_btn" 
                    style="margin: 20px auto; box-shadow: none; min-width: 130px; background: #bf0060; width: 150px;" >
              KREIRAJ IZLAZ KART.
            </button>
          
           
            <button class="blue_btn btn_small_text" 
                    id="print_exit_card_btn" 
                    style="margin: 20px auto; box-shadow: none; min-width: 130px; width: 150px;" >
              PRINT IZLAZ KART.
            </button>
            
          </div>
          
          
          <div class="col-md-2 col-sm-12">
           
            <button class="blue_btn btn_small_text" 
                    id="logistic_data_btn" 
                    style="margin: 20px auto; box-shadow: none; min-width: 130px; background: #1e7d01; width: 150px;" >
              LOGISTIČKI PODACI
            </button>
            
          </div>
          
          
          
        </div>
        
        
        
        
        
        
        
        <!-- OVDJE GENERIRAM QR CODE -->
        <!--
        <div class="row">

          <div class="col-md-6 col-sm-12">
            
            
            <div class="row">
              
              <div class="col-md-6 col-sm-12" id="cit_qrcode" style="padding-top: 20px;">
                
              </div>
              
              
              <div class="col-md-6 col-sm-12">
                
                
                
              </div>
              
              
              
            </div>
            
            

          </div>

          <div class="col-md-6 col-sm-12">

          </div>


        </div>
        -->
        
        <div class="row" style="background: antiquewhite; padding: 20px 10px 20px;" >
        
          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_find_sirov_location`, valid.find_sirov_location, data.find_sirov_location || null, "") }
          </div>

        </div>
        
        
        <div class="row" style="padding-left: 15px; padding-right: 15px;">
          <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="sirov_locations_box" style="padding: 0px;">
              <!-- OVDJE IDE LISTA SVIH LOKACIJA -->    
          </div> 
        </div>
        
        
      </div> <!-- KRAJ TABA SKLADIŠTE -->
      
      
      <div class="cit_tab_box proizvodi" style="display: none;" >
      
        <div class="row" style="margin-top: 40px;">
          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_choose_linked_products`, valid.choose_linked_products, null, "" ) }
          </div>

        </div>

        <div class="row">
          <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="kupci_proizvodi_box" style="padding-top: 20px;">
              <!-- OVDJE IDE LISTA SVIH PARTNERA I NJIHOVIH PROIZVODA -->    
          </div> 
        </div>

      </div>
      
      <div class="cit_tab_box police_alata" style="display: none;" >
       

       
        <!-- OVDJE IDU POLICE ALATA -->
        <div class="row">
          <div  class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" 
                id="alat_shelfs_box" style="padding-top: 20px;">
            <!--- OVDJE IDE TABLICA POLICA ALATA -->    
          </div>
        </div>
        
      </div>
      
      
      
      <div class="cit_tab_box vozilo" style="display: none;" >
       
        <!-- OVDJE IDU PODACI VOZILA  -->
        
        
        <div class="row" style="margin-top: 30px;" >
        
          <div class="col-md-2 col-sm-12">
            ${cit_comp(rand_id+`_voz_user`, valid.voz_user, data.voz_user, data.voz_user?.full_name || "" )}
          </div>
        
        
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_voz_marka`, valid.voz_marka, data.voz_marka) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_voz_reg`, valid.voz_reg, data.voz_reg) }
          </div> 
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_voz_sas`, valid.voz_sas, data.voz_sas) }
          </div> 
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_voz_buy_date`, valid.voz_buy_date, null,  "") }
          </div>
          
          
          
        </div>  
        
        <div class="row">
       
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_voz_leasing`, valid.voz_leasing, data.voz_leasing) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_voz_leasing_house`, valid.voz_leasing_house, data.voz_leasing_house) }
          </div> 
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_voz_leasing_exp_date`, valid.voz_leasing_exp_date, null,  "") }
          </div>
          
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_voz_last_servis_date`, valid.voz_last_servis_date, null,  "") }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_voz_reg_exp_date`, valid.voz_reg_exp_date, null,  "") }
          </div>
       
       
        </div>
        
        

        
        
        <div class="row">
         <br>
         <br>
         
          treba upozoriti tjedan dana prije isteka reg na obavljanje servisa <br>
            <br>
            i upozoriti za obnavljanje osiguranja i registracije<br>

          
        </div>
       
                       
        
        
        <div class="row">
          <div  class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" 
                id="xxxxxxx___" style="padding-top: 20px;">
            <!--- OVDJE IDE NEKA TABLICA -->   
          </div> 
        </div>
        
        
        
        
        
        
        
        
        
      </div>
      <!--KRAJ TAB BOX ZA VOZILA-->
      
      
      


    </div>  
  </section>
</div>

`;


  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };
  
  wait_for( `$('#${data.id}').length > 0`, async function() {
    
    console.log(data.id + 'component injected into html');
    toggle_global_progress_bar(false);
    
    $(`#cit_page_title h3`).text(`Resursi`);
    
    
    if ( data._id ) {
      document.title = ( data.sirovina_sifra + "--" + window.concat_naziv_sirovine(data) + "--" + (data.dobavljac?.naziv || "") );
    } else {
      document.title = "RESURSI VELPROM";
    };
    
    
    
    ask_before_close_register_event();

    
    // Instantiate $.blueberryChart
    $('.cit_graf').blueberryChart({
      chartData: [
        [0, 20, 33, 10, 54, 90, 70, 84, 95, 100],
        [0, 30, 23, 20, 44, 60, 10, 50, 10, 40]
      ],
      chartLabeles: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'],
      showLines: true,
      showDots: true,
      lineColors: ['#FF5252', '#777777']
    });
    
    
    // $('.cit_tooltip').tooltip();
    $('.cit_tooltip').tooltip('dispose').tooltip({ boundary: 'window' });
    
    
    setTimeout( function() {
    
      var status_data = {
        id: `sirovstatus`+cit_rand(),
        statuses: data.statuses,
        
        statuses_count: data.statuses_count || null,
        
        goto_status: (data.goto_status || null),
        for_module: `sirov`,

      };

      this_module.status_module.create( status_data, $(`#${data.id}_statuses_box`) );

    }, 100);  
    
    
    var rand_id = this_module.cit_data[data.id].rand_id;
    
    // ----------------------------------------------------------------------
    this_module.find_partner = await get_cit_module(`/modules/partners/find_partner.js`, null);
    
    if ( !this_module.find_partner ) {
      toggle_global_progress_bar(false);
      return;
    };
    
    var find_partner_data = {
      id: rand_id+`_find_partner`,
      for_module: "sirov",
      pipe: this_module.make_kontakti_in_sirov,
    };
    
    this_module.find_partner.create( find_partner_data, $(`#find_partner_in_sirov`) );    
    
    wait_for(`$("#find_partner_in_sirov").find(".single_select").length > 0`, function() {
      this_module.make_kontakti_in_sirov(data.dobavljac);
    }, 5*1000);

    // ----------------------------------------------------------------------
    
    
    $(`html`)[0].scrollTop = 0;
    
    /*
    
    var qrcode = new QRCode("cit_qrcode", {
        text: "#sirov/asd35aa4d3a5da4d3ad/warehouse/true",
        width: 256,
        height: 256,
        colorDark : "#000000",
        colorLight : "#ffffff",
        correctLevel : QRCode.CorrectLevel.H
    });
    
    */


    $(`#tab_police_alata`).off(`click`);
    $(`#tab_police_alata`).on(`click`, this_module.make_alat_shelfs_list );
    
    
    
    $(`#show_sirov_scan_btn`).off(`click`);
    $(`#show_sirov_scan_btn`).on(`click`,  function() {
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      $(`#scaner_popup`).addClass(`cit_show`);
      
      this_module.scaner_start(data);
      
      
    });
    
    
    $(`#close_scaner`).off(`click`);
    $(`#close_scaner`).on(`click`,  function() {
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      $(`#scaner_popup`).removeClass(`cit_show`);
      
      this_module.scaner_stop(data);
      
      
    });
    
    
    
    
    
    
    $(`#copy_sirov_btn`).off(`click`);
    $(`#copy_sirov_btn`).on(`click`, this_module.copy_sirov );
    
    
    $(`#save_sirov_btn`).off(`click`);
    $(`#save_sirov_btn`).on(`click`, this_module.save_sirov );
    

    $(`#save_rabat`).off(`click`);
    $(`#save_rabat`).on(`click`, this_module.save_current_sirov_rabat );
    
    $(`#update_rabat`).off(`click`);
    $(`#update_rabat`).on(`click`, this_module.save_current_sirov_rabat );
    
    
    $(`#${data.id} .cit_input.number`).off(`blur`);
    $(`#${data.id} .cit_input.number`).on(`blur`, function() {
      
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      if ( window.sirov_find_mode ) {
        gen_find_query_number(this_module, this);
        return;
      }; // kraj jel FIND MODE
      
      if ( !data ) return;
      
      
      // na silu upiši ovaj property ako ga nema !!!!
      if ( this.id.indexOf(`doc_valuta_manual`) > -1 ) {
        if ( !data.doc_valuta_manual ) data.doc_valuta_manual = null;
      };
      
      
      set_input_data(this, this_module.cit_data[data.id] );
      
      if ( this.id.indexOf(`_alt_cijena`) > -1 ) {
        
        // ako je upisan omjer i ako je upisana alt cijena
        if ( data.normativ_omjer && data.alt_cijena ) {
          data.cijena = data.alt_cijena / data.normativ_omjer;
          $(`#` + rand_id+ `_cijena`).val( cit_format(data.cijena, 3) );
        };
        
      };
      
      // ako nije baš polje za m2 cijenu
      if ( this.id.indexOf(`_m2_cijena`) == -1 ) this_module.set_m2_price(this);
      
      // ako JESTE baš polje za m2 cijenu ---> ovo više ne koristim jer sam zaključao cijenu po kvadratu (input polje)
      // if ( this.id.indexOf(`_m2_cijena`) > -1 ) this_module.set_kom_price(this);
      
    });
    
    
    $(`#${data.id} .cit_input.string`).off(`blur`);
    $(`#${data.id} .cit_input.string`).on(`blur`, function() {
  
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      if ( !data ) return;
      
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      set_input_data(this, this_module.cit_data[data.id] );
      
    });    
    
    
    $(`#${data.id} .cit_text_area`).off(`blur`);
    $(`#${data.id} .cit_text_area`).on(`blur`, function() {
            
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      set_input_data(this, this_module.cit_data[data.id] );
      
    });

    
    
    $('#'+rand_id+`_write_skl_card_num`).off('keypress');
    $('#'+rand_id+`_write_skl_card_num`).on('keypress', function (e) {

      e.stopPropagation();
      // e.preventDefault();
      
      if (e.keyCode === 13) {   $(`#card_num_btn`).trigger(`click`);   };
    });
    
    
    
    $(`#${data.id} .graf_arrow`).off(`click`);
    $(`#${data.id} .graf_arrow`).on(`click`, function() {
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var parent_box =  $(this).closest(`.sirov_records_graf_box`);
      
      if ( parent_box.hasClass(`cit_closed`) == true ) {
        parent_box.removeClass(`cit_closed`);
        
      } else {
        parent_box.addClass(`cit_closed`);
      };
      
      
    });
    

    $('#'+rand_id+'_record_est_time').data(`cit_run`, this_module.choose_record_est_time );
    
    $('#'+rand_id+'_nosac_cijena').data(`cit_run`, function(state) { data.nosac_cijena = state; } );
    
    $('#'+rand_id+'_for_search').data(`cit_run`, function(state) { data.for_search = state; } );
    
    
    $(`#`+rand_id+`_find_sirov_original`).data('cit_props', {
      
      desc: 'pretraga originalnog producta u modulu sirov koji tada postaje sirovina :) ',
      local: false,
      url: '/find_product',
      
      find_in: [
        "sifra",
        "full_product_name",
        "full_kupac_name",
        "mater",
        "komentar",
        "interni_komentar",
      ],
      return: {},
      /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */
      show_cols: [
        "sifra",
        "full_product_name",
        "full_kupac_name",
        "komentar",
        "interni_komentar",
      ],
      col_widths: [
        1, // "sifra",
        3, // "full_product_name",
        3, // "full_kupac_name",
        3, // "komentar",
        3, // "interni_komentar",
      ],
      format_cols: {
        sifra: "center",
      },
      query: {},
      
      filter: null,
      
      show_on_click: false,
      /*list_width: 700,*/
      
      cit_run: this_module.open_sirov_original, // kraj cit run
      
      
    });  
    
    
    
    
    
    
    
    if ( data.original ) {
      
      $('#'+ data.id + ' .link_to_original').attr(
        `href`, 
        `#project/${data.original.proj_sifra || null }/item/${data.original.item_sifra || null }/variant/${ data.original.variant || null }/status/null`
      );    
      
    };
    
    
    this_module.make_list_kupaca(data);  
    
    
    $(`#`+rand_id+`_roba_kupac`).data('cit_props', {
      
      // !!!findpartner!!!
      
      desc: 'pretraga partnera u modulu sirov ali za _roba kupac kada se radi o VELPROM PROIZVODU',
      local: false,
      url: '/find_partner',
      find_in: [
        "partner_sifra",
        "oib",
        "vat_num",
        "naziv",
        "grupacija_flat",
        "status",
        "status_flat",
        "komentar_all_adrese",
        "komentar_all_kont",
        "kontakti_flat",
        "adrese_flat",
        "public_komentar",
        "private_komentar",
        "skla_komentar",
      ],
      return: {},
      /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */
      col_widths:[
        1, // "sifra_link",
        1, // "oib",
        3, // "naziv",
        1, // "grupacija_naziv",
        1, // "status_naziv",
        5, // "sjediste",
      
      ],
      show_cols: [
        "sifra_link",
        "oib",
        "naziv",
        "grupacija_naziv",
        "status_naziv",
        "sjediste",
      ],
      format_cols: {
        "sifra_link": "center",
        "oib": "center",
        "grupacija_naziv": "center",
        "status_naziv": "center",
      },
      query: {},
      filter: this_module.find_partner.find_partner_filter,
      
      show_on_click: false,
      list_width: 700,
      
      cit_run: async function(state) {
        
        console.log(state);
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        
        if ( window.sirov_find_mode ) {
          
          
          if ( state ) {
            
            // ----------------------------------- AKO JE IZABRAO KUPCA ZA PRETRAGU PRIKAŽI GA U INPUT POLJU 
            // inače ako nije za pretragu ------ > ga odmah stavi u donju listu !!!
            
            var full_partner = 
              ( state?.naziv || "") + 
              ( state?.grupacija_naziv ? (", " + state?.grupacija_naziv) : "" ) + 
              ( state?.sjediste ? (", " + state?.sjediste) : "" );
              $('#'+current_input_id).val(full_partner);

          };
          
          gen_find_query_select( this_module,  state || null, "list_kupaca", state?._id || null );
          return;
        };

        // stani ako je user obrisao sve u inputu !!!!
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          return;
        };

        
        var state = cit_deep(state);
        data.list_kupaca =  upsert_item( data.list_kupaca, state, `_id` ) ;
        
        this_module.make_list_kupaca(data);
 
        
      },
    });  
    
    
    this_module.fill_alat_owner(data);
    
    $(`#`+rand_id+`_alat_owner`).data('cit_props', {
      
      // !!!findpartner!!!
      
      desc: 'pretraga partnera u modulu sirov ali za vlasnika alata',
      local: false,
      url: '/find_partner',
      find_in: [
        "partner_sifra",
        "oib",
        "vat_num",
        "naziv",
        "grupacija_flat",
        "status",
        "status_flat",
        "komentar_all_adrese",
        "komentar_all_kont",
        "kontakti_flat",
        "adrese_flat",
        "public_komentar",
        "private_komentar",
        "skla_komentar",
      ],
      return: {},
      /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */
      col_widths:[
        1, // "sifra_link",
        1, // "oib",
        3, // "naziv",
        1, // "grupacija_naziv",
        1, // "status_naziv",
        5, // "sjediste",
      
      ],
      show_cols: [
        "sifra_link",
        "oib",
        "naziv",
        "grupacija_naziv",
        "status_naziv",
        "sjediste",
      ],
      format_cols: {
        "sifra_link": "center",
        "oib": "center",
        "grupacija_naziv": "center",
        "status_naziv": "center",
      },
      query: {},
      filter: this_module.find_partner.find_partner_filter,
      
      show_on_click: false,
      list_width: 700,
      
      cit_run: async function(state) {
        
        console.log(state);
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        
        if ( window.sirov_find_mode ) {
          gen_find_query_select( this_module,  state || null, "alat_owner._id", state?._id || null );
          return;
        };

        // stani ako je user obrisao sve u inputu !!!!
        if ( $('#'+current_input_id).val() == "" || state == null ) {
          $('#'+current_input_id).val(``);
          $("#link_to_alat_owner").attr(`href`, `#`);
          data.alat_owner = null;
          return;
        };
                
        var state = cit_deep(state);
        
        data.alat_owner = state;
        
        this_module.fill_alat_owner(data );
        
      },
    });  
    
    

    $(`#`+rand_id+`_osnovna_jedinica`).data('cit_props', {
      desc: 'odabir osnovna_jedinica unutarnja u velpromu u sirov module',
      local: true,
      show_cols: ['jedinica', 'naziv'],
      return: {},
      show_on_click: true,
      list: 'jedinice',
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( window.sirov_find_mode ) {
          gen_find_query_select( this_module, state || null, "osnovna_jedinica.sifra", state?.sifra || null );
          return;
        };
        
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.osnovna_jedinica = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.osnovna_jedinica = state;
        };
        
      },
    });  


    
    $(`#`+rand_id+`_osnovna_jedinica_out`).data('cit_props', {
      desc: 'odabir osnovna_jedinica VANJSKA od dobavljača u sirov module',
      local: true,
      show_cols: ['jedinica', 'naziv'],
      return: {},
      show_on_click: true,
      list: 'jedinice',
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        if ( window.sirov_find_mode ) {
          gen_find_query_select( this_module, state || null, "osnovna_jedinica_out.sifra", state?.sifra || null );
          return;
        };
        
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.osnovna_jedinica_out = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.osnovna_jedinica_out = state;
        };
      },
    });
    
    
    
    $(`#`+rand_id+`_min_order_unit`).data('cit_props', {
      desc: 'odabir _min_order_unit u sirov module',
      local: true,
      show_cols: ['jedinica', 'naziv'],
      return: {},
      show_on_click: true,
      list: 'jedinice',
      
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        if ( window.sirov_find_mode ) {
          gen_find_query_select( this_module, state || null, "min_order_unit.sifra", state?.sifra || null );
          return;
        };
        
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.min_order_unit = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.min_order_unit = state;
        };
      },
    });    

    
    
    $(`#`+rand_id+`_tip_sirovine`).data('cit_props', {
      desc: 'odabir _tip_sirovine u sirov modulu',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'tip_sirovine',
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        if ( window.sirov_find_mode ) {
          gen_find_query_select( this_module, state || null, "tip_sirovine.sifra", state?.sifra || null );
          return;
        };

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.tip_sirovine = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.tip_sirovine = state;
          console.log(data);
        };
      },
    });    
    

    
    $(`#`+rand_id+`_klasa_sirovine`).data('cit_props', {
      desc: 'odabir klase sirovine u sirov modulu',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'klasa_sirovine',
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        if ( window.sirov_find_mode ) {
          gen_find_query_select( this_module, state || null, "klasa_sirovine.sifra", state?.sifra || null );
          return;
        };
        

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.klasa_sirovine = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.klasa_sirovine = state;
        };
      },
    });
 
    

    $(`#`+rand_id+`_vrsta_materijala`).data('cit_props', {
      desc: 'odabir vrsta mater u sirov modulu',
      local: true,
      show_cols: [ 'naziv'],
      return: {},
      show_on_click: true,
      list: 'vrsta_mater',
      cit_run: function(state) {

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        if ( window.sirov_find_mode ) {
          gen_find_query_select( this_module, state || null, "vrsta_materijala.sifra", state?.sifra || null );
          return;
        };
        
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.vrsta_materijala = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.vrsta_materijala = state;
        };

      },
    });

  
    
    $(`#`+rand_id+`_vrsta_papira`).data('cit_props', {
      desc: 'odabir vrsta papira u sirov modulu',
      local: true,
      show_cols: [ 'naziv'],
      return: {},
      show_on_click: true,
      list: 'vrsta_papira',
      cit_run: function(state) {

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        if ( window.sirov_find_mode ) {
          gen_find_query_select( this_module, state || null, "vrsta_papira.sifra", state?.sifra || null );
          return;
        };
        
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.vrsta_papira = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.vrsta_papira = state;
        };

      },
    });
        
    
    

    $(`#`+rand_id+`_premaz_papira`).data('cit_props', {
      desc: 'odabir premaz papira u sirov modulu',
      local: true,
      show_cols: [ 'naziv'],
      return: {},
      show_on_click: true,
      list: 'premaz_papira',
      cit_run: function(state) {

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        if ( window.sirov_find_mode ) {
          gen_find_query_select( this_module, state || null, "premaz_papira.sifra", state?.sifra || null );
          return;
        };
        
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.premaz_papira = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.premaz_papira = state;
        };

      },
    });
    
    
    $(`#`+rand_id+`_kvaliteta_1`).data('cit_props', {
      desc: 'odabir _kvaliteta_1 u sirov modulu',
      local: true,
      show_cols: [ 'naziv'],
      return: {},
      show_on_click: true,
      list: 'kvaliteta_1',
      cit_run: function(state) {

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.kvaliteta_1 = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.kvaliteta_1 = state.naziv;
        };

      },
    });

    
    
    $(`#`+rand_id+`_kvaliteta_2`).data('cit_props', {
      desc: 'odabir _kvaliteta_2 u sirov modulu',
      local: true,
      show_cols: [ 'naziv'],
      return: {},
      show_on_click: true,
      list: 'kvaliteta_2',
      cit_run: function(state) {

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.kvaliteta_2 = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.kvaliteta_2 = state.naziv;
        };

      },
    });
    
    

    $(`#`+rand_id+`_fsc`).data('cit_props', {
      desc: 'odabir FSC u sirov modulu',
      local: true,
      show_cols: [ 'naziv'],
      return: {},
      show_on_click: true,
      list: 'fsc',
      cit_run: function(state) {

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.fsc = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.fsc = state;
                
          if ( data.fsc.sifra == "FSC3" && $(`#`+rand_id+`_fsc_perc`).val() == "" ) {
            popup_warn("Obavezno upišite postotak za FSC MIX !!!", function() {
              $(`#`+rand_id+`_fsc_perc`).trigger(`click`);
              $(`#`+rand_id+`_fsc_perc`).trigger(`focus`);  
            });
            
          };

        };

      },
    });


    $(`#`+rand_id+`_zemlja_pod`).data('cit_props', {
      desc: 'odabir zemlja_pod u sirov modulu',
      local: true,
      show_cols: [ 'naziv'],
      return: {},
      show_on_click: true,
      list: 'countries',
      cit_run: function(state) {

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.zemlja_pod = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.zemlja_pod = state;
        };

      },
    });
  

    $(`#`+rand_id+`_dobavljac`).data('cit_props', {
      desc: 'izaberi firmu koja je dobavljač sirovine',
      local: false,
      url: '/search_partner',
      /* find_in: [ 'email', 'full_name', 'name' ], */
      return: { _id: 1, naziv: 1, adrese: 1, oib: 1 , dobavljac: 1 },
      col_widths: [ 0, 2, 3, 1 ],
      show_cols: [ 'naziv', 'naziv_link', 'full_adresa', 'oib' ],
      format_cols: {oib: "center"}, 
      query: {},
      filter: function(items, jQuery) {
        
        var $ = jQuery;
   
        var new_items = [];
        $.each(items, function(index, item) {
          
          // uzmi smao dobavljače
          if ( item.dobavljac === true ) {
            
            var full_adresa = global ? global.concat_full_adresa(item, `VAD1`) : window.concat_full_adresa(item, `VAD1`);
            
            var naziv_link = 
`<a href="#partner/${item._id}/status/null" 
    target="_blank"
    onClick="event.stopPropagation();"> 
    ${item.naziv} 
</a>`;
            
            new_items.push({ _id: item._id, naziv: item.naziv, naziv_link: naziv_link, full_adresa: full_adresa, oib: item.oib });
            
          };
          
        });

        return new_items;
      },
      
      show_on_click: false,
      list_width: 1200,
      
      cit_run: function(state) {

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.dobavljac = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.dobavljac = state;
          
          
          this_module.make_kontakti_in_sirov(data.dobavljac);
          
        };

      },
      
    });

    
    $(`#`+rand_id+`_doc_lang`).data('cit_props', {
      desc: 'odabir jezika za generiranje dokumenta order dobavljacu  u sirov module',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'lang',
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.doc_lang = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.doc_lang = state;
        };
        
      },
    });  
    
    
    $(`#`+rand_id+`_doc_valuta`).data('cit_props', {
      desc: 'odabir valute u kojoj će e generirati dokument sa cijenama u toj valuti :)',
      local: true,
      show_cols: ['naziv', 'valuta'],
      return: {},
      show_on_click: true,
      list: 'valute',
      cit_run: function(state) {
       
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.doc_valuta = null;
        } else {
          $('#'+current_input_id).val(state.valuta);
          data.doc_valuta = state;
        };
        
      },
    });
    
    
    $(`#`+rand_id+`_doc_adresa`).data('cit_props', {
      desc: 'odabir adrese iz liste kontakata za generiranje dokumenta order dobavljacu u sirov module',
      local: true,
      show_cols: ['full_adresa'],
      return: {},
      show_on_click: true,
      list: function() {
        return $(`#svi_kontakti_box`).data(`cit_result`);
      },
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.doc_adresa = null;
        } else {
          $('#'+current_input_id).val(state.full_adresa);
          data.doc_adresa = state;
        };
        
      },
    });  
    

    
    // WEB PRISTUPI
    this_module.web_pristupi_to_html(rand_id);
    
    
    $(`#${rand_id}_web_pristupi`).off('blur');
    $(`#${rand_id}_web_pristupi`).on('blur', function() {
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      // upiši u dataset text iz text area
      data.web_pristupi = $(`#${rand_id}_web_pristupi`).val();
      
      // pretvori u html na ljevoj strani
      this_module.web_pristupi_to_html(rand_id);
      
    });    
    
    
    
    this_module.make_sirov_gallery(data);
    
    
    // sve sirovine u grupi
    this_module.make_grupa_list(data);

    // svi rabati za ovu sirovinu
    this_module.make_sirov_rabats_list(data);
    
    
    $(`#`+rand_id+`_grupa`).data('cit_props', {

      desc: 'prikači sirovinu u grupu u sirov modulu',
      local: false,

      url: '/find_grupa',
      find_in: [ 'sifra', 'name', 'sirovine_flat' ],
      
      return: { _id: 1, sifra: 1, name: 1, sirovine: 1, sirov_links: 1 },
      
      /* _id ne moram upisivati width jer ga automatski preskače kad radi result tablicu */
      /* ALI ZATO moram upisati nula za sirovine jer ih ne želim prikazati i pošto nisu string nego objekt  */
      show_cols: ['sifra', 'name', 'sirov_links'],
      col_widths: [ 1, 2, 3 ],
      filter: function(items, jQuery) {

        var $ = jQuery;
        var new_items = [];
        
        $.each(items, function(index, item) {
          
          var sirovine_html = ``;
          
          $.each(item.sirovine, function(sirov_ind, sirov) {
            sirovine_html += `<a style="text-align: center;" href="#sirov/${sirov._id}/status/null" target="_blank" onClick="event.stopPropagation();" >${sirov.name}</a>`;
          });
          
          new_items.push({ 
            _id: item._id,
            sifra: item.sifra,
            name: item.name,
            sirovine: item.sirovine,
            sirov_links: sirovine_html
          });
          
        });

        return new_items;
      },

      
      format_cols: { 
        'name': "center",
        'sirov_links': "center" 
      },
      query: {},
      show_on_click: true,
      list_width: 1000,

      cit_run: async function (state) {

        var state = cit_deep(state);
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];

        // ako ova sirovina još nije saved
        // onda nemoj ići dalje

        if ( data._id == null ) {
          popup_warn(`Prije odabira grupe potrebno je spremiti ovu sirovinu !`);
          return;
        };


        if ( state == null ) {
          // $('#'+current_input_id).val("");
          // data.grupa = null;
          // console.log(data);
          return;
        };

        var old_grupa_id = null

        
      
        var popup_text =
`Jeste li sigurni da želite prebaciti ovu robu u grupu koju ste izabrali ???`;
      
                
        function choose_grupa_yes() {

          // ako već ima grupu
          if (data.grupa && data.grupa._id !== state._id ) {
            old_grupa_id = data.grupa._id;
          };

          data.grupa = state;

          if ( !data.grupa.sirovine ) data.grupa.sirovine = []; 

          var this_sirovina_obj = {
            name:  data.naziv,
            _id: data._id,
          };

          // dodaj ovu sirovinu u trenutnu grupu
          data.grupa.sirovine = upsert_item(data.grupa.sirovine,  this_sirovina_obj, "_id" );
          data.grupa.sirovine_flat = JSON.stringify(data.grupa.sirovine);
          data.grupa_flat = JSON.stringify(data.grupa);

          // sve sirovine u grupi
          this_module.make_grupa_list(data);

          $('#'+current_input_id).val(state.name);
          console.log(data);

          var grupa = cit_deep(data.grupa);

          // ova dva propertija samo dodajem u grupu iako ne trebaju tamo biti 
          // samo zato da pošaljem request
          // KASNIJE IH OBRIŠEM NA SERVERU !!!!!
          grupa.old_grupa_id = old_grupa_id;
          grupa.sirovina_id = data._id;

          $.ajax({
            headers: {
              'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
            },
            type: "POST",
            cache: false,
            url: `/update_grupa`,
            data: JSON.stringify(grupa),
            contentType: "application/json; charset=utf-8",
            dataType: "json"
          })
          .done(function (result) {
            console.log(result);
            toggle_global_progress_bar(false);

            if ( result.success == true ) {
              popup_msg(`Ova sirovina je dodana grupi`);
            } else {
              if ( result.msg ) popup_error(result.msg);
              if ( !result.msg ) popup_error(`Greška prilikom dodavanja sirovine u ovu grupu!`);
            };

          })
          .fail(function (error) {
            toggle_global_progress_bar(false);
            console.log(error);
            popup_error(`Došlo je do greške na serveru prilikom dodavanja sirovine u ovu grupu!`);
          });

                  
          
          
          
        };        
        
        
        function choose_grupa_no() {
          show_popup_modal(false, popup_text, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, popup_text, null, 'yes/no', choose_grupa_yes, choose_grupa_no, null);
              

        
        

      },

    });


    $(`#edit_grupa_name`).off('click');
    $(`#edit_grupa_name`).on('click', async function() {

      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id = data.rand_id;
      
      
      if ( $(`#`+rand_id+`_grupa`).val() == "" ) {
        popup_warn(`Ime ne može biti prazno!!!`);
        return;
      };

      if ( !data.grupa || !data.grupa._id ) {
        popup_warn(`Trenutno niste izabrali niti jednu grupu !!!`);
        return;
      };
      
        
      var popup_text =
`Jeste li sigurni da želite promjeniti ime grupe ???`;
            

      function change_grupa_name_yes() {


        toggle_global_progress_bar(true);

        $.ajax({
          headers: {
            'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
          },
          type: "POST",
          cache: false,
          url: `/edit_grupa_name`,
          data: JSON.stringify({
            new_name:  $(`#`+rand_id+`_grupa`).val(),
            id: data.grupa._id,
          }),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
        .done(function (result) {

          console.log(result);

          toggle_global_progress_bar(false);

          if ( result.success == true ) {
            // update lokalno unutar sirovina data
            data.grupa.name = $(`#`+rand_id+`_grupa`).val();
            data.grupa_flat = JSON.stringify(data.grupa);

            popup_msg(`Ime grupe je promjenjeno`);
            
            setTimeout( function() { $(`#save_sirov_btn`).trigger(`click`); }, 200 );
            
            
            
          } else {
            popup_error(result.msg);
          };

        })
        .fail(function (error) {
          toggle_global_progress_bar(false);
          console.log(error);
          popup_error(`Došlo je do greške na serveru prilikom promjene imena grupe`);
        });
        
        
      };      
      

      function change_grupa_name_no() {
        show_popup_modal(false, popup_text, null );
      };

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', change_grupa_name_yes, change_grupa_name_no, null);
      
      
    });

    
    $(`#remove_from_grupa`).off('click');
    $(`#remove_from_grupa`).on('click', async function() {

      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id = data.rand_id;


      if ( !data.grupa || !data.grupa._id ) {
        popup_warn(`Trenutno niste izabrali niti jednu grupu !!!`);
        return;
      };


      var popup_text =
`Jeste li sigurni da želite<br>IZBACITI OVU ROBU IZ GRUPE:<br>${ data.grupa?.name || ""  } ???`;
      
  
      
      function remove_from_grupa_yes() {

        toggle_global_progress_bar(true);

        $.ajax({
          headers: {
            'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
          },
          type: "POST",
          cache: false,
          url: `/remove_from_grupa`,
          data: JSON.stringify({
            sirov_id:  data._id,
            id: data.grupa._id,
          }),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
        .done(function (result) {

          console.log(result);

          toggle_global_progress_bar(false);

          if ( result.success == true ) {
            // update lokalno unutar sirovina data
            data.grupa = null;
            data.grupa_flat = null;

            $(`#`+rand_id+`_grupa`).val(``);

            popup_msg(`Ova sirovina je izbačena iz grupe !!!`);

            // sve sirovine u grupi
            this_module.make_grupa_list(data);
            
            setTimeout( function() { $(`#save_sirov_btn`).trigger(`click`); }, 200 );

          } else {
            popup_error(result.msg);
          };

        })
        .fail(function (error) {
          toggle_global_progress_bar(false);
          console.log(error);
          popup_error(`Došlo je do greške na serveru prilikom izbacivanja ove sirovine iz grupe !!!`);
        });

        
      };      
      
      
      function remove_from_grupa_no() {
        show_popup_modal(false, popup_text, null );
      };

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', remove_from_grupa_yes, remove_from_grupa_no, null);
            
      
      
      
    });
    

    $(`#new_grupa_btn`).off('click');
    $(`#new_grupa_btn`).on('click', async function() {
      
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      

      if ( $(`#`+rand_id+`_new_grupa_naziv`).val() == "" ) {
        popup_warn(`Ime ne može biti prazno!!!`);
        return;
      };
      
      
      if ( !data._id ) {
        popup_warn(`Prije dodavanja proizvoda u grupu ili kreiranja nove grupe, obavezno spremiti ovu robu !!!`);
        return;
      };

      var old_grupa_id = null
      // ako već ima grupu
      if ( data.grupa?._id ) {
        old_grupa_id = data.grupa._id;
      };
      

      var popup_text =
`Jeste li sigurni da želite napraviti novu grupu i ubaciti ovu sirovinu u tu grupu ???`;
      
  
      function new_grupa_yes() {
  
        
        var sirovina_id = data._id;

        toggle_global_progress_bar(true);

        $.ajax({
          headers: {
            'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
          },
          type: "POST",
          cache: false,
          url: `/new_grupa`,
          data: JSON.stringify({
            name:  $(`#`+rand_id+`_new_grupa_naziv`).val(),
            sirovina_id: data._id,
            sirovina_naziv: data.naziv,
            sirovina_sifra: data.sirovina_sifra,
            sirovina_dobav_naziv: data.dobavljac?.naziv || "",
            old_grupa_id: old_grupa_id,
          }),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
        .done(function (result) {

          console.log(result);

          toggle_global_progress_bar(false);

          if ( result.success == true ) {
            // update lokalno unutar sirovina data
            data.grupa = result.grupa;
            data.grupa_flat = JSON.stringify(data.grupa);

            // upiši ime nove grupacije u grupa input polje
            $(`#`+rand_id+`_grupa`).val( $(`#`+rand_id+`_new_grupa_naziv`).val() );
            // obriši ime iz polja za kreiranje nove grupacije
            $(`#`+rand_id+`_new_grupa_naziv`).val("");

            this_module.make_grupa_list(data);
            popup_msg(`Nova grupa je kreirana i ova sirovina je dodana u grupu !`);


            setTimeout( function() { $(`#save_sirov_btn`).trigger(`click`); }, 200 );



          } else {
            popup_error(result.msg);
          };

        })
        .fail(function (error) {
          toggle_global_progress_bar(false);
          console.log(error);
          popup_error(`Došlo je do greške na serveru prilikom promjene imena grupacije`);
        });


        
      };
      
      
      function new_grupa_no() {
        show_popup_modal(false, popup_text, null );
      };
      

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', new_grupa_yes, new_grupa_no, null);

      

    });


    $('#'+rand_id+'_sirov_docs').data(`this_module`, this_module );
    $('#'+rand_id+'_sirov_docs').data(`cit_run`, this_module.update_sirov_docs );
    
    
    $('#'+rand_id+'_sirov_pics').data(`this_module`, this_module );
    $('#'+rand_id+'_sirov_pics').data(`cit_run`, this_module.update_sirov_pics );
    
    
    
    
    
    this_module.make_specs_list(data);
    
    
    this_module.status_query = {
      
      /* user vidi samo statuse koji su poslani njemu */
      /*"to.user_number": window.cit_user.user_number,*/
      
      dep_to: { sifra: "NAB", naziv: "Nabava" },
      start: { $gte: Date.now() - 1000*60*60*24*31*4 }, // samo zadnja 4 mjeseca
      branch_done: { $ne: true },
      
      /*"status_tip.sifra" : "IS1634121913167369",*/ // ovo je ZAHTJEV NABAVI ZA SIROVINU
      /* $and: [ { "sirov": {$exists: true } }, { "sirov._id" : data._id } ], */
      
      /*
      responded: null, 
      seen: null,
      */
      
      /* "status_tip.is_work" : { $ne: true } */
      
    };
    
    this_module.status_cols_in_records = [ 
      
        "product_link",
        "status_tip_naziv",
        "komentar",
        "from_full_name",
        "to_full_name",
        "est_deadline_hours",
        "work_hours",
        "est_deadline_date",
        "rok_isporuke",
        "start",
        "end",
        "duration",
        "late",
        "docs",
        'button_delete',
      ];
    
    this_module.status_custom_headers_in_records = [
      
      "Stavka",
      "Status",
      "Komentar",
      "Od",
      "Prema",
      "Procjena sati",
      "Upisano sati",
      "Procjena datum",
      "Rok isporuke",
      "Start",
      "End",
      "Trajanje sati",
      "Kasni sati",
      "Docs",
      'OBRIŠI',
    ];
    
    this_module.status_col_widths_in_records = [
      
      1.7, // "Stavka"
      1.5, // "Status",
      3, //"Komentar",
      1, //"Od",
      1, //"Prema",
      0.7, //"Procjena sati",
      0.7, // "Upisano sati",
      1, // "Procjena datum",
      1, // "Rok isporuke",
      1, //"Start",
      1, //"End",
      0.7, //"Trajanje",
      0.7, //"Kasni",
      3.8, //"Docs",
      0.5, //'OBRIŠI,
    ];

    
    $(`#`+rand_id+`_find_status_for_sirov_record`).data('cit_props', {
      //!!!findstatus!!!
      desc: 'pretraga statusa u modulu sirov za kreiranje recorda u sirovini',
      local: false,
      url: '/find_status',
      
      find_in: [
        "sirov_flat",
        "full_product_name",
        "dep_to_flat",
        "to_flat",
        "dep_from_flat",
        "from_flat",
        "komentar",
        "status_tip_flat",
      ],
      
      query: this_module.status_query,
      
      return: {},
      
      show_cols: this_module.status_cols_in_records,
      custom_headers: this_module.status_custom_headers_in_records,
      col_widths: this_module.status_col_widths_in_records,
      format_cols: this_module.status_module.format_cols_obj,
      
      filter: [ this_module.status_module.statuses_filter, this_module.remove_duplicate_sirov_statuses ],
      
      show_on_click: true,
      list_width: 700,
      
      cit_run: function (state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          
        } else {
          
          // var komentar_no_br = state.komentar.replace(/\<br\>/ig, " ");
          // $('#'+current_input_id).val( komentar_no_br );
          
          if ( !data.selected_statuses_in_sirov ) data.selected_statuses_in_sirov = [];
          
          data.selected_statuses_in_sirov = upsert_item(data.selected_statuses_in_sirov, state, `sifra`);
          
          data.selected_statuses_in_sirov = cit_deep( data.selected_statuses_in_sirov );  
            
        };
        
        this_module.make_selected_statuses_list(data);
        
      },
   
      button_delete: function(e) {
        
        e.stopPropagation(); 
        e.preventDefault();
        
        console.log("kliknuo DELETE za STATUS");
        
      },
      
      
    }); 
    
    
    /*
    // ovo služi da osvježim drop listu 
    
    ------------------------ NE KORISTIM OVO VIŠE JER SADA LIST PROPERTY MOŽE BITI FUNKCIJA KOJA VRAĆA SVJEŽU LISTU ARRAY ------------------------------
    
    $('#'+rand_id+'_choose_spec').off(`click.choose_spec`);
    $('#'+rand_id+'_choose_spec').on(`click.choose_spec`, function (e) {
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      // console.log(`kliknuo na rn files `);
      $('#'+rand_id+'_choose_spec').data('cit_props').list = data.specs;
      
    });
    ------------------------------------------------------
    */
    
    $(`#`+rand_id+`_choose_spec`).data('cit_props', {
      desc: 'odabir specifikacije za price history',
      local: true,
      show_cols: ['komentar'],
      return: {},
      show_on_click: true,
      
      list: function() { 
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        return data.specs;
      
      },
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.current_price_hist = null;
          
        } else {
          
          $('#'+current_input_id).val(state.komentar);
          if ( !data.current_price_hist ) data.current_price_hist = {};
          data.current_price_hist.spec = cit_deep(state);
        };
        
      },
      
      
      
    });  
    
    
    $('#'+rand_id+'_price_hist_date').data(`cit_run`, function choose_price_hist_date(state, this_input) {
      
      var this_comp_id = this_input.closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      if ( state == null || this_input.val() == ``) {
        this_input.val(``);
        data.price_hist_date = null;
        console.log(`_price his date ----> : `, null );
        return;
      };

      // ovo je datum ali točno na početku dana !!!!! -----> u 00:00
      data.price_hist_date = cit_ms_at_zero( state ) + 2*1000 ; // zapravo 2 sekunde nakon PONOĆI !!!!! 
      console.log(`_price his date ----> : `, new Date(state));

    });
    
    
    
    $(`#save_price_hist_btn`).off(`click`);
    $(`#save_price_hist_btn`).on(`click`, this_module.save_price_hist );
        
    
    this_module.make_price_hist_list(data);
    
    
    $(`#save_specs`).off('click');
    $(`#save_specs`).on('click', this_module.save_current_specs );
        
    $(`#update_specs`).off('click');
    $(`#update_specs`).on('click', this_module.save_current_specs );
    
    
    $(`#card_num_btn`).off('click');
    $(`#card_num_btn`).on('click', this_module.add_card_num );
    
        
    $(`#save_sirov_location_btn`).off('click');
    $(`#save_sirov_location_btn`).on('click', this_module.save_sirov_location );
        
    $(`#update_sirov_location_btn`).off('click');
    $(`#update_sirov_location_btn`).on('click', this_module.save_sirov_location );
    

    $(`#save_variant_sirov_location_btn`).off('click');
    $(`#save_variant_sirov_location_btn`).on('click', this_module.save_sirov_location );
     
    
    
    $('#empty_QR_cards_btn').off("click");
    $('#empty_QR_cards_btn').on("click", async function() {
      
      var this_button = this;
      
      
      /*
      ---------------------- ODLUČIO SAM DA SE MOŽE KREIRATI KARTICA BEZ LOKACIJE !!!!!!! ---------------------
      if ( !window.current_location ) {
        popup_warn(`Niste označili niti jednu lokaciju!!!`);
        return;
      };
      */
      
      var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      
      var empty_cards_count = cit_number( $('#' + rand_id + `_empty_cards_count`).val() );
      
      if ( empty_cards_count === null ) {
        popup_warn(`Potrebno upisati koliko praznih kartica želite generirati !!!`);
        return;
      };
      
      
      if ( data.selected_pro_primka === null && data.selected_primka === null && data.choose_rn === null ) {
        popup_warn(`Potrebno izabrati točnu PRIMKU ili PRIMKU IZ PROIZVODNJE ili RADNI NALOG !!!`);
        return;
      };
      
        
      this_module.create_or_print_skl_card(this_button, `new_cards`, empty_cards_count);
      
      
    }); 
        
    
          
    $('#new_skl_card_btn').off("click");
    $('#new_skl_card_btn').on("click", async function() {
      
      var this_button = this;
      
      
      /*
      ---------------------- ODLUČIO SAM DA SE MOŽE KREIRATI KARTICA BEZ LOKACIJE !!!!!!! ---------------------
      if ( !window.current_location ) {
        popup_warn(`Niste označili niti jednu lokaciju!!!`);
        return;
      };
      */
      
      var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var popup_text =
`Ova lokacija već ima generirane skladišne kartice !!!<br>
Želite li prepisati stare kartice?<br>
<br>
Ako to napravite morate fizički promijeniti sve kartice na svakoj paleti ili pakiranju na ovoj lokaciji !!!
`;
      
      
      if ( window.current_location.palet_sifre?.length > 0 ) {
        
        function new_cards_yes() {
          this_module.create_or_print_skl_card(this_button, `new_cards`);
        };

        function new_cards_no() {
          show_popup_modal(false, popup_text, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, popup_text, null, 'yes/no', new_cards_yes, new_cards_no, null);
        
        
      }
      else {
        
        this_module.create_or_print_skl_card(this_button, `new_cards`);
        
      };
      
 
      
    }); 
    
    
    
    $('#print_skl_card_btn').off("click");
    $('#print_skl_card_btn').on("click", function() {
      
      var this_button = this;
      
      if ( !window.current_location ) {
        popup_warn(`Niste označili niti jednu lokaciju!!!`);
        return;
      };

      var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      if ( !window.current_location.palet_sifre || window.current_location.palet_sifre?.length == 0 ) {
        popup_warn(`Trenutno nije kreirana niti jadna skladišna kartica!<br>Kliknite na gumb KREIRAJ SKL. KART. !!! `);
        return;
      }
      else {
        this_module.create_or_print_skl_card(this_button, `print_cards`); 
      };
      
      
    }); 
  
    

    $('#new_exit_card_btn').off("click");
    $('#new_exit_card_btn').on("click", async function() {
      
      var this_button = this;
      
      if ( !window.current_location ) {
        popup_warn(`Niste označili niti jednu lokaciju!!!`);
        return;
      };

      
      var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var popup_text =
`Ova lokacija već ima generirane IZLAZNE kartice !!!<br>
Želite li prepisati stare kartice?<br>
<br>
Ako to napravite morate fizički promijeniti sve IZLAZNE kartice !!!
`;
      
      
      if ( window.current_location.palet_sifre?.length > 0 ) {
        
        function new_cards_yes() {
          this_module.create_or_print_EXIT_card(this_button, `new_exit_cards`);
        };

        function new_cards_no() {
          show_popup_modal(false, popup_text, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, popup_text, null, 'yes/no', new_cards_yes, new_cards_no, null);
        
        
      }
      else {
        
        this_module.create_or_print_EXIT_card(this_button, `new_exit_cards`);
        
      };
      
 
      
    }); 
        
   
    $('#print_exit_card_btn').off("click");
    $('#print_exit_card_btn').on("click", function() {
      
      var this_button = this;
      
      if ( !window.current_location ) {
        popup_warn(`Niste označili niti jednu lokaciju!!!`);
        return;
      };

      var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      if ( !window.current_location.palet_sifre || window.current_location.palet_sifre?.length == 0 ) {
        popup_warn(`Trenutno nije kreirana niti jadna IZLAZNA kartica!<br>Kliknite na gumb KREIRAJ IZLAZ KART. !!! `);
        return;
      }
      else {
        this_module.create_or_print_EXIT_card(this_button, `print_exit_cards`); 
      };
      
      
    }); 
    



    
    $(`#`+rand_id+`_choose_linked_products`).data('cit_props', {

      desc: `find produkte po nacrtima unutar svih sirovina za povezane proizvode ${rand_id}`,
      local: false,
      url: '/find_product',

      find_in: [
        "sifra",
        "full_product_name",
        "full_kupac_name",
        "mater",
        "komentar",
        "interni_komentar",
        "prod_specs_flat",
        "nacrt_specs_flat",
      ],
      return: {},
      /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */
      show_cols: [
        "sifra",
        "template_nacrta",
        
        "full_product_name",
        "full_kupac_name",
        "komentar",
        "interni_komentar",
        "nacrt_komentar",
        
        "nacrt_docs"
      ],
      col_widths: [
        1, // "sifra",
        1, // "template_nacrta",
        
        3, // "full_product_name",
        3, // "full_kupac_name",
        3, // "komentar",
        3, // "interni_komentar",
        3, // "nacrt_komentar",
        
        8, // "nacrt_docs"
      ],
      format_cols: {
        sifra: "center",
        template_nacrta: "center",
      },
      query: {},

      filter: this_module.product_module.nacrt_find_filter,

      show_on_click: false,
      
      cit_run: function(state) {
        
        
        
      },

    });  
    
    
    
    
    
    this_module.make_records_list(data);
    
    
    
    
    // primkaeventi
    /*
    
          sifra,
          
          ms_record,
          ms_time,

          primka_record,
          primka_pro_record,

          count,
          
          neto_masa_paketa_kg,
          bruto_masa_paketa_kg,

          neto_masa_palete_kg,
          bruto_masa_palete_kg,

          location_from,
          
          palet_sifre,

          palet_count,
          
          box_count_on_palet,
          red_count_on_palet,
          box_count_in_red,

          palet_unit_count,
          paleta_x,
          paleta_y,
          paleta_z,

          box_count,
          box_unit_count,
          box_x,
          box_y,
          box_z,

          sklad,
          sector,
          level,
          shelf,   
      */
    
    
    
    $('#'+rand_id+'_choose_primka').data('cit_props', {
      // !!!findprimka!!!
      desc: 'za odabir PRIMKE na koju se veže ova lokacija koju trenutno upisujem u modulu SIROVINE',
      local: false,
      url: '/find_docs',
      find_in: [ 'docs.doc_sifra' ],
      col_widths: [ 1, 1 ],
      custom_headers: [
        "PRIMKA",
        "DATUM",
      ],
      format_cols: {
        refer_doc: "center",
        time: "date_time",
      },
      show_cols: [ 'refer_doc', 'time' ], // rok u ovom slučaju znači kad je došla roba !!!!!!!
      query: function() {
      
        var req = {};
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        if ( !data.dobavljac?._id ) {
          
          popup_warn(`Nedostaju podaci DOBAVLJAČA za ovu sirovinu !!!!`);
          req = `abort_req`; // ovo mi služi da abortam sve u search_database unutar global_func.js
          
        } else {
          
          req = { 
            _id: data.dobavljac._id,
            get_doc_type: `in_record`,
            get_doc_sirov_id: data._id,
          };
          
        };
        
        return req;
      
      },
      show_on_click: true,
      list_width: 700,
      cit_run: function(state) {
        
        /*
        
        --------- OVO JE PRIMJER OBJEKTA / STATE-a ---------
        
        sifra: doc.sifra,
        doc_type: doc.doc_type,
        doc_sifra: doc.doc_sifra,
        time: doc.time,
        doc_link: doc.doc_link,
        doc_sirovs: doc.doc_sirovs,
        rok: doc.rok,
        
        
        */
        
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.selected_primka = null;
        } else {
          $('#'+current_input_id).val(state.refer_doc);
          data.selected_primka = state;
        };
      },
      
    });
    
    $('#'+rand_id+'_choose_pro_primka').data('cit_props', {
      // !!!findprimka!!!
      desc: 'za odabir PRIMKE IZ PROIZVODNJE na koju se veže ova lokacija koju trenutno upisujem u modulu SIROVINE',
      local: false,
      url: '/find_docs',
      find_in: [ 'docs.doc_sifra' ],
      col_widths: [ 1, 1 ],
      custom_headers: [
        "PRIMKA",
        "DATUM",
      ],
      format_cols: {
        refer_doc: "center",
        time: "date_time",
      },
      show_cols: [ 'refer_doc', 'time' ], // rok u ovom slučaju znači kad je došla roba !!!!!!!
      query: function() {
      
        var req = {};
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        if (  !data.list_kupaca || data.list_kupaca?.length == 0 ) {
          
          popup_warn(`Nedostaju podaci KUPCA za ovu sirovinu !!!!`);
          req = `abort_req`; // ovo mi služi da abortam sve u search_database unutar global_func.js
          
        } else {
          
          var kupci_ids = [];
          $.each(data.list_kupaca, function(k_ind, kupac) {
            if ( kupac._id ) kupci_ids.push( kupac._id );
          });
          
          
          req = { 
            _id: { $in: kupci_ids },
            get_doc_type: `in_pro_record`,
            get_doc_sirov_id: data._id,
          };
          
        };
        
        return req;
      
      },
      show_on_click: true,
      list_width: 700,
      cit_run: function(state) {
        
        
        /*
        
        --------- OVO JE PRIMJER OBJEKTA / STATE-a ---------
        
        sifra: doc.sifra,
        doc_type: doc.doc_type,
        doc_sifra: doc.doc_sifra,
        time: doc.time,
        doc_link: doc.doc_link,
        doc_sirovs: doc.doc_sirovs,
        rok: doc.rok,
        
        
        */
        
        
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.selected_pro_primka = null;
        } else {
          $('#'+current_input_id).val(state.refer_doc);
          data.selected_pro_primka = state;
        };
      },
      
    });
        
        
    $(`#`+rand_id+`_choose_location_from`).data('cit_props', {
      
      desc: 'odabir lokacije FROM u sirovinama za kreiranje medjuskladišnice',
      local: true,
      
      show_cols: [ 
        'palet_sifre', 
        'count',
        'sklad_naziv',
        'sector_sifra', 
        'level_sifra',
        'shelf_naziv'
      ],
      col_widths: [ 
        1, // 'palet_sifre', 
        1, // 'count',
        3, // 'sklad_naziv',
        1, // 'sector_sifra', 
        1, // 'level_sifra',
        1, // 'shelf_naziv'
      ],
      custom_headers: [
        "SKL. ŠIFRE",
        "KOLIČINA",
        "SKLADIŠTE",
        "SEKTOR",
        "RAZINA",
        "POLICA",
      ],
      return: {},
      show_on_click: true,
      list_width: 700,
      
      format_cols: {
        count: 2,
        palet_sifre: "center",
        sklad_naziv: "center",
        sector_sifra: "center",
        level_sifra: "center",
        shelf_naziv: "center",
      },
      
      list: function() {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        
        $.each( data.locations, function(loc_ind, loc) {
          
          // dodaj ove propertije tako da mogu kreirati jednostavu listu !!!
          // dodaj ove propertije tako da mogu kreirati jednostavu listu !!!
          data.locations[loc_ind].sklad_naziv = data.locations[loc_ind].sklad?.naziv || ``;
          data.locations[loc_ind].sector_sifra = data.locations[loc_ind].sector?.sifra || ``;
          data.locations[loc_ind].level_sifra = data.locations[loc_ind].level?.sifra || ``;
          data.locations[loc_ind].shelf_naziv = data.locations[loc_ind].shelf?.naziv || ``;
          
        });
        
        var primka_count = 0;
        if ( data.selected_primka?.primka_count ) primka_count = data.selected_primka.primka_count;
        if ( data.selected_pro_primka?.primka_count ) primka_count = data.selected_pro_primka.primka_count;
        
        
        var list = [ 
          /* PRVE TRI LOKACIJE SU default lokacije !!! */
          { 
            sifra: "NEMALOC",
            palet_sifre: `&nbsp;&nbsp;&#8212;&nbsp;&nbsp;`,
            count: data.selected_primka ? primka_count : 0,
            sklad_naziv: "NEMA LOKACIJU",
            sector_sifra: ``,
            level_sifra: ``,
            shelf_naziv: ``,
          },
          
          { 
            sifra: "ZAPROB",
            palet_sifre: `&nbsp;&nbsp;&#8212;&nbsp;&nbsp;`,
            count: data.selected_primka ? primka_count : 0,
            sklad_naziv: "ZAPRIMLJENA ROBA",
            sector_sifra: ``,
            level_sifra: ``,
            shelf_naziv: ``,
          },
          
          { 
            sifra: "PROROB",
            palet_sifre: `&nbsp;&nbsp;&#8212;&nbsp;&nbsp;`, 
            count: data.selected_pro_primka ? primka_count : 0,
            sklad_naziv: "ROBA ZA DOSTAVU",
            sector_sifra: ``,
            level_sifra: ``,
            shelf_naziv: ``,
          },
          
          /* ako postoje lokacije onda uzmi njih */
          ...data.locations, 
          
        ];
        
        return list;
         
      },
      
      cit_run: function(state) {
      
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.location_from = null;
        } else {
          $('#'+current_input_id).val( 
            state.sklad_naziv  + ` — ` +
            state.sector_sifra + ` — ` +
            state.level_sifra  + ` — ` +
            state.shelf_naziv 
          );
          
          data.location_from = state;
        };
        
      },
      
    });  
   
    
    $('#'+rand_id+'_choose_rn').data('cit_props', {
      // !!!findprimka!!!
      desc: 'za odabir SIFRA RADNOG NALOGA koji je povezan s ovom SKLADIŠNOM KARTICOM koju trenutno upisujem u modulu SIROVINE',
      local: false,
      url: '/find_sirov_records',
      find_in: [ 'records.doc_sifra' ],
      col_widths: [ 1, 1 ],
      custom_headers: [
        "RADNI NALOG",
        "DATUM",
      ],
      format_cols: {
        refer_doc: "center",
        time: "date_time",
      },
      show_cols: [ 'refer_doc', 'time' ], // record_est_time u ovom slučaju znači kad je datum RN !!!!!!!
      query: function() {
      
        var req = {};
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        if ( !data._id ) {
          
          popup_warn(`Nedostaje ID od ove sirovine !!!!`);
          req = `abort_req`; // ovo mi služi da abortam sve u search_database unutar global_func.js
          
        } else {
          
          req = { 
            _id: data._id,
            find_record_type: "radni_nalog"
          };
          
        };
        
        return req;
      
      },
      show_on_click: true,
      list_width: 700,
      cit_run: function(state) {
        
        
        /*
        
        --------- OVO JE PRIMJER OBJEKTA / STATE-a ---------
        
        sifra: doc.sifra,
        doc_type: doc.doc_type,
        doc_sifra: doc.doc_sifra,
        time: doc.time,
        doc_link: doc.doc_link,
        doc_sirovs: doc.doc_sirovs,
        rok: doc.rok,
        
        
        */
        
        
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.selected_rn = null;
        } else {
          $('#'+current_input_id).val(state.refer_doc);
          data.selected_rn = state;
        };
      },
      
    });

    
    $(`#`+rand_id+`_find_sirov_location`).data('cit_props', {
      // !!!findlocation!!!
      desc: 'za pretragu lokacija po paletnoj sifri i po primci u modulu SIROVINE',
      local: false,
      min_string_length: 5, // samo kada upiše 5 znakova onda počni pretragu
      url: '/find_sirov_location',
      
      parent: `#sirov_locations_box`,
      
      find_in: [ 'locations.palet_sifre', 'locations.primka_record.doc_sifra', 'locations.primka_pro_record.doc_sifra' ],
      return: {},
      query: {},
      
      
      show_cols: this_module.locations_show_cols,
      col_widths: this_module.locations_col_widths,
      custom_headers: this_module.locations_custom_headers,
      format_cols: this_module.locations_format_cols,
      
      
      show_on_click: false,
      list_width: 700,
      
      local_filter: this_module.loc_filter,
      reg_events: this_module.register_location_list_events,
      
      button_edit: this_module.location_button_edit_func,
      button_delete: this_module.location_button_delete_func,
      
      cit_run: async function(state) { console.log(state); },
      
      
    });  
    

    // ovo je custom listener 
    // samo da vrati originalnu listu 
    // kada obriše sve u polju za pretragu lokacija
    $(`#`+rand_id+`_find_sirov_location`).off(`keyup.sirov_location`);
    $(`#`+rand_id+`_find_sirov_location`).on(`keyup.sirov_location`, function(e) {
      
      var this_value = $(this).val();
      
      // IGNORE AKO JE TIPKA TAB !!!!!!
      if ( e.which !== 9 && this_value.length == 0 ) { /* length mora biti nula ----> to znači da je obrisano sve u polju */
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        this_module.make_locations_list(data);
        
      };
      
    });
    

    
    
    
    
    
    $(`#`+rand_id+`_sklad`).data('cit_props', {
      desc: 'odabir SKLADIŠTA u sirov modulu',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'sklad',
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.sklad = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.sklad = state;
        };
      },
    });
    
    
    $(`#`+rand_id+`_sector`).data('cit_props', {
      desc: 'odabir SECTORA u sirov modulu',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: function() {
        var sector_list = [];
        
        if ( data.sklad ) {
        
          $.each( window.cit_local_list.sector, function(s_ind, sec) {
            
            if ( sec.short == data.sklad.short ) {
              sector_list.push( sec );  
            };

          });
          
        }
        else {
          // uzmi SVE !!!
          sector_list = window.cit_local_list.sector;
        }
        
        return sector_list;
        
      },
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          
          $('#'+current_input_id).val(``);
          $(`#`+rand_id+`_sklad`).val(``);
          
          data.sector = null;
          data.sklad = null;
          
        } else {
          $('#'+current_input_id).val(state.naziv);
          $(`#`+rand_id+`_sklad`).val(state.sklad_naziv);
          data.sector = state;
          
        };
      },
    });
    
    
    $(`#`+rand_id+`_level`).data('cit_props', {
      desc: 'odabir LEVELA u sirov modulu',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'level',
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.level = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.level = state;
        };
      },
    });
    
    
    $(`#`+rand_id+`_alat_shelf`).data('cit_props', {
      
      desc: 'odabir POLICE ALATA u sirov modulu',
      min_string_length: 1, // trigeriraj pretragu odmah nakon upisa samo jedno
      
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'alat_shelf',
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.alat_shelf = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.alat_shelf = state;
        };
      },
      
      
      
    });
    

    $('#'+rand_id+'_loc_est_date').data(`cit_run`, function choose_loc_est_date(state, this_input) {
      
      var this_comp_id = this_input.closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      if ( state == null || this_input.val() == ``) {
        this_input.val(``);
        data.ms_time = null;
        console.log(`_loc est date ----> data.ms_time: `, null );
        return;
      };

      data.ms_time = state;
      console.log(`_loc est date ----> data.ms_time: `, new Date(state));

    });

    
    
    
    this_module.make_locations_list(data);
    
    
    
  }, 500*1000 );

  return {
    html: component_html,
    id: data.id
  };

},
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};

  
  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
  };
  this_module.set_init_data = set_init_data;

  
  this_module.new_sirov = {

    for_search: false,  
    not_active: false,  

    sirovina_sifra: null,

    is_ostatak: false,

    naziv: null,

    cijena: null,
    alt_cijena: null,
    pro_cijena: null,
    m2_cijena: null,

    price_hist: [],

    stari_naziv: null,
    povezani_nazivi: null,

    detaljan_opis: null,

    osnovna_jedinica: null,
    osnovna_jedinica_out: null,
    normativ_omjer: null,


    pk_kolicina: null,
    nk_kolicina: null,
    rn_kolicina: null,
    order_kolicina: null,
    sklad_kolicina: null,


    tip_sirovine: null,    
    klasa_sirovine: null,
    vrsta_materijala: null,
    
    premaz_papira: null,
    vrsta_papira: null,
    
    grupa: null,
    grupa_flat: null,    

    kvaliteta_1: null,
    kvaliteta_2: null,

    dobavljac: null,
    dobav_naziv: null,
    dobav_sifra: null,
    
    
    min_order: null,
    min_order_unit: null, 

    fsc: null,
    fsc_perc: null,

    povezani_kupci: [],
    povezani_kupci_flat: null, 


    linked_products: [],
    linked_products_flat: null,

    kontra_tok: null,
    tok: null,
    kontra_tok_x_tok: null,  

    po_valu: null,
    po_kontravalu: null,
    val_x_kontraval: null,   
    
    sloj_1: null,
    val_1: null,
    sloj_2: null,
    val_2: null,
    sloj_3: null,
    


    debljina_mm: null,
    gramaza_g: null,
    
    duzina: null,
    sirina: null,
    visina: null,

    povrsina_m2: null,
    volumen_m3: null,
    volumen_l: null,
    promjer: null,

    min_kolicina: null,
    max_kolicina: null,
    
    palete_free: null,
    palete_taken: null,
    palete_broken: null,

    boja: null,
    zemlja_pod: null,

    average_skart: null,

    osnovna_neto_masa_kg: null,
    osnovna_bruto_masa_kg: null,
    
    
    
    stroj_kw: null,
    stroj_god: null,
    

    /*
    neto_masa_paketa_kg: null,
    bruto_masa_paketa_kg: null,
    komada_u_pakiranju_kom: null,
    paketa_na_paleti_kom: null,
    komada_na_paleti_kom: null,
    */
    
    
    neto_masa_palete_kg: null,
    bruto_masa_palete_kg: null,

    nosac_cijena: false,
    kapacitet_kukice: null,

    
    

    voz_user: null,
    voz_marka: null,
    voz_reg: null,
    voz_sas: null,
    voz_buy_date: null,
    voz_leasing: null,
    voz_leasing_house: null,
    voz_leasing_exp_date: null,
    voz_last_servis_date: null,
    voz_reg_exp_date: null,

    


    rabats: [],
    web_pristupi: null,
    specs: [],


    ask_kolicina_1: null,
    ask_kolicina_2: null,
    ask_kolicina_3: null,
    ask_kolicina_4: null,
    ask_kolicina_5: null,
    ask_kolicina_6: null,
    

    doc_valuta_manual: null,
    doc_ponuda_num: null,
    order_price: null,

    record_est_time: null,  


    statuses: [],

    locations: [


    ],


    records: [


    ], /* kraj records */

    
    paleta_x: null,
    paleta_y: null,
    paleta_z: null,
    masa_kg: null,
    
    
    
    alat_shelf: null,
    alat_owner: null,
    
    list_kupaca: [],
    
    alat_stamp_num: null,
    alat_max_stamp: null,
    
    alat_fi: null,
    alat_nest_count: null,
    
    
    rok_dostave: null,
    exp_month: null,
    
    
    sirov_pics: [],
    
    
    
    
  }; // kraj new sirov 

  
  function create_valid() {
    
    var valid = {

      sirovina_sifra: { element:"input", type:"string", lock:true, visible:true, label:"Šifra robe" },

      for_search: { element:"switch", type:"bool", lock:false, visible:true, label:"Za istraživanje"},
      not_active: { element:"switch", type:"bool", lock:false, visible:true, label:"NIJE AKTIVNO"}, 

      naziv: { element:"input", type:"string", lock:false, visible:true, label:"Naziv", },

      cijena: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label:"Cijena", required: true},
      alt_cijena: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label:"Dobav. Cijena",  },
      pro_cijena: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label:"Proiz. Cijena",  },
      m2_cijena: { element:"input", type:"number", decimals: 3, lock:true, visible:true, label:"Cijena / m2",  },

      stari_naziv: { element:"input", type:"string", lock:false, visible:true, label:"Stari naziv ili šifra"},
      povezani_nazivi: { element:"input", type:"string", lock:false, visible:true, label:"Povezani nazivi"},

      detaljan_opis: { element:"text_area", type:"string", lock:false, visible:true, label:"Detaljan opis"},

      
      osnovna_jedinica: { element:"single_select", type:"simple", lock:false, visible:true, label:"VELPROM jedinica mjere INTERNO", required: true},
      min_order_unit: { element:"single_select", type:"simple", lock:false, visible:true, label:"Jedinica za narudžbu", required: true },  
      normativ_omjer: { element:"input", type:"number", decimals: 10, lock:false, visible:true, label:"Omjer VELPROM jed. / ALTERNATIV jed.", required: true},
      
      
      osnovna_jedinica_out: { element:"single_select", type:"simple", lock:false, visible:true, label:"Alternativna jedinica", required: false}, 
      


      pk_kolicina: { element:"input", type:"number", decimals:2, lock:false, visible:true, label:"Rezervirano po PK" },
      nk_kolicina: { element:"input", type:"number", decimals:2, lock:false, visible:true, label:"Rezervirano po NK (za proizvodnju)" },
      rn_kolicina: { element:"input", type:"number", decimals:2, lock:false, visible:true, label:"Rezervirano po RN" },
      order_kolicina: { element:"input", type:"number", decimals:2, lock:false, visible:true, label:"U dolasku po ND" },
      sklad_kolicina:  { element:"input", type:"number", decimals:2, lock:false, visible:true, label:"Količina na skladištu", required: true },
      
      
      palete_free: { element:"input", type:"number", decimals: 0, lock:false, visible:true, label:"Slobodne palete" },
      palete_taken: { element:"input", type:"number", decimals: 0, lock:false, visible:true, label:"Zauzete palete" },
      palete_broken: { element:"input", type:"number", decimals: 0, lock:false, visible:true, label:"Oštećene palete" },
      

      klasa_sirovine: { element:"single_select", type:"simple", lock:false, visible:true, label:"Klasa", required: true },
      vrsta_materijala: { element:"single_select", type:"simple", lock:false, visible:true, label:"Vrsta materijala"},
      
      vrsta_papira: { element:"single_select", type:"simple", lock:false, visible:true, label:"Vrsta papira"},
      premaz_papira: { element:"single_select", type:"simple", lock:false, visible:true, label:"Premaz papira"},
     
     
      grupa: { element:"single_select", type:"", lock:false, visible:true, label:"GRUPA"},
      new_grupa_naziv: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "Naziv nove grupe" },

      
      kvaliteta_1: { element:"single_select", type:"same_width", lock:false, visible:true, label:"Kvaliteta kartona (P, T, B, Š ...)" },
      kvaliteta_2: { element:"single_select", type:"same_width", lock:false, visible:true, label:"Valovi kartona (e, b, c, eb, bc ...)" },
      
      /* kvaliteta_1: { element:"input", type:"string", lock:false, visible:true, label:"Kvaliteta kartona (P, T, B, Š ...)"}, */
      /* kvaliteta_2: { element:"input", type:"string",  lock:false, visible:true, label:"Valovi kartona (e, b, c, eb, bc ...)"}, */

      dobavljac: { element:"single_select", type:"simple", lock:false, visible:true, label:"Dobavljač", required: true},
      dobav_naziv: { element:"input", type:"string", lock:false, visible:true, label:"Naziv kod dobavljača ili alter. naziv"},
      dobav_sifra: { element:"input", type:"string", lock:false, visible:true, label:"Šifra kod dobavljača ili alter. oznaka"},

      tip_sirovine:  { element:"single_select", type:"simple", lock:false, visible:true, label:"Tip", required: true},

      min_order: { element:"input", type:"number", decimals:2, lock:false, visible:true, label:"Minimalna narudžba"},
      

      premaz: { element:"input", type:"string", lock:false, visible:true, label:"Premaz papira SLOBODAN UNOS" },  

      fsc: { element:"single_select", type:"simple", lock:false, visible:true, label:"Kategorija FSC"},  
      fsc_perc: { element:"input", type:"number", decimals:2, lock:false, visible:true, label:"% za FSC Mix"},   

      povezani_kupci: { element:"multi_select", type:"simple", lock:false, visible:true, label:"Prati ove partnere"},

      choose_linked_products: { element:"single_select", type:"", lock:false, visible:true, label:"Prati ove proizvode"},  

      kontra_tok: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Papir Kontra tok (uvijek 1.) / mm"},
      tok: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Papir Tok (uvijek 2.) / mm"},
      po_valu: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Po valu (uvijek 1.) / mm"},
      po_kontravalu: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Po kontra-valu (uvijek 2.) / mm"},
      
      
      
      sloj_1: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"LICE gramatura / g"},
      val_1: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"VAL 1 gramatura / g"},
      sloj_2: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"SREDNJI SLOJ gramatura / g"},
      val_2: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"VAL 2 gramatura / g"},
      sloj_3: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"NALIČJE gramatura / g"},
      
      

      debljina_mm: { element:"input", type:"number", decimals:2, lock:false, visible:true, label:"Debljina / mm"},  
      gramaza_g: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Gramatura"},
      duzina: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Dužina / mm"},
      sirina: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Širina / mm"},
      visina: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Visina / mm"},

      povrsina_m2: { element:"input", type:"number", decimals:3, lock:false, visible:true, label:"Površina / m2"},
      volumen_m3: { element:"input", type:"number", decimals:3, lock:false, visible:true, label:"Volumen / m3"},
      volumen_l: { element:"input", type:"number", decimals:3, lock:false, visible:true, label:"Volumen / litra"},

      masa_kg: { element:"input", type:"number", decimals:3, lock:false, visible:true, label:"Masa / kilogram"}, 
      rok_dostave: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Rok dostave / RADNI dani"},
      exp_month: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Rok trajanja / mjeseci"},       

      promjer: { element:"input", type:"number", decimals:3, lock:false, visible:true, label:"Promjer / mm"},

      min_kolicina: { element:"input", type:"number", decimals:3, lock:false, visible:true, label:"Minimalna količina u skladištu"},
      max_kolicina: { element:"input", type:"number", decimals:3, lock:false, visible:true, label:"Maksimalna količina u skladišu"},

      boja: { element:"input", type:"string",  lock:false, visible:true, label:"Boja"},
      zemlja_pod: { element:"single_select", type:"simple", lock:false, visible:true, label:"Zemlja podrijetla"},

      average_skart: { element:"input", type:"number", decimals:2, lock:false, visible:true, label:"Prosječni % škarta"},

      osnovna_neto_masa_kg: { element:"input", type:"number", decimals:3, lock:false, visible:true, label:"Neto masa jedinice / kg"},
      osnovna_bruto_masa_kg: { element:"input", type:"number", decimals:3, lock:false, visible:true, label:"Bruto masa jedinice / kg"},
      
      nosac_cijena: { element:"switch", type:"bool", decimals:0, lock:false, visible:true, label:"Kukica ima nosač cijena"},
      kapacitet_kukice: { element:"input", type:"number", decimals:2, lock:false, visible:true, label:"Kapacitet kukice / mm"},

      
      
      // komada_u_pakiranju_kom: { element:"input", type:"number", decimals:2, lock:false, visible:true, label:"Komada u pakiranju"},
      // paketa_na_paleti_kom: { element:"input", type:"number", decimals:2, lock:false, visible:true, label:"Paketa na paleti / kom"},
      // komada_na_paleti_kom: { element:"input", type:"number", decimals:2, lock:false, visible:true, label:"Komada na paleti / kom"},

      
      

      alat_stamp_num: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Broj udaraca alata"},
      alat_max_stamp: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"MAX broj udaraca alata"},      

      alat_nest_count: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Broj gnjezda alata"},


      rabat_min: { element:"input", type:"number", decimals:2, lock:false, visible:true, label:"Min količina za rabat"},
      rabat_max: { element:"input", type:"number", decimals:2, lock:false, visible:true, label:"Max količina za rabat"},
      rabat_perc: { element:"input", type:"number", decimals:2, lock:false, visible:true, label:"Postotak rabata"},

      specs_komentar: { element:"input", type:"string", lock:false, visible:true, label:"Opis specifikacija"},  
      sirov_docs: { "element": "upload", "type": "multiple", "lock": false, "visible": true, "label": "Izaberi dokumente" },  


      choose_spec: { element:"single_select", type:"simple", lock:false, visible:true, label:"Povezani dokument za NOVU CIJENU (dokument za opravdanje cijene - opcionalno)" },
      price_hist_date: { element: "simple_calendar", type: "date", lock: false, visible: true, "label": "Cijena vrijedi OD DATUMA" },

      ask_kolicina_1: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label:"Količina 1"},
      ask_kolicina_2: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label:"Količina 2"},  
      ask_kolicina_3: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label:"Količina 3"}, 

      ask_kolicina_4: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label:"Količina 4"},  
      ask_kolicina_5: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label:"Količina 5"},  
      ask_kolicina_6: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label:"Količina 6"},  

      web_pristupi: { "element": "text_area", "type": "string", "lock": false, "visible": true, "label": "Editiraj Web pristupe" },  

      doc_lang: { element:"single_select", type:"simple", lock:false, visible:true, label:"Jezik dokumenta" },
      doc_valuta: { element:"single_select", type:"simple", lock:false, visible:true, label: "Valuta" },
      doc_valuta_manual: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label: "Prepiši Tečaj" },
      doc_ponuda_num: { element:"input", type:"string", lock:false, visible:true, label:"Po ponudi"},
      doc_adresa: { element:"single_select", type:"same_width", lock:false, visible:true, label:"Adresa na dokumentu" },

      alat_fi: { element:"input", type:"number", decimals: 0, lock:false, visible:true, label: "Promjer &#8960; Alata u mm" },

      alat_val: { element:"input", type:"number", decimals: 0, lock:false, visible:true, label: "Štanca po VALU" },     
      alat_kontra: { element:"input", type:"number", decimals: 0, lock:false, visible:true, label: "Štanca po KONTRAVALU" },     

     
     
      /* --------------START--------------- STROJ ----------------------------- */
      stroj_kw: { element:"input", type:"number", decimals: 0, lock:false, visible:true, label: "Snaga stroja / kW" },  
      stroj_god: { element:"input", type:"number", decimals: 0, lock:false, visible:true, label: "Godina proizvodnje" },  
     
     
     
      
      /* --------------START--------------- VOZILA ----------------------------- */
      
      voz_user: { element:"single_select", type:"simple", lock:false, visible:true, label:"Vozač" },
      voz_marka: { element:"input", type:"string", lock:false, visible:true, label:"Marka Vozila"},
      voz_reg: { element:"input", type:"string", lock:false, visible:true, label:"Registracija Vozila"},
      voz_sas: { element:"input", type:"string", lock:false, visible:true, label:"Broj Šasije Vozila"},
      voz_buy_date: { element: "simple_calendar", type: "date", lock: false, visible: true, "label": "Datum Kupnje" },
      voz_leasing: { element:"switch", type:"bool", lock:false, visible:true, label:"Leasing vozila"},
      voz_leasing_house: { element:"input", type:"string", lock:false, visible:true, label:"Leasing Kuća Vozila"},
      
      voz_leasing_exp_date: { element: "simple_calendar", type: "date", lock: false, visible: true, "label": "Datum Isteka Leasinga" },
      voz_last_servis_date: { element: "simple_calendar", type: "date", lock: false, visible: true, "label": "Datum Zadnjeg Servisa" },
      voz_reg_exp_date: { element: "simple_calendar", type: "date", lock: false, visible: true, "label": "Datum Isteka Registracije" },
            
            
      /* --------------END--------------- VOZILA ----------------------------- */
      
      

      find_status_for_sirov_record: { element:"single_select", type:"", lock:false, visible:true, label:"Izaberi povezane statuse" },

      find_sirov_original: { element:"single_select", type:"", lock:false, visible:true, label:"Roba je Velprom proizvod" },
      alat_owner: { element:"single_select", type:"", lock:false, visible:true, label:"Izaberi Vlasnika ALATA" },    
      
      roba_kupac: { element:"single_select", type:"", lock:false, visible:true, label:"Izaberi Kupca PROIZVODA" },    


      order_price: { element:"input", type:"number", decimals: 2, lock:false, visible:true, label: "Prepiši cijenu" },
      choose_record: { element:"check_box", type:"bool", lock:false, visible:true, label:"Označi"},
      record_est_time:  { element: "simple_calendar", type: "date_time", lock: false, visible: true, "label": "Procjenjeni datum" },     

      sirov_pics: { "element": "upload", "type": "multiple", "lock": false, "visible": true, "label": "Izaberi slike" },           


      /* -------------START--------------- PODACI ZA LOCATION  ------------------------------------------ */
      
      empty_cards_count: { element:"input", type:"number", decimals: 0, lock:false, visible:true, label: "Koliko praznih kartica?" },
      
      write_skl_card_num: { element:"input", type:"string", lock:false, visible:true, label:"Upiši ŠIFRU SKL. KARTICE "},
      
      
      choose_ms: { element:"check_box", type:"bool", lock:false, visible:true, label:"Označi MS"},
      
      neto_masa_paketa_kg: { element:"input", type:"number", decimals:3, lock:false, visible:true, label:"Neto m paketa/kg"},
      bruto_masa_paketa_kg: { element:"input", type:"number", decimals:3, lock:false, visible:true, label:"Bruto m paketa/kg"},
      
      neto_masa_palete_kg: { element:"input", type:"number", decimals:3, lock:false, visible:true, label:"Neto m palete/kg"},
      bruto_masa_palete_kg: { element:"input", type:"number", decimals:3, lock:false, visible:true, label:"Bruto m palete/kg"},

      choose_location_from: { element:"single_select", type:"simple", lock:false, visible:true, label:"SA lokacije" },
      choose_primka: { element:"single_select", type:"simple", lock:false, visible:true, label:"Primka dobavljača" },  
      choose_pro_primka: { element:"single_select", type:"simple", lock:false, visible:true, label:"Primka IZ PROIZVODNJE" },  
      choose_rn: { element:"single_select", type:"simple", lock:false, visible:true, label:"Povezani RN" },  
      
      
      find_sirov_location: { element:"single_select", type:"simple", lock:false, visible:true, label:"Pronađi lokacju po sklad. šifri ili primci" },

      palet_count: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Broj paleta"},     
      box_count_on_palet: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Broj pakiranja na paleti"}, 
      red_count_on_palet: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Broj redova na paleti"}, 
      box_count_in_red: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Broj pakiranja u redu"}, 

      palet_unit_count: { element:"input", type:"number", decimals:3, lock:false, visible:true, label:"Količina robe na paleti"},     

      paleta_x: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Dužina palete"},
      paleta_y: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Širina palete"},
      paleta_z: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Visina palete"},

      box_count: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Broj pakiranja bez palete"},   
      box_unit_count: { element:"input", type:"number", decimals:3, lock:false, visible:true, label:"Količina robe u pakiranju"},     
      box_x: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Ukupna Dužina pakiranja"},
      box_y: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Ukupna Širina pakiranja"},
      box_z: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Ukupna Visina pakiranja"},
      
      sklad: { element:"single_select", type:"simple", lock:false, visible:true, label:"Skladište" },
      sector: { element:"single_select", type:"simple", lock:false, visible:true, label:"Sektor" },
      level: { element:"single_select", type:"simple", lock:false, visible:true, label:"Level" },
      alat_shelf: { element:"single_select", type:"simple", lock:false, visible:true, label:"Polica Alata" },
      
      loc_est_date: { element: "simple_calendar", type: "date_time", lock: false, visible: true, "label": "Datum MS" },
      loc_komentar: { element:"input", type:"string", lock:false, visible:true, label:"Komentar MS"},  
      
      
      /* -------------END--------------- PODACI ZA LOCATION  ------------------------------------------ */
      
      /*
      
      ------------------------------------------------- STARI PODACI ZA LOCATION -------------------------------------------------
      "sifra" : "local16514880622060789",
      "primka_time" : 1651488062206.0,
      "ms_time" : 1651488062206.0,
      "primka_record" : null,
      "ms_record" : null,
      "count" : 1,
      "sklad" : "P3",
      "sector" : "S1",
      "level" : "L1",
      "shelf" : "A1"
      
      */
     
      
};
    this_module.valid = valid;
    
  }
  this_module.create_valid = create_valid;
  
  create_valid();
    

  async function make_grupa_list(data) {
    
    this_module.find_sirov = await get_cit_module(`/modules/sirov/find_sirov.js`, null);
    
    if ( !data.grupa || !data.grupa?.sirovine?.length ) {
      $("#grupa_box").html(``);
      return;
    };

    if ( data.grupa?.sirovine?.length > 0 ) {
      
      // get sve sirovine tj kompletne objekte iz baze
      var grupa_sirovs = await get_db_sirovs( data.grupa.sirovine );
      
      
      grupa_sirovs = this_module.find_sirov.find_sirov_filter(grupa_sirovs, jQuery );
      
      
      $.each(grupa_sirovs, function(gsir_index, sirov) {
        var sirovina_link = 
            `<a style="text-align: center;" href="#sirov/${sirov._id}/status/null" target="_blank" onClick="event.stopPropagation();" >${sirov.full_naziv}</a>`;
        
        // promjeni property full naziv da bude zapraqvo link !!!!
        // promjeni property full naziv da bude zapraqvo link !!!!
        
        grupa_sirovs[gsir_index].full_naziv = sirovina_link;
        
      });
      
    };
    
    
    var valid = this_module.valid;
    
    var grupa_sirovs_props = {
      // !!!findsirov!!!
      desc: 'Samo za kreiranje tablice svih sirovina unutar trenutno izabrane grupe u sirovina modulu',
      local: true,
      list: grupa_sirovs,
      
      parent: "#grupa_box",
      
      return: {},
      /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */
      col_widths:[
        1, // "sirovina_sifra",
        2, // "full_naziv",
        2, // "full_dobavljac",
        2, // grupa_naziv
        1, // "boja",
        1, // "cijena",
        1, // "kontra_tok",
        1, // "tok",
        1, // "po_valu",
        1, // "po_kontravalu",
        2, // "povezani_nazivi",
       
        
        1.5, // "pk_kolicina",
        1.5, // "nk_kolicina",
        1.5, // "order_kolicina",
        1.5, // "sklad_kolicina"
        1.5, // last_status
        4, // spec_docs
      
      ],
      show_cols: [
        "sirovina_sifra",
        "full_naziv",
        "full_dobavljac",
        "grupa_naziv",
        "boja",
        "cijena",
        "kontra_tok",
        "tok",
        "po_valu",
        "po_kontravalu",
        "povezani_nazivi",
        
        
        `pk_kolicina`,
        `nk_kolicina`,
        `order_kolicina`,
        `sklad_kolicina`,
        `last_status`,
        `spec_docs`,
        
      ],
      custom_headers: [
        
        `ŠIFRA`,  // "sirovina_sifra",
        `NAZIV`,  // "full_naziv",
        `DOBAV.`,  // "full_dobavljac",
        `GRUPA`,  // "grupa_naziv",
        `BOJA`,  // "boja",
        `CIJENA`,  // "cijena",
        `KONTRA TOK`,  // "kontra_tok",
        `TOK`,  // "tok",
        `VAL`,  // "po_valu",
        `KONTRA VAL`,  // "po_kontravalu",
        `POVEZANO`,  // "povezani_nazivi",
        
        `PK RESERV`,  // "pk_kolicina",
        `NK RESERV`,  // "nk_kolicina",
        `NARUČENO`,  // "order_kolicina",
        `SKLADIŠTE`,  // "sklad_kolicina",
        `STATUS`,
        
        `DOCS`, // spec_docs
        
      ],
      
      sums: [
        `pk_kolicina`,
        `nk_kolicina`,
        `order_kolicina`,
        `sklad_kolicina`,
      ],
      
      format_cols: {
        "sirovina_sifra": "center",
        "grupa_naziv": "center",
        "boja": "center",
        "cijena": valid.cijena.decimals,
        "kontra_tok": valid.kontra_tok.decimals,
        "tok": valid.tok.decimals,
        "po_valu": valid.po_valu.decimals,
        "po_kontravalu": valid.po_kontravalu.decimals,
        
        "pk_kolicina": valid.pk_kolicina.decimals,
        "nk_kolicina": valid.nk_kolicina.decimals,
        "order_kolicina": valid.order_kolicina.decimals,
        "sklad_kolicina": valid.sklad_kolicina.decimals,
      },
      query: {},
      
      show_on_click: false,
      
      cit_run: function(state) { console.log(state); },
      
      
    };
    
    if ( grupa_sirovs.length > 0 ) create_cit_result_list( grupa_sirovs, grupa_sirovs_props );

  };
  this_module.make_grupa_list = make_grupa_list;
  
  function make_sirov_rabats_list(data) {
    
    cit_local_list.sirov_rabats_for_table = (data.rabats && data.rabats.length > 0) ? data.rabats : [];
    var ibans_props = {
      desc: 'samo za kreiranje tablice svih rabata unutar jedne sirovine',
      local: true,

      list: 'sirov_rabats_for_table',

      show_cols: ['min', "max", 'perc', 'button_edit', 'button_delete' ],
      format_cols: { min: 2, max: 2, perc: 2 },
      col_widths: [3, 3, 2, 1, 1],
      parent: "#sirov_rabats_box",
      return: {},
      show_on_click: false,

      cit_run: function(state) { console.log(state); },

      button_edit: function(e) { 


        e.stopPropagation(); 
        e.preventDefault();

        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var index = find_index(data.rabats, sifra , `sifra` );

        window.current_sirov_rabat = cit_deep( data.rabats[index] );


        $('#save_rabat').css(`display`, `none`);
        $('#update_rabat').css(`display`, `flex`);


        format_number_input(window.current_sirov_rabat.min, $('#'+rand_id+`_rabat_min`)[0], null);
        format_number_input(window.current_sirov_rabat.max, $('#'+rand_id+`_rabat_max`)[0], null);
        format_number_input(window.current_sirov_rabat.perc, $('#'+rand_id+`_rabat_perc`)[0], null);      


      },
      button_delete: async function(e) {


        e.stopPropagation(); 
        e.preventDefault();

        var this_button = this;

        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          data.rabats = delete_item(data.rabats, sifra , `sifra` );

          $(`#`+parent_row_id).remove();

        };

        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj Rabat?`, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj Rabat?`, null, 'yes/no', delete_yes, delete_no, null);


      },

    };
    // ako imam išta u tom arraju onda napravi tablicu !!!!
    if (cit_local_list.sirov_rabats_for_table.length > 0 ) create_cit_result_list(cit_local_list.sirov_rabats_for_table, ibans_props );
  };
  this_module.make_sirov_rabats_list = make_sirov_rabats_list;

  
  function save_current_sirov_rabat() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    if ( !window.current_sirov_rabat ) window.current_sirov_rabat = {};
    
    if ( !window.current_sirov_rabat.sifra ) window.current_sirov_rabat.sifra = `sirrabat`+cit_rand();
    
    var min = cit_number($('#'+rand_id+`_rabat_min`).val());
    window.current_sirov_rabat.min = $.isNumeric(min) ? min : null;
    
    var max = cit_number($('#'+rand_id+`_rabat_max`).val());
    window.current_sirov_rabat.max = $.isNumeric(max) ? max : null;
        
    var perc = cit_number($('#'+rand_id+`_rabat_perc`).val());
    window.current_sirov_rabat.perc = $.isNumeric(perc) ? perc : null;
    
    

    if ( 
      (!window.current_sirov_rabat.min && window.current_sirov_rabat.min !== 0) 
      ||
      !window.current_sirov_rabat.max
      ||
      !window.current_sirov_rabat.perc
    ) {
      popup_warn(`Potrebno je upisati sve podatke !!!!`);
      return;
    };
    
 
    data.rabats = upsert_item(data.rabats, window.current_sirov_rabat, `sifra`);
    
    data.rabats_flat = JSON.stringify(data.rabats);
    
    
    this_module.make_sirov_rabats_list(data);
    
    window.current_sirov_rabat = null;
    
    
    $('#'+rand_id+`_rabat_min`).val("");
    $('#'+rand_id+`_rabat_max`).val("");
    $('#'+rand_id+`_rabat_perc`).val("");
    
    
    if ( this.id == `update_rabat` ) {
      $(this).css(`display`, `none`);
      $(`#save_rabat`).css(`display`, `flex`);
    };
    
  };
  this_module.save_current_sirov_rabat = save_current_sirov_rabat;
  

  
  function save_sirov() {
    
    var this_comp_id = $('#save_sirov_btn').closest('.cit_comp')[0].id; // useo sam ovaj gumb bezveze  - nije bitno koji element
    var data = this_module.cit_data[this_comp_id];
    
    // kada je velprom proizvod nemoj tražiti da klasa sirovine bude obavezna !!!!
    var exeptions = function (  valid_key, valid_obj, data ) {
      var required = valid_obj.required;
      if ( valid_key == "klasa_sirovine" && data.tip_sirovine?.sifra == "TSIR10" /* PROIZVOD VELPROM  */ ) {
        required = false;
      };
      
      
      if ( valid_key == "klasa_sirovine" && data.tip_sirovine?.sifra == "TSIR9PROC" /* PROCESNI ELEMENT  */ ) {
        required = false;
      };
      
      return required;
    };
    
    
    var required_ok = cit_required( this_module.valid, this_module.cit_data[this_comp_id], exeptions );
    
    if ( required_ok.counter > 0 ) {
      popup_warn(`Niste upisali ove obavezne podatke:<br>${required_ok.podaci}`);
      toggle_global_progress_bar(false);
      $(`#save_sirov_btn`).css("pointer-events", "all");
      return;
    };

    
    if ( 
      (
        data.dobavljac?._id == "616ed91eb5b0d06adf5b4b35"   /* TO JE VELPROM */ 
        ||
        data.alat_owner?._id == "616ed91eb5b0d06adf5b4b35"  /* TO JE VELPROM */ 
      )
      
      &&
      
      ( data.tip_sirovine && data.tip_sirovine?.sifra !== "TSIR9PROC" && data.tip_sirovine?.sifra !== "TSIR10" ) // nije pomoćni i nije procesni element
      
      
    ) {
      
      if ( !data.pro_cijena ) {
        popup_warn(`Ako je VELPROM ILI LASER PROFI FORMA D.O.O. DOBAVLJAČ ili PROIZVOĐAČ ove robe, obavezno morate upisati proizvodnu cijenu!!!`);
        toggle_global_progress_bar(false);
        $(`#save_sirov_btn`).css("pointer-events", "all");
        return;
      };
      
    };
    
    
    /* AKO NIJE VELPROM */ 
    if ( data.dobavljac?._id !== "616ed91eb5b0d06adf5b4b35" ) {
      
      if ( !data.alt_cijena ) {
        popup_warn(`Obavezno upisati cijenu dobavljača tj nabavnu cijenu!!!`);
        toggle_global_progress_bar(false);
        $(`#save_sirov_btn`).css("pointer-events", "all");
        return;
      };
      
    };
    
    
    
    $(`#save_sirov_btn`).css("pointer-events", "none");
    toggle_global_progress_bar(true);
    
    $.each( data, function( key, value ) {
      // ako je objekt ili array onda ga flataj
      if ( $.isPlainObject(value) || $.isArray(value) ) {
        data[key+'_flat'] = JSON.stringify(value);
      };
    });
    
    
    if ( data.kvaliteta_1 && data.kvaliteta_2) {
      data.kvaliteta_mix = data.kvaliteta_1 + "/" + data.kvaliteta_2;
    };
    
    if ( data.kontra_tok !== null  && data.tok !== null ) {
      data.kontra_tok_x_tok = "P" + data.kontra_tok + "X" + data.tok;
    };
    
    if ( data.po_valu !== null  && data.po_kontravalu !== null ) {
      data.val_x_kontraval = data.po_valu + "X" + data.po_kontravalu;
    };
    
    
    if ( data.alat_val !== null  && data.alat_kontra !== null ) {
      data.alat_prirez = "A" + data.alat_val + "X" + data.alat_kontra;
    };
    
    
    data.full_naziv = window.concat_naziv_sirovine(data);
    
    this_module.cit_data[this_comp_id] = save_metadata( this_module.cit_data[this_comp_id] );
    
    
    // od sada radim s kopijom data
    // od sada radim s kopijom data
    // od sada radim s kopijom data
    
    var data = cit_deep( this_module.cit_data[this_comp_id] );
    
    // od sada radim s kopijom data
    // od sada radim s kopijom data
    // od sada radim s kopijom data
    
    // ------------- START --------------- PRETVORI SVE STATUS OBJEKTE U SAMO _id array ----------------------------
    
    var status_ids = [];
    // obriši statuse jer ću id uzimati ručno !!!!!
    if ( data.statuses && $.isArray(data.statuses) && data.statuses.length > 0 ) {
      $.each(data.statuses, function( ind, status )  {
        status_ids.push(status._id);
      });
    };
    
    data.statuses = status_ids;
    
    
    if ( data.dobavljac && data.dobavljac._id ) {
      data.dobavljac_flat = JSON.stringify(data.dobavljac);
      data.dobavljac = data.dobavljac._id;
    } else {
      data.dobavljac = null;
    };
    
    
    if ( data.original && data.original._id ) {
      data.original = data.original._id;
    } else {
      data.original = null;
    };
    
    
    if ( data.alat_owner && data.alat_owner._id ) {
      data.alat_owner = data.alat_owner._id;
    } else {
      data.alat_owner = null;
    };
    
    
    var list_kupaca_ids = [];
    if ( data.list_kupaca && data.list_kupaca?.length > 0 ) {
      $.each(data.list_kupaca, function(k_ind, kupac) {
        list_kupaca_ids.push(kupac._id);
      });
      // zamjeni kupce za samo IDs
      data.list_kupaca = list_kupaca_ids;
    };
  
    
    // ------------ END ---------------- PRETVORI SVE STATUS OBJEKTE U SAMO _id array ----------------------------
    
    // return;
    

    $.ajax({
      headers: {
        'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
      },
      type: "POST",
      cache: false,
      url: `/save_sirov`,
      data: JSON.stringify( data ),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done(function (result) {
      
      console.log(result);
      if ( result.success == true ) {

        this_module.cit_data[this_comp_id]._id = result.sirov._id;
        this_module.cit_data[this_comp_id].sirovina_sifra = (result.sirov.sirovina_sifra || null);
        
        var rand_id = this_module.cit_data[this_comp_id].rand_id;
        $(`#`+rand_id+`_sirovina_sifra`).val( this_module.cit_data[this_comp_id].sirovina_sifra );
        
        
        var hash_data = {
          sirov: result.sirov._id,
          status: null
        };

        update_hash(hash_data);
        
        cit_toast(`PODACI SU SPREMLJENI!`);

      } else {
        if ( result.msg ) popup_error(result.msg);
        if ( !result.msg ) popup_error(`Greška prilikom spremanja!`);
      };

    })
    .fail(function (error) {
      
      console.log(error);
      popup_error(`Došlo je do greške na serveru prilikom spremanja!`);
    })
    .always(function() {
      
      toggle_global_progress_bar(false);
      $(`#save_sirov_btn`).css("pointer-events", "all");
      
      
    });


    
    
  };
  this_module.save_sirov = save_sirov;
  
  
  function copy_sirov() {
    
    
    var data = get_comp_data( $(this) , null, this_module);
    var rand_id =  data.rand_id;
    
    
    if ( !data._id ) {
      popup_warn(`Ne možete kopirati robu koja nije spremljena !!!`);
      return;
    };
    
    delete data._id;
    
    data.grupa = null;
    $(`#` + rand_id + `_grupa`).val(``);
    this_module.make_grupa_list(data);
    
    
    data.sirovina_sifra = null;
    $(`#` + rand_id + `_sirovina_sifra`).val(``);
    
    data.naziv = null;
    $(`#` + rand_id + `_naziv`).val(``);
    
    data.stari_naziv = null;
    $(`#` + rand_id + `_stari_naziv`).val(``);
    
    popup_warn(`Naziv i Stari naziv/šifra te grupa sirovine su obrisani!!!<br>Potrebno je upisati te podatke i spremniti kopiju.`);
    
  };
  this_module.copy_sirov = copy_sirov;
  
  
  
  function make_specs_list(data) {
    
    // SPECS OD SIROVINE

    var specs_for_table = [];
    if ( data.specs && data.specs.length > 0 ) {
      
      $.each(data.specs, function(index, spec_obj ) {
        
        var { sifra, time, komentar } = spec_obj;
        
        var all_docs = "";
        if ( spec_obj.docs && $.isArray(spec_obj.docs) ) {
          
          
          var criteria = [ '~time' ];
          multisort( spec_obj.docs, criteria );
          
          $.each(spec_obj.docs, function(doc_index, doc) {
            var doc_html = 
            `
            <div class="docs_row">
              <div class="docs_date">${ cit_dt(doc.time).date_time }</div>
              <div class="docs_link">${ doc.link }</div>
            </div>
            `;
            
            all_docs += doc_html
            
          }); // kraj loopa po docs
          
        }; // kraj ako ima docs
        
        
        specs_for_table.push({ sifra, time, komentar, docs: all_docs });
        
      }); // kraj loopa svih specs
      
    };
    
    
    
    var specs_props = {
      
      desc: 'samo za kreiranje tablice svih specifikacija unutar sirovine ',
      local: true,
      
      list: specs_for_table,
      
      show_cols: [ "komentar", "time", "docs", 'button_edit', 'button_delete' ],
      format_cols: { time: "date_time" },
      col_widths: [
        
        4, // "komentar",
        2, // "time",
        4, // "docs",
        1, // 'button_edit',
        1, // 'button_delete'
      
      ],
      parent: "#sirov_specs_box",
      return: {},
      show_on_click: false,
      cit_run: function(state) { console.log(state); },
      
      button_edit: function(e) { 
        
        e.stopPropagation(); 
        e.preventDefault();
        
        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var index = find_index(data.specs, sifra , `sifra` );

        window.current_specs = cit_deep( data.specs[index] );
  
        
        $('#save_specs').css(`display`, `none`);
        $('#update_specs').css(`display`, `flex`);
        
        $('#'+rand_id+`_specs_komentar`).val( window.current_specs.komentar || "");
        
        console.log("kliknuo edit za specifikacije sirovine");
        
        
      },
      button_delete: async function(e) {
        
        e.stopPropagation();
        e.preventDefault();
        var this_button = this;
        
        console.log("kliknuo DELETE za specifikacije sirovine");
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          data.specs = delete_item(data.specs, sifra , `sifra` );
          
          $(`#`+parent_row_id).remove();
  
        };
        
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ove Specifikacije?`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ove Specifikacije?`, null, 'yes/no', delete_yes, delete_no, null);
        
        
      },
      
    };
    if ( specs_for_table.length > 0 ) create_cit_result_list(specs_for_table, specs_props );
      
    
  };
  this_module.make_specs_list = make_specs_list;
  
  

  function update_sirov_docs(files, button, arg_time) {

    var new_docs_arr = [];
    $.each(files, function(f_index, file) {

      var file_obj = {
        sifra: `doc`+cit_rand(),
        time: arg_time,
        link: `<a href="/docs/${time_path(arg_time)}/${file.filename}" target="_blank" onClick="event.stopPropagation();" >${ file.originalname }</a>`,
      };
      new_docs_arr.push( file_obj );
    });

    console.log( new_docs_arr );

    // napravi novi objekt ako current ne postoji
    if ( !window.current_specs ) window.current_specs = {};


    if ( $.isArray(window.current_specs.docs) ) {
      // ako postoji docs array onda dodaj nove filove
      window.current_specs.docs = [ ...window.current_specs.docs, ...new_docs_arr ];
    } else {
      // ako ne postoji onda stavi ove nove filove
      window.current_specs.docs = new_docs_arr;
    };    

  };
  this_module.update_sirov_docs = update_sirov_docs;
  
  
  function save_current_specs() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    var comp_id = rand_id+`_sirov_docs`;
    
    
    // vrati nazad da gumb za save dokumenata  bude vidljiv
    // ionako ću sakriti parent i cijelu listu obrisati
    $(`#${comp_id}_upload_btn`).css(`display`, `flex`);
    
    // obriši listu
    $(`#${comp_id}_list_items`).html( "" );
    // sakrij listu i button
    $(`#${comp_id}_list_box`).css( `display`, `none` );
    
    
    if ( !window.current_specs ) window.current_specs = {};
    
    // ako nema sifre od spec onda napravi novu sifru
    // ako nema sifre od spec onda napravi novu sifru
    if ( !window.current_specs.sifra ) window.current_specs.sifra = `specs`+cit_rand();
    
    window.current_specs.komentar = $('#'+rand_id+`_specs_komentar`).val();
    
    window.current_specs.time = window.current_specs.time || Date.now(); 
    
    if ( !window.current_specs.komentar ) {
      popup_warn(`Potrebno je upisati kratki opis specifikacija !!!`);
      return;
    };
    
    // ako su docs undefined pretvori ih u null
    if ( !window.current_specs.docs ) window.current_specs.docs = null;
    
    data.specs = upsert_item(data.specs, window.current_specs, `sifra`);
    data.specs_flat = JSON.stringify(data.specs);
    
    // update choose spec list
    $(`#`+rand_id+`_choose_spec`).data(`cit_props`).list = data.specs;
    
    this_module.make_specs_list(data);
    
    window.current_specs = null;
    
    $('#'+rand_id+`_specs_komentar`).val("");
    
    if ( this.id == `update_specs` ) {
      $(this).css(`display`, `none`);
      $(`#save_specs`).css(`display`, `flex`);
    };
    
    
  };
  this_module.save_current_specs = save_current_specs;
  
  
  
  
  async function run_save_location(this_button) {
    
    
    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        
    if ( !window.cit_user ) {
      popup_warn(`Niste ulogirani !!!!`);
      return;
    };


    // ako user nije izabrao lokaciju onda stavi NEMA LOKACIJU
    if ( !data.location_from ) {
      
      data.location_from = { 
        sifra: "NEMALOC",
        palet_sifre: `&nbsp;&nbsp;&#8212;&nbsp;&nbsp;`,
        count: data.selected_primka ? primka_count : 0,
        sklad_naziv: "NEMA LOKACIJU",
        sector_sifra: ``,
        level_sifra: ``,
        shelf_naziv: ``,
      };

    };

    
    refresh_simple_cal( $('#'+rand_id+`_loc_est_date`) );

    
    var loc_sifra = null;
    
    // ako je ovo update onda spremi POSTOJEĆU sifru !!!!
    if ( this_button.id == `update_sirov_location_btn` ) {
      
      loc_sifra = window.current_location.sifra;
      
    } else if ( this_button.id == `save_sirov_location_btn` ) {
      // AKO JE SAVE BUTTON ONDA NARAPVI NOVU ŠIFRU
      loc_sifra = `sirloc`+cit_rand();
      
    } else if ( this_button.id == `save_variant_sirov_location_btn` ) {
      // AKO JE VARIJACIJA LOKACIJE ISTO NAPRAVI NOVU ŠIFRU !!
      loc_sifra = `sirloc`+cit_rand();  
      
    };
    
 
    window.current_location = {
      

      sifra: loc_sifra,
      
      komentar:  data.loc_komentar,  
      user: {
        user_id: window.cit_user._id,
        user_number: window.cit_user.user_number,
        full_name: window.cit_user.full_name,
        email: window.cit_user.email,
      },

      time: Date.now(),
      
      palet_sifre: data.palet_sifre || [],
      
      ms_time: data.ms_time || null,
      
      primka_record: data.selected_primka?.refer_doc || null, // data.selected_primka
      primka_pro_record: data.selected_pro_primka?.refer_doc || null, // data.selected_pro_primka
      rn_record: data.selected_rn?.refer_doc || null,
      
      
      neto_masa_paketa_kg: data.neto_masa_paketa_kg,
      bruto_masa_paketa_kg: data.bruto_masa_paketa_kg,

      neto_masa_palete_kg: data.neto_masa_palete_kg,
      bruto_masa_palete_kg: data.bruto_masa_palete_kg,

      location_from: data.location_from || null,
      
      palet_count: data.palet_count,
      
      box_count_on_palet: data.box_count_on_palet,
      red_count_on_palet: data.red_count_on_palet,
      box_count_in_red: data.box_count_in_red,

      palet_unit_count: data.palet_unit_count,
      paleta_x: data.paleta_x,
      paleta_y: data.paleta_y,
      paleta_z: data.paleta_z,

      box_count: data.box_count,
      
      box_unit_count: data.box_unit_count,
      box_x: data.box_x,
      box_y: data.box_y,
      box_z: data.box_z,

      sklad: data.sklad,
      sector: data.sector,
      level: data.level,
      shelf: data.alat_shelf, 
      
    };


    var count = 0;

    // ako ima količinu na paleti 
    if ( data.palet_unit_count ) count = data.palet_unit_count;

    // količina na paleti * broj paleta
    if ( 
      data.palet_count && data.palet_count > 1 
      && 
      data.palet_unit_count
    ) { count = data.palet_unit_count * data.palet_count; }

    // količina u pakiranju odvojeno
    if ( data.box_unit_count ) count = data.box_unit_count;
    

    // količina u pakiranj * broj pakiranja
    if ( 
      data.box_count
      &&
      data.box_unit_count
      ) { count = data.box_count * data.box_unit_count; };

      if (
        data.box_count_on_palet
        &&
        data.box_count
      ) {
        popup_warn(
`Ne možete u isto vrijeme upisati broj pakiranja odvojeno (bez palete) 
i broj pakiranja na paleti je to nema smisla!!!<br>
Ako su pakiranja odvojeno oda ih upisuj u polje odvojeno, 
ako su pakiranja na paleti onda ih upisuj na paleti !!!`);
        return;
    };

    // broj pakiranja na paleti * količina u paketu
    if (
      data.box_count_on_palet && data.box_count_on_palet > 1 
      &&
      data.box_unit_count
    ) {
      count = data.box_count_on_palet * data.box_unit_count;
    };



    // 
    if (
      data.palet_count && data.palet_count > 1 
      &&
      data.box_count_on_palet && data.box_count_on_palet > 1 
      &&
      data.box_unit_count
    ) {
      count = data.palet_count * data.box_count_on_palet * data.box_unit_count;
    };


    if (
      data.palet_unit_count
      &&
      data.box_unit_count
      && 
      !data.box_count_on_palet // dakle NIJE upisano koiko pakiranja na paleti
    ) {
      popup_warn(
`Ne možete u isto vrijeme upisati količinu u pakiranju 
i količinu na paleti!!!<br>
Ako je roba bez pakiranja onda upisju u polje jed. količina na paleti.<br>
Ako je roba u pakiranju i onda su pakiranja postavljena na paletu 
- onda upisuj količina u pakiranju i broj pakiranja na paleti !!!`);
      return;
      };
    
    
    if (
      data.palet_unit_count
      &&
      data.box_unit_count
      && 
      data.box_count_on_palet // UPISANO JE KOLIKO PAKIRANJA NA PALETI
    ) {
      
      if ( data.box_unit_count * data.box_count_on_palet !== data.palet_unit_count ) {
      
      popup_warn(
`Ukupna količina na paleti nije ista kao umnožak broja pakiranja na paleti i količine u pakiranju !!!`);
      return;
        
      };    
      
    };
    
    
    if ( !count  ) {
      popup_warn(`Potrebno je upisati količinu na paleti ili količinu u pakiranju !!!`);
      return;
    };

    window.current_location.count = count;
    
    
    data.locations = upsert_item(data.locations, window.current_location, `sifra`);
    
    this_module.make_locations_list(data);
    
    
    
    /* ------------------------------- RESETIRAM SVA POLJA ---------------------------------- */
    
    
    $.each( window.current_location, function(loc_key, loc_value) {
      data[loc_key] = null;
      $('#' + rand_id + `_` + loc_key ).val('');
    });
    
    // ovo su dodatni propsi koji nemaju isto ime kao propsi unutar current location
    // ovo su dodatni propsi koji nemaju isto ime kao propsi unutar current location
    // ovo su dodatni propsi koji nemaju isto ime kao propsi unutar current location
    
    data.alat_shelf = null;
    data.palet_sifre = null;
    data.ms_time = null;
    data.selected_primka = null;
    
    $('#'+rand_id+`_choose_location_from`).val('');
    $('#'+rand_id+`_alat_shelf`).val('');
    $('#'+rand_id+`_palet_sifra`).val(''); 
    $('#'+rand_id+`_loc_est_date`).val('');
    $('#'+rand_id+`_choose_primka`).val('');
    $('#'+rand_id+`_choose_pro_primka`).val('');
    $('#'+rand_id+`_choose_rn`).val('');
    
    
    // resetiram cijeli trenutni location !!!!!
    window.current_location = null;
    
    
    $(`#update_sirov_location_btn`).css(`display`, `none`);
    $(`#save_variant_sirov_location_btn`).css(`display`, `none`);
    $(`#save_sirov_location_btn`).css(`display`, `flex`);
    
    
    setTimeout(function () {
      $(`#save_sirov_btn`).trigger(`click`);
    }, 200);
    
   
    
  };
  this_module.run_save_location = run_save_location;
  
  
  
  function make_card_num_list(data) {
    
    var rand_card_id = cit_rand();
    
    $(`#card_num_list_box`).html(``);
    var card_list_html = ``;
    $.each(data.card_num_list , function( c_ind, card_sifra ) {
      card_list_html += 
        `<div  id="${card_sifra}_${rand_card_id}_skl_card_pill" class="skl_card_pill">
          ${card_sifra}<i id="${card_sifra}_${rand_card_id}_delete_card_num" class="far fa-times delete_card_num"></i>
        </div>`;
    });
    
    $(`#card_num_list_box`).html(card_list_html);
    
    
    setTimeout( function() {
      
      $(`.delete_card_num`).off(`click`);
      $(`.delete_card_num`).on(`click`, function() {
        
        var this_button = this;
        
        var this_card_num =  this_button.id.split(`_`)[0];
        var this_rand_id =  this_button.id.split(`_`)[1];
        
        
        var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        
        $(`#${this_card_num}_${this_rand_id}_skl_card_pill`).remove();
        

        data.card_num_list = delete_item(data.card_num_list, this_card_num, null );
        
        
        this_module.make_card_num_list(data);
        
        console.log(data.card_num_list);
        
      });
      
    }, 50 );
        
    
    
    
  };
  this_module.make_card_num_list = make_card_num_list;
  
  
  function add_card_num() {
    
    var this_button = this;
    
    
    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    var new_card = $(`#` + rand_id + `_write_skl_card_num`).val();
    new_card = new_card.toUpperCase();
    
    if ( !new_card ) {
      popup_warn(`Polje je prazno :)`);
      
            
      // fokusiraj na popup ali sa zakašnjenjem da ne trigeriram popup
      setTimeout(function() {  
        console.log(` //////////////////////////////////////////////// FOCUS NA OK BTN `);
        $(`#cit_popup_modal_OK`).trigger(`focus`); 
      }, 500);
      
      return;
    };
    
    if ( new_card.length !== 5 ) {
      popup_warn(`Skladišna kartica uvijek mora imati 5 znakova !!!!`);
      
      // fokusiraj na body ali sa zakašnjenjem da ne trigeriram popup
      setTimeout(function() {  
        console.log(` //////////////////////////////////////////////// FOCUS NA OK BTN `);
        $(`#cit_popup_modal_OK`).trigger(`focus`); 
      }, 500);
      
      return;
    };
    
    // obriši input polje
    $(`#` + rand_id + `_write_skl_card_num`).val(``);
    
    
    data.card_num_list = upsert_item(data.card_num_list, new_card, null );
    
    console.log(data.card_num_list);
    
    this_module.make_card_num_list(data);

    
  };
  this_module.add_card_num = add_card_num;  
  

  async function save_sirov_location() {
    
    
    var this_button = this;
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    
    
    // ako je ovo kreiranje nove lokacije od postojeće lokacije
    // ako je ovo kreiranje nove lokacije od postojeće lokacije
    // ako je ovo kreiranje nove lokacije od postojeće lokacije
    
    if (  this_button.id == `save_variant_sirov_location_btn` ) {
      
            
      var popup_text = `
Jeste li sigurni da želite napraviti NOVU LOKACIJU na osnovu podataka od ove lokacije ?<br>
Provjerite sva polja !!!
`;
      
      function loc_variant_yes() {

        this_module.run_save_location(this_button);

      };

      function loc_variant_no() {
        show_popup_modal(false, popup_text, null );
      };
      
      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', loc_variant_yes, loc_variant_no, null);

     
    }
    else {
      this_module.run_save_location(this_button);
    };
    

    
  };
  this_module.save_sirov_location = save_sirov_location;
    
  
  
  
  function update_sirov_pics(files, button, arg_time) {
    
    var this_comp_id = $(`#save_sirov_btn`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    

    var new_docs_arr = [];
    $.each(files, function(f_index, file) {

      var file_obj = {
        filename: file.filename,
        sifra: `doc`+cit_rand(),
        time: arg_time,
        link: `<a href="/docs/${time_path(arg_time)}/${file.filename}" target="_blank" onClick="event.stopPropagation();" >${ file.originalname }</a>`,
      };
      new_docs_arr.push( file_obj );
    });

    console.log( new_docs_arr );

    // napravi novi objekt ako current ne postoji
    if ( !data.sirov_pics ) data.sirov_pics = [];


    if ( $.isArray(data.sirov_pics) ) {
      // ako postoji docs array onda dodaj nove filove
      data.sirov_pics = [ ...data.sirov_pics, ...new_docs_arr ];
    } else {
      // ako ne postoji onda stavi ove nove filove
      data.sirov_pics = new_docs_arr;
    };  
    
    this_module.make_sirov_gallery(data);
    
  };
  this_module.update_sirov_pics = update_sirov_pics;
  
  
  
  function make_sirov_gallery(data) {
    
    var this_comp_id = $(`#save_sirov_btn`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    
    $(`#`+rand_id + `_gallery_box`).html(``);
    
    /*
    ------------------------------------------------------
    data.sirov_pics = [
      
      { filename: `1647005857982-2022_01_sve.jpg` },
      { filename: `1647007246056-dobra_stara_vremena.jpg` },
      { filename: `1650353909135-tok_vs_klapna.png` },
      { filename: `1645001540096-screenshot_2021_08_23_at_10_24_50.png` },
      { filename: `1648032452988-meeting.png` },

      { filename: `1647005857982-2022_01_sve.jpg` },
      { filename: `1647007246056-dobra_stara_vremena.jpg` },
      { filename: `1650353909135-tok_vs_klapna.png` },
      { filename: `1645001540096-screenshot_2021_08_23_at_10_24_50.png` },
      { filename: `1648032452988-meeting.png` },
      
    ];
    ------------------------------------------------------
    */
    
    if ( $.isArray(data.sirov_pics) == false || data.sirov_pics?.length == 0 ) return;
    

    var cit_gal_images = ``;
    $.each(data.sirov_pics, function(p_ind, pic) {
      cit_gal_images += `<img src="/docs/${time_path(pic.time)}/${pic.filename}" style="width: 300px; height: auto;" />`;
    });
    
    var gallery = 
`
<div class="gallery_left_arrow"><i class="fas fa-arrow-alt-circle-left"></i></div>
<div class="gallery_right_arrow"><i class="fas fa-arrow-alt-circle-right"></i></div>
<div class="cit_gallery_parent" style="width: ${ 300 * data.sirov_pics.length }px;">${ cit_gal_images }</div>
`;
    
    $(`#` + rand_id + `_gallery_box`).html(gallery);

    
    setTimeout( function() {
      
      var parent = $(`#` + rand_id + `_gallery_box .cit_gallery_parent`);
      
      $(`#` + rand_id + `_gallery_box .gallery_left_arrow`).off(`click`);
      $(`#` + rand_id + `_gallery_box .gallery_left_arrow`).on(`click`, function() {
        
        var trans = parent.data(`pos_x`) || 0;
        if ( trans == 0 ) return;
        
        var new_trans = trans+300;
        
        parent.css(`transform`, `translateX(${new_trans}px)`);
        parent.data(`pos_x`, new_trans );
        
      });
      
      
      $(`#` + rand_id + `_gallery_box .gallery_right_arrow`).off(`click`);
      $(`#` + rand_id + `_gallery_box .gallery_right_arrow`).on(`click`, function() {
        
        var trans = parent.data(`pos_x`) || 0;
        
        var new_trans = trans-300;
        
        var window_width = $(window).width();
        var parent_width = parent.width();
        
        
        // ako sam došao do kraja slika onda vrati na nulu
        if ( trans <= 0 && parent_width + trans < window_width ) new_trans = 0;
        
        parent.css(`transform`, `translateX(${new_trans}px)`);
        parent.data(`pos_x`, new_trans );
      });
      
      
      $(`#` + rand_id + `_gallery_box img`).off(`click`);
      $(`#` + rand_id + `_gallery_box img`).on(`click`, function() {
        var image_bckg_layer = 
`<div id="image_bckg_layer">
<div class="gallery_left_arrow"><i class="fas fa-arrow-alt-circle-left"></i></div>
<div class="gallery_right_arrow"><i class="fas fa-arrow-alt-circle-right"></i></div>

  <img src="${this.src}" /> 
</div>
`;
        if ( $(`#image_bckg_layer`).length == 0 ) $(`body`).prepend(image_bckg_layer);
        
        
        
        
        setTimeout( function() {
          $(`#image_bckg_layer`).off(`click`);
          $(`#image_bckg_layer`).on(`click`,function() {
            $(`#image_bckg_layer`).remove();
          });
        }, 200);
        
        
        
      });
      
      
      
    }, 200 );
    
    
    
  };
  this_module.make_sirov_gallery = make_sirov_gallery;

  
  
  function save_price_hist() {
    
    
    var this_button = this;
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    
    
    refresh_simple_cal( $('#'+rand_id+`_price_hist_date`) );
    
    
    if ( !data._id ) {
      popup_warn(`Potrebno je spremiti robu prije upisivanja cijene`);
      return;
    };
    
    if ( !data.current_price_hist ) data.current_price_hist = {};
    if ( !data.current_price_hist.sifra ) data.current_price_hist.sifra = `pricehist`+cit_rand();
    
    // ako je spec undefined tj ne postoji pretvori to u null
    if ( !data.current_price_hist.spec ) data.current_price_hist.spec = null;
    
    data.current_price_hist.time = data.current_price_hist.time || Date.now(); 
    data.current_price_hist.from_date = data.price_hist_date || Date.now(); 
    data.current_price_hist.user = window.cit_user.full_name;
    
    
    // ako nije velprom proizvod tj nema pro cijenu
    if ( !data.pro_cijena ) {
    
      if ( !data.cijena || !data.alt_cijena ) {
        popup_warn(`Potrebno upisati cijenu i alt. cijenu.`);
        return;
      };
      
    };
    
    if ( data.pro_cijena && !data.cijena ) {
      
      popup_warn(`Potrebno upisati izlaznu cijenu za VELPROM proizvod.`);
      return;
    };
    
    
    if ( data.pro_cijena && data.alt_cijena ) {
      
      popup_warn(`Roba ne može imati proizvodnu cijenu i cijenu dobavljača u isto vrijeme - nema smisla :)`);
      return;
    };
    
    
    
    data.current_price_hist.alt_cijena = data.alt_cijena || null; 
    data.current_price_hist.pro_cijena = data.pro_cijena || null; 
    
    data.current_price_hist.cijena = data.cijena || null; 
    
    
    data.price_hist = upsert_item(data.price_hist, data.current_price_hist, `sifra`);
    
    this_module.make_price_hist_list(data);
    
    data.current_price_hist = null;
    
    $('#'+rand_id+`_price_hist_date`).val(``);
    data.price_hist_date = null;
    
  };
  this_module.save_price_hist = save_price_hist;
  
  
  function make_price_hist_list(data) {
    
    // PRICE HISTORY OD SIROVINE

    var price_hist_for_table = [];
    
    if ( data.price_hist?.length > 0 ) {
      
      
      $.each(data.price_hist, function(index, hist_obj ) {
        
        var { sifra, time, user, from_date, cijena, alt_cijena, pro_cijena, spec } = hist_obj;
        
        var all_docs = "";
        if ( hist_obj.spec && $.isArray(hist_obj.spec?.docs) ) {
          
          
          var spec = hist_obj.spec;
          
          var criteria = [ '~time' ];
          multisort( spec.docs, criteria );
          
          $.each(spec.docs, function(doc_index, doc) {
            
            var doc_html = 
            `
            <div class="docs_row">
              <div class="docs_date">${ cit_dt(doc.time).date_time }</div>
              <div class="docs_link">${ doc.link }</div>
            </div>
            `;
            
            all_docs += doc_html
            
          }); // kraj loopa po docs
          
        }; // kraj ako ima docs
        
        
        var time_and_user = cit_dt(time).date_time + `<br>` + (user || ``);
        
        price_hist_for_table.push({ sifra, time, user, time_and_user, from_date, cijena, alt_cijena, pro_cijena, price_docs: all_docs });
        
      }); // kraj loopa po price hist
      
    }; // ako je price hist length > 0
    
    
    
    var price_hist_props = {
      
      desc: 'samo za kreiranje tablice za price history ',
      local: true,
      
      list: price_hist_for_table,
      
      show_cols: [ 
        "time_and_user",
        "from_date",
        "cijena",
        "alt_cijena",
        "pro_cijena",
        "price_docs",
        
        'button_delete',
      ],
      custom_headers: [
        "USER",
        "OD KADA",
        "CIJENA",
        "DOB. CIJENA",
        "PRO. CIJENA",
        "DOCS",
        "DEL",
      ],
      format_cols: { 
 
        from_date: "date",
        cijena: 3,
        alt_cijena: 3,
        pro_cijena: 3,
      },
      col_widths: [
        
        2, // "time_and_user",
        2, // from_date
        1, // "cijena",
        1, // "alt_cijena",
        1, // pro cijena
        5, // "price_docs"
        
        1, // "button_delete"
        
      ],
      parent: "#price_hist_box",
      return: {},
      show_on_click: false,
      cit_run: function(state) { console.log(state); },


      button_delete: async function(e) {

        e.stopPropagation();
        e.preventDefault();


        console.log(`kliknuo na DELETE PRICE HISTORY`);


        var this_button = this;

        var popup_text = `Jeste li sigurni da želite OBRISATI OVAJ RED?`;  


        function price_delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);

          var price_hist = find_item( data.price_hist, sifra , `sifra` );


          if ( get_super_editors().indexOf(window.cit_user.user_number) == -1 ) {
            popup_warn(`Nemate ovlasti za brisanje ovog podatka !!!!`);
            return;
          };

          
          data.price_hist = delete_item(data.price_hist, sifra , `sifra` );
          
          $(`#`+parent_row_id).remove();

          
          // ponovo napravi listu !!!!!
          this_module.make_price_hist_list(data);

          this_module.save_sirov();

        }; // kraj delete yes funkcija



        function price_delete_no() {
          show_popup_modal(false, popup_text, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, popup_text, null, 'yes/no', price_delete_yes, price_delete_no, null);


      },

      
    }; // kraj price hist props !!!
    
    
    if ( price_hist_for_table.length > 0 ) {
     
      var criteria = [ '!from_date', '!time' ]; // prvo po from time 
      multisort( price_hist_for_table, criteria );

      create_cit_result_list(price_hist_for_table, price_hist_props );
    };
      
    
  };
  this_module.make_price_hist_list = make_price_hist_list;
  
  
  
  function web_pristupi_to_html(rand_id) {
    
    var this_comp_id = $(`#${rand_id}_web_pristupi`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    
    if ( data.web_pristupi ) {

      var web_html = "";
      var web_text_arr =   data.web_pristupi.split("\n");

      $.each(web_text_arr, function(index, line) {
        var line = line.trim();
        if ( line.indexOf(`https://`) > -1 || line.indexOf(`http://`) > -1 ) {
          // kreiraj a tag
          line = `<a href="${line}" target="_blank" onClick="event.stopPropagation();">${line}</a><br>`;
        } else {
          line += `<br>`; // if ( line.length > 5 )
        };
        web_html += line;
      });

      $(`#web_pristupi_box`).html(web_html);
      
      setTimeout( function() {
        // stavi custom visinu text area 
        $('#'+rand_id+`_web_pristupi`).css(`height`, $('#web_pristupi_box').height()+20 + "px" );

      }, 100 );

    };

  };
  this_module.web_pristupi_to_html = web_pristupi_to_html;
  
  
  function make_kontakti_in_sirov(state) {
   
    
    $("#link_to_partner").attr(`href`, `#`);
    console.log(state);

    var this_comp_id = $(`#find_partner_in_sirov`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    
    
    if ( state == null ) {
      data.dobavljac = null;
      $("#find_partner_in_sirov").find(".single_select").val("");
      $(`#svi_kontakti_box`).html("");
      return;
    };
    
    var full_partner = 
        state.naziv + 
        ( state.grupacija_naziv ? (", " + state.grupacija_naziv) : "" ) + 
        ( state.sjediste ? ", "+state.sjediste : "" );


    $("#find_partner_in_sirov").find(".single_select").val(full_partner);

    data.dobavljac = {
      _id: state._id,
      naziv: state.naziv,
      sjediste: state.sjediste,
      grupacija_naziv:  state.grupacija_naziv,
      status_naziv: state.status_naziv
    };

    console.log(data);
    
    $("#link_to_partner").attr(`href`, `#partner/${state._id}/status/null`);

    // na silu obriši sve prethodne kontakte (samo HTML)
    $(`#svi_kontakti_box`).html("");
    
    
    
    if ( window.sirov_find_mode ) {
      gen_find_query_select( this_module, state || null, "dobavljac", state?._id || null );
      return;
    };

    
    

    this_module.partner_module.make_kontakt_list(state);

    // čekaj da se pojave gumbi u listi kontakata za edit i delete
    // pa ih onda obriši :)
    // jer u projektima nema smisla editirati kontakte partnera !!!!
    wait_for(`$("#svi_kontakti_box .result_row_edit_btn").length > 0`, function() {
      
      $("#svi_kontakti_box .result_row_edit_btn").remove();
      $("#svi_kontakti_box .result_row_delete_btn").remove();
      
      // koristim array koji sam napravio od svih kontakata da bi napravio drop list za slanje dokumenata ( odabir adrese )
      // $(`#`+rand_id+`_doc_adresa`).data('cit_props').list = $(`#svi_kontakti_box`).data(`cit_result`);
      
    }, 5*1000); 

    
  };
  this_module.make_kontakti_in_sirov = make_kontakti_in_sirov;
  
  
  
  function fill_alat_owner( data ) {
    
    $("#link_to_alat_owner").attr(`href`, `#`);
    console.log(data.alat_owner);

    var this_comp_id = $(`#`+data.rand_id+`_alat_owner`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    if ( data.alat_owner == null ) return;
    
    
    var filtered_owner_array = this_module.find_partner.find_partner_filter( [ data.alat_owner ], jQuery );
    data.alat_owner = filtered_owner_array[0]; // uzmi samo prvi item jer mi vraća array !!!!
  
    
    var full_partner = 
        (data.alat_owner?.naziv || "") + 
        ( data.alat_owner?.grupacija_naziv ? (", " + data.alat_owner?.grupacija_naziv) : "" ) + 
        ( data.alat_owner?.sjediste ? ", "+data.alat_owner?.sjediste : "" );

    $(`#`+data.rand_id+`_alat_owner`).val(full_partner);
    
    $("#link_to_alat_owner").attr(`href`, `#partner/${data.alat_owner._id}/status/null`); 
    
    
  };
  this_module.fill_alat_owner = fill_alat_owner;
  
  

  function make_list_kupaca(data) {
    
    
    data.list_kupaca = this_module.find_partner.find_partner_filter( data.list_kupaca, jQuery );
    
    
    var list_kupaca_props =  {
      // !!!findpartner!!!
      desc: 'pretraga partnera kupaca ove robe tj velprom proizvoda u modulu sirovine',
      local: true,
      
      list: data.list_kupaca,
      
      custom_headers: [
        "ŠIFRA",
        "OIB",
        "NAZIV",
        "GRUPACIJA",
        "STATUS PARTNERA",
        "SJEDIŠTE",
        "DEL",
      ],
      col_widths:[
        1, // "sifra_link",
        1, // "oib",
        3, // "naziv",
        1, // "grupacija_naziv",
        1, // "status_naziv",
        5, // "sjediste",
        0.5, // delete btn
      
      ],
      show_cols: [
        /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */
        "sifra_link",
        "oib",
        "naziv",
        "grupacija_naziv",
        "status_naziv",
        "sjediste",
        "button_delete",
      ],
      format_cols: {
        "sifra_link": "center",
        "oib": "center",
        "grupacija_naziv": "center",
        "status_naziv": "center",
      },
      
      parent: "#list_kupaca_box",
      return: {},
      show_on_click: false,
      cit_run: function(state) { console.log(state); },
      
      button_delete: async function(e) {

        e.stopPropagation();
        e.preventDefault();


        console.log(`kliknuo na DELETE KUPCA U VELPROM ROBI`);


        var this_button = this;


        if ( get_super_editors().indexOf( window.cit_user.user_number ) == -1 ) {
          popup_warn(`Nemate ovlsti za izbacivanje kupca s liste!!`);
          return;
        };


        function remove_kupac_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          
          $(`#` + parent_row_id).remove();

          data.list_kupaca = delete_item( data.list_kupaca, sifra , `_id` );
          
          this_module.save_sirov();

        }; // kraj delete yes funkcija

        var popup_text = `Jeste li sigurni da želite izbaciti KUPCA?`;

        function remove_kupac_no() {
          show_popup_modal(false, popup_text, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, popup_text, null, 'yes/no', remove_kupac_yes, remove_kupac_no, null);


      },
      
      
      
        
    };  // kraj list kupaca props
    


    if (data.list_kupaca?.length > 0 ) {
      create_cit_result_list( data.list_kupaca, list_kupaca_props );
    };
  
  };
  this_module.make_list_kupaca = make_list_kupaca;
  
  async function get_dobavljac(dobavljac_id) {
    
    return new Promise( async function(resolve, reject) {
    
      var db_partner = null;

      var props = {
        filter: null,
        url: '/find_partner',
        query: { _id: dobavljac_id },
        desc: `pretraga dobavljaca da dobijem sve podatke kako bih mogao generirati dokument  !!!`,
      };

      try {
        db_partner = await search_database(null, props );
      } catch (err) {
        console.log(err);
        resolve(null);

      };

      // ako je našao nešto onda uzmi samo prvi član u arraju
      if ( db_partner && $.isArray(db_partner) && db_partner.length > 0 ) {
        db_partner = db_partner[0];
        resolve(db_partner);
      } else {
        resolve(null);
      };
    
    }); // kraj promisa
    
    
  };
  this_module.get_dobavljac = get_dobavljac;

  
  async function get_povezane_statuse(status_query) {
    
    return new Promise( async function(resolve, reject) {
    
      var db_result = null;

      var props = {
        filter: null,
        url: '/find_status',
        query: status_query,
        desc: `pretraga statusa prilikom generairanja recorda za sirovine  !!!`,
      };

      try {
        db_result = await search_database(null, props );
      } catch (err) {
        console.log(err);
        resolve(null);

      };

      // ako je našao nešto onda uzmi samo prvi član u arraju
      if ( db_result && $.isArray(db_result) && db_result.length > 0 ) {
        resolve(db_partner);
      } else {
        resolve(null);
      };
    
    }); // kraj promisa
    
    
  };
  this_module.get_povezane_statuse = get_povezane_statuse;

 
  async function create_or_print_skl_card( this_button, arg_action_type, empty_cards_count ) {


    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

/*

    
    if ( !window.current_location.sklad && !data.sklad ) {
      popup_warn(`Odaberite skladište!`);
      return;
    };

    if ( !window.current_location.sector && !data.sector ) {
      popup_warn(`Odaberite sektor!`);
      return;
    };


    if ( !window.current_location.level &&  !data.level ) {
      popup_warn(`Odaberite level!`);
      return;
    };
    
    // ako je alat tj klasa ----> KS5
    if ( !data.klasa_sirovine?.sifra == "KS5" && ( !window.current_location.alat && !data.alat_shelf ) ) {
      popup_warn(`Za alat obavezno upisati policu!`);
      return;
    };
    
*/

    var preview_mod = await get_cit_module('/modules/previews/cit_previews.js', `load_css`);
    
    var primka_record =  window.current_location?.primka_record || data.selected_primka || null;
    var primka_pro_record =  window.current_location?.primka_pro_record || data.selected_pro_primka || null;
    var rn_record = window.current_location?.rn_record || data.selected_rn || null;
  
    
    if ( arg_action_type == `new_cards` && empty_cards_count ) {
      
      
      var get_palet_sifre = await get_new_palet_sifre(empty_cards_count);
      
      if ( get_palet_sifre.success == true ) {
        
        data.palet_sifre = get_palet_sifre.sifre;
        
      };
      
      
    }
    else if ( arg_action_type == `new_cards` && !empty_cards_count ) {
      
      var card_count = 0;
      
      // ali ako ima palet count onda uzmi taj broj
      if ( window.current_location?.palet_count ) card_count = window.current_location.palet_count;
      
      // ako ima pakete na paleti onda OVERRIDE pa uzmi taj broj
      if ( window.current_location?.box_count_on_palet ) {
        card_count = window.current_location.box_count_on_palet;
        // ako ima još i broj paleta onda pomnoži broj pakiranja na paleti s brojem paleta
        if ( window.current_location.palet_count ) card_count = window.current_location.box_count_on_palet * window.current_location.palet_count;
      };
      
      
      // ako ima box count ODVOJENO onda OPET OVERRIDE NA TAJ BROJ
      if ( window.current_location?.box_count ) card_count = window.current_location.box_count;
      

      var get_palet_sifre = await get_new_palet_sifre(card_count);
      
      
      if ( get_palet_sifre.success == true ) {
        
        data.palet_sifre = get_palet_sifre.sifre;
        // stavi nove sifre u current location i napravi UPSERT U LOCATIONS !!!
        window.current_location.palet_sifre = data.palet_sifre;
        
        data.locations = upsert_item( data.locations, window.current_location, `sifra`);
        
        setTimeout( function() {
          // prikaži nove šifre koje sam upravo dobio sa servera
          this_module.make_locations_list(data);
          $(`#save_sirov_btn`).trigger(`click`);
        }, 700 );
        
      };
      
      
    } 
    else if ( arg_action_type == `print_cards` ) {
      
      data.palet_sifre = window.current_location.palet_sifre;

      // ako se radi samo o printanju sklad akrtice
      if ( !data.palet_sifre || data.palet_sifre?.length == 0 ) {
        popup_warn(
`
Nema niti jedne šifre skladišne kartice !!!<br>
Potrebno je ili SPREMITI NOVU lokaciju i zatim kreirati nove skladišne kartice ili izabrati EDITIRANJE POSTOJEĆE lokacije !!!
`);
        return;
      };
    
    };
    

    var db_partner = null;
    var dobavljac_id = data.dobavljac?._id ? data.dobavljac._id : null;

    if ( dobavljac_id ) {
      db_partner = await this_module.get_dobavljac(dobavljac_id);
    };

    if ( !db_partner ) {
      popup_error(`Greška prilikom pretrage dobavljača u bazi !!!!`);
      return;
    };

    var data_for_doc = {
      
      sifra: "dfd"+cit_rand(),
      
      time: Date.now(),

      for_module: `sirov`,

      proj_sifra : null,
      item_sifra : null,
      variant : null,
      status: null,
      
      /*
      ---------------------------------------- OVO SU SVI PODACI ZA LOCATION OBJ ----------------------------------------
      sifra,
      palet_sifre,
          
      ms_record,
      ms_time,

      primka_record,
      primka_pro_record,

      count,

      neto_masa_paketa_kg,
      bruto_masa_paketa_kg,

      neto_masa_palete_kg,
      bruto_masa_palete_kg,

      choose_location_from,
      choose_primka,
      choose_palet_sifra,

      palet_count,

      box_count_on_palet,
      red_count_on_palet,
      box_count_in_red,

      palet_unit_count,
      paleta_x,
      paleta_y,
      paleta_z,

      box_count,
      box_unit_count,
      box_x,
      box_y,
      box_z,

      sklad,
      sector,
      level,
      shelf,
      
      ---------------------------------------- OVO SU SVI PODACI ZA LOCATION OBJ ----------------------------------------
      */
      
      primka_record: primka_record,
      primka_pro_record: primka_pro_record,
      rn_record: rn_record,
      
      jedinica: data.osnovna_jedinica?.jedinica || "",
      
      palet_sifre: data.palet_sifre,
      
      palet_unit_count: window.current_location.palet_unit_count || null,
      box_unit_count: window.current_location.box_unit_count || null,
      
      
      sklad: window.current_location.sklad || data.sklad,
      sector: window.current_location.sector || data.sector,
      level: window.current_location.level || data.level,
      alat_shelf: window.current_location.shelf || data.alat_shelf,

      sirov_id: data._id || null,
      full_record_name: (data.sirovina_sifra + "--" + window.concat_naziv_sirovine(data) + "--" + data.dobavljac?.naziv ),

      
      doc_valuta: ( data.doc_valuta?.valuta || "HRK" ), // default je HRK
      doc_valuta_manual: (data.doc_valuta_manual || null ), // manual tečaj
      referent: window.cit_user.full_name,
      place: `Velika Gorica`,

      doc_type: `sklad_kartica`,
      doc_num: "----",

      oib: (db_partner.oib || db_partner.vat_num || ""),
      partner_name: db_partner.naziv,
      partner_adresa: null,
      partner_id: db_partner._id,
      

      /*
      partner_place: `67122 ALTRIP`,
      partner_country: `Germany`,
      */

      rok: Date.now(),
      dobav_ponuda_sifra: null,

      items: null,

    };

    // return;

    preview_mod.generate_cit_doc( data_for_doc, data.doc_lang?.sifra || "hr" );

  };
  this_module.create_or_print_skl_card = create_or_print_skl_card;
  
  
  
  async function create_or_print_EXIT_card( this_button, arg_action_type ) {


    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    
    if ( !window.current_location.sklad && !data.sklad ) {
      popup_warn(`Odaberite skladište!`);
      return;
    };

    if ( !window.current_location.sector && !data.sector ) {
      popup_warn(`Odaberite sektor!`);
      return;
    };


    if ( !window.current_location.level &&  !data.level ) {
      popup_warn(`Odaberite level!`);
      return;
    };
    
    // ako je alat
    if ( !data.klasa_sirovine?.sifra == "KS5" && ( !window.current_location.alat && !data.alat_shelf ) ) {
      popup_warn(`Za alat obavezno upisati policu!`);
      return;
    };
    
    
        
    if ( !data.partner_doc_sirovs[0].original ) {
      popup_warn(`Nedostaju podaci proizvoda !!!`);
      return;
    };
    
    
    var DB_product = await ajax_find_query( 
      { _id: { $in: [ data.partner_doc_sirovs[0].original ] } },
      `/find_product`,
      null
    );
    // pošto mi vraća array uzmi samo prvi (i jedini ) item
    if ( DB_product?.length > 0 ) DB_product = DB_product[0];
    
    
    
    if ( !DB_product ) {
      popup_warn(`Nedostaju podaci proizvoda za ovu izlaznu robu !!!`);
      return;
    };
    

    var preview_mod = await get_cit_module('/modules/previews/cit_previews.js', `load_css`);
    
    
    var db_partner = null;
    var dobavljac_id = data.dobavljac?._id ? data.dobavljac._id : null;

    if ( dobavljac_id ) {
      db_partner = await this_module.get_dobavljac(dobavljac_id);
    };

    if ( !db_partner ) {
      popup_error(`Greška prilikom pretrage dobavljača u bazi !!!!`);
      return;
    };

    var data_for_doc = {
      
      sifra: "dfd"+cit_rand(),
      time: Date.now(),

      for_module: `sirov`,

      proj_sifra : null,
      item_sifra : null,
      variant : null,
      status: null,
      
      /*
      ---------------------------------------- OVO SU SVI PODACI ZA LOCATION OBJ ----------------------------------------
      sifra,
      palet_sifre,
          
      ms_record,
      ms_time,

      primka_record,
      primka_pro_record,

      count,

      neto_masa_paketa_kg,
      bruto_masa_paketa_kg,

      neto_masa_palete_kg,
      bruto_masa_palete_kg,

      choose_location_from,
      choose_primka,
      choose_palet_sifra,

      palet_count,

      box_count_on_palet,
      red_count_on_palet,
      box_count_in_red,

      palet_unit_count,
      paleta_x,
      paleta_y,
      paleta_z,

      box_count,
      box_unit_count,
      box_x,
      box_y,
      box_z,

      sklad,
      sector,
      level,
      shelf,
      
      ---------------------------------------- OVO SU SVI PODACI ZA LOCATION OBJ ----------------------------------------
      */
      
      primka_record: window.current_location?.primka_record || null,
      primka_pro_record: window.current_location?.primka_pro_record || null,
      rn_record: window.current_location?.choose_rn || null,
      
      jedinica: data.osnovna_jedinica?.jedinica || "",
      
      palet_sifre: data.palet_sifre,
      
      palet_unit_count: window.current_location.palet_unit_count || null,
      box_unit_count: window.current_location.box_unit_count || null,
      
      
      sklad: window.current_location.sklad || data.sklad,
      sector: window.current_location.sector || data.sector,
      level: window.current_location.level || data.level,
      alat_shelf: window.current_location.shelf || data.alat_shelf,

      sirov_id: data._id || null,
      full_record_name: (data.sirovina_sifra + "--" + window.concat_naziv_sirovine(data) + "--" + data.dobavljac?.naziv ),

      doc_valuta: ( data.doc_valuta?.valuta || "HRK" ), // default je HRK
      doc_valuta_manual: (data.doc_valuta_manual || null ), // manual tečaj
      referent: window.cit_user.full_name,
      place: `Velika Gorica`,

      doc_type: `exit_kartica`,
      doc_num: "----",

      oib: (db_partner.oib || db_partner.vat_num || ""),
      partner_name: db_partner.naziv,
      partner_adresa: null,
      partner_id: db_partner._id,
      

      /*
      partner_place: `67122 ALTRIP`,
      partner_country: `Germany`,
      */

      rok: Date.now(),
      dobav_ponuda_sifra: null,

      items: null,
      
      
      product: DB_product,

    };

    // return;

    preview_mod.generate_cit_doc( data_for_doc, data.doc_lang?.sifra || "hr" );

  };
  this_module.create_or_print_EXIT_card = create_or_print_EXIT_card;
  
  
  
  
  function update_offer_docs(files, button, arg_time) {

    var new_offer_arr = [];
    $.each(files, function(f_index, file) {

      var file_obj = {
        sifra: `offer`+cit_rand(),
        time: arg_time,
        link: `<a href="/docs/${time_path(arg_time)}/${file.filename}" target="_blank" onClick="event.stopPropagation();" >${ file.originalname }</a>`,
      };
      new_offer_arr.push( file_obj );
    });

    console.log( new_offer_arr );

    // napravi novi objekt ako current ne postoji
    if ( !window.current_offer ) window.current_offer = {};


    if ( $.isArray(window.current_offer.docs) ) {
      // ako postoji docs array onda dodaj nove filove
      window.current_offer.docs = [ ...window.current_offer.docs, ...new_offer_arr ];
    } else {
      // ako ne postoji onda stavi ove nove filove
      window.current_offer.docs = new_offer_arr;
    };    

  };
  this_module.update_offer_docs = update_offer_docs;
  
  
  function setup_records_table(data, valid, records_list) {
    
    
    var records_za_table = [];
    
    var criteria = [ '~time' ];
    multisort( records_list, criteria );

    $.each(records_list, function(index, record_obj ) {

      var {

        count,
        time,
        record_parent,

        sifra,
        doc_sifra,
        type,
        doc_link,
        record_statuses,
        record_kalk,
        record_est_time, // procjena vremena kada će doći do promjene
        storno_time,

        from_to, // samo za medju sklad

        storno_kolicina,
        pk_kolicina,
        nk_kolicina,
        rn_kolicina,
        order_kolicina,
        sklad_kolicina,


        partner_name,
        partner_adresa, 
        partner_id,


      } = record_obj;


      var grana_btn = 
`
<button id="${sifra}_record_grana" class="blue_btn small_btn record_grana" style="margin: 0 auto; box-shadow: none;">
  <i class="fas fa-code-branch"></i>
</button>
`;


      var product_links = ``;

      if ( record_statuses?.length > 0 ) {

        $.each( record_statuses, function( r_ind, rec_status ) {

          var kupac_link = ``;

          if ( rec_status.kupac_naziv && rec_status.kupac_id ) {

            kupac_link = `ZA:
            <a href="#partner/${rec_status.kupac_id}/status/null" 
                style="text-align: left;"
                target="_blank"
                onClick="event.stopPropagation();"> 
                ${rec_status.kupac_naziv} 
            </a>
            `;

          };

          // ako nema podatke partnera u glavnom objektu 
          // pogledaj u record parent !!!!
          if ( !kupac_link && record_parent && record_parent.record_statuses ) {
            // ------------------------------------------------- PRONADJI KUPAC U STATUSIMA OD PARENTA
            // ------------------------------------------------- PRONADJI KUPAC U STATUSIMA OD PARENTA
            $.each( record_parent.record_statuses, function( r_ind, rec_status ) {

              if ( rec_status.partner_name && rec_status.partner_id ) {

                kupac_link = `ZA:
                <a href="#partner/${rec_status.kupac_id}/status/null" 
                    style="text-align: left;"
                    target="_blank"
                    onClick="event.stopPropagation();"> 
                    ${rec_status.kupac_naziv} 
                </a>
                `;

              };
            });

            // ------------------------------------------------- PRONADJI KUPAC U STATUSIMA OD PARENTA
            // ------------------------------------------------- PRONADJI KUPAC U STATUSIMA OD PARENTA
          };


          product_links += 
            `
            <a href="${rec_status.link}" 
                style="text-align: left;"
                target="_blank"
                onClick="event.stopPropagation();"> 
                ${rec_status.full_record_name} 
            </a> 


            ${kupac_link}

            `;


        }); // kral loop po statusima ovog recorda

      }; // kraj ako ima record statuses


      if ( record_kalk ) {
        
        product_links += this_module.gen_record_kalk_partner_link(record_obj, record_kalk);

      };

     

      var choose_record = cit_comp( sifra + `_` + data.rand_id + `_choose_record`, valid.choose_record, false );

      // za sada ne PRIKAZUJEM OVO
      var record_est_time_input = cit_comp( sifra + `_` + data.rand_id + `_record_est_time`, valid.record_est_time, record_est_time );

      if ( storno_time ) doc_link = `<span class="cit_storno_pill">PONIŠTENO</span>` + (doc_link || "");

      records_za_table.push({

        grana_btn,
        choose_record,
        time,
        record_parent,
        sifra,
        doc_sifra,
        type,
        doc_link,
        record_statuses,
        record_est_time,
        record_est_time_input,
        product_links,
        from_to,

        pk_kolicina,
        nk_kolicina,
        rn_kolicina,
        order_kolicina,
        sklad_kolicina,


      });

    }); // kraj loopa svih records


    return records_za_table;
    
    
  };
  this_module.setup_records_table = setup_records_table;
  
  function make_records_list(data) {


    var valid = this_module.valid;

    $(`#sirov_records`).html(``);

    var records_za_table = [];
    
    var criteria = [ 'time' ];
    multisort( data.records, criteria );
    
    // OVDJE AUTOMATSKI DODJELJUJEM PARENTE NA RECORDIMA KOJI SU NASTALI PUTEM KALK REZERVACIJE !!!!
    // OVDJE AUTOMATSKI DODJELJUJEM PARENTE NA RECORDIMA KOJI SU NASTALI PUTEM KALK REZERVACIJE !!!!
    // OVDJE AUTOMATSKI DODJELJUJEM PARENTE NA RECORDIMA KOJI SU NASTALI PUTEM KALK REZERVACIJE !!!!
    
    $.each(data.records, function(r_ind, record) {
      
      var curr_record = record;
      
      if ( curr_record.type == `order_dobav` ) {
        $.each(data.records, function(offer_ind, offer_dobav) {
          if ( offer_dobav.type == `offer_dobav` && offer_dobav.doc_sifra == curr_record.doc_parent?.doc_sifra )  {
            data.records[r_ind].record_parent = cit_deep(offer_dobav);
            data.records[offer_ind].storno_time = curr_record.time;
          };
        });
      };
      
      
      if ( curr_record.type == `in_record` ) {
        $.each(data.records, function(order_ind, order_dobav) {
          if ( order_dobav.type == `order_dobav` && order_dobav.doc_sifra == curr_record.doc_parent?.doc_sifra )  {
            data.records[r_ind].record_parent = cit_deep(order_dobav);
            data.records[order_ind].storno_time = curr_record.time;
          };
        });
      };
      
      
      
      if ( curr_record.type == `order_reserv`) {
        $.each(data.records, function(offer_ind, offer_record) {
          if ( offer_record.type == `offer_reserv` && offer_record.record_kalk.kalk_sifra == curr_record.record_kalk.kalk_sifra)  {
            data.records[r_ind].record_parent = cit_deep(offer_record);
            data.records[offer_ind].storno_time = curr_record.time;
            
          };
        });
      };
      
      if ( curr_record.type == `radni_nalog` ) {
        $.each(data.records, function(order_ind, order_record) {
          if ( order_record.type == `order_reserv` && order_record.record_kalk.kalk_sifra == curr_record.record_kalk.kalk_sifra )  {
            // record parent od radnog naloga je zapravo ----->  record parent od ordera  a  to je zapravo  -----> offer reserv record 
            data.records[r_ind].record_parent = cit_deep(order_record.record_parent); 
            data.records[order_ind].storno_time = curr_record.time;
          };
        });
      };
      
      
    });
    
    // OVDJE AUTOMATSKI DODJELJUJEM PARENTE NA RECORDIMA KOJI SU NASTALI PUTEM KALK REZERVACIJE !!!!
    // OVDJE AUTOMATSKI DODJELJUJEM PARENTE NA RECORDIMA KOJI SU NASTALI PUTEM KALK REZERVACIJE !!!!
    // OVDJE AUTOMATSKI DODJELJUJEM PARENTE NA RECORDIMA KOJI SU NASTALI PUTEM KALK REZERVACIJE !!!!
    
    
    var records_list = data.records;
    
    
    // ako je AKTIVAN FILTER tj kliknut gumb za filter
    // ako je AKTIVAN FILTER tj kliknut gumb za filter
    // ako je AKTIVAN FILTER tj kliknut gumb za filter    
    if (window.sirov_records_are_filtered ) records_list = data.filtered_records;

    if ( records_list && records_list.length > 0 ) {
      
      
      records_za_table = this_module.setup_records_table(data, valid, records_list);
      
      
      var records_props = {
        desc: 'samo za kreiranje tablice svih recorda u sirovini',
        local: true,

        list: records_za_table,

        show_cols: [
          `grana_btn`,
          `choose_record`,
          `time`,
          `doc_link`,
          `record_est_time`, // `record_est_time_input`
          `product_links`,
          
          `pk_kolicina`,
          `nk_kolicina`,
          `rn_kolicina`,
          
          `order_kolicina`,
          `sklad_kolicina`,
          
          'button_delete',
        ],
        custom_headers: [
          `Grana`,
          `Označi`,
          `Vrijeme`,
          `Tip/Dokument`,
          `Est. time`,
          `Povezano`,
          
          `Reserv PK`,
          `Rezerv NK`,
          `Rezerv RN`,
          `ND`,
          `SKLAD`,
          
          'OBRIŠI',
        ],
        col_widths: [
          1, // `grana_btn`,
          1, // `choose_record`,
          2, // `time`,
          2, // `type_name`,
          2.3, // `record_est_time_input`,
          4, // `product_links`,
          
          1, // `Reserv PK`,
          1, // `Rezerv NK`,
          1, // `Rezerv RN`,
          1, // `Naručeno`,
          1, // `Skladište`,
         
          1, // 'button_delete',

        ],
        format_cols: { 
          product_links: "left",
          time: "date_time",
          
          record_est_time: "date_time",
          
          type_name: "center",
          
          pk_kolicina: 3,
          nk_kolicina: 3,
          rn_kolicina: 3,
          
          order_kolicina: 3,
          sklad_kolicina: 3,
        },

        parent: "#sirov_records",
        return: {},
        show_on_click: false,

        cit_run: function(state) { console.log(state); },
        

        button_delete: async function(e) {

          e.stopPropagation();
          e.preventDefault();


          console.log(`kliknuo na STORNO OD RECORDA`);


          var this_button = this;

          function storno_yes() {

            var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
            var data = this_module.cit_data[this_comp_id];
            var rand_id =  this_module.cit_data[this_comp_id].rand_id;

            var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
            var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);

            var record = find_item( data.records, sifra , `sifra` );
            
            
            if ( record.type == `offer_dobav` ) {
              popup_warn(`Nema smisla stornirati ZAHTJEV ZA PONUDU OD DOBAVLJAČA :-) !!!`);
              return;
            };
            

            if ( get_super_editors().indexOf(window.cit_user.user_number) == -1 ) {
              popup_warn(`Nemate ovlasti za brisanje ovog podatka !!!!`);
              return;
            };
            
           
            var index = find_index( data.records, sifra , `sifra` );

            data.records[index].storno_time = Date.new();
            
              
            var all_kolicine = [ `pk_kolicina`, `nk_kolicina`, `rn_kolicina`, `order_kolicina` ]; 
            
            $.each( all_kolicine, function(k_ind, kol) {
              if ( kol ) data.records[index].storno_kolicina = -1 * kol; // negativna vrijednost 
            });
            
            
            /*
            var criteria = [ '~time' ];
            var records_copy = cit_deep(data.records);
            multisort( records_copy, criteria );
            var last_record = records_copy[0];
            */

            this_module.make_records_list(data);

            this_module.save_sirov();

          }; // kraj delete yes funkcija

          var popup_text = `Jeste li sigurni da želite napraviti STORNO ovog Zapisa?`;

          function storno_no() {
            show_popup_modal(false, popup_text, null );
          };

          var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
          var show_popup_modal = pop_mod.show_popup_modal;
          show_popup_modal(`warning`, popup_text, null, 'yes/no', storno_yes, storno_no, null);


        },

      }; // kraj records props


      if (records_za_table.length > 0 ) {
        
        create_cit_result_list(records_za_table, records_props );

        
        // OVO JE ZA _choose_record 
        // OVO JE ZA _choose_record 
        // OVO JE ZA _choose_record 
        
        wait_for( `$("#sirov_records .cit_check_box").length > 0`, function() {

          $('#sirov_records .cit_check_box').each( function() {

            $(this).data(`cit_run`, function(state, check_elem) { 

              var this_comp_id = $(check_elem).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;

              console.log(`--------check box ---------`);
              console.log(data);

              // ugasi sve record check boxes 
              // osim ove koje sam kliknuo !!!!
              $('#sirov_records .cit_check_box').each( function() {
                if ( this.id !== check_elem.id ) $(this).removeClass('on').addClass('off');
              });

              // id od check box je na primjer 
              // record354354334_sir3243432434_choose_record

              var choosed_sifra = check_elem.id.split(`_`)[0];

              if ( state !== false )  {
                data.current_record_parent = find_item( data.records, choosed_sifra, `sifra`);
              } else {
                data.current_record_parent = null;
              };


              console.log( data.current_record_parent );

            }); // kraj cit run func in data

          }); // kraj each check box

          
          
          $('#sirov_records .record_est_time_input .cit_input').each( function() {
            
            
            $(this).data(`cit_run`, async function( state, this_input, on_click ) {
              
              var this_comp_id = this_input.closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;

              var record_sifra = this_input[0].id.split(`_`)[0];
              var index = find_index( data.records, record_sifra , `sifra` );

              var curr_time = get_date_units( new Date(data.records[index].record_est_time) );
              var new_time = get_date_units( new Date(state) );
              
              if (
                
                (curr_time.yyyy !== new_time.yyyy
                ||
                curr_time.mnt !== new_time.mnt
                ||
                curr_time.day !== new_time.day // ovo je sve provjera da budem siguran da se vrijeme promijenilo !!!!!!
                ||
                curr_time.hh !== new_time.hh 
                ||
                curr_time.mm !== new_time.mm )
                
                && 
                
                !on_click /* samo ako nije prvi klik na polje datuma  */ ) {
                
                function change_est_time_yes() {
                  
                  if ( state == null || this_input.val() == ``) {
                    popup_warn(`Datum ne smije biti prazan!`);
                    // vrati nazad postojeći datum !!!
                    this_input.val( cit_dt( data.records[index].record_est_time).date_time );
                    return;
                  };
                  
                  data.records[index].record_est_time = state;
                  console.log(`record_sifra ${record_sifra} ----> record_est_time: `, new Date(state));
                  
                  if ( data.records[index].type == `order_dobav` ) {
                    this_module.send_status_for_est_time_change( data.records[index] );
                  };
                  
                };


                function change_est_time_no() {
                  this_input.val( cit_dt( data.records[index].record_est_time).date_time )
                  show_popup_modal(false, `Jeste li sigurni da želite promjeniti datum?`, null );
                };

                var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
                var show_popup_modal = pop_mod.show_popup_modal;
                show_popup_modal(`warning`, `Jeste li sigurni da želite promjeniti datum?`, null, 'yes/no', change_est_time_yes, change_est_time_no, null);
              
              };
              
            });
            
            
            // refresh_simple_cal( $(this) );
            
            
          }); // kraj loopa po svim inputima za est time u 
          
          
          $('#sirov_records .record_grana').each( function() {

            $(this).off(`click`);
            $(this).on(`click`, function(e) { 
              
              var this_comp_id = $(this).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;
              
              // ako je već fiiltrirana lista onda reseriraj !!!
              // ako je već fiiltrirana lista onda reseriraj !!!
              
              if ( window.sirov_records_are_filtered ) {
                
                window.sirov_records_are_filtered = false;
                this_module.make_records_list(data);
                
                setTimeout( function() {
                  $('#sirov_records .record_grana').each( function() {
                    $(this).css(`background`, `#3f6ad8`);
                  });
                }, 100 );

                return;
                
              };
              
              window.sirov_records_are_filtered = true;
              
              console.log(`-------- record grana  ---------`);
              console.log(data);
              
              var record_sifra = this.id.split(`_`)[0];
              var index = find_index( data.records, record_sifra , `sifra` );
              
              var current_record_sifra = data.records[index].sifra || null;
              var curr_record_parent_sifra = data.records[index].record_parent?.sifra || null;

              data.filtered_records = [];
              
              $.each( data.records, function(r_ind, record) {
                
                if ( curr_record_parent_sifra ) {
                  
                  // ako ima parent objekt ONDA JE CHILD
                  // ako je child onda traži šifru za parenta i za svu djecu traži record parent sifra
                  if ( 
                    
                    record.sifra == curr_record_parent_sifra 
                      ||
                    (record.record_parent && record.record_parent?.sifra == curr_record_parent_sifra)
                    
                  ) data.filtered_records.push(record);
                  
                } 
                else {
                  // ako NEMA parent objekt onda je to PARENT
                  // ako je PARENT onda traži šifru i za svu djecu traži record parent sifra
                  if ( 
                    record.sifra == current_record_sifra
                      ||
                    (record.record_parent && record.record_parent?.sifra == current_record_sifra)
                  ) data.filtered_records.push(record);
                  
                };
                
              });
              
              if ( data.filtered_records.length == 0 ) {
                window.sirov_records_are_filtered = false;
                return;
              };
              
              this_module.make_records_list(data);
              
              setTimeout( function() {
                $('#sirov_records .record_grana').each( function() {
                  $(this).css(`background`, `#bf0060`);
                });
              }, 100 );
              
              
            }); // kraj cit run func in data

          }); // kraj each grana button

          
          
          
          
        },5*1000 );

      }; // kraj ifa ako records table array nije prazan


    }; // kraj ako postoji records length > 0


  };
  this_module.make_records_list = make_records_list;
  
  
  
  function loc_filter(locations_list, parent_box_selector) {
    
    var valid = this_module.valid;
    
    var this_comp_id = $(`${parent_box_selector}`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
      
    var locations_za_table = [];

    var criteria = [ '!ms_time' ];
    multisort( locations_list, criteria );

    $.each(locations_list, function(index, local_obj ) {


      /*
      STARI PODACI OVAKO IZGLEDAJU
      ---------------------------------------------
        sifra,
        primka_time,
        ms_time,

        primka_record,
        ms_record,

        count,

        sklad,
        sector,
        level,
        shelf,
      ---------------------------------------------
      */


      var {

        user,

        time,
        sifra,
        palet_sifre,

        ms_time,

        palet_count,
        box_count,
        palet_unit_count,
        box_unit_count,

        box_count_on_palet,
        red_count_on_palet,
        box_count_in_red,


        primka_record, // data.selected_primka
        primka_pro_record, // data.selected_pro_primka
        rn_record, // data.selected_rn


        count,

        neto_masa_paketa_kg,
        bruto_masa_paketa_kg,
        neto_masa_palete_kg,
        bruto_masa_palete_kg,

        location_from,



        paleta_x,
        paleta_y,
        paleta_z,

        box_x,
        box_y,
        box_z,


        sklad,
        sector,
        level,
        shelf,





      } = local_obj;


      var ista_primka_btn = 
`
<button id="${sifra}_ista_primka" class="blue_btn small_btn ista_primka" style="margin: 0 auto; box-shadow: none;">
  <i class="fas fa-code-branch"></i>
</button>
`;


      var masa_html = `
<div style="display: flex;"><div style="width: 60%; text-align: right;" >NETO PAK:</div><div style="width: 40%; text-align: right;">${neto_masa_paketa_kg || "" } kg</div></div>
<div style="display: flex;"><div style="width: 60%; text-align: right;" >BRUTO PAK:</div><div style="width: 40%; text-align: right;">${bruto_masa_paketa_kg || "" } kg</div></div>
<div style="display: flex;"><div style="width: 60%; text-align: right;" >NETO PAL:</div><div style="width: 40%; text-align: right;">${neto_masa_palete_kg || "" } kg</div></div>
<div style="display: flex;"><div style="width: 60%; text-align: right;" >BRUTO PAL:</div><div style="width: 40%; text-align: right;">${bruto_masa_palete_kg || "" } kg</div></div>

`;

      var location_from_html = `
<div style="display: flex;">
<div style="width: 100%; text-align: left;">
  ${ (location_from?.sklad_naziv || '') + `&nbsp;&nbsp;&#8212;&nbsp;&nbsp;` + (location_from?.sector || '') + `&nbsp;&nbsp;&#8212;&nbsp;&nbsp;` + (location_from?.level || '') + `&nbsp;&nbsp;&#8212;&nbsp;&nbsp;` + (location_from?.shelf || '') }
</div>
</div>

`;        


    var boxes_on_palet_html = `      

<div style="display: flex;"><div style="width: 60%; text-align: right;" >PAK NA PAL:</div><div style="width: 40%; text-align: right;">${box_count_on_palet || "" }</div></div>
<div style="display: flex;"><div style="width: 60%; text-align: right;" >BROJ REDOVA:</div><div style="width: 40%; text-align: right;">${red_count_on_palet || "" }</div></div>
<div style="display: flex;"><div style="width: 60%; text-align: right;" >PAK U REDU:</div><div style="width: 40%; text-align: right;">${box_count_in_red || "" }</div></div>
`;


    var dim_html = `      

<div style="display: flex;">
<div style="width: 40%; text-align: right;" >PAK D/Š/V:</div>
<div style="width: 60%; text-align: right;">${ (box_x || "") + ` / ` + (box_y || "") + ` / ` + (box_z || "") }</div>
</div>
<div style="display: flex;">
<div style="width: 40%; text-align: right;" >PAL D/Š/V:</div>
<div style="width: 60%; text-align: right;">${ (paleta_x || "") + ` / ` + (paleta_y || "") + ` / ` + (paleta_z || "") }</div>
</div>
`;      

      
    var loc_html = `
<div style="display: flex;">
<div style="width: 100%; text-align: left; font-size: 13px !important;">
  ${ (sklad?.short || '') + `&nbsp;&nbsp;&#8212;&nbsp;&nbsp;` + (sector?.sifra || '') + `&nbsp;&nbsp;&#8212;&nbsp;&nbsp;` + (level?.sifra || '') + `&nbsp;&nbsp;&#8212;&nbsp;&nbsp;` + (shelf?.naziv || '') }
</div>
</div>
`;


      var choose_ms_html = cit_comp( sifra + `_` + data.rand_id + `_choose_ms`, valid.choose_ms, false, null, "margin: 0 auto;", "choose_ms" );


      var palet_sifre_pills = ``;

      if ( palet_sifre?.length > 0 ) {
        $.each( palet_sifre, function(p_ind, palet_sifra) {
          palet_sifre_pills += `<span class="palet_sifra_pill">${palet_sifra}</span>`;
        });
      };

      
      var doc_link = primka_record?.link || "";
      if ( primka_pro_record ) doc_link = primka_pro_record.link || "";
      if ( rn_record ) doc_link = rn_record.link || "";

      locations_za_table.push({

        choose_ms_html,

        user,
        user_full_name: user.full_name,
        doc_sifra_link: doc_link,

        time,
        sifra,
        palet_sifre,
        palet_sifre_html: palet_sifre_pills,

        ms_time,

        palet_count,
        box_count,
        palet_unit_count,
        box_unit_count,

        box_count_on_palet,
        red_count_on_palet,
        box_count_in_red,

        primka_record, // data.selected_primka
        primka_pro_record, // data.selected_pro_primka
        rn_record,
        
        count,

        neto_masa_paketa_kg,
        bruto_masa_paketa_kg,
        neto_masa_palete_kg,
        bruto_masa_palete_kg,

        location_from,



        paleta_x,
        paleta_y,
        paleta_z,

        box_x,
        box_y,
        box_z,


        sklad,
        sector,
        level,
        shelf,

        /* -------- GENERIRANI HTML ELEMENTI -------- */

        ista_primka_btn,
        masa_html,
        location_from_html,
        boxes_on_palet_html,
        dim_html,
        loc_html,


      });

    }); // kraj loopa svih lokacija

    return locations_za_table;
    
  };
  this_module.loc_filter = loc_filter;
  
  
  
  function register_location_list_events(parent_box_selector) {

    wait_for( `$("${parent_box_selector} .ista_primka").length > 0`, function() {

      $(`${parent_box_selector} .ista_primka`).each( function() {

        $(this).off(`click`);
        $(this).on(`click`, function(e) { 

          var this_comp_id = $(this).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          // ako je već fiiltrirana lista onda napravi RESET !!!
          // ako je već fiiltrirana lista onda napravi RESET !!!

          if ( window.sirov_locations_are_filtered ) {

            window.sirov_locations_are_filtered = false;
            this_module.make_locations_list(data);

            setTimeout( function() {
              $(`${parent_box_selector} .ista_primka`).each( function() {
                $(this).css(`background`, `#3f6ad8`);
              });
            }, 100 );


            return;


          };



          window.sirov_locations_are_filtered = true;

          console.log(`-------- sirov locations  ---------`);
          console.log(data);

          var local_sifra = this.id.split(`_`)[0];
          var index = find_index( data.locations, local_sifra , `sifra` );

          var curr_refer_doc = data.locations[index].refer_doc || null;
          

          // resetiraj filtered listu
          data.filtered_locations = [];

          $.each( data.locations, function(r_ind, local) {
            
            if ( local.refer_doc == refer_sifra ) {
              
              data.filtered_locations.push(local);
              
            } else if ( local.primka_pro_record?.sifra == curr_primka_sifra ) {
              
              data.filtered_locations.push(local);
              
            };
          });

          // ako nema ništa u filtriranoj listi onda resetiraj varijablu
          if ( data.filtered_locations.length == 0 ) {
            window.sirov_locations_are_filtered = false;
            return;
          };


          this_module.make_locations_list(data);

          // ofarbaj sve gumbe u crveno tak da se vidi da je lista lokacija u filtriranom stanju
          setTimeout( function() {
            $(`${parent_box_selector} .ista_primka`).each( function() {
              $(this).css(`background`, `#bf0060`);
            });
          }, 100 );


        }); // kraj cit run func in data

      }); // kraj each grana button

      $(`${parent_box_selector} .choose_ms`).each( function() {

        $(this).data(`cit_run`, async function(state, check_elem) { 

          var this_comp_id = $(check_elem).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          console.log(`--------check box ---------`);
          console.log(data);

          // ugasi sve docs check boxes 
          // osim ove koje sam kliknuo !!!!
          $(`${parent_box_selector} .choose_ms`).each( function() {
            if ( this.id !== check_elem.id ) $(this).removeClass('on').addClass('off');
          });

          // id od check box je na primjer 
          // dfd354354334_sir3243432434_choose_doc

          var choosed_sifra = check_elem.id.split(`_`)[0];

          if ( state !== false )  {

            window.current_location = find_item( data.locations, choosed_sifra, `sifra`);

          } 
          else {
            window.current_location = null;
          };

          console.log( window.current_location );

        }); // kraj cit run za choose_ms

      }); // kraj each choose_ms check box

    }, 50*1000 );

  };
  this_module.register_location_list_events = register_location_list_events;

  this_module.locations_show_cols = [

    "ista_primka_btn",
    "choose_ms_html",

    "doc_sifra_link",

    "user_full_name",
    "time",

    "palet_sifre_html",
    "ms_time",


    "palet_count",
    "palet_unit_count",
    "box_count",
    "box_unit_count",
    "boxes_on_palet_html",


    "masa_html",
    "dim_html",


    "location_from_html",
    "loc_html",
    "count",

    'button_edit', 
    'button_delete',

  ];
  this_module.locations_custom_headers = [

    "ISTI DOC.",
    "SEL.",
    "DOC.",

    "USER",
    "TIME",
    "SKL. ŠIFRA",
    "TIME MS",

    "BROJ PAL",
    "KOL. NA PAL",
    "BROJ PAK",
    "KOL. U PAK",
    "PAK. NA PAL",

    "MASA",
    "DIM",

    "SA LOK.",
    "NA LOK.",

    "UK. KOL.",

    "EDT",

    "DEL",

  ];
  this_module.locations_col_widths = [

    1, // "ISTA PR.",
    0.7, // CHOOSE MS
    1, // "PRIMKA",


    1.5, // "USER",
    1.5, // "TIME",
    2, // "SKL. ŠIFRA",
    1.5, // "TIME MS",

    1, // "BROJ PAL",
    1, // "KOL. NA PAL",
    1, // "BROJ PAK",
    1, // "KOL. U PAK",

    3, // "PAK. NA PAL",

    3, // "MASA",
    3, // "DIM",

    3, // "SA LOK.",
    3, // "NA LOK.",

    2, // "UK. KOL.",

    0.7, // "EDIT",
    0.7, // "DELETE",
  ];
  this_module.locations_format_cols =  { 

    "ista_primka_btn": "center",
    "user_full_name": "center",
    "time": "date_time",

    "doc_sifra_link": "center",
    "palet_sifre_html": "center",
    "ms_time": "date_time",


    "palet_count": "center",
    "palet_unit_count": "center",
    "box_count": "center",
    "box_unit_count": "center",


    "count": 2,

  };
  
  
  
  function location_button_edit_func(e) { 

    e.stopPropagation(); 
    e.preventDefault();

    var valid = this_module.valid;

    console.log("kliknuo edit za lokaciju sirovine");


    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    var parent_row_id = $(this).closest('.search_result_row')[0].id;
    var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
    var index = find_index(data.locations, sifra , `sifra` );


    window.current_location = cit_deep( data.locations[index] );


    $('#save_sirov_location_btn').css(`display`, `none`);

    $('#update_sirov_location_btn').css(`display`, `flex`);
    $('#save_variant_sirov_location_btn').css(`display`, `flex`);


    if ( window.current_location.ms_time ) {
      $('#'+rand_id+`_loc_est_date`).val( cit_dt(window.current_location.ms_time).date || "");
      data.ms_time = window.current_location.ms_time;
    } else {
      $('#'+rand_id+`_loc_est_date`).val(``);
      data.ms_time = null;
    };


    data.loc_komentar = window.current_location.komentar;
    $('#'+rand_id+`_loc_komentar`).val( data.loc_komentar || "");


    data.palet_sifre = window.current_location.palet_sifre;
    // $('#'+rand_id+`_palet_sifra`).val( data.palet_sifre || ""); // ne popunjavam ovo polje jer ima multiple sifre

    data.selected_primka = window.current_location.primka_record;
    $('#'+rand_id+`_choose_primka`).val( data.selected_primka?.refer_doc || "");
    
    data.selected_pro_primka = window.current_location.primka_pro_record;
    $('#'+rand_id+`_choose_pro_primka`).val( data.selected_pro_primka?.refer_doc || "");
    
    data.selected_rn = window.current_location.rn_record;
    $('#'+rand_id+`_choose_rn`).val( data.selected_rn?.refer_doc || "");
    

    data.neto_masa_paketa_kg = window.current_location.neto_masa_paketa_kg;
    $('#'+rand_id+`_neto_masa_paketa_kg`).val( cit_format(data.neto_masa_paketa_kg, valid.neto_masa_paketa_kg.decimals) || "");

    data.bruto_masa_paketa_kg = window.current_location.bruto_masa_paketa_kg;
    $('#'+rand_id+`_bruto_masa_paketa_kg`).val( cit_format(data.bruto_masa_paketa_kg, valid.neto_masa_paketa_kg.decimals)  || "");

    data.neto_masa_palete_kg = window.current_location.neto_masa_palete_kg;
    $('#'+rand_id+`_neto_masa_palete_kg`).val( cit_format(data.neto_masa_palete_kg, valid.neto_masa_paketa_kg.decimals)  || "");

    data.bruto_masa_palete_kg = window.current_location.bruto_masa_palete_kg;
    $('#'+rand_id+`_bruto_masa_palete_kg`).val( cit_format(data.bruto_masa_palete_kg, valid.neto_masa_paketa_kg.decimals)  || "");


    data.location_from = window.current_location.location_from;
    var location_naziv = ``;

    if ( data.location_from ) {

      location_naziv = 
        (data.location_from.sklad  || ``) + `-` + 
        (data.location_from.sector || ``) + `-` +
        (data.location_from.level  || ``) + `-` +
        (data.location_from.shelf  || ``);
    };

    $('#'+rand_id+`_choose_location_from`).val( location_naziv );


    data.palet_count = window.current_location.palet_count;
    $('#'+rand_id+`_palet_count`).val( cit_format(data.palet_count, valid.palet_count.decimals)  || "" );

    data.box_count_on_palet = window.current_location.box_count_on_palet;
    $('#'+rand_id+`_box_count_on_palet`).val( cit_format(data.box_count_on_palet, valid.box_count_on_palet.decimals) || "" );

    data.red_count_on_palet = window.current_location.red_count_on_palet;
    $('#'+rand_id+`_red_count_on_palet`).val( cit_format(data.red_count_on_palet, valid.red_count_on_palet.decimals) || "" );

    data.box_count_in_red = window.current_location.box_count_in_red;
    $('#'+rand_id+`_box_count_in_red`).val( cit_format(data.box_count_in_red, valid.box_count_in_red.decimals) || "" );

    data.palet_unit_count = window.current_location.palet_unit_count;
    $('#'+rand_id+`_palet_unit_count`).val( cit_format(data.palet_unit_count, valid.palet_unit_count.decimals) || "" );

    data.paleta_x = window.current_location.paleta_x;
    $('#'+rand_id+`_paleta_x`).val( cit_format(data.paleta_x, valid.paleta_x.decimals) || "" );

    data.paleta_y = window.current_location.paleta_y;
    $('#'+rand_id+`_paleta_y`).val( cit_format(data.paleta_y, valid.paleta_y.decimals) || "" );

    data.paleta_z = window.current_location.paleta_z;
    $('#'+rand_id+`_paleta_z`).val( cit_format(data.paleta_z, valid.paleta_z.decimals) || "" );


    data.box_count = window.current_location.box_count;
    $('#'+rand_id+`_box_count`).val( cit_format(data.box_count, valid.box_count.decimals) || "" );

    data.box_unit_count = window.current_location.box_unit_count;
    $('#'+rand_id+`_box_unit_count`).val( cit_format(data.box_unit_count, valid.box_unit_count.decimals) || "" );

    data.box_x = window.current_location.box_x;
    $('#'+rand_id+`_box_x`).val( cit_format(data.box_x, valid.box_x.decimals) || "" );

    data.box_y = window.current_location.box_y;
    $('#'+rand_id+`_box_y`).val( cit_format(data.box_y, valid.box_y.decimals) || "" );

    data.box_z = window.current_location.box_z;
    $('#'+rand_id+`_box_z`).val( cit_format(data.box_z, valid.box_z.decimals) || "" );


    data.sklad = window.current_location.sklad;
    $('#'+rand_id+`_sklad`).val( data.sklad?.short || "" );

    data.sector = window.current_location.sector;
    $('#'+rand_id+`_sector`).val( data.sector?.sifra || "" );

    data.level = window.current_location.level;
    $('#'+rand_id+`_level`).val( data.level?.sifra || "" );

    data.alat_shelf = window.current_location.shelf;
    $('#'+rand_id+`_alat_shelf`).val( data.alat_shelf?.naziv || "" );



  };
  this_module.location_button_edit_func = location_button_edit_func;
  
  
  async function location_button_delete_func(e) {

    e.stopPropagation();
    e.preventDefault();
    
    var this_button = this;


    if ( get_super_editors().indexOf(window.cit_user.user_number) == -1 ) {
      popup_warn(`Nemate ovlasti za brisanje lokacije !!!!`);
      return;
    };

    console.log("kliknuo DELETE za lokaciju sirovine");

    function delete_yes() {

      var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
      var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);

      // postavi ovu sifru koju želim deletati u array za delete tako da na serveru to obrišem !!!!
      if ( !data.delete_locations ) data.delete_locations = [];
      data.delete_locations.push({sifra: sifra });

      // obriši ovu lokaciju u listi svih lokacija !!!
      data.locations = delete_item(data.locations, sifra , `sifra` );
      $(`#`+parent_row_id).remove();
      

    };


    function delete_no() {
      show_popup_modal(false, `Jeste li sigurni da želite obrisati ovu lokaciju?`, null );
    };

    var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
    var show_popup_modal = pop_mod.show_popup_modal;
    show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovu lokaciju?`, null, 'yes/no', delete_yes, delete_no, null);


    
  };
  this_module.location_button_delete_func = location_button_delete_func;
  
  function make_locations_list(data, for_find ) {
    
    var valid = this_module.valid;
    
    $(`#sirov_locations_box`).html(``);


    var locations_list = data.locations;

    if ( window.sirov_locations_are_filtered ) locations_list = data.filtered_locations;
    
    if ( !locations_list || locations_list.length == 0 ) return;
    
    
    var locations_za_table = this_module.loc_filter(locations_list, `#sirov_locations_box`);

    var locations_props = {
      desc: 'samo za kreiranje tablice svih locations za sirovinu',

      local: true,

      list: locations_za_table,

      show_cols: this_module.locations_show_cols,
      custom_headers: this_module.locations_custom_headers,
      col_widths: this_module.locations_col_widths,

      format_cols: this_module.locations_format_cols,


      parent: "#sirov_locations_box",
      return: {},
      show_on_click: false,

      button_edit: this_module.location_button_edit_func,

      button_delete: this_module.location_button_delete_func,

      cit_run: function(state) { console.log(state); },

    }; // kraj records props

    if ( locations_za_table.length > 0 ) {

      create_cit_result_list(locations_za_table, locations_props );
      
      this_module.register_location_list_events( locations_props.parent );

    }; // kraj ifa ako locations_za_table array nije prazan

  };
  this_module.make_locations_list = make_locations_list;

  
  
  
  
  function make_doc_spec_for_record(doc_data, arg_pdf) {

    // može biti bilo koji gumb unutar html-a sirovine
    var this_comp_id = $(`#save_specs`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    var doc_type_human_name = "";
  
    if ( doc_data.doc_type == "offer_dobav" ) doc_type_human_name = " ZAHTJEV ZA PONUDU ";
    if ( doc_data.doc_type == "order_dobav" ) doc_type_human_name = " NARUDŽBA DOBAVLJAČU ";
    if ( doc_data.doc_type == "in_record" ) doc_type_human_name = " PRIMKA ";
    if ( doc_data.doc_type == "in_pro_record" ) doc_type_human_name = " PRIMKA PRO. ";
    
    
    var new_sirov_spec = {
      
      "docs": [
          {
            "sifra": "doc"+cit_rand(),
            "time": arg_pdf.time,
            "link": `<a href="/docs/${time_path(arg_pdf.time)}/${arg_pdf.ime_filea}" target="_blank" onClick="event.stopPropagation();" >${ arg_pdf.ime_filea }</a>`,
          }
        ],
      "sifra": "specs"+cit_rand(),
      "komentar": ( doc_data.doc_sifra ? doc_data.doc_sifra : "" ) + doc_type_human_name + " (Automatski generiran dokument)",
      "time": Date.now(),
    };
    
    data.specs = upsert_item(data.specs, new_sirov_spec, `sifra`);
    
    data.specs_flat = JSON.stringify(data.specs);
    
    // update choose spec list
    $(`#`+rand_id+`_choose_spec`).data(`cit_props`).list = data.specs;
    
    this_module.make_specs_list(data);
    
    window.current_specs = null;
    
    
    //save sirov
    // $("#save_sirov_btn").trigger(`click`);
    
    
  };
  this_module.make_doc_spec_for_record = make_doc_spec_for_record;

  
  async function sirov_status_and_record( data_for_doc, arg_pdf, arg_all_sirovine, arg_index ) {
    
    
    
    if ( !window.admin_conf || !window.admin_conf.status_conf ) {
      popup_warn(`Nedostaju ADMIN CONF podaci o tipovima !!!`);
      return;
    };
    
    
    var arg_sirov = arg_all_sirovine ? arg_all_sirovine[arg_index] : null;
    
    
    return new Promise( async function(resolve, reject) {

      var record_kolicina = 0;
      
      
      // OVO JE ZAPRAVO MAX COUNT AKO IMAM VIŠE KOLIČINA !!!
      // OVO JE ZAPRAVO MAX COUNT AKO IMAM VIŠE KOLIČINA !!!
      /*
      
      VIŠE NE BROJIM KOLIČINU NA OSNOVU ITEMS ARRAYA  ------> items array mi isklučivo služi za prikazivanje na dokumentu !!!!!
      VIŠE NE BROJIM KOLIČINU NA OSNOVU ITEMS ARRAYA  ------> items array mi isklučivo služi za prikazivanje na dokumentu !!!!!
      VIŠE NE BROJIM KOLIČINU NA OSNOVU ITEMS ARRAYA  ------> items array mi isklučivo služi za prikazivanje na dokumentu !!!!!
      
      
      if ( data_for_doc.items?.length > 0 ) {
        $.each( data_for_doc.items, function( i_ind, item ) { 
          if ( item.count && item.count > record_kolicina ) {
            record_kolicina = item.count;
          };
        });
      };
      */
      
      
      /*
      --------------------NE KORISTIM NE KORISTIM ZA SADA------------------------------
      --------------------------------------------------
      if ( arg_sirov ) {
        record_kolicina = 0;
        if ( data_for_doc.items?.length > 0 ) {
          $.each( data_for_doc.items, function( i_ind, item ) { 
            if ( 
              item.count 
              &&
              item.sirov_id == arg_sirov._id
              &&
              item.count > record_kolicina 
            ) {
              record_kolicina = item.count;
            }
          });
        };
      };
      --------------------------------------------------
      --------------------------------------------------
      */
      
      // ako je ovo partner page onda ima argument sirov
      if ( arg_sirov ) {
        
        // resetiram količinu
        record_kolicina = 0;
        
        if ( arg_sirov.doc_kolicine?.length > 0 ) {
          
          $.each( arg_sirov.doc_kolicine, function( k_ind, kol ) {
            if ( kol.count > record_kolicina) record_kolicina = kol.count;
          });
        };
        
      };
      
      var this_button = null;
      var this_comp_id = null;
      var data = null;
      var rand_id = null;


      // partner_module: this_module,
      // partner_data: data,
      // ako je zahtjev za ponudu napravljen u partneru / dobavljaču
      if ( data_for_doc.partner_data ) {

        // PARTNER PAGE
        // uzeo sam bilo koji gumb unutar partner modula ----> NIJE BITNO !!!
        this_button = $(`#add_sirov_to_doc_btn`)[0];

        this_comp_id = $(this_button).closest('.cit_comp')[0].id;
        data = data_for_doc.partner_module.cit_data[this_comp_id];
        rand_id =  data_for_doc.partner_module.cit_data[this_comp_id].rand_id;

      } 
      else {

        // SIROV PAGE -----> NE KORISTIM VIŠE 
        // uzeo sam bilo koji gumb unutar sirov modula ----> NIJE BITNO !!!
        this_button = $(`#sirov_order`)[0];
        this_comp_id = $(this_button).closest('.cit_comp')[0].id;
        data = this_module.cit_data[this_comp_id];
        rand_id =  this_module.cit_data[this_comp_id].rand_id;

      };

      var record_statuses = [];

      var record_statuses_sifre = [];

      var new_status_data = {};

      // neka se malo svaki puta POVEĆA timestamp tako da lakše i logičnije sortira
      // neka se malo svaki puta POVEĆA timestamp tako da lakše i logičnije sortira
      
      new_status_data.curr_time = Date.now() + (arg_index*10 || 0); 

      new_status_data.status_tip = null;
      
      // "naziv" : "POSLANI UPITI DOBAV. SIROVINA",
      if ( data_for_doc.doc_type == `offer_dobav` ) new_status_data.status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS16340541453248845`, `sifra`) ); 
      // "naziv" : "SIROVINA NARUČENA",
      if ( data_for_doc.doc_type == `order_dobav` ) new_status_data.status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS16360270099667788`, `sifra`) );
      // "naziv" : "SIROVINA DOŠLA",
      if ( data_for_doc.doc_type == `in_record` ) new_status_data.status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS16348135958806685`, `sifra`) ); 
      
      
      new_status_data.full_product_name = null;
      new_status_data.dep_to = null;

      // new_status_data.new_sifra = `status`+ cit_rand();


      if (arg_sirov) {

        new_status_data.sirov = {
          "_id": arg_sirov._id,
          "sirovina_sifra": arg_sirov.sirovina_sifra,
          "naziv": arg_sirov.naziv,
          "full_naziv": window.concat_naziv_sirovine(arg_sirov),
          "dobavljac": {
            "_id": arg_sirov.dobavljac._id,
            "naziv": arg_sirov.dobavljac.naziv,
            "sjediste": window.concat_full_adresa(arg_sirov.dobavljac, `VAD1`),
          }
        };

      } 
      else {

        new_status_data.sirov = {
          "_id": data._id,
          "sirovina_sifra": data.sirovina_sifra,
          "naziv": data.naziv,
          "full_naziv": window.concat_naziv_sirovine(data),
          "dobavljac": {
            "_id": data.dobavljac._id,
            "naziv": data.dobavljac.naziv,
            "sjediste": window.concat_full_adresa(data.dobavljac, `VAD1`),
          },

        }; 


      };


      // ----------------------START -------------------------- if ako u arrayu ima selected statuses
      // ----------------------START -------------------------- if ako u arrayu ima selected statuses
      // ----------------------START -------------------------- if ako u arrayu ima selected statuses

      var doc_statuses = null;

      if ( !arg_sirov && data.selected_statuses_in_sirov?.length > 0 ) doc_statuses = data.selected_statuses_in_sirov; // kada radim recorde direktno u sirovini
      if ( arg_sirov && arg_sirov.doc_statuses?.length > 0 ) doc_statuses = arg_sirov.doc_statuses; // kada radim recorde iz PARTNERA !!!

      if ( doc_statuses ) {

        /*
        --------------------------------------------------------------------------------------------------------------
        UZMI PRODUCT OD SVAKOG SELECTED STATUSA
        --------------------------------------------------------------------------------------------------------------
        var all_products = [];
        var all_products_query = { _id: { $in: [] } };


        $.each( data.selected_statuses_in_sirov, function( s_ind, status ) {
          if ( status.product_id && all_products_query._id.$in.indexOf(status.product_id) == -1 ) all_products_query._id.$in.push( status.product_id );
        });

        var props = {
          filter: null, // status_mod.statuses_filter,
          url: `/find_product`, // /${ hash_data.statuses_page || "none" }`,
          query: all_products_query,
          desc: `pretraga svih producta od svih statusa kod kreiranja novog recorda u sirovinama`,
        };

        try {
          all_products = await search_database(null, props );
        } catch (err) {
          console.log(err);
        };

        if (
          !all_products 
          ||
          ($.isArray(all_products ) && all_products.length == 0)
        ) {
          popup_error(`Došlo je do greške prilikom učitavanja produkata za selektirane statuse !!!`);
          return;
        };

        --------------------------------------------------------------------------------------------------------------
        UZMI PRODUCT OD SVAKOG SELECTED STATUSA
        --------------------------------------------------------------------------------------------------------------

        */

        new_status_data.komentar = "";

        new_status_data.status_link = "";

        $.each( doc_statuses, function( s_ind, status ) {
          record_statuses_sifre.push(status.sifra);
        });


        // BITNO !!!!!!!!
        // BITNO !!!!!!!!
        // uspoređujem da li sam već napravio ovaj status !!!!! ------> tako da ne šaljem dupli !!!
        var compare_status_tip_sifra = null;

        if ( data_for_doc.doc_type == `offer_dobav` ) compare_status_tip_sifra = "IS16340541453248845"; // tražim "POSLANI UPITI DOBAV. SIROVINA",
        if ( data_for_doc.doc_type == `order_dobav` ) compare_status_tip_sifra = "IS16360270099667788"; // tražim "SIROVINA NARUČENA",
        if ( data_for_doc.doc_type == `in_record`   ) compare_status_tip_sifra = "IS16348135958806685"; // tražim "SIROVINA DOŠLA",
        
        
        // AKO trebam odgovoriti na status koji sam poslao prije 
        // ORDER ----> ODGOVOR NA OFFER
        // PRIMKA -----> ODGOVOR NA ORDER 
        var reply_to_status_tip_sifra = null;
        if ( data_for_doc.doc_type == `order_dobav` ) reply_to_status_tip_sifra = "IS16340541453248845"; // odogvaram na  "POSLANI UPITI DOBAV. SIROVINA", ---> tj offer status
        if ( data_for_doc.doc_type == `in_record`   ) reply_to_status_tip_sifra = "IS16360270099667788"; // odgovaram na "SIROVINA NARUČENA", ---> tj order status
        
  
        // ovak pazim da ne ponavljam isti status u grani (ako je Martina već poslala odgovor u toj grani )
        // jer Martina će poslati puno ahtjeva za ponudu, ali SAMO za prvi zahtjev treba poslati svima obavjest unutar nekog PROJEKTA

        var filtered_statuses = await already_has_this_status_tip_in_branch( record_statuses_sifre, compare_status_tip_sifra );

        // samo provjeravam da nije null ili undefined !!!!! ----> tj da li ima nešto :P
        if ( filtered_statuses ) {
          filtered_statuses = filtered_statuses.result;
        } else {
          filtered_statuses = [];
        };
        
        
        
        
        // --------------------------------- LOOP PO SELECTED STATUSES ---------------------------------
        // --------------------------------- LOOP PO SELECTED STATUSES ---------------------------------
        // --------------------------------- LOOP PO SELECTED STATUSES ---------------------------------
        
        

        $.each( doc_statuses, async function( s_ind, status ) {
          

          // uzmi samo objekte od statusa koji su filtrirani
          if ( filtered_statuses.indexOf(status.sifra) > -1 ) {
            
            
            
            var branch = await ajax_complete_branch(status);
            
            if ( branch && branch.length > 0 ) {
              // uzmi cijeli status sa svim propertijima !!!!!!!!!
              // uzmi cijeli status sa svim propertijima !!!!!!!!!
              status = find_item( branch, status.sifra, `sifra` );
            };
            
            
            var reply_work_result = write_reply_for_or_work_done( new_status_data, branch || data.statuses, reply_to_status_tip_sifra );
            if ( data.statuses ) data.statuses = reply_work_result.statuses;
            
            new_status_data.reply_for = reply_work_result.reply_for || null;
            new_status_data.done_for_status = reply_work_result.done_for_status || null;
            
            new_status_data.elem_parent_object = reply_work_result.elem_parent_object || null;
            new_status_data.elem_parent = reply_work_result.elem_parent || null;
            
            
            // može biti MANY statusa za ovaj record jer Martina može napraviti order jedne te iste sirovine za nekoliko različitih proizvoda
            // može biti MANY statusa za ovaj record jer Martina može napraviti order jedne te iste sirovine za nekoliko različitih proizvoda
            // može biti MANY statusa za ovaj record jer Martina može napraviti order jedne te iste sirovine za nekoliko različitih proizvoda

            record_statuses.push({

              sifra: status.sifra || null,
              proj_sifra: status.proj_sifra || null,
              product_id : status.product_id || null,
              item_sifra : status.item_sifra || null,
              variant : status.variant || null,
              link : `#project/${status.proj_sifra}/item/${status.item_sifra}/variant/${status.variant}/status/${new_status_data.new_sifra}`,
              full_record_name: status.full_product_name || null,
              count: ( status.sirov ? (status.order_sirov_count || 0) : 0 ),
              kupac_naziv: status.kupac_naziv || null,
              kupac_id: status.kupac_id || null,

            });


            new_status_data.kupac_naziv = status.kupac_naziv || null;
            new_status_data.kupac_id = status.kupac_id || null;


            // var product = find_item( all_products, status.product_id, `_id` );

            if ( 
              data_for_doc.doc_type == `offer_dobav`
              ||
              data_for_doc.doc_type == `order_dobav`
              || 
              data_for_doc.doc_type == `in_record`

            ) {

              new_status_data.est_deadline_date = data_for_doc.rok;
              
              
              new_status_data.full_product_name = status.full_product_name; // prepiši product name od selected statusa

              new_status_data.status_link = `#project/${status.proj_sifra}/item/${status.item_sifra}/variant/${status.variant}/status/${new_status_data.new_sifra}`

              new_status_data.dep_to = status.dep_from || null;

              new_status_data.to = {
                "_id" : status.from._id,
                "name" :  status.from.name,
                "user_number" : status.from.user_number,
                "full_name" : status.from.full_name,
                "email" : status.from.email
              };


              if (status && status.elem_parent) {

                // ali ako već postoji elem parent u trenutom statusu u loop-u
                // onda samo prepiši taj elem parent
                new_status_data.elem_parent = status.elem_parent;
                new_status_data.elem_parent_object = status.elem_parent_object;

              } else {

                // napravi da elem parent od novog statusa bude trenutni status u loop-u
                new_status_data.elem_parent = status.sifra;
                new_status_data.elem_parent_object = cit_deep(status);

              };
              
              

              if ( data_for_doc.doc_type == `offer_dobav` ) {
                
                new_status_data.order_sirov_count = record_kolicina;
                new_status_data.komentar = "POSLANI ZAHTJEVI ZA PONUDE (automatski kreiran status) KOLIČINA: " + record_kolicina ;

                new_status_data.offer_sirov_id = data._id || null;

                // "naziv" : "POSLANI UPITI DOBAV. SIROVINA",
                // new_status_data.status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS16340541453248845`, `sifra`) ); 
              
              }; // kraj ako je OFFER sirovine
              
              

              if ( data_for_doc.doc_type == `order_dobav` ) {
                
                new_status_data.order_sirov_count = record_kolicina;

                new_status_data.komentar = "SIROVINA JE NARUČENA (automatski kreiran status) KOLIČINA: " + record_kolicina ;

                new_status_data.order_sirov_id = arg_sirov._id || data._id || null;

                // "naziv" : "SIROVINA NARUČENA",
                // new_status_data.status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS16360270099667788`, `sifra`) ); 
              
              };  // kraj ako je ORDER sirovine

              
              
              if ( data_for_doc.doc_type == `in_record` ) {

                new_status_data.order_sirov_count = record_kolicina;

                new_status_data.komentar = "SIROVINA JE DOŠLA (automatski kreiran status) KOLIČINA: " + arg_sirov.doc_kolicine[0].primka_count ;

                new_status_data.order_sirov_id = arg_sirov._id || data._id || null;

                // "naziv" : "SIROVINA DOŠLA",
                // new_status_data.status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS16348135958806685`, `sifra`) ); 

              };  // kraj ako je ORDER sirovine


              // prepiši podatke o rokovima od INICIJALNOG statusa tj ZAHTJEVA ZA NABAVU SIROVINE
              // prepiši podatke o rokovima od INICIJALNOG statusa tj ZAHTJEVA ZA NABAVU SIROVINE

              new_status_data.rok_isporuke = status.rok_isporuke || null;
              new_status_data.potrebno_dana_proiz = status.potrebno_dana_proiz || null;
              new_status_data.potrebno_dana_isporuka = status.potrebno_dana_isporuka || null;
              new_status_data.rok_proiz = status.rok_proiz || null;
              new_status_data.rok_za_def = status.rok_za_def || null;

              var saved_status = await this_module.ajax_save_status( cit_deep(new_status_data), status, arg_sirov || data , `Product`, status.product_id || null );


            }; // kraj ako je offer ili order

          }; // kraj ako je ova status unutar filtered status arraya tj ako ne šaljem duplikate statusa 

        }); // kraj loopa po selected stat

      }; 

      // ------------------------------------------------ kraj if ako u arrayu ima selected statuses
      // ------------------------------------------------ kraj if ako u arrayu ima selected statuses
      // ------------------------------------------------ kraj if ako u arrayu ima selected statuses
      // ------------------------------------------------ kraj if ako u arrayu ima selected statuses



      /*
      if ( data_for_doc.doc_type == `order_dobav` ) {
        compare_status_tip_sifra = "IS16360270099667788"; // tražim "SIROVINA NARUČENA",
        // ovo je sifra za tražim "POSLANI UPITI DOBAV. SIROVINA",
        var statusi_sa_offerima = this_module.get_povezane_statuse( {"status_tip.sifra": "IS16340541453248845", branch_done: false } );
      };

      */
      
      
      if ( data_for_doc.partner_data ) {

        // PARTNER PAGE
        // uzeo sam bilo koji gumb unutar partner modula ----> NIJE BITNO !!!
        this_button = $(`#add_sirov_to_doc_btn`)[0];

        this_comp_id = $(this_button).closest('.cit_comp')[0].id;
        data = data_for_doc.partner_module.cit_data[this_comp_id];
        rand_id =  data_for_doc.partner_module.cit_data[this_comp_id].rand_id;

      }
      else {

        // SIROV PAGE ------> NE KORISTIM VIŠE !!!!!
        // uzeo sam bilo koji gumb unutar sirov modula ----> NIJE BITNO !!!
        this_button = $(`#sirov_order`)[0];
        this_comp_id = $(this_button).closest('.cit_comp')[0].id;
        data = this_module.cit_data[this_comp_id];
        rand_id =  this_module.cit_data[this_comp_id].rand_id;

      };      
      
      // ovdje spremam interni status koje Martina šalje samoj sebi !!!
      // ovdje spremam interni status koje Martina šalje samoj sebi !!!
      
      var saved_sirov_status = null;
        
      // STATUS TIP "naziv" : "NABAVA - ZAHTJEV ZA PONUDU DOBAVLJAČA" !!!!!  
      if ( data_for_doc.doc_type == `offer_dobav` ) {
        new_status_data.status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS1635256284641925`, `sifra`) ); 
      };
      
      // naziv: 'NABAVA - SIROVINA NARUČENA',
      if ( !data_for_doc.order_avans && data_for_doc.doc_type == `order_dobav` ) {
        new_status_data.status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS16360269826066462`, `sifra`) ); 
      };
      
      
      
      // naziv: 'NABAVA - SIROVINA DOŠLA',
      if ( data_for_doc.doc_type == `in_record` ) {
        new_status_data.status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS16352566219725337`, `sifra`) ); 
      };
              
      
        

      if ( data_for_doc.doc_type == `offer_dobav` ) {

        // new_status_data.new_sifra = `status`+ cit_rand();
        
        new_status_data.elem_parent = null;
        new_status_data.elem_parent_object = null;

        new_status_data.est_deadline_date = data_for_doc.rok;
        
        new_status_data.order_sirov_count = record_kolicina;
        new_status_data.komentar = "NABAVA - POSLAN ZAHTJEV ZA PONUDU (Automatski kreiran status) KOLIČINA: " + record_kolicina ;


        new_status_data.dep_to = {
            "sifra" : "NAB",
            "naziv" : "Nabava"
        };

        new_status_data.to = {
            "_id" : window.cit_user._id,
            "name" :  window.cit_user.name,
            "user_number" : window.cit_user.user_number,
            "full_name" : window.cit_user.full_name,
            "email" : window.cit_user.email
        };

        new_status_data.rok_isporuke = null;
        new_status_data.potrebno_dana_proiz = null;
        new_status_data.potrebno_dana_isporuka = null;
        new_status_data.rok_proiz = null;
        new_status_data.rok_za_def = null;

        // status za offer dobav ----> na primjer Sandra iz nabave šalje sama sebi kao podsjetnik !!!!!!!!!
        saved_sirov_status = await this_module.make_sirovina_status( 
          data_for_doc,
          cit_deep(new_status_data),
          record_statuses, 
          arg_pdf,
          data,
          arg_sirov,
          arg_all_sirovine,
          arg_index
        );
        
 
        
      };
     
      
      // obavijesti skladište samo ako nije predujam
      // ako jeste predujam ona samo obavijesti financije, a financije trebaju obavijestiti skladište !!!!
      
      // -------------- ZA SKLADIŠTE PORUKA ---------------- ako NE TREBA PLATITI ROBU UNAPRIJED
      // -------------- ZA SKLADIŠTE PORUKA ---------------- ako NE TREBA PLATITI ROBU UNAPRIJED
      if ( !data_for_doc.order_avans && data_for_doc.doc_type == `order_dobav` ) {
        
        // var order_parent_status = await get_status_parent(arg_sirov, data);
        
        var order_parent_status = null;
        var parent_status = null;
        var last_status = null;
        var last_status_time = 4654767424778;

        
        if ( data_for_doc.sirov_status ) {
          
          var branch = await ajax_complete_branch( data_for_doc.sirov_status );
          
          if ( branch && branch.length > 0 ) {
            // uzmi cijeli status sa svim propertijima !!!!!!!!!
            // uzmi cijeli status sa svim propertijima !!!!!!!!!
            data_for_doc.sirov_status = find_item( branch, data_for_doc.sirov_status.sifra, `sifra` );
          };
          
          
          
          // ODOVORI NA STATUS TIP "naziv" : "NABAVA - ZAHTJEV ZA PONUDU DOBAVLJAČA" !!!!!  
          var reply_work_result = write_reply_for_or_work_done( new_status_data, branch || data.statuses, `IS1635256284641925` ); // odgovori na 
          if ( data.statuses ) data.statuses = reply_work_result.statuses;
          
          new_status_data.reply_for = reply_work_result.reply_for || null;
          new_status_data.done_for_status = reply_work_result.done_for_status || null;
            
          new_status_data.elem_parent_object = reply_work_result.elem_parent_object || null;
          new_status_data.elem_parent = reply_work_result.elem_parent || null;
          
          order_parent_status = reply_work_result.elem_parent_object || null;
          
        } else {
          
          new_status_data.reply_for = null;
          new_status_data.done_for_status = null;
            
          new_status_data.element_parent_object = null;
          new_status_data.element_parent = null;
          
        }; 
        
        
       // new_status_data.new_sifra = `status`+ cit_rand();

        new_status_data.est_deadline_date = data_for_doc.rok;
        
        new_status_data.order_sirov_count = record_kolicina;
        new_status_data.komentar = "NABAVA - SIROVINA JE NARUČENA (automatski kreiran status) KOLIČINA: " + record_kolicina ;

        // new_status_data.status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS16360269826066462`, `sifra`) ); 

        new_status_data.dep_to = {
          "sifra" : "SKL",
          "naziv" : "Skladište"
        };

        /*
        new_status_data.to = {
            "_id" : window.cit_user._id,
            "name" :  window.cit_user.name,
            "user_number" : window.cit_user.user_number,
            "full_name" : window.cit_user.full_name,
            "email" : window.cit_user.email
        };
        */

        new_status_data.to = {
            "_id" : "60cc9207f0b3e71b940aa874",
            "name" :  "alex",
            "user_number" : 42,
            "full_name" : "Martinović Aleksandra",
            "email" : "aleksandra.martinovic@velprom.hr",
        };
        

        new_status_data.rok_isporuke = order_parent_status?.rok_isporuke || null;
        new_status_data.potrebno_dana_proiz = order_parent_status?.potrebno_dana_proiz || null;
        new_status_data.potrebno_dana_isporuka = order_parent_status?.potrebno_dana_isporuka || null;
        new_status_data.rok_proiz = order_parent_status?.rok_proiz || null;
        new_status_data.rok_za_def = order_parent_status?.rok_za_def || null;
        
        // status npr od Sandre iz nabave prema skladištu na primjer Alex ---->  kod order dobav
        saved_sirov_status = await this_module.make_sirovina_status( 
          data_for_doc,
          cit_deep(new_status_data), 
          record_statuses,
          arg_pdf,
          data,
          arg_sirov,
          arg_all_sirovine,
          arg_index 
        );
     
      
        
      };

      // ------------------------------ ako NE TREBA PLATITI ROBU UNAPRIJED
      // ------------------------------ ako NE TREBA PLATITI ROBU UNAPRIJED
      
      
      // -------------OVO JE ZA FINANCIJE ----------------- PLATI AVANS ILI SAMO PORUKA DA TREBA PLATITI  !!!!!!
      // -------------OVO JE ZA FINANCIJE ----------------- PLATI AVANS ILI SAMO PORUKA DA TREBA PLATITI  !!!!!!
      
      if ( data_for_doc.doc_type == `order_dobav` ) {
        
        // var order_parent_status = await get_status_parent(arg_sirov, data);
        
        var order_parent_status = null;
        var parent_status = null;
        var last_status = null;
        var last_status_time = 4654767424778;

        
        if ( !data_for_doc.order_avans ) {
          new_status_data.komentar = "NABAVA - POTREBNO PLATITI ROBU U VALUTI (automatski kreiran status) KOLIČINA: " + arg_sirov.doc_kolicine[0].order_count;
          // "naziv" : "NABAVA - POTREBNO PLATITI ROBU",
          new_status_data.status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS16557202880964136`, `sifra`) ); 
        
        };
        
        if ( data_for_doc.order_avans ) {
          new_status_data.komentar = "NABAVA - PLATI PREDUJAM ZA ROBU (automatski kreiran status) KOLIČINA: " + arg_sirov.doc_kolicine[0].order_count;
          // "naziv" : "NABAVA - POTREBNO PLATITI PREDUJAM",
          new_status_data.status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS16560675376649348`, `sifra`) ); 
        };
        
        
        if ( data_for_doc.sirov_status ) {
          
          var branch = await ajax_complete_branch( data_for_doc.sirov_status );
          
          if ( branch && branch.length > 0 ) {
            // uzmi cijeli status sa svim propertijima !!!!!!!!!
            // uzmi cijeli status sa svim propertijima !!!!!!!!!
            data_for_doc.sirov_status = find_item( branch, data_for_doc.sirov_status.sifra, `sifra` );
          };
          
          // satatus uvezi  plaćenja odgovori na parenta !!!
          var reply_work_result = write_reply_for_or_work_done( new_status_data, branch || data.statuses );
          if ( data.statuses ) data.statuses = reply_work_result.statuses;
          
          new_status_data.reply_for = reply_work_result.reply_for || null;
          new_status_data.done_for_status = reply_work_result.done_for_status || null;
            
          new_status_data.elem_parent_object = reply_work_result.elem_parent_object || null;
          new_status_data.elem_parent = reply_work_result.elem_parent || null;
          
          order_parent_status = reply_work_result.elem_parent_object || null;
          
        } else {
          
          new_status_data.reply_for = null;
          new_status_data.done_for_status = null;
            
          new_status_data.element_parent_object = null;
          new_status_data.element_parent = null;
          
        }; 
        
        
        
        // new_status_data.new_sifra = `status`+ cit_rand();

        new_status_data.est_deadline_date = data_for_doc.rok;
        
        new_status_data.order_sirov_count = record_kolicina;
        

        
        new_status_data.dep_to = {
          "sifra" : "FIN",
          "naziv" : "Financije"
        };

        /*
        new_status_data.to = {
            "_id" : window.cit_user._id,
            "name" :  window.cit_user.name,
            "user_number" : window.cit_user.user_number,
            "full_name" : window.cit_user.full_name,
            "email" : window.cit_user.email
        };
        */

        new_status_data.to = {
          "_id" : "60cc9206f0b3e71b940aa858",
          "name" : "marina",
          "user_number" : 14,
          "full_name" : "Bratković Marina",
          "email" : "marina@velprom.hr"
        };

        new_status_data.rok_isporuke = order_parent_status?.rok_isporuke || null;
        new_status_data.potrebno_dana_proiz = order_parent_status?.potrebno_dana_proiz || null;
        new_status_data.potrebno_dana_isporuka = order_parent_status?.potrebno_dana_isporuka || null;
        new_status_data.rok_proiz = order_parent_status?.rok_proiz || null;
        new_status_data.rok_za_def = order_parent_status?.rok_za_def || null;
        
        // status za financije za order dobav
        saved_sirov_status = await this_module.make_sirovina_status( 
          data_for_doc,
          cit_deep(new_status_data),
          record_statuses,
          arg_pdf,
          data,
          arg_sirov,
          arg_all_sirovine,
          arg_index
        );
     
      };

      // -------------OVO JE ZA FINANCIJE ----------------- PLATI AVANS ILI SAMO PORUKA DA TREBA PLATITI  !!!!!!
      // -------------OVO JE ZA FINANCIJE ----------------- PLATI AVANS ILI SAMO PORUKA DA TREBA PLATITI  !!!!!!
      
      
      // ------------------------------------------ ALEX ŠALJE NABAVI DA JE DOŠLA ROBA ------------------------------------------
      // ------------------------------------------ ALEX ŠALJE NABAVI DA JE DOŠLA ROBA ------------------------------------------
      if ( data_for_doc.doc_type == `in_record` ) {
        
        // var primka_parent_status = await get_status_parent(arg_sirov, data);
        
        var primka_parent_status = null;
        var parent_status = null;
        var last_status = null;
        var last_status_time = 4654767424778;

        
        new_status_data.komentar = "NABAVA - SIROVINA DOŠLA (automatski kreiran status) KOLIČINA: " + record_kolicina ;

        // "naziv" : "NABAVA - SIROVINA DOŠLA",
        new_status_data.status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS16352566219725337`, `sifra`) ); 
        
        
        if ( data_for_doc.sirov_status ) {
          
          var branch = await ajax_complete_branch( data_for_doc.sirov_status );
          
          if ( branch && branch.length > 0 ) {
            // uzmi cijeli status sa svim propertijima !!!!!!!!!
            // uzmi cijeli status sa svim propertijima !!!!!!!!!
            data_for_doc.sirov_status = find_item( branch, data_for_doc.sirov_status.sifra, `sifra` );
          };
          
          // ODGOVORI NA naziv: 'NABAVA - SIROVINA NARUČENA',
          var reply_work_result = write_reply_for_or_work_done( new_status_data, branch || data.statuses, `IS16360269826066462` );
          if ( data.statuses ) data.statuses = reply_work_result.statuses;
          
          new_status_data.reply_for = reply_work_result.reply_for || null;
          new_status_data.done_for_status = reply_work_result.done_for_status || null;
            
          new_status_data.elem_parent_object = reply_work_result.elem_parent_object || null;
          new_status_data.elem_parent = reply_work_result.elem_parent || null;
          
          primka_parent_status = reply_work_result.elem_parent_object || null;
          
        } 
        else {
          
          new_status_data.reply_for = null;
          new_status_data.done_for_status = null;
            
          new_status_data.element_parent_object = null;
          new_status_data.element_parent = null;
          
        }; 
        
        // new_status_data.new_sifra = `status`+ cit_rand();

        new_status_data.est_deadline_date = data_for_doc.rok;
        
        new_status_data.order_sirov_count = record_kolicina;
        
        new_status_data.dep_to = {
          "sifra" : "NAB",
          "naziv" : "Nabava"
        };
        

        /*
        new_status_data.to = {
            "_id" : window.cit_user._id,
            "name" :  window.cit_user.name,
            "user_number" : window.cit_user.user_number,
            "full_name" : window.cit_user.full_name,
            "email" : window.cit_user.email
        };
        */

        new_status_data.to = {
          "_id" : "60cc9207f0b3e71b940aa884",
          "name" : "martina",
          "user_number" : 58,
          "full_name" : "Šaravanja Martina",
          "email" : "martina@velprom.hr"
        };

        
        new_status_data.rok_isporuke = primka_parent_status?.rok_isporuke || null;
        new_status_data.potrebno_dana_proiz = primka_parent_status?.potrebno_dana_proiz || null;
        new_status_data.potrebno_dana_isporuka = primka_parent_status?.potrebno_dana_isporuka || null;
        new_status_data.rok_proiz = primka_parent_status?.rok_proiz || null;
        new_status_data.rok_za_def = primka_parent_status?.rok_za_def || null;
        
        // status od skladišta prema nabavi da je roba došla
        saved_sirov_status = await this_module.make_sirovina_status( 
          data_for_doc,
          cit_deep(new_status_data),
          record_statuses,
          arg_pdf,
          data,
          arg_sirov,
          arg_all_sirovine,
          arg_index 
        );
        
        
        
        
        
     
      };
      // ------------------------------------------ ALEX ŠALJE NABAVI DA JE DOŠLA ROBA ------------------------------------------
      // ------------------------------------------ ALEX ŠALJE NABAVI DA JE DOŠLA ROBA ------------------------------------------
      

      
      var new_record = this_module.make_new_record( data_for_doc, record_statuses, arg_pdf, this_module, data, arg_sirov );

      var this_button = null;
      var this_comp_id = null;
      var data = null;
      var rand_id = null;

      // dodaj ove propertije u data for doc !!!!
      data_for_doc.pdf_name = arg_pdf.ime_filea;
      data_for_doc.pdf_time = arg_pdf.time;
      data_for_doc.doc_link = new_record.doc_link;

      // ubaci dokument listu dokumenata

      if ( data_for_doc.partner_data ) {
        
        if ( saved_sirov_status ) data_for_doc.sirov_status = cit_deep(saved_sirov_status);
        
        // spremam record na serveru U SIROVINU
        var record_result = await ajax_upsert_record(new_record);

        // console.log(record_result);
        console.log(`------------ GOTOV PARTNER DOC RECORD ZA SIROVINU: ` + arg_sirov.sirovina_sifra );
        
        //  OVO ŠALJEM NAZAD U CIT PREVIEW 
        
        resolve({
          new_record: new_record,
          data_for_doc: data_for_doc,
        });

      } 
      else {

        // ------------NE KORISTIM VIŠE !!!!!! -------------- ZA SIROV MODULE --------------------------
        // ------------NE KORISTIM VIŠE !!!!!! -------------- ZA SIROV MODULE --------------------------
        // ------------NE KORISTIM VIŠE !!!!!! -------------- ZA SIROV MODULE --------------------------
        
        this_module.make_doc_spec_for_record(data_for_doc, arg_pdf);

        data.records.push(new_record);

        this_module.make_records_list(data);

        resolve({
          new_record: new_record,
          data_for_doc: data_for_doc,
        });
        
        setTimeout( function() {
          this_module.save_sirov();
        }, 300 );

      };

    
    }); // kraj promisa  
      
    
  }; 
  this_module.sirov_status_and_record = sirov_status_and_record;
  
  
  async function send_status_for_est_time_change(record) {
    
    var db_partner = null;

    var props = {
      filter: null,
      url: '/find_partner',
      query: record,
      desc: `pretraga statusa kod promjene est time !!!!`,
    };

    try {
      db_partner = await search_database(null, props );
    } catch (err) {
      console.log(err);
      resolve(null);

    };

    // ako je našao nešto onda uzmi samo prvi član u arraju
    if ( db_partner && $.isArray(db_partner) && db_partner.length > 0 ) {
      db_partner = db_partner[0];
      resolve(db_partner);
    } else {
      resolve(null);
    };

    
  };
  this_module.send_status_for_est_time_change = send_status_for_est_time_change;
  
  

  function make_new_record( data_for_doc, record_statuses, arg_pdf, sirov_module, data, arg_sirov ) {
    
    /*    
    var criteria = [ '~time' ];
    var records_copy = cit_deep(data.records);
    multisort( records_copy, criteria );
    var last_record = records_copy[0];
    */
    
    var new_record = null;
    
    
    var smaller_sirov_status = null;

    // sirov status je status koji NABAVA ŠALJE SAMA SEBI KAO EVIDENCIJU DA JE ROBA NARAČENA ILI JE DOŠLA ILI TREBA PLATITI ITD .....
    
    if ( data_for_doc.sirov_status ) {

      smaller_sirov_status = {
        "full_product_name" : data_for_doc.sirov_status.full_product_name || null,
        "link" : data_for_doc.sirov_status.link || null,
        "sifra" : data_for_doc.sirov_status.sifra || null,
        "elem_parent" : data_for_doc.sirov_status.elem_parent || null,
        "elem_parent_object" : data_for_doc.sirov_status.elem_parent_object || null,
        "status_tip_naziv" : data_for_doc.sirov_status.status_tip?.naziv || data_for_doc.sirov_status.status_tip_naziv || null,
        "_id" : data_for_doc.sirov_status._id || null,
        "status_id" : data_for_doc.sirov_status.status_id || null,
      };
    };

    
    
    
    if ( data_for_doc.doc_type == `offer_dobav` ) {
      
      
      var doc_link = 
`
<a href="/docs/${time_path(arg_pdf.time)}/${arg_pdf.ime_filea}" target="_blank" onClick="event.stopPropagation();" > 
  ${ window.map_name_to_type[data_for_doc.doc_type] }: ${ data_for_doc.doc_sifra }
</a>
`; 
      
      var count = 0;
      
      if ( arg_sirov ) {
        
        // ako postoji arg_sirov onda radim dokumente u partner page 
        if ( arg_sirov.doc_kolicine?.length > 0 ) {
          $.each( arg_sirov.doc_kolicine, function(k_ind, kol) {
            if ( cit_number(kol.count) !== null && kol.count > count ) count = kol.count; // uzimam najveći broj koji imam !!!
          });
        };
        
      } else {
        // ako nema arg sirov onda sam u sirov page pa uzimam količine direktno iz polja koje sam upisao
        // tj iz data objekta jer tada je data object zapravo objekt sirovine !!!
        count = data.ask_kolicina_6 || data.ask_kolicina_5 || data.ask_kolicina_4 || data.ask_kolicina_3 || data.ask_kolicina_2 || data.ask_kolicina_1 || 0;
      };
      
      
      
      new_record = {
        
        sirov_id: arg_sirov._id || data._id || null, // ako imam arg sirov onda je to u PARTNER ali ako NEMAM onda uzmi datakoji će biti onda SIROV
        
        count: count,
        
        record_parent: data.current_record_parent || null,
        
        doc_parent: data_for_doc.doc_parent || null,
        
        time: Date.now(),
        
        sifra: `record`+cit_rand(),
        
        doc_sifra: data_for_doc.doc_sifra,
        pdf_name: arg_pdf.ime_filea,
        doc_link: doc_link,
        
        type: data_for_doc.doc_type,
      
        record_statuses: record_statuses || null,
        
        sirov_status: smaller_sirov_status || null,
      
        record_est_time: data.record_est_time,
        
        from_to: null, // samo za medju sklad
        
        
        storno_time: null,
        storno_kolicina: null,
        
        pk_kolicina: null,
        nk_kolicina: null,
        rn_kolicina: null,
              
        order_kolicina: null,
        sklad_kolicina: null,
        
        partner_name: data_for_doc.partner_name,
        partner_adresa: data_for_doc.partner_adresa,
        partner_id: data_for_doc.partner_id,

      };
      
    
    }; // kraj ako je offer dobav
    
    if ( data_for_doc.doc_type == `order_dobav` ) {
      
      var doc_link = 
`
<a href="/docs/${time_path(arg_pdf.time)}/${arg_pdf.ime_filea}" target="_blank" onClick="event.stopPropagation();" > 
  ${ window.map_name_to_type[data_for_doc.doc_type] }: ${ data_for_doc.doc_sifra }
</a>
`;
      
      var count = 0;
      
      if ( arg_sirov ) {
        
        // ako postoji arg_sirov onda radim dokumente u partner page 
        if ( arg_sirov.doc_kolicine?.length > 0 ) {
          $.each( arg_sirov.doc_kolicine, function(k_ind, kol) {
            if ( cit_number(kol.count) !== null && kol.count > count ) count = kol.count; // uzimam najveći broj koji imam !!!
          });
        };
      } else {
        // ako nema arg sirov onda sam u sirov page pa uzimam količine direktno iz polja koje sam upisao
        // tj iz data objekta jer tada je data object zapravo objekt sirovine !!!
        count = data.ask_kolicina_1 || 0;
      };
      
      
      new_record = {
        
        sirov_id: arg_sirov._id || data._id || null, 
        
        count: count,
        
        record_parent: data.current_record_parent || null,
        
        
        
        prev_doc: data_for_doc.prev_doc || null,
        doc_parent: data_for_doc.doc_parent || null,
      
        
        time: Date.now(),
        
  
        
        sifra: `record`+cit_rand(),
        
        doc_sifra: data_for_doc.doc_sifra,
        
        pdf_name: arg_pdf.ime_filea,
        pdf_time: arg_pdf.time,
        
        doc_link: doc_link,
        
        type: data_for_doc.doc_type,
      
        record_statuses: record_statuses,
        
        sirov_status: smaller_sirov_status || null,
        
        record_est_time: data_for_doc.rok, // procjena vremena kada će doći do promjene
        
        from_to: null, // samo za medju sklad
        
        storno_time: null,
        storno_kolicina: null,
        
        pk_kolicina: null,
        nk_kolicina: null,
        rn_kolicina: null,

        order_kolicina: count,
        sklad_kolicina: null,
        
        
        partner_name: data_for_doc.partner_name,
        partner_adresa: data_for_doc.partner_adresa,
        partner_id: data_for_doc.partner_id,
        

      };
      
    
    }; // kraj ako je order dobav
    
    if ( data_for_doc.doc_type == `in_record` ) {
      
      var doc_link = 
`
<a href="/docs/${time_path(arg_pdf.time)}/${arg_pdf.ime_filea}" target="_blank" onClick="event.stopPropagation();" > 
  ${ window.map_name_to_type[data_for_doc.doc_type] }: ${ data_for_doc.doc_sifra }
</a>
`;
      
      var count = 0;
      
      if ( arg_sirov ) {
        
        // ako postoji arg_sirov onda radim dokumente u partner page 
        // za primku je uvijek jedan količina dotične sirovine !!!!!
        if ( arg_sirov.doc_kolicine?.length > 0 ) {
          $.each( arg_sirov.doc_kolicine, function(k_ind, kol) {
            if ( cit_number(kol.count) !== null && kol.count > count ) count = kol.count; // uzimam najveći broj koji imam !!!
          });
        };
        
      } else {
        // ako nema arg sirov onda sam u sirov page pa uzimam količine direktno iz polja koje sam upisao
        // tj iz data objekta jer tada je data object zapravo objekt sirovine !!!
        count = data.ask_kolicina_1 || 0;
      };
      
      
      new_record = {
        
        doc_kolicine: arg_sirov.doc_kolicine || null,
        
        sirov_id: arg_sirov._id || data._id || null, 
        
        count: count,
        
        record_parent: data.current_record_parent || null,
        
        prev_doc: data_for_doc.prev_doc || null,
        doc_parent: data_for_doc.doc_parent || null,
      
        
        time: Date.now(),
        
  
        
        sifra: `record`+cit_rand(),
        
        doc_sifra: data_for_doc.doc_sifra,
        
        pdf_name: arg_pdf.ime_filea,
        pdf_time: arg_pdf.time,
        
        doc_link: doc_link,
        
        type: data_for_doc.doc_type,
      
        record_statuses: record_statuses,
        
        sirov_status: smaller_sirov_status || null,
        
        record_est_time: data_for_doc.rok, // procjena vremena kada će doći do promjene il ako je trenutna promjena kao na primjer kod PRIMKE !!!
        
        from_to: null, // samo za medju sklad
        
        storno_time: null,
        storno_kolicina: null,
        
        pk_kolicina: null,
        nk_kolicina: null,
        rn_kolicina: null,

        order_kolicina: null,
        sklad_kolicina: count,
        
        
        partner_name: data_for_doc.partner_name,
        partner_adresa: data_for_doc.partner_adresa,
        partner_id: data_for_doc.partner_id,
        

      };
      
    
    }; // kraj ako je in record 
    
    if ( data_for_doc.doc_type == `in_pro_record` ) {
      
      var doc_link = 
`
<a href="/docs/${time_path(arg_pdf.time)}/${arg_pdf.ime_filea}" target="_blank" onClick="event.stopPropagation();" > 
  ${ window.map_name_to_type[data_for_doc.doc_type] }: ${ data_for_doc.doc_sifra }
</a>
`;
      
      var count = 0;
      
      if ( arg_sirov ) {
        
        // ako postoji arg_sirov onda radim dokumente u partner page 
        if ( arg_sirov.doc_kolicine?.length > 0 ) {
          $.each( arg_sirov.doc_kolicine, function(k_ind, kol) {
            if ( cit_number(kol.count) !== null && kol.count > count ) count = kol.count; // uzimam najveći broj koji imam !!!
          });
        };
      } else {
        // ako nema arg sirov onda sam u sirov page pa uzimam količine direktno iz polja koje sam upisao
        // tj iz data objekta jer tada je data object zapravo objekt sirovine !!!
        count = data.ask_kolicina_1 || 0;
      };
      
      
      new_record = {
        
        sirov_id: arg_sirov._id || data._id || null, 
        
        count: count,
        
        record_parent: data.current_record_parent || null,
        
        
        prev_doc: data_for_doc.prev_doc || null,
        doc_parent: data_for_doc.doc_parent || null,
        
        
        time: Date.now(),
        
  
        
        sifra: `record`+cit_rand(),
        
        doc_sifra: data_for_doc.doc_sifra,
        
        pdf_name: arg_pdf.ime_filea,
        pdf_time: arg_pdf.time,
        
        doc_link: doc_link,
        
        type: data_for_doc.doc_type,
      
        record_statuses: record_statuses,
        
        sirov_status: smaller_sirov_status || null,
        
        record_est_time: data_for_doc.rok, // procjena vremena kada će doći do promjene
        
        from_to: null, // samo za medju sklad
        
        storno_time: null,
        storno_kolicina: null,
        
        pk_kolicina: null,
        nk_kolicina: null,
        rn_kolicina: null,

        order_kolicina: null,
        sklad_kolicina: count,
        
        
        partner_name: data_for_doc.partner_name,
        partner_adresa: data_for_doc.partner_adresa,
        partner_id: data_for_doc.partner_id,
        

      };
      
    
    }; // kraj ako je in PRO record 
    
    
    return new_record;
    
    
  };
  this_module.make_new_record = make_new_record;
  
  
  async function make_sirovina_status( data_for_doc, new_status_data, record_statuses, arg_pdf, data, arg_sirov, arg_all_sirovine, arg_index ) {
    
    
    new_status_data.new_sifra = `status` + cit_rand();
    
    if ( arg_sirov ) {
      // new_status_data.full_product_name = ( arg_sirov.sirovina_sifra + "--" + arg_sirov.full_naziv + "--" + (arg_sirov.dobavljac?.naziv || "") );
      new_status_data.full_product_name = arg_sirov.dobavljac?.naziv || ""; // ime dobavljača
      new_status_data.status_link = `#partner/${data._id}/status/${new_status_data.new_sifra}`;
    }
    else {
      new_status_data.full_product_name = ( data.sirovina_sifra + "--" + data.full_naziv + "--" + (data.dobavljac?.naziv || "") );
      new_status_data.status_link = `#sirov/${data._id}/status/${new_status_data.new_sifra}`;
    };

    
    return new Promise ( async function(resolve, reject) {

      new_status_data.docs = [{
        sifra: `doc`+cit_rand(),
        time: arg_pdf.time,
        link: `<a href="/docs/${time_path(arg_pdf.time)}/${arg_pdf.ime_filea}" target="_blank" onClick="event.stopPropagation();" >${ arg_pdf.ime_filea }</a>`,
      }];

      if ( record_statuses.length > 0 ) {

        /*
        -----------------------------------------------------------------------------
        OVO JE PRIMJER: 
        record_statuses.push({

          sifra: status.status,
          proj_sifra: status.proj_sifra,
          product_id : status.product_id,
          item_sifra : status.item_sifra,
          variant : status.variant,
          link : `#project/${status.proj_sifra}/item/${status.item_sifra}/variant/${status.variant}/status/${new_sifra}`,
          full_record_name: status.full_product_name,

        });
        -----------------------------------------------------------------------------
        */

        new_status_data.komentar +=  `<br>POVEZANO:`;

        $.each(record_statuses, function( r_ind, rec_status ) {

          new_status_data.komentar += 
            `
            <br>
            <a href="${rec_status.link}" 
                target="_blank"
                onClick="event.stopPropagation();"> 
                ${rec_status.full_record_name} 
            </a>
            `;
        });

      }; // kraj ako ima nešto u record satuses


      var saved_status = null;
      
      
      new_status_data = cit_deep(new_status_data);
      
      if ( data_for_doc.partner_data ) {

        // ---------  SAMO AKO JE OVO PRVI LOOP ONDA SPREMI STATUSE ZA NABAVU U PARTNER MODULU  ------------------------------------------
        // spremi samo prvi status jer nema smisla da bude nekoliko istih zahtjeva za sirovinu
        if ( arg_index === 0 ) {
          saved_status = await this_module.ajax_save_status( new_status_data, null, arg_sirov || data, `Partner`, data._id );
          
        };
        // ---------------------------------------------------

      } else {
        
        // NE KORISTIM OVO VIŠE !!!!!
        // ovo je slučaj kad radim nabavljanje robe iz sirovine a ne iz partnera
        saved_status = await this_module.ajax_save_status( new_status_data, null, arg_sirov || data, `Sirov`, data._id ); 
      };
      
      resolve( saved_status );
      
    }); // kraj promisa  
    
    
  };
  this_module.make_sirovina_status = make_sirovina_status;
  
  
  function ajax_save_status( new_status_data, status, data, Model_name, Model_id ) {
    
    var status_reply = {

      "rok_isporuke" : status?.rok_isporuke || new_status_data.rok_isporuke || null,
      "potrebno_dana_proiz" : status?.potrebno_dana_proiz || new_status_data.potrebno_dana_proiz || null,
      "potrebno_dana_isporuka" : status?.potrebno_dana_isporuka || new_status_data.potrebno_dana_isporuka || null,
      "rok_proiz" : status?.rok_proiz || new_status_data.rok_proiz || null,
      "rok_za_def" : status?.rok_za_def || new_status_data.rok_za_def || null,


      "full_product_name" : new_status_data.full_product_name,
      "product_id" : status?.product_id || null,
      "link" : new_status_data.status_link,
      "proj_sifra": status?.proj_sifra || null,
      "item_sifra": status?.item_sifra || null,
      "variant" : status?.variant || null,
      
      "sifra" : new_status_data.new_sifra,
      "status" : new_status_data.new_sifra,
      
      kupac_naziv: new_status_data.kupac_naziv || null,
      kupac_id: new_status_data.kupac_id || null,
      

      "elem_parent" : new_status_data.elem_parent || null,
      "elem_parent_object" : new_status_data.elem_parent_object || null,

      "time" : new_status_data.curr_time,
      "work_times" : null,
      "docs" : new_status_data.docs || null,
      "est_deadline_hours" : null,
      "est_deadline_date" : new_status_data.est_deadline_date || null,
      "start" : new_status_data.curr_time,
      "end" : null,

      "old_status_tip" : status?.status_tip || null,

      "status_tip" : new_status_data.status_tip,

      "dep_from" : {
          "sifra" : "NAB",
          "naziv" : "Nabava"
      },

      "from" : {
          "_id" : window.cit_user._id,
          "name" :  window.cit_user.name,
          "user_number" : window.cit_user.user_number,
          "full_name" : window.cit_user.full_name,
          "email" : window.cit_user.email
      },


      "dep_to" : new_status_data.dep_to,
      "to" : new_status_data.to,

      "old_komentar" : status?.komentar || null,
      "komentar" : new_status_data.komentar,

      "reply_for" : new_status_data.reply_for || null,

      "sirov" : new_status_data.sirov || null,
      
      "order_sirov_count" : new_status_data.order_sirov_count || null,

      "responded" : null,
      "done_for_status" : new_status_data.done_for_status || null, /* ovo je samo kada je ovaj trenutni status kraj rada za neki prijašnji satus */
      "work_finished" : null,
      "seen" : null,

    };       

    
    
    // obriši sirov jer nema smisla ponavljati sve podatke koje ionako imam u doc itemu (mislim na sve doc_sirovs i doc_kolicine )
    if ( Model_name == "Partner" ) status_reply.sirov = null;
    

    return new Promise( function( resolve, reject ) {

      $.ajax({
        headers: {
          'X-Auth-Token': window.cit_user.token
        },
        type: "POST",
        cache: false,
        url: `/save_status`,
        data: JSON.stringify( {
          ...status_reply,
          Model_name, 
          Model_id,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done( async function (result) {

        console.log(result);

        if ( result.success == true ) {

          cit_toast(`STATUS JE SPREMLJENI!`);
          cit_toast(`Poslan status prema ${ status_reply.to.full_name }`, `background: #3f6ad8;`);

          cit_socket.emit( 'new_status', status_reply );
          animate_badge( $(`#cit_task_from_me`), 1 );

          var sirov_comp_id = null
          var sirov_data = null;

          var saved_status = await save_status_in_all_modules(null, result, Model_name );
          
          
          resolve( result.status );

        } else {

          // ako result NIJE SUCCESS = true

          if ( result.msg ) console.error(result.msg);
          if ( !result.msg ) console.error(`Greška prilikom spremanja STATUSA!`);
          
          resolve( null );

        };

      })
      .fail(function (error) {
        console.error(`Došlo je do greške na serveru prilikom spremanja STATUSA!`);
        console.log(error);
        
        resolve( null );
        
      })
      .always(function() {

        toggle_global_progress_bar(false);

      });

    });  // kraj promisa
      

  }; // kraj ajax save status
  this_module.ajax_save_status = ajax_save_status;

  
  function already_has_this_status_tip_in_branch(status_sifre, compare_status_tip_sifra) {
    
    return new Promise( function(resolve, reject) {
    

      $.ajax({
        headers: {
          'X-Auth-Token': window.cit_user.token
        },
        type: "POST",
        cache: false,
        url: `/already_has_this_status_tip_in_branch`,
        data: JSON.stringify( {
          status_sifre: status_sifre,
          compare_status_tip_sifra: compare_status_tip_sifra
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done( function (result) {

        console.log(result);

        if ( result.success == true ) {
          
            resolve(result);
          
        } else {

          // ako result NIJE SUCCESS = true

          if ( result.msg ) console.error(result.msg);
          if ( !result.msg ) console.error(`Greška prilikom spremanja STATUSA!`);
          
          resolve(null);

        };

      })
      .fail(function (error) {
        console.error(`Došlo je do greške na serveru prilikom spremanja STATUSA!`);
        console.log(error);
        resolve(null);
      })
      .always(function() {
        toggle_global_progress_bar(false);
      });
      
      
    }); // kraj promisa
    
    
    
    
  };
  this_module.already_has_this_status_tip_in_branch = already_has_this_status_tip_in_branch;
  
  
  function make_selected_statuses_list(data, arg_this_module) {
    
    
    if ( !data.selected_statuses_in_sirov ) {
      $(`#selected_statuses_box`).html('');
      return;
    };
    
    // ako nema selektiranih statusa
    if ( data.selected_statuses_in_sirov.length == 0 ) {
      $(`#selected_statuses_box`).html('');
      return;
    };
    
    data.selected_statuses_in_sirov = this_module.status_module.statuses_filter(data.selected_statuses_in_sirov, $);
    
    // ovaj filter inače NE KREIRA DELETE BUTTON JER SE STATUSI INAČE NE SMIJU BRISATI kada su u listi ( osim u projektu) !!!
    // zato moram ručno obrisati property hide delete tako da ipak prikaže delet buttone 
    // SAMO U OVOJ LISTI !!!!!
    $.each( data.selected_statuses_in_sirov, function(s_ind, status) {
      data.selected_statuses_in_sirov[s_ind].hide_delete = false;
    });
    
    var selected_statuses_props = {
      
      hide_sort_arrows: true,
      
      desc: `samo za kreiranje liste odabranih statusa na koje user u nabavi kad MArtina odgovara na zahtjee za sirovinu  `,
      local: true,
      
      list: data.selected_statuses_in_sirov,
      
      show_cols: [ 
      
        "product_link",
        "status_tip_naziv",
        "komentar",
        "from_full_name",
        "to_full_name",
        "est_deadline_hours",
        "work_hours",
        "est_deadline_date",
        "rok_isporuke",
        "start",
        "end",
        "duration",
        "late",
        "docs",
        'button_delete',
      ],
      
      custom_headers: [
      
        "STAVKA",
        "STATUS",
        "KOMENTAR",
        "OD",
        "PREMA",
        "PROCJENA SATI",
        "UPISANO SATI",
        "PROCJENA DATUM",
        "ROK ISPORUKE",
        "START",
        "END",
        "TRAJANJE SATI",
        "KASNI SATI",
        "DOCS",
        'OBRIŠI',
        
      ],
      col_widths: [
      
        1.7, // "Stavka"
        1.5, // "Status",
        3, //"Komentar",
        1, //"Od",
        1, //"Prema",
        0.7, //"Procjena sati",
        0.7, // "Upisano sati",
        1, // "Procjena datum",
        1, // "Rok isporuke",
        1, //"Start",
        1, //"End",
        0.7, //"Trajanje",
        0.7, //"Kasni",
        3.8, //"Docs",
        0.5, //'OBRIŠI,
      ],
      format_cols: this_module.status_module.format_cols_obj,
      
      parent: `#selected_statuses_box`,
      return: {},
      show_on_click: false,
      cit_run: function(state) { console.log(state); },
   
      button_delete: async function(e) {
        
        e.stopPropagation();
        e.preventDefault();
        
        var this_button = this;
        
        console.log("kliknuo DELETE za SELECTED STATUS");
        
        
        function delete_yes() {

          
          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          
          var this_module = arg_this_module || this_module;
          
          
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          data.selected_statuses_in_sirov = delete_item(data.selected_statuses_in_sirov, sifra , `sifra` );
          
          $(`#`+parent_row_id).remove();
        };
        
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite izbaciti ovaj ovaj Status s liste?`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite izbaciti ovaj ovaj Status s liste?`, null, 'yes/no', delete_yes, delete_no, null);
        
      },
      
    };
    
    // obriši sve od prije generiranja nove liste
    $(`#selected_statuses_box`).html('');
    
    if (data.selected_statuses_in_sirov.length > 0 ) {
      create_cit_result_list(data.selected_statuses_in_sirov, selected_statuses_props );
    }; // kraj ako ima nešto u arrajuodabrane statuse
    
    
  };
  this_module.make_selected_statuses_list = make_selected_statuses_list;
  
  
  function remove_duplicate_sirov_statuses(items, jQuery ) {
      
    
    var $ = jQuery;
    
    var criteria = [ '~time' ];

    if ( typeof global !== "undefined" ) {
      global.multisort( items, criteria );
    } else {
      multisort( items, criteria );
    };

    

    var uniq_statuses = [];

    $.each( items, function( s_ind, status ) {

      var curr_status_sifra = status.sifra;
      var curr_product_id = status.product_id;

      // provjeravam jel status doslovno isti tj jel ima istu šifru
      var is_copy = false;
      $.each( uniq_statuses, function( uniq_ind, uniq_status ) {
        if ( uniq_status.sifra == curr_status_sifra ) is_copy = true;
      });

      // ILI
      // provjeram jel postoji status za isti proizvod !!!
      var same_product = false;
      $.each( uniq_statuses, function( uniq_ind, uniq_status ) {
        if ( uniq_status.product_id == curr_product_id ) same_product = true;
      });
      
      
      function is_nabava_status(status) {
        var is_nabava = false;
        if (
          status.status_tip 
          &&
          status.status_tip.cat 
          &&
          status.status_tip.cat.sifra == `nabava`
          ) {
          is_nabava = true;
        };
        
        return is_nabava;
        
      };

      
      // ne smje se ponavljati, ne smje biti od istog producta, ne smje biti status za nabavu !!!!
      if ( is_copy == false && same_product == false && is_nabava_status(status) == false ) uniq_statuses.push(status);
      
    });

    return uniq_statuses;

  };
  this_module.remove_duplicate_sirov_statuses = remove_duplicate_sirov_statuses;
  
  
  
  async function open_sirov_original(product) {
    
    
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    var this_input = $('#'+current_input_id)[0];

    if ( product == null ) {
      
      $('#'+current_input_id).val(``);
      data.original = null;
      
    } 
    else {
    
    var pop_text = 
`Jeste li sigurni da želite definirati ovaj proizvod kao izlaznu Robu?
<br>
<br>
Svi podaci koje ste do sada upisali će biti obrisani !!!
`;
    
    async function original_yes() {

      console.log(product);
      
      var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
 
      // obriši ako je user nešto napisao u poljima podaci
      $(`#`+data.id+ ` .cit_tab_box.podaci .cit_input`).each( function() {
        if ( this.id.indexOf(`_sirovina_sifra`) == -1 ) $(this).val(``);
      });
      
      // obriši ako je user nešto napisao u poljima skladiste
      $(`#`+data.id+ ` .cit_tab_box.skladiste .cit_input`).each( function() {
        if ( this.id.indexOf(`_sirovina_sifra`) == -1 ) $(this).val(``);
      });

      var sirovina_sifra = data.sirovina_sifra || null;
      var db_id = data._id || null;
      
      //resetiraj sve unutar sirovine OSIM SIROVINA SIFRA  i _id od sirovine !!!
      this_module.cit_data[this_comp_id] = cit_deep( { ...data, ...this_module.new_sirov, sirovina_sifra: sirovina_sifra, _id: db_id } );
    
      data = this_module.cit_data[this_comp_id];
      
      data.original = cit_deep(product);
      
      data.naziv = product.full_product_name;
      $('#'+rand_id + `_naziv`).val( product.full_product_name );
      
      $('#'+current_input_id).val(product.full_product_name);
      
      $('#'+ data.id + ' .link_to_original').attr(
        `href`, 
        `#project/${product.proj_sifra || null }/item/${product.item_sifra || null }/variant/${ product.variant || null }/status/null`
      );
      
      console.log(data);
      
    }; // kraj od yes function od popup

      
    function original_no() {
      
      show_popup_modal(false, pop_text, null );
      $('#'+current_input_id).val(``);
      data.original = null;
      
    };

    var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
    var show_popup_modal = pop_mod.show_popup_modal;
    show_popup_modal(`warning`, pop_text, null, 'yes/no', original_yes, original_no, null);
      
    }; // kraj od elese (ako state nije null )
    
  };
  this_module.open_sirov_original = open_sirov_original;
  
  
  function choose_record_est_time(state, this_input) {
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      data.record_est_time = null;
      console.log(`record_est_time: `, null );
      return;
    };
    
    data.record_est_time = state;
    console.log(`new record_est_time: `, new Date(state));
    
  };
  this_module.choose_record_est_time = choose_record_est_time;
  
  
  
  function make_alat_shelfs_list() {
    
    
    var this_button = this;
    
    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    
     
    $.ajax({
      headers: {
        'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
      },
      type: "POST",
      cache: false,
      url: `/get_alat_shelfs`,
      data: JSON.stringify( {} ),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done(function (result) {
      
      console.log(result);
      
      if ( result.success == true ) {


        if ( result.alat_locations?.shelfs?.length > 0 ) {
          
          
          data.alat_locations = result.alat_locations;

          var criteria = [ 'sifra' ];
          multisort( result.alat_locations.shelfs, criteria );

          $.each( data.alat_locations.shelfs, function(shelf_index, shelf_obj ) {

            var {
              alati
            } = shelf_obj;
            
            
            var alati_links = ``;
            
            if ( alati?.length > 0 ) {
              
              
              $.each( alati, function(a_ind, alat) {
                
              /*
                {
                    "shelf" : "R4",
                    "id" : "6234650d393084daf17da3e5",
                    "sifra" : "ALA168.",
                    "old_sifra" : "000856",
                    "time" : 1651358578145.0,
                    "user_id" : "60a4bc4f8617c34c16d1979f",
                    "user_number" : 10,
                    "full_name" : "Toni Kutlić",
                    "email" : "toni.kutlic@velprom.hr"
                }
                
                */ 

                
                var link_alata =
`
<a  href="#sirov/${alat._id}/status/null" 
    class="cit_tooltip" target="_blank" 
    onClick="event.stopPropagation();" 
    style="margin-right: 10px;"
    data-toggle="tooltip" 
    data-placement="top" 
    data-html="true" 
    title="${ cit_dt(alat.time).date_time + "<br>" + alat.full_name }" >  
    
  ${ alat.sifra || "ALAT" }${ alat.old_sifra ? ` (${ alat.old_sifra })` : "" }
  
</a>
`;
                alati_links += link_alata;
                
              });
            
            };

       
            data.alat_locations.shelfs[shelf_index].alati_links = alati_links;
            

          }); // kraj loopa svih polica alata


          var alat_shelfs_props = {
            
            desc: 'samo za kreiranje tablice svih polica alata unutar ROBA PAGE',
            local: true,

            list: data.alat_locations,

            show_cols: [
              "sifra",
              "visina",
              "dubina",
              "max",
              "free",
              "count",
              "sklad",
              "alati_links",

              'button_edit',
            ],
            custom_headers: [
              "ŠIFRA",
              "VISINA",
              "DUBINA",
              "MAX BR.",
              "SLOBODNO",
              "ZAUZETO",
              "SKLADIŠTE",
              "ALATI",

              'EDITIRAJ',
            ],
            col_widths: [
              
              1, // "sifra",
              1, // "visina",
              1, // "dubina",
              1, // "max",
              1, // "free",
              1, // "count",
              1, // "sklad",
              6, // alati_links

              1, // 'button_edit',

            ],

            format_cols: { 
              sifra: "center",
              visina: "center",
              dubina: "center",
              max: "center",
              free: "center",
              count: "center",
              sklad: "center",
            },

            parent: "#alat_shelfs_box",
            return: {},
            show_on_click: false,

            cit_run: function(state) { console.log(state); },

            button_edit: async function(e) {

              e.stopPropagation();
              e.preventDefault();

              console.log(`kliknuo na EDIT OD POLICE ALATA`);

            },

          }; // kraj records props

          $("#alat_shelfs_box").html(``);

          create_cit_result_list(data.alat_locations.shelfs, alat_shelfs_props );

          wait_for( `$("#alat_shelfs_box .search_result_row").length > 0`, function() {
            reset_tooltips();
          }, 5*1000 );

        }; // kraj ako postoji alat shelfs length > 0
        
      } else {
        
        if ( result.msg ) popup_error(result.msg);
        if ( !result.msg ) popup_error(`Greška prilikom GET ALAT SHELFS !!!`);
      };

    })
    .fail(function (error) {
      
      console.log(error);
      popup_error(`Došlo je do greške na serveru prilikom GET ALAT SHELFS funkcije!!!`);
    })
    .always(function() {
      
      toggle_global_progress_bar(false);
      $(this_button).css("pointer-events", "all");
      
    });
    
    
    
  };
  this_module.make_alat_shelfs_list = make_alat_shelfs_list;
  
  
  function set_m2_price(elem) {
    
    var this_comp_id = $(elem).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    
    if ( 
      
        (
          (data.po_valu && data.po_kontravalu)
          ||
          (data.kontra_tok && data.tok)
        )
      
      &&
      data.osnovna_jedinica.jedinica == "kom"
      &&
      ( data.cijena || data.cijena === 0 ) 
    ) {
      
      var area = (data.po_valu && data.po_kontravalu) ? (data.po_valu * data.po_kontravalu / 1000000) : null;
      if ( !area ) area = (data.kontra_tok && data.tok) ? (data.kontra_tok * data.tok / 1000000) : null;
      
      
      if ( area ) {
        data.m2_cijena = data.cijena / area;
        $(`#` + rand_id + `_m2_cijena`).val( cit_format( data.m2_cijena, 3 ) );
      };
        
    };
    
  };
  this_module.set_m2_price = set_m2_price;
  
  
  function set_kom_price(elem) {
    
    var this_comp_id = $(elem).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    
    if ( 
      (data.po_valu && data.po_kontravalu)
      ||
      (data.kontra_tok && data.tok)
      &&
      data.osnovna_jedinica.jedinica == "kom"
      &&
      ( data.m2_cijena || data.m2_cijena === 0) 
    ) {
      
      var area = (data.po_valu && data.po_kontravalu) ? (data.po_valu * data.po_kontravalu / 1000000) : null;
      if ( !area ) area = (data.kontra_tok && data.tok) ? (data.kontra_tok * data.tok / 1000000) : null;
      
      
      if ( area ) {
        data.cijena = data.m2_cijena * area;
        $(`#` + rand_id + `_cijena`).val( cit_format( data.cijena, 3 ) );
      };
        
    };
    
  };
  this_module.set_kom_price = set_kom_price;
  
  
  function update_alat_shelfs(shelf_obj) {
    
    
    if ( !window.cit_user ) {
      popup_warn(`Niste ulogirani !!!`);
      return;
    };
    
     
    $.ajax({
      headers: {
        'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
      },
      type: "POST",
      cache: false,
      url: `/update_alat_shelfs`,
      data: JSON.stringify( {
        
        shelf: "R4",
        alat_id: "6234650d393084daf17da3e5",
        alat_sifra: "ALA168.",
        alat_old_sifra: "000856",
        
        
        time: Date.now(),
        user_id: window.cit_user._id,
        user_number: window.cit_user.user_number,
        full_name: window.cit_user.full_name,
        email: window.cit_user.email
        
        
        
      } ),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done(function (result) {
      
      console.log(result);
      
      if ( result.success == true ) {

        
        
        
        
      } else {
        if ( result.msg ) popup_error(result.msg);
        if ( !result.msg ) popup_error(`Greška prilikom  GET ALAT SHELFS !!!`);
      };

    })
    .fail(function (error) {
      
      console.log(error);
      popup_error(`Došlo je do greške na serveru prilikom GET ALAT SHELFS  funkcije!!!`);
    })
    .always(function() {
      
      toggle_global_progress_bar(false);
      $(this_button).css("pointer-events", "all");
      
    });
    
    
  };
  // kraj update_alat_shelf

  
  
  
  
  
  function scaner_start(data) {

      // This method will trigger user permissions
    Html5Qrcode.getCameras().then(devices => {
      /**
       * devices would be an array of objects of type:
       * { id: "id", label: "label" }
       */
      if (devices && devices.length) {
        
        var cameraId = devices[0].id;
        
        
        /*
        var camera_pills = ``;
        
        $.each( devices, function(c_ind, cam) {
          
          camera_pills += `<div class="camera_pill" id="camera_pill_${c_ind+1}">CAMERA ${c_ind+1}</div>`;
          
          
        })
        
        
        */
        
        // cit_play(`scan_start`); // zvuk
        
        
        window.cit_camera_scaner = new Html5Qrcode(/* element id */ "scaner_screen");
        
        
        
        window.prev_scan_texts = [];
        
        window.cit_camera_scaner.start(
          { facingMode: "environment" }, // cameraId,  -------> ovo koristim kada je na dsktopu 
          {
            aspectRatio: 1.333334,
            fps: 10,    // Optional, frame per seconds for qr code scanning
            // qrbox: { width: 250, height: 250 }  // Optional, if you want bounded box UI
          },
          (decodedText, decodedResult) => {
            
            console.log( ` ------------ SCAN USPIO ------------` );
            
            console.log( decodedText );
            
            
            if ( window.prev_scan_texts.indexOf(decodedText) > -1 ) return;
            
            window.prev_scan_texts.push(decodedText);
            
            
            cit_play(`scan_done`); // zvuk
            
            var text = $(`#text_out_box`).html();
            
            
            $(`#text_out_box`).html(text + `<br>` + decodedText );
         
            
            // console.log( decodedResult );
            
          },
          (errorMessage) => {
            // console.log( ` ------------ errorMessage ------------` );
            // console.log( errorMessage );
          })
        .catch((err) => {
          
            // console.log( ` ------------ errorMessage ------------` );
            // console.log( errorMessage );
          
        });
        
        
      }
    }).catch(err => {
      // handle err
    });  
    
    
    
    
    
    
    
  };
  this_module.scaner_start = scaner_start;
  
  
  function scaner_stop(data) {

    // cit_play(`scan_stop`);
    
    window.prev_scan_texts = [];
    $(`#text_out_box`).html(``);

    window.cit_camera_scaner.stop()
    .then((ignore) => {
      // QR Code scanning is stopped.
      console.log( ` ------------ SCAN STOP ------------` );
      console.log( ignore );
      
    }).catch((err) => {
      // Stop failed, handle it.
      console.log( ` ------------ ERROR ----- SCAN STOP ------------` );
      console.log( err );
    });

    
    
  };
  this_module.scaner_stop = scaner_stop;
    
  
  
  
  function gen_record_kalk_partner_link(record_obj, record_kalk) {
      
    var kalk_link = 
`
<a href="#project/${record_kalk.proj_sifra || null}/item/${record_kalk.item_sifra || null}/variant/${record_kalk.variant || null}/kalk/${record_kalk.kalk_sifra || null}/proces/null" 
  style="text-align: left;"
  target="_blank"
  onClick="event.stopPropagation();"> 
  ${ record_kalk.full_product_name || "NEDOSTAJE IME PROZIVODA ?" } 
</a>
<br>
<a href="#partner/${record_obj.partner_id}/status/null" 
  style="text-align: left;"
  target="_blank"
  onClick="event.stopPropagation();"> 
  ${record_obj.partner_name} 
</a>
<br>

`;
    
    
    return kalk_link;
    
  };
  this_module.gen_record_kalk_partner_link = gen_record_kalk_partner_link;
  
  
  
  
  async function get_modules() {
    
    this_module.status_module = await get_cit_module(`/modules/status/status_module.js`, `load_css`);
    this_module.product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`); 
    this_module.partner_module = await get_cit_module(`/modules/partners/partner_module.js`, `load_css`);
    
  };
  this_module.get_modules = get_modules;
  
  get_modules();
  
  this_module.cit_loaded = true;
 
} // end of module scripts
  
  
}; // kraj modula

