function make_records_list(data) {

  // ADRESE TABLICA

    
  $(`#sirov_records`).html(``);
    
  var records_za_table = [];

  if ( data.records && data.records.length > 0 ) {

    $.each(data.records, function(index, record_obj ) {

      var {

      } = record_obj;

      var status_link = ``;

      if ( sirov_id ) {
        var href = `"#sirov/${sirov_id}/status/${status}"`;
        status_link = 
`<a style="text-align: center; margin: 0 auto;" href=${href} target="_blank" onClick="event.stopPropagation();" 
  class="cit_tooltip" data-toggle="tooltip" data-placement="bottom" data-html="true" title="${full_record_name}">
  <span class="record_pill sirov">S</span>
</a>`;
      };

      if ( proj_sifra ) {
        var href = `#project/${proj_sifra}/item/${item_sifra}/variant/${variant}/status/${status}`;
        status_link = 
`<a style="text-align: center; margin: 0 auto;" href="${href}" target="_blank" onClick="event.stopPropagation();"
  class="cit_tooltip" data-toggle="tooltip" data-placement="bottom" data-html="true" title="${full_record_name}">
  <span class="record_pill proj">P</span>
</a>`;        

      };
      
      function make_pill(type) {
        
        
        function show_sifra(type) { 
        
          var doc_sifra = ``;
        
          var map_count_to_sifra =  {
            
            order_dobav: `nd_sifra`,
            in_record: `pr_sifra`,
            offer_reserv: `po_sifra`,
            order_reserv: `nk_sifra`,
            ms_kolicina: `ms_sifra`,
            utrosak: `ut_sifra`,
            skart: `sk_sifra`,
            pro_record: `pro_sifra`,
            pr_pro_record: `pr_pro_sifra`,
            out_record: `ot_sifra`,
            
          };
          
          // loop po svim propertijima  koji su brojčani
          $.each(map_count_to_sifra, function(type_key, sifra_key ) {
            // ako nađeš property koji nije null i ako je traj property trenutni property od tog zapisa
            // onda uzmi sifru dokumenta od tog type
            
            // i ako taj key postoji !!!
            if ( record_obj[ type_key ] && record_obj.type == type_key) doc_sifra = record_obj[ sifra_key ] || "NEMA ŠIFRU !!!" ;
          });
         
          return doc_sifra;
          
        };
        
        var record_types_pills = {
          order_dobav: `<span data-toggle="tooltip" data-placement="bottom" title="NARUDŽBA DOBAVLJAČU" class="cit_tooltip nd">${ show_sifra(type) }</span>`,
          in_record : `<span data-toggle="tooltip" data-placement="bottom" title="PRIMKA OD DOBAVLJAČA" class="cit_tooltip pr">${ show_sifra(type) }</span>`,
          offer_reserv : `<span data-toggle="tooltip" data-placement="bottom" title="PONUDA" class="cit_tooltip po">${ show_sifra(type) }</span>`,
          order_reserv : `<span data-toggle="tooltip" data-placement="bottom" title="NARUDŽBA KUPCA" class="cit_tooltip nk">${ show_sifra(type) }</span>`,
          ms_kolicina : `<span data-toggle="tooltip" data-placement="bottom" title="MEĐU-SKLADIŠNICA" class="cit_tooltip ms">${ show_sifra(type) }</span>`,
          utrosak : `<span data-toggle="tooltip" data-placement="bottom" title="UTROŠAK" class="cit_tooltip ut">${ show_sifra(type) }</span>`,
          skart : `<span data-toggle="tooltip" data-placement="bottom" title="ŠKART" class="cit_tooltip sk">${ show_sifra(type) }</span>`,
          pro_record : `<span data-toggle="tooltip" data-placement="bottom" title="PROIZVEDENO" class="cit_tooltip pro">${ show_sifra(type) }</span>`,
          pr_pro_record : `<span data-toggle="tooltip" data-placement="bottom" title="PRIMKA IZ PROIZVODNJE" class="cit_tooltip pr_pro">${ show_sifra(type) }</span>`,
          out_record : `<span data-toggle="tooltip" data-placement="bottom" title="OTPREMNICA" class="cit_tooltip ot">${ show_sifra(type) }</span>`,
        };
        
        return  record_types_pills[type];
        
      };
      
      
      function make_record_link(type) {
        
        var record_links = {
          order_dobav: `<a href="/docs/${nd_pdf}" target="_blank" onClick="event.stopPropagation();">${ cit_format( order_dobav,10 ) }</a>`,
          in_record : `<a href="/docs/${pr_pdf}" target="_blank" onClick="event.stopPropagation();">${  cit_format( in_record,10) }</a>`,
          offer_reserv : `<a href="/docs/${pr_pdf}" target="_blank" onClick="event.stopPropagation();">${  cit_format( offer_reserv,10) }</a>`,
          order_reserv : `<a href="/docs/${pr_pdf}" target="_blank" onClick="event.stopPropagation();">${  cit_format( order_reserv,10) }</a>`,
          ms_kolicina : `<a href="/docs/${pr_pdf}" target="_blank" onClick="event.stopPropagation();">${  cit_format( ms_kolicina,10) }</a>`,
          utrosak : `<a href="/docs/${pr_pdf}" target="_blank" onClick="event.stopPropagation();">${  cit_format( utrosak,10) }</a>`,
          skart : `<a href="/docs/${pr_pdf}" target="_blank" onClick="event.stopPropagation();">${  cit_format( skart,10) }</a>`,
          pro_record : `<a href="/docs/${pr_pdf}" target="_blank" onClick="event.stopPropagation();">${  cit_format( pro_record,10) }</a>`,
          pr_pro_record :`<a href="/docs/${pr_pdf}" target="_blank" onClick="event.stopPropagation();">${  cit_format( pr_pro_record,10) }</a>`,
          out_record : `<a href="/docs/${pr_pdf}" target="_blank" onClick="event.stopPropagation();">${  cit_format( out_record,10) }</a>`,
        };
        return  record_links[type];
        
      };
      
      
      function make_record_btn(type, sifra, parent) {
        
        var all_children = [];
        
        var make_button = true;
        
        if ( parent == null ) {
          
          $.each(data.records, function(p_ind, p_record) {
            if (p_record.parent == sifra ) all_children.push(p_record);
          });
          
          if ( all_children.length > 0 ) make_button = false;
          
          /*
          $.each( all_children, function(c_ind, c_record ) {
            if ( c_record[type] !== null ) make_button = false;
          });
          */
          
        } else {
          
          var parent_obj = null;
          
          $.each(data.records, function(f_ind, f_record) {
            if ( f_record.sifra == parent ) parent_obj = f_record;
          });
          
          if ( parent_obj[type] !== null ) make_button = false;
          
          $.each(data.records, function(p_ind, p_record) {
            if ( p_record.parent == parent_obj.sifra ) all_children.push(p_record);
          });
          
          
          // ako postoji child koji je NOVIJI od ovoga onda NEMA BUTTONA !!!!
          $.each( all_children, function(c_ind, c_record ) {
            if ( c_record.time > record_obj.time ) make_button = false;
          });
          
        };
        
        
        var record_btns = {
          
          order_dobav: `<button id="${sifra}_order_dobav_btn" class="blue_btn small_btn" style="margin: 0 auto; box-shadow: none;">ND</button>`,
          in_record: `<button id="${sifra}_in_record_btn" class="blue_btn small_btn" style="margin: 0 auto; box-shadow: none;">PR</button>`,
          offer_reserv: `<button id="${sifra}_offer_reserv_btn" class="blue_btn small_btn" style="margin: 0 auto; box-shadow: none;">PO</button>`,
          order_reserv: `<button id="${sifra}_order_reserv_btn" class="blue_btn small_btn" style="margin: 0 auto; box-shadow: none;">NK</button>`,
          ms_kolicina: `<button id="${sifra}_ms_kolicina_btn" class="blue_btn small_btn" style="margin: 0 auto; box-shadow: none;">MS</button>`,
          
          utrosak: `<button id="${sifra}_utrosak_btn" class="blue_btn small_btn" style="margin: 0 auto; box-shadow: none;">UT</button>`,
          skart: `<button id="${sifra}_skart_btn" class="blue_btn small_btn" style="margin: 0 auto; box-shadow: none;">ŠK</button>`,
          pro_record: `<button id="${sifra}_pro_record_btn" class="blue_btn small_btn" style="margin: 0 auto; box-shadow: none;">PRO</button>`,
          pr_pro_record: `<button id="${sifra}_pr_pro_record_btn" class="blue_btn small_btn" style="margin: 0 auto; box-shadow: none;">P.P.</button>`,
          out_record: `<button id="${sifra}_out_record_btn" class="blue_btn small_btn" style="margin: 0 auto; box-shadow: none;">OT</button>`,
          
        };
      
        return  make_button ? record_btns[type] : "";
        
      };
      
      
      var grana_btn = 
`
<button id="${sifra}_record_grana" class="blue_btn small_btn record_grana" style="margin: 0 auto; box-shadow: none;">
  <i class="fas fa-code-branch"></i>
</button>
`;
      
      
      var sklad_link = "";
      
      if ( skladiste ) {
        
        sklad_link = 
`<a href="/skladiste/${skladiste}/sector/${sector}/level/${level}" 
    target="_blank" onClick="event.stopPropagation();" 
    style="text-align: center !important;" >
  ${ skladiste +"-"+ sector +"-"+ level }
</a>
`;
        
      };

          
          
      

      records_za_table.push({
        
        grana_btn,

        sifra,
        type,
        type_badge: make_pill(type),
        status,

        status_link,

        nd_sifra, 
        nd_link: order_dobav ? make_record_link(`order_dobav`) : make_record_btn(`order_dobav`, sifra, parent),
        order_dobav,

        pr_sifra,
        pr_link: in_record ? make_record_link(`in_record`) : make_record_btn(`in_record`, sifra, parent),
        in_record,

        po_sifra,
        po_link: offer_reserv ? make_record_link(`offer_reserv`) : make_record_btn(`offer_reserv`, sifra, parent),
        offer_reserv,

        nk_sifra,
        nk_link: order_reserv ? make_record_link(`order_reserv`) : make_record_btn(`order_reserv`, sifra, parent),
        order_reserv,

        ms_sifra,
        ms_link: ms_kolicina ? make_record_link(`ms_kolicina`) : make_record_btn(`ms_kolicina`, sifra, parent),
        ms_kolicina,
        ms_from_to,

        ut_sifra,
        ut_link: utrosak ? make_record_link(`utrosak`) : make_record_btn(`utrosak`, sifra, parent),
        utrosak,

        sk_sifra,
        sk_link: skart ? make_record_link(`skart`) : make_record_btn(`skart`, sifra, parent),
        skart,

        pro_sifra,
        pro_link: pro_record ? make_record_link(`pro_record`) : make_record_btn(`pro_record`, sifra, parent),
        pro_record,

        pr_pro_sifra,
        pr_pro_link: pr_pro_record ? make_record_link(`pr_pro_record`) : make_record_btn(`pr_pro_record`, sifra, parent),
        pr_pro_record,

        ot_sifra,
        ot_link: out_record ? make_record_link(`out_record`) : make_record_btn(`out_record`, sifra, parent),
        out_record,

        sklad_link: sklad_link,
        time,
        sklad_count,
        count,

      });

    }); // kraj loopa svih adresa

    cit_local_list.records_za_table =  records_za_table;
    
    
  }; // kraj ako postoji records length > 0

    
  var records_props = {

    hide_sort_arrows: true,

    desc: 'samo za kreiranje tablice svih recorda  u modulu sirovine ---> u biti to je history te sirovine !!!',
    local: true,
    list: records_za_table,
    show_cols: [

      `grana_btn`,
      `type_badge`,
      `time`,
      `status_link`,
      `nd_link`,
      `pr_link`,
      `po_link`,
      `nk_link`,
      `ms_link`,
      `ut_link`,
      `sk_link`,
      `pro_link`,
      `pr_pro_link`,
      `ot_link`,
      `sklad_link`,
      `sklad_count`,
      `count`,
    ],
    custom_headers: [
      
      `GRANA`,
      
      `TIP`,
      
      `TIME`,

      `IZVOR`, // status_link

      `NARUDŽBA D.`, // nd_link

      `PRIMKA`, // pr_link

      `PONUDA`, // po_link

      `NARUDŽBA K.`, // nk_link

      `MEĐU SKL.`, // ms_link

      `UTROŠAK`, // ut_link

      `ŠKART`, // sk_link

      `PROIZ.`, // pro_link

      `PRIM. PRO`, // pr_pro_link

      `OTPREMNICA`, // ot_link

      `POZICIJA`, // `sklad_link`,

      
      `REAL.`,
      `KALK.`,
      
    ],
    format_cols: {

      type: `center`,

      sklad_link: `center`,

      time: `date_time`,
      sklad_count: 10,
      count: 10,



    },
    col_widths: [
      0.7, // grana
      1.5, // `type_badge`,
      1.5, // `time`,

      1, // `status_link`,

      1, // `nd_link`,
      
      1, // `pr_link`,
      
      1, // `po_link`,
      
      1, // `nk_link`,
      
      1, // `ms_link`,
      
      1, // `ut_link`,
      
      1, // `sk_link`,
      
      1, // `pro_link`,
      
      1, // `pr_pro_link`,
      
      1, // `ot_link`,
      
      1, // `sklad_link`,

      
      1, // `sklad_count`,
      1, // `count`,

    ],
    parent: "#sirov_records",
    return: {},
    show_on_click: false,

    cit_run: function(state) { console.log(state); },


  };
    
  if ( records_za_table.length > 0 ) {
    
    create_cit_result_list(records_za_table, records_props );
    $('.cit_tooltip').tooltip('dispose').tooltip({ boundary: 'window' });
    
    
    // wait
    
    
  };


  };
  this_module.make_records_list = make_records_list;
  