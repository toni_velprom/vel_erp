var module_object = {
create: ( data, parent_element, placement ) => {
  var this_module = window[module_url]; 
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };
  
  var { id } = data;
  
  var valid = {
    find_sirov: { element:"single_select", type:"", lock:false, visible:true, label:"Pretraga robe"},
    
    sirov_find_mode: { element:"switch", type:"bool", lock:false, visible:true, label:"Uključi detaljnu pretragu"},
    
    find_test: { element:"single_select", type:"", lock:false, visible:true, label:"Pretraga TEST"},
    
    add_sirov_filter: { element:"single_select", type:"same_width", lock:false, visible:true, label:"Dodaj filer"},
    
    kvaliteta_1: { element:"input", type:"string", lock:false, visible:true, label:"Kvaliteta kartona (P, T, B, Š ...)"},
    kvaliteta_2: { element:"input", type:"string",  lock:false, visible:true, label:"Valovi kartona (e, b, c, eb, bc ...)"},

    val_min: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Val OD"},
    val_max: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Val DO"},
    
    kontra_val_min: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Kontra Val OD"},
    kontra_val_max: { element:"input", type:"number", decimals:0, lock:false, visible:true, label:"Kontra Val DO"},
    
    promjer_min: { element:"input", type:"number", decimals:3, lock:false, visible:true, label:"Promjer OD"},
    promjer_max: { element:"input", type:"number", decimals:3, lock:false, visible:true, label:"Promjer OD"},
    
    
  };
  
  
  this_module.valid = valid;
  
  this_module.set_init_data(data);
  
  var rand_id = `sirfind`+cit_rand();
  this_module.cit_data[id].rand_id = rand_id;
  
  var component_html = null;
  
  if ( data.for_module == "sirov" ) {
    
  component_html = 
    
`

<div id="${id}" class="sirovine_search_body cit_comp">
 
  <section>
    <div class="container-fluid">
     
      <div class="row">

        <div class="col-md-5 col-sm-12" style="margin-bottom: 5px;" >
          ${ cit_comp(rand_id+`_find_sirov`, valid.find_sirov, null, "") }
        </div>
        
        <!--
        <div class="col-md-5 col-sm-12" style="margin-bottom: 5px;" >
          ${ cit_comp(rand_id+`_find_test`, valid.find_test, null, "") }
        </div>
        -->
        
        <!--
        <div class="col-md-3 col-sm-12" style="margin-bottom: 5px;" >
          ${ cit_comp(rand_id+`_add_sirov_filter`, valid.add_sirov_filter, null, "") }
        </div>
        -->
        
        <div class="col-md-3 col-sm-12" style="margin-bottom: 5px;" >
          <button class="blue_btn btn_small_text" id="show_find_sirov_table_btn" >
            <i class="far fa-search" style="margin-right: 10px;"></i>
            PRIKAŽI REZULTATE
          </button>
        </div>
        
        
        <div class="col-md-3 col-sm-12" style="margin-bottom: 5px;" >
          
          ${ cit_comp( rand_id+`_sirov_find_mode`, valid.sirov_find_mode, window.sirov_find_mode || false ) }
          
        </div>
        
        
        
      </div>
      
      <div id="last_line_sirov_find"></div>
      
      <div class="row">
        <div  class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" 
              id="show_find_sirov_table_box" style="padding-top: 0; padding-bottom: 0;">
          <!-- OVDJE IDE LISTA PRETRAGE SIROVINA tj robe -->    
        </div> 
      </div>

    </div>  
  </section>
</div>
 

`;



}
else {
  
  component_html =  
`
<div id="${id}" class="sirovine_search_body cit_comp">
  ${ cit_comp(rand_id+`_find_sirov`, valid.find_sirov, null, "") } 
</div> 
`;  
  
  
};


  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };
  
  
  var condition_for_sirov = function() {
    
    var is_ok = false;
    if ( $(`#${data.id}`).length > 0 && this_module.sirov_module.valid ) is_ok = true;
    return is_ok;
      
  };
  
  
  wait_for( condition_for_sirov, function() {
    
    console.log(data.id + 'component injected into html');
    
    var sirov_valid = this_module.sirov_module.valid;
    
    $('#'+rand_id+'_sirov_find_mode').data(`cit_run`, function(state) { 
      
      if ( window.sirov_find_mode !== state ) {
        
        window.sirov_find_mode = state;
        if ( state == false ) window.cit_find_query = null;
        if ( state == true ) window.cit_find_query = {};
        
        var sirov_data = {
          id: `sirov_module`,
        };
        this_module.sirov_module.create( sirov_data, $(`#cont_main_box`) );
        
        
      };
    
    });
    
    
    /*
      find_in: [
        "mama",
        "tata",
        "pas",
        "kravata",
      ],
    
    */
       
    
    $(`#`+rand_id+`_find_test`).data('cit_props', {
      desc: 'pretraga TEST u modulu find sirov',
      local: false,
      url: '/find_test',
      
      find_in: [
        
        "mama",
        "tata",
        "pas",
        "kravata",
        
      ],

      return: {},
      
      show_cols: [
        "mama",
        "tata",
        "pas",
        "kravata",
      ],
      
      query: {},
      
      filter: null,
      
      show_on_click: false,
      list_width: 700,
      cit_run:  function(state) { console.log(state); },
    });  

    
    
    var find_sirov_props = {
      // !!!findsirov!!!
      desc: 'pretraga sirovine u modulu find sirov',
      
      
      // ispod su propsi samo za remote find:
      local: false,
      url: '/find_sirov',
      return: { },
      find_in: [
        "sirovina_sifra", 
        "naziv",
        "full_naziv",
        "stari_naziv",
        "povezani_nazivi",
        "detaljan_opis",
        "grupa_flat",  
        
        "kvaliteta_1",
        "kvaliteta_2", 
        
        "kvaliteta_mix",
        
        "kontra_tok_x_tok",
        "val_x_kontraval",
        "alat_prirez",
        
        "dobavljac_flat", 
        "dobav_naziv", 
        "dobav_sifra", 
        "fsc_flat", 
        "povezani_kupci_flat", 
        "boja",   
        "zemlja_pod_flat",
        
        "specs_flat"
        
        
      ],
      query: {},
      
      // filter može biti i lokalno i remote
      filter: this_module.find_sirov_filter,
      
      
      /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */
      col_widths:[
        1, // "sirovina_sifra",
        2, // "full_naziv",
        2, // "full_dobavljac",
        1, // grupa_naziv
        1, // "boja",
        1, // "cijena",
        1, // "kontra_tok",
        1, // "tok",
        1, // "po_valu",
        1, // "po_kontravalu",
        2, // "povezani_nazivi",
        
        1, // "pk_kolicina",
        1, // "nk_kolicina",
        1, // "order_kolicina",
        1, // "sklad_kolicina"
        1, // last_status
        
        4, // spec_docs
      
      ],
      show_cols: [
        "sifra_link",
        "full_naziv",
        "full_dobavljac",
        "grupa_naziv",
        "boja",
        "cijena",
        "kontra_tok",
        "tok",
        "po_valu",
        "po_kontravalu",
        "povezani_nazivi",
        
        `pk_kolicina`,
        `nk_kolicina`,
        `order_kolicina`,
        `sklad_kolicina`,
        `last_status`,
        
        `spec_docs`,
        
      ],
      custom_headers: [
        `ŠIFRA`,  // "sirovina_sifra",
        `NAZIV`,  // "full_naziv",
        `DOBAV.`,  // "full_dobavljac",
        `GRUPA`,  // "grupa_naziv",
        `BOJA`,  // "boja",
        `CIJENA`,  // "cijena",
        `KONTRA TOK`,  // "kontra_tok",
        `TOK`,  // "tok",
        `VAL`,  // "po_valu",
        `KONTRA VAL`,  // "po_kontravalu",
        `POVEZANO`,  // "povezani_nazivi",
        
        `PK RESERV`,  // "pk_kolicina",
        `NK RESERV`,  // "nk_kolicina",
        `NARUČENO`,  // "order_kolicina",
        `SKLADIŠTE`,  // "sklad_kolicina",
        `STATUS`,
        
        `DOCS`, // spec_docs
        
      ],
      format_cols: {
        "sifra_link": "center",
        "grupa_naziv": "center",
        "boja": "center",
        "cijena": sirov_valid.cijena.decimals,
        "kontra_tok": sirov_valid.kontra_tok.decimals,
        "tok": sirov_valid.tok.decimals,
        "po_valu": sirov_valid.po_valu.decimals,
        "po_kontravalu": sirov_valid.po_kontravalu.decimals,
        
        "pk_kolicina": sirov_valid.pk_kolicina.decimals,
        "nk_kolicina": sirov_valid.nk_kolicina.decimals,
        "order_kolicina": sirov_valid.order_kolicina.decimals,
        "sklad_kolicina": sirov_valid.sklad_kolicina.decimals,
        
      },
      
      
      show_on_click: false,
      list_width: 700,
      
      cit_run: async function(state) {
        
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        // kada izaberem neki red onda resetiraj da NE BUDE VIŠE FIND MODE
        // kada izaberem neki red onda resetiraj da NE BUDE VIŠE FIND MODE
        // kada izaberem neki red onda resetiraj da NE BUDE VIŠE FIND MODE
        
        window.sirov_find_mode = false;
        window.cit_find_query = null;
        
        
        
        $('#'+rand_id+'_sirov_find_mode').removeClass(`on`).addClass(`off`);
        
        console.log(state);
        
        var state = cit_deep(state);
                
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        // stani ako je user obrisao sve u inputu !!!!
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          return;
        };
        
        // SAMO AKO JE FIND ZA SIROV PAGE TJ MODULE
        // SAMO AKO JE FIND ZA SIROV PAGE TJ MODULE
        // SAMO AKO JE FIND ZA SIROV PAGE TJ MODULE
        if ( data.for_module == "sirov" ) {
        
          toggle_global_progress_bar(true);

          if ( !this_module.sirov_module ) {
            toggle_global_progress_bar(false);
            return;
          };


          var hash_data = {
            sirov: state._id,
            status: null
          };

          update_hash(hash_data);

          
          var db_result = null;

          var props = {
              filter: this_module.find_sirov_filter,
              url: '/find_sirov',
              query: { _id: state._id },
              desc: `pretraga u unutar find sirov  unutar cit run tj kada user izabere red na drop listi `,
            };

          try {
            db_result = await search_database(null, props );
          } 
          catch (err) {
            console.log(err);
          };

          // ako je našao BILO ŠTO onda uzmi samo prvi član u arraju
          if ( db_result && $.isArray(db_result) && db_result.length > 0 ) {
            db_result = db_result[0];
          };


          var sirov_data = {
            ...db_result,
            id: `sirov_module`,
          };
          
          toggle_global_progress_bar(false);
          this_module.sirov_module.create( sirov_data, $(`#cont_main_box`) );

          return;
          
        }; // kraj ako je find sirov za MODUL SIROV
        
        // ako nije za modul sirov
        // pipe je funkcija od nekog drugogog modula kojem trebam predati ovaj state
        if ( data.pipe ) data.pipe( state );

      }, // kraj cit run
      
      
    };
    this_module.find_sirov_props = find_sirov_props;
    
    
    $(`#show_find_sirov_table_btn`).off('click');
    $(`#show_find_sirov_table_btn`).on('click', function() {
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var find_sirov_props_for_table = { 
        
        ...this_module.find_sirov_props,
        
        parent: `#show_find_sirov_table_box`,
        get_count: true,
        max_count: null, // resetiraj max conunt
        max_pages: null,
        
        sums: [
        `pk_kolicina`,
        `nk_kolicina`,
        `order_kolicina`,
        `sklad_kolicina`,
        ],
        
      };
      
      var search_string = $(`#` + rand_id + `_find_sirov`).val() || null;
      
      search_database( search_string, find_sirov_props_for_table).then(
        
        function( remote_result ) { 
          
          
          // ako remote query vrati [] tj prazan array
          if ( $.isArray(remote_result) && remote_result.length == 0 ) {
            create_cit_result_list( [], find_sirov_props_for_table, $(`#`+rand_id+`_find_sirov`)[0].id );   
            return;  
          };  
          
          
          // alert( remote_result.count );
          var max_pages = Math.ceil( remote_result.count / 100 );

          find_sirov_props_for_table.max_count = remote_result.count || null;
          find_sirov_props_for_table.max_pages = max_pages || null;

          create_cit_result_list( remote_result.docs, find_sirov_props_for_table, $(`#`+rand_id+`_find_sirov`)[0].id ); 

          setTimeout( function() { register_grab_to_scroll(`show_find_sirov_table_box`);  }, 200 );
            
            
        },
        function( error ) { 
          console.error(`SEARCH DATABASE ERROR ZA FIND SIROV TABLE:\n${find_sirov_props_for_table.desc}`)
          console.error( error );
        }
      );
      
    });
    
    
    
    $(`#`+rand_id+`_find_sirov`).data('cit_props', find_sirov_props);  

    
    $(`#`+rand_id+`_add_sirov_filter`).data('cit_props', {
      desc: 'odabir dodatnog querija u modulu find sirov',
      local: true,
      show_cols: [ 'naziv'],
      return: {},
      show_on_click: true,
      list: 'sirov_filter',
      cit_run: function(state) {

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
        } else {
          this_module.generate_sirov_query_inputs(state, data);
          
        };

      },
    });

    
    
  }, 500*1000 );

  return {
    html: component_html,
    id: data.id
  };

},
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};

  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
  };
  this_module.set_init_data = set_init_data;
  
  
  async function get_sirov_module() {
    
    this_module.sirov_module = await get_cit_module(`/modules/sirov/sirov_module.js`, 'load_css');
  
  };
  this_module.get_sirov_module = get_sirov_module;
  
  get_sirov_module();
  
  function find_sirov_filter(items, jQuery ) {
        
    var $ = jQuery;
    
    function cit_dt(number, date_format) {

      var date = null;

      // ako nije undefined ili null onda napravi datum od broja 
      if ( number || number === 0 ) {
        date = new Date( number );  
      } else {
        date = new Date();  
      };

      var sek = date.getSeconds();
      var min = date.getMinutes();
      var hr = date.getHours();
      var dd = date.getDate();
      var mm = date.getMonth() + 1; // January is 0!
      var yyyy = date.getFullYear();

      // ako je samo jedna znamenka
      if (dd < 10) {
        dd = '0' + dd;
      };
      // ako je samo jedna znamenka
      if (mm < 10) {
        mm = '0' + mm;
      };

      // ako je samo jedna znamenka
      if (sek < 10) {
        sek = '0' + sek;
      };


      // ako je samo jedna znamenka
      if (min < 10) {
        min = '0' + min;
      };

      // ako je samo jedna znamenka
      if (hr < 10) {
        hr = '0' + hr;
      };

      if ( !date_format || date_format == 'd.m.y' ) {
        date =  dd + '.' + mm + '.' + yyyy;
      };


      // ako nije neveden format ili ako je sa crticama
      if ( date_format == 'y_m_d' ) {
        date = yyyy + '_' + mm + '_' + dd;
      };  

      // ako nije neveden format ili ako je sa crticama
      if ( date_format == 'y-m-d' ) {
        date = yyyy + '-' + mm + '-' + dd;
      };


      time = hr + ':' + min + ':' + sek;


      return {
        date: date,
        time: time,
        date_time: date + " " + time,
      }

    };
    
    var new_items = [];
    
    var curr_time = Date.now();
    
    $.each(items, function(index, item) {

      var sjediste = null;
      var grupacija_naziv = null;
      var status_naziv = null;
      var dobavljac_kontakti = null;

      var full_dobavljac = null;
      var full_partner_name = null
      
      var full_naziv_sirovine = typeof global !== "undefined" ? global.concat_naziv_sirovine(item) : window.concat_naziv_sirovine(item);
      
      var last_status = ``;
      
      // ovo je specijalna sifra za alate !!!
      // ovo je specijalna sifra za alate !!!
      // if ( item.klasa_sirovine && item.klasa_sirovine.sifra == `KS5` ) {
      // ------------------------------------------------------------
        
        // ako ima statuse u sirovini
        if ( item.statuses && $.isArray(item.statuses) && item.statuses.length > 0 ) {
          
          var criteria = [ '!time' ];
      
          if ( typeof window == "undefined" ) {
            global.multisort( item.statuses, criteria );
          } else {
            multisort( item.statuses, criteria );
          };
          
          last_status = item.statuses[0].status_tip.naziv;
          
        };
         
      
      // ------------------------------------------------------------
      // };
      
      
      if ( 
        item.dobavljac                    &&
        item.dobavljac.adrese             &&
        $.isArray(item.dobavljac.adrese)  &&
        item.dobavljac.adrese.length > 0
      ) {

        sjediste = typeof global !== "undefined" ? global.concat_full_adresa(item.dobavljac, `VAD1` ) : window.concat_full_adresa(item.dobavljac, `VAD1`);
        
      };
      
      item.pk_kolicina = 0;
      item.nk_kolicina = 0;
      item.rn_kolicina = 0;
      item.order_kolicina = 0;
      
      
      // ako sirovina ima records
      if ( item.records && $.isArray(item.records) && item.records.length > 0 ) {
        
        $.each(item.records, function( r_ind, record ) {
      
          if ( 
            record.record_est_time >= curr_time // novije je od danasnjeg datuma ili jednako 
            &&
            !record.storno_time // nije storno !!!
            
          ) {

            item.pk_kolicina += record.pk_kolicina ? record.pk_kolicina : 0;
            item.nk_kolicina += record.nk_kolicina ? record.nk_kolicina : 0;
            item.rn_kolicina += record.rn_kolicina ? record.rn_kolicina : 0;
            item.order_kolicina = record.order_kolicina ? record.order_kolicina : 0;
          };

        });
        
      };
      
      if ( item.dobavljac ) {
        
        grupacija_naziv = item.dobavljac.grupacija ? item.dobavljac.grupacija.name || "" : "";
        status_naziv = item.dobavljac.status ? item.dobavljac.status.naziv : "";
        dobavljac_kontakti = item.dobavljac.kontakti ? item.dobavljac.kontakti : null;
        
        full_partner_name = 
          item.dobavljac.naziv + 
          ( item.dobavljac.grupacija_naziv ? ", " + item.dobavljac.grupacija_naziv : "" ) + 
          ( sjediste ? ", " + sjediste : "" );
        
        
        // prepiši populated dobavljac u jednostavniju verziju
        // prepiši populated dobavljac u jednostavniju verziju
        // prepiši populated dobavljac u jednostavniju verziju
        item.dobavljac = { 
          ...item.dobavljac,
          _id: item.dobavljac._id,
          naziv: item.dobavljac.naziv,
          kontakti: dobavljac_kontakti,
          sjediste, 
          grupacija_naziv, 
          status_naziv,
        };
        
      };
      
      
      
      var all_docs = "";
      
      if ( item.specs && $.isArray(item.specs) && item.specs.length > 0 ) {
        
        $.each(item.specs, function( spec_ind, spec_obj ) {
          
          if ( spec_obj.docs && $.isArray(spec_obj.docs) ) {

            var criteria = [ '~time' ];
            multisort( spec_obj.docs, criteria );

            $.each(spec_obj.docs, function(doc_index, doc) {
              var doc_html = 
              `
              <div class="docs_row">
                <div class="docs_date">${ cit_dt(doc.time).date_time }</div>
                <div class="docs_link">${ doc.link }</div>
              </div>
              `;

              all_docs += doc_html

            }); // kraj loopa po docs

          }; // kraj ako spec_obj ima docs
          
        });
        
      }; // kraj ako sirovina ima specs
      
      new_items.push({
        
        
        ...item,
        
        sifra_link:  `<a href="#sirov/${item._id}/status/null" target="_blank"  onClick="event.stopPropagation();"> ${ item.sirovina_sifra } </a>`,
        
        full_naziv: full_naziv_sirovine,
        full_dobavljac: full_partner_name,
        /* status_naziv: ( item.status ? (item.status.naziv || "") : "" ), */
        grupa_naziv: (item.grupa ? (item.grupa.name || "") : "" ),
        spec_docs: all_docs,
        last_status,
        
      });
      

    }); // kraj loopa po svim items

    return new_items;

  };
  this_module.find_sirov_filter = find_sirov_filter;
  
  
  function generate_sirov_query_inputs(state, data) {
    
    var rand_id = data.rand_id;
    var valid = this_module.valid;
    
    /*
    --------------------------------------------------------------------------------
    sirov_filter: [

      { "sifra": "SIRF1", "naziv": "Kvaliteta kartona (P, 2T, B, Š ...)" }, // Kvaliteta 1
      { "sifra": "SIRF2", "naziv": "Valovi/slojevi kartona (E, B, C, EB, BC ...)" }, // Kvaliteta 2
      { "sifra": "SIRF3", "naziv": "Val" },
      { "sifra": "SIRF4", "naziv": "Kontra Val" },
      { "sifra": "SIRF5", "naziv": "Dužina" },
      { "sifra": "SIRF6", "naziv": "Širina" },
      { "sifra": "SIRF7", "naziv": "Visina" },
      { "sifra": "SIRF8", "naziv": "Promjer" },

    ],
    --------------------------------------------------------------------------------
    */
    var query_input_html = ``;
    
    if ( state.sifra == 'SIRF1' && $(`#delete_kvaliteta_1`).length == 0 ) {
      
      query_input_html = 
          `
          <div class="query_input_container" >
            <div class="query_input_box">
              ${ cit_comp(rand_id+`_kvaliteta_1`, valid.kvaliteta_1, "" ) }
            </div>
            <div class="query_btn_box">
              <button id="delete_kvaliteta_1" class="blue_btn small_btn" style="margin: 21px auto 0 5px; box-shadow: none;" >
                <i class="far fa-times"></i>
              </button>
            </div>
          </div>
          `;
      
    };

    if ( state.sifra == 'SIRF2' && $(`#delete_kvaliteta_2`).length == 0 ) {
      
      query_input_html = 
        `
       <div class="query_input_container" >
         
          <div class="query_input_box">
            ${ cit_comp(rand_id+`_kvaliteta_2`, valid.kvaliteta_2, "" ) }
          </div>
          
          <div class="query_btn_box">
            <button id="delete_kvaliteta_2" class="blue_btn small_btn" style="margin: 21px auto 0 5px; box-shadow: none;" >
              <i class="far fa-times"></i>
            </button>
         </div>
         
        </div>
        `;
      
    };
    
    
    /*
    { ageMin: { $lte: 0 }, ageMax: { $gte: 2 } }
    */
    
    $("#last_line_sirov_find").before(query_input_html);
    
    
    
    /*
    ------------------------------------------------------------------------------------
    REGISTRIRAM SVE EVENTE BEZ 
    */
    
    
    $(`#`+rand_id+`_kvaliteta_1`).off(`blur`);
    $(`#`+rand_id+`_kvaliteta_1`).on(`blur`, function() {
      
      var sirov_query = $(`#`+rand_id+`_find_sirov`).data('cit_props').query;
      if ( !sirov_query.$and ) sirov_query.$and = [];
      
      if ( $(this).val() == `` ) {
        $.each( sirov_query.$and, function(q_ind, query_obj) {
          if ( query_obj.hasOwnProperty('kvaliteta_1') ) sirov_query.$and.splice(q_ind, 1);
        });
        return;
      };
      
      
      /*
      var new_value = set_input_data(
        this, // input elem
        sirov_query, // data parent
        null, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
        "dont_update" // određujem ovako jel treba napraviti update dataseta ili samo vratiti value
      );
      */
      
      // svakako obriši query pa ga onda ponovo napravi !!!!
      $.each( sirov_query.$and, function(q_ind, query_obj) {
        if ( query_obj.hasOwnProperty('kvaliteta_1') ) sirov_query.$and.splice(q_ind, 1);
      });
      
      
      // ^ ----> od početka
      // $ ----> do kraja
      // i ----> case insensitive
      sirov_query.$and.push({ kvaliteta_1: { "$regex": '^'+  $(this).val()  +'$', "$options": "mi" }  });
      
      $(`#`+rand_id+`_find_sirov`).data('cit_props').query = sirov_query;
      console.log(sirov_query);
      
      
    });
    
    
    $(`#`+rand_id+`_kvaliteta_2`).off(`blur`);
    $(`#`+rand_id+`_kvaliteta_2`).on(`blur`, function() {
      
      var sirov_query = $(`#`+rand_id+`_find_sirov`).data('cit_props').query;
      if ( !sirov_query.$and ) sirov_query.$and = [];
      
      if ( $(this).val() == `` ) {
        $.each( sirov_query.$and, function(q_ind, query_obj) {
          if ( query_obj.hasOwnProperty('kvaliteta_2') ) sirov_query.$and.splice(q_ind, 1);
        });
        return;
      };
      
      /*
      var new_value = set_input_data(
        this, // input elem
        sirov_query, // data parent
        null, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
        "dont_update" // određujem ovako jel treba napraviti update dataseta ili samo vratiti value
      );
      */
      
      
      // svakako obriši query pa ga onda ponovo napravi !!!!
      $.each( sirov_query.$and, function(q_ind, query_obj) {
        if ( query_obj.hasOwnProperty('kvaliteta_2') ) sirov_query.$and.splice(q_ind, 1);
      });
      
      // ^ ----> od početka
      // $ ----> do kraja
      // i ----> case insensitive
      sirov_query.$and.push({  kvaliteta_2: { "$regex": '^'+  $(this).val()  +'$', "$options": "mi" }   });   
      
      $(`#`+rand_id+`_find_sirov`).data('cit_props').query = sirov_query;
      console.log(sirov_query);
      
    });
    
    
    $(`#delete_kvaliteta_1`).off(`click`);
    $(`#delete_kvaliteta_1`).on(`click`, function() {
      
      var sirov_query = $(`#`+rand_id+`_find_sirov`).data('cit_props').query;
      if ( !sirov_query.$and ) sirov_query.$and = [];
      
      $.each( sirov_query.$and, function(q_ind, query_obj) {
        if ( query_obj.hasOwnProperty('kvaliteta_1') ) sirov_query.$and.splice(q_ind, 1);
      });
      
      $(this).closest(`.query_input_container`).remove();
      
      
      
    });
    
    
    $(`#delete_kvaliteta_2`).off(`click`);
    $(`#delete_kvaliteta_2`).on(`click`, function() {
      
      var sirov_query = $(`#`+rand_id+`_find_sirov`).data('cit_props').query;
      if ( !sirov_query.$and ) sirov_query.$and = [];
      
      $.each( sirov_query.$and, function(q_ind, query_obj) {
        if ( query_obj.hasOwnProperty('kvaliteta_2') ) sirov_query.$and.splice(q_ind, 1);
      });
      
      $(this).closest(`.query_input_container`).remove();
      
      
    });
    
    
  };
  this_module.generate_sirov_query_inputs = generate_sirov_query_inputs;
  
  
  this_module.cit_loaded = true;
 
} // end of module scripts
};

