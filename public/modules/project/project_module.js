var module_object = {
  
create: async ( data, parent_element, placement ) => {
  
  var this_module = window[module_url]; 
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };


  var valid = {
    
  proj_sifra: { element: "input", type: "string", lock: true, visible: true, label: "ID" },
  naziv: { element: "input", type: "string", lock: false, visible: true, label: "Naziv projekta", required: true },
  
  kupac: { element: "single_select", type: "simple", lock: false, visible: true, label: "Partner", required: true },
    
  free_kontakt:  { element: "input", type: "string", lock: false, visible: true, label: "Kontakt upita (slobodan upis)" }, 
  
  choose_order_kontakt: { element: "single_select", type: "", lock: false, visible: true, label: "Kontakt upita (odabir)" },
  choose_doc_adresa: { element: "single_select", type: "", lock: false, visible: true, label: "Adresa primatelja na dokumentu" },
  choose_dostav_adresa: { element: "single_select", type: "", lock: false, visible: true, label: "Adresa dostave" },
  
  choose_payment: { element: "single_select", type: "", lock: false, visible: true, label: "Način plaćanja" },
    
  proj_specs_komentar: { element:"input", type:"string", lock:false, visible:true, label:"Opis dokumenata"},
  proj_docs: { "element": "upload", "type": "multiple", "lock": false, "visible": true, "label": "Izaberi dokumente" },  
  
  doc_lang: { element:"single_select", type:"simple", lock:false, visible:true, label:"Jezik dokumenta" },
  doc_valuta: { element:"single_select", type:"simple", lock:false, visible:true, label: "Valuta" },
  doc_valuta_manual: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label: "Prepiši Tečaj" },  
  doc_rok_time: { element: "simple_calendar", type: "date", lock: false, visible: true, "label": "Rok dokumenta" },  
  doc_komentar: { "element": "text_area", "type": "string", "lock": false, "visible": true, "label": "Napomena vidljiva PARTNERU u dokumentu" },
  
  /* contact_new: { element: "text_area", type: "string", lock: false, visible: true, label: "Nove tvrtke i kontatki (ako nisu u bazi)" }, */
  
  referent: { element: "single_select", type: "simple", lock: true, visible: true, label: "Referent", required: true },
    
  referent_zamjena: { element: "single_select", type: "simple", lock: false, visible: true, label: "Referent Zamjena" },
    
  proj_status: { element: "single_select", type: "same_width", lock: false, visible: true, label: "Status", required: true },
  komentar: { element: "text_area", type: "string", lock: false, visible: true, label: "Vanjski komentar" },
  interni_komentar: { element: "text_area", type: "string", lock: false, visible: true, label: "Interni komentar", required: true },

  proj_add_product: { element: "single_select", type: "same_width", lock: false, visible: true, label: "Dodajte novi Proizvod" },
    
  create_time: { element: "simple_calendar", type: "date_time", lock: true, visible: true, label: "Kreirano", required: true }, 
    
  /* -----------START----------- BITNI DATUMI ----------------------- */  
  
    
    
  rok_isporuke: { element: "simple_calendar", type: "date_time", lock: false, visible: true, label: "Rok isporuke", required: true },

  potrebno_dana_proiz: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Potrebno dana za proizvodnju", required: true },
  potrebno_dana_isporuka: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Potrebno dana za isporuku", required: true },

  rok_proiz: { element: "simple_calendar", type: "date_time", lock: false, visible: true, label: "Rok proizvodnje", required: true },  
  rok_za_def: { element: "simple_calendar", type: "date_time", lock: false, visible: true, label: "Početak proiz. / ROK ZA DEF. NK", required: true },
    
  

  /* -----------END----------- BITNI DATUMI ----------------------- */
    

};
  
  
  
  this_module.valid = valid;
  
  
  var new_project = {
    
    proj_sifra: null,
    goto_item: null,
    goto_variant: null,
    goto_status: null,
    goto_kalk: null,
    goto_proces: null,
    
    naziv: null,
    referent:null,
    referent_zamjena: null,
    kupac: null,
    proj_status: null,
    komentar: null,
    interni_komentar: null,
    
    start: null,
    end: null,
    
    items: [],
    
    
    rok_isporuke: null,

    potrebno_dana_proiz: null,
    potrebno_dana_isporuka: null,

    rok_proiz: null,
    rok_za_def: null,
    
    free_kontakt: null,
    order_kontakt: null,
    doc_adresa: null,
    dostav_adresa: null,
    payment: null,
    
    
    proj_specs: [],
    
    
  };
  
  

  // TODO -----> OVO JE DUMMY DATA ZA TESTIRANJE ---- OBRISATI KASNIJE !!!!
  // var data = { ...data, ...proj_test_data };
  
  if ( !data.proj_sifra ) {
    
    data = { ...data, ...new_project };
    
    data.referent = {
      _id: window.cit_user._id,
      name: window.cit_user.name,
      user_number: window.cit_user.user_number,
      full_name: window.cit_user.full_name,
      email: window.cit_user.email,
    };
    
    
  };
  
  
  
  if ( !data.create_time ) data.create_time = Date.now();
  if ( !data.doc_valuta_manual ) data.doc_valuta_manual = null;
  if ( !data.doc_komentar ) data.doc_komentar = null;
  
  
  this_module.set_init_data(data);
  
  
  

  var { id } = data;
  var rand_id = `proj`+cit_rand();
  this_module.cit_data[id].rand_id = rand_id;
  
  
  

  var pis_find_mode_title = ``;

  if ( window.pis_find_mode ) {

    pis_find_mode_title = 
    `
    <div class="row">
      <h4 class="project_find_mode_title" >UKLJUČENA DETALJNA PRETRAGA !!!</h4>
    </div>
    `;
  };
  
  
  
  
  var component_html =
`
<div  id="${id}" class="cit_comp project_comp" 
      data-project_id="${ data._id || '' }"
      data-proj_sifra="${ data.proj_sifra ||  '' }"
      style="${ window.pis_find_mode ? `background: #f3e6e9`: `` }" >
      
      <div id="product_shortcuts_box">
      
      </div>
      
  
  <section>

    <div class="container-fluid">
     
     ${pis_find_mode_title}
     
      <div class="row">
        <div class="col-md-5 col-sm-12">
          <b>SPREMLJENO:</b> <span id="${rand_id}_user_saved">${data.user_saved?.full_name || "" }</span>
          &nbsp;&nbsp;
          <span id="${rand_id}_saved">${ data.saved ? cit_dt(data.saved).date_time : "" }</span>
        </div>
        <div class="col-md-5 col-sm-12">
          <b>EDITIRANO:</b> <span id="${rand_id}_user_edited">${data.user_edited?.full_name || "" }</span>
          &nbsp;&nbsp;
          <span id="${rand_id}_edited">${ data.edited ? cit_dt(data.edited).date_time : "" }</span>
        </div>
        <div class="col-md-2 col-sm-12">
          <button class="blue_btn btn_small_text" id="save_proj_btn" >
            SPREMI PROJEKT
          </button>
        </div>
      </div>
     
      <div class="row">
       
        <div class="col-md-2 col-sm-12">
          ${cit_comp(rand_id+`_proj_sifra`, valid.proj_sifra, data.proj_sifra)}
        </div>
        
        <div class="col-md-4 col-sm-12">
          ${cit_comp(rand_id+`_naziv`, valid.naziv, data.naziv)}
        </div>
        
        <div class="col-md-2 col-sm-12">
          ${cit_comp(rand_id+`_referent`, valid.referent, data.referent, data.referent?.full_name || "" )}
        </div>
        
        <div class="col-md-2 col-sm-12">
          ${cit_comp(rand_id+`_referent_zamjena`, valid.referent_zamjena, data.referent_zamjena, data.referent_zamjena?.full_name || "" )}
        </div>
        
        <div class="col-md-2 col-sm-12">
          ${cit_comp(rand_id+`_proj_status`, valid.proj_status, data.proj_status, data.proj_status?.naziv || "" )}
        </div>        
        
      </div>
      
      <div class="row" >
      
        <div class="col-md-3 col-sm-12">
          ${cit_comp(rand_id+`_create_time`, valid.create_time, data.create_time )}
        </div>
      
      
        <div class="col-md-9 col-sm-12">
          ${cit_comp(rand_id+`_free_kontakt`, valid.free_kontakt, data.free_kontakt )}
        </div>
      
      </div>
      
      
      <div class="row" >
        
        <div class="col-md-2 col-sm-12">
           ${cit_comp(rand_id+`_rok_isporuke`, valid.rok_isporuke, data.rok_isporuke )}
        </div>

        <div class="col-md-2 col-sm-12">
          ${cit_comp(rand_id+`_potrebno_dana_proiz`, valid.potrebno_dana_proiz, data.potrebno_dana_proiz || null )}
        </div>

        <div class="col-md-2 col-sm-12">
          ${cit_comp(rand_id+`_potrebno_dana_isporuka`, valid.potrebno_dana_isporuka, data.potrebno_dana_isporuka || null )}
        </div>

        <div class="col-md-2 col-sm-12">
           ${cit_comp(rand_id+`_rok_proiz`, valid.rok_proiz, data.rok_proiz )}
        </div>

        <div class="col-md-2 col-sm-12">
          ${cit_comp(rand_id+`_rok_za_def`, valid.rok_za_def, data.rok_za_def)}
        </div>


      </div> 
      
      
      <div class="row" style="margin-top: 30px; margin-bottom: 0;"><h4 style="font-size: 15px; margin: 14px 0 7px 30px; font-weight: 700;">KREIRAJ PONUDU ILI NARUDŽBU</h4></div> 
      
      
      
      <div class="row" style="margin-top: -5px; border-top: 5px solid #a4dafc; background: #f7f3ee; padding-top: 20px;" >
       
        <div class="col-md-2 col-sm-12">
          ${ cit_comp( data.id+`_doc_lang`, valid.doc_lang, { sifra: `hr`, naziv: `HRVATSKI` },  "HRVATSKI") }
        </div>

        <div class="col-md-1 col-sm-12">
          ${ cit_comp(data.id+`_doc_valuta`, valid.doc_valuta, { "sifra": "DEFV1", "naziv": "Hrvatska", "valuta": "HRK" },  "HRK") }
        </div>

        <div class="col-md-1 col-sm-12">
          ${ cit_comp( data.id+`_doc_valuta_manual`, valid.doc_valuta_manual, "") }
        </div>
       
        
        <div class="col-md-2 col-sm-12">
          ${ cit_comp(`${data.id}_doc_rok_time`, valid.doc_rok_time, null, "") }
        </div>
        
      </div>  
       
      <div class="row" style="background: #f7f3ee;" >
        <div class="col-md-12 col-sm-12">
          ${ cit_comp(rand_id+`_doc_komentar`, valid.doc_komentar, data.doc_komentar) }
        </div>
      </div> 
        
      <div class="row" style="
        justify-content: center; 
        background: #f7f3ee; 
        padding-bottom: 30px; 
        box-shadow: 0 0.46875rem 2.1875rem rgb(4 9 20 / 3%), 
                    0 0.9375rem 1.40625rem rgb(4 9 20 / 3%), 
                    0 0.25rem 0.53125rem rgb(4 9 20 / 5%),
                    0 0.125rem 0.1875rem rgb(4 9 20 / 3%);" >    

        <div class="col-md-3 col-sm-12">
          <button class="blue_btn btn_small_text offer_kupac_btn" style="margin: 20px auto 0px; box-shadow: none;">
            PONUDA KUPCU
          </button>
        </div>
      
        
        <div class="col-md-3 col-sm-12">
          <button class="blue_btn btn_small_text order_kupac_btn" style="margin: 20px auto 0px; box-shadow: none;">
            NARUDŽBA KUPCA
          </button>
        </div>
        
      </div>
            
      
      

      <div class="row" style="margin-top: 30px;" >

        <div class="col-md-5 col-sm-12">
          ${ cit_comp(rand_id+`_proj_specs_komentar`, valid.proj_specs_komentar, "" ) }
        </div>

        <div class="col-md-5 col-sm-12">
          ${ cit_comp(rand_id+`_proj_docs`, valid.proj_docs, "", "", `margin: 20px auto 0 0;` ) }
        </div>

        <div class="col-md-2 col-sm-12">

          <button class="blue_btn btn_small_text save_proj_specs" style="margin: 20px auto 0 0; box-shadow: none;">
            SPREMI DOKUMENTE
          </button>

          <button class="violet_btn btn_small_text update_proj_specs" style="margin: 20px auto 0 0; box-shadow: none; display: none;">
            AŽURIRAJ DOKUMENTE
          </button>

        </div>

      </div>


      <div class="row">
        <div  class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" 
              id="${rand_id}_proj_specs_box" style="padding-top: 20px;">
            <!-- OVDJE IDE LISTA SVIH proj SPECS ZA OVAJ PROIZVOD -->    
        </div> 
      </div>
      
      
      <div class="row">
        
        <div class="col-md-11 col-sm-12" id="find_partner_in_project">
          <!-- OVDJE UBACUJEM FIND PARTNER MODULE -->
        </div>

        <div class="col-md-1 col-sm-12" >
          <a  id="link_to_partner" href="#" target="_blank" onclick="event.stopPropagation();"
              class="blue_btn btn_small_text small_btn_link_left" 
              style="box-shadow: none; margin: 22px auto 0 0; width: 28px; height: 28px;" >

            <i style="font-size: 18px;" class="far fa-external-link-square-alt"></i>

          </a>
        </div>


      </div>
      
      <div class="row" style="padding: 0 15px;">
        <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="svi_kontakti_box" style="padding: 2px;" >
          
          <!-- OVDJE JE POPIS SVIH KONTAKTA OD OVE TVRTKE ID JE ISTI KAO NA PAGE-u OD PARTNERA -->
          <!-- ali poštto nikada te dvije stranice nisu otvorane u isto vrijeme onda je ok -->
          
        </div>
      </div>
      
      <!--
      <div class="row">
        <div class="col-md-12 col-sm-12">
          ${ "" /* cit_comp(rand_id+`_contact_new`, valid.contact_new, data.contact_new ) */ }
        </div>
      </div>
      -->
      
      <div class="row">
        <div class="col-md-12 col-sm-12">
          ${cit_comp(rand_id+`_choose_order_kontakt`,
                     valid.choose_order_kontakt,
                     data.order_kontakt,
                     data.order_kontakt?.full_adresa ? ( data.order_kontakt?.kontakt + " --- " + data.order_kontakt?.full_adresa ) : ""
                    )}
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-12 col-sm-12">
          ${cit_comp(rand_id+`_choose_doc_adresa`,
                     valid.choose_doc_adresa,
                     data.doc_adresa,
                     data.doc_adresa?.full_adresa ? ( data.doc_adresa?.kontakt + " --- " + data.doc_adresa?.full_adresa ) : ""
                    )}
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          ${cit_comp(rand_id+`_choose_dostav_adresa`,
                     valid.choose_dostav_adresa,
                     data.dostav_adresa,
                     data.dostav_adresa?.full_adresa ? ( data.dostav_adresa?.kontakt + " --- " + data.dostav_adresa?.full_adresa ) : ""
                    )}
        </div>
      </div>  
      
      
      <div class="row">
        <div class="col-md-12 col-sm-12">
          ${cit_comp(rand_id+`_choose_payment`,
                     valid.choose_payment,
                     null, /* ne upisujem ništa u input polje jer je prekomplicirano i često ne stane u jedan red */
                     "" /* ne upisujem ništa u input polje jer je prekomplicirano i često ne stane u jedan red */
                    )}
        </div>
      </div> 
      
      <div class="row" style="margin-top: 20px;" > 
        <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="payment_box" style="padding-top: 0;">
          <!-- OVDJE IDE SAMO JEDNO PLAĆANJE SAMO ZA OVAJ PROJEKT -->
        </div>
      </div>

      
      
      <div class="row">
       
        <div class="col-md-6 col-sm-12">
          ${cit_comp(rand_id+`_komentar`, valid.komentar, data.komentar)}
        </div>
        
        <div class="col-md-6 col-sm-12">
          ${cit_comp(rand_id+`_interni_komentar`, valid.interni_komentar, data.interni_komentar)}
        </div>
        
      </div>      
      
 
      
    </div>
    
  </section>
  
  <section style="margin-top: 20px; margin-bottom: 20px;">
   
    <h6 style="margin-left: 20px;">STAVKE PROJEKTA:</h6>
   
    <div class="container-fluid">
     
      <div class="row">
        <div class="col-md-3 col-sm-12">
        ${cit_comp(rand_id+`_proj_add_product`, valid.proj_add_product , data.proj_add_product, "" )}
        </div>
      </div>   
    
    </div>
    
  </section>        


  
<div class="row" id="proj_stavke_line"></div>  
      
</div>



`;

  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };
  
  wait_for( `$('#${data.id}').length > 0`, async function() {
    
    
    ask_before_close_register_event();
    
    
    this_module.partner_module = await get_cit_module(`/modules/partners/partner_module.js`, `load_css`);
    this_module.product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
    this_module.kalk_module = await get_cit_module(`/modules/kalk/kalk_module.js`, `load_css`);

    
    var rand_id = this_module.cit_data[data.id].rand_id;
    
    
    
    // ----------------------------------------------------------------------
    var find_partner = await get_cit_module(`/modules/partners/find_partner.js`, null);
    
    if ( !find_partner ) {
      toggle_global_progress_bar(false);
      return;
    };
    
    
    var find_partner_data = {
      id: rand_id+`_find_partner`,
      for_module: "project",
      pipe: this_module.make_kontakti_in_project,
    };
    find_partner.create( find_partner_data, $(`#find_partner_in_project`) );    

    
    wait_for(`$("#find_partner_in_project").find(".single_select").length > 0`, function() {
      this_module.make_kontakti_in_project(data.kupac);
    }, 5*1000);
    
    
    // ----------------------------------------------------------------------
    
    if ( data.naziv ) {
      document.title = data.proj_sifra + ". PROJEKT " + data.naziv;
    } else {
      document.title = "PROJEKTI VELPROM";  
    };
    
    
    
    $(`#cit_page_title h3`).text(`Projekti`);
    $('.cit_tooltip').tooltip();
    $(`html`)[0].scrollTop = 0;
    

    $(`#${data.id} .cit_input.number`).off(`blur`);
    $(`#${data.id} .cit_input.number`).on(`blur`, function() {
      
      var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id = data.rand_id;
      
      
      if ( window.pis_find_mode ) {
        this_module.gen_find_query_number(this);
        return;
      }; // kraj jel FIND MODE
      
      
      set_input_data(this, data);
      
      this_module.update_rokove(this);
      
    });
    
    $(`#${data.id} .cit_input.string`).off(`blur`);
    $(`#${data.id} .cit_input.string`).on(`blur`, function() {
      
      
      var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id = data.rand_id;
      
      set_input_data(this, data);
    });
    
    $(`#${data.id} .cit_text_area`).off(`blur`);
    $(`#${data.id} .cit_text_area`).on(`blur`, function() {
      
      var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id = data.rand_id;
      
      set_input_data(this, data);
    });    
    
    $(`#save_proj_btn`).off(`click`);
    $(`#save_proj_btn`).on(`click`, this_module.save_proj );
    
   
    $('#'+rand_id+'_referent').data('cit_props', {
      desc: 'za odabir prodaja referenta u modulu UPITI tj projekti',
      local: false,
      url: '/search_kal_users',
      find_in: [ 'email', 'full_name', 'name' ],
      return: { _id: 1, user_number: 1, email: 1, full_name: 1, name: 1 },
      col_widths: [ 2, 3, 3, 2 ],
      show_cols: ['user_number', 'email', 'full_name', 'name' ],
      query: {},
      show_on_click: false,
      list_width: 700,
      cit_run: this_module.choose_referent,
    });
    
    
    $('#'+rand_id+'_referent_zamjena').data('cit_props', {
      desc: 'za odabir prodaja _referent_zamjena u modulu PROJEKTI ',
      local: false,
      url: '/search_kal_users',
      find_in: [ 'email', 'full_name', 'name' ],
      return: { _id: 1, user_number: 1, email: 1, full_name: 1, name: 1 },
      col_widths: [ 2, 3, 3, 2 ],
      show_cols: ['user_number', 'email', 'full_name', 'name' ],
      query: {},
      show_on_click: false,
      list_width: 700,
      cit_run: this_module.choose_referent_zamjena,
    });     
    
    
    $('#'+rand_id+'_proj_status').data('cit_props', {
      desc: 'unutar project modula za odabir statusa projekta',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'project_status',
      cit_run: this_module.choose_proj_status,
    });
    
    
    $('#'+rand_id+'_proj_add_product').data('cit_props', {
      desc: 'unutar project modula za dodavanje novog tipa proizvoda u projekt ' + rand_id,
      local: true,
      /*col_widths: [70, 15, 15],*/
      show_cols: ['naziv'], // 'button_edit', 'button_delete'
      return: {},
      show_on_click: true,
      list: 'tip_proizvoda',
      cit_run: this_module.add_new_product,
    });    
    
    
    $('#'+rand_id+'_create_time').data(`cit_run`, this_module.update_create_time );
    
    refresh_simple_cal( $('#'+rand_id+`_create_time`) );
    
    
    $('#'+rand_id+'_rok_isporuke').data(`cit_run`, this_module.update_rok_isporuke );
    $('#'+rand_id+'_rok_proiz').data(`cit_run`, this_module.update_rok_proiz );
    $('#'+rand_id+'_rok_za_def').data(`cit_run`, this_module.update_rok_za_def );

    
    $(`#`+rand_id+`_choose_order_kontakt`).data('cit_props', {
      desc: 'odabir tko je zapravo tražio upit od osoba u partnerskoj firmi',
      local: true,
      show_cols: [`kontakt`, `pozicija_kontakta`, `full_adresa`, ],
      return: {},
      show_on_click: true,
      
      list: function() { 
        
        return $(`#svi_kontakti_box`).data(`cit_result`) || [];
        /*
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        return data.specs;
        
        */
      },
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        console.log(state);
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.order_kontakt = null;
        } else {
          
          $('#'+current_input_id).val( state.kontakt + ` --- ` + state.full_adresa );
          data.order_kontakt = state;
          
        };
        
      },
    });  
    
    $(`#`+rand_id+`_choose_doc_adresa`).data('cit_props', {
      desc: 'odabir adrese za dokumente tj glavna adresa na koju idu računi',
      local: true,
      show_cols: [`kontakt`, `pozicija_kontakta`, `full_adresa`, ],
      return: {},
      show_on_click: true,
      
      list: function() { 
        
        return $(`#svi_kontakti_box`).data(`cit_result`) || [];
        /*
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        return data.specs;
        
        */
      },
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        console.log(state);
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.doc_adresa = null;
        } else {
          
          $('#'+current_input_id).val( state.kontakt + ` --- ` + state.full_adresa );
          data.doc_adresa = state;
          
        };
        
      },
      
      
    });  
    
    $(`#`+rand_id+`_choose_dostav_adresa`).data('cit_props', {
      desc: 'odabir adrese dostave za sve buduće dokumente',
      local: true,
      show_cols: [`kontakt`, `pozicija_kontakta`, `full_adresa`, ],
      return: {},
      show_on_click: true,
      
      list: function() { 
        
        return $(`#svi_kontakti_box`).data(`cit_result`) || [];
        /*
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        return data.specs;
        
        */
      },
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        console.log(state);
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.dostav_adresa = null;
        } else {
          
          $('#'+current_input_id).val( state.kontakt + ` --- ` + state.full_adresa );
          data.dostav_adresa = state;
          
        };
        
      },
      
      
    });  
    
    $(`#`+rand_id+`_choose_payment`).data('cit_props', {
      
      desc: 'DROP LISTA ZA ODABIR PLAĆANJA UNUTAR PROJEKTA ' + rand_id,
      local: true,
      show_cols: [`time`, `def_rows` ],
      col_widths: [
        1, // 'time',
        10, // 'def',
      ],
       format_cols: { 
        time: "date_time",
      },
      return: {},
      show_on_click: true,
      
      list: function() { 
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        if ( data.kupac && data.kupac.placanje_kupac?.length > 0 ) {

          $.each(data.placanje_kupac, function(p_ind, pay) {

            var def_rows = ``;
            $.each(pay.def, function(def_ind, def) {

              def_rows += `
              <div class="def">
                <div style="width: 10%; text-align: right;">${ cit_format( def.perc, 2) }%</div>
                <div style="width: 35%; text-align: center;">${def.type_naziv}</div>
                <div style="width: 35%;  text-align: center;">${def.event_naziv}</div>
                <div style="width: 20%;  text-align: center;">VALUTA ${def.valuta} DANA</div>
              </div>
              `;

            });

            items[p_ind].def_rows = def_rows;

          });
          
        };
        
        return data.kupac.placanje_kupac;
        
      },
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        console.log(state);
        
        if ( state == null ) {
          data.payment = null;
        } else {
          data.payment = state;
        };
        
        $('#'+current_input_id).val(``);
        this_module.project_payment_list(data);
        
      },
      
      
    });  
    
    
    
    $(`#`+data.id+`_doc_lang`).data('cit_props', {
      desc: 'odabir jezika za generiranje ponude u module kalklulacija offer',
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: 'lang',
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.doc_lang = null;
        } else {
          $('#'+current_input_id).val(state.naziv);
          data.doc_lang = state;
        };
        
      },
    });  
    
    $(`#`+data.id+`_doc_valuta`).data('cit_props', {
      desc: 'odabir valute u kojoj će e generirati dokument sa cijenama u toj valuti unutar modula KALK OFFER :)',
      local: true,
      show_cols: ['naziv', 'valuta'],
      return: {},
      show_on_click: true,
      list: 'valute',
      cit_run: function(state) {
       
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.doc_valuta = null;
        } else {
          $('#'+current_input_id).val(state.valuta);
          data.doc_valuta = state;
        };
        
      },
    });
    
    $(`#${data.id}_doc_rok_time`).data(`cit_run`,  function (state, this_input) {

      var this_comp_id = this_input.closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      if ( state == null || this_input.val() == ``) {
        this_input.val(``);
        data.doc_rok_time = null;
        console.log(`doc_rok_time: `, null );
        return;
      };

      data.doc_rok_time = state;
      console.log( `new doc_rok_time: `, new Date(state) );

    });
    
    
    $(`#${data.id} .offer_kupac_btn`).off(`click`);
    $(`#${data.id} .offer_kupac_btn`).on(`click`, this_module.offer_kupac );
    
    
    $(`#${data.id} .order_kupac_btn`).off(`click`);
    $(`#${data.id} .order_kupac_btn`).on(`click`, this_module.order_kupac );
    
    
    $(`#${data.id} .save_proj_specs`).off('click');
    $(`#${data.id} .save_proj_specs`).on('click', this_module.save_current_proj_specs );
        
    $(`#${data.id} .update_proj_specs`).off('click');
    $(`#${data.id} .update_proj_specs`).on('click', this_module.save_current_proj_specs );
    
    
    $('#'+rand_id+'_proj_docs').data(`this_module`, this_module );
    $('#'+rand_id+'_proj_docs').data(`cit_run`, this_module.update_proj_docs );
    
    
    
    
    this_module.make_proj_specs_list(data);
    this_module.project_payment_list(data);
    this_module.make_product_shortcuts(data);
    
    var created_items_html = ``;
    
    if ( data.items && $.isArray(data.items) && data.items.length > 0 ) {
      
      $.each( data.items, function(item_ind, item) {
        
        var id = `prod`+cit_rand(); 
        this_module.cit_data[data.id].items[item_ind].id = id;
        
        var product_data = {
          ...item,
          full_kupac_name: this_module.gen_full_kupac_name(data.kupac) || null,
          id: id,
          // ovdje prenosim dalje goto propse koje sam dobio od router ------> project_module
          goto_item: data.goto_item,
          goto_variant: data.goto_variant,
          goto_status: data.goto_status,
          goto_kalk: data.goto_kalk,
          goto_proces: data.goto_proces,
          
          doc_adresa: data.doc_adresa || null,
          dostav_adresa: data.dostav_adresa || null,
        };
        
        
        
        // this_module.product_module.create( product_data, $(`#proj_stavke_line`), `before`);
        
        created_items_html += this_module.product_module.create(product_data).html;
        
      });
      
      $(`#proj_stavke_line`).before(created_items_html); 
      console.log(` --------------- Svi producti su kreirani --------------- `);
      
    };
    
    /*
    $('#customers_izaberi_park').data('we_auto', {
      desc: 'za odabir parkinga u customers modulu - za tablicu actions',
      local: false,
      url: '/search_park_places',
      return: { pp_sifra: 1, pp_name: 1, pp_address: 1, _id: 1, pp_current_price: 1 },
      hide_cols: ['_id', 'pp_current_price'],
      query: {},
      show_on_click: true,
      // parent: '#big_stats_table_box',
      // list: 'proba_find',
    });
        
    $('#pp_choose_owner').data('we_auto', {
      desc: 'pretraga usera za odabir ownera',
      local: false,
      url: '/search_customers',
      find_in: ['email', 'name', 'user_address', 'user_tel' ],
      col_widths: [0, 20, 20, 20, 10, 15, 15],
      return: { _id: 1, user_number: 1, email: 1, name: 1, user_tel: 1, full_name: 1, business_name: 1 },
      hide_cols: ['_id' ],
      query: {},
      show_on_click: false,
      list_width: 1000,
    });
    */
    
    
    console.log(data.id + ' component injected into html');
    
    //if ( STORE.find(data.id, `all`) == null ) STORE.make(data.id, data);
    
    /*
    var on_change = function() { 
      console.log(`promijeno sam data unutar ${data.id} `);
      
      // this_module.create( new_data, $(`#cit_root`) );
      
      this_module.data_to_html(STORE.find(data.id, 'all' ));
    };
    */
    
    // var component_listener_id = store_listener( data.id, 'all', on_change, data.id);

    /*    
    $(`#${data.id} input`).off('keyup');
    $(`#${data.id} input`).on('keyup', function() {
      var prop = $(this).attr('data-prop');
      var input_value = $(this).val();
      STORE.update(data.id, prop, input_value, `promjena input value ${prop} unutar #${data.id} componente` );
    });    */
    
  }, 500*1000 );

  return {
    html: component_html,
    id: data.id
  };

},
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};
  
  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
    
  };
  this_module.set_init_data = set_init_data;
  
  
  function data_to_html(data) {
    $.each(data, function(prop, value) {
      var elem_to_update = $(`#${data.id}_${prop}_label`);
      if ( elem_to_update.length > 0 && elem_to_update.html() !== value ) {
        elem_to_update.html(value);
      };
    });
  };
  this_module.data_to_html = data_to_html;
  
  
  function choose_proj_status(state) {
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    
    if ( state == null ) {
      data.proj_status = null;
      $('#'+current_input_id).val("");
      return;
    };    
    
    $('#'+current_input_id).val(state.naziv);
    
    
    // console.log(this_comp_id);
    // console.log(this_module.cit_data[this_comp_id].status);
   
    data.proj_status = state;
    
    // samo provjeravam:
    console.log(data.proj_status);
    
  };
  this_module.choose_proj_status = choose_proj_status;
  
  
  function choose_referent(state) {
    
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id = data.rand_id;
    
    
    if ( window.pis_find_mode ) {
      gen_find_query_select( state || null, "referent._id", state?._id || null );
      return;
    };
    
    
    if ( state == null ) {
      data.referent = null;
      $('#'+current_input_id).val("");
      return;
    };
    
    $('#'+current_input_id).val(state.full_name);
    data.referent = state;
    
    console.log(data);
    
  };
  this_module.choose_referent = choose_referent;
    

  function choose_referent_zamjena(state) {
    
    
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id = data.rand_id;
    
    
    
    if ( window.pis_find_mode ) {
      gen_find_query_select( state || null, "referent_zamjena._id", state?._id || null );
      return;
    };
    
    
    if ( state == null ) {
      data.referent_zamjena = null;
      $('#'+current_input_id).val("");
      return;
    };
    
    $('#'+current_input_id).val(state.full_name);
    data.referent_zamjena = state;
    
    console.log(data);
    
  };
  this_module.choose_referent_zamjena = choose_referent_zamjena;

  
  function add_new_product(state) {
    
    if ( window.pis_find_mode ) {
      gen_find_query_select( state || null, "tip.sifra", state?.sifra || null, "Product" );
      return;
    };
    
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    
    /*
    if ( !data._id ) {
      popup_warn(`Za otvaranje novog proizvoda morate prvo spremiti projekt !!!`);
      return;
    };
    */
    
    
    if ( state == null ) {
      $('#'+current_input_id).val(``);
      return;
    };    
    
    $('#'+current_input_id).val(state.naziv);

    
    var product_data = {
      tip: state,
      id: `prod`+cit_rand(),

      project_id: data._id || null,
      proj_sifra: data.proj_sifra,
      item_sifra: null,
      variant: null,
      
      rok_isporuke: data.rok_isporuke || null,
      potrebno_dana_proiz: data.potrebno_dana_proiz || null,
      potrebno_dana_isporuka: data.potrebno_dana_isporuka || null,
      rok_proiz: data.rok_proiz || null,
      rok_za_def: data.rok_za_def || null,
      

    };
    this_module.product_module.create( product_data, $(`#proj_stavke_line`), `before`);
    
  };
  this_module.add_new_product = add_new_product;  

  
  function make_kontakti_in_project(state) {
   
    $("#link_to_partner").attr(`href`, `#`);
    
    console.log(state);

    var this_comp_id = $(`#find_partner_in_project`).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    if ( state == null ) {
      data.kupac = null;
      $("#find_partner_in_project").find(".single_select").val("");
      $(`#svi_kontakti_box`).html("");
      return;
    };

    var full_kupac = this_module.gen_full_kupac_name(state);
    
    $("#find_partner_in_project").find(".single_select").val(full_kupac);

    /*
    
    data.kupac = {
      _id: state._id,
      naziv: state.naziv,
      sjediste: state.sjediste,
      grupacija_naziv:  state.grupacija_naziv,
      status_naziv: state.status_naziv
    };
    
    */

    var adresa_sjedista = concat_full_adresa( state, `VAD1` );
    
    if ( !adresa_sjedista )  {
      popup_warn(`Izabrani partner nema definirano sjedište!!!<br>Potrebno je otvoriti partnera u posebnom prozoru te zatim kreirati adresu sjedišta i barem jedan kontakt na tom sjedištu.`);
      return;
    };
    
    data.kupac = cit_deep(state);
    
    console.log(data);
    
    
    $("#link_to_partner").attr(`href`, `#partner/${state._id}/status/null`)

    // na silu obriši sve prethodne kontakte (samo HTML)
    $(`#svi_kontakti_box`).html("");

    this_module.partner_module.make_kontakt_list(state);

    // čekaj da se pojave gumbi u listi kontakata za edit i delete
    // pa ih onda obriši :)
    // jer u projektima nema smisla editirati kontakte partnera !!!!
    wait_for(`$("#svi_kontakti_box .result_row_edit_btn").length > 0`, function() {
      
      $("#svi_kontakti_box .result_row_edit_btn").remove();
      $("#svi_kontakti_box .result_row_delete_btn").remove();
      
      
      
    }, 5*1000); 


    
  };
  this_module.make_kontakti_in_project = make_kontakti_in_project;
  

  function save_proj() {
    
    var this_comp_id = $('#save_proj_btn').closest('.cit_comp')[0].id; // useo sam ovaj gumb bezveze  - nije bitno koji id
    var proj_data = this_module.cit_data[this_comp_id];
    
    
    $(`#save_proj_btn`).css("pointer-events", "none");
    toggle_global_progress_bar(true);
    
    // samo ako je start PRAZAN ONDA GA UPIŠI !!!!!
    if ( !proj_data.start  ) proj_data.start = Date.now();
    
    var project_copy = cit_deep(proj_data);
    
    
    // ITEMS SU ZAPRAVO PRODUCTS !!!!
    // ITEMS SU ZAPRAVO PRODUCTS !!!!
    // ITEMS SU ZAPRAVO PRODUCTS !!!!
    
    var just_item_ids = [];
    $.each( project_copy.items, function( item_index, item ) {
      if (item._id) just_item_ids.push(item._id);
    });
    
    // ITEMS SU ZAPRAVO PRODUCTS !!!!
    // ITEMS SU ZAPRAVO PRODUCTS !!!!
    // ITEMS SU ZAPRAVO PRODUCTS !!!!
    
    project_copy.items = just_item_ids;
    
    $.each( project_copy, function( key, value ) {
      // ako je objekt ili array onda ga flataj
      if ( $.isPlainObject(value) || $.isArray(value) ) {
        proj_data[key+'_flat'] = JSON.stringify(value);
      };
    });
    
    project_copy = save_metadata( project_copy );
    
    var required = cit_required( this_module.valid, project_copy );
    
    
    if ( 
      $("#find_partner_in_project input").val() == ""
      ||
      project_copy.kupac?.length == 0
    ) {
      $("#find_partner_in_project input").addClass(`missing_data`);
    };
    
    if ( required.counter > 0 ) {
      
      popup_warn(`Niste upisali ove obavezne podatke:<br>${required.podaci}`);
      toggle_global_progress_bar(false);
      $(`#save_proj_btn`).css("pointer-events", "all");
      return;
      
    };
    
    project_copy.kupac_flat = JSON.stringify( project_copy.kupac );
    
    // stavi da je kupac samo _id a ne cijeli objekt
    project_copy.kupac = project_copy.kupac._id;
    
    // return;
    
    $.ajax({
      headers: {
        'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
      },
      type: "POST",
      cache: false,
      url: `/save_proj`,
      data: JSON.stringify( project_copy ),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done(function (result) {
      
      console.log(result);
      if ( result.success == true ) {

        this_module.cit_data[this_comp_id]._id = result.proj._id;
        this_module.cit_data[this_comp_id].proj_sifra = (result.proj.proj_sifra || null);
        var rand_id = this_module.cit_data[this_comp_id].rand_id;
        $(`#`+rand_id+`_proj_sifra`).val( this_module.cit_data[this_comp_id].proj_sifra );
        
        // dodaj novonastali _id u html tj u data attribute
        var project_element = $('#save_proj_btn').closest('.cit_comp'); 
        
        project_element.attr('data-project_id', result.proj._id );
        project_element.attr('data-proj_sifra', result.proj.proj_sifra );
        
        cit_toast(`PODACI SU SPREMLJENI!`);

      } else {
        if ( result.msg ) popup_error(result.msg);
        if ( !result.msg ) popup_error(`Greška prilikom spremanja!`);
      };

    })
    .fail(function (error) {
      
      console.log(error);
      popup_error(`Došlo je do greške na serveru prilikom spremanja!`);
    })
    .always(function() {
      
      toggle_global_progress_bar(false);
      $(`#save_proj_btn`).css("pointer-events", "all");
      
    });
    
    
  };
  this_module.save_proj = save_proj;
  
  
  function gen_full_kupac_name(state) {
    
    var full_kupac_naziv = 
        state.naziv + 
        ( state.grupacija_naziv ? (", " + state.grupacija_naziv) : "" ) + 
        ( state.sjediste ? ", " + state.sjediste : "" );

    return full_kupac_naziv;
    
  };
  this_module.gen_full_kupac_name = gen_full_kupac_name;


  function update_rok_proiz( state, this_input ) {
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      data.rok_proiz = null;
      console.log(`update_rok_proiz: `, null );
      return;
    };
    
    data.rok_proiz = state;
    console.log("update_rok_proiz = ", new Date(state));
    
    this_module.update_rokove( this_input[0] );
    
    
  };
  this_module.update_rok_proiz = update_rok_proiz;  
  
    
  function update_create_time(state, this_input ) {
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      data.create_time = null;
      console.log(`update_create_time: `, null );
      return;
    };
    
    data.create_time = state;
    console.log("update_create_time = ", new Date(state));
    
  };
  this_module.update_create_time = update_create_time;
    
  
  
  function update_rok_isporuke(state, this_input ) {
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      data.rok_isporuke = null;
      console.log(`update_rok_isporuke: `, null );
      return;
    };
    
    data.rok_isporuke = state;
    console.log("update_rok_isporuke = ", new Date(state));
    
    this_module.update_rokove(this_input[0]);
    
  };
  this_module.update_rok_isporuke = update_rok_isporuke;
  
  
  function update_rok_za_def(state, this_input ) {
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      data.rok_za_def = null;
      console.log(`rok_za_def: `, null );
      return;
    };
    
    data.rok_za_def = state;
    console.log("rok_za_def = ", new Date(state));
    
  };
  this_module.update_rok_za_def = update_rok_za_def;
  
  
  function update_rokove(elem) {
    
    var this_comp_id = $(elem).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    if ( 
      elem.id.indexOf(`_potrebno_dana_proiz`) > -1 
      ||
      elem.id.indexOf(`_potrebno_dana_isporuka`) > -1
    ) {

      if ( data.rok_isporuke !== null ) {

        
        var rok_proiz = work_time_reverse( data.rok_isporuke, data.potrebno_dana_isporuka );
        var rok_za_def = work_time_reverse( data.rok_isporuke, (data.potrebno_dana_proiz || 0) + (data.potrebno_dana_isporuka || 0) );
        
        
        $('#'+rand_id+`_rok_proiz`).val( cit_dt(rok_proiz).date_time );
        data.rok_proiz = Number(rok_proiz);
        
        $('#'+rand_id+`_rok_za_def`).val( cit_dt(rok_za_def).date_time );
        data.rok_za_def = Number(rok_za_def);

      } else {

        $('#'+rand_id+`_rok_proiz`).val("");
        data.rok_proiz = null;
        
        $('#'+rand_id+`_rok_za_def`).val("");
        data.rok_za_def = null;

      };
      

    };
    
    
    if ( 
      elem.id.indexOf(`_rok_isporuke`) > -1 
    ) {

      if ( 
        data.potrebno_dana_proiz    !== null     &&
        data.potrebno_dana_isporuka !== null
        
      ) {

        var rok_za_def = work_time_reverse( data.rok_isporuke, (data.potrebno_dana_proiz || 0) + (data.potrebno_dana_isporuka || 0) );
        
        var rok_proiz = work_time_reverse( data.rok_isporuke, data.potrebno_dana_isporuka );
        
        $('#'+rand_id+`_rok_proiz`).val( cit_dt(rok_proiz).date_time );
        data.rok_proiz = Number(rok_proiz);
        
        $('#'+rand_id+`_rok_za_def`).val( cit_dt(rok_za_def).date_time );
        data.rok_za_def = Number(rok_za_def);

      } else {

        $('#'+rand_id+`_rok_proiz`).val("");
        data.rok_proiz = null;
        
        $('#'+rand_id+`_rok_za_def`).val("");
        data.rok_za_def = null;

      };

    };
    
    
    
    
    
    if ( 
      elem.id.indexOf(`_rok_proiz`) > -1   &&
      
      data.potrebno_dana_proiz    !== null &&
      data.potrebno_dana_isporuka !== null

    ) {

      
      var rok_isporuke = work_time_forward( data.rok_proiz, data.potrebno_dana_isporuka );
        
      var rok_za_def = work_time_reverse( data.rok_proiz, data.potrebno_dana_proiz );
      

      $('#'+rand_id+`_rok_isporuke`).val( cit_dt(rok_isporuke).date_time );
      data.rok_isporuke = Number(rok_isporuke);
      
      $('#'+rand_id+`_rok_za_def`).val( cit_dt(rok_za_def).date_time );
      data.rok_za_def = Number(rok_za_def);
      
    };
    
    
    
  };
  this_module.update_rokove = update_rokove;
  
  
  function project_payment_list(data) {
    
    var payment_props = {
      
      desc: 'samo za jedno odabrano plaćanje za ovaj projekt list',
      local: true,
      
      list: data.payment ? [data.payment] : [],
      
      show_cols: ['time', 'def_rows', 'button_delete' ],
      col_widths: [
        1.5, // 'time',
        10, // 'def_rows',
        1, // 'button_delete'
      ],
      
       format_cols: { 
        time: "date_time",
      },
      parent: "#payment_box",
      return: {},
      show_on_click: false,
      
      cit_run: function(state) { console.log(state); },
     
      button_delete: async function(e) {
        
        e.stopPropagation();
        e.preventDefault();
        
      
        var this_button = this;
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          
          data.payment = delete_item(data.payment, sifra , `sifra` );
          
          $(`#`+parent_row_id).remove();

        };
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj zapis plaćanja?`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj zapis plaćanja?`, null, 'yes/no', delete_yes, delete_no, null);
         
        
      },
      
    };
    
    
    if ( data.payment ) {
      create_cit_result_list( [data.payment], payment_props );
    }
    else {
      $("#payment_box").html(``);
    };
    
  };
  this_module.project_payment_list = project_payment_list;
  
  
  function make_product_shortcuts(data) {
    
    var product_shortcuts = `
      <div  class="product_shortcut cit_tooltip" 
              data-toggle="tooltip" data-placement="right" data-html="true" title="Skupi sve"
              id="products_compres" style="top: 100px"><i class="fas fa-compress-alt"></i></div>

        <div  class="product_shortcut cit_tooltip" 
              data-toggle="tooltip" data-placement="right" data-html="true" title="Raširi sve"
              id="products_expand" style="top: 135px"><i class="fas fa-expand-alt"></i></div>

      `;
    
    var product_shortcuts_top = 135;
    
    if ( data.items && $.isArray(data.items) && data.items.length > 0 ) {
      
      $.each( data.items, function(item_ind, item) {
        
        product_shortcuts += `
        <div  class="product_shortcut scroll_to_product_btn cit_tooltip" 
              style="top: ${ product_shortcuts_top + (item_ind+1)*35 }px"
              data-toggle="tooltip" data-placement="right" data-html="true" title="${item.full_product_name}">
              ${item_ind+1}
        </div>`;
        
      });
      
      
      $(`#product_shortcuts_box`).html(product_shortcuts);
      
    };
    // kraj ako ima iteme tj producte u projectu
    
    wait_for(`$(".product_shortcut").length > 0`, function() {
      
      $("#products_compres").off(`click`);
      $("#products_compres").on(`click`, function() {
        
        $(`.product_comp`).each( function() {
          $(this).find(`.cit_accord`).removeClass(`cit_active`);
          $(this).find(`.cit_panel`).css({"height": "0px", "overflow" : "hidden"});
        });
        
        // scroll s animacijom do PRVOG PRODUCTA
        setTimeout( function() { 
          cit_scroll( $(`.cit_comp.product_comp`).eq(0), -60, 200 );
        }, 350 );
        
      });
      
      $("#products_expand").off(`click`);
      $("#products_expand").on(`click`, function() {
        
        $(`.product_comp`).each( function() {
          $(this).find(`.cit_accord`).addClass(`cit_active`);
          $(this).find(`.cit_panel`).css({"height": "auto", "overflow" : "unset"});
        });
        
        // scroll s animacijom do PRVOG PRODUCTA
        setTimeout( function() { 
          cit_scroll( $(`.cit_comp.product_comp`).eq(0), -60, 200 );
        }, 350 );
        
      });
      
      
      $(`.scroll_to_product_btn`).each( function(product_index) {
        
        $(this).off(`click`);
        $(this).on(`click`, function() {
          
          // ------------------------ COMPRES SVE PRODUCTE ------------------------
          // $("#products_compres").trigger(`click`);
          
          $(`.product_comp`).each( function(compres_index) {
          
            // PRVO skupi sve IZNAD selektiranog producta
            if ( compres_index < product_index ) {
              $(this).find(`.cit_accord`).removeClass(`cit_active`);
              $(this).find(`.cit_panel`).css({"height": "0px", "overflow" : "hidden"});
            };
            
          });

          
          // EXPAND samo product za koji sam kliknio button
          setTimeout( function() { 
            $(`.cit_comp.product_comp`).eq(product_index).find(`.cit_accord`).addClass(`cit_active`);
            $(`.cit_comp.product_comp`).eq(product_index).find(`.cit_panel`).css({"height": "auto", "overflow" : "unset"});
          }, 50 );
          
          // scroll s animacijom do tog producta
          setTimeout( function() { 
            cit_scroll( $(`.cit_comp.product_comp`).eq(product_index), -60, 200 );
          }, 400 );
          
          
          
          // scroll s animacijom do tog producta
          setTimeout( function() { 
            
            // skupi sve ISPOD selektiranog producta
            // skupi sve ISPOD selektiranog producta
            $(`.product_comp`).each( function(compres_index) {
              if ( compres_index > product_index ) {
                $(this).find(`.cit_accord`).removeClass(`cit_active`);
                $(this).find(`.cit_panel`).css({"height": "0px", "overflow" : "hidden"});
              };
            });
            
          }, 450 );
          
          
          
        });
        
      });
      
      
      var scroll_debounce = null;
      $(document).off('scroll.product_shortcuts')
      $(document).on('scroll.product_shortcuts', function (event) {
        
        clearTimeout(scroll_debounce);
        scroll_debounce = setTimeout( function() {
          
          var product_in_view_index = null;
          var product_comp = null;
          
          var kalk_in_view_index = null;
          var kalk_box = null;
          
          
          $(`.cit_comp.product_comp`).each( function(product_index) {
            // realna pozicija od producta se računa kao offset producta - scroll
            var real_top = $(this).offset().top - $(window).scrollTop();
            var panel_height = $(this).find(`.cit_panel`).css(`height`);
            
            if ( real_top <= 100 && panel_height !== `0px`) { // ako je na početak diva na vrh ekrana tj manje od 200 px i ako je product kartica expandana
              product_in_view_index = product_index;
              product_comp = this;
              
            };
            
          });
          
          
          if ( product_in_view_index !== null ) {
            // obriši svima cit active
            $(`.scroll_to_product_btn`).removeClass(`cit_active`);
            
            // poništi na svim SAVE PRODUCT gumbima fixed position
            // BEZ ANIMACIJE
            
            $(`.save_product_btn`).removeClass(`cit_fixed`);
            
            // dodaj CLASS ACTIVE samo na onaj gumb koji paše indexu
            $(`.scroll_to_product_btn`).eq(product_in_view_index).addClass(`cit_active`);
            
            // fixed position za gumb save na trenutnom productu
            $(product_comp).find(`.save_product_btn`).addClass(`cit_fixed`);
            
          } 
          else {
            
            
            if ( $(`.save_product_btn.cit_fixed`).length > 0 ) {
              
              var curr_save_btn = $(`.save_product_btn.cit_fixed`);
              
              var product_id = curr_save_btn.attr(`data-product_comp_id`);
              var product_real_top = $(`#`+product_id).offset().top - $(window).scrollTop();
              
              // prvo animiraj na staru poziciju
              curr_save_btn.css({
                "top":  (product_real_top+5) + "px",
                "right": "190px"
              });
              
              // nakon animacije obriši klasu za fixed position
              setTimeout( function() {
                $(`.save_product_btn`).removeClass(`cit_fixed`);
                curr_save_btn.css({
                  "top":  "",
                  "right": ""
                });
              }, 350 ); 
              
              
            };
            
            
              
            // obriši svima cit active
            $(`.scroll_to_product_btn`).removeClass(`cit_active`);
            
          };
          
          
          
          
          if ( product_comp && $(product_comp).find(`.kalkulacija_tab_btn`).hasClass(`cit_active`) ) {

            $(product_comp).find(`.kalk_box`).each( function(kalk_box_index) {
              
              if ( $(this).hasClass(`cit_closed`) == false ) {
                // realna pozicija od kalk box se računa kao offset kalk box - scroll
                var real_top = $(this).offset().top - $(window).scrollTop();
                if ( real_top <= 100 ) { // ako je na početak diva na vrh ekrana tj manje od 200 px i ako je product kartica expandana
                  kalk_in_view_index = kalk_box_index;
                  kalk_box = this;
                };
                
              };

            });
            
            
            if ( kalk_in_view_index !== null ) {
              
              $(`.save_kalk_btn`).removeClass(`cit_fixed`);
              
              var save_kalk_btn_class = ``;
              if ( $(kalk_box).attr(`data-kalk_type`) == `offer_kalk` ) save_kalk_btn_class = `.save_offer_kalk_btn`;
              if ( $(kalk_box).attr(`data-kalk_type`) == `pro_kalk` ) save_kalk_btn_class = `.save_pro_kalk_btn`;
              if ( $(kalk_box).attr(`data-kalk_type`) == `post_kalk` ) save_kalk_btn_class = `.save_post_kalk_btn`;
     
              // fixed position za gumb save na trenutnom kalk type containeru
              $(kalk_box).closest(`.kalk_comp`).find(save_kalk_btn_class).addClass(`cit_fixed`);
              
            } 
            else {
              
              if ( $(`.save_kalk_btn.cit_fixed`).length > 0 ) {
                $(`.save_kalk_btn.cit_fixed`).removeClass(`cit_fixed`);
              };
              
            };
          
          } else {
            
            $(`.save_kalk_btn.cit_fixed`).removeClass(`cit_fixed`);
            
          };
          
        }, 300);  // kraj debounse od scrolla

      }); // kraj specijalnog scroll eventa s namespace-om
      
      
      
      
    }, 5*1000);
    
  };
  this_module.make_product_shortcuts = make_product_shortcuts;
  

  function save_current_proj_specs() {

    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    var comp_id = rand_id+`_proj_docs`;


    // vrati nazad da gumb za save dokumenata  bude vidljiv
    // ionako ću sakriti parent i cijelu listu obrisati
    $(`#${comp_id}_upload_btn`).css(`display`, `flex`);

    // obriši listu
    $(`#${comp_id}_list_items`).html( "" );
    // sakrij listu i button
    $(`#${comp_id}_list_box`).css( `display`, `none` );


    if ( !window.current_proj_specs ) window.current_proj_specs = {};

    if ( !window.current_proj_specs.sifra ) window.current_proj_specs.sifra = `projspecs`+cit_rand();

    window.current_proj_specs.komentar = $('#'+rand_id+`_proj_specs_komentar`).val();

    window.current_proj_specs.time = window.current_proj_specs.time || Date.now(); 


    if ( !window.current_proj_specs.komentar  ) {
      popup_warn(`Potrebno je upisati kratki opis za dokumente !!!`);
      return;
    };


    if ( !window.current_proj_specs.docs ) window.current_proj_specs.docs = null;

    data.proj_specs = upsert_item(data.proj_specs, window.current_proj_specs, `sifra`);

    data.proj_specs_flat = JSON.stringify(data.proj_specs);

    this_module.make_proj_specs_list(data);

    window.current_proj_specs = null;

    $('#'+rand_id+`_proj_specs_komentar`).val("");

    if ( $(this).hasClass(`update_proj_specs`) ) {
      $(this).css(`display`, `none`);
      $(`#${data.id} .save_proj_specs`).css(`display`, `flex`);
    };

  };
  this_module.save_current_proj_specs = save_current_proj_specs;
  
  
  function update_proj_docs( files, button, arg_time ) {

    var this_comp_id = $(button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    var new_docs_arr = [];

    $.each(files, function(f_index, file) {

      var file_obj = {
        sifra: `doc`+cit_rand(),
        time: arg_time,
        link: `<a href="/docs/${time_path(arg_time)}/${file.filename}" target="_blank" onClick="event.stopPropagation();" >${ file.originalname }</a>`,
      };
      new_docs_arr.push( file_obj );
    });

    console.log( new_docs_arr );

    // napravi novi objekt ako current ne postoji
    if ( !window.current_proj_specs ) window.current_proj_specs = {};


    if ( $.isArray(window.current_proj_specs.docs) ) {
      // ako postoji docs array onda dodaj nove filove
      window.current_proj_specs.docs = [ ...window.current_proj_specs.docs, ...new_docs_arr ];
    } else {
      // ako ne postoji onda stavi ove nove filove
      window.current_proj_specs.docs = new_docs_arr;
    };    

  };
  this_module.update_proj_docs = update_proj_docs;
  

  function make_proj_specs_list(data) {

    // SPECS OD PROJEKTA

    var specs_for_table = [];

    if ( data.proj_specs && data.proj_specs.length > 0 ) {

      $.each(data.proj_specs, function(index, spec_obj ) {

        var { sifra, time, komentar } = spec_obj;

        var all_docs = "";
        if ( spec_obj.docs && $.isArray(spec_obj.docs) ) {


          var criteria = [ '!time' ];
          multisort( spec_obj.docs, criteria );

          $.each(spec_obj.docs, function(doc_index, doc) {
            var doc_html = 
            `
            <div class="docs_row">
              <div class="docs_date">${ cit_dt(doc.time).date_time }</div>
              <div class="docs_link">${ doc.link }</div>
            </div>
            `;

            all_docs += doc_html;

          }); // kraj loopa po docs

        }; // kraj ako ima docs

        specs_for_table.push({ sifra, time, komentar, docs: all_docs });

      }); // kraj loopa svih proj specifikacija

    };



    var proj_specs_props = {

      desc: 'samo za kreiranje tablice svih specs od projekta ',
      local: true,

      list: specs_for_table,

      show_cols: [ "komentar", "time", "docs", 'button_edit', 'button_delete' ],
      format_cols: { time: "date_time" },
      col_widths: [

        4, // "komentar",
        2, // "time",
        4, // "docs",
        1, // 'button_edit',
        1, // 'button_delete'

      ],
      parent: `#${data.rand_id}_proj_specs_box`,
      return: {},
      show_on_click: false,
      cit_run: function(state) { console.log(state); },

      button_edit: function(e) { 

        e.stopPropagation(); 
        e.preventDefault();

        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var index = find_index(data.proj_specs, sifra , `sifra` );

        window.current_proj_specs = cit_deep( data.proj_specs[index] );


        $(`#${data.id} .save_proj_specs`).css(`display`, `none`);
        $(`#${data.id} .update_proj_specs`).css(`display`, `flex`);

        $('#'+rand_id+`_proj_specs_komentar`).val( window.current_proj_specs.komentar || "");

        console.log("kliknuo edit proj specs !!!");


      },
      button_delete: async function(e) {

        e.stopPropagation();
        e.preventDefault();

        var this_button = this;

        console.log("kliknuo DELETE proj specs ");

        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);

          data.proj_specs = delete_item(data.proj_specs, sifra , `sifra` );

          $(`#`+parent_row_id).remove();

        };


        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj dokument?`, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj dokument?`, null, 'yes/no', delete_yes, delete_no, null);


      },

    };
    if ( specs_for_table.length > 0 ) {
      
      var criteria = [ '!time' ];
      multisort( specs_for_table, criteria );      
      create_cit_result_list(specs_for_table, proj_specs_props );
      
    };


  };
  this_module.make_proj_specs_list = make_proj_specs_list;

  
  async function offer_kupac() {

    var preview_mod = await get_cit_module('/modules/previews/cit_previews.js', `load_css`);
    
    var offer_button = this;
    
    var this_comp_id = $(offer_button).closest('.cit_comp')[0].id;
    var project_data =  this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    refresh_simple_cal( $('#'+project_data.id+`_doc_rok_time`) );
    
    var sjediste = concat_full_adresa( project_data.kupac, `VAD1`); // VAD1 je sjedište

    if ( !sjediste ) {
      popup_warn(`Kupac nema adresu sjedišta!!!<br>Potrebno otvoriti karticu kupca i napraviti sjedište.`);
      return;
    };

    $.each( project_data.kupac.kontakti, function (k_ind, kontakt) {
      if ( kontakt.adresa_kontakta?.vrsta_adrese?.sifra == `VAD1` ) {
        project_data.partner_adresa = kontakt;
        project_data.partner_adresa.adrese = [ project_data.partner_adresa.adresa_kontakta ]; // postavljam u array jer concat full adresa treba array kao argument
        project_data.partner_adresa.full_adresa = concat_full_adresa( project_data.partner_adresa );
      };
    }); 

    
    if ( !project_data.partner_adresa?.full_adresa ) {
      popup_warn(`Partner nema adresu sjedišta (glavnu adresu)!!!<br>Potrebno je upisati adresu sjedišta u kartici partnera !!!`);
      return;
    };
    
    
    if ( !project_data.doc_adresa ) {
      popup_warn(`Niste izabrali adresu primatelja za ovaj dokument!!!`);
      return;
    };
    

    
    if ( !project_data.doc_lang ) {
      project_data.doc_lang = { sifra: `hr`, naziv: `HRVATSKI` };
    };
    
    if ( !project_data.kupac ) {
      popup_warn(`Potrebno izabrati kupca u projektu!`);
      return;
    };
    
    if ( !project_data.doc_rok_time ) {
      popup_warn(`Potrebno izabrati rok dokumenta!`);
      return;
    };
    
    if ( !project_data.items || project_data.items.length == 0 ) {
      popup_warn(`Projekt nema niti jedan proizvod !!!`);
      return;
    };
    
    
    if ( !project_data.payment ) {
      popup_warn(`Niste izabrali način plaćanja!!!`);
      return;
    };
    
    
    if ( !project_data.dostav_adresa ) {
      popup_warn(`Niste izabrali adresu dostave za ovaj dokument!!!`);
      return;
    };
    
    
    // default dostav adresa je u projektu
    var dostav_adresa = project_data.dostav_adresa || null;
    
    var items = [];
    var all_products = [];
    
    // podaci od producta su u projectu
    // ali moram pregledati u modul od producta da dobijem točne podatke koji su svježi
    $.each(project_data.items, function(i_ind, item) {
      
      // ovaj product module sam importao u project i on u sebi ima podatke svih aktivnih products na ekranu
      // this_module.product_module.cit_data
      $.each( window[`/modules/products/product_module.js`].cit_data, function(product_key_id, product_data) {
        // ako je 
        if ( item.id == product_data.id ) {
          // ako postoji dostav adresa na razini produkta onda uzmi tu adresu
          if ( product_data.dostav_adresa ) dostav_adresa = product_data.dostav_adresa;
          
          all_products.push(product_data);
          
        };
        
      });
      
      
    });
    
    
    var min_rok_za_def = new Date(3000, 10, 10); // neki veliki datum -- bilo koji je ok
    min_rok_za_def = Number(min_rok_za_def);
    $.each( all_products, function( p_ind, product_data )  {
      if ( product_data.rok_za_def && product_data.rok_za_def < min_rok_za_def ) min_rok_za_def = product_data.rok_za_def;
    });
    
    if ( 
      new Date(min_rok_za_def).getFullYear() !== 3000 // samo ako nije originalni veliki datum (3000 god)
      &&
      project_data.doc_rok_time > min_rok_za_def // samo ako je odabrani datum veći od NAJBLIŽEG ROKA  za definiciju  -> OD BILO KOJEG PRODUCTA
    ) {
      popup_warn(`Najbliži Rok za definiciju jednog od proizvoda je:<br><br>${ cit_dt( min_rok_za_def ).date } !!!<br><br>Ne možeš izabrati datum iza tog datuma :P`);
      $(`#${project_data.id}_doc_rok_time`).val(``);
      project_data.doc_rok_time = null;
      return;
    };
    
    
    var db_partner = null;
    if( !project_data.kupac || !project_data.kupac._id ) {
      popup_warn(`Nedostaje info o kupcu unutar projekta !!!`);
      return;
    };
    
    db_partner = project_data.kupac;
    
    
    var doc_sirovs = [];
    
    $.each( all_products, function( p_ind, product_data )  {

      
      var kalk_module_data = null;
      // uzmi podatak kalkulacije iz globalnog OBJEKTA
      if ( product_data.kalk ) {
        $.each(this_module.kalk_module.cit_data, function(kalk_key_id, kalk_data) {
          if ( kalk_data.id == product_data.kalk.id ) kalk_module_data = kalk_data;
        });
      };
      
      
      $.each( kalk_module_data.offer_kalk, function(k_ind, kalk) {

        if ( kalk.show_in_offer ) {

          var price_start = kalk.kalk_final_unit_price;
          var price_final = null;

          // postoji popust u kunama onda izračunaj popust u %
          if ( kalk.kalk_discount_money ) {
            kalk.kalk_discount = 100 * kalk.kalk_discount_money / (price_start * kalk.kalk_naklada);
            format_number_input( kalk.kalk_discount, $(`#`+kalk.kalk_sifra+`_kalk_discount`)[0], 2);
          };
          // ako mi fali popust u kn onda ga postavi na nula
          if ( !kalk.kalk_discount_money ) kalk.kalk_discount_money = 0;
          
          
          // ako marža u kunama onda izračunaj maržu u %
          if ( kalk.kalk_markup_money ) {
            kalk.kalk_markup = 100 * kalk.kalk_markup_money / (price_start * kalk.kalk_naklada);
            format_number_input( kalk.kalk_markup, $(`#`+kalk.kalk_sifra+`_kalk_markup`)[0], 2);
          };
          // ako mi fali marža u parama on je postavi na nula
          if ( !kalk.kalk_markup_money ) kalk.kalk_markup_money = 0;

          
          // startna cijena je sada s popustom
          price_start = kalk.kalk_final_unit_price * (100 + (kalk.kalk_markup || 0) )/100;
          price_final = price_start * (100 - (kalk.kalk_discount || 0) )/100;


          var product_info =  {
            
            product_id: kalk_module_data.product_id,
            
            proj_sifra : kalk_module_data.proj_sifra || null,
            item_sifra : kalk_module_data.item_sifra || null,
            variant : kalk_module_data.variant || null,

            naziv: product_data.full_product_name || "", 
            jedinica: "kom",

            unit_price: price_start,
            discount: kalk.kalk_discount,
            dis_unit_price: price_final,

            count: kalk.kalk_naklada,
            dis_price: price_final * kalk.kalk_naklada,
            decimals: 2,
            
            dostav_adresa: product_data.dostav_adresa.full_adresa || null,
            
            

          };
          
          
          items.push(product_info);
          
          doc_sirovs.push({
            
            out_roba: true,
            
            _id: "", // još uvijek nemam id od sirovine jer još uvvijek nije izlazna roba
            sirovina_sifra: "", // još uvijek nemam niti sifru sirovine
            naziv: product_info.naziv,
            
            product_id: product_info.product_id,
            proj_sifra : product_info.proj_sifra || null,
            item_sifra : product_info.item_sifra || null,
            variant : product_info.variant || null,
            
            

            doc_kolicine: [
              {

                sifra: `dockol` + cit_rand(),
                
                unit_price: price_start,
                dis_unit_price: price_final,
                discount: product_info.kalk_discount,
                
                count: product_info.count,
                price: product_info.dis_price,
                
                next_price: null,
                next_count: null,
                
                next_approved: false,
                next_doc: false,

              }
            ],

            
            
          });
          

        }; // ako ima označeno u show in offer

      }); // loop po offer kalks


    }); // kraj loop po all products
    
    
    var data_for_doc = {
      
      doc_type: `offer_reserv`,
      
      sifra: "dfd"+cit_rand(),
      
      time: Date.now(),

      for_module: `kalk`,

      proj_sifra : project_data.proj_sifra || null,
      
      /*item_sifra : product_data.item_sifra || null,*/
      /*variant : product_data.variant || null,*/

      status: null,

      /*sirov_id: null,*/
      /* full_record_name: product_data.full_product_name, */

      doc_valuta: ( project_data.doc_valuta?.valuta || "HRK" ), // default je HRK
      doc_valuta_manual: ( project_data.doc_valuta_manual || null ), // manual tečaj
      
      doc_pay: project_data.payment,
      
      referent: window.cit_user.full_name,
      place: `Velika Gorica`,

      
      doc_num: `---`,

      oib: (db_partner.oib || db_partner.vat_num || ""),

      partner_name: db_partner.naziv,
      partner_adresa: project_data.partner_adresa.full_adresa,
      doc_adresa: project_data.doc_adresa.full_adresa,
      dostav_adresa: project_data.dostav_adresa.full_adresa,
      partner_id: db_partner._id,

      /*
      partner_place: `67122 ALTRIP`,
      partner_country: `Germany`,
      */

      rok: min_rok_za_def == 32530719600000 ? null : min_rok_za_def, // new Date(3000, 10, 10) == 32530719600000 // neki veliki datum -- bilo koji je ok
      dobav_ponuda_sifra: ``,

      items: items,
      
      doc_sirovs: doc_sirovs,
      
      doc_komentar: esc_html(project_data.doc_komentar || null ),
      
      
    };
    
    preview_mod.generate_cit_doc( data_for_doc, project_data.doc_lang.sifra );
  
  };
  this_module.offer_kupac = offer_kupac;


  async function order_kupac() {

    var preview_mod = await get_cit_module('/modules/previews/cit_previews.js', `load_css`);
    
    var offer_button = this;
    
    var this_comp_id = $(offer_button).closest('.cit_comp')[0].id;
    var project_data =  this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    
    refresh_simple_cal( $('#'+project_data.id+`_doc_rok_time`) );
    
    var sjediste = concat_full_adresa( project_data.kupac, `VAD1`); // VAD1 je sjedište

    if ( !sjediste ) {
      popup_warn(`Kupac nema adresu sjedišta!!!<br>Potrebno otvoriti karticu kupca i napraviti sjedište.`);
      return;
    };

    // NA 
    $.each( project_data.kupac.kontakti, function (k_ind, kontakt) {
      if ( kontakt.adresa_kontakta?.vrsta_adrese?.sifra == `VAD1` ) { // kontak od sjedišta VAD1 je sjedište
        project_data.partner_adresa = kontakt;
        project_data.partner_adresa.adrese = [ project_data.partner_adresa.adresa_kontakta ]; // savljam samo jednu adresu kontakta
        project_data.partner_adresa.full_adresa = concat_full_adresa( project_data.partner_adresa );
      };
    }); 
    
    if ( !project_data.doc_adresa ) {
      popup_warn(`Niste izabrali adresu primatelja za ovaj dokument!!!`);
      return;
    };
    
    if ( !project_data.dostav_adresa ) {
      popup_warn(`Niste izabrali adresu dostave za ovaj dokument!!!`);
      return;
    };
    
    if ( !project_data.doc_lang ) {
      project_data.doc_lang = { sifra: `hr`, naziv: `HRVATSKI` };
    };
    
    if ( !project_data.kupac ) {
      popup_warn(`Potrebno izabrati kupca u projektu!`);
      return;
    };
    
    if ( !project_data.doc_rok_time ) {
      popup_warn(`Potrebno izabrati rok dokumenta!`);
      return;
    };
    
    if ( !project_data.items || project_data.items.length == 0 ) {
      popup_warn(`Projekt nema niti jedan proizvod !!!`);
      return;
    };
    
    
    if ( !project_data.payment ) {
      popup_warn(`Niste izabrali način plaćanja!!!`);
      return;
    };
    
    
    var items = [];
    var all_products = [];
  
    $.each(project_data.items, function(i_ind, product) {
      $.each(this_module.product_module.cit_data, function(product_key_id, product_data) {
        if ( product.id == product_data.id ) all_products.push(product_data);
      });
    });
    
    
    var min_rok_za_def = new Date(3000, 10, 10); // neki veliki datum -- bilo koji je ok -----> pretvoreno u broj = 32530719600000
    min_rok_za_def = Number(min_rok_za_def);
    $.each( all_products, function( p_ind, product_data )  {
      if ( product_data.rok_za_def && product_data.rok_za_def < min_rok_za_def ) min_rok_za_def = product_data.rok_za_def;
    });
 
    
    if ( 
      min_rok_za_def !== 32530719600000 // ako nije dummy rok za definicju -----> to jest 3000 god
      &&
      project_data.doc_rok_time > min_rok_za_def // samo ako je odabrani datum veći od roka za definiciju
    ) {
      popup_warn(`Rok za definiciju jednog od proizvoda je:<br><br>${ cit_dt( min_rok_za_def ).date } !!!<br><br>Ne možeš izabrati datum iza tog datuma :P`);
      $(`#${project_data.id}_doc_rok_time`).val(``);
      project_data.doc_rok_time = null;
      return;
    };
    
    
    var min_rok_isporuke = new Date(3000, 10, 10); // neki veliki datum -- bilo koji je ok -----> pretvoreno u broj = 32530719600000
    min_rok_isporuke = Number(min_rok_isporuke);
    $.each( all_products, function( p_ind, product_data )  {
      if ( product_data.rok_isporuke && product_data.rok_isporuke < min_rok_isporuke ) min_rok_isporuke = product_data.rok_isporuke;
    });
    
    
    
    var db_partner = null;
    if( !project_data.kupac || !project_data.kupac._id ) {
      popup_warn(`Nedostaje info o kupcu unutar projekta !!!`);
      return;
    };
    db_partner = project_data.kupac;
    
    
    /*  -------------------- doc sirovs su zapravo proizvodi koji još nisu postali roba ali ubacujem da prop atko da u svakom  record bude isti  */
    
    var doc_sirovs = [];
    
    
    $.each( all_products, function( p_ind, product_data ) {
      
      var kalk_module_data = null;
      // uzmi podatak kalkulacije iz globalnog STORA
      if ( product_data.kalk ) {
        $.each(this_module.kalk_module.cit_data, function(kalk_key_id, kalk_data) {
          if ( kalk_data.id == product_data.kalk.id ) kalk_module_data = kalk_data;
        });
      };
      
      var rok_isporuke_text = ``;
          
      if ( product_data.rok_isporuke ) {
        rok_isporuke_text = ` (isporuka: ${ cit_dt( product_data.rok_isporuke ).date })`;
      }; 
      
      $.each( kalk_module_data.offer_kalk, function(k_ind, kalk) {

        if ( kalk.kalk_for_pro ) {

          var price_start = kalk.kalk_final_unit_price;
          var price_final = null;

          // postoji popust u kunama onda izračunaj popust u %
          if ( kalk.kalk_discount_money ) {
            kalk.kalk_discount = 100 * kalk.kalk_discount_money / (price_start * kalk.kalk_naklada);
            format_number_input( kalk.kalk_discount, $(`#`+kalk.kalk_sifra+`_kalk_discount`)[0], 2);
          };
          // ako mi fali popust u kn onda ga postavi na nula
          if ( !kalk.kalk_discount_money ) kalk.kalk_discount_money = 0;
          // ako marža u kunama onda izračunaj maržu u %
          if ( kalk.kalk_markup_money ) {
            kalk.kalk_markup = 100 * kalk.kalk_markup_money / (price_start * kalk.kalk_naklada);
            format_number_input( kalk.kalk_markup, $(`#`+kalk.kalk_sifra+`_kalk_markup`)[0], 2);
          };
          // ako mi fali marža u parama on je postavi na nula
          if ( !kalk.kalk_markup_money ) kalk.kalk_markup_money = 0;

          // startna cijena je sada s popustom
          price_start = kalk.kalk_final_unit_price * (100 + (kalk.kalk_markup || 0) )/100;
          price_final = price_start * (100 - (kalk.kalk_discount || 0) )/100;

          
          var product_info = {
            
            product_id: kalk_module_data.product_id,
            
            proj_sifra : kalk_module_data.proj_sifra || null,
            item_sifra : kalk_module_data.item_sifra || null,
            variant : kalk_module_data.variant || null,
            
            
            naziv: product_data.full_product_name + rok_isporuke_text, 
            jedinica: "kom",
            
            unit_price: price_start,
            discount: kalk.kalk_discount,
            dis_unit_price: price_final,

            count: kalk.kalk_naklada,
            dis_price: price_final * kalk.kalk_naklada,
            decimals: 2,
            rok_isporuke: product_data?.rok_isporuke || null,
            
            
            dostav_adresa: product_data.dostav_adresa.full_adresa || null,
            

          };
          

          items.push(product_info);
      
          doc_sirovs.push({
            
            out_roba: true,
            
            _id: "", // još uvijek nemam id od sirovine jer još uvvijek nije izlazna roba
            sirovina_sifra: "", // još uvijek nemam niti sifru sirovine
            naziv: product_info.naziv,
            
            product_id: product_info.product_id,
            proj_sifra : product_info.proj_sifra || null,
            item_sifra : product_info.item_sifra || null,
            variant : product_info.variant || null,
       

            doc_kolicine: [
              {

                sifra: `dockol` + cit_rand(),
                
                unit_price: price_start,
                dis_unit_price: price_final,
                discount: product_info.kalk_discount,
                
                count: product_info.count,
                price: product_info.dis_price,
                
                next_price: null,
                next_count: null,
                
                next_approved: true,
                next_doc: true,

              }
            ],

            
            
          });
          
          

        }; // ako ima označeno u show in offer

      }); // loop po offer kalks


    }); // kraj loop po all products
    
    
    var data_for_doc = {

      doc_type: `order_reserv`,
      
      sifra: "dfd"+cit_rand(),
      
      time: Date.now(),

      for_module: `kalk`,
      
      proj_sifra : project_data.proj_sifra || null,
      
      /* item_sifra : product_data.item_sifra || null,*/
      /* variant : product_data.variant || null, */

      status: null,

      /* sirov_id: null, */
      /* full_record_name: product_data.full_product_name, */


      doc_valuta: ( project_data.doc_valuta?.valuta || "HRK" ), // default je HRK
      doc_valuta_manual: (project_data.doc_valuta_manual || null ), // manual tečaj
      
      doc_pay: project_data.payment,
      
      referent: window.cit_user.full_name,
      place: `Velika Gorica`,

      doc_num: `---`,

      oib: (db_partner.oib || db_partner.vat_num || ""),

      partner_name: db_partner.naziv,
      partner_adresa: project_data.partner_adresa.full_adresa,
      doc_adresa: project_data.doc_adresa.full_adresa,
      dostav_adresa: project_data.dostav_adresa.full_adresa,
      partner_id: db_partner._id,

      /*
      partner_place: `67122 ALTRIP`,
      partner_country: `Germany`,
      */

      rok: min_rok_za_def, // pošto ima više proizvoda sa različitim rokovima za def onda stavi onaj najbliži koji prvi dolazi
      dobav_ponuda_sifra: ``,

      items: items,
      
      doc_sirovs: doc_sirovs,
      
      doc_komentar: project_data.doc_komentar || null,
      
    };
    
    preview_mod.generate_cit_doc( data_for_doc, project_data.doc_lang.sifra );
  
  };
  this_module.order_kupac = order_kupac;


  async function save_doc_in_proj_specs( data_for_doc, arg_pdf ) {

    
    var this_comp_id =  $(`#save_proj_btn`).closest('.cit_comp')[0].id;
    var project_data =  this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    var doc_type_human_name = "";
    if ( data_for_doc.doc_type == "offer_reserv" ) doc_type_human_name = " PONUDA KUPCU ";
    if ( data_for_doc.doc_type == "order_reserv" ) doc_type_human_name = " POTVRDA NARUDŽBE KUPCA ";
    if ( data_for_doc.doc_type == "radni_nalog" ) doc_type_human_name = ` RADNI NALOG ZA ${data_for_doc.full_record_name} `;

    var komentar = ( data_for_doc.doc_sifra ? data_for_doc.doc_sifra : "" ) + doc_type_human_name + " (Automatski generiran dokument)";

    var new_proj_spec = {

      "docs": [
        {
          "sifra": "doc"+cit_rand(),
          "time": arg_pdf.time,
          "link": `<a href="/docs/${time_path(arg_pdf.time)}/${arg_pdf.ime_filea}" target="_blank" onClick="event.stopPropagation();" >${ arg_pdf.ime_filea }</a>`
        }
      ],
      "sifra": "projspecs"+cit_rand(),
      "komentar": komentar,
      "time": Date.now(),
    };

    project_data.proj_specs = upsert_item( project_data.proj_specs, new_proj_spec, `sifra`);

    this_module.make_proj_specs_list(project_data);
    
    
    setTimeout( function() {  $(`#save_proj_btn` ).trigger(`click`); }, 200);
    

  };  
  this_module.save_doc_in_proj_specs = save_doc_in_proj_specs;
  
  
  
  this_module.cit_loaded = true;
 
} // end of module scripts
};
