var module_object = {
create: (data, parent_element, placement ) => {
  var this_module = window[module_url]; 
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };
  
  var { id } = data;
  
  
  var valid = {
    find_project: { element:"single_select", type:"", lock:false, visible:true, label:"Pretraga projekata"},
    find_product: { element:"single_select", type:"", lock:false, visible:true, label:"Pretraga proizvoda"},
    find_status: { element:"single_select", type:"", lock:false, visible:true, label:"Pretraga statusa"},
    pis_find_mode: { element:"switch", type:"bool", lock:false, visible:true, label:"Uključi detaljnu pretragu"},
    
  };
  

  this_module.set_init_data(data);
  
  var rand_id = `findPIS`+cit_rand();
  this_module.cit_data[id].rand_id = rand_id;
  
  
  var component_html =
`

<div id="${id}" class="poject_search_body cit_comp">
  <section>
    <div class="container-fluid">
     
     
      <div class="row" style="margin-bottom: 5px;" >
        
        <div class="col-md-2 col-sm-12">
          <div class="row">
            <div class="col-md-12 col-sm-12" style="margin-bottom: 5px;" >
              <!-- OVO JE ISTO KAO ZA FIND SIROV  window.sirov_find_mode  -->
              ${ cit_comp( rand_id+`_pis_find_mode`, valid.pis_find_mode, window.pis_find_mode || false ) }
            </div>
          </div>
        </div>  
        
        <div class="col-md-10 col-sm-12">
          <div class="row">
           
            <div class="col-md-3 col-sm-12">
              ${ cit_comp(rand_id+`_find_project`, valid.find_project, null, "") }
            </div>

            <div class="col-md-1 col-sm-12" style="margin-bottom: 5px;" >
              <button class="blue_btn btn_small_text cit_tooltip" id="show_find_project_table_btn" style="margin: 20px auto 0 0; box-shadow: none;"
                      data-toggle="tooltip" data-placement="bottom" data-html="true" title="Prikaži u tablici" >
                <i class="far fa-search" style="margin: 0; font-size: 16px; font-weight: 700;"></i>
              </button>
            </div>

            <div class="col-md-3 col-sm-12">
              ${ cit_comp(rand_id+`_find_product`, valid.find_product, null, "") }
            </div>

            <div class="col-md-1 col-sm-12" style="margin-bottom: 5px;" >
              <button class="blue_btn btn_small_text cit_tooltip" id="show_find_product_table_btn" style="margin: 20px auto 0 0; box-shadow: none;"
                      data-toggle="tooltip" data-placement="bottom" data-html="true" title="Prikaži u tablici" >
                <i class="far fa-search" style="margin: 0; font-size: 16px; font-weight: 700;"></i>
              </button>
            </div>

            <div class="col-md-3 col-sm-12">
              ${ cit_comp(rand_id+`_find_status`, valid.find_status, null, "") }
            </div>

            <div class="col-md-1 col-sm-12" style="margin-bottom: 5px;" >
              <button class="blue_btn btn_small_text cit_tooltip" id="show_find_status_table_btn" style="margin: 20px auto 0 0; box-shadow: none;"
                      data-toggle="tooltip" data-placement="bottom" data-html="true" title="Prikaži u tablici" >
                <i class="far fa-search" style="margin: 0; font-size: 16px; font-weight: 700;"></i>
              </button>
            </div>
          
          </div>
        </div>
        
        

      </div>
      
      <div class="row">
        <div  class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" 
              id="pis_result_box" style="padding-top: 0; padding-bottom: 0;">
          <!-- OVDJE IDE LISTA PRETRAGE OD PROJEKTA / PRODUCTA / STATUSA  -->    
        </div> 
      </div>
    </div>  
  </section>
</div>

`;

  


  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };
  
  wait_for( `$('#${data.id}').length > 0`, async function() {
    
    console.log(data.id + 'component injected into html');
    
    
    $('.cit_tooltip').tooltip();
    
    // if ( $('.cit_tooltip').length > 0 ) $('.cit_tooltip').tooltip('dispose').tooltip({ boundary: 'window' });
    
    
    $('#'+rand_id+'_pis_find_mode').data(`cit_run`, function(state) { 
      
      if ( window.pis_find_mode !== state ) {
        
        window.pis_find_mode = state;
        if ( state == false ) window.cit_find_query = null;
        if ( state == true ) window.cit_find_query = {};
        
        var sirov_data = {
          id: `sirov_module`,
        };
        this_module.sirov_module.create( sirov_data, $(`#cont_main_box`) );
        
      };
    
    });
    
    
    
    
    
    this_module.project_module = await get_cit_module(`/modules/project/project_module.js`, 'load_css');
    
    this_module.status_module = await get_cit_module(`/modules/status/status_module.js`, 'load_css');
    
    
    this_module.product_module = await get_cit_module(`/modules/products/product_module.js`, 'load_css');
    
    
    $(`#`+rand_id+`_find_project`).data('cit_props', {
      
      desc: 'pretraga projekta u modulu find PIS',
      local: false,
      url: '/find_proj',
      
      find_in: [
        "proj_sifra",
        "naziv",
        "kupac_flat",
        "referent_flat",
        "referent_zamjena_flat",
        "proj_status_flat",
        "komentar",
        "interni_komentar",
        "proj_specs_flat",
      ],
      return: {},
      /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */
      show_cols: [
        "proj_sifra",
        "kupac_podaci",
        "naziv",
        "referent_ime",
        "referent_zamjena_ime",
        "status_naziv",
        "komentar",
        "interni_komentar",
      ],
      col_widths: [
        1, // "proj_sifra",
        2, // "kupac_podaci",
        2, // "naziv",
        1, // "referent_ime",
        1, // "referent_zamjena_ime",
        1, // "status_naziv",
        2, // "komentar",
        2, // "interni_komentar",
      ],
      format_cols: {
        "proj_sifra": "center",
        referent_ime: "center",
        referent_zamjena_ime: "center",
        status_naziv: "center",
        
      },
      query: {},
      
      filter: this_module.find_project_filter,
      
      show_on_click: false,
      list_width: 700,
      
      cit_run: this_module.open_project,
      
    });  
    
    $(`#`+rand_id+`_find_product`).data('cit_props', {
      
      desc: 'pretraga producta u modulu find PIS',
      local: false,
      url: '/find_product',
      
      find_in: [
        "sifra",
        "full_product_name",
        "full_kupac_name",
        "mater",
        "komentar",
        "interni_komentar",
        "prod_specs_flat",
        "nacrt_specs_flat",
      ],
      return: {},
      /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */
      show_cols: [
        
        "sifra",
        "template_nacrta",
        
        "full_product_name",
        "full_kupac_name",
        "interni_komentar",
        
        "nacrt_komentar",
        "nacrt_docs",
        
        "graf_komentar",
        "graf_docs",
        
      ],
      col_widths: [
        1, // "sifra",
        1, // "template_nacrta",
        
        3, // "full_product_name",
        3, // "full_kupac_name",
        3, // "interni_komentar",
        
        3, // "nacrt_komentar",
        8, // "nacrt_docs"
        
        3, // "graf_komentar"
        8, // "graf_docs"
      ],
      format_cols: {
        sifra: "center",
        template_nacrta: "center",
      },
      query: {},
      
      filter: null,
      
      show_on_click: false,
      
      list_width: 700,
      
      filter: this_module.product_module.nacrt_find_filter,
      
      cit_run: this_module.open_project, // kraj cit run
      
      
    });  
    
    $(`#`+rand_id+`_find_status`).data('cit_props', {
      
      desc: 'pretraga statusa u modulu find PIS',
      local: false,
      url: '/find_status',
      
      find_in: [
        "sirov_flat",
        "full_product_name",
        "dep_to_flat",
        "to_flat",
        "dep_from_flat",
        "from_flat",
        "komentar",
        "status_tip_flat",
      ],
      return: {},
      
      show_cols: this_module.status_module.show_cols_arr(),
      custom_headers: this_module.status_module.custom_headers_arr(),
      col_widths: this_module.status_module.col_widths_arr(),
      
      format_cols: this_module.status_module.format_cols_obj,
      
      query: {},
      
      filter: this_module.status_module.statuses_filter,
      
      show_on_click: false,
      list_width: 700,
      
      cit_run: this_module.open_project, // kraj cit run
      
      
      button_reply: function(e) {
        e.stopPropagation(); 
        e.preventDefault();
        
        console.log("kliknuo REPLY za STATUS");
        
      },
      
      button_edit: function(e) {
        e.stopPropagation(); 
        e.preventDefault();
        
        console.log("kliknuo EDIT za STATUS");
        
      },
      
      button_seen: function(e) {
        e.stopPropagation(); 
        e.preventDefault();
        
        console.log("kliknuo SEEN za STATUS");
        
      },
      
      button_delete: function(e) {
        e.stopPropagation(); 
        e.preventDefault();
        
        console.log("kliknuo DELETE za STATUS");
        
      },
      
      
    }); 
    
    
    $(`#show_find_sirov_table_btn`).off('click');
    $(`#show_find_sirov_table_btn`).on('click', function() {
      
      var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var find_sirov_props_for_table = { 
        ...find_sirov_props,
        parent: `#show_find_sirov_table_box`,
        get_count: true,
        max_count: null, // resetiraj max conunt
        max_pages: null,
        
        sums: [
        `pk_kolicina`,
        `nk_kolicina`,
        `order_kolicina`,
        `sklad_kolicina`,
        ],
        
      };
      
      var search_string = $(`#`+rand_id+`_find_sirov`).val() || null;
      
      search_database( search_string, find_sirov_props_for_table).then(
        
        function( remote_result ) { 
          
          // alert( remote_result.count );
          
          var max_pages = Math.ceil( remote_result.count / 100 );
          
          find_sirov_props_for_table.max_count = remote_result.count || null;
          find_sirov_props_for_table.max_pages = max_pages || null;
          
          create_cit_result_list( remote_result.docs, find_sirov_props_for_table, $(`#`+rand_id+`_find_sirov`)[0].id ); 
          
          setTimeout( function() { register_grab_to_scroll(`show_find_sirov_table_box`);  }, 200 );
        },
        function( error ) { 
          console.error(`SEARCH DATABASE ERROR ZA FIND SIROV TABLE:\n${find_sirov_props_for_table.desc}`)
          console.error( error );
        }
      );
      
    });
    
    
    

  }, 500*1000 );

  return {
    html: component_html,
    id: data.id
  };

},
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};

  
  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
  };
  this_module.set_init_data = set_init_data;
  
  
  function find_project_filter(items, jQuery) {
        
    var $ = jQuery;

    var new_items = [];
    
    $.each(items, function(proj_index, item) {

      var sjediste = null;
      var grupacija_naziv = null;
      var status_naziv = null;
      var kupac_kontakti = null;

      var referent_ime = (item.referent ? item.referent.full_name : "");
      var referent_zamjena_ime = (item.referent_zamjena ? item.referent_zamjena.full_name : "");

      var kupac_podaci = null;
      var full_partner = null;

      if ( 
        item.kupac                   &&
        item.kupac.adrese            &&
        $.isArray(item.kupac.adrese) && 
        item.kupac.adrese.length > 0 
      ) {

        sjediste = global ? global.concat_full_adresa(item.kupac, `VAD1`) : window.concat_full_adresa(item.kupac, `VAD1`);
        grupacija_naziv = item.kupac.grupacija ? item.kupac.grupacija.name : "";
        status_naziv = item.kupac.status ? item.kupac.status.naziv : "";
        
        // kupac_kontakti = item.kupac.kontakti ? item.kupac.kontakti : null;

        // dodaj neke propertije koje trebam
        items[proj_index].kupac = { 
          
          ...item.kupac,
          
          /* kontakti: kupac_kontakti, */
          sjediste, 
          grupacija_naziv, 
          status_naziv 
        };  
        
        
        var full_partner = 
          item.kupac.naziv + 
          ( item.kupac.grupacija_naziv ? ", "+item.kupac.grupacija_naziv : "" ) + 
          ( item.kupac.sjediste ? ", "+item.kupac.sjediste : "" );

        
      };
      
      
      // -------

      new_items.push({
        ...item,
        kupac_podaci: full_partner,
        referent_ime,
        referent_zamjena_ime,
        status_naziv: ( item.status ? item.status.naziv : "" )
      });

    }); // kraj loopa po svim items

    return new_items;

  };
  this_module.find_project_filter = find_project_filter;
  
  
  async function open_project(state) {
    
    
    // window.scroll_elems_ids = [];
    
    
    console.log(state);
    
    // ako je state null onda STANI !!!!
    if ( !state ) return;

    var state = cit_deep(state);

    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    // ako iz nekog razloga nije uspio loadati project module
    if ( !this_module.project_module ) {
      console.error(`PROJECT MODULE NIJE UČITAN`);
      return;
    };

    var proj_sifra = state.proj_sifra;
        
    if ( !proj_sifra ) {
      popup_error(`Nije moguće otvoriti projekt jer nedostaje sifra projekta !!!<br>Greška na serveru.<br>Nazovite broj 092 3133 015`);
      return;
    };

    var db_result = null;
    

    var props = {
      filter: this_module.find_project_filter,
      url: '/find_proj',
      query: { proj_sifra: proj_sifra },
      desc: `pretraga u PIS module unutar cit run za pretragu projekta`,
    };
    
    try {
      db_result = await search_database(null, props );
    } catch (err) {
      console.log(err);
    };
    
    // ako je našao nešto onda uzmi samo prvi član u arraju
    if ( db_result && $.isArray(db_result) && db_result.length > 0 ) {

      db_result = db_result[0];

      var criteria = [ 'order_num' ];
      if ( db_result.items && $.isArray(db_result.items) && db_result.items.length > 0 ) multisort( db_result.items, criteria );
      
      var project_data = {
        ...db_result,
        id: `project_module`,

        goto_item: (state.item_sifra || null),
        goto_variant: ( state.variant || null),
        goto_status: ( state.status || null),
        
      };

      var hash_data = {
        project: state.proj_sifra,
        item: (state.item_sifra || null),
        variant: (state.variant || null),
        status: (state.status || null),
      };

      update_hash(hash_data);

      this_module.project_module.create( project_data, $(`#cont_main_box`) );

    };
    
    /*
    else {

      popup_error(`Došlo je do greške!!!<br> Ne mogu pronaći ovaj projekt :(`);

    };  
    */
    
  };
  this_module.open_project = open_project;
  
  
  this_module.cit_loaded = true;
 
} // end of module scripts
};

