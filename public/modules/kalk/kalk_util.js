


function is_start_proces(arg_proc) {

  var is_start = false;
  var real_sirov = 0;

  $.each(arg_proc.ulazi, function(au_ind, arg_ulaz) {

    if ( 
      // ako ima tip sirovine u ULAZU NE SMJE BITI POMOĆNI TIP ----> i mora imati _id i NE SMJE IMATI KALK ELEMENT
      ( !arg_ulaz.kalk_element && arg_ulaz._id && arg_ulaz.tip_sirovine && arg_ulaz.tip_sirovine.sifra !== `TSIR11` )
      ||
      ( !arg_ulaz.kalk_element && arg_ulaz.kalk_plate_id ) // također može biti montažna ploča

    ) {

      real_sirov += 1;

    };

  });


  if ( real_sirov == 1 ) {
    is_start = true;
  };

  return is_start;

};




function find_extra_data_ulaz_price( extra_data, arg_sifra ) {

  
  var extra_price = null;
  
  if ( !extra_data ) return null;
  if ( !extra_data.ulaz_prices ) return null;
  
  $.each( extra_data.ulaz_prices, function( ulaz_key, price ) {
    if ( ulaz_key == arg_sifra ) extra_price = price;
  });  
  
  return extra_price;
  
};


function convert_db_sirov_to_kalk_sirov( sir, curr_ulaz ) {

  
  var sir = reduce_sir(sir);
  
  // ako je ljepenka
  var sirov_area = (sir.po_valu || 0) * (sir.po_kontravalu || 0);
  // ako je papir
  if ( !sirov_area ) sirov_area = (sir.kontra_tok || 0) * (sir.tok || 0);

  var full_name = sir.sirovina_sifra + "--" + sir.full_naziv + "--" + sir.dobavljac?.naziv;

  var new_ulaz = { 
    
    original_sirov: sir,

    sifra: curr_ulaz?.sifra || "ulaz"+cit_rand(),

    _id: sir._id,
    kalk_full_naziv: full_name,

    kalk_element: null,

    kalk_po_valu: sir.po_valu || sir.kontra_tok || null, // ljepenka i papir imaju različita mjesta dimenzija
    kalk_po_kontravalu: sir.po_kontravalu || sir.tok || null,

    uzimam_po_valu: null,
    uzimam_po_kontravalu: null,

    kalk_ostatak_val: null,
    kalk_ostatak_kontra: null,

    kalk_skart: null,

    kalk_unit_area: sirov_area/1000000,

    /* unit i unit price upisujem odmah prilikom odabira - kasnije se može mijenjati kada proše grod best plate find */
    kalk_unit: sir.osnovna_jedinica?.jedinica || null,

    kalk_unit_price: sir.cijena || null,
    real_unit_price: sir.cijena || null,

    kalk_price: null,
    kalk_kom_in_product: null,
    kalk_kom_in_sirovina: 1,

    kalk_kom_val: null,
    kalk_kom_kontra: null,

    kalk_sum_sirovina: curr_ulaz?.kalk_sum_sirovina || 1,
    kalk_skart: null,

    kalk_extra_kom:  curr_ulaz?.kalk_extra_kom || 0,

    kalk_noz_big: null,
    kalk_nest_count: null,
    kalk_color_cover: null,

    kalk_color_C: null,
    kalk_color_M: null,
    kalk_color_Y: null,
    kalk_color_K: null,
    kalk_color_W: null,
    kalk_color_V: null,

    vrsta_materijala: sir.vrsta_materijala || null,
    klasa_sirovine: sir.klasa_sirovine || null,
    tip_sirovine: sir.tip_sirovine || null,

    kvaliteta_1: sir.kvaliteta_1 || null,
    kvaliteta_2: sir.kvaliteta_2 || null,
    debljina_mm: sir.debljina_mm || null,
    gramaza_g: sir.gramaza_g || null,


  };

  return new_ulaz;


};


var map_extra_data_to_text = {

  ulaz_val: "ULAZ VAL", 
  ulaz_kontra: "ULAZ KONTRA.", 
  izlaz_val: "IZLAZ VAL",
  izlaz_kontra: "IZLAZ KONTRA.",
  color_cover: "POKRIV. BOJOM %",
  proces_radni_nalog: "DOPUNA RN",
  proces_drop: "ODUSTAO",

  multiload: "MULTILOAD",
  kasir_num: "BROJ KAŠIR.",
  

  forme: "FORMA",
  alat_apart: "ALAT ODVOJENO",

    
  };

window.scitex_colors_ids = {

  light_magenta : "62665e3f156e13cf69c28f05",
  magenta : "62665d74156e13cf69c28ea0",

  light_cyan : "62665de6156e13cf69c28ed8",
  cyan : "62629402c104e83dcf095a97",

  black : "62665e12156e13cf69c28ee7",
  yellow : "62665db2156e13cf69c28ec0",

};

window.glue_classes = [

  { sifra: `KS8LJEP`,      naziv: `LJEPILO` },
  { sifra: `KS8VRG`,       naziv: `LJEPILO VRUĆE - GRANULE` },
  { sifra: `KS8VRPAT`,     naziv: `LJEPILO VRUĆE - PATRONE` },
  { sifra: `KS8SPECGLUE`,  naziv: `LJEPILO - SPECIJALNO` },
  { sifra: `KS8COLDGLUE`,  naziv: `LJEPILO HLADNO` },
  { sifra: `KS8FOREXGLUE`, naziv: `LJEPILO - FOREX` },
  { sifra: "KS125",        naziv: "LJEPILO HLADNO POLIM. - KAŠIRANJE" },
  { sifra: "KS125KASHL",   naziv: "LJEPILO HLADNO VODENO - KAŠIRANJE" },

];

// pronadji sve ISTE sirovine u svim ostalim ulazima osim ovog ulaza
// i zbroj količine

function all_other_kolicina( curr_kalk, arg_ulaz ) {

  var other_kolicina_sum = 0;

  $.each( curr_kalk.procesi, function( p_ind, proces ) {


    $.each( proces.ulazi, function( u_ind, ulaz ) {


      // ako ima _id && NEMA KALK ELEMENT !!!!
      if (
        ulaz._id && !ulaz.kalk_element 
        &&
        ulaz._id == arg_ulaz._id // ulaz mora imati istu sirovinu
        &&
        ulaz.sifra !== arg_ulaz.sifra // ali mora preskočit trenutni ulaz po kojem traži sve ostale
      ) {
        other_kolicina_sum += kom_plus_sum_plus_extra(ulaz);
      }; // kraj ako ima _id i NIJE NEMA KALK ELEM i nije isti ulaz

    });


  });


  return other_kolicina_sum;

};


function get_ulaz_mat(ulaz) {
  
  var mater = null;
  var found_mat = false;
  
  $.each( window.cit_local_list.mat_mix, function(m_ind, mat_obj ) {
    
    function IS_B_val( ulaz ) {
    
      var match = false;
      
      if ( ulaz.vrsta_materijala?.sifra == `VM3` ) {
        
        if ( 
          
          ulaz.vrsta_materijala?.sifra == mat_obj.sifra_mat
          &&
          ulaz.kvaliteta_1 == mat_obj.kvaliteta_1
          &&
          ulaz.kvaliteta_2 == mat_obj.kvaliteta_2
          &&
          (mat_obj.kvaliteta_2.toLowerCase() == `b`? ulaz.gramaza_g == mat_obj.gramaza_g : true) /* provjeri jel ista gramaža samo ako je ovo B VAL */
          
        ) {
          
          match = true;
          
        }
        
      };
      
      
      return match;
      
    };
    
    function NOT_val( ulaz ) {
    
      var match = false;
      
      if ( ulaz.vrsta_materijala?.sifra !== `VM3` ) {
        
        if ( 
          ulaz.vrsta_materijala?.sifra == mat_obj.sifra_mat
          &&
          ulaz.debljina_mm == mat_obj.debljina_mm
        ) {
          match = true;
        };
        
      };
      
      return match;
      
    };
    
    if ( found_mat == false && IS_B_val(ulaz) ) {
      mater = mat_obj;
      found_mat = true; // čim nadje prvi onda prestani tražiti
    };
    
    if ( found_mat == false && NOT_val(ulaz) ) {
      mater = mat_obj;
      found_mat = true; // čim nadje prvi onda prestani tražiti
    };
    
  });
   
  return mater;
  
};


function kom_plus_sum_plus_extra(obj, koef) {
  
  // koeficijent mi služi da podjelim ili pomnožim kad želim pretvoriti na primjer mili litre u litre  ili slično
  // ponekad želim prikazati količinu u mililitrima unutar kalkulacije ali unutra sirov recorda želim da bude u litrama !!!!
  
  var in_sir = obj.kalk_kom_in_sirovina || 1; // jedinična količina  ---> uglavnom = 1, ali kad se radi na primje o scitex boji može biti 0,005 kg
  var sum_sir = obj.kalk_sum_sirovina || 0;
  var extra_sir = obj.kalk_extra_kom || 0;
  
  var result = in_sir * ( sum_sir + extra_sir );
  
  return result * ( koef || 1 );
  
};


function sum_plus_extra(obj, koef) {
  
  // koeficijent mi služi da podjelim ili pomnožim kad želim pretvoriti na primjer mili litre u litre  ili slično
  // ponekad želim prikazati količinu u mililitrima unutar kalkulacije ali unutra sirov recorda želim da bude u litrama !!!!
  var result = ( (obj.kalk_sum_sirovina || 0) + (obj.kalk_extra_kom || 0) );
  
  return result * ( koef || 1 ); 
  
};



function gen_dobiveno_ulaz(arg_ulazi) {

  var ukupno_dobiveno_iz_ulaza = 0;

  $.each(arg_ulazi, function(au_ind, arg_ulaz) {


    if ( 
      (arg_ulaz.tip_sirovine && arg_ulaz.tip_sirovine.sifra !== `TSIR11`) // ako ima tip sirovine u ULAZU  NE SMJE BITI POMOĆNI TIP
      ||
      (arg_ulaz.original_sirov?.tip_sirovine && arg_ulaz.original_sirov?.tip_sirovine?.sifra !== `TSIR11`) // ako ima tip sirovine u ORIG SIROV  NE SMJE BITI POMOĆNI TIP
      ||
      arg_ulaz.kalk_element // ili ako je jedostavno neki element koji je nastao u prijašnjem procesu
      
    ) {
      ukupno_dobiveno_iz_ulaza += sum_plus_extra(arg_ulaz);
    };

  });

  return ukupno_dobiveno_iz_ulaza;

};



function sum_ulaz_kom_val_kontra(arg_ulazi) {

  var sum_ulaz_kom = 0;
  var sum_ploca_val = 0;
  var sum_ploca_kontra = 0;

  $.each( arg_ulazi, function( u_ind, ulaz ) {

    if ( 
      (ulaz.tip_sirovine && ulaz.tip_sirovine.sifra !== `TSIR11`)
      ||
      (ulaz.original_sirov?.tip_sirovine && ulaz.original_sirov?.tip_sirovine?.sifra !== `TSIR11`)
      ||
      ulaz.kalk_element // ili ako je jedostavno neki element koji je nastao u prijašnjem procesu
    ) {
    
    sum_ulaz_kom += sum_plus_extra(ulaz);
    
    sum_ploca_val += (ulaz.kalk_po_valu || 0) * sum_plus_extra(ulaz);
    sum_ploca_kontra += (ulaz.kalk_po_kontravalu || 0) * sum_plus_extra(ulaz);
      
    };

  });

  return {
    kom: sum_ulaz_kom,
    val: sum_ploca_val,
    kontra: sum_ploca_kontra
  }

};


function find_elem_alate(product_data, curr_proces, arg_curr_ulaz) {


  var alati = [];

  $.each( product_data.elements, function(e_ind, elem) {

    $.each( elem.elem_plates, function(plate_ind, plate) {

      if ( 
        plate.plate_id == arg_curr_ulaz?.kalk_plate_id
        &&
        elem.elem_alat?.length > 0 // ako taj element ima u sebi jedan ili više alata
      ) {

        $.each(  elem.elem_alat, function(alat_ind, alat) {
          alati.push(alat);
        }); // loop po svim alatima za ovaj element

      }; // jel plate unuter elementa je isti plate koji je izabran unutar ovog procesa

    });  // po svim plate unutar nekog elementa

  }); // lopp po svim elementima u productu


  return alati;


};


function make_extra_data_pill( data, kalk, kalk_type, proces_index, extra_key, extra_value) {

  var delete_btn = ``;

  // kada je oznaka extra proces onda se to ne može brisati
  // ali ako je extra proces kopiran iz pro_kalk u offer_kalk ( kada user radi variant_of ili original !!! )
  // onda ipak dopusti user da to obriše

  var color_palete = [`#d3097e`, `#76af00`, `#800552`, `#cc5d29`, `#31a299` ];

  var plate_names = [];

  $.each( kalk.procesi, function(p_ind, proc) {

    $.each( proc.ulazi, function(proc_ulaz_ind, proc_ulaz) {

      if ( proc_ulaz.kalk_plate_id ) {

        var curr_plate_name = proc_ulaz.kalk_plate_naziv.split(` ---`)[0];


        /*
        var has_name = false;
        $.each( plate_names, function(pl_ind, plate_name) {
          if ( plate_name == curr_plate_name) has_name = true;
        });
        if (has_name == false ) plate_names.push(curr_plate_name);
        */


        plate_names = insert_uniq(plate_names, curr_plate_name);

      }; // has plate id

    }); // loop ulazi

  }); // loop procesi


  if ( kalk_type == `offer_kalk` ) {
    delete_btn = `<div class="delete_extra_pill"><i class="fas fa-times"></i></div>`;
  };

  if ( extra_key == `forme` ) delete_btn = ``;
  if ( extra_key == `proces_radni_nalog` ) delete_btn = ``;

  if ( extra_key == `alat_apart` ) {
    extra_value = ``;
    delete_btn = ``;
  };

  var pill_html = ``;

  
   
  if ( extra_key == `forme` ) {

    var forme_arr = extra_value; // samo sam promjenio ime da bude jasnije što je value !!! pošto je to array naziva od ploča !!!!
    $.each( forme_arr, function(f_ind, forma_naziv)  {

      var pill_bg = ``;

      // ako je ovo extra data FORME
      // onda pogledaj koji je to index u arraju svih naziva ploča
      // i taj isti index uzmi da bude boja u arraju predefiniranih boja !!!
      if ( plate_names.indexOf(forma_naziv) > -1 ) {
        var modulo = plate_names.indexOf(forma_naziv) % color_palete.length;
        pill_bg = `style="background: ${ color_palete[ modulo ] }"`;
      };

      pill_html += `<div class="extra_data_pill ${extra_key}" ${pill_bg}>FORMA ${forma_naziv} ${delete_btn}</div>`;

    });
    
    
    
  };

  
  
  /*
  ------------------------------------------------------------------------------------------
  ------------------------------------------------------------------------------------------
  
  if ( extra_key == `kalk_glue_tlak` ) {
    pill_html = `<div class="extra_data_pill ${extra_key}" >TLAK ${extra_value.naziv} </div>`;
  };
  
    
  if ( extra_key == `kalk_glue_speed` ) {
    pill_html = `<div class="extra_data_pill ${extra_key}" >BRZINA LJEP. ${extra_value} </div>`;
  };
  
  
  if ( extra_key == `kalk_glue_dizna` ) {
    pill_html = `<div class="extra_data_pill ${extra_key}" >DIZNA ${extra_value.naziv} </div>`;
  };
  
  if ( extra_key == `kalk_glue_m2` ) {
    pill_html = `<div class="extra_data_pill ${extra_key}" >LJEP. ELEM m<sup>2</sup> ${extra_value.naziv} </div>`;
  };
  
  ------------------------------------------------------------------------------------------
  ------------------------------------------------------------------------------------------
  */
  
  // <sup>2</sup>
  
  if ( extra_key == `action_prep_time` ) {
    pill_html = `<div class="extra_data_pill ${extra_key}" >PREP TIME ${extra_value} </div>`;
  };
  
  if ( extra_key == `action_work_time` ) {
    pill_html = `<div class="extra_data_pill ${extra_key}" >WORK TIME ${extra_value} </div>`;
  };
  
  if ( extra_key == `izlaz_kom` ) {
    pill_html = `<div class="extra_data_pill ${extra_key}" >IZLAZ KOM. ${extra_value.kom} </div>`;
  };
  
    
  if ( extra_key == `satnica_prep` ) {
    pill_html = `<div class="extra_data_pill ${extra_key}" >SAT. PREP. ${extra_value} </div>`;
  };
  
  if ( extra_key == `satnica_radnik` ) {
    pill_html = `<div class="extra_data_pill ${extra_key}" >SAT. RADNIK ${extra_value} </div>`;
  };
  
  if ( extra_key == `satnica_stroj` ) {
    pill_html = `<div class="extra_data_pill ${extra_key}" >SAT. STROJ ${extra_value} </div>`;
  };
  
  
  if ( extra_key == `radnik_count` ) {
    pill_html = `<div class="extra_data_pill ${extra_key}" >BROJ RAD. STROJ ${extra_value} </div>`;
  };
  
  if ( extra_key == `radnik_count_prep` ) {
    pill_html = `<div class="extra_data_pill ${extra_key}" >BROJ RAD. PREP. ${extra_value} </div>`;
  };
  
  
  // ako nije niti jedan od ovih gore opcija onda je pill html još uvijek prazan
  if ( !pill_html ) pill_html = `<div class="extra_data_pill ${extra_key}" >${ map_extra_data_to_text[extra_key] } ${extra_value} ${delete_btn } </div>`;
  
  
  // nemoj raditi pill ako su custom ulazne cijene
  if ( extra_key == `ulaz_prices` ) {
    
    pill_html = ``; // tj vrati prazan string;
    
  };
  
  
  return pill_html;

};


function create_kalk_naslov(data, kalk) {

  var kalk_naslov_html =
`

<span class="title_tip">${ data.tip ? data.tip.naziv : "---" }</span>

<span class="title_circle"></span>
<span class="title_naklada">${ kalk.kalk_naklada ?  cit_format(kalk.kalk_naklada, 0) : "---" }</span> kom

<span class="title_circle"></span>
<span class="title_mat">${ kalk.kalk_ploca_mat?.naziv || "" }</span>

<span class="title_circle"></span>
<span class="title_unit_price">${ kalk.kalk_final_unit_price_dis_mark ? cit_format( kalk.kalk_final_unit_price_dis_mark, 2)+" kn" : "" }</span>

<span class="title_circle"></span>
<span class="title_unit_price">${ kalk.kalk_final_sum_price_dis_mark ? cit_format( kalk.kalk_final_sum_price_dis_mark, 2)+" kn" : "" }</span>


<span class="title_circle"></span>
<span class="title_mark">M: ${ kalk.kalk_markup ? cit_format( kalk.kalk_markup, 2) : "" }</span>

<span class="title_circle"></span>
<span class="title_discount">R: ${ kalk.kalk_discount? cit_format( kalk.kalk_discount, 2) : "" }</span>

<span class="title_circle"></span>
<span class="title_sloter"></span>


`;


  return kalk_naslov_html;

};


function get_all_kalk_ulazi_sirovs(curr_kalk, arg_proces ) {

  var sirov_ulazi = [];

  $.each( curr_kalk.procesi, function( p_ind, proces ) {

    function filter_proces(curr_proces, arg_proces) {
      var is_ok = null;
      if ( !arg_proces ) {
        // ako uopće nema procesa onda pusti sve !!!!
        is_ok = true;
      } else {
        // ako imam argument proces onda pusti samo ako se ispravan proces 
        if ( curr_proces.row_sifra == arg_proces.row_sifra ) is_ok = true;
      };
      return is_ok;

    };

    if ( filter_proces( proces, arg_proces ) ) {

      $.each( proces.ulazi, function( u_ind, ulaz ) {
        

        // ako ima _id && NEMA KALK ELEMENT !!!!
        if ( ulaz._id && !ulaz.kalk_element ) {
          
          var kolicina_koef = 1;

          
          /*
          
          // ------------------------------ AKO JE SCITEX BOJA ONDA JE KOLIČINA u polju prikazana U MILI LITRAMA PA MORAM PODJELIT SA 1000 !!!
          $.each(window.scitex_colors_ids, function(color_name_key, color_id ) {
            if ( ulaz._id == color_id ) kolicina_koef = 1/1000;
          });


          // ------------------------------ AKO JE LJEPILO  --- PRIKAZUJEM GA U MILI GRAMIMA PA TREBAM PODJELITI SA 10000
          var sir_klasa_sifra = ulaz.klasa_sirovine?.sifra || ulaz.original_sirov?.klasa_sirovine?.sifra;
          var is_glue = find_item( window.glue_classes, sir_klasa_sifra, "sifra");
          if ( is_glue ) kolicina_koef = 1/1000;
          
          */
          
          
          
          var already_in = find_item(sirov_ulazi, ulaz._id || ``, `_id`);

          if ( already_in ) {
            // ako već imam ovu sirovinu u prijašnjem ulazu onda samo DODAJ količine NA POSTOJEĆU SIROVINU !!!!
            var index = find_index(sirov_ulazi, ulaz._id || ``, `_id`);
            
            sirov_ulazi[index].sum_kolicina += kom_plus_sum_plus_extra(ulaz) * kolicina_koef;

          }
          else {
            
            // kreiram novi sirov ulaz !!!
            // kreiram novi sirov ulaz !!!
            // kreiram novi sirov ulaz !!!
            
            var sirov_obj = {

              ...ulaz,

              _id: ulaz._id,
              kalk_full_naziv: ulaz.kalk_full_naziv,
              kalk_kom_in_sirovina: ulaz.kalk_kom_in_sirovina || 0,
              kalk_sum_sirovina: ulaz.kalk_sum_sirovina || 0,
              kalk_extra_kom: ulaz.kalk_extra_kom || 0,
              kalk_skart: 0,

              vrsta_materijala: ulaz.vrsta_materijala || null,
              klasa_sirovine: ulaz.klasa_sirovine || null,
              tip_sirovine: ulaz.tip_sirovine || null,


              kvaliteta_1: ulaz.kvaliteta_1 || null,
              kvaliteta_2: ulaz.kvaliteta_2 || null,
              debljina_mm: ulaz.debljina_mm || null,
              gramaza_g: ulaz.gramaza_g || null,

              sum_kolicina: kom_plus_sum_plus_extra(ulaz) * kolicina_koef,


            };
            
            
            sirov_ulazi.push(sirov_obj);
            
          }; // kraj else ( ako je ovo novi ulaz )

        }; // kraj ako ima _id i NIJE NEMA KALK ELEM

      }); // kraj loopa po svim ulazim au ovom procesu !!!


    }; // ako je samo jedan proces ------> filter jednog procesa ili za sve procese

  }); // loop po svim procesima u ovom kalku


  return sirov_ulazi;

};


async function get_db_sirovs(sirov_ulazi, product_data ) {

  var sirov_ids = [];

  $.each( sirov_ulazi, function( s_ind, sirov ) {
    // ponekad samo dajem idjeve, a ponekad dajem cijele objekte od sirovina
    if ( typeof sirov == "string" ) { sirov_ids.push(sirov); }
    else { sirov_ids.push(sirov._id); };
    
  });


  if ( sirov_ids.length == 0 ) return [];

  var props = {
    filter: null,
    url: '/find_sirov',
    query: { _id: { $in:  sirov_ids }, on_data: product_data?.rok_za_def || null },
    desc: `pretraga sirovina za offer ili order reserv unutar kalkulacija ` + cit_rand(),
  };

  var db_result = [];

  try {
    db_result = await search_database(null, props );
  } catch (err) {
    console.log(err);
    return null;
  };

  if ( db_result && $.isArray(db_result) && db_result.length > 0 ) {
    return db_result;
  }; 


};


function reduce_kalk_original_sirovs(kalk_obj) {
  

  if ( kalk_obj.offer_kalk?.length > 0 ) {
    $.each(kalk_obj.offer_kalk, function( kalk_ind, arg_kalk ) {
      if (arg_kalk.procesi?.length > 0 ) {   
        $.each(arg_kalk.procesi, function( proc_ind, arg_proc ) {
          if ( arg_proc.ulazi?.length > 0 ) {
            $.each(arg_proc.ulazi, function( ul_ind, arg_ulaz ) {
              if( arg_ulaz.original_sirov && $.isPlainObject( arg_ulaz.original_sirov ) && arg_ulaz.original_sirov._id ) {
                kalk_obj.offer_kalk[kalk_ind].procesi[proc_ind].ulazi[ul_ind].original_sirov = arg_ulaz.original_sirov._id;
              };
            }); 
          };
        });  
      }
    });
  };
  
  if ( kalk_obj.pro_kalk?.length > 0 ) {
    $.each(kalk_obj.pro_kalk, function( kalk_ind, arg_kalk ) {
      if (arg_kalk.procesi?.length > 0 ) {   
        $.each(arg_kalk.procesi, function( proc_ind, arg_proc ) {
          if ( arg_proc.ulazi?.length > 0 ) {
            $.each(arg_proc.ulazi, function( ul_ind, arg_ulaz ) {
              if( arg_ulaz.original_sirov && $.isPlainObject( arg_ulaz.original_sirov ) && arg_ulaz.original_sirov._id ) {
                kalk_obj.pro_kalk[kalk_ind].procesi[proc_ind].ulazi[ul_ind].original_sirov = arg_ulaz.original_sirov._id;
              };
            }); 
          };
        });  
      }
    });
  };
  
  if ( kalk_obj.post_kalk?.length > 0 ) {
    $.each(kalk_obj.post_kalk, function( kalk_ind, arg_kalk ) {
      if (arg_kalk.procesi?.length > 0 ) {   
        $.each(arg_kalk.procesi, function( proc_ind, arg_proc ) {
          if ( arg_proc.ulazi?.length > 0 ) {
            $.each(arg_proc.ulazi, function( ul_ind, arg_ulaz ) {
              if( arg_ulaz.original_sirov && $.isPlainObject( arg_ulaz.original_sirov ) && arg_ulaz.original_sirov._id ) {
                kalk_obj.post_kalk[kalk_ind].procesi[proc_ind].ulazi[ul_ind].original_sirov = arg_ulaz.original_sirov._id;
              };
            }); 
          };
        });  
      }
    });
  };  
  

  return kalk_obj;
  
  
};


async function populate_kalk_original_sirovs(kalk_obj) {
  
  
  var sir_ids = [];
  
  if ( kalk_obj.offer_kalk?.length > 0 ) {
    $.each(kalk_obj.offer_kalk, function( kalk_ind, arg_kalk ) {
      if (arg_kalk.procesi?.length > 0 ) {   
        $.each(arg_kalk.procesi, function( proc_ind, arg_proc ) {
          if ( arg_proc.ulazi?.length > 0 ) {
            $.each(arg_proc.ulazi, function( ul_ind, arg_ulaz ) {
              if( arg_ulaz.original_sirov ) {
                sir_ids = insert_uniq(sir_ids, arg_ulaz.original_sirov?._id || arg_ulaz.original_sirov);
              };
            }); 
          };
        });  
      }
    });
  };
  
  
  if ( kalk_obj.pro_kalk?.length > 0 ) {
    $.each(kalk_obj.pro_kalk, function( kalk_ind, arg_kalk ) {
      if (arg_kalk.procesi?.length > 0 ) {   
        $.each(arg_kalk.procesi, function( proc_ind, arg_proc ) {
          if ( arg_proc.ulazi?.length > 0 ) {
            $.each(arg_proc.ulazi, function( ul_ind, arg_ulaz ) {
              if( arg_ulaz.original_sirov ) {
                sir_ids = insert_uniq(sir_ids, arg_ulaz.original_sirov?._id || arg_ulaz.original_sirov);
              };
            }); 
          };
        });  
      }
    });
  };
  

  if ( kalk_obj.post_kalk?.length > 0 ) {
    $.each(kalk_obj.post_kalk, function( kalk_ind, arg_kalk ) {
      if (arg_kalk.procesi?.length > 0 ) {   
        $.each(arg_kalk.procesi, function( proc_ind, arg_proc ) {
          if ( arg_proc.ulazi?.length > 0 ) {
            $.each(arg_proc.ulazi, function( ul_ind, arg_ulaz ) {
              if( arg_ulaz.original_sirov ) {
                sir_ids = insert_uniq(sir_ids, arg_ulaz.original_sirov?._id || arg_ulaz.original_sirov);
              };
            }); 
          };
        });  
      }
    });
  };
  

  var db_sirovine = await get_db_sirovs(sir_ids);
  
  var db_sirovine_map = {};
  $.each( db_sirovine, function( sir_ind, sir ) {
    db_sirovine_map[sir._id] = reduce_sir(sir);
  });
  
    
  if ( db_sirovine === null ) {
    popup_warn(`Nije došlo do populacije sirovina u kalkulaciji zbog greške na serveru !!!`);
    return kalk_obj;
  };
  
  
  if ( kalk_obj.offer_kalk?.length > 0 ) {
    $.each(kalk_obj.offer_kalk, function( kalk_ind, arg_kalk ) {
      if (arg_kalk.procesi?.length > 0 ) {   
        $.each(arg_kalk.procesi, function( proc_ind, arg_proc ) {
          if ( arg_proc.ulazi?.length > 0 ) {
            $.each(arg_proc.ulazi, function( ul_ind, arg_ulaz ) {
              if( arg_ulaz.original_sirov ) {
                kalk_obj.offer_kalk[kalk_ind].procesi[proc_ind].ulazi[ul_ind].original_sirov = db_sirovine_map[arg_ulaz.original_sirov?._id || arg_ulaz.original_sirov ] || null;
              };
            }); 
          };
        });  
      }
    });
  };
  
  
  if ( kalk_obj.pro_kalk?.length > 0 ) {
    $.each(kalk_obj.pro_kalk, function( kalk_ind, arg_kalk ) {
      if (arg_kalk.procesi?.length > 0 ) {   
        $.each(arg_kalk.procesi, function( proc_ind, arg_proc ) {
          if ( arg_proc.ulazi?.length > 0 ) {
            $.each(arg_proc.ulazi, function( ul_ind, arg_ulaz ) {
              if( arg_ulaz.original_sirov ) {
                kalk_obj.pro_kalk[kalk_ind].procesi[proc_ind].ulazi[ul_ind].original_sirov = db_sirovine_map[arg_ulaz.original_sirov?._id || arg_ulaz.original_sirov ] || null;
              };
            }); 
          };
        });  
      }
    });
  };
  

  if ( kalk_obj.post_kalk?.length > 0 ) {
    $.each(kalk_obj.post_kalk, function( kalk_ind, arg_kalk ) {
      if (arg_kalk.procesi?.length > 0 ) {   
        $.each(arg_kalk.procesi, function( proc_ind, arg_proc ) {
          if ( arg_proc.ulazi?.length > 0 ) {
            $.each(arg_proc.ulazi, function( ul_ind, arg_ulaz ) {
              if( arg_ulaz.original_sirov ) {
                kalk_obj.post_kalk[kalk_ind].procesi[proc_ind].ulazi[ul_ind].original_sirov = db_sirovine_map[arg_ulaz.original_sirov?._id || arg_ulaz.original_sirov ] || null;
              };
            }); 
          };
        });  
      }
    });
  };
 
  return kalk_obj;
  
};


// ------------------------------------------------- SVE WORK FUNC ----------------------------------------------------------------------
// ------------------------------------------------- SVE WORK FUNC ----------------------------------------------------------------------

var VELPROCES = {};

/*

function kalk_ulaz_price( this_module, arg_kalk, arg_proces, arg_ulaz  ) {
  
};

*/



function kalk_scitex_or_fuji( norma_boje, this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index) {
  
  
  
  var missing_color = false;
  var multiload = 1;
  
  
  // ako postoji multiload u extra data onda uzmi taj broj
  if ( arg_extra_data && arg_extra_data.multiload ) {
    multiload = arg_extra_data.multiload;
  };

  $.each( arg_ulazi, function( u_ind, ulaz ) {

    var {
        kalk_color_C,
        kalk_color_M,
        kalk_color_Y,
        kalk_color_K,
        kalk_color_W,
        kalk_color_V,
      } = ulaz;
    

    if (

        (
          kalk_color_C ||
          kalk_color_M ||
          kalk_color_Y ||
          kalk_color_K ||
          kalk_color_W ||
          kalk_color_V
        )
      &&
      missing_color == false
      && 
      ulaz.klasa_sirovine?.sifra !== `KSSBO` // ne smjem uvrstiti scitex boje  ---> te boje ignoriram  u ovom loopu jer UPRAVO njih upisujem

    ) {

      // više ne koristim ovo ---> zato što računam cijenu i utrošak za svaku sirovinu
      // više ne koristim ovo
      // var color_price = calc_color( kalk_color_C, kalk_color_M, kalk_color_Y, kalk_color_K, kalk_color_W, kalk_color_V, `scitex_work` );
      
      // POLA STAVI U CYAN
      if ( kalk_color_C ) {
        
        var color_index = find_index( arg_ulazi, window.scitex_colors_ids.cyan , `_id` );
        
        // stavi da količina boje bude ista kao količina forme ili ploče
        arg_ulazi[color_index].kalk_sum_sirovina = ulaz.kalk_sum_sirovina || 0;
        arg_ulazi[color_index].kalk_extra_kom = ulaz.kalk_extra_kom || 0;
        
        arg_ulazi[color_index].kalk_kom_in_sirovina = norma_boje * kalk_color_C/1000000;
        
        // POLA STAVI U CYAN A POLA STAVI U LIGHT CYAN
        arg_ulazi[color_index].kalk_kom_in_sirovina = arg_ulazi[color_index].kalk_kom_in_sirovina / 2; 
        
        arg_ulazi[color_index].real_unit_price = arg_ulazi[color_index].kalk_kom_in_sirovina * arg_ulazi[color_index].original_sirov.cijena;
        arg_ulazi[color_index].kalk_price = arg_ulazi[color_index].real_unit_price * sum_plus_extra(arg_ulazi[color_index]);
        
      };
      
      // POLA STAVI U LIGHT CYAN
      if ( kalk_color_C ) {
        var color_index = find_index( arg_ulazi, window.scitex_colors_ids.light_cyan , `_id` );
        
        // stavi da količina boje bude ista kao količina forme ili ploče
        arg_ulazi[color_index].kalk_sum_sirovina = ulaz.kalk_sum_sirovina || 0;
        arg_ulazi[color_index].kalk_extra_kom = ulaz.kalk_extra_kom || 0;
        
        arg_ulazi[color_index].kalk_kom_in_sirovina = norma_boje * kalk_color_C/1000000;
        
        // POLA STAVI U CYAN A POLA STAVI U LIGHT CYAN
        arg_ulazi[color_index].kalk_kom_in_sirovina = arg_ulazi[color_index].kalk_kom_in_sirovina / 2; 
        
        arg_ulazi[color_index].real_unit_price = arg_ulazi[color_index].kalk_kom_in_sirovina * arg_ulazi[color_index].original_sirov.cijena;
        arg_ulazi[color_index].kalk_price = arg_ulazi[color_index].real_unit_price * sum_plus_extra(arg_ulazi[color_index]);
        
      };
      
      // POLA STAVI U MAGENTA
      if ( kalk_color_M ) {
        var color_index = find_index( arg_ulazi, window.scitex_colors_ids.magenta , `_id` );
        
        // stavi da količina boje bude ista kao količina forme ili ploče
        arg_ulazi[color_index].kalk_sum_sirovina = ulaz.kalk_sum_sirovina || 0;
        arg_ulazi[color_index].kalk_extra_kom = ulaz.kalk_extra_kom || 0;
        
        arg_ulazi[color_index].kalk_kom_in_sirovina = norma_boje * kalk_color_M/1000000;
        
        // POLA STAVI U CYAN AA POLA STAVI U LIGHT CYAN
        arg_ulazi[color_index].kalk_kom_in_sirovina = arg_ulazi[color_index].kalk_kom_in_sirovina / 2; 
        
        arg_ulazi[color_index].real_unit_price = arg_ulazi[color_index].kalk_kom_in_sirovina * arg_ulazi[color_index].original_sirov.cijena;
        arg_ulazi[color_index].kalk_price = arg_ulazi[color_index].real_unit_price * sum_plus_extra(arg_ulazi[color_index]);
        
      };
      
      // POLA STAVI U LIGHT MAGENTA
      if ( kalk_color_M ) {
        var color_index = find_index( arg_ulazi, window.scitex_colors_ids.light_magenta , `_id` );
        
        // stavi da količina boje bude ista kao količina forme ili ploče
        arg_ulazi[color_index].kalk_sum_sirovina = ulaz.kalk_sum_sirovina || 0;
        arg_ulazi[color_index].kalk_extra_kom = ulaz.kalk_extra_kom || 0;
        
        arg_ulazi[color_index].kalk_kom_in_sirovina = norma_boje * kalk_color_M/1000000;
        
        // POLA STAVI U CYAN AA POLA STAVI U LIGHT CYAN
        arg_ulazi[color_index].kalk_kom_in_sirovina = arg_ulazi[color_index].kalk_kom_in_sirovina / 2; 
        
        arg_ulazi[color_index].real_unit_price = arg_ulazi[color_index].kalk_kom_in_sirovina * arg_ulazi[color_index].original_sirov.cijena;
        arg_ulazi[color_index].kalk_price = arg_ulazi[color_index].real_unit_price * sum_plus_extra(arg_ulazi[color_index]);
        
      };
      
      // YELLOW
      if ( kalk_color_Y ) {
        var color_index = find_index( arg_ulazi, window.scitex_colors_ids.yellow , `_id` );
        
        // stavi da količina boje bude ista kao količina forme ili ploče
        arg_ulazi[color_index].kalk_sum_sirovina = ulaz.kalk_sum_sirovina || 0;
        arg_ulazi[color_index].kalk_extra_kom = ulaz.kalk_extra_kom || 0;
        
        arg_ulazi[color_index].kalk_kom_in_sirovina = norma_boje * kalk_color_Y/1000000;
        
        arg_ulazi[color_index].real_unit_price = arg_ulazi[color_index].kalk_kom_in_sirovina * arg_ulazi[color_index].original_sirov.cijena;
        arg_ulazi[color_index].kalk_price = arg_ulazi[color_index].real_unit_price * sum_plus_extra(arg_ulazi[color_index]);
        
      };
      
      // KEY
      if ( kalk_color_K ) {
        
        var color_index = find_index( arg_ulazi, window.scitex_colors_ids.black , `_id` );
        
        // stavi da količina boje bude ista kao količina forme ili ploče
        arg_ulazi[color_index].kalk_sum_sirovina = ulaz.kalk_sum_sirovina || 0;
        arg_ulazi[color_index].kalk_extra_kom = ulaz.kalk_extra_kom || 0;
        
        arg_ulazi[color_index].kalk_kom_in_sirovina = norma_boje * kalk_color_K/1000000;
        
        arg_ulazi[color_index].real_unit_price = arg_ulazi[color_index].kalk_kom_in_sirovina * arg_ulazi[color_index].original_sirov.cijena;
        arg_ulazi[color_index].kalk_price = arg_ulazi[color_index].real_unit_price * sum_plus_extra(arg_ulazi[color_index]);
        
      };
      
      
      // sum_scitex_work_time +=  sum_plus_extra(ulaz) / work_scitex_speed / multiload;

    }
    else {
      
      // ZA SADA NEĆU TRAŽITI DA UPISUJE BOJE U FORMU
      
      /*
      if ( missing_color == false && ulaz.klasa_sirovine?.sifra !== `KSSBO` ) { // nemoj gladati ako je ulaz zaista prava boja SCITEX BOJA ( KSSBO ) 
        popup_warn(`Potrebno upisati barem površinu za jednu boju za ploče koje ulaze u SCITEX proces !!!`); 
        missing_color = true;
      };
      return;
      */
      
      
    };

  });  // kraj loopa po svim ulazima u ovom procesu
    
  
  
  return arg_ulazi;
  
};

  
function kalk_glue( this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index ) {


  
  var elem_opis = ``;
  
  var glue_count = 0;
  var element_count = 0;
  
  var max_skart = 0;

  
  // ------------------------------------ START LOOPA PO ULAZIMA ------------------------------------
  
  // samo brojim sve varijable koje trebam
  $.each(arg_ulazi, function (u_ind, ulaz) {

    // { sifra: `TSIR11`, naziv: `POMOĆNI ELEMENT` },
    if (
      ulaz.tip_sirovine?.sifra !== `TSIR11` 
      && 
      ulaz.original_sirov?.tip_sirovine?.sifra !== `TSIR11`
    ) {
      element_count += 1;
      elem_opis = `${    ulaz.kalk_element?.elem_tip?.short || "(" + ulaz.kalk_element?.elem_opis + ")"   } + ` + elem_opis;
      
      // uzmi postotak skarta ali samo ako je najveći
      if ( 
        ulaz.kalk_element?.elem_tip?.elem_skart
        &&
        ulaz.kalk_element.elem_tip.elem_skart > max_skart 
      )  {
        max_skart = ulaz.kalk_element.elem_tip.elem_skart;
      };
      
    };

    
    // AKO ovo je shelf lock
    if ( 
      ulaz.klasa_sirovine?.sifra == "KS111" 
      ||
      ulaz.original_sirov?.tip_sirovine?.sifra == "KS111" 
    ) {
      shelf_count += 1
      elem_opis = `LOCK + ` + elem_opis;
    };

    var klasa_sirovine = ulaz.klasa_sirovine?.sifra || ulaz.original_sirov?.klasa_sirovine?.sifra;
    
    if (
      (ulaz.original_sirov?.klasa_sirovine || ulaz.klasa_sirovine) 
      &&
      find_item( window.glue_classes, klasa_sirovine, "sifra")
    ) { 
      // window.glue_classes ovo su sva moguća ljepila TJ SVE KLASE ZA LJEPILA
      glue_count += 1;
    };

  }); // ------------------------------------ KRAJ LOOPA PO ULAZIMA ------------------------------------
  

  // ako ima bilo što u elem opisu ----> dodaj na početak na primjer P3-1 PIŠTOLJ LJEP.
  if ( elem_opis ) elem_opis = `P${ (arg_curr_proces_index+1) + `-1` } IZLAZ LJEPLJENJE -- ` + elem_opis;

  // obriši zadnja dva stringa tj + znak na kraju
  elem_opis = elem_opis.slice(0, -2);


  if (glue_count == 0) {
    popup_warn(`Za proces ${arg_curr_proces_index+1} potrebno je dodati minimalno 1 ljepilo !!!`);
    return null;
  };
  

  if (element_count < 2) {
    popup_warn(`Za proces ${arg_curr_proces_index+1} potrebno je dodati minimalno 2 elementa koja treba zaljepiti !!!`);
    return null;
  };


  arg_changed_props = {
    // mjenjam cijeli objekt za kalk element
    kalk_element: {

      sifra: 'elem' + cit_rand(),
      add_elem: null,
      elem_val: null,
      elem_kontra: null,

      elem_parent: null,

      elem_tip: {
        sifra: arg_radna_stn.elem_sifra,
        naziv: arg_radna_stn.elem_naziv,
        proces: arg_radna_stn.type,
        elem_skart: max_skart || 0,
        kalk: true
      },
      elem_opis: elem_opis,

      elem_cmyk: ``,
      elem_pantone: ``,
      elem_vrsta_boka: null,
      elem_plates: null,

      elem_on_product: 1,

      elem_RB: null,
      elem_polica_razmak: null,
      elem_polica_pregrade: null,
      elem_polica_nosivost: null,
      elem_front_orient: null,
      elem_plate_count: 1,
    },
    
  };
  
  
  return arg_changed_props;
  

}; // kraj kalk_glue




  
function kalk_forming( this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index ) {

  var elem_opis = ``;
  
  var glue_count = 0;
  var element_count = 0;
  
  var max_skart = 0;

  // samo brojim sve varijable koje trebam
  $.each(arg_ulazi, function (u_ind, ulaz) {

    // { sifra: `TSIR11`, naziv: `POMOĆNI ELEMENT` },
    if (
      ulaz.tip_sirovine?.sifra !== `TSIR11` 
      && 
      ulaz.original_sirov?.tip_sirovine?.sifra !== `TSIR11`
    ) {
      element_count += 1;
      elem_opis = `${    ulaz.kalk_element?.elem_tip.short || "(" + ulaz.kalk_element?.elem_opis + ")"   } + ` + elem_opis;
      
      
      // uzmi postotak skarta ali samo ako je najveći
      if ( 
        ulaz.kalk_element?.elem_tip?.elem_skart
        &&
        ulaz.kalk_element.elem_tip.elem_skart > max_skart 
      )  {
        max_skart = ulaz.kalk_element.elem_tip.elem_skart;
      };
      
      
      
    };


  });

  // ako ima bilo što u elem opisu ----> dodaj na početak na primjer P3-1 PIŠTOLJ LJEP.
  if ( elem_opis ) elem_opis = `P${ (arg_curr_proces_index+1) + `-1` } FORMIRANO -- ` + elem_opis;

  // obriši zadnja dva stringa tj + znak na kraju
  elem_opis = elem_opis.slice(0, -2);


  arg_changed_props = {
    
    kalk_element: {

      sifra: 'elem' + cit_rand(),
      add_elem: null,
      elem_val: null,
      elem_kontra: null,

      elem_parent: null,

      elem_tip: {
        sifra: arg_radna_stn.elem_sifra,
        naziv: arg_radna_stn.elem_naziv,
        proces: arg_radna_stn.type,
        elem_skart: max_skart || 0,
        kalk: true
      },
      elem_opis: elem_opis,

      elem_cmyk: ``,
      elem_pantone: ``,
      elem_vrsta_boka: null,
      elem_plates: null,

      elem_on_product: 1,

      elem_RB: null,
      elem_polica_razmak: null,
      elem_polica_pregrade: null,
      elem_polica_nosivost: null,
      elem_front_orient: null,
      elem_plate_count: 1,
    },
  };
  
  
  return arg_changed_props;
  

}; // kraj kalk_forming





function glue_prices( arg_ulazi, min_ulaz_kom, min_ul_extra_kom ) {
  
  var missing_glue_data = false;
  
  // --------------------------------------------------------------------------------------------------------
  // KALK SIROVINE SAMO LJEPILA
  // --------------------------------------------------------------------------------------------------------
  $.each( arg_ulazi, function( u_ind, ulaz ) {
    
    // preskoči ako je već bila greška 
    if (  
      missing_glue_data == false
      &&
      ulaz.klasa_sirovine && find_item( window.glue_classes, ulaz.klasa_sirovine.sifra, "sifra") // jel u ima klasu ljepila ???
    ) { // ovo su sva moguća ljepila
      
      // ako nema točaka ljepljenja neka bude uvijek 1 točka !!!
      if ( !ulaz.kalk_glue_points ) ulaz.kalk_glue_points = 1;
      
      if (
          ( !ulaz.kalk_glue_mm || !ulaz.kalk_glue_dizna || !ulaz.kalk_glue_m2 ) // mora imati duzinu ljepljenja / diznu / okvirno najveći komad kvadratno
          &&
          missing_glue_data == false // nemoj prokazati popup ako sam već prikazao tj prikaži samo kod prve greške
        ) {
        missing_glue_data = true;
        popup_warn(`Za izračun je potrebno upisati:<br>dužinu ukupnog ljepljenja u mm,<br>veličinu dizne<br>i max. veličinu elementa.`);
        return;
      };
      
      if (
          ( !ulaz.kalk_kom_in_sirovina ) // mora biti upisano koliko mase ljepljenje ima u jednom ljepljenju
          &&
          missing_glue_data == false // nemoj prikazati popup ako sam već prikazao tj prikaži samo kod prve greške
        ) {
        missing_glue_data = true;
        popup_warn(`Za izračun je upisati JED. KOLIČINU ljepila za JEDNO ljepljenje (POLJE JED. KOLIČINA).`);
        return;
      };
      
      
      arg_ulazi[u_ind].kalk_sum_sirovina = min_ulaz_kom; // OVO JE SUM + EXTRA !!!!!!!!!!!!!!!!!!!!!!!!!!!
      
      
      arg_ulazi[u_ind].real_unit_price = ulaz.kalk_kom_in_sirovina * ulaz.original_sirov.cijena;
      arg_ulazi[u_ind].kalk_price = arg_ulazi[u_ind].real_unit_price * sum_plus_extra( ulaz );
      
    };

  }); // end loop po ulazima
  
  
  // --------------------------------------------------------------------------------------------------------
  // KALK SIROVINE SAMO SHELF LOCKOVI
  // --------------------------------------------------------------------------------------------------------
  $.each( arg_ulazi, function( u_ind, ulaz ) {
    
    var missing_elem_data = false;
    
    // preskoči ako je već bila greška 
    if (  
      missing_elem_data == false
      &&
      ulaz.klasa_sirovine?.sifra == "KS111" // shelf
    ) { 
    
      if (
          ( !ulaz.kalk_kom_in_sirovina ) // ne smije biti samo 1 jedini shelf
          &&
          missing_elem_data == false // nemoj prokazati popup ako sam već prikazao tj prikaži samo kod prve greške
        ) {
        missing_elem_data = true;
        popup_warn(`Upišite JED. KOLIČINU ZA SHELF LOCKOVE !!!`);
        return;
      };

      arg_ulazi[u_ind].real_unit_price = arg_ulazi[u_ind].kalk_kom_in_sirovina * arg_ulazi[u_ind].original_sirov.cijena;
      arg_ulazi[u_ind].kalk_price = arg_ulazi[u_ind].real_unit_price * sum_plus_extra(arg_ulazi[u_ind]);
      
      
    };

  });
    
  
  
  return arg_ulazi;
  
  
  
};
// END glue_peices



function izlazi_after_clean( this_module, product_data, curr_kalk, ulazi, arg_radna_stn, ukupno_dobiveno_iz_ulaza, arg_curr_proces_index ) {
  
  var all_izlazi = [];
  
  if ( !ulazi[0].kalk_plate_id ) {
    popup_warn(`Nije moguće odrediti izlaze jer niste definirali formu na ulazu i program ne zna koji elementi nastaju nakog optrgavanja !!!`);
    return;
  };
  
  if ( ulazi?.length > 1 ) {
    
    popup_warn(`Za svaku FORMU treba napraviti poseban proces optrgavanja!!!`);
    return;
    
  };
  
  
  // pregledaj sve elemente
  $.each( product_data.elements, function(e_ind, elem) {

    // ako elem ima plates tj forme
    $.each( elem.elem_plates, function(plate_ind, plate) {

      // ako plate id u ovom elementu == id forme u ovom ulazu
      if ( plate.plate_id == ulazi[0].kalk_plate_id ) {

        // kopiraj element
        var elem_copy = cit_deep(elem);


        // upiši u kalk element točan broj koliko tih elemenata stane na baš ovu specifičnu montažnu ploču !!!!
        elem_copy.elem_plate_count = plate.count;
        elem_copy.elem_opis = `P${ arg_curr_proces_index+1 } IZLAZ ${ elem_copy.elem_opis }`,
        elem_copy.elem_tip.kalk = true; // pretvori ovaj standardni element u KALK element !!!!!

        // reducaj sve alate koji su zapravo objekti sirovina
        // reducaj sve alate koji su zapravo objekti sirovina JER SU PREVELIKI !!!!!
        if ( elem_copy.elem_alat?.length > 0 ) {
          $.each( elem_copy.elem_alat, function(alat_ind, alat) {
            elem_copy.elem_alat[alat_ind] = reduce_sir(alat); // reducaj sve alate koji su zapravo objekti sirovina
          });
        };

        var old_izlazi_arr = curr_kalk.procesi[arg_curr_proces_index].izlazi;

        function find_same_element_in_izlazi(izlaz_arr, elem_copy) {

          var found_izlaz = null;
          $.each( izlaz_arr, function(iz_ind, old_exit) {

            // ako nađem izlaz koji ima isti tip elementa kao ovaj element koji sada trebam
            // i ako ima isti redni broj element onda prekopiraj tu istu sifru elementa od izlaza
            // jer se može dogoditi da obrišem stari element i ponovo ga napravim ali da bude isti tip elementa

            if ( 
              old_exit.kalk_element?.elem_tip?.sifra == elem_copy.elem_tip.sifra 
              &&
              /* mora također biti isti redni broj elementa jer npr police su isti tip ali različiti su redni brojevi kao i npr pregrade */
              (old_exit.kalk_element.elem_RB || null) == (elem_copy.elem_RB || null) 
            ) {

              found_izlaz = old_exit;
            };

          });
          
          return found_izlaz;
          
        };


        var old_izlaz = find_same_element_in_izlazi(old_izlazi_arr, elem_copy);

        // ako sam našao da ovaj element već postoji u izlazima onda uzmi tu postojeću šifru elementa
        // A NE NOVU ŠIFRU ELEMENTA !!!!

        if ( old_izlaz ) elem_copy.sifra = old_izlaz.kalk_element.sifra;

        
        var proc_skart = curr_kalk.procesi[arg_curr_proces_index].proces_skart ? (1 - curr_kalk.procesi[arg_curr_proces_index].proces_skart) : 1;
        
        
        var arg_changed_props = {

          kalk_sum_sirovina: Math.ceil( plate.count * sum_plus_extra(ulazi[0]) * proc_skart ),
          kalk_element: elem_copy,
          kalk_kom_in_product: elem_copy.elem_on_product || 1,

          kalk_color_C: null,
          kalk_color_M: null,
          kalk_color_Y: null,
          kalk_color_K: null,
          kalk_color_W: null,
          kalk_color_V: null,

          kalk_noz_big: null,
          kalk_nest_count: null,

          kalk_po_valu: elem_copy.elem_val || null,
          kalk_po_kontravalu: elem_copy.elem_kontra || null,


          kalk_plate_naziv: ulazi[0].kalk_plate_naziv || null, // plate.naziv  + ` --- ` + plate.sirov.full_naziv,
          kalk_plate_id: ulazi[0].kalk_plate_id || null,  // plate.plate_id,

        };

        // također kopiraj šifru cijelog izlaza (već sam gore kopirao šifu elementa)
        if ( old_izlaz ) arg_changed_props.sifra = old_izlaz.sifra;


        // jako je bitno da sada dajem samo jedan ulaz  [ ulazi[0] ] !!!!
        // ako ima više ulaza tj više montažnih ploča ova funkcija neće raditi kako treba
        // ako ima više ulaza tj više montažnih ploča ova funkcija neće raditi kako treba 
        var jedan_izlaz_arr = 
            this_module.gen_izlazi( null, arg_changed_props, arg_curr_proces_index, curr_kalk, [ ulazi[0] ], arg_radna_stn, null, /*ukupno_dobiveno_iz_ulaza*/ null, null );

        all_izlazi.push( jedan_izlaz_arr[0] );

      }; // kraj ako je isti plate id u element i u current ulazu

    });  // loop po pločama koje na sebi imaju ovaj element

  }); // loop po svim elementima 

    
  return all_izlazi;
  
  
};
// ---------------------------- KRAJ izlazi after clean ------------------------------------------



VELPROCES.dummy_work = function dummy_work( this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index ) {


  
  
  /*
  $(`#` + curr_kalk.kalk_rand + `_` + arg_action.action_sifra + `_satnica_stroj` ).val( cit_format(krajser_price, 2) );
  $(`#` + curr_kalk.kalk_rand + `_` + arg_action.action_sifra + `_satnica_prep` ).val( cit_format(krajser_prep_price, 2) );
  $(`#` + curr_kalk.kalk_rand + `_` + arg_action.action_sifra + `_satnica_radnik` ).val( cit_format(satnica_radnik, 2) );
  */
  
  if ( !arg_ulazi || arg_ulazi.length == 0 ) {
    popup_warn(`Nedostaju ulazne sirovine ili elementi !!!!`);
    return null;
  };

  var warn_popup = false;

  var sum_ulaz_kom = 0;
  
  var minimal_ulaz = 9999999999;
  var min_ul_extra_kom = 0;

  $.each( arg_ulazi, function( u_ind, ulaz ) {
    // ne smje biti pomoćna sirovina !!!
    // -----> jedino sve ostali ulazi kao npr kalk elementi koji nisu pomoćne jer pomoćne sirovine se ne prenose dalje u sljedeće procese !!!!
    if (
      (ulaz.tip_sirovine && ulaz.tip_sirovine.sifra !== `TSIR11`)
      ||
      (ulaz.original_sirov?.tip_sirovine && ulaz.original_sirov?.tip_sirovine?.sifra !== `TSIR11`)
      ||
      ulaz.kalk_element // ili ako je jedostavno neki element koji je nastao u prijašnjem procesu
    ) {
      
      if ( sum_plus_extra(ulaz) < minimal_ulaz ) {
        minimal_ulaz = sum_plus_extra(ulaz);
        min_ul_extra_kom = ulaz.kalk_extra_kom || 0;
      };
      
      sum_ulaz_kom += sum_plus_extra(ulaz);
    };
    
  });

  var ukupno_dobiveno_iz_ulaza = gen_dobiveno_ulaz(arg_ulazi);

  
  var satnica_stroj = 0;
  var satnica_prep = 0;
  var satnica_radnik = 0;
  
  var radnik_count = 1;
  var radnik_count_prep = 1;
  
  
  var prep_time = 0;
  var work_time = 0;
  
  var prirez_val = null;
  var prirez_kontra = null;
  
  var change_action_props = null;
  
  
  
/*
  
  if (
    !arg_extra_data?.satnica_stroj    ||
    arg_extra_data?.satnica_prep      ||
    arg_extra_data?.satnica_radnik   ||
    arg_extra_data?.action_prep_time  ||
    arg_extra_data?.action_work_time   ||
    ) {
    
    
    popup_warn (
`
Potrebno upisati podatke:<br>
SATNICA STROJ<br>
SATNICA PRIPREME<br>
SATNICA RADNIKA<br>
VRIJEME RADA<br>
VRIJEME PRIPREME<br>

`)
    
  };
  
*/
  
  
  

  if ( cit_number(arg_extra_data?.satnica_stroj ) !== null ) satnica_stroj = arg_extra_data.satnica_stroj;
  if ( cit_number(arg_extra_data?.satnica_prep ) !== null ) satnica_prep = arg_extra_data.satnica_prep;
  if ( cit_number(arg_extra_data?.satnica_radnik ) !== null ) satnica_radnik = arg_extra_data.satnica_radnik;
  if ( cit_number(arg_extra_data?.radnik_count ) !== null ) radnik_count = arg_extra_data.radnik_count;
  if ( cit_number(arg_extra_data?.radnik_count_prep ) !== null ) radnik_count_prep = arg_extra_data.radnik_count_prep;
  
  
  
  if ( cit_number(arg_extra_data?.action_prep_time) !== null ) prep_time = arg_extra_data.action_prep_time * radnik_count_prep;
  if ( cit_number(arg_extra_data?.action_work_time) !== null ) work_time = arg_extra_data.action_work_time * radnik_count;
  
  
  // ako je scitex ili fuji
  if ( 
    arg_radna_stn.sifra.indexOf(`SCITEXRAD`) > -1 
      ||
    arg_radna_stn.sifra.indexOf(`FUJIRAD`) > -1 
  ) {
    
    var norma_boje = 0.01; // za scitex
    if ( arg_radna_stn.sifra.indexOf(`FUJIRAD`) > -1 ) norma_boje = 0.03; // za fuji
    
    arg_ulazi = kalk_scitex_or_fuji( norma_boje, this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index);
    
  };
  
  
  
  var ulazi = arg_ulazi || this_module.gen_ulazi_from_izlazi(arg_ulazi);

  
  var arg_changed_props = null;
  
  var izlazi = arg_izlazi || this_module.gen_izlazi( null, arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, arg_radna_stn, ukupno_dobiveno_iz_ulaza, prirez_val, prirez_kontra );
  
  // AKO JE OPTRGAVANJE !!!!!!!
  // "sifra": "RADS1CLEAN", "naziv": "OPTRGAVANJE NAKON ŠTANCE ILI ZUND-a
  if ( arg_radna_stn.sifra == `RADS1CLEAN` ) {
    izlazi = izlazi_after_clean( this_module, product_data, curr_kalk, ulazi, arg_radna_stn, ukupno_dobiveno_iz_ulaza, arg_curr_proces_index );
  };
  
  
  if ( arg_radna_stn.sifra.indexOf(`RADGLUE`) > -1 ) {

    // if ( !arg_extra_data?.izlaz_kom ) {
      


    
      // popup_warn(`Potrebno upisati koliko komada izlazi nakon LJEPLJENJA !!!`);
      // return;
    // };
    
    arg_changed_props = kalk_glue( this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index );  
    
    if ( arg_changed_props === null ) return;
   
    
    
    // kalkulacija cijena kada je ljepljnje u pitanju
    ulazi = glue_prices(ulazi, minimal_ulaz, min_ul_extra_kom);
    
    
    if ( !arg_extra_data ) {
      arg_extra_data = {};
      curr_kalk.procesi[arg_curr_proces_index].extra_data = {};
    };

    // var izlaz_kom_object = { sifra: curr_kalk.procesi[arg_curr_proces_index].izlazi[0].sifra, kom: minimal_ulaz };
    // arg_extra_data.izlaz_kom = izlaz_kom_object;
    // curr_kalk.procesi[arg_curr_proces_index].extra_data.izlaz_kom = izlaz_kom_object;

    // arg_extra_data.izlaz_kom.kom ---- > prije sam koristio ovaj upis u extra ----> sada uzimam automatski minimalni broj
    izlazi = this_module.gen_izlazi( "just_first", arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, arg_radna_stn, minimal_ulaz , null, null );
    
    
    
  };
    
  // AKO JE FORMIRANJE !!!
  // AKO JE FORMIRANJE !!!
  // AKO JE FORMIRANJE !!!
  if ( arg_radna_stn.sifra.indexOf(`FORMPRO`) > -1 ) {

    
    
    arg_changed_props = kalk_forming( this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index );  
    
    if ( arg_changed_props === null ) return;
   
    
    if ( !arg_extra_data ) {
      arg_extra_data = {};
      curr_kalk.procesi[arg_curr_proces_index].extra_data = {};
    };

    
    // var izlaz_kom_object = { sifra: curr_kalk.procesi[arg_curr_proces_index].izlazi[0].sifra, kom: minimal_ulaz };
    // arg_extra_data.izlaz_kom = izlaz_kom_object;
    // curr_kalk.procesi[arg_curr_proces_index].extra_data.izlaz_kom = izlaz_kom_object;

    
    izlazi = this_module.gen_izlazi( "just_first", arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, arg_radna_stn, minimal_ulaz, null, null );
    
    
  };  
  
  
  
  
  // ako postoji custom broj izlaza onda uzmi taj custom broj da sa njim djelim sve kako bi dobio unit time i unit price
  // na primjer kod ljepljenja trebam definidrati koliko sljepljenih stvari izađe iz procesa i taj broj onda koristim dalje umjesto ukupnog broja ulaza !!!!
  if ( arg_extra_data?.izlaz_kom ) {
    sum_ulaz_kom = arg_extra_data?.izlaz_kom.kom;
  };
  
  var unit_time = ( prep_time + work_time )/sum_ulaz_kom;
  
  var time = unit_time * sum_ulaz_kom;
  
  var unit_price = 
      ( prep_time/60 * satnica_prep  )/sum_ulaz_kom +
      ( work_time/60 * satnica_stroj )/sum_ulaz_kom +
      ( (work_time+prep_time)/60 * satnica_radnik )/sum_ulaz_kom;
  
  var price = unit_price * sum_ulaz_kom;



  var action = arg_action || this_module.gen_action( change_action_props, curr_kalk, prep_time, work_time, unit_time, time, unit_price, price, arg_radna_stn, arg_curr_proces_index );

  var proces = {
    extra_data: arg_extra_data || {},
    proc_koment: arg_proc_koment || "",
    row_sifra: `procesrow`+cit_rand(),
    ulazi: ulazi,
    action: action,
    izlazi: izlazi, 
  }; // kraj procesa

  return {
    prep_time,
    work_time,
    
    unit_time,
    time,
    unit_price,
    price,
    proces
  };

};



async function add_scitex_colors(new_proces, product_data) {

  var all_scitex_boje = null;
  var props = {
    filter: null,
    url: '/find_sirov',
    query: { "klasa_sirovine.sifra": "KSSBO", on_date: product_data?.rok_za_def || null }, /* {  naziv: `SCITEX BOJA` },  */
    desc: `pretraga za sve scitex boje koje imaju specijalnu klasu sirovine SCITEX BOJA !!!! `
  };

  try {
    all_scitex_boje = await search_database( null, props );
  } catch (err) {
    console.log(err);
    return;
  };


  var scitex_boja_ulazi = [];
  $.each( all_scitex_boje, function(scitex_ind, scitex_color) {
    var scitex_ulaz = convert_db_sirov_to_kalk_sirov(scitex_color);
    new_proces.ulazi = upsert_item( new_proces.ulazi, scitex_ulaz, `_id`);
  });

  // new_proces.ulazi = [ ...new_proces.ulazi, ...scitex_boja_ulazi ];

  return new_proces;

};


function add_glue(new_proces, product_data, this_module) {
      
  var new_ulaz = this_module.fresh_ulaz();
  new_proces.ulazi.push(new_ulaz);
  return new_proces;

};


function calc_color( C, M, Y, K, W, V, func_name ) {


  var color_price_sum = 0;
  var color_price = 0;
  var color_use = 0;

  if ( func_name == `scitex_work` ) {
    color_price = 0.038 * window.EUR_tecaj;  
    color_use = 10; // u mili litrama
  };

  if ( func_name == `fuji_work` ) {
    color_price = 0.2 * window.EUR_tecaj;
    color_use = 30; // u mili litrama
  };

  var color_price_sum = ( (C || 0) + (M || 0) + (Y || 0) + (K || 0) + (W || 0) + (V || 0) ) / 1000000 * color_use * color_price;

  return color_price_sum

};



  
  /*
  ------------------------------------------------------------------------------------------------------------
  ZA SADA NE KORISTIM OVO !!!!!!
  ------------------------------------------------------------------------------------------------------------
  
  async function cut_plate( this_input ) { 
    
    var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    var product_id = $('#'+data.id).closest('.product_comp')[0].id;
    var product_data =  this_module.product_module.cit_data[product_id];

    var ulaz_row = $(this_input).closest('.ulaz_row');

    var kalk_type = ulaz_row.attr(`data-kalk_type`);
    var kalk_sifra = ulaz_row.attr(`data-kalk_sifra`);
    var row_sifra = ulaz_row.attr(`data-row_sifra`);
    var ulaz_sifra = ulaz_row.attr(`data-ulaz_sifra`);

    var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null ); 


    var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
    var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
    var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null ); 

    var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );

    
    var sirovina_or_elem = null;
        
    var val_dim = null;
    var kontra_dim = null;

    var real = null;
    var ostaci = null;

    var prirez_val = null;
    var prirez_kontra = null;
    

    
    
    if ( curr_ulaz._id || curr_ulaz.prodon_sirov_id ) {
      
      // ako je ovo prava sirovina a ne neki element
      //!!!findsirov!!!
      
      var props = {
        filter: null,
        url: '/find_sirov',
        query: { _id: curr_ulaz._id || curr_ulaz.prodon_sirov_id },
        desc: `pretraga za sirovinu kod upisa u ulaz sum sirovina`,
      };


      try {
        sirovina_or_elem = await search_database(null, props );
      } 
      catch (err) {
        console.log(err);

        $(this_input).val(``);
        curr_ulaz.kalk_sum_sirovina = null;
        return;
      };

      // ako je našao sirovinu onda je to uvijek array
      // zato samo uzmi prvi item
      if ( sirovina_or_elem && $.isArray(sirovina_or_elem) ) {
        sirovina_or_elem = sirovina_or_elem[0];

      };
      
      
      val_dim = ( sirovina_or_elem.po_valu || sirovina_or_elem.kontra_tok);
      kontra_dim = (sirovina_or_elem.po_kontravalu || sirovina_or_elem.tok);
    
    };
    
    
    
    
    // if ( 
    //   (sirovina.po_valu && sirovina.po_kontravalu)
    //   ||
    //       (sirovina.kontra_tok && sirovina.tok)
    //  ) {
      
   
      
    sirovina.kalk_po_valu = prirez_val || val_dim;
    sirovina.kalk_po_kontravalu = prirez_kontra || kontra_dim;
    sirovina.kalk_unit_price = sirovina.cijena;
    sirovina.kalk_unit = sirovina.osnovna_jedinica.jedinica;
    

    
    
    // if (  )
    

    if ( curr_proces.izlazi.length == 1 ) prirez_val = curr_proces.izlazi[0].kalk_po_valu; // prvo gledaj ono što je upisano input polju izlaza
    if ( !prirez_val ) prirez_val = curr_proces.extra_data?.izlaz_val || null;

    if ( !prirez_val ) prirez_val = sirovina.po_valu; // ako je sirovina ljepenka - pogledaj po valu
    if ( !prirez_val ) prirez_val = sirovina.kontra_tok; // ako je sirovina papir - pogledaj obrnuto tj kontra tok

    if ( curr_proces.izlazi.length == 1 ) prirez_kontra = curr_proces.izlazi[0].kalk_po_kontravalu; // prvo gledaj ono što je upisano input polju izlaza
    if ( !prirez_kontra ) prirez_kontra = curr_proces.extra_data?.izlaz_kontra || null;

    if ( !prirez_kontra ) prirez_kontra = sirovina.po_kontravalu; // ako je sirovina ljepenka - pogledaj po kontravlu
    if ( !prirez_kontra ) prirez_kontra = sirovina.tok; // ako je papir gledaj tok

    sirovina.kalk_po_valu = prirez_val || val_dim;
    sirovina.kalk_po_kontravalu = prirez_kontra || kontra_dim;
    sirovina.kalk_unit_price = sirovina.cijena;
    sirovina.kalk_unit = sirovina.osnovna_jedinica.jedinica;

    real = this_module.kalk_real_plate( sirovina.po_valu, sirovina.po_kontravalu, prirez_val, prirez_kontra, sirovina_or_elem, curr_kalk );
    if ( real ) ostaci = this_module.make_ostatak(real, sirovina);


    // ------------------------------------------------------------

    curr_ulaz.kalk_po_valu = real.real_val;
    curr_ulaz.kalk_po_kontravalu = real.real_kontra;

    curr_ulaz.kalk_ostatak_val = ostaci ? ostaci.val : 0;
    curr_ulaz.kalk_ostatak_kontra = ostaci ? ostaci.kontra : 0;

    curr_ulaz.kalk_unit_area = real.area;

    curr_ulaz.real_unit_price = real.price;
    curr_ulaz.kalk_price = real.price * curr_ulaz.kalk_sum_sirovina;

    curr_ulaz.kalk_kom_in_sirovina = real.kom;
    curr_ulaz.kalk_kom_val = real.kom_val;
    curr_ulaz.kalk_kom_kontra = real.kom_kontra;

    // ------------------------------------------------------------



  }; // kraj ako je ovo sirovina tj ako ima _id
  this_module.cut_plate = cut_plate;
  */


  
  async function register_new_kalk_events( this_module, data, sifra, type) {
    
    
    var kalk_box = $(`#` + sifra + `_kalkulacija`);
    var kalk_box_height = kalk_box.outerHeight();
    
    var proces_scroll_box = $(`#` + sifra + `_proces_scroll_box`);
    var center_scroll_box = $(`#` + sifra + `_center_scroll_box`);
    
    var proces_scroll_box_height = proces_scroll_box.height(); // .outerHeight();
    
    // $(`#` + sifra + `_scroll_kalk_left`).css(`height`, proces_scroll_box_height + `px`);
    // $(`#` + sifra + `_scroll_kalk_right`).css(`height`, proces_scroll_box_height + `px`);
    
    
    var scroll_left_interval = 0;
    $(`#` + sifra + `_scroll_kalk_left`).off(`mouseenter`);
    $(`#` + sifra + `_scroll_kalk_left`).on(`mouseenter`, function() {
      clearInterval( scroll_left_interval );
      scroll_left_interval = setInterval( function() {
        center_scroll_box[0].scrollLeft -= 100;  
      }, 100);
    });
    
    $(`#` + sifra + `_scroll_kalk_left`).off(`mouseleave`);
    $(`#` + sifra + `_scroll_kalk_left`).on(`mouseleave`, function() {
      clearInterval( scroll_left_interval );
    });
    
    
    var scroll_right_interval = 0;
    $(`#` + sifra + `_scroll_kalk_right`).off(`mouseenter`);
    $(`#` + sifra + `_scroll_kalk_right`).on(`mouseenter`, function() {
      
      clearInterval( scroll_right_interval );
      scroll_right_interval = setInterval( function() {
        center_scroll_box[0].scrollLeft += 100;
      }, 100);
      
    });
    
    
    $(`#` + sifra + `_scroll_kalk_right`).off(`mouseleave`);
    $(`#` + sifra + `_scroll_kalk_right`).on(`mouseleave`, function() {
      clearInterval( scroll_right_interval );
      
    });    
    
    
    
    $(`#` + sifra + `_scroll_kalk_up`).off(`click`);
    $(`#` + sifra + `_scroll_kalk_up`).on(`click`, function() {
      center_scroll_box[0].scrollTop -= 100;
    });
    
    $(`#` + sifra + `_scroll_kalk_down`).off(`click`);
    $(`#` + sifra + `_scroll_kalk_down`).on(`click`, function() {
      center_scroll_box[0].scrollTop += 100;
    });
    

    
    $(`#` + sifra + `_show_in_offer`).data(`cit_run`, async function(state, check_elem) { 

      
      var this_comp_id = $(check_elem).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var kalk_box = $(check_elem).closest('.kalk_box');
        
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      
      console.log(`--------show in offer check box ---------`);
      console.log(data);

      curr_kalk.show_in_offer = state;
      
    }); // kraj cit run func in data
    
    $(`#` + sifra + `_kalk_fixed_mark`).data(`cit_run`, async function(state, check_elem) { 

      var this_comp_id = $(check_elem).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var kalk_box = $(check_elem).closest('.kalk_box');

      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);

      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );

      console.log(`-------- fixed mark check box ---------`);
      console.log(data);
      
      if ( state == true ) {
        
        if ( curr_kalk.kalk_fixed_price ) {
          popup_warn(`Kalkulacija ne može imati fiksnu cijenu i fiksnu maržu/popust u isto vrijeme!!<br>...nema smisla :)`);
          $(`#` + sifra + `_kalk_fixed_mark`).removeClass(`on`).addClass(`off`);
          return;
        };
        curr_kalk.kalk_fixed_mark = state;
        
        
        $(`#`+sifra+`_kalk_discount`).css(`pointer-events`, `none`);
        $(`#`+sifra+`_kalk_markup`).css(`pointer-events`, `none`);
        $(`#`+sifra+`_kalk_discount_money`).css(`pointer-events`, `none`);
        $(`#`+sifra+`_kalk_markup_money`).css(`pointer-events`, `none`);

      }
      else {
        
        var super_editori = get_super_editors(); 
        
        // ako nije NITI JEDAN OD EDITORA onda ne može brisati ugovor ili tender za fiznu cijenu
        if ( super_editori.indexOf( window.cit_user.user_number ) == -1 ) {
          popup_warn(`Nemate ovlasti za brisanje fiksne marže ili popusta !!!`);
          $(`#` + sifra + `_kalk_fixed_mark`).removeClass(`off`).addClass(`on`);
          return;
        };
        curr_kalk.kalk_fixed_mark = state;
        
        $(`#`+sifra+`_kalk_discount`).css(`pointer-events`, `all`);
        $(`#`+sifra+`_kalk_markup`).css(`pointer-events`, `all`);
        $(`#`+sifra+`_kalk_discount_money`).css(`pointer-events`, `all`);
        $(`#`+sifra+`_kalk_markup_money`).css(`pointer-events`, `all`);
        
        
      };

    }); // kraj cit run func in data

    $(`#` + sifra + `_kalk_fixed_discount`).data(`cit_run`, async function(state, check_elem) { 

      var this_comp_id = $(check_elem).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var kalk_box = $(check_elem).closest('.kalk_box');

      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);

      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );

      console.log(`-------- fixed discount check box ---------`);
      console.log(data);
      
      if ( state == true ) {
        
        if ( curr_kalk.kalk_fixed_price ) {
          popup_warn(`Kalkulacija ne može imati fiksnu cijenu i fiksnu maržu/popust u isto vrijeme!!<br>...nema smisla :)`);
          $(`#` + sifra + `_kalk_fixed_discount`).removeClass(`on`).addClass(`off`);
          return;
        };
        curr_kalk.kalk_fixed_discount = state;
        
        $(`#`+sifra+`_kalk_discount`).css(`pointer-events`, `none`);
        $(`#`+sifra+`_kalk_markup`).css(`pointer-events`, `none`);
        $(`#`+sifra+`_kalk_discount_money`).css(`pointer-events`, `none`);
        $(`#`+sifra+`_kalk_markup_money`).css(`pointer-events`, `none`);

        
      }
      else {
        
        var super_editori = get_super_editors(); 
        
        // ako nije NITI JEDAN OD EDITORA onda ne može brisati ugovor ili tender za fiznu cijenu
        if ( super_editori.indexOf( window.cit_user.user_number ) == -1 ) {
          popup_warn(`Nemate ovlasti za brisanje fiksne marže ili popusta !!!`);
          $(`#` + sifra + `_kalk_fixed_discount`).removeClass(`off`).addClass(`on`);
          return;
        };
        curr_kalk.kalk_fixed_discount = state;
        $(`#`+sifra+`_kalk_discount`).css(`pointer-events`, `all`);
        $(`#`+sifra+`_kalk_markup`).css(`pointer-events`, `all`);
        $(`#`+sifra+`_kalk_discount_money`).css(`pointer-events`, `all`);
        $(`#`+sifra+`_kalk_markup_money`).css(`pointer-events`, `all`);
        
        
      };

    }); // kraj cit run func in data

    
    window.kalk_arrow_is_clicked = false;
    
    $(`#`+sifra+`_kalkulacija .kalk_arrow`).off(`click`);
    $(`#`+sifra+`_kalkulacija .kalk_arrow`).on(`click`, function() {
      
      
      window.kalk_arrow_is_clicked = true;
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var kalk_box = $(this).closest('.kalk_box');
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      // nekad nema offer kalkulacija tj nema kalk uopće pa moram provjeriti
      if ( curr_kalk ) {
        
        var parent_kalk =  $(this).closest(`.kalk_box`);

        if ( parent_kalk.hasClass(`cit_closed`) == true ) {
          parent_kalk.removeClass(`cit_closed`);
          curr_kalk.closed = false;

        } else {
          parent_kalk.addClass(`cit_closed`);
          curr_kalk.closed = true;
        };
        
        var kalk_box = $(`#` + sifra + `_kalkulacija`);
        var kalk_box_height = kalk_box.outerHeight();
        $(`#` + kalk_sifra + `_scroll_kalk_left`).css(`height`, kalk_box_height+`px`);
        $(`#` + kalk_sifra + `_scroll_kalk_right`).css(`height`, kalk_box_height+`px`);
        
        
      };
      
      
    }); // kraj klik na kalk arrow
    

    $(`#`+sifra+`_add_radna_stanica`).data('cit_props', {
      
      desc: 'odabir nove radne stanice unutar kalkulacija za kalk sifra =  ' + sifra,
      local: true,
      show_cols: [
        'naziv',
        'proces_type_naziv', 
        'elem_naziv',
        'unit',
        // 'interval',
        'func',
        'speed',
        'price'
      ],
      custom_headers: [
        'NAZIV',
        'VRSTA PROCESA',
        'IZLAZNI ELEMENT',
        'JEDINICA',
        // 'INTER.',
        'FUNC',
        'OSNOVNA BRZINA',
        'CIJENA'
      ],
      
      col_widths: [
        5, // 'naziv',
        2, // 'proces_type_naziv', 
        3, // 'elem_naziv',
        1, // 'unit',
        // 0.7, // 'interval',
        1.5, // 'func'
        1, // 'speed',
        1, // 'price'
      ],
      
      format_cols: { speed: 2, price: 2, },
      
      return: {},
      show_on_click: true,
      list: `radne_stanice`,
      cit_run: async function(state) {
        
        
        var kalk_sifra = sifra;
        var kalk_type = type;
      
        console.log(`--------------------------------`, kalk_sifra);
        console.log(`--------------------------------`, kalk_type);
        

        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        
        var kalk_box = $('#'+current_input_id).closest('.kalk_box');
        
        var kalk_type = kalk_box.attr(`data-kalk_type`);
        var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
        
        console.log(`---------- FROM ATTR ----------------------`, kalk_sifra);
        console.log(`---------- FROM ATTR ----------------------`, kalk_type);

        
        var product_id = $('#'+data.id).closest('.product_comp')[0].id;
        var product_data =  this_module.product_module.cit_data[product_id];
        
        console.log(state);
        
        // return;
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          return;
        };
        
        var curr_kalk =  this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
        
        
        
        if ( kalk_type == `post_kalk` ) {
          popup_warn(
`Ne možete dodavati naknadne procese u CRVENOJ (post) kalkulaciji!<br><br>Crvena (post) kalkulacija služi samo za evidenciju rada.<br>Novi proces možete kreirati u ZELENOJ kalkulaciji !!!`)
          return;
        };
        
        
        if ( kalk_type == `offer_kalk` && data.pro_kalk?.length > 0 ) {
          popup_warn(`Ne možete dodavati naknadne procese u PLAVOJ kalkulaciji za PONUDE nakon što je već kreirana kalkulacija za proizvodnju!<br>Novi proces možete kreirati u ZELENOJ kalkulaciji !!!`)
          return;
        };
        
        // ako nema broja procesa onda će biti zadnji na kraju
        if ( !curr_kalk.for_proces ) curr_kalk.for_proces = curr_kalk.procesi.length + 1; // na primje ako imam 3 procesa onda će dor proces biti 4
        
        // UVIJEK SMANJUJEM FOR PROCESA ZA MINUS JEDAN ( u ovom primjeru će index biti opet 3 )
        var new_proces_index = curr_kalk.for_proces-1;
        
        var new_proces = this_module.fresh_proces(state);

        
        // dodaj novi proces  
        curr_kalk.procesi.push(new_proces); // splice( curr_kalk.for_proces-1, 0, new_proces );
        
        
        // this_module.update_kalk( curr_kalk, product_data, new_proces_index, null, null, changed_action=true );
        
        
        // curr_kalk.procesi.push( new_proces );
        
        // resetiraj input polje za redni broj insertiranja
        curr_kalk.for_proces = null;
        $(`#${sifra}_for_proces`).val( "" );
                
        
        console.log(`--------------- make kalk kod odabira _add_radna_stanica ----------------`);
        var this_input = $('#'+current_input_id)[0];
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
        
        wait_for( `$(".proces_row[data-row_sifra='${ new_proces.row_sifra }']").length > 0`, function() {
          
          var new_proces_element = $(".proces_row[data-row_sifra='" + new_proces.row_sifra + "']");
          
          cit_scroll(new_proces_element, -50, 200);
          
        }, 5*1000 );
        

      },
      
    });
    
    
    $(`#`+sifra+`_kalk_komentar`).off(`blur`);
    $(`#`+sifra+`_kalk_komentar`).on(`blur`, async function() {
      
      
      var this_input = this;
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      
      // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
      var product_id = $(this).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var kalk_box = $(this).closest('.kalk_box');
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      $(`#${curr_kalk.kalk_sifra}_kalkulacija .title_komentar`).html( $(this).val() );
      
      
      curr_kalk.kalk_komentar = $(this).val();
      
      // set_input_data(this, curr_kalk, kalk_sifra );
      
      console.log( curr_kalk );
      
      console.log(`--------------- make kalk kod odabira _kalk_komentar ----------------`);
      
      var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
      // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      
    });
    
    
    $(`#`+sifra+`_add_fixed_price_contract`).data('cit_props', {
      
      desc: 'odabir novog ugovora za FIXNU CIJENU u kalkulaciji : ' + sifra,
      local: true,
      show_cols: [ 
        "naziv",
        "date",
        "rok",
        "status_naziv",
        "start",
        "end",
        "komentar", 
        "doc_links"
      ],
      format_cols: { date: "date", rok: "date", start: "date", end: "date" },
      col_widths: [
        2, // "naziv",
        1, // "date",
        1, // "rok",
        2, // "status_naziv",
        1, // "start",
        1, // "end",
        3, // "komentar", 
        4, // "doc_links"
        
      ],
      return: {},
      show_on_click: true,
      
      list: function() {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        var product_id = $('#'+current_input_id).closest('.product_comp')[0].id;
        var product_data =  this_module.product_module.cit_data[product_id];

        var project_id = $('#'+current_input_id).closest('.project_comp')[0].id;
        var project_data =  this_module.project_module.cit_data[project_id];
        
        function generate_docs_html(docs) {
          
          // TENDER DOCS
          var all_docs = "";
          if ( docs?.length > 0 ) {

            var criteria = [ '~time' ];
            multisort( docs, criteria );

            $.each(docs, function(doc_index, doc) {
              var doc_html = 
              `
              <div class="docs_row">
                <div class="docs_date">${ cit_dt(doc.time).date_time }</div>
                <div class="docs_link">${ doc.link }</div>
              </div>
              `;

              all_docs += doc_html

            }); // kraj loopa po docs

          }; // kraj ako ima docs
          
          return all_docs;

        }; // kraj generate_docs_html

        var ugovori_tenderi_list = [];
        
        if ( project_data.kupac?.ugovori?.length > 0 ) {
        
          $.each(project_data.kupac.ugovori, function(u_ind, ugovor ) {
            
            ugovori_tenderi_list.push({
              sifra: ugovor.sifra,
              naziv: ugovor.naziv || null,
              date: ugovor.date || null,
              rok: ugovor.rok || null,
              start: ugovor.start || null,
              end: ugovor.end || null,
              komentar: ugovor.komentar || null,
              status: ugovor.status || null,
              status_naziv: ugovor.status?.naziv || null,
              docs: ugovor.docs || null,
              doc_links: generate_docs_html(ugovor.docs),
              
            });
            
          });  
          
        };
        
        if ( project_data.kupac?.tenders?.length > 0 ) {
        
          $.each(project_data.kupac.tenders, function(t_ind, tender ) {
            
            ugovori_tenderi_list.push({
              
              sifra: tender.sifra,
              naziv: tender.naziv || null,
              date: tender.date || null,
              rok: tender.rok || null,
              start: tender.start || null,
              end: tender.end || null,
              komentar: tender.komentar || null,
              status: tender.status || null,
              status_naziv: tender.status?.naziv || null,
              docs: tender.docs,
              doc_links: generate_docs_html(tender.docs),
              
            });
            
            
          });  
          
        };
        
        
        return ugovori_tenderi_list;
        
      },
      
      cit_run: function(state) {
        
        
        var kalk_sifra = sifra;
        var kalk_type = type;
      
        console.log(`--------------------------------`, kalk_sifra);
        console.log(`--------------------------------`, kalk_type);
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        
        var kalk_box = $('#'+current_input_id).closest('.kalk_box');
        
        var kalk_type = kalk_box.attr(`data-kalk_type`);
        var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
        
        console.log(`---------- FROM ATTR ----------------------`, kalk_sifra);
        console.log(`---------- FROM ATTR ----------------------`, kalk_type);


        console.log(state);
        
        // return;
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          return;
        };
        
        var curr_kalk =  this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
        
        
        if ( curr_kalk.kalk_fixed_mark || curr_kalk.kalk_fixed_discount ) {
          popup_warn(`Nema smisla definirati fiksnu cijenu ako imate fiksnu maržu/popust !!!!`);
          return;
        };
        
        var fixed_price_in_diff_kalk = false;
        
        $.each( data.offer_kalk, function(k_ind, find_kalk) {
          
          if (
            find_kalk.kalk_sifra !== curr_kalk.kalk_sifra // ako je neka druga kalkulacija osim ove trenutne u kojoj sam sada
            &&
            find_kalk.kalk_fixed_price?.length > 0 // ako ta druga kalkulacija već ima ugovor ili tender
          ) {
            
            fixed_price_in_diff_kalk = true;
          };
          
        });
        
        if ( fixed_price_in_diff_kalk == true ) {
          popup_warn(`Samo jedna kalkulacija može imati fiksnu cijenu!!!`);
          return;
        };
        
        
        if ( curr_kalk.kalk_fixed_price?.length > 0 ) { 
          popup_warn(`Možete pridružiti samo JEDAN ugovor ili tender za fiksnu cijenu!<br>Ako želite dodati neki drugi ugovor morate prvo obrisati postojeći!!!`);
          return;
        };
        
        
        // potrebno je da ovo bude array !!!! tako da mi radi kako treba generiranje liste -----> u ovom slučaju imam samo jedan item
        // samo jedan item !!!
        // samo jedan item !!!
        
        curr_kalk.kalk_fixed_price = [state];
        
        $(`#`+curr_kalk.kalk_sifra + `_kalk_fixed_price`).addClass(`on`).removeClass(`off`);
        
        this_module.make_ugovor_or_tender_list(data, curr_kalk);
        
      },
      
      
    });
    
    
    $(`#`+sifra+`_add_mark_or_disc_contract`).data('cit_props', {
      
      desc: 'odabir novog ugovora fiksnu maržu ili popust za kalkulaciju : ' + sifra,
      local: true,
      show_cols: [ 
        "naziv",
        "date",
        "rok",
        "status_naziv",
        "start",
        "end",
        "komentar", 
        "doc_links"
      ],
      format_cols: { date: "date", rok: "date", start: "date", end: "date" },
      col_widths: [
        2, // "naziv",
        1, // "date",
        1, // "rok",
        2, // "status_naziv",
        1, // "start",
        1, // "end",
        3, // "komentar", 
        4, // "doc_links"
        
      ],
      return: {},
      show_on_click: true,
      
      list: function() {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        var product_id = $('#'+current_input_id).closest('.product_comp')[0].id;
        var product_data =  this_module.product_module.cit_data[product_id];

        var project_id = $('#'+current_input_id).closest('.project_comp')[0].id;
        var project_data =  this_module.project_module.cit_data[project_id];

        
        
        function generate_docs_html(docs) {
          
          // TENDER DOCS
          var all_docs = "";
          if ( docs?.length > 0 ) {

            var criteria = [ '~time' ];
            multisort( docs, criteria );

            $.each(docs, function(doc_index, doc) {
              var doc_html = 
              `
              <div class="docs_row">
                <div class="docs_date">${ cit_dt(doc.time).date_time }</div>
                <div class="docs_link">${ doc.link }</div>
              </div>
              `;

              all_docs += doc_html

            }); // kraj loopa po docs

          }; // kraj ako ima docs
          
          return all_docs;

        }; // kraj generate_docs_html

        var mark_disc_contract_list = [];
        
        if ( project_data.kupac?.ugovori?.length > 0 ) {
        
          $.each(project_data.kupac.ugovori, function(u_ind, ugovor ) {
            
            mark_disc_contract_list.push({
              sifra: ugovor.sifra,
              naziv: ugovor.naziv || null,
              date: ugovor.date || null,
              rok: ugovor.rok || null,
              start: ugovor.start || null,
              end: ugovor.end || null,
              komentar: ugovor.komentar || null,
              status: ugovor.status || null,
              status_naziv: ugovor.status?.naziv || null,
              docs: ugovor.docs || null,
              doc_links: generate_docs_html(ugovor.docs),
              
            });
            
          });  
          
        };
        
        if ( project_data.kupac?.tenders?.length > 0 ) {
        
          $.each(project_data.kupac.tenders, function(t_ind, tender ) {
            
            mark_disc_contract_list.push({
              
              sifra: tender.sifra,
              naziv: tender.naziv || null,
              date: tender.date || null,
              rok: tender.rok || null,
              start: tender.start || null,
              end: tender.end || null,
              komentar: tender.komentar || null,
              status: tender.status || null,
              status_naziv: tender.status?.naziv || null,
              docs: tender.docs,
              doc_links: generate_docs_html(tender.docs),
              
            });
            
            
          });  
          
        };
        
        
        return mark_disc_contract_list;
        
      },
      
      cit_run: function(state) {
        
        
        var kalk_sifra =  sifra;
        var kalk_type = type;
      
        console.log(`--------------------------------`, kalk_sifra);
        console.log(`--------------------------------`, kalk_type);
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        var kalk_box = $('#'+current_input_id).closest('.kalk_box');
        
        var kalk_type = kalk_box.attr(`data-kalk_type`);
        var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
        
        console.log(`---------- FROM ATTR ----------------------`, kalk_sifra);
        console.log(`---------- FROM ATTR ----------------------`, kalk_type);

        console.log(state);
        
        // return;
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          return;
        };
        
        var curr_kalk =  this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
        
        /*
        if ( fixed_price_in_diff_kalk == true ) {
          popup_warn(`Samo jedna kalkulacija može imati fiksnu cijenu!!!`);
          return;
        };
        
        if ( curr_kalk.kalk_fixed_price?.length > 0 ) { 
          popup_warn(`Možete pridružiti samo JEDAN ugovor ili tender za fiksnu cijenu!<br>Ako želite dodati neki drugi ugovor morate prvo obrisati postojeći!!!`);
          return;
        };
        */
        
        
        // potrebno je da ovo bude array !!!! tako da mi radi kako treba generiranje liste -----> u ovom slučaju imam samo jedan item
        // samo jedan item !!!
        // samo jedan item !!!
        
        curr_kalk.mark_disc_contract_list = [state];
        
        // $(`#`+curr_kalk.kalk_sifra + `_kalk_fixed_mark`).addClass(`on`).removeClass(`off`);
        
        this_module.make_mark_disc_contract_list(data, curr_kalk);
        
      },
      
      
    });
    
    
    $(`#${sifra}_kalkulacija .copy_kalk_btn`).off(`click`);
    $(`#${sifra}_kalkulacija .copy_kalk_btn`).on(`click`, async function() {
      
      
      var this_input = this;
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      
      // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
      var product_id = $(this).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var kalk_box = $(this).closest('.kalk_box');
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      
      var popup_text = `Jeste li sigurni da želite KOPIRATI ovu kalkulaciju?`;
      

      function copy_kalk_yes() {

        var new_kalk = cit_deep(curr_kalk);

        // nova šifra !!!!
        new_kalk.kalk_sifra = `kalk`+cit_rand();
        // novi kalk rand !!!!!
        new_kalk.kalk_rand = `kalkrandom`+ cit_rand();
        

        new_kalk.kalk_reserv = false;
        new_kalk.kalk_for_pro = false;
        new_kalk.show_in_offer = false;
        new_kalk.kalk_fixed_price = null;
        

        $.each( new_kalk.procesi, function( p_ind, proces ) {

          new_kalk.procesi[p_ind].row_sifra = `procesrow`+cit_rand();

          $.each( proces.ulazi, function( ul_ind, proces_ulaz ) {
            new_kalk.procesi[p_ind].ulazi[ul_ind].sifra = "ulaz"+cit_rand();
          });


          $.each( proces.izlazi, function( iz_ind, proces_izlaz ) {

            var curr_proces_izlaz = proces_izlaz.sifra;
            var new_proces_izlaz =  "izlaz"+cit_rand();

            // nova sifa izlaza
            new_kalk.procesi[p_ind].izlazi[iz_ind].sifra = new_proces_izlaz;

            // opet loop po svim procesima
            $.each( new_kalk.procesi, function( find_p_ind, find_proces ) {
              // loop po svim ulazima
              $.each( find_proces.ulazi, function( find_ul_ind, find_proces_ulaz ) {
                // ako bilo koji ulaz ima sifru izlaz ako ovaj izlaz onda to promjeni isto u novu sifru
                if ( find_proces_ulaz.izlaz_sifra == curr_proces_izlaz ) new_kalk.procesi[find_p_ind].ulazi[find_ul_ind].izlaz_sifra = new_proces_izlaz;
              }); // manji loop po ulazima
              
            }); // mani loop po procesima

          }); // kraj loop po svim izlazima
          
          

        }); // kraj glavnog loopa po procesima


        this_module.cit_data[this_comp_id][kalk_type] = upsert_item( this_module.cit_data[this_comp_id][kalk_type], new_kalk, `kalk_sifra` );
        

        console.log(`--------------- make kalk kada kliknem na copy kalk btn ----------------`);
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
        // reset_tooltips();

      };

      
      function copy_kalk_no() {
        show_popup_modal(false, popup_text, null );
      };

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', copy_kalk_yes, copy_kalk_no, null);
   
      
    });
    
   
   
    // ZA SADA NE KORISTIM OVO I NIJE GOTOVO DO KRAJA
    // ZA SADA NE KORISTIM OVO I NIJE GOTOVO DO KRAJA
    // ZA SADA NE KORISTIM OVO I NIJE GOTOVO DO KRAJA
    $(`#${sifra}_kalkulacija .check_kalk_btn`).off(`click`);
    $(`#${sifra}_kalkulacija .check_kalk_btn`).on(`click`, async function() {
      
      
      var this_input = this;
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      
      // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
      var product_id = $(this).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var kalk_box = $(this).closest('.kalk_box');
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );

      var popup_text = `Jeste li sigurni da želite PROVJERITI ovu kalkulaciju?`;
      
      
      function find_deficit_ulaz(arg_kalk, arg_sifra) {
        
        var found_ulaz = null;
        
        $.each( arg_kalk.procesi, function( p_ind, proces ) {

          $.each( proces.ulazi, function( ul_ind, proces_ulaz ) {
            
            if ( proces_ulaz.sifra == arg_sifra ) {
              found_ulaz = proces_ulaz;
              
            };
            
          });

        }); // kraj loopa po procesima
       
        return found_ulaz;
        
      };
      

      async function check_kalk_yes() {
        
        var all_kalk_sir_ids = [];
        
        $.each( curr_kalk.procesi, function( p_ind, proces ) {

          $.each( proces.ulazi, function( ul_ind, proces_ulaz ) {
            
            if ( proces_ulaz._id ) {
              all_kalk_sir_ids = insert_uniq( all_kalk_sir_ids, proces_ulaz._id );
            };
            
          });

        }); // kraj glavnog loopa po procesima

        
        var DB_sirovine = await ajax_find_query( { _id: { $in: all_kalk_sir_ids },  }, `/find_sirov`, this_module.find_sirov.find_sirov_filter );
        

        console.log(`--------------- make kalk kalda kliknem na check kalk btn ----------------`);
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null, `return_html` );
        $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
        reset_tooltips();
        
        setTimeout( function() {
          
          
          var deficit_count = 0;
          
          // skini deficit class sa svih ulaz redova
          // skini deficit class sa svih ulaz redova
          $(`.${kalk_type}_container .ulaz_row`).removeClass(`cit_deficit`);
          
          
          $(`#${data.id} .${kalk_type}_container .ulaz_row`).each( function() {
            
            var curr_ulaz_sifra = $(this).attr(`data-ulaz_sifra`);
            var deficit_ulaz = find_deficit_ulaz(curr_kalk, curr_ulaz_sifra);
            
            // ovo ne koristim za sada
            // if ( is_deficit_row ) $(this).addClass(`cit_error`);
            
            if ( deficit_ulaz.deficit == true ) {
              // $(`#${of_kalk.kalk_sifra}_kalkulacija [data-ulaz_sifra='${op_ulaz.sifra}']`).addClass(`cit_deficit`); 
              $(this).addClass(`cit_deficit`);
              deficit_count += 1;
            };
            
          }); // loop po svim redovima ulaza  
          
          if ( deficit_count > 0 ) {
            popup_warn(`Nema dovoljno sirovina koje su označene crvenom bojom!!!`);
          };
          
        }, 200 );

      };

      
      function check_kalk_no() {
        show_popup_modal(false, popup_text, null );
      };

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', check_kalk_yes, check_kalk_no, null);
   
      
    });
    
    
    
    $(`#${sifra}_kalkulacija .delete_kalk_btn`).off(`click`);
    $(`#${sifra}_kalkulacija .delete_kalk_btn`).on(`click`, async function() {
      
      var this_input = this;
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      
      // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
      var product_id = $(this).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var kalk_box = $(this).closest('.kalk_box');
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      
      if ( kalk_type == `offer_kalk` && ( curr_kalk.kalk_reserv || curr_kalk.kalk_for_pro) ) {
        popup_warn(`Nije moguće obrisati kalkulaciju za ponudu koja ima aktivnu rezervaciju po ponudi ili po narudžbi !!!!`);
        return;
      };
      
      
      var popup_text = `Jeste li sigurni da želite OBRISATI ovu kalkulaciju?`;
      
      function delete_kalk_yes() {
        
        this_module.cit_data[this_comp_id][kalk_type] = delete_item( this_module.cit_data[this_comp_id][kalk_type], curr_kalk.kalk_sifra , `kalk_sifra` );

        $(`#${curr_kalk.kalk_sifra}_kalkulacija`).remove();

        console.log(`--------------- make kalk kalda kliknem na delete kalk btn ----------------`);
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null, `return_html` );
        $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
        reset_tooltips();
        
      };

      function delete_kalk_no() {
        show_popup_modal(false, popup_text, null );
      };

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', delete_kalk_yes, delete_kalk_no, null);
   
      
    });
    
    
    $(`#`+sifra+`_prirez_val`).off(`blur`);
    $(`#`+sifra+`_prirez_val`).on(`blur`, async function() {
      
      
      var this_input = this;
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      
      // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
      var product_id = $(this).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var kalk_box = $(this).closest('.kalk_box');
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      set_input_data(this, curr_kalk, kalk_sifra );
      
      var sirovine_ids = [];
      $.each(curr_kalk.procesi[0].ulazi, function(u_ind, ulaz) {
        var sir_id = ulaz._id ? ulaz._id : null; 
        if ( sir_id ) sirovine_ids.push(sir_id);
      });
      
      
      var best_plate_ulazi = await this_module.best_plate_find( 
        null,
        this_comp_id, 
        product_id,
        kalk_type,
        kalk_sifra,
        curr_kalk,
        sirovine_ids,
        proces_index=null
      );
      
      if ( best_plate_ulazi ) curr_kalk.procesi[0].ulazi = best_plate_ulazi;
      
      // this_module.update_kalk( curr_kalk, product_data, curr_proces_index=0, changed_ulaz=true, null, null );

      // setTimeout( function() {
        console.log(`--------------- make kalk kod odabira _prirez_val ----------------`);
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      // }, 400)
      
    });
    
    
    $(`#`+sifra+`_prirez_kontra`).off(`blur`);
    $(`#`+sifra+`_prirez_kontra`).on(`blur`, async function() {
      
      var this_input = this;
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $(this).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var kalk_box = $(this).closest('.kalk_box');
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      set_input_data(this, curr_kalk, kalk_sifra );
      
      
      var sirovine_ids = [];
      $.each(curr_kalk.procesi[0].ulazi, function(u_ind, ulaz) {
        var sir_id = ulaz._id ? ulaz._id : null; 
        if ( sir_id ) sirovine_ids.push(sir_id);
      });
      
      var best_plate_ulazi = await this_module.best_plate_find( 
        null,
        this_comp_id,
        product_id,
        kalk_type,
        kalk_sifra,
        curr_kalk,
        sirovine_ids,
        proces_index=null
      );
      
      if ( best_plate_ulazi ) curr_kalk.procesi[0].ulazi = best_plate_ulazi;
      
      this_module.update_kalk( curr_kalk, product_data, curr_proces_index=0, changed_ulaz=true, null, null );

      // setTimeout( function() {
        console.log(`--------------- make kalk kod odabira _prirez_kontra ----------------`);
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      // }, 400 );
      
    });
    
    
    $(`#${sifra}_for_proces`).off(`blur`);
    $(`#${sifra}_for_proces`).on(`blur`, function() {
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      
      var product_id = $(this_input).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var kalk_box = $(this_input).closest('.kalk_box');
      
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      if ( $(this_input).val() == `` ) {
        // ako je prazno
        curr_kalk.for_proces = null;
        
      } else {
        
        var input_value = cit_number( $(this_input).val() );
        
        if ( input_value !== null ) {
          curr_kalk.for_proces = input_value;
          format_number_input( curr_kalk.for_proces, this_input, 0);
        } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
          popup_warn(`Upis nije broj :-P`);
          $(this_input).val(``);
        };
        
      }
      
    });
    
    
    $(`#`+sifra+`_kalk_naklada`).off(`blur`);
    $(`#`+sifra+`_kalk_naklada`).on(`blur`, async function() {
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
      var product_id = $(this_input).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var kalk_box = $(this_input).closest('.kalk_box');
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      set_input_data(this_input, curr_kalk, kalk_sifra );
      
      
      $(`#${kalk_sifra}_kalkulacija .kalk_naslov .title_naklada`).html( $(this_input).val() || "---" );
      
      
      
      var sirovine_ids = [];
      
      $.each(curr_kalk.procesi[0].ulazi, function(u_ind, ulaz) {
        var sir_id = ulaz._id ? ulaz._id : null; 
        if ( sir_id ) sirovine_ids.push(sir_id);
      });
      
      var best_plate_ulazi = await this_module.best_plate_find( 
        null,
        this_comp_id,
        product_id,
        kalk_type,
        kalk_sifra,
        curr_kalk,
        sirovine_ids,
        proces_index=null
      );
      if ( best_plate_ulazi ) curr_kalk.procesi[0].ulazi = best_plate_ulazi;
      
      
      this_module.update_kalk( curr_kalk, product_data, curr_proces_index=0, changed_ulaz=true, null, null );
      
      
      console.log(`--------------- make kalk kod odabira _kalk_naklada ----------------`);
      var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null, `return_html` );
      $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      reset_tooltips();
      
      setTimeout( function() {
        this_module.write_prirez(null, this_comp_id, product_id, kalk_type, curr_kalk.kalk_sifra );
      }, 100 );
      
    });
    
    
    
    $(`#${sifra}_kalk_ploca_mat`).data('cit_props', {
      
      desc: 'odabir _kalk_ploca_mat unutar kalkulacije : ' + sifra,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: `mat_mix`,
      cit_run: async function(state) {
        
        var this_input = $('#'+current_input_id)[0];
        
        var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
        var product_id = $(this_input).closest('.product_comp')[0].id;
        var product_data =  this_module.product_module.cit_data[product_id];


        var kalk_box = $(this_input).closest('.kalk_box');
        var kalk_type = kalk_box.attr(`data-kalk_type`);
        var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);

        var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          curr_kalk.kalk_ploca_mat = null;
          return;
        };

        $('#'+current_input_id).val(state.naziv);
        
        curr_kalk.kalk_ploca_mat = state;

        // set_input_data(this_input, curr_kalk, kalk_sifra ); ----> ovo je samo ako je value number !!!!

        $(`#${kalk_sifra}_kalkulacija .kalk_naslov .title_mat`).html( state.naziv || "---" );

        var sirovine_ids = [];

        $.each(curr_kalk.procesi[0].ulazi, function(u_ind, ulaz) {
          var sir_id = ulaz._id ? ulaz._id : null; 
          if ( sir_id ) sirovine_ids.push(sir_id);
        });

        var best_plate_ulazi = await this_module.best_plate_find( 
          null,
          this_comp_id,
          product_id,
          kalk_type,
          kalk_sifra,
          curr_kalk,
          sirovine_ids,
          proces_index=null
        );
        if ( best_plate_ulazi ) curr_kalk.procesi[0].ulazi = best_plate_ulazi;

        this_module.update_kalk( curr_kalk, product_data, curr_proces_index=0, changed_ulaz=true, null, null );


        console.log(`--------------- make kalk kod odabira _kalk_naklada ----------------`);
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null, `return_html` );
        $(`#${data.id} .${kalk_type}_container`).html(this_kalk);

        setTimeout( function() {
          this_module.write_prirez(null, this_comp_id, product_id, kalk_type, curr_kalk.kalk_sifra );
        }, 200 );
        
        
      },
      
    });
        
    
    
    
    
    $(`#`+sifra+`_kalk_discount`).off(`blur`);
    $(`#`+sifra+`_kalk_discount`).on(`blur`, async function() {
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
      var product_id = $(this_input).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      var kalk_box = $(this_input).closest('.kalk_box');
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      
      set_input_data(this_input, curr_kalk, kalk_sifra );

      var new_value = cit_number( $(this_input).val() );
      if ( new_value == null ) new_value = 0;
      
      
      var price_start = curr_kalk.kalk_final_unit_price;
      var price_final = null;
           
      // ako nema niti maržu niti popust 
      if ( !curr_kalk.kalk_markup && !curr_kalk.kalk_discount ) {
        price_final = price_start;
        curr_kalk.kalk_discount_money = 0;
        curr_kalk.kalk_markup_money = 0
        
      };
      // ima maržu nema popust
      if ( curr_kalk.kalk_markup && !curr_kalk.kalk_discount ) {
        price_final = price_start * (100 + curr_kalk.kalk_markup )/100;
        curr_kalk.kalk_discount_money = 0;
        curr_kalk.kalk_markup_money = (price_final - price_start) * curr_kalk.kalk_naklada;
      };
      // ima popust nema maržu
      if ( !curr_kalk.kalk_markup && curr_kalk.kalk_discount ) {
        price_final = price_start * (100 - curr_kalk.kalk_discount )/100;
        curr_kalk.kalk_discount_money = ( price_start - price_final ) * curr_kalk.kalk_naklada;
        curr_kalk.kalk_markup_money = 0;
      };
      
      format_number_input( curr_kalk.kalk_markup, $(`#`+kalk_sifra+`_kalk_markup`)[0], 2);
      format_number_input( curr_kalk.kalk_discount, $(`#`+kalk_sifra+`_kalk_discount`)[0], 2);
      
      format_number_input( curr_kalk.kalk_markup_money, $(`#`+kalk_sifra+`_kalk_markup_money`)[0], 2);
      format_number_input( curr_kalk.kalk_discount_money, $(`#`+kalk_sifra+`_kalk_discount_money`)[0], 2);
      
      
      // ima i maržu i popust
      if ( curr_kalk.kalk_markup && curr_kalk.kalk_discount ) {
        
        // startna cijena je sada s popustom
        price_start = curr_kalk.kalk_final_unit_price * (100 + curr_kalk.kalk_markup )/100;
        curr_kalk.kalk_markup_money = (price_start - curr_kalk.kalk_final_unit_price) * curr_kalk.kalk_naklada;
        
        price_final = price_start * (100 - curr_kalk.kalk_discount )/100;
        curr_kalk.kalk_discount_money = (price_start - price_final) * curr_kalk.kalk_naklada;
        
        format_number_input( curr_kalk.kalk_markup_money, $(`#`+kalk_sifra+`_kalk_markup_money`)[0], 2);
        format_number_input( curr_kalk.kalk_discount_money, $(`#`+kalk_sifra+`_kalk_discount_money`)[0], 2);
        
      };

      
      curr_kalk.kalk_final_unit_price_dis_mark = price_final;
      curr_kalk.kalk_final_sum_price_dis_mark = price_final * curr_kalk.kalk_naklada;
     
      
      var unit_price_final_input = $(`#` + kalk_sifra + `_kalk_final_unit_price_dis_mark`)[0];
      format_number_input( price_final, unit_price_final_input, 2);


      var sum_price_final_input = $(`#` + kalk_sifra + `_kalk_final_sum_price_dis_mark`)[0];
      format_number_input( price_final * curr_kalk.kalk_naklada, sum_price_final_input, 2);
      
      if ( curr_kalk.kalk_final_unit_price_dis_mark < curr_kalk.kalk_final_unit_price ) {
        popup_warn(`Finalna cijena NE SMJE biti manja od početne cijene!!!<br>Probaj ponovo :)`);
        this_module.reset_kalk_prices(curr_kalk);
      };
      
    });
    
    
    $(`#`+sifra+`_kalk_markup`).off(`blur`);
    $(`#`+sifra+`_kalk_markup`).on(`blur`, async function() {
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
      var product_id = $(this_input).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      var kalk_box = $(this_input).closest('.kalk_box');
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      
      set_input_data( this_input, curr_kalk, kalk_sifra );
      
      var new_value = cit_number( $(this_input).val() );
      if ( new_value == null ) new_value = 0;

      var price_start = curr_kalk.kalk_final_unit_price;
      var price_final = null;
           
      // ako nema niti maržu niti popust 
      if ( !curr_kalk.kalk_markup && !curr_kalk.kalk_discount ) {
        price_final = price_start;
        curr_kalk.kalk_discount_money = 0;
        curr_kalk.kalk_markup_money = 0
      };
      // ima maržu nema popust
      if ( curr_kalk.kalk_markup && !curr_kalk.kalk_discount ) {
        price_final = price_start * (100 + curr_kalk.kalk_markup )/100;
        curr_kalk.kalk_discount_money = 0;
        curr_kalk.kalk_markup_money = (price_final - price_start) * curr_kalk.kalk_naklada;
      };
      // ima popust nema maržu
      if ( !curr_kalk.kalk_markup && curr_kalk.kalk_discount ) {
        price_final = price_start * (100 - curr_kalk.kalk_discount )/100;
        curr_kalk.kalk_discount_money = ( price_start - price_final ) * curr_kalk.kalk_naklada;
        curr_kalk.kalk_markup_money = 0;
      };
      
      format_number_input( curr_kalk.kalk_markup, $(`#`+kalk_sifra+`_kalk_markup`)[0], 2);
      format_number_input( curr_kalk.kalk_discount, $(`#`+kalk_sifra+`_kalk_discount`)[0], 2);
      
      format_number_input( curr_kalk.kalk_markup_money, $(`#`+kalk_sifra+`_kalk_markup_money`)[0], 2);
      format_number_input( curr_kalk.kalk_discount_money, $(`#`+kalk_sifra+`_kalk_discount_money`)[0], 2);
      
      // ima i maržu i popust
      if ( curr_kalk.kalk_markup && curr_kalk.kalk_discount ) {
        
        // startna cijena je sada s popustom
        price_start = curr_kalk.kalk_final_unit_price * (100 + curr_kalk.kalk_markup )/100;
        curr_kalk.kalk_markup_money = (price_start - curr_kalk.kalk_final_unit_price) * curr_kalk.kalk_naklada;
        
        price_final = price_start * (100 - curr_kalk.kalk_discount )/100;
        curr_kalk.kalk_discount_money = (price_start - price_final) * curr_kalk.kalk_naklada;
        
        format_number_input( curr_kalk.kalk_markup_money, $(`#`+kalk_sifra+`_kalk_markup_money`)[0], 2);
        format_number_input( curr_kalk.kalk_discount_money, $(`#`+kalk_sifra+`_kalk_discount_money`)[0], 2);
        
      };

      
      curr_kalk.kalk_final_unit_price_dis_mark = price_final;
      curr_kalk.kalk_final_sum_price_dis_mark = price_final * curr_kalk.kalk_naklada;
      
      var unit_price_final_input = $(`#` + kalk_sifra + `_kalk_final_unit_price_dis_mark`)[0];
      format_number_input( price_final, unit_price_final_input, 2);


      var sum_price_final_input = $(`#` + kalk_sifra + `_kalk_final_sum_price_dis_mark`)[0];
      format_number_input( price_final * curr_kalk.kalk_naklada, sum_price_final_input, 2);

      
      if ( curr_kalk.kalk_final_unit_price_dis_mark < curr_kalk.kalk_final_unit_price ) {
        popup_warn(`finalna cijena NE SMJE biti manja od proizvodne cijene!!!<br>Probaj ponovo :)`);
        this_module.reset_kalk_prices(curr_kalk);
      };
      

    });
    
    
    $(`#`+sifra+`_kalk_discount_money`).off(`blur`);
    $(`#`+sifra+`_kalk_discount_money`).on(`blur`, async function() {

      var this_input = this;

      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
      var product_id = $(this_input).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];

      var kalk_box = $(this_input).closest('.kalk_box');
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);

      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );


      set_input_data(this_input, curr_kalk, kalk_sifra );

      var new_value = cit_number( $(this_input).val() );
      if ( new_value == null ) new_value = 0;

      var price_start = curr_kalk.kalk_final_unit_price;
      var price_final = null;

      if ( !curr_kalk.kalk_discount_money && !curr_kalk.kalk_markup_money ) {
        curr_kalk.kalk_markup_money = 0;
        curr_kalk.kalk_discount_money = 0;
        curr_kalk.kalk_markup = 0;
        curr_kalk.kalk_discount = 0;
      };
      
      // postoji popust u kunama onda izračunaj popust u %
      if ( curr_kalk.kalk_discount_money && !curr_kalk.kalk_markup_money ) {
        curr_kalk.kalk_markup_money = 0;
        curr_kalk.kalk_markup = 0;
        curr_kalk.kalk_discount = 100 * curr_kalk.kalk_discount_money / (price_start * curr_kalk.kalk_naklada);
      };
      
      // ako marža u kunama onda izračunaj maržu u %
      if ( !curr_kalk.kalk_discount_money && curr_kalk.kalk_markup_money ) {
        curr_kalk.kalk_discount_money = 0;
        curr_kalk.kalk_discount = 0;
        curr_kalk.kalk_markup = 100 * curr_kalk.kalk_markup_money / (price_start * curr_kalk.kalk_naklada);
      };
      
      if ( curr_kalk.kalk_discount_money && curr_kalk.kalk_markup_money ) {
        curr_kalk.kalk_markup = 100 * curr_kalk.kalk_markup_money / (price_start * curr_kalk.kalk_naklada);
        var sum_price_with_markup = (price_start * curr_kalk.kalk_naklada) + curr_kalk.kalk_markup_money;
        curr_kalk.kalk_discount = 100 * curr_kalk.kalk_discount_money / sum_price_with_markup
      };
      
      format_number_input( curr_kalk.kalk_markup, $(`#`+kalk_sifra+`_kalk_markup`)[0], 2);
      format_number_input( curr_kalk.kalk_discount, $(`#`+kalk_sifra+`_kalk_discount`)[0], 2);
      
      format_number_input( curr_kalk.kalk_markup_money, $(`#`+kalk_sifra+`_kalk_markup_money`)[0], 2);
      format_number_input( curr_kalk.kalk_discount_money, $(`#`+kalk_sifra+`_kalk_discount_money`)[0], 2);
      
      
      // ako nema niti maržu niti popust 
      if ( !curr_kalk.kalk_markup && !curr_kalk.kalk_discount ) price_final = price_start;
      // ima maržu nema popust
      if ( curr_kalk.kalk_markup && !curr_kalk.kalk_discount ) price_final = price_start * (100 + curr_kalk.kalk_markup )/100;
      // ima popust nema maržu
      if ( !curr_kalk.kalk_markup && curr_kalk.kalk_discount ) price_final = price_start * (100 - curr_kalk.kalk_discount )/100;

      // ima i maržu i popust
      if ( curr_kalk.kalk_markup && curr_kalk.kalk_discount ) {

        // startna cijena je sada s popustom
        price_start = curr_kalk.kalk_final_unit_price * (100 + curr_kalk.kalk_markup )/100;
        price_final = price_start * (100 - curr_kalk.kalk_discount )/100;
      };


      curr_kalk.kalk_final_unit_price_dis_mark = price_final;
      curr_kalk.kalk_final_sum_price_dis_mark = price_final * curr_kalk.kalk_naklada;


      var unit_price_final_input = $(`#` + kalk_sifra + `_kalk_final_unit_price_dis_mark`)[0];
      format_number_input( price_final, unit_price_final_input, 2);


      var sum_price_final_input = $(`#` + kalk_sifra + `_kalk_final_sum_price_dis_mark`)[0];
      format_number_input( price_final * curr_kalk.kalk_naklada, sum_price_final_input, 2);

      if ( curr_kalk.kalk_final_unit_price_dis_mark < curr_kalk.kalk_final_unit_price ) {
        popup_warn(`finalna cijena NE SMJE biti manja od proizvodne cijene!!!<br>Probaj ponovo :)`);
        this_module.reset_kalk_prices(curr_kalk);
      };


    });


    $(`#`+sifra+`_kalk_markup_money`).off(`blur`);
    $(`#`+sifra+`_kalk_markup_money`).on(`blur`, async function() {

      var this_input = this;

      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
      var product_id = $(this_input).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];

      var kalk_box = $(this_input).closest('.kalk_box');
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);

      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );


      set_input_data(this_input, curr_kalk, kalk_sifra );

      var new_value = cit_number( $(this_input).val() );
      if ( new_value == null ) new_value = 0;

      var price_start = curr_kalk.kalk_final_unit_price;
      var price_final = null;

      if ( !curr_kalk.kalk_discount_money && !curr_kalk.kalk_markup_money ) {
        curr_kalk.kalk_markup_money = 0;
        curr_kalk.kalk_discount_money = 0;
        curr_kalk.kalk_markup = 0;
        curr_kalk.kalk_discount = 0;
      };
      
      // postoji popust u kunama onda izračunaj popust u %
      if ( curr_kalk.kalk_discount_money && !curr_kalk.kalk_markup_money ) {
        curr_kalk.kalk_markup_money = 0;
        curr_kalk.kalk_markup = 0;
        curr_kalk.kalk_discount = 100 * curr_kalk.kalk_discount_money / (price_start * curr_kalk.kalk_naklada);
      };
      
      // ako marža u kunama onda izračunaj maržu u %
      if ( !curr_kalk.kalk_discount_money && curr_kalk.kalk_markup_money ) {
        curr_kalk.kalk_discount_money = 0;
        curr_kalk.kalk_discount = 0;
        curr_kalk.kalk_markup = 100 * curr_kalk.kalk_markup_money / (price_start * curr_kalk.kalk_naklada);
      };
      
      if ( curr_kalk.kalk_discount_money && curr_kalk.kalk_markup_money ) {
        curr_kalk.kalk_markup = 100 * curr_kalk.kalk_markup_money / (price_start * curr_kalk.kalk_naklada);
        var sum_price_with_markup = (price_start * curr_kalk.kalk_naklada) + curr_kalk.kalk_markup_money;
        curr_kalk.kalk_discount = 100 * curr_kalk.kalk_discount_money / sum_price_with_markup
      };
      
      format_number_input( curr_kalk.kalk_markup, $(`#`+kalk_sifra+`_kalk_markup`)[0], 2);
      format_number_input( curr_kalk.kalk_discount, $(`#`+kalk_sifra+`_kalk_discount`)[0], 2);
      
      format_number_input( curr_kalk.kalk_markup_money, $(`#`+kalk_sifra+`_kalk_markup_money`)[0], 2);
      format_number_input( curr_kalk.kalk_discount_money, $(`#`+kalk_sifra+`_kalk_discount_money`)[0], 2);
      
      

      // ako nema niti maržu niti popust 
      if ( !curr_kalk.kalk_markup && !curr_kalk.kalk_discount ) price_final = price_start;
      // ima maržu nema popust
      if ( curr_kalk.kalk_markup && !curr_kalk.kalk_discount ) price_final = price_start * (100 + curr_kalk.kalk_markup )/100;
      // ima popust nema maržu
      if ( !curr_kalk.kalk_markup && curr_kalk.kalk_discount ) price_final = price_start * (100 - curr_kalk.kalk_discount )/100;

      // ima i maržu i popust
      if ( curr_kalk.kalk_markup && curr_kalk.kalk_discount ) {

        // startna cijena je sada s popustom
        price_start = curr_kalk.kalk_final_unit_price * (100 + curr_kalk.kalk_markup )/100;
        price_final = price_start * (100 - curr_kalk.kalk_discount )/100;
      };


      curr_kalk.kalk_final_unit_price_dis_mark = price_final;
      curr_kalk.kalk_final_sum_price_dis_mark = price_final * curr_kalk.kalk_naklada;


      var unit_price_final_input = $(`#` + kalk_sifra + `_kalk_final_unit_price_dis_mark`)[0];
      format_number_input( price_final, unit_price_final_input, 2);


      var sum_price_final_input = $(`#` + kalk_sifra + `_kalk_final_sum_price_dis_mark`)[0];
      format_number_input( price_final * curr_kalk.kalk_naklada, sum_price_final_input, 2);

      if ( curr_kalk.kalk_final_unit_price_dis_mark < curr_kalk.kalk_final_unit_price ) {
        popup_warn(`finalna cijena NE SMJE biti manja od proizvodne cijene!!!<br>Probaj ponovo :)`);
        this_module.reset_kalk_prices(curr_kalk);
      };


    });

    
    $(`#`+sifra+`_kalk_final_unit_time`).off(`blur`);
    $(`#`+sifra+`_kalk_final_unit_time`).on(`blur`, async function() {
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
      var product_id = $(this_input).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      var kalk_box = $(this_input).closest('.kalk_box');
      
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      
      if (  $(this).val() == `` ) {
        curr_kalk.kalk_final_unit_time = null;
        return;
      };

      
      var input_value = cit_number( $(this).val() );
      
      if ( input_value !== null ) {
        curr_kalk.kalk_final_unit_time = input_value;
        
        var value_text = !input_value ? "" : cit_format( input_value, 0);
        $(this).val( value_text );
        
      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
        popup_warn(`Nije broj :-P`);
        // stavi postojeću
        var value_text = !curr_kalk.kalk_final_unit_time ? "" : cit_format( curr_kalk.kalk_final_unit_time, 0);
        $(this).val( value_text );
      };
      
      console.log(curr_kalk);
      
      
    });
    
    
    $(`#`+sifra+`_kalk_final_time`).off(`blur`);
    $(`#`+sifra+`_kalk_final_time`).on(`blur`, async function() {
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
      var product_id = $(this_input).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      var kalk_box = $(this_input).closest('.kalk_box');
      
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      
      if (  $(this).val() == "" ) {
        curr_kalk.kalk_final_time = null;
        return;
      };

      
      var input_value = cit_number( $(this).val() );
      
      if ( input_value !== null ) {
        
        curr_kalk.kalk_final_time = input_value;
        
        var value_text = !input_value ? "" : cit_format( input_value, 0);
        $(this).val( value_text );
        
        // napravi update tooltipa tj title za tooltip
        $(this).attr( `title`, curr_kalk.kalk_final_time ? cit_h( curr_kalk.kalk_final_time) : "" );
        
        
        reset_tooltips();
        
      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
        popup_warn(`Nije broj :-P`);
        // stavi postojeću
        var value_text = !curr_kalk.kalk_final_time ? "" : cit_format( curr_kalk.kalk_final_time, 0);
        $(this).val( value_text );
      };
      
      console.log(curr_kalk);
      
      
    });
    
    
    $(`#`+sifra+`_kalk_final_unit_price`).off(`blur`);
    $(`#`+sifra+`_kalk_final_unit_price`).on(`blur`, async function() {
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
      var product_id = $(this_input).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      var kalk_box = $(this_input).closest('.kalk_box');
      
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      
      if (  $(this).val() == "" ) {
        curr_kalk.kalk_final_unit_price = null;
        return;
      };

      
      var input_value = cit_number( $(this).val() );
      
      if ( input_value !== null ) {
        curr_kalk.kalk_final_unit_price = input_value;
        var value_text = !input_value ? "" : cit_format( input_value, 2);
        $(this).val( value_text );
        
        var naklada = curr_kalk.kalk_naklada;
        curr_kalk.kalk_final_sum_price = input_value * naklada;
        $(`#`+sifra+`_kalk_final_sum_price`).val( cit_format( curr_kalk.kalk_final_sum_price, 2) );
        
      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
        
        popup_warn(`Nije broj :-P`);
        // stavi postojeću
        var value_text = !curr_kalk.kalk_final_unit_price ? "" : cit_format( curr_kalk.kalk_final_unit_price, 2);
        $(this).val( value_text );
      };
      
      console.log(curr_kalk);
      
    });
    
    
    $(`#`+sifra+`_kalk_final_sum_price`).off(`blur`);
    $(`#`+sifra+`_kalk_final_sum_price`).on(`blur`, async function() {
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
      var product_id = $(this_input).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      var kalk_box = $(this_input).closest('.kalk_box');
      
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      
      if (  $(this).val() == "" ) {
        curr_kalk.kalk_final_sum_price = null;
        return;
      };

      
      var input_value = cit_number( $(this).val() );
      
      if ( input_value !== null ) {
        curr_kalk.kalk_final_sum_price = input_value;
        var value_text = !input_value ? "" : cit_format( input_value, 2);
        $(this).val( value_text );
        
      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
        popup_warn(`Nije broj :-P`);
        // stavi postojeću
        var value_text = !curr_kalk.kalk_final_sum_price ? "" : cit_format( curr_kalk.kalk_final_sum_price, 2);
        $(this).val( value_text );
      };
      
      console.log(curr_kalk);
      
    });
    
    
    $(`#${sifra}_kalkulacija .kalk_prirez_btn`).off(`click`);
    $(`#${sifra}_kalkulacija .kalk_prirez_btn`).on(`click`, this_module.write_prirez );

    
    $(`#${sifra}_kalkulacija .find_best_plate`).off(`click`);
    $(`#${sifra}_kalkulacija .find_best_plate`).on(`click`, async function (e) {

      var this_input = this;
      
      var kalk_type  = $(this).closest(`.kalk_box`).attr(`data-kalk_type`);
      var kalk_sifra = $(this).closest(`.kalk_box`).attr(`data-kalk_sifra`);
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var product_id = $(this).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );

      
      if ( $(`#${sifra}_for_proces`).val() == "" || !curr_kalk.for_proces ) {
        popup_warn(`Potrebno upisati broj procesa !!!`);
        return;
      };
      
      
      // resetiraj ulaze u odabranom procesu
      // tako da ih mogu napraviti ponovo !!!!
      if ( curr_kalk.procesi?.length > 0 ) curr_kalk.procesi[curr_kalk.for_proces - 1].ulazi = [];
      
      
      var new_ulazi = await this_module.best_plate_find(e);
      
      
      curr_kalk.procesi[curr_kalk.for_proces - 1].ulazi = new_ulazi || [];

      
      console.log(`--------------- make kalks on button FIND BEST PLATE ----------------`);
      var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null, `return_html` );
      $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      setTimeout( function() {
        this_module.write_prirez(null, this_comp_id, product_id, kalk_type, curr_kalk.kalk_sifra );
      }, 200 );


    }); // kraj eventa klik na best plate
    


    function estimate_proces_skart( arg_curr_kalk, arg_proces_index ) {

      
      var curr_proces = arg_curr_kalk.procesi[arg_proces_index];
      var radna_stn = curr_proces.action.action_radna_stn;
      var max_skart = 0;
      
      
      $.each( curr_proces.ulazi, function(u_ind, ulaz) {


          // ako ima tip sirovine još uvijek NE SMJE biti tip sirovine pomoćna sirovina/element kao na primje ljepilo i slično
          if (
          (ulaz.tip_sirovine && ulaz.tip_sirovine.sifra !== `TSIR11`) // ako je prava sirovina onda mora imati tip sirovine RAZLIČIT OD POMOĆNI ELEMENT
            ||
          (ulaz.original_sirov?.tip_sirovine && ulaz.original_sirov?.tip_sirovine?.sifra !== `TSIR11`)
            ||
          ulaz.kalk_element && !ulaz._id  // ili može biti element nastao nekim prijašnjim procesom TJ PROCESNI ELEMENT
          ) {

            // ------------------------------------------------------------------------------------------
            // PROVJERIO SAM DA JE PRAVA ULAZNA SIROVINA
            // ------------------------------------------------------------------------------------------
            

            var ulaz_kolicina = ulaz.kalk_sum_sirovina;

            var skart_perc = 0;

            if ( ulaz_kolicina < 20 ) skart_perc = radna_stn["<20"] || 0;
            if ( ulaz_kolicina >= 20 &&  ulaz_kolicina < 50 ) skart_perc = radna_stn["20-50"] || 0;
            if ( ulaz_kolicina >= 50 &&  ulaz_kolicina < 100 ) skart_perc = radna_stn["50-100"] || 0;
            if ( ulaz_kolicina >= 100 &&  ulaz_kolicina < 200 ) skart_perc = radna_stn["100-200"] || 0;
            if ( ulaz_kolicina >= 200 &&  ulaz_kolicina < 300 ) skart_perc = radna_stn["200-300"] || 0;
            if ( ulaz_kolicina >= 300 &&  ulaz_kolicina < 500 ) skart_perc = radna_stn["300-500"] || 0;
            if ( ulaz_kolicina >= 500 &&  ulaz_kolicina < 1000 ) skart_perc = radna_stn["500-1000"] || 0;
            if ( ulaz_kolicina >= 1000 ) skart_perc = radna_stn[">1000"] || 0;
            
            

            // !!!!!!!!!!!!!!!!!!!!!!!! VAŽNO !!!!!!!!!!!!!!!!!!!!!!!!
            // skart_perc može biti < 1  ----> tada je to postotak npr 0,03 tj 3%
            // ako je skart_perc >= 1  onda to nije postotak nego jednostavno procjenjeni broj komada skarta

            // ------------------------------------ KOMADNI ŠKART ------------------------------------

            if ( skart_perc >= 1 && skart_perc > radna_stn.min_skart ) {
              // pretvaram KOMADNI ŠKART U POSTOTAK !!!!!
              // na primjer ako piše 20 kom škarta i imam 200 ploča
              // to znači da je skart estimate = 20 / 200 = 0,1 tj 10%
              // ako je estimate veći od proces količina -----> onda je koef veći od 1 na primjer 30 extra / 20 idealno = 1,5
              skart_perc = skart_perc / ulaz_kolicina;
            };

            // ako je perc manji od MIN
            if ( skart_perc * ulaz_kolicina < radna_stn.min_skart ) {
              skart_perc = radna_stn.min_skart / ulaz_kolicina;
            };

            // VAŽNO  !!!!!!
            // VAŽNO  !!!!!!
            // VAŽNO  !!!!!!
            
            if ( data.tip?.sub_tip == "STALAK" && skart_perc * ulaz_kolicina > 300 ) {
              // MAX ŠKART ZA STALAK JE UVIJEK 300 kom za sada !
              skart_perc = 300 / ulaz_kolicina;
            } else if ( data.tip?.sub_tip !== "STALAK" && skart_perc * ulaz_kolicina > 200 ) {
              // MAX ŠKART ZA SVE OSIM STALAKA JE UVIJEK 200 kom za sada !!!!
              skart_perc = 200 / ulaz_kolicina;
            };
            
            
            if ( skart_perc > max_skart ) max_skart = skart_perc;
            
            
            // ----------------------------------------------------------------------------------------------------------------
            // POSTOTAK SKARTA IZ KALK ELEMENTA 
            // uzmi postotak skarta IZ KALK ELEMENTA, ali samo ako je najveći
            // ----------------------------------------------------------------------------------------------------------------
            
            
            if ( 
              ulaz.kalk_element?.elem_tip?.elem_skart
              &&
              ulaz.kalk_element.elem_tip.elem_skart > max_skart 
            )  {
              max_skart = ulaz.kalk_element.elem_tip.elem_skart;
            };
            
            
            // ---------------------------- BRUNO MI REKAO DA SMANJIM ŠKART ŠTO JE VEĆA NAKLADA 
            if ( arg_curr_kalk.kalk_naklada > 100 && max_skart >= 0.03 ) {
              
              max_skart = max_skart - ( (arg_curr_kalk.kalk_naklada - 100) * max_skart/200 );
              if ( max_skart < 0.01 ) max_skart = 0.01;
              
            };
            
            
            
            // var skart_kom =  Math.ceil( skart_perc * ulaz_kolicina ); // zaokruži da bude integer
            

          }; // kraj da nije pomoćna sirovina/element

      }); // kraj loopa po svim ulazima ovog procesa

      arg_curr_kalk.procesi[arg_proces_index].proces_skart = max_skart || 0;
      
      
    };
    // END estimate proces skart
    
    
    // gumb za procjenu škarta
    $(`#${sifra}_kalkulacija .estimate_skart_btn`).off(`click`);
    $(`#${sifra}_kalkulacija .estimate_skart_btn`).on(`click`, async function (e) {

      var this_input = this;
      
      var kalk_type  = $(this).closest(`.kalk_box`).attr(`data-kalk_type`);
      var kalk_sifra = $(this).closest(`.kalk_box`).attr(`data-kalk_sifra`);
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var product_id = $(this_input).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      var naklada = curr_kalk.kalk_naklada;

      
      $.each( curr_kalk.procesi, function(p_ind, proc) {
        estimate_proces_skart( curr_kalk, p_ind );
      });
      

      $.each(curr_kalk.procesi, function(p_ind, proc) {
        
        // obriši SVE SVE custom izlaze !!!!
        if ( curr_kalk.procesi[p_ind].extra_data?.izlaz_kom ) delete curr_kalk.procesi[p_ind].extra_data.izlaz_kom;
        
        // ako ulaz ima _id i 
        if ( is_start_proces(proc) ) { 
          // --------------- NA POČETKU RESET EXTRA NA NULA !!!!!!!!!!!!!!!
          // --------------- NA POČETKU RESET EXTRA NA NULA !!!!!!!!!!!!!!!
          curr_kalk.procesi[p_ind].ulazi[0].kalk_extra_kom = 0;
          
        };
        
        
      });

      
      console.log(`--------------- make kalks on button ADD EXTRA BUTTON ----------------`);
      var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null, `return_html` );
      $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      
      
    }); // kraj eventa klik na SAMO ESTIMATE ŠKART
        
        
    
    
    
    // gumb za dodavanje extra za škarta
    $(`#${sifra}_kalkulacija .add_extra_btn`).off(`click`);
    $(`#${sifra}_kalkulacija .add_extra_btn`).on(`click`, async function (e) {

      var this_input = this;
      
      var kalk_type  = $(this).closest(`.kalk_box`).attr(`data-kalk_type`);
      var kalk_sifra = $(this).closest(`.kalk_box`).attr(`data-kalk_sifra`);
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var product_id = $(this_input).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      var naklada = curr_kalk.kalk_naklada;

      
      $.each( curr_kalk.procesi, function(p_ind, proc) {
        estimate_proces_skart( curr_kalk, p_ind );
      });
      
      


      $.each(curr_kalk.procesi, function(p_ind, proc) {
        
        
        // obriši SVE SVE  custom izlaze !!!!
        if ( curr_kalk.procesi[p_ind].extra_data?.izlaz_kom ) delete curr_kalk.procesi[p_ind].extra_data.izlaz_kom;
        
        // ako ulaz ima _id i ako je start proces
        if ( is_start_proces(proc) ) { 
          // --------------- NA POČETKU RESET EXTRA NA NULA !!!!!!!!!!!!!!!
          // --------------- NA POČETKU RESET EXTRA NA NULA !!!!!!!!!!!!!!!
          curr_kalk.procesi[p_ind].ulazi[0].kalk_extra_kom = 0;
          
        };
        
      });


      
      // ------------------------------------------------------------------------------------------
      
      var x;
      var is_satis = false;
      var x_count = 0;
      for( x=1; x <= 10000; x++ ) {
        
        x_count += 1;
        
        $.each(curr_kalk.procesi, function(p_ind, proc) {
          // DODAJ EXTRA KOMADA SAMO NA POČETNE PROCESE TJ ULAZE U POĆETNIM PROCESIMA !!!!!!!!!
          // DODAJ EXTRA KOMADA SAMO NA POČETNE PROCESE TJ ULAZE U POĆETNIM PROCESIMA !!!!!!!!!
          if ( is_start_proces(proc) && is_satis == false ) { 
            curr_kalk.procesi[p_ind].ulazi[0].kalk_extra_kom = ( curr_kalk.procesi[p_ind].ulazi[0].kalk_extra_kom || 0 ) + 1; 
          };  
        });
        
        
        if ( x_count == 62 ) {
          console.log(`x_count == 62`);
        };
        
        // NAPRAVI UPDATE NA PRVOM PROCESU I TO ĆE SE PROPAGIRATI NA SVE OSTALE PROCESE DO KRAJA
        this_module.update_kalk( curr_kalk, product_data, 0, changed_ulaz=true, null, null );
        
        var last_izlaz_kom = curr_kalk.procesi[ curr_kalk.procesi.length - 1 ].izlazi[0].kalk_sum_sirovina;

        if ( last_izlaz_kom >= naklada ) {
          is_satis = true;
          break;
        };

      }; // kraj for loopa
      

      // ------------------------------------------------------------------------------------------
      
      

      console.log(`--------------- make kalks on button ADD EXTRA BUTTON ----------------`);
      var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null, `return_html`, kalk_type, kalk_sifra );
      $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      
      
    }); // kraj eventa klik na add extra btn
     
    
    
    
    // gumb za dodavanje extra za škarta
    $(`#${sifra}_kalkulacija .add_extra_reverse_btn`).off(`click`);
    $(`#${sifra}_kalkulacija .add_extra_reverse_btn`).on(`click`, async function (e) {

      var this_input = this;
      
      var kalk_type  = $(this).closest(`.kalk_box`).attr(`data-kalk_type`);
      var kalk_sifra = $(this).closest(`.kalk_box`).attr(`data-kalk_sifra`);
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var product_id = $(this_input).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      var naklada = curr_kalk.kalk_naklada;

      
      $.each( curr_kalk.procesi, function(p_ind, proc) {
        estimate_proces_skart( curr_kalk, p_ind );
      });
      
      

      $.each(curr_kalk.procesi, function(p_ind, proc) {
        
        
        // obriši SVE custom izlaze !!!!
        if ( curr_kalk.procesi[p_ind].extra_data?.izlaz_kom ) delete curr_kalk.procesi[p_ind].extra_data.izlaz_kom;
        
        // ako ulaz ima _id i ako je start proces
        if ( is_start_proces(proc) ) { 
          // --------------- NA POČETKU RESET EXTRA NA NULA !!!!!!!!!!!!!!!
          // --------------- NA POČETKU RESET EXTRA NA NULA !!!!!!!!!!!!!!!
          curr_kalk.procesi[p_ind].ulazi[0].kalk_extra_kom = 0;
          
          
        };
      
        
        $.each( curr_kalk.procesi[p_ind].ulazi, function(ul_ind, ulaz) {
          // vrati original kom ako postoji
          if ( ulaz.original_kom ) curr_kalk.procesi[p_ind].ulazi[ul_ind].kalk_sum_sirovina = ulaz.original_kom;
          curr_kalk.procesi[p_ind].ulazi[ul_ind].skart_koef = null;
          
        });
        
        $.each( curr_kalk.procesi[p_ind].izlazi, function(iz_ind, izlaz) {
          // vrati original kom ako postoji
          if ( izlaz.original_kom ) curr_kalk.procesi[p_ind].izlazi[iz_ind].kalk_sum_sirovina = izlaz.original_kom;
          curr_kalk.procesi[p_ind].ulazi[iz_ind].skart_koef = null;
          
        });
        
        
        
      });

      
      
      
      
      

      
      // ------------------------------------------------------------------------------------------
      

      function find_prev_proces_index( curr_kalk, izlaz_sifra_od_ulaza, arg_curr_proc_index ) {


        var prev_proces_index = null;

        var prev;

        for( prev = arg_curr_proc_index - 1 ; prev >= 0; prev-- ) {

          var prev_izlaz = find_item( curr_kalk.procesi[prev].izlazi, izlaz_sifra_od_ulaza, `sifra` );

          if ( prev_izlaz && prev_proces_index == null ) { // pazi da čim nađe prvi izlaz da prestane zapisivati prev index !!!

            prev_proces_index = prev;

          };


        }; // loop unazad po svim prijašnjim procesima


        return prev_proces_index;


      };


      function kalk_extra_reverse( curr_kalk, arg_proces_index, arg_skart_koef ) {


        var curr_proces = curr_kalk.procesi[arg_proces_index];
        var curr_skart_koef = curr_proces.proces_skart ? (1 - curr_proces.proces_skart) : 1; // prvo pogledaj skart za current proces

        // ako postoji argument skart koef onda ga pomnoži sa trenutnim koeficijentom !!!!
        // NA PRIMJER ako je skart u argumentu 0.97 i ako je trenutni skart 0.95 ----> onda trenutni skart postaje 0.95 * 0.97 = 0.9215

        if ( arg_skart_koef ) curr_skart_koef = curr_skart_koef * arg_skart_koef;


        var izlaz;
        // ako nije posve zadnji proces onda dodaj u izlaze sum (ne u extra polje)
        // ILI AKO SAMO POSTOJI JEDAN PROCES
        if ( arg_proces_index < curr_kalk.procesi.length -1  || curr_kalk.procesi.length == 1 ) {

          $.each( curr_proces.izlazi, function(iz_ind, izlaz ) {

            // samo ako do sada nema skart_koef
            if ( !izlaz.skart_koef ) {
              curr_kalk.procesi[arg_proces_index].izlazi[iz_ind].original_kom = izlaz.kalk_sum_sirovina;

              var potrebno_kom = Math.ceil( izlaz.kalk_sum_sirovina/arg_skart_koef );
              curr_kalk.procesi[arg_proces_index].izlazi[iz_ind].kalk_sum_sirovina = potrebno_kom;
              curr_kalk.procesi[arg_proces_index].izlazi[iz_ind].skart_koef = arg_skart_koef; // dodajem skart koef da znam da sam to izračunao
            };

          });

        };



        // DODAJ EXTRA KOMADA SAMO NA POČETNE PROCESE TJ ULAZE U POĆETNIM PROCESIMA !!!!!!!!!
        // DODAJ EXTRA KOMADA SAMO NA POČETNE PROCESE TJ ULAZE U POĆETNIM PROCESIMA !!!!!!!!!



        if ( is_start_proces(curr_proces) ) { 

          $.each( curr_proces.ulazi, function(u_ind, ulaz ) {

            // samo ako do sada nema skart_koef
            if ( !ulaz.skart_koef ) {
              var potrebno_kom = Math.ceil( ulaz.kalk_sum_sirovina / curr_skart_koef );
              var extra_kom = potrebno_kom - ulaz.kalk_sum_sirovina;
              curr_kalk.procesi[arg_proces_index].ulazi[u_ind].kalk_extra_kom = extra_kom; // DODAJ U EXTRA POLJE ( ne u sum sirovina)
              curr_kalk.procesi[arg_proces_index].ulazi[u_ind].skart_koef = curr_skart_koef;
            };

          });

          // -------------------------------- I TO JE KRAJ OVE GRANE JER NEMA VIŠE PRIJE OVOG PROCESA  !!!


        } else { 


          // ako nije start proces onda dodaj količinu u sum sirovina (NE U EXTRA POLJE)
          $.each( curr_proces.ulazi, function(u_ind, ulaz) {

            // samo ako do sada nema skart_koef
            if ( !ulaz.skart_koef ) {
              curr_kalk.procesi[arg_proces_index].ulazi[u_ind].original_kom = ulaz.kalk_sum_sirovina;
              var potrebno_kom =  Math.ceil( ulaz.kalk_sum_sirovina / curr_skart_koef );
              curr_kalk.procesi[arg_proces_index].ulazi[u_ind].kalk_sum_sirovina = potrebno_kom;
              curr_kalk.procesi[arg_proces_index].ulazi[u_ind].skart_koef = curr_skart_koef;
            };

          });


          // ako nije start proces onda traži sve procese od svih ulaza
          // pogledaj jel ovaj ulaz negdje IZLAZ

          $.each( curr_proces.ulazi, function(u_ind, ulaz) {

            var prev_proc_index = find_prev_proces_index(curr_kalk, ulaz.izlaz_sifra, arg_proces_index);
            if ( prev_proc_index !== null ) kalk_extra_reverse( curr_kalk, prev_proc_index, curr_skart_koef );

          });




        }; // kraj else ako ovo nije start proces 




      }; // kraj  kalk_extra_reverse


      kalk_extra_reverse(curr_kalk, curr_kalk.procesi.length - 1, 1 );

      // ------------------------------------------------------------------------------------------
      
      
      this_module.update_kalk( curr_kalk, product_data, curr_proces_index=0, null, null, changed_action=true );
      

      console.log(`--------------- make kalks on button ADD EXTRA BUTTON ----------------`);
      var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null, `return_html`, kalk_type, kalk_sifra );
      $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      
      
    }); // kraj eventa klik na add extra btn
        
    
        
    
    
    
    // gumb za dodavanje extra za škarta
    $(`#${sifra}_kalkulacija .schema_procesa_btn`).off(`click`);
    $(`#${sifra}_kalkulacija .schema_procesa_btn`).on(`click`, async function (e) {

      var this_input = this;
      
      var kalk_type  = $(this).closest(`.kalk_box`).attr(`data-kalk_type`);
      var kalk_sifra = $(this).closest(`.kalk_box`).attr(`data-kalk_sifra`);
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var product_id = $(this_input).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      var naklada = curr_kalk.kalk_naklada;

      
      
      console.log(`--------------- make kalks on button ADD EXTRA BUTTON ----------------`);
      var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null, `return_html`, kalk_type, kalk_sifra );
      $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      
      
    }); // kraj eventa klik na add extra btn
        
    
        
    
/*    
    
    $(document).ready(function() {
    var data = {
      operators: {
        operator1: {
          top: 100,
          left: 260,
          properties: {
            title: 'Operator 1',
            inputs: {},
            outputs: {
              output_1: {
                label: 'Output 1',
              }
            }
          }
        },
        operator2: {
          top: 320,
          left: 260,
          properties: {
            title: 'Operator 2',
            inputs: {
              input_1: {
                label: 'Input 1',
              },
              input_2: {
                label: 'Input 2',
              },
            },
            outputs: {}
          }
        },
      },
      links: {
        link_1: {
          fromOperator: 'operator1',
          fromConnector: 'output_1',
          toOperator: 'operator2',
          toConnector: 'input_2',
        },
      }
    };

    // Apply the plugin on a standard, empty div...
    $('#example_3_vertical').flowchart({
      verticalConnection: true,
      data: data
    });
  });
    
    */
    
    $(`#${sifra}_kalkulacija .kalk_mont_plates_btn`).off(`click`);
    $(`#${sifra}_kalkulacija .kalk_mont_plates_btn`).on(`click`, async function (e) {

      
      
      var this_input = this;
      
      var kalk_type  = $(this).closest(`.kalk_box`).attr(`data-kalk_type`);
      var kalk_sifra = $(this).closest(`.kalk_box`).attr(`data-kalk_sifra`);
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var product_id = $(this).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      if ( !curr_kalk.kalk_naklada ) {
        popup_warn(`Niste upisali nakladu`);
        return;
      };
       
      
      var popup_text = 
`Jeste li sigurni da želite pokrenti kalkulaciju MONTAŽNIH PLOČA?
<br>
Ova akcija će resetirati sve postojeće ulaze i izlaze u PRVOM procesu.
`;
      
      async function kalk_plates_yes() {

        if ( curr_kalk.procesi?.length > 0 ) curr_kalk.procesi[0].ulazi = [];
        
        
        // dobijem array koliko kojih ploča treba biti minimalno na primjer [2, 1]
        var result = this_module.kalk_plates(e);
        
        // u ovom slučaju za nakladu od 100 kom
        // idealno je { back_i_sides: 1*100, footer: 1*100, header: 1*100, shelfs: 4*100 }
        var IDEAL_product_elements  = {};
        $.each( result.product_elements, function(elem_key, elem_count ) {
          IDEAL_product_elements[elem_key] = elem_count * curr_kalk.kalk_naklada;
        });
        
        
        // ------------------------- AKO USER PREF. MANJAK ONDA ĆE GA APP UPOZORITI 
        
        
        // u ovom objektu stoji koliko svakih elemenata ima u ovoj kombinaciji ploča
        // npr za [2, 1 ]  ------> kombinaciju bit će ukupno ovliko elemenata ----->  { back_i_sides: 4, footer: 4, header: 4, shelfs: 10 }
        // u ovom slučaju minimalni elem count je 4
        var min_elem_count = 9999999999;
        $.each( result.plate_comb.plates, function(elem_key, elem_count ) {
          if ( elem_count < min_elem_count ) min_elem_count = elem_count;
        });
        
        
        // u ovom slučaju min count for naklada je 25 ako je naklada  = 100 kom
        // 100/4 = 25
        var min_elem_count_for_naklada = curr_kalk.kalk_naklada / min_elem_count;
        min_elem_count_for_naklada = Math.ceil( min_elem_count_for_naklada );
        
        
        var REAL_product_elements = cit_deep( result.plate_comb.plates );
        $.each( REAL_product_elements, function(elem_key, elem_count ) {
          REAL_product_elements[elem_key] = elem_count * min_elem_count_for_naklada
        });
        
        // -----------END-------------- AKO USER PREF. MANJAK ONDA ĆE GA APP UPOZORITI 
        
        
        
        if ( curr_kalk.allow_mont_visak == true ) {
          
          // Traži max umjesto min
          var max_elem_count = 0;
          $.each( result.plate_comb.plates, function(elem_key, elem_count ) {
            if ( elem_count > max_elem_count ) max_elem_count = elem_count;
          });


          // u ovom slučaju min count for naklada je 25 ako je naklada  = 100 kom
          // 100/4 = 25
          var max_elem_count_for_naklada = curr_kalk.kalk_naklada / max_elem_count;
          max_elem_count_for_naklada = Math.ceil( max_elem_count_for_naklada );


          var REAL_product_elements = cit_deep( result.plate_comb.plates );
          $.each( REAL_product_elements, function(elem_key, elem_count ) {
            REAL_product_elements[elem_key] = elem_count * max_elem_count_for_naklada
          });
          
          
        };
        
        
        var real_ideal_diff = {};
        $.each( REAL_product_elements, function(real_key, real_count ) {
          
          var real_count = real_count;
          var ideal_count = IDEAL_product_elements[real_key];
          
          if ( ideal_count !== real_count ) {
            real_ideal_diff[real_key] = real_count - ideal_count;
          };
          
        });
        
        
        var most_negative = 0;
        if ( curr_kalk.allow_mont_visak == true ) {
          
          // traži najveći negativni diff tj kojeg elementa nedostaje najviše
          $.each( real_ideal_diff, function(dif_ind, diff) {
            if ( diff < most_negative ) most_negative = diff;
          });
          
          
          if ( most_negative < 0 ) {
            
            
          };
          
          
        };
        
        
        
        if ( Object.keys(real_ideal_diff).length > 0 ) {
          
          console.log( real_ideal_diff );
          
          return;
          
        };
        
        
        var sirov_ids = [];

        $.each( result.plates_elements, function(p_ind, plate) {
          if ( sirov_ids.indexOf(plate.sirov_id) == -1 ) sirov_ids.push( plate.sirov_id );
        });
        
        
        
        
        
        return;
        
        
        
        var mont_plate_sirovs = await this_module.get_sirov_for_mont_plates(sirov_ids, product_data );
        
        if ( mont_plate_sirovs ) {
          
          // moram paziti na redosljed jer preko http requesta možda array sirovina neće biti jednako poredan 
          $.each( result.plates_elements, function(p_ind, plate) {
            
            var current_sirov_id = plate.sirov_id;
            
            $.each( mont_plate_sirovs, function(s_ind, sirov) {
              
              // ubaci cijeli sirov objekt u plate objekt
              if ( sirov._id == current_sirov_id ) {
                
                result.plates_elements.sirov = sirov;
                  
              };
              
            });

          });
          
          
          $.each( result.plates_elements, function(p_ind, plate) {
            
            result.plates_elements[p_ind].sirov.full_naziv = window.concat_naziv_sirovine(sirov);
            
            
                                            // state, upsert, arg_data, arg_product_id, arg_kalk, arg_proces
            var new_ulaz = select_find_sirov( 
              result.plates_elements[p_ind].sirov, // state
              upsert=false, // upsert
              data, // arg_data
              product_id, // arg_product_id
              curr_kalk, // arg_kalk
              curr_kalk.procesi[0] // arg_proces
            );
            
            // comb je isti index kao index od plates elements !!!!
            var multiplier = result.plate_comb.comb[p_ind];
            
            // new_ulaz.kalk_kom_in_sirovina = 
            
            curr_kalk.procesi[0].ulazi.push( new_ulaz );
            
            
          });
          
          
          
          
        };
        
        
        
        
        
        return;

        
        
        
        
        curr_kalk.procesi[0].ulazi = new_ulazi || [];


        console.log(`--------------- button kalk_mont_plates_btn ----------------`);
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null, `return_html` );
        $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
        setTimeout( function() {
          this_module.write_prirez( null, this_comp_id, product_id, kalk_type, curr_kalk.kalk_sifra );
        }, 200 );
        
      }; // kraj kalk plate yes

      function kalk_platese_no() {
        show_popup_modal(false, popup_text, null );
      };

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', kalk_plates_yes, kalk_platese_no, null);

      
    }); // kraj eventa klik na best plate
    

    
    $('#'+sifra+'_kalk_reserv').data(`cit_run`, async function( state, this_elem ) { 
      
  
        var this_comp_id = $(this_elem).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
        
        var kalk_box = $(this_elem).closest('.kalk_box');
        
        var kalk_type = kalk_box.attr(`data-kalk_type`);
        var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);

        var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
        
        
        var curr_reserv_sifra = curr_kalk.kalk_sifra;
        var curr_reserv_state = curr_kalk.kalk_reserv;
        
      
        var kalk_is_in_prod = false;
      
        // ako bilo koji kalk u offeru ima oznaku da je u produkciji
        $.each( data[kalk_type], function( k_ind, find_kalk ) {
          if ( find_kalk.kalk_for_pro == true )  kalk_is_in_prod = true;
        });
      
      
      
      if ( kalk_is_in_prod ) {
        
        popup_warn(
`Ne možete napraviti mijenjati rezervaciju ZA PONUDU jer te već odredili jednu od kalkulacija za PROIZVODNJU!
<br>
Ako želite stornirati ili rezervirati sirovine za ponudu, morate prvo stornirati i zaustaviti REZERVACIJU za proizvodnju!
`);
          
        // vrati nazad switch KAKO JE BILO !!!
        // vrati nazad switch KAKO JE BILO !!!

        reverse_switch(state, this_elem);
          
        return;
      };
          
      var popup_text = "";
      
      if ( state == true ) {
        popup_text = 
        `Jeste li sigurni da želite pokrenti REZERVACIJU SIROVINA ZA PONUDU?
        <br>
        Ova akcija će resetirati sve druge postojeće REZERVACIJE (za ponudu) ZA OVAJ PROIZVOD.
        `;
      };
      
      if ( state == false ) {
        
        popup_text = 
        `Jeste li sigurni da želite pokrenti STORNO REZERVACIJE?
        <br>
        Ova akcija će resetirati sve REZERVACIJE (za ponudu) ZA OVAJ PROIZVOD.
        `;
      };
      
      async function kalk_reserv_yes() {
        
        if ( state == true ) {
          
          var extra_added = false
          if ( window.kalk_extra_added?.length > 0 ) {
            $.each(window.kalk_extra_added, function(extra_ind, extra_obj) {
              if ( extra_obj.kalk_type == kalk_type && extra_obj.kalk_sifra == kalk_sifra ) extra_added = true;
            });
          };
          
          if ( extra_added == false ) {
            popup_warn(`Potrebno kliknuti na gumb DODAJ EXTRA KOLIČINU prije ove radnje !!!`);
            return;
          };
          
          // ------------------------------ RESERVACIJA TRUE ------------------------------
          
          $.each( data[kalk_type], async function( k_ind, find_kalk ) {
            
            if ( 
              find_kalk.kalk_sifra !== curr_reserv_sifra // ako je u loopu kalk različit od trentno kliknutog switch-a
              &&
              find_kalk.kalk_reserv == true  // i ako je taj kalk reserviran
            ) {
              
              // prebaci switch u poziciju false
              $(`#` + find_kalk.kalk_sifra + `_kalk_reserv`).removeClass(`on`).addClass(`off`);
              data[kalk_type][k_ind].kalk_reserv = false; // pretvori u false 
              // onda storaniraj taj kalk u 
              var storno_result = await this_module.storno_reserv( find_kalk.kalk_sifra, this_elem, null, `offer_reserv` );
              
              // alert("Storno " + find_kalk.kalk_sifra );
            };
            
            
            if ( 
              find_kalk.kalk_sifra == curr_reserv_sifra // ako je u loopu kalk ISTI od trentno kliknutog switch-a tj. ako sam nasao kliknuti kalk
              &&
              !find_kalk.kalk_reserv // i ako taj kalk NIJE RESERVIRAN !!!
            ) {
              
              data[kalk_type][k_ind].kalk_reserv = true; // pretvori u TRUE 
              // onda reserviraj taj kalk
              // popup_warn(`Rezervacija sirovina se upisuje SAMO KADA KLIKNETE NA GUMB PONUDA KUPCU !!!!`);
              
              
              this_module.offer_reserv_record(this_elem);
              
            };
             
          });
          
            
        } 
        else {
          
          
          // ------------------------------ RESERVACIJA FALSE onda loop po svima i  sve stavi da su false ------------------------------
          
          $.each( data[kalk_type], async function( k_ind, find_kalk ) {
            
            // nije bitno jel kalk koji je kliknut ili neki drugi !!!
            // ostavi sve kalkove koji su true na false i storniraj !!!!
            // find_kalk.kalk_sifra !== curr_reserv_sifra // ako je u loopu kalk različit od trentno kliknutog switch-a
            
            if ( find_kalk.kalk_reserv == true ) {
              
              $(`#` + find_kalk.kalk_sifra + `_kalk_reserv`).removeClass(`on`).addClass(`off`);
              
              data[kalk_type][k_ind].kalk_reserv = false; // pretvori u false 
              
              var storno_result = await this_module.storno_reserv( find_kalk.kalk_sifra, this_elem, null, `offer_reserv` );
              
            };
             
          });
  
          
        };
        

        var offer_kalk = this_module.make_kalks( this_elem, data, data.offer_kalk, `offer_kalk`, data.tip || null );
        $(`#${data.id} .offer_kalk_container`).html(offer_kalk);
        setTimeout( function() { $(`#${data.id} .save_offer_kalk_btn`).trigger(`click`); }, 50 );
        
        
      }; // kraj yes funkcije

      function kalk_reserv_no() {
        
        reverse_switch(state, this_elem);
        show_popup_modal(false, popup_text, null );
        
      };

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', kalk_reserv_yes, kalk_reserv_no, null);
    
    });
    
    
    $('#'+sifra+'_kalk_sample').data(`cit_run`,  async function( state, this_elem ) { 


      var this_comp_id = $(this_elem).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var kalk_box = $(this_elem).closest('.kalk_box');

      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);

      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );


      if ( state == false && curr_kalk.kalk_sample ) {

        popup_warn(
    `Ako želite ODUSTATI OD UZORKA potrebno je to definirati u kartici proizvoda sa šifrom uzorka ${ curr_kalk.sample_sifra }!!!`);

        // vrati nazad switch na TRUE !!!
        // vrati nazad switch na TRUE !!!
        setTimeout( function() { 
          $(this_elem).removeClass(`off`).addClass(`on`);
        }, 300 );
        return;

      };



      // ------------------------ SAMO AKO PRAVI UZORAK I VEĆ GA NIJE NAPRAVIO ------------------------
      if ( state == true && !curr_kalk.kalk_sample ) {

        var popup_text = `Jeste li sigurni da želite pokrenuti kreiranje novog uzorka!!!`;
        
        async function kalk_sample_yes() {

          var sample_result = await get_new_counter(`sample`, false);
          curr_kalk.sample_sifra = `UZ` + sample_result.number + `.`;
          curr_kalk.kalk_sample = true;

          // ------------------------ SPREMI OFFER KALK ------------------------
          // $(`#${data.id} .save_offer_kalk_btn`).trigger(`click`);

          // spremi ovu trenutnu offer kalk
          setTimeout( function() { this_module.create_sample_product(this_elem) }, 200 );

        }; // kraj yes funkcije

        function kalk_sample_no() {
          reverse_switch(state, this_elem);
          show_popup_modal(false, popup_text, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, popup_text, null, 'yes/no', kalk_sample_yes, kalk_sample_no, null);

      };

    });


    $('#'+sifra+'_kalk_for_pro').data(`cit_run`,  async function( state, this_elem ) { 


      var this_comp_id = $(this_elem).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var kalk_box = $(this_elem).closest('.kalk_box');

      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);

      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );


      var curr_for_pro_sifra = curr_kalk.kalk_sifra;

      
      var product_id = $(this_elem).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      

      if ( state == true && curr_kalk.kalk_reserv == false ) {

        popup_warn(
`Ne možete izabrati kalkulaciju za NK ako niste tu kalkulaciju reservirali za PK!!!!
<br>
Prvo označite REZERV. PK i zatim označite ZA REZERV. NK !!!
`);

        // vrati nazad switch na FALSE !!!
        // vrati nazad switch na FALSE !!!

        setTimeout( function() { 
          $(this_elem).removeClass(`on`).addClass(`off`);
        }, 300 );
        
        return;
      };

      var popup_text = "";

      if ( state == true ) popup_text = `Jeste li sigurni da želite OZNAČITI ovu kalkulaciju za NK?<br>To će stornirati prijašnju rezervaciju po PONUDI`;

      if ( state == false ) popup_text = `Jeste li sigurni da želite IZBACITI ovu kalkulaciju iz NK?<br>To će stornirati trenutnu rezervaciju po NK`;


      async function kalk_for_pro_yes() {

        if ( state == true  && !curr_kalk.kalk_for_pro ) {
          
          
          var extra_added = false
          if ( window.kalk_extra_added?.length > 0 ) {
            $.each(window.kalk_extra_added, function(extra_ind, extra_obj) {
              if ( extra_obj.kalk_type == kalk_type && extra_obj.kalk_sifra == kalk_sifra ) extra_added = true;
            });
          };
          
          if ( extra_added == false ) {
            popup_warn(`Potrebno kliknuti na gumb DODAJ EXTRA KOLIČINU prije ove radnje !!!`);
            return;
          };
          
          
          // ------------------------------ ZA PROIZVODNJU TRUE ------------------------------
          // ako current kalk DO SADA NIJE BIO ZA PROIZVODNJU
       

          curr_kalk.kalk_for_pro = true; // pretvori u TRUE 
          // onda reserviraj taj kalk
          // popup_warn(`Rezervacija sirovina za NK se upisuje SAMO KADA KLIKNETE NA GUMB NARUDŽBA KUPCA!!!`);

          var storno_result = await this_module.storno_reserv( curr_kalk.kalk_sifra, this_elem, null, `offer_reserv` );

          /*
          var offer_kalk = this_module.make_kalks( this_elem, data, data.offer_kalk, `offer_kalk`, data.tip || null );
          $(`#${data.id} .offer_kalk_container`).html(offer_kalk);
          setTimeout( function() { $(`#${data.id} .save_offer_kalk_btn`).trigger(`click`); }, 50 );
          */

          var this_kalk_title = create_kalk_naslov(data, curr_kalk);
          $(`#` + product_data.id + ` .product_for_pro_title` ).html(this_kalk_title);


          var kalk_copy = cit_deep(curr_kalk);
          kalk_copy.kalk_sifra = `kalk`+cit_rand();
          if ( kalk_copy.kalk_rand ) delete kalk_copy.kalk_rand;
          data.pro_kalk = [ kalk_copy ];

          var saved_records = await this_module.order_reserv_record(this_elem);
          
          var pro_kalk = this_module.make_kalks( this_elem, data, data.pro_kalk, `pro_kalk`, data.tip || null );
          $(`#${data.id} .pro_kalk_container`).html(pro_kalk);
          
            
        } 
        else if ( state == false && curr_kalk.kalk_for_pro == true ) {

          // ------------------------------ ZA PROIZVODNJU FALSE ------------------------------
          // i ako je postojeći kalk bio do sada TRUE ZA PRODUKCIJU
          
          curr_kalk.kalk_for_pro = false; // pretvori u false 
          var storno_result = await this_module.storno_reserv( curr_kalk.kalk_sifra, this_elem, null, `order_reserv` );
          
          
        }; // kraj od od else tj ako je state FALSE
        
        
        
        var offer_kalk = this_module.make_kalks( this_elem, data, data.offer_kalk, `offer_kalk`, data.tip || null );
        $(`#${data.id} .offer_kalk_container`).html(offer_kalk);
        
        setTimeout( function() { $(`#${data.id} .save_offer_kalk_btn`).trigger(`click`); }, 400 );
        setTimeout( function() { $(`#${data.id} .save_pro_kalk_btn`).trigger(`click`); }, 500 );
        
        

      }; // kraj yes funkcije

      function kalk_for_pro_no() {


        reverse_switch(state, this_elem);
        
        show_popup_modal(false, popup_text, null );

      };

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', kalk_for_pro_yes, kalk_for_pro_no, null);

    });
    
    
    $('#'+sifra+'_kalk_radni_nalog').data(`cit_run`, async function( state, this_elem ) { 
      

      var this_comp_id = $(this_elem).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var kalk_box = $(this_elem).closest('.kalk_box');

      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);

      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );


      var curr_reserv_sifra = curr_kalk.kalk_sifra;
      var curr_reserv_state = curr_kalk.kalk_reserv;



      var popup_text = "";

      if ( state == true ) { popup_text = `Jeste li sigurni da želite NAPRAVITI RADNE NALOGE?`; };
      
      if ( state == false ) { popup_text = `Jeste li sigurni da želite STORNIRATI RADNE NALOGE?`; };

      async function radni_nalog_yes() {
        
        if ( state == true ) {
          
          this_module.make_radni_nalog(this_elem);
          
        } 
        else {
          
          // ------------------------------ RADNI NALOG FALSE ------------------------------
          
          curr_kalk.kalk_radni_nalog = false;
          
          // bilo koji element koji je za ovu kalkulaciju 
          // bilo koji element koji je za ovu kalkulaciju 
          // bilo koji element koji je za ovu kalkulaciju  ---- NIJE BITNO KOJI
          var post_koment_element = $(`#` + data.post_kalk[0].kalk_sifra + `_kalk_komentar`)[0];
          
          // storniram reservacije za radne SVE naloge !!!
          var storno_result = await this_module.storno_reserv( data.post_kalk[0].kalk_sifra, post_koment_element, null, `radni_nalog` );

          // sukcesivno postavi sve statuse od radnih naloga da rn_droped bude objekt od usera i vrijeme kada je rn droped napravljen
          $.each(data.post_kalk[0], function(p_ind, proces) {
            
            setTimeout( async function () {
              // ako je već droped onda ne trebaš tj preskoči kreiranje rn droped ako je već napravljen
              if ( !proces.extra_data?.proces_drop ) {
                var rn_updated_statuses = await this_module.ajax_proces_rn_update_statuses( data._id, proces.row_sifra, `rn_droped` );
              };
              
            }, p_ind*200);
            
          });
          
          
        };
        
      }; // kraj yes funkcije

      function radni_nalog_no() {
        
        
        reverse_switch(state, this_elem);
        
        show_popup_modal(false, popup_text, null );
        
      };

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', radni_nalog_yes, radni_nalog_no, null);
    
    });
    
    
    $('#'+sifra+'_allow_mont_visak').data(`cit_run`, function(state, this_elem) { 
      
      
      var this_comp_id = $(this_elem).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var kalk_box = $(this_elem).closest('.kalk_box');
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      curr_kalk.allow_mont_visak = state;
      
      console.log(curr_kalk);
    
    });
    
    
    $('#'+sifra+'_kalk_polov').data(`cit_run`, function(state, this_elem) { 
      
      
      var this_comp_id = $(this_elem).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var kalk_box = $(this_elem).closest('.kalk_box');
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      curr_kalk.kalk_polov = state;
      
      console.log(curr_kalk.kalk_polov);
    
    });
    
    
    /*
    $(`.kalk_box`).each( function() {
      
      var kalk_box = $(this);
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      if ( window.registered_grab_scroll_array[this_comp_id] ) {
        return;
      } else {
        window.registered_grab_scroll_array[this_comp_id] = true;
      };
      
      console.log(  this_comp_id  +  `----------------------------------------------- TRAŽIO JE RAND ID`);
      
      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      
      if ( curr_kalk ) {
        kalk_box[0].scrollLeft = curr_kalk.kalk_scroll || 0;
        // if ( window.scroll_elems_ids.indexOf(this.id) == -1 ) {
        register_grab_to_scroll( this.id, `kalk_scroll`);
      };
      
      // window.scroll_elems_ids.push(this.id);
      
    });
    
    */
    
    
    
  }; 
// ------------------END------------------------------ register new kalk events
  

  





