

VELPROCES.glue_polica_work = function glue_polica_R_work(
this_module, naklada, arg_ulazi,  arg_izlazi,  arg_action,  arg_radna_stn,  product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index )
{
    
    /*
    
    {
      "sifra": "RAD255", "naziv": "DORADA STALAK - ljepljenje polica s duplim ojačanjem",  "func": "glue_polica_R_work",
      "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
      "elem_sifra": "SEL1005", "elem_naziv": "Zaljepljena polica s duplim ojačanjem",
      "unit": "kom", "interval": "h", "speed": 50, "price": 150,
    },
    
    */
    
    var radna_stn = arg_radna_stn || find_item(window.cit_local_list.radne_stanice, `RAD255`, `sifra`);
    
    var glue_polica_prep_time = find_item(window.cit_local_list.pro_const, `PCONSGLUEPREPTIME`, `sifra`).value; // u satima
    var glue_polica_prep_price = find_item(window.cit_local_list.pro_const, `PCONS4GLUEPREPPRICE`, `sifra`).value; // u 150 kuna
    
    
    var sum_polica = 0;
    var sum_polica_R1 = 0;
    var sum_polica_R2 = 0;

    
    var sum_work_time = 0;

    var missing_polica = false;
    
    
    if ( arg_ulazi.length < 3 )  {
      popup_warn(`Za ovaj proces potrebno je dodati minimalno 3 ulaza !!!`);
      return;
    };
    
    $.each( arg_ulazi, function( u_ind, ulaz ) {
      // SEL6 ZNAČI BAZA POLICE
      if ( ulaz.kalk_element?.elem_tip?.sifra == `SEL6` ) {
        sum_polica += ulaz.kalk_sum_sirovina;
      };
      
      // SEL7 ZNAČI PRVO OJAČANJE POLICE
      if ( ulaz.kalk_element?.elem_tip?.sifra == `SEL7` ) {
        sum_polica_R1 += ulaz.kalk_sum_sirovina;
      };
      
      // SEL8 ZNAČI DRUGO OJAČANJE POLICE
      if ( ulaz.kalk_element?.elem_tip?.sifra == `SEL8` ) {
        sum_polica_R2 += ulaz.kalk_sum_sirovina;
      };
      
      
    })
    
    if ( sum_polica == 0 || sum_polica_R1 == 0 || sum_polica_R2 == 0 )  {
      popup_warn(`Za ovaj proces je potrebno dodati bazu police i DVA ojačenja za policu`);
      return;
    };
    

    var prep_time = glue_polica_prep_time * 60;

    var unit_time = 1/radna_stn.speed * 60;
    var time = sum_polica/radna_stn.speed * 60;
    
    var unit_price = (glue_polica_prep_time/sum_polica * glue_polica_prep_price) + ( 1/radna_stn.speed * radna_stn.price );
    var price = (glue_polica_prep_time * glue_polica_prep_price) + ( sum_polica/radna_stn.speed * radna_stn.price );

    
    var ulazi = arg_ulazi || this_module.gen_ulazi_from_izlazi(arg_ulazi);
    var ukupno_dobiveno_iz_ulaza = sum_polica;
    
    var arg_changed_props = null;
    var izlazi = this_module.gen_izlazi( null, arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, radna_stn, ukupno_dobiveno_iz_ulaza, null, null );
    
    var change_action_props = null;
    var action = arg_action || this_module.gen_action( change_action_props, curr_kalk, prep_time, time, unit_price, price, radna_stn );

    var proces = {
      extra_data: arg_extra_data || {},
      proc_koment: arg_proc_koment || "",
      row_sifra: `procesrow`+cit_rand(),
      ulazi: ulazi,
      action: action,
      izlazi: izlazi, 
    }; // kraj procesa

    return {
      prep_time,
      unit_time,
      time,
      unit_price,
      price,
      proces
    };

  };

VELPROCES.glue_footer_work = function glue_footer_work( 
this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index ) 
{
    
    /*
    
      {
        "sifra": "RAD255FOOTER", "naziv": "DORADA STALAK - ljepljenje footera",  "func": "glue_footer_work",
        "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
        "elem_sifra": "SEL1005FOOTER", "elem_naziv": "Zaljepljen footer",
        "unit": "kom", "interval": "h", "speed": 70, "price": 150,
      },
    
    */
    
    var radna_stn = arg_radna_stn || find_item(window.cit_local_list.radne_stanice, `RAD255FOOTER`, `sifra`);
        
    var glue_footer_prep_time = find_item(window.cit_local_list.pro_const, `PCONSGLUEPREPTIME`, `sifra`).value;
    var glue_footer_prep_price = find_item(window.cit_local_list.pro_const, `PCONS4GLUEPREPPRICE`, `sifra`).value;
    
    var sum_footer = 0;
    var sum_polica_R1 = 0;
    
    var sum_work_time = 0;

    var missing_polica = false;
    
    
    if ( arg_ulazi.length < 2 )  {
      popup_warn(`Za ovaj proces potrebno je dodati minimalno 2 ulaza !!!`);
      return;
    };
    
    $.each( arg_ulazi, function( u_ind, ulaz ) {
      // SEL10 ZNAČI FOOTER
      if ( ulaz.kalk_element?.elem_tip?.sifra == `SEL10` ) {
        sum_footer += ulaz.kalk_sum_sirovina;
      };
      
      // SEL7 ZNAČI PRVO OJAČANJE POLICE
      if ( ulaz.kalk_element?.elem_tip?.sifra == `SEL7` ) {
        sum_polica_R1 += ulaz.kalk_sum_sirovina;
      };
      
    })
    
    if ( sum_footer == 0 || sum_polica_R1 == 0 )  {
      popup_warn(`Za ovaj proces je potrebno dodati footer i ojačanje police`);
      return;
    };
    

    var prep_time = glue_footer_prep_time * 60;

    var unit_time = 1/radna_stn.speed * 60;
    var time = sum_footer/radna_stn.speed * 60;
    
    var unit_price = (glue_footer_prep_time/sum_footer * glue_footer_prep_price) + ( 1/radna_stn.speed * radna_stn.price );
    var price = (glue_footer_prep_time * glue_footer_prep_price) + ( sum_footer/radna_stn.speed * radna_stn.price );

    
    var ulazi = arg_ulazi || this_module.gen_ulazi_from_izlazi(arg_ulazi);
    var ukupno_dobiveno_iz_ulaza = sum_footer;
    
    var arg_changed_props = null;
    var izlazi = this_module.gen_izlazi( null, arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, radna_stn, ukupno_dobiveno_iz_ulaza, null, null );
    
    var change_action_props = null;
    var action = arg_action || this_module.gen_action( change_action_props, curr_kalk, prep_time, time, unit_price, price, radna_stn );

    var proces = {
      extra_data: arg_extra_data || {},
      proc_koment: arg_proc_koment || "",
      row_sifra: `procesrow`+cit_rand(),
      ulazi: ulazi,
      action: action,
      izlazi: izlazi, 
    }; // kraj procesa

    return {
      prep_time,
      unit_time,
      time,
      unit_price,
      price,
      proces
    };
    

  };
  
VELPROCES.glue_polica_back_work = function glue_polica_back_work( 
this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index )
{
    
    /*
    
{
  "sifra": "RAD253POLICE", "naziv": "DORADA STALAK - ljepljenje polica na leđa",  "func": "glue_polica_back_work",
  "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
  "elem_sifra": "SEL1003POLICE", "elem_naziv": "Zaljepljene police na leđima",
  "unit": "kom", "interval": "h", "speed": 100, "price": 150,
},
    
    */
    
    var radna_stn = arg_radna_stn || find_item(window.cit_local_list.radne_stanice, `RAD253POLICEBACK`, `sifra`);
        
    var glue_polica_back_prep_time = find_item(window.cit_local_list.pro_const, `PCONSGLUEPREPTIME`, `sifra`).value;
    var glue_polica_back_prep_price = find_item(window.cit_local_list.pro_const, `PCONS4GLUEPREPPRICE`, `sifra`).value;
    
    var sum_back = 0;
    var sum_police = 0;
    
    var sum_work_time = 0;
    
    if ( arg_ulazi.length < 2 )  {
      popup_warn(`Za ovaj proces potrebno je dodati minimalno 2 ulaza !!!`);
      return;
    };
    
    $.each( arg_ulazi, function( u_ind, ulaz ) {
      // SEL1 ZNAČI LEĐA
      if ( ulaz.kalk_element?.elem_tip?.sifra == `SEL1` ) {
        sum_back += ulaz.kalk_sum_sirovina;
      };
      
      // ako u opisu ima ljepljenje polic s jednim ili duplim ojačanjem
      if ( 
        ulaz.kalk_element?.elem_opis.indexOf(`ljepljenje polica s jednim`) > -1 
        ||
        ulaz.kalk_element?.elem_opis.indexOf(`ljepljenje polica s duplim`) > -1 
      ) {
        sum_police += ulaz.kalk_sum_sirovina;
      };
      
      
    });
    
    
    if ( sum_back == 0 || sum_police == 0 )  {
      popup_warn(`Za ovaj proces je potrebno dodati leđa stalka i već pripremljene police !!!`);
      return;
    };
    
    
    var prep_time = glue_polica_back_prep_time * 60;

    var unit_time = 1/radna_stn.speed * 60;
    var time = sum_police/radna_stn.speed * 60;
    
    var unit_price = (glue_polica_back_prep_time/sum_police * glue_polica_back_prep_price) + ( 1/radna_stn.speed * radna_stn.price );
    var price = (glue_polica_back_prep_time * glue_polica_back_prep_price) + ( sum_police/radna_stn.speed * radna_stn.price );

    
    var ulazi = arg_ulazi || this_module.gen_ulazi_from_izlazi(arg_ulazi);
    var ukupno_dobiveno_iz_ulaza = sum_back;
    
    var arg_changed_props = null;
    var izlazi = this_module.gen_izlazi( null, arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, radna_stn, ukupno_dobiveno_iz_ulaza, null, null );
    
    var change_action_props = null;
    var action = arg_action || this_module.gen_action( change_action_props, curr_kalk, prep_time, time, unit_price, price, radna_stn );

    var proces = {
      extra_data: arg_extra_data || {},
      proc_koment: arg_proc_koment || "",
      row_sifra: `procesrow`+cit_rand(),
      ulazi: ulazi,
      action: action,
      izlazi: izlazi, 
    }; // kraj procesa

    return {
      prep_time,
      unit_time,
      time,
      unit_price,
      price,
      proces
    };
    

  };
