var module_object = {
create: function( data, parent_element, placement ) {
  
  var this_module = window[module_url]; 
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };
  
  var valid = {
    
    kalk_komentar: { element: "text_area", type: "string", lock: false, visible: true, label: "Komentar kalkulacije" },
    
    doc_adresa: { element:"single_select", type:"same_width", lock:false, visible:true, label:"Adresa primatelja" },
    
    dostav_adresa: { element:"single_select", type:"same_width", lock:false, visible:true, label:"Adresa dostave" },
    
    doc_rok_time:  { element: "simple_calendar", type: "date", lock: false, visible: true, "label": "Rok dokumenta" },
    
    kalk_polov: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Polovanjka" },
    
    kalk_naklada:  { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Naklada"},
    
    ulaz_noz_big: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Metara nož/big"},
    ulaz_color_cover: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "POKR. BOJOM"},
    
    ulaz_mat: { element:"single_select", type:"simple", lock:true, visible:true, label:"Materijal ulaza" },
    
    
    kalk_color_C: { element: "input", type: "number", decimals: 0, lock: true, visible: true, label: "CYAN"},
    kalk_color_M: { element: "input", type: "number", decimals: 0, lock: true, visible: true, label: "MAGENTA"},
    kalk_color_Y: { element: "input", type: "number", decimals: 0, lock: true, visible: true, label: "YELLOW"},
    kalk_color_K: { element: "input", type: "number", decimals: 0, lock: true, visible: true, label: "KEY"},
    kalk_color_W: { element: "input", type: "number", decimals: 0, lock: true, visible: true, label: "WHITE"},
    kalk_color_V: { element: "input", type: "number", decimals: 0, lock: true, visible: true, label: "VARNISH"},
    
    
    kalk_nest_count: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "GNJEZDA"},
    
    
    ulaz_val: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "ULAZ VAL"},
    ulaz_kontra: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "ULAZ KONTRA"},
    
    izlaz_val: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "IZLAZ VAL"},
    izlaz_kontra: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "IZLAZ KONTRA"},
    
    new_offer_naklada:  { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Nova naklada"},
      
    kalk_reserv: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Reserv. PK" },
    kalk_for_pro: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Reserv. NK" },
    kalk_sample: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Napravi uzorak" },
    
    kalk_glue_shelf: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Ulijepi police na leđa" },
    
    kalk_radni_nalog: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Radni nalozi za sve procese !!!" },
    proces_radni_nalog: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Dopuna RN !" },
    proces_drop: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "ODUSTANI !!!" },
    alat_apart: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "ALAT ODVOJENO" },
    
    kalk_discount:  { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Rabat / %"},    
    kalk_markup:  { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Marža / %"},
    
    kalk_discount_money:  { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Rabat u KN"},    
    kalk_markup_money:  { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Marža u KN"},
    
    kalk_final_unit_time:  { element: "input", type: "number", decimals: 2, lock: true, visible: true, label: "Vrijeme / Kom"}, 
    kalk_final_time:  { element: "input", type: "number", decimals: 2, lock: true, visible: true, label: "Uk. Vrijeme"}, 
    
    kalk_final_unit_price:  { element: "input", type: "number", decimals: 2, lock: true, visible: true, label: "Cijena Kom"}, 
    kalk_final_sum_price:  { element: "input", type: "number", decimals: 2, lock: true, visible: true, label: "Uk. cijena"}, 
    
    kalk_final_unit_price_dis_mark:  { element: "input", type: "number", decimals: 2, lock: true, visible: true, label: "Final / Kom"}, 
    kalk_final_sum_price_dis_mark:  { element: "input", type: "number", decimals: 2, lock: true, visible: true, label: "Uk. final"}, 
    
    kalk_final_alat_price:  { element: "input", type: "number", decimals: 2, lock: true, visible: true, label: "Uk. cijena alata"}, 
    
    
    broj_klapni: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Broj Klapni"}, 
    prirez_val:  { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Prirez Val"},  
    
    for_proces: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Za proces"},  
    
    prirez_kontra:  { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Prirez Kontra val"},  


    kalk_find_sirov: { element: "single_select", type: "", lock: false, visible: true, label: "Izaberi sirovinu" },
    kalk_find_plate: { element: "single_select", type: "", lock: false, visible: true, label: "Izaberi formu" },
    
    
    ulaz_price: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Cijena ulaza"},
    ulaz_unit_price: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Jedinična cijena"},
    
    ulaz_element: { element: "single_select", type: "simple", lock: false, visible: true, label: "Element" },
    
    izlaz_element: { element: "single_select", type: "simple", lock: true, visible: true, label: "Element" },

    ulaz_kom_in_product: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Komada u proizvodu"},
    
    ulaz_kom_in_sirovina: { element: "input", type: "number", decimals: 10, lock: false, visible: true, label: "Jed. Količina u ulazu"},
    
    ulaz_sum_sirovina: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Komada Sirovine"},
    ulaz_extra_kom: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "EXTRA KOM Sirovine"},
    
    
    action_radna_stn:  { element: "single_select", type: "simple", lock: false, visible: true, label: "Izaberi radnu stanicu" },
    
    action_prep_time: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Priprema / min"},
    action_work_time: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Rad / min"},
    
    satnica_stroj: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Satnica stroj"},
    satnica_prep: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Satnica pripreme"},
    satnica_radnik: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Satnica radnik"},
    radnik_count: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Broj radnika"},
    radnik_count_prep: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Broj radnika u pripremi"},
    

    action_multiload: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Multiload"},
    action_kasir_num: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Broj kaširanja"},
    
    kalk_glue_mm: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Dužina ljep."},
    kalk_glue_points: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Točaka ljep."},
    kalk_glue_speed: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Brzina ljep. mm/s"},
    kalk_glue_tlak: { element: "single_select", type: "same_width", lock: false, visible: true, label: "Tlak ljep." },
    kalk_glue_dizna: { element: "single_select", type: "same_width", lock: false, visible: true, label: "Dizna ljep." },
    kalk_glue_m2: { element: "single_select", type: "same_width", lock: false, visible: true, label: "Max elem." },
    
    
    action_unit_time: { element: "input", type: "number", decimals: 2, lock: true, visible: true, label: "Jedinično vrijeme / min"},
    action_unit_price: { element: "input", type: "number", decimals: 2, lock: true, visible: true, label: "Cijena / kom"},
    action_est_price: { element: "input", type: "number", decimals: 2, lock: true, visible: true, label: "Cijena"},
    
    izlaz_kom_in_product: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Komada u proizvodu"},
    izlaz_kom_on_plate: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Komada na ploči"},
    izlaz_name:  { element: "input", type: "string", lock: true, visible: true, label: "Izlaz ime" },
    izlaz_sum_sirovina: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Komada"},

    
    add_radna_stanica: { element: "single_select", type: "", lock: false, visible: true, label: "Dodaj novu radnu stanicu" },
    
    add_fixed_price_contract: { element: "single_select", type: "", lock: false, visible: true, label: "Dodaj ugovor ili tender za FIKSNU CIJENU" },
    add_mark_or_disc_contract: { element: "single_select", type: "", lock: false, visible: true, label: "Ugovor za POPUST ILI MARŽU" },
    

    kalk_fixed_price: { element:"check_box", type:"bool", lock:true, visible:true, label:"FIX. CIJENA" },
    show_in_offer: { element:"check_box", type:"bool", lock:false, visible:true, label:"U DOC." },
    
    kalk_fixed_mark: { element:"check_box", type:"bool", lock:false, visible:true, label:"FIX. MARŽA" },
    kalk_fixed_discount: { element:"check_box", type:"bool", lock:false, visible:true, label:"FIX. RABAT" },
    
    // { element: "input", type: "string", lock: false, visible: true, label: "Kvaliteta/slojevi" },
    kalk_ploca_mat: { element: "single_select", type: "simple", lock: false, visible: true, label: "Kvaliteta/slojevi" },
    
    // new_offer_mat: { element: "input", type: "string", lock: false, visible: true, label: "Kvaliteta/slojevi" },
    
    new_offer_mat: { element: "single_select", type: "same_width", lock: false, visible: true, label: "Kvaliteta/slojevi" },
    
    allow_mont_visak: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Montažne ploče sa viškom ?" },
    
    proc_koment: { element: "text_area", type: "string", lock: false, visible: true, label: "Komentar procesa" },
    
    
    doc_lang: { element:"single_select", type:"simple", lock:false, visible:true, label:"Jezik dokumenta" },
    doc_valuta: { element:"single_select", type:"simple", lock:false, visible:true, label: "Valuta" },
    doc_valuta_manual: { element:"input", type:"number", decimals: 3, lock:false, visible:true, label: "Prepiši Tečaj" },
  
    

  };
  

  // TODO ovo je samo privremeno radim override -------> kasnije moram obrisati ovu liniju !!!!!
  // TODO ovo je samo privremeno radim override -------> kasnije moram obrisati ovu liniju !!!!!
  // TODO ovo je samo privremeno radim override -------> kasnije moram obrisati ovu liniju !!!!!
  
  // if ( data.kalk ) delete data.kalk;
  // data = { ...data, ...dummy_kalk() };
  
  // TODO ovo je samo privremeno radim override -------> kasnije moram obrisati ovu liniju !!!!!
  // TODO ovo je samo privremeno radim override -------> kasnije moram obrisati ovu liniju !!!!!
  // TODO ovo je samo privremeno radim override -------> kasnije moram obrisati ovu liniju !!!!!
  
  // data = cit_deep(data);
  
  // if ( !data.item_sifra ) data = { ...data, ...dummy_kalk() };
  
  var criteria = [
    'sifra',
    'rb'
  ];
  
  
  // multisort(data.elements, criteria);

  
  this_module.set_init_data(data);
  
  var { id } = data;
  this_module.valid = valid;  
  var rand_id = `kalkmod`+cit_rand();
  console.log(  id   +  `  ------------- DOBIO JE RAND ID -------`);
  
  this_module.cit_data[id].rand_id = rand_id;
  
  // window.registered_grab_scroll_array[id] = null;
  
  
  
  data.kalk_events_registered = false;
  
  
  var component_html =

`

  <section  id="${id}" class="cit_comp kalk_comp" >

    <div class="container-fluid" style="margin-bottom: 5px;" >
      
      <div class="row" style="margin-top: 5px; margin-bottom: 5px;">

        <div class="col-md-2 col-sm-12 meta_offer_kalk_save_box">
          <b>SPREMLJENO:</b> ${data.meta_offer_kalk?.user_saved?.full_name || "" }
          &nbsp;&nbsp;
          ${ data.meta_offer_kalk?.saved ? cit_dt(data.meta_offer_kalk.saved).date_time : "" }
        </div>

        <div class="col-md-2 col-sm-12 meta_offer_kalk_edit_box">
          <b>EDITIRANO:</b> ${data.meta_offer_kalk?.user_edited?.full_name || "" }
          &nbsp;&nbsp;
          ${ data.meta_offer_kalk?.edited ? cit_dt(data.meta_offer_kalk.edited).date_time : "" }
        </div>

        
        <div class="col-md-2 col-sm-12 new_offer_container">
          ${ cit_comp(`${data.id}_new_offer_naklada`, valid.new_offer_naklada, "" ) }
        </div>
        
        <div class="col-md-2 col-sm-12 new_offer_container">
          ${ cit_comp(`${data.id}_new_offer_mat`, valid.new_offer_mat, null, "" ) }
        </div>
        
        
        <!-- 
        <div class="col-md-1 col-sm-12">
          <button class="cit_tooltip blue_btn btn_small_text new_offer_kalk_btn" 
                  data-toggle="tooltip" data-placement="top" data-html="true" title="Napravi novu kalkulaciju po template-u"
                  style="margin: 20px 0 0 auto; box-shadow: none; padding: 0 10px;">
            
            <i class="fas fa-calculator" style="font-size: 18px; margin-right: 10px;"></i>
            TEMPLATE KALKULACIJA
          </button>        
        </div>
        -->
        
        <div class="col-md-1 col-sm-12">
          <button class="cit_tooltip blue_btn btn_small_text empty_offer_kalk_btn" 
                  data-toggle="tooltip" data-placement="top" data-html="true" title="Napravi PRAZNU kalkulaciju bez template-a"
                  style="margin: 20px 0 0 auto; box-shadow: none; padding: 0 10px;">
            
            <i class="far fa-sticky-note" style="font-size: 18px; margin-right: 10px;"></i>
            PRAZNA KALKULACIJA
          </button>        
        </div>
        
        <div class="col-md-1 col-sm-12">
          &nbsp;
        </div>
        
        <div class="col-md-2 col-sm-12">
          <button class="blue_btn btn_small_text save_offer_kalk_btn save_kalk_btn" style="margin: 20px 10px 0 auto; box-shadow: none; width: 180px;">
            SPREMI KALK. PONUDA
          </button>
        </div>
        
      </div> 

      <div class="row">
        
      </div>
      
    </div> <!-- KRAJ CONTAINER FLUID -->
    
    
    <div class="offer_kalk_container" style="background: #d6eaf7" >
      
      ${ this_module.make_kalks( null, data, data.offer_kalk, `offer_kalk`, data.tip || null, `return_html` ) }  
      
    </div>
    
    

    <div class="container-fluid" style="margin-top: 10px;" >
     
      <div class="row" style="margin-bottom: 10px;" >

      <div class="col-md-2 col-sm-12 meta_pro_kalk_save_box">
        <b>SPREMLJENO:</b> ${data.meta_pro_kalk?.user_saved?.full_name || "" }
        &nbsp;&nbsp;
        ${ data.meta_pro_kalk?.saved ? cit_dt(data.meta_pro_kalk.saved).date_time : "" }
      </div>
      <div class="col-md-2 col-sm-12 meta_pro_kalk_edit_box">
        <b>EDITIRANO:</b> ${data.meta_pro_kalk?.user_edited?.full_name || "" }
        &nbsp;&nbsp;
        ${ data.meta_pro_kalk?.edited ? cit_dt(data.meta_pro_kalk.edited).date_time : "" }
      </div>
      
      <div class="col-md-6 col-sm-12">
        &nbsp;
      </div>
      
      <div class="col-md-2 col-sm-12" style="pointer-events: all;" >
        <button class="blue_btn btn_small_text save_pro_kalk_btn save_kalk_btn" style="margin: 0 10px 0 auto; box-shadow: none; width: 180px;">
          SPREMI KALK. PROIZ.
        </button>
      </div>
      

      </div> 
      
    </div>
    
    <div class="pro_kalk_container" style="background: #e2ede5" >
      ${ this_module.make_kalks( null, data, data.pro_kalk, `pro_kalk`, data.tip || null, `return_html` ) }   
    </div>
   
    
    <div class="container-fluid" style="margin-top: 10px;" >
     
      <div class="row" style="margin-bottom: 10px;" >

        <div class="col-md-2 col-sm-12 meta_post_kalk_save_box">
          <b>SPREMLJENO:</b> ${data.meta_post_kalk?.user_saved?.full_name || "" }
          &nbsp;&nbsp;
          ${ data.meta_post_kalk?.saved ? cit_dt(data.meta_post_kalk.saved).date_time : "" }
        </div>
        <div class="col-md-2 col-sm-12 meta_post_kalk_edit_box">
          <b>EDITIRANO:</b> ${data.meta_post_kalk?.user_edited?.full_name || "" }
          &nbsp;&nbsp;
          ${ data.meta_post_kalk?.edited ? cit_dt(data.meta_post_kalk.edited).date_time : "" }
        </div>
        
        <div class="col-md-6 col-sm-12">
          &nbsp;
        </div>
        
        
        <div class="col-md-2 col-sm-12">
          <button class="blue_btn btn_small_text save_post_kalk_btn save_kalk_btn" style="margin: 0 10px 0 auto; box-shadow: none; width: 180px;">
            SPREMI POST KALK.
          </button>
        </div>

      </div> 
    
    </div>
      
    <div class="post_kalk_container" style="background: #f3e6e9" >
      ${ this_module.make_kalks( null, data, data.post_kalk, `post_kalk`, data.tip || null, `return_html` ) } 
    </div>


</section>

`;

  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };
  
  
  wait_for( `$('#${data.id}').length > 0`, async function() {
    
    console.log(data.id + 'component injected into html');
    $('.cit_tooltip').tooltip();
   
    
    // ako podaci od kalkulacije odgovarajugoto podacima unutar URL-a !!!!
    if ( 
      data.goto_item == data.item_sifra 
      &&
      data.goto_variant == data.variant 
    ) {

      // ALI POSTOJI GOTO KALK ALI NEMA GOTO PROCES tj == null
      if ( data.goto_kalk && !data.goto_proces ) {

        var kalk_row = null;
        var product_comp = null;
        var kalk_tab = null;


        // ovaj loop je kada tražim kalkulaciju  !!!!
        $(`.kalk_box`).each( function() {
          if ( this.id.indexOf( data.goto_kalk ) > -1 ) {
            kalk_row = $(this);
            product_comp = kalk_row.closest(`.product_comp`);
            
            // kalk_tab = product_comp.find(`.cit_tab_btn`).eq(3);
            
            product_comp.find(`.cit_tab_btn`).each( function() {
              if ( this.id.indexOf(`cit_tab_btn_kalk`) > -1 ) kalk_tab = $(this);
            });
            
            
          };
        });



        if ( kalk_row ) {

          // otvori tab za kalkulacije !!!!
          kalk_tab.trigger(`click`);

          // ako je kalkulacija koja je goto kalk colapsana onda je moram otvoriti
          if ( $(`#`+data.goto_kalk+`_kalkulacija`).hasClass(`cit_closed`) ) {
            $(`#`+data.goto_kalk+`_kalkulacija .kalk_arrow`).trigger(`click`);
          };
          
          setTimeout( function() {
            var scroll_top = kalk_row.offset().top;
            $(`html`)[0].scrollTop = scroll_top - 200;
          }, 500);

          setTimeout( function() { kalk_row.addClass(`blink_bg`);  }, 500);
          setTimeout( function() { kalk_row.removeClass(`blink_bg`);  }, 2500);
        };

      }; // kraj ako postoji goto status

      // ALI POSTOJI GOTO KALK
      // I TAKOĐER POSTOJI GOTO PROCES

      if ( data.goto_kalk && data.goto_proces ) {

        var kalk_row = null;
        var proces_row = null;

        var product_comp = null;
        var kalk_tab = null;


        // ovaj loop je kada tražim proces  !!!
        $(`.proces_row`).each( function() {

          if ( $(this).attr(`data-row_sifra`) == data.goto_proces ) {
            proces_row = $(this);
            product_comp = proces_row.closest(`.product_comp`);
            
            // kalk_tab = product_comp.find(`.cit_tab_btn`).eq(3);
            
            product_comp.find(`.cit_tab_btn`).each( function() {
              if ( this.id.indexOf(`cit_tab_btn_kalk`) > -1 ) kalk_tab = $(this);
            });
            
            
          };

        });


        if ( proces_row ) {

          // otvori tab za kalkulacije !!!!
          kalk_tab.trigger(`click`);


          // ako je kalkulacija koja je goto kalk colapsana onda je moram otvoriti
          if ( $(`#`+data.goto_kalk+`_kalkulacija`).hasClass(`cit_closed`) ) {
            $(`#`+data.goto_kalk+`_kalkulacija .kalk_arrow`).trigger(`click`);
          };

          setTimeout( function() {
            var scroll_top = proces_row.offset().top;
            $(`html`)[0].scrollTop = scroll_top - 200;
          }, 500);

          setTimeout( function() { proces_row.addClass(`blink_bg`);  }, 500);
          setTimeout( function() { proces_row.removeClass(`blink_bg`);  }, 2500);
        };

      }; // kraj ako postoji goto status
    
    };
    // kraj ako se potrefio goto item i goto varijant
    
    $(`#${data.id}_new_offer_naklada`).off(`blur`);
    $(`#${data.id}_new_offer_naklada`).on(`blur`, function() {
      window.new_offer_naklada = cit_number( $(this).val() );
      format_number_input( window.new_offer_naklada, this, 0);
    });
    
    
    $(`#${data.id}_doc_valuta_manual`).off(`blur`);
    $(`#${data.id}_doc_valuta_manual`).on(`blur`, function() {
      
      if ( $(this).val() == `` ) {
        data.doc_valuta_manual = null;
        return;
      };
      
      var input_val = cit_number ( $(this).val() );
      
      if ( input_val == null ) {
        
        popup_warn(`Nije broj :-P`);
        $(this).val(``);
        data.doc_valuta_manual = null;
        return;
        
      };
      
      data.doc_valuta_manual = input_val;
      $(this).val( cit_format( input_val, 2 ) );
      
    });

     
    $(`#${data.id}_new_offer_mat`).data('cit_props', {
      
      desc: 'odabir _new_offer_mat  kod kreiranja nove kalkulacije : ' + data.id,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: `mat_mix`,
      cit_run: function(state) {
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          window.new_offer_mat = null;
          return;
        };

        $('#'+current_input_id).val(state.naziv);
        window.new_offer_mat = state;
        
      },
      
    });
    


    
    $(`#${data.id} .new_offer_kalk_btn`).off(`click`);
    $(`#${data.id} .new_offer_kalk_btn`).on(`click`, async function() {
      
      var this_button = this;
      
      var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      
      // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
      var product_id = $(this_button).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      if ( !product_data.elements || product_data.elements?.length == 0 ) {
        popup_warn(`Prije generiranja kalkulacije morate dodati barem jedan element proizvodu.`);
        return;
      };
      
      var kalk_type = `offer_kalk`;
      
      var naklada = window.new_offer_naklada;
      
      if ( $(`#${data.id}_new_offer_naklada`).val() == "" || !window.new_offer_naklada ) {
        popup_warn(`Potrebno upisati novu nakladu !!!`);
        return;
      };

      var new_kalk = null;

      var naklada = window.new_offer_naklada;
      var mat = window.new_offer_mat;
      

      // ------------------------------ F201 ------------------------------
      if ( product_data.tip?.sifra == 'TP20' ) {


        if ( $(`#${data.id}_new_offer_mat`).val() == "" || !window.new_offer_mat ) {
          popup_warn(`Potrebno upisati materijal !!!`);
          return;
        };

        new_kalk = {

          kalk_komentar: null,
          kalk_scroll: null,

          time: Date.now(),
          kalk_sifra: `kalk`+cit_rand(),

          prirez_val: null,
          prirez_kontra: null,

          kalk_ploca_mat: window.new_offer_mat,
          kalk_naklada: naklada,

          kalk_reserv: false,
          kalk_for_pro: false,
          kalk_radni_nalog: false,
          
          
          kalk_polov: false,
          allow_mont_visak: false,

          broj_klapni: 1,

          kalk_discount: null,
          kalk_markup: null,
          
          kalk_discount_money: null,
          kalk_markup_money: null,
          

          kalk_final_unit_time: null,
          kalk_final_time: null,

          kalk_final_unit_price: null,
          kalk_final_sum_price: null,

          kalk_final_unit_price_dis_mark: null,
          kalk_final_sum_price_dis_mark: null, 

          
          kalk_fixed_price: null,
          kalk_fixed_mark: null,
          kalk_fixed_discount: null,
          

          procesi: [], // kraj svih procesa u kalk

          sirov_records: [],


        }; // kraj new kalk objekta

        var prirez_objekt = this_module.get_prirez( product_data, new_kalk );
        var prirez_val = prirez_objekt?.val || null;
        var prirez_kontra = prirez_objekt?.kontra || null;
        
        if ( prirez_val && prirez_kontra ) {
          new_kalk.prirez_val = prirez_val;
          new_kalk.prirez_kontra = prirez_kontra;
        };
        

        var broj_boja = 0;

        // uzimam CMYK ZAPIS OD PRVOG ELEMENTA  U PRODUCTU !!!!
        // to je obično element s nazivom prirez kutije 
        if ( product_data.elements[0].elem_cmyk.indexOf(`1/`) > -1 ) broj_boja = 1;
        if ( product_data.elements[0].elem_cmyk.indexOf(`2/`) > -1 ) broj_boja = 2;
        if ( product_data.elements[0].elem_cmyk.indexOf(`3/`) > -1 ) broj_boja = 3;
        if ( product_data.elements[0].elem_cmyk.indexOf(`4/`) > -1 ) broj_boja = 4;


        var duz_klapne = product_data.visina;

        if ( !duz_klapne ) {
          popup_warn(`Potrebno upisati visinu kutije !`);
          return;
        };

        var ulazi = await this_module.best_plate_find( 
          null,
          this_comp_id,
          product_id,
          `offer_kalk`,
          new_kalk.kalk_sifra,
          new_kalk,
          null,
          proces_index=null
        );

        var koliko_ima_producta_sum = 0;
        $.each( ulazi, function(u_ind, ulaz) {
          koliko_ima_producta_sum += (ulaz.kalk_kom_in_sirovina || 1) * ulaz.kalk_sum_sirovina;
        });

        if ( koliko_ima_producta_sum < naklada ) {
          popup_warn(`Nažalost nema dovoljno sirovine! Potrebno naručiti !!!!`);
          return;
        };

        var ljep_klapna_ulazi = null;

        var prirez_povrsina_naklada = null;
        
        if ( prirez_val && prirez_kontra ) prirez_povrsina_naklada = prirez_val * prirez_kontra / 1000000 * naklada;

        if ( prirez_povrsina_naklada && prirez_povrsina_naklada < 1000 ) {

          var krajser_result = VELPROCES.krajser_work( 
            this_module,
            naklada,
            ulazi,
            null, // izlazi
            null, // action
            null, // radna stn
            product_data,
            new_kalk,
            
            arg_proc_koment=null,
            arg_extra_data=null,
            curr_proces_index=0
          );
          
          new_kalk.procesi.push( krajser_result.proces );
          
          var usjek_ulazi = this_module.gen_ulazi_from_izlazi( krajser_result.proces.izlazi );

          var usjek_result = VELPROCES.usjek_work( 
            this_module,
            naklada,
            usjek_ulazi,
            null, // izlazi
            null, // action
            null, // radna stn
            product_data,
            new_kalk,
            
            arg_proc_koment=null,
            arg_extra_data=null,
            curr_proces_index=1
          );

          new_kalk.procesi.push( usjek_result.proces );
          ljep_klapna_ulazi = this_module.gen_ulazi_from_izlazi(usjek_result.proces.izlazi);
          
        }
        else {

          var sloter_result = VELPROCES.sloter_work( 
            this_module,
            naklada,
            ulazi,
            null, // izlazi
            null, // action
            null, // radna stn
            product_data,
            new_kalk,
            
            arg_proc_koment=null,
            arg_extra_data=null,
            curr_proces_index=1
          );
          new_kalk.procesi.push( sloter_result.proces );

          ljep_klapna_ulazi = this_module.gen_ulazi_from_izlazi(sloter_result.proces.izlazi);
          
        };


        var ljep_klapna_result = VELPROCES.ljep_klapna_work( 
          this_module,
          naklada,
          ljep_klapna_ulazi,
          null, // izlazi
          null, // action
          null, // adna stn
          product_data,
          new_kalk,
          
          arg_proc_koment=null,
          arg_extra_data=null,
          curr_proces_index=2
        );
        new_kalk.procesi.push( ljep_klapna_result.proces );


        var pakir_ulazi = this_module.gen_ulazi_from_izlazi(ljep_klapna_result.proces.izlazi);

        var pakir_result = VELPROCES.pakir_work( 
          this_module,
          naklada,
          pakir_ulazi,
          null, // izlazi
          null, // action
          null, // radna stn
          product_data,
          new_kalk,
          
          arg_proc_koment=null,
          arg_extra_data=null,
          curr_proces_index=3
        );
        
        new_kalk.procesi.push( pakir_result.proces );

        var sklad_ulazi = this_module.gen_ulazi_from_izlazi(pakir_result.proces.izlazi);
        
        var sklad_result = VELPROCES.sklad_work( 
          this_module,
          naklada,
          sklad_ulazi,
          null, // izlazi
          null, // action
          null, // radna stn
          product_data,
          new_kalk,
          
          arg_proc_koment=null,
          arg_extra_data=null,
          curr_proces_index=4
        );
        new_kalk.procesi.push( sklad_result.proces );

        var dostav_ulazi = this_module.gen_ulazi_from_izlazi(sklad_result.proces.izlazi);

        var dostav_result = VELPROCES.dostav_work( 
          this_module,
          naklada,
          dostav_ulazi,
          null, // izlazi
          null, // action
          null, // radna stn
          product_data,
          new_kalk,
          
          arg_proc_koment=null,
          arg_extra_data=null,
          curr_proces_index=5
        );
        new_kalk.procesi.push( dostav_result.proces );

      }; 
      // --------- KRAJ --------------------- F201 ------------------------------


      // ------------------------------ PODNI STALAK ------------------------------
      if ( product_data.tip?.sifra == 'TP1' ) {
         
         
      };
      // ---------- KRAJ -------------------- PODNI STALAK ------------------------------
      
      
      
      if ( !data.offer_kalk ) data.offer_kalk = [];
      if ( new_kalk ) data.offer_kalk.push(new_kalk);
      
      console.log(`--------------- make kalk NEW OFFER KALK ----------------`);
      
      var this_kalk = this_module.make_kalks( this_button, data, data[kalk_type], kalk_type, data.tip || null );
      // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      
      wait_for( `$("#${new_kalk.kalk_sifra}_kalkulacija").length > 0`, function() {
        
        // resetiraj new naklada polje za sljedeći upis !!!!
        $(`#${data.id}_new_offer_naklada`).val("");
        window.new_offer_naklada = null;

        // resetiraj new mat za kasnije !!!!
        $(`#${data.id}_new_offer_mat`).val("");
        window.new_offer_mat = null;
        
      }, 5*1000);
      

    });
    
    
    $(`#${data.id} .empty_offer_kalk_btn`).off(`click`);
    $(`#${data.id} .empty_offer_kalk_btn`).on(`click`, async function() {

      // napravi PRAZNU kalkulaciju !!! BEZ PROCESA --- proces je samo prazan array

      var this_button = this;

      var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];

      // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
      var product_id = $(this_button).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];


      if ( !product_data.elements || product_data.elements?.length == 0 ) {
        popup_warn(`Prije generiranja kalkulacije morate dodati barem jedan element proizvodu.`);
        return;
      };

      var kalk_type = `offer_kalk`;

      var naklada = window.new_offer_naklada;

      if ( $(`#${data.id}_new_offer_naklada`).val() == "" || !window.new_offer_naklada ) {
        popup_warn(`Potrebno upisati novu nakladu !!!`);
        return;
      };

      var new_kalk = null;

      var naklada = window.new_offer_naklada;
      var mat = window.new_offer_mat;


      var new_kalk = {

        kalk_komentar: null,

        kalk_scroll: null,

        time: Date.now(),
        kalk_sifra: `kalk`+cit_rand(),

        prirez_val: null,
        prirez_kontra: null,

        kalk_ploca_mat: window.new_offer_mat,
        kalk_naklada: naklada,

        kalk_reserv: false,
        kalk_for_pro: false,
        kalk_radni_nalog: false,

        kalk_polov: false,
        allow_mont_visak: false,

        broj_klapni: null,

        kalk_discount: null,
        kalk_markup: null,

        kalk_discount_money: null,
        kalk_markup_money: null,

        kalk_final_unit_time: null,
        kalk_final_time: null,

        kalk_final_unit_price: null,
        kalk_final_sum_price: null,


        kalk_final_unit_price_dis_mark: null,
        kalk_final_sum_price_dis_mark: null,

        kalk_fixed_price: null,
        kalk_fixed_mark: null,
        kalk_fixed_discount: null,


        procesi: [],

        sirov_records: [],

      }; // kraj empty kalk

      new_kalk.procesi = [ 
        this_module.fresh_proces()
      ];

      if ( !data.offer_kalk ) data.offer_kalk = [];
      data.offer_kalk.push(new_kalk);
      
      console.log(`--------------- make kalk EMPTY OFFER KALK ----------------`);
      
      var this_kalk = this_module.make_kalks( this_button, data, data[kalk_type], kalk_type, data.tip || null );
      // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      
      wait_for( `$("#${new_kalk.kalk_sifra}_kalkulacija").length > 0`, function() {
        
        // resetiraj new naklada polje za sljedeći upis !!!!
        $(`#${data.id}_new_offer_naklada`).val("");
        window.new_offer_naklada = null;

        // resetiraj new mat za kasnije !!!!
        $(`#${data.id}_new_offer_mat`).val("");
        window.new_offer_mat = null;
        
      }, 20*1000);
      
      
  });
    
    
    $(`#${data.id} .save_offer_kalk_btn`).off(`click`);
    $(`#${data.id} .save_offer_kalk_btn`).on(`click`, this_module.save_kalk );
    
    $(`#${data.id} .save_pro_kalk_btn`).off(`click`);
    $(`#${data.id} .save_pro_kalk_btn`).on(`click`, this_module.save_kalk );
    
    
    $(`#${data.id} .save_post_kalk_btn`).off(`click`);
    $(`#${data.id} .save_post_kalk_btn`).on(`click`, this_module.save_kalk );
    
    
    
  }, 20*1000 );

  return {
    html: component_html,
    id: data.id
  };

},
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};
  
  function set_init_data(data) {
    
    // ako već postoji kalk objekt koji ima isti database _id
    // ONDA GA OBRIŠI TAKO DA NEMA DUPLIH
    $.each( this_module.cit_data, function( key, data_object ) {
      if ( data_object._id == data._id ) delete  this_module.cit_data[key];
    });
    
    this_module.cit_data[data.id] = data;
    
  };
  this_module.set_init_data = set_init_data;
  
  
  
  function reset_kalk_prices( curr_kalk ) {
      
    curr_kalk.kalk_markup = 0;
    curr_kalk.kalk_discount = 0;
    curr_kalk.kalk_markup_money = 0;
    curr_kalk.kalk_discount_money = 0;

    curr_kalk.kalk_final_unit_price_dis_mark = curr_kalk.kalk_final_unit_price;
    curr_kalk.kalk_final_sum_price_dis_mark = curr_kalk.kalk_final_unit_price * curr_kalk.kalk_naklada;


    format_number_input( curr_kalk.kalk_markup, $(`#`+curr_kalk.kalk_sifra+`_kalk_markup`)[0], 2);
    format_number_input( curr_kalk.kalk_discount, $(`#`+curr_kalk.kalk_sifra+`_kalk_discount`)[0], 2);
    format_number_input( curr_kalk.kalk_markup_money, $(`#`+curr_kalk.kalk_sifra+`_kalk_markup_money`)[0], 2);
    format_number_input( curr_kalk.kalk_discount_money, $(`#`+curr_kalk.kalk_sifra+`_kalk_discount_money`)[0], 2);

    var unit_price_final_input = $(`#` + curr_kalk.kalk_sifra + `_kalk_final_unit_price_dis_mark`)[0];
    format_number_input( curr_kalk.kalk_final_unit_price_dis_mark, unit_price_final_input, 2);
    var sum_price_final_input = $(`#` + curr_kalk.kalk_sifra + `_kalk_final_sum_price_dis_mark`)[0];
    format_number_input( curr_kalk.kalk_final_sum_price_dis_mark, sum_price_final_input, 2);

  };
  this_module.reset_kalk_prices = reset_kalk_prices;

  
  
  
  
  async function save_kalk(e, arg_button, save_all) {
    
    
    var this_button = arg_button || this;
    var this_button_id = arg_button?.id || this.id;

    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    
    // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
    var product_id = $(this_button).closest('.product_comp')[0].id;
    var product_data = this_module.product_module.cit_data[product_id];
    
    
    
    
    
    return new Promise( function( resolve, reject) {
      
      if ( !window.cit_user ) {
        popup_warn(`Niste ulogirani !!!`);
        resolve(null);
        return;
      };
      
      
      var curr_user = { 
        _id: window.cit_user._id,
        user_number: window.cit_user.user_number,
        full_name: window.cit_user.full_name,
        email: window.cit_user.email 
      };
      
    
      var kalk_type = null;

      
      // -------------------------- od ovog trena radim sa kopijom kalk a ne s originalom !!!!! -----------------------
      var kalk_obj = cit_deep( this_module.cit_data[this_comp_id] );
      
      // ažuriraj product full name unutar kalkulacije
      kalk_obj.full_product_name = this_module.product_module.generate_full_product_name( product_data );
      
      // ne treba mi original kalk jer je prevelik za ajax i sve ide slabije :)
      if ( kalk_obj.original_kalk ) delete kalk_obj.original_kalk;
      
      
      kalk_obj = reduce_kalk_original_sirovs(kalk_obj);

      var meta_prop = null;

      if ( $(this_button).hasClass("save_offer_kalk_btn") ) { kalk_obj.type = `offer`; meta_prop = `meta_offer_kalk`; };
      if ( $(this_button).hasClass("save_pro_kalk_btn")   ) { kalk_obj.type = `pro`; meta_prop = `meta_pro_kalk`; };
      if ( $(this_button).hasClass("save_post_kalk_btn")  ) { kalk_obj.type = `post`; meta_prop = `meta_post_kalk`; };

      if ( save_all  ) { 
        kalk_obj.type = `all`;
      };


      if ( !kalk_obj.offer_kalk) kalk_obj.offer_kalk = [];
      if ( !kalk_obj.meta_offer_kalk) kalk_obj.meta_offer_kalk = {};


      if ( !kalk_obj.pro_kalk) kalk_obj.pro_kalk = [];
      if ( !kalk_obj.meta_pro_kalk) kalk_obj.meta_pro_kalk = {};


      if ( !kalk_obj.post_kalk) kalk_obj.post_kalk = [];
      if ( !kalk_obj.meta_post_kalk) kalk_obj.meta_post_kalk = {};

      /*
      if ( !kalk_obj.product_id ) {
        popup_warn(`Prvo morate spremiti ovaj Proizvod !!!`);
        return;
      };
      */

      $(this_button).css("pointer-events", "none");
      toggle_global_progress_bar(true);

      // ako data nema _id znači da je ovo NOVI DATA SET !!!!
      if ( !kalk_obj[meta_prop].saved ) {

        kalk_obj[meta_prop].user_saved = curr_user;
        kalk_obj[meta_prop].saved = Date.now();
        
        $(`#${kalk_obj.id} .${meta_prop}_save_box`).html(
`
<b>SPREMLJENO:</b> ${kalk_obj[meta_prop].user_saved.full_name }
&nbsp;&nbsp;
${ cit_dt( kalk_obj[meta_prop].saved).date_time }
`
        );
        

      } 
      else {

        kalk_obj[meta_prop].user_edited = curr_user;
        kalk_obj[meta_prop].edited = Date.now();
        
        $(`#${kalk_obj.id} .${meta_prop}_edit_box`).html(
`
<b>EDITIRANO:</b> ${kalk_obj[meta_prop].user_edited.full_name }
&nbsp;&nbsp;
${ cit_dt( kalk_obj[meta_prop].edited).date_time }
`
        );
        
      };

      var date_now = Date.now();
      

      if ( save_all  ) {

        // --------------------------------------------
        kalk_obj.meta_offer_kalk.user_saved = curr_user;
        kalk_obj.meta_offer_kalk.saved = date_now;
        // --------------------------------------------

        kalk_obj.meta_pro_kalk.user_saved = curr_user;
        kalk_obj.meta_pro_kalk.saved = date_now;
        // --------------------------------------------

        kalk_obj.meta_post_kalk.user_saved = curr_user;
        kalk_obj.meta_post_kalk.saved = date_now;
        // --------------------------------------------

      };

      $.ajax({
        headers: {
          'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
        },
        type: "POST",
        cache: false,
        url: `/save_kalk`,
        data: JSON.stringify( kalk_obj ),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done(function (result) {

        console.log(result);

        if ( result.success == true ) {

          this_module.cit_data[this_comp_id]._id = result.kalk._id;
          
          
          // UPDATE ORIGINAL KALK UNUTAR ORIGINALNOG KALKA !!!!!!!!
          this_module.cit_data[this_comp_id].original_kalk = cit_deep( result.kalk );
          

          // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
          var product_id = $(this_button).closest('.product_comp')[0].id;
          var product_data = this_module.product_module.cit_data[product_id];

          var current_kalk_id = product_data.kalk.id;
          
          
          // prekopiraj spremljeni kalk u product data
          // ----------------------- za sada necu uzeti verziju iz rezulta  sa servera nego ću ostaviti lokalnu verziju --------------------------- 
          // product_data.kalk = result.kalk;
          // upiši nazad nazad kalk id
          product_data.kalk.id = current_kalk_id;
          
          
          // napravi samo update _id ako ga nema tj ako je ovo prvi puta da spremam kalk
          product_data.kalk._id = result.kalk._id;
          
          
          // također upiši _id u trenutni kalk ukojem se sada nalazim
          this_module.cit_data[this_comp_id]._id = result.kalk._id;
          
          resolve( result.kalk._id );
          
          cit_toast(`KALKULACIJA SPREMLJENA!`);
          
        } else {
          
          if ( result.msg ) popup_error(result.msg);
          if ( !result.msg ) popup_error(`Greška prilikom spremanja!`);
          
          resolve( null );
          
        };

      })
      .fail(function (error) {

        console.log(error);
        popup_error(`Došlo je do greške na serveru prilikom spremanja kalkulacije !!!`);
        
        resolve( null );
        
      })
      .always(function() {

        toggle_global_progress_bar(false);
        $(this_button).css("pointer-events", "all");

      });
    
    }); // kraj promisa
      
      
  };
  this_module.save_kalk = save_kalk;
  
  
  async function update_kalk_product_name( arg_product_id, arg_full_product_name ) {
    
    $.ajax({
      headers: {
        'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
      },
      type: "POST",
      cache: false,
      url: `/update_kalk_product_name`,
      data: JSON.stringify({
        product_id: arg_product_id,
        full_product_name: arg_full_product_name
      }),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done(function (result) {

      console.log(result);
      if ( result.success == true ) {

        cit_toast(`AŽURIRANO IME PROIZVODA U KALKULACIJI !!!`);

      } else {
        if ( result.msg ) popup_error(result.msg);
        if ( !result.msg ) popup_error(`Greška prilikom ažuriranja imena produkta unutar kalkulacije!`);
      };

    })
    .fail(function (error) {

      console.log(error);
      popup_error(`Došlo je do greške na serveru prilikom ažuriranja imena produkta unutar kalkulacije!`);
    })
    .always(function() {

      toggle_global_progress_bar(false);

    });
    
    
  };
  this_module.update_kalk_product_name = update_kalk_product_name;
  
  
  
  function kalk_real_plate( po_valu, po_kontravalu, arg_prirez_val, arg_prirez_kontra, ulaz, kalk ) {
    
    
    var price_m2 = get_price_m2(ulaz).price_m2; // vraća cijenu i dimenczije ulazne ploče !!!
    
    // može biti nula
    if ( !price_m2 && price_m2 !== 0 ) return;
    
    
    var prirez_val = arg_prirez_val || get_price_m2(ulaz).val; // vraća cijenu i dimenzije ulazne ploče !!! -----> VAL
    var prirez_kontra = arg_prirez_kontra || get_price_m2(ulaz).kontra; // vraća cijenu i dimenzije ulazne ploče !!! -----> KONTRA

    
    var koef_val = po_valu / prirez_val;
    var kom_val = parseInt(koef_val);
    var ostatak_val = po_valu - ( kom_val * prirez_val );

    var koef_kontra = po_kontravalu / prirez_kontra;
    var kom_kontra = parseInt(koef_kontra);
    var ostatak_kontra = po_kontravalu - ( kom_kontra * prirez_kontra )

    // realno što uzimam na počerku stavim da bude cijela ploča
    var real_val = po_valu;
    var real_kontra = po_kontravalu;
    

    if ( ostatak_val >= 300 ) {
      real_val = po_valu - ostatak_val;
    };

    if ( ostatak_kontra >= 400 ) {
      real_kontra = po_kontravalu - ostatak_kontra;
    };

    
    var real_unit_area = (real_val * real_kontra) / 1000000;
    var real_unit_price = price_m2 * real_unit_area;
    var kalk_kom = kom_val * kom_kontra;
    
    return {
      real_val: real_val,
      real_kontra: real_kontra,
      area: real_unit_area,
      ostatak_val: ostatak_val >= 300 ? ostatak_val : null,
      ostatak_kontra: ostatak_kontra >= 400 ? ostatak_kontra : null,
      kom: kalk_kom,
      kom_val: kom_val,
      kom_kontra: kom_kontra,
      price: real_unit_price,
      
    }
    
  };
  this_module.kalk_real_plate = kalk_real_plate;
  

  function make_ostatak( real, arg_ulaz) {


    var ostatak_val = null;
    var ostatak_kontra = null;

    // ako IMA ostatak po valu ali NEMA po kontra
    if ( real.ostatak_val && !real.ostatak_kontra ) { 

      ostatak_val = { 
        sifra: "ostatak"+cit_rand(), // kasnije treba SAVE u bazu !!!!!
        _id: null,
        kalk_full_naziv: arg_ulaz.kalk_full_naziv,
        kalk_po_valu: real.ostatak_val,
        kalk_po_kontravalu: arg_ulaz.kalk_po_kontravalu,
        area: real.ostatak_val * arg_ulaz.kalk_po_kontravalu / 1000000,
      };

      ostatak_kontra = null;

    };

    // ako NEMA ostatak po valu ali IMA po kontra
    if ( !real.ostatak_val && real.ostatak_kontra ) { 

      ostatak_val = null;

      ostatak_kontra = { 
        sifra: "ostatak"+cit_rand(), // kasnije treba SAVE u bazu !!!!!
        _id: null,
        kalk_full_naziv: arg_ulaz.kalk_full_naziv,
        kalk_po_valu: arg_ulaz.kalk_po_valu,
        kalk_po_kontravalu: real.ostatak_kontra,
        area: arg_ulaz.kalk_po_valu * real.ostatak_kontra / 1000000,
      };

    };

    // ako IMA ostatak i po valu i kontra
    if ( real.ostatak_val && real.ostatak_kontra ) { 

      ostatak_val = { 
        sifra: "ostatak"+cit_rand(), // kasnije treba SAVE u bazu !!!!!
        _id: null,
        kalk_full_naziv: arg_ulaz.kalk_full_naziv,
        kalk_po_valu: real.ostatak_val,
        kalk_po_kontravalu: arg_ulaz.kalk_po_kontravalu,
        area: real.ostatak_val * arg_ulaz.kalk_po_kontravalu / 1000000
      };

      ostatak_kontra = { 
        sifra: "ostatak"+cit_rand(), // kasnije treba SAVE u bazu !!!!!
        _id: null,
        kalk_full_naziv: arg_ulaz.kalk_full_naziv,
        kalk_po_valu: arg_ulaz.kalk_po_valu - real.ostatak_val, // trebam odbiti po valu jer sam već uzeo taj dio kada sam napravio ostatak val !!!!!!
        kalk_po_kontravalu: real.ostatak_kontra,
        area: ( arg_ulaz.kalk_po_valu - real.ostatak_val ) * real.ostatak_kontra / 1000000,
        
      };

    };

    return {
      val: ostatak_val,
      kontra: ostatak_kontra,
    };

  };
  this_module.make_ostatak = make_ostatak; 
  
 
  /* 
  
  cut_cicak_work		DORADA - REZANJE ČIČAK TRAKE (muške i ženske)
  glue_cicak_M_work		DORADA - NANOŠENJE ČIČAK TRAKE - MUŠKA
  glue_cicak_F_work		DORADA - NANOŠENJE ČIČAK TRAKE - ŽENSKA

 */ 
  
  
  
  function make_kalks( this_input, data, kalks, kalk_type, tip_proizvoda, return_html, arg_kalk_type, arg_kalk_sifra ) {
    
    // ako ne postoji ovaj array onda ga napravi
    if ( !window.kalk_extra_added) window.kalk_extra_added = [];

    if ( arg_kalk_type && arg_kalk_sifra ) {
      this_module.check_is_extra_added( arg_kalk_type, arg_kalk_sifra );
    }; // kraj jel imam argumente za specifičan kalk type i kalk sifru
    
    
    /*
    
    ZA SADA NE RADIM DIFF JER STALNO GENERIRAM NOVE KALK ELEMENTE (po defaultu na izlazu stalno radim nove sifre )
    ZA SADA NE RADIM DIFF JER STALNO GENERIRAM NOVE KALK ELEMENTE (po defaultu na izlazu stalno radim nove sifre )
    ----------------------------------------------------------------------------------------------------
    if ( !return_html ) { 
    
      var orig_kalks = data.original_kalk ? data.original_kalk[kalk_type] : [];

      var kalks_diff = cit_deep_diff.map( orig_kalks, kalks );

      window.cit_created_count = 0;
      window.cit_updated_count = 0;
      window.cit_deleted_count = 0;
      window.cit_unchanged_count = 0;

      cit_traverse( kalks_diff, `type` );

      // ako ništa nije novo samo stani !!!!
      // ako ništa nije novo samo stani !!!! -----> tj nemoj raditi update jer je sve ostalo isto
      // ako ništa nije novo samo stani !!!!
      if ( window.cit_created_count == 0 && window.cit_updated_count == 0 && window.cit_deleted_count == 0 ) {
        return;
      };
    
    };
    ----------------------------------------------------------------------------------------------------
    */
    
    var all_kalks = null; 
    
    if ( return_html  ) {
      
      all_kalks = this_module.run_generate_kalk( this_input, data, kalks, kalk_type, tip_proizvoda, return_html );
      return all_kalks;
      
    } else {
      
      // ------------------------------------------ STAVIO SAM CIJELI MAKE KALKS  U TIMEOUT !!!!!!!!! ------------------------------------------
      // ovaj timeout je zato što kada dođe do renderiranja cijelog html-a za kalk
      // moram napraviti mali delay tako da aplikacija shvati za sam se prebacio na sljedeći input ( kalk_input_elem_id == current_input_id )
      // ako to radim bez delay-a ona aplikacija zabilježi da sam još uvijek na prethodnom inputu !!!!!
      
      setTimeout( function() { 
        all_kalks = this_module.run_generate_kalk( this_input, data, kalks, kalk_type, tip_proizvoda, return_html );
        $(`#${data.id} .${kalk_type}_container`).html(all_kalks);
        reset_tooltips();
      }, 300 );
      
    };
    
  };
  this_module.make_kalks = make_kalks;

  
  
  function run_generate_kalk( this_input, data, kalks, kalk_type, tip_proizvoda, return_html )  {
    

    var scroll_top = 0;
    var scroll_left = 0;


    var kalk_input_elem_id = null;

    // ako se curr input nalazi unutar kalk polja koje scrollam
    if ( $('#'+current_input_id).closest(`.center_scroll_box`).length > 0 ) {

      // var random_prefix = current_input_id.split(`_`)[0];
      // obrišem prednji dio koji je random pa zato ne mogu pronaći takav isti id nakon ponovnog generiranja svih kalks 
      // kalk_input_elem_id = current_input_id.replace( random_prefix, `` );
      
      
      // TODO -----> MORAM PROVJERITI JEL OVO NEMA POSLJEDICA
      // ZA SADA UZIMAM CIJEL ID JER SAM NAPRAVIO DA MI SE KALK RAND ID NIKADA NE MIJENJA
      // ZA SADA UZIMAM CIJEL ID JER SAM NAPRAVIO DA MI SE KALK RAND ID NIKADA NE MIJENJA
      
      kalk_input_elem_id = current_input_id;

    };


    // moram zapamtit scroll prije renderiranja
    var center_scroll_box = $(this_input).closest(`.center_scroll_box`);

    var start_kalk_sifra = null;

    if ( center_scroll_box?.length > 0 ) {

      start_kalk_sifra = center_scroll_box[0].id.replace(`_center_scroll_box`, ``);
      scroll_top = center_scroll_box[0].scrollTop;
      scroll_left = center_scroll_box[0].scrollLeft;
    };

    var valid = this_module.valid;

    var all_kalks = ``;


    function make_all_procesi(procesi, kalk) {

      // kalk.kalk_rand = cit_rand();
      if ( !kalk?.kalk_rand ) kalk.kalk_rand = cit_rand();
      

      var all_procesi = ``;
      $.each(procesi, function(p_ind, proc) {

        var proc_html = this_module.make_kalk_proces( data, proc, kalk, kalk_type, tip_proizvoda, p_ind);
        all_procesi += proc_html;
      });

      return all_procesi;

    };


    var naslov_type = "";

    /*
    if ( kalk_type == `offer_kalk` ) naslov_type = "KALKULACIJA ZA PONUDU";
    if ( kalk_type == `pro_kalk` ) naslov_type = "KALKULACIJA ZA PROIZVODNJU";
    if ( kalk_type == `post_kalk` ) naslov_type = "POST KALKULACIJA";
    */

    if ( kalk_type == `offer_kalk` ) naslov_type = ""; // "KALKULACIJA ZA PONUDU";
    if ( kalk_type == `pro_kalk` ) naslov_type = ""; // "KALKULACIJA ZA PROIZVODNJU";
    if ( kalk_type == `post_kalk` ) naslov_type = ""; // "POST KALKULACIJA";


    $.each( kalks, function( k_ind, kalk ) {
      
      
      
      // -------------------------------------------------- START ------------------------------------------------------------
      // REMOVAJ FORME U EXTRA DATA U SVIM PROCESIMA AKO VIŠE NIGDJE NEMA TE FORME !!!!!
      // --------------------------------------------------------------------------------------------------------------
      var all_ulaz_forms = [];  
      var all_extra_data_forms = [];  
      
      $.each( kalk.procesi, function(proces_ind, find_proces) {
        
        
        // DODAJ IMENA FORME IZ ULAZA
        $.each( find_proces.ulazi, function(u_ind, find_ulaz) {
          if ( find_ulaz.kalk_plate_naziv && find_ulaz.kalk_plate_id ) {
            var form_name = find_ulaz.kalk_plate_naziv.split(` ---`)[0];
            all_ulaz_forms = upsert_item(all_ulaz_forms, form_name);
          };
        });
        

        if ( find_proces.extra_data?.forme?.length > 0 ) {

          var forme_length = find_proces.extra_data.forme.length;
          var i;
          for ( i=forme_length - 1; i>=0; i-- ) {
            // ako nigdje nema u niti jednom ulazu ove forme ( DO SADA tj  u ovom procesu i svim prethodnima ) onda tu formu removaj iz eztra data !!!
            if ( all_ulaz_forms.indexOf( find_proces.extra_data.forme[i] ) == -1 ) {
              // removaj ovu formu
              kalk.procesi[proces_ind].extra_data.forme.splice(i, 1); 
            }
          };

        };
        
        
      });

      // --------------------------------------------------- END -----------------------------------------------------------
      // REMOVAJ FORME U EXTRA DATA U SVIM PROCESIMA AKO VIŠE NIGDJE NEMA TE FORME !!!!!
      // --------------------------------------------------------------------------------------------------------------
          
      
      
      
      
      


      var final_sum = 0;
      var final_time = 0;

      var all_sirovs = [];


      var final_alat_price = 0;

      if ( kalk.procesi?.length > 0 ) {

        $.each(kalk.procesi, function(p_ind, proc) {

          $.each(proc.ulazi, function(u_ind, ulaz) {

            if ( ulaz._id ) {


              // ako je alat uvjek stavi da je jedan komad
              if ( ulaz.klasa_sirovine?.sifra == "KS5") ulaz.kalk_sum_sirovina = 1; 

              if ( ulaz.klasa_sirovine?.sifra == "KS5" && proc.extra_data?.alat_apart ) {
                final_alat_price += (ulaz.real_unit_price || ulaz.kalk_unit_price);
                ulaz.kalk_price = 0;

                if ( proc.extra_data && proc.extra_data.ulaz_prices ) {
                  // posve obriši ovaj property tj ne smje biti custom cijena ako je alat odvojeno
                  if ( proc.extra_data.ulaz_prices.hasOwnProperty(ulaz.sifra) ) delete proc.extra_data.ulaz_prices[ulaz.sifra];
                }
              };

              // ako je alat ali nije odvojeno onda napravi update cijene alata
              if ( ulaz.klasa_sirovine?.sifra == "KS5" && !proc.extra_data?.alat_apart ) {
                ulaz.kalk_price = (ulaz.real_unit_price || ulaz.kalk_unit_price); // * ulaz.kalk_sum_sirovina;
              };

              var ulaz_price = 0;

              var extra_ulaz_price = find_extra_data_ulaz_price( proc.extra_data, ulaz.sifra );

              if (  extra_ulaz_price !== null ) {
                ulaz_price = extra_ulaz_price;
              } else {
                ulaz_price = ulaz.kalk_price !== null ? ulaz.kalk_price : 0;
              };


              final_sum += ulaz_price;

            };

          }); // loop po ulazima

          final_sum += proc.action.action_est_price || 0;

          final_time += ( proc.action.action_work_time || 0 ) + ( proc.action.action_prep_time || 0 );

        }); // loop po procesima

      }; // kraj ako postoje procesi


      if ( !kalk.kalk_discount ) kalk.kalk_discount = 0;
      if ( !kalk.kalk_markup ) kalk.kalk_markup = 0;


      var price_fix = 0;

      // ako nema novčanog popusta ili marže
      if ( !kalk.kalk_discount_money && !kalk.kalk_markup_money ) {
        kalk.kalk_discount = 0;
        kalk.kalk_markup = 0;
        kalk.kalk_discount_money = 0;
        kalk.kalk_markup_money = 0;
      };

      // ako ima popust ali nema maržu
      if ( kalk.kalk_discount_money && !kalk.kalk_markup_money ) {
        kalk.kalk_markup_money = 0;
        kalk.kalk_markup = 0;
        kalk.kalk_discount = 100 * kalk.kalk_discount_money / final_sum;
      };

      // ako nema popust ali IMA MARŽU
      if ( !kalk.kalk_discount_money && kalk.kalk_markup_money ) {
        kalk.kalk_discount_money = 0;
        kalk.kalk_discount = 0;
        kalk.kalk_markup = 100 * kalk.kalk_markup_money / final_sum;
      };

      // ako ima MARŽ I POPUST 
      if ( kalk.kalk_discount_money && kalk.kalk_markup_money ) {
        kalk.kalk_markup = 100 * kalk.kalk_markup_money / final_sum;
        var sum_with_markup = kalk.kalk_markup_money + final_sum;
        kalk.kalk_discount = 100 *  kalk.kalk_discount_money / sum_with_markup;
      };


      if ( kalk.kalk_discount ) price_fix = kalk.kalk_discount * -1; // discount mora biti negativan broj
      if ( kalk.kalk_markup ) price_fix = kalk.kalk_markup ;

      kalk.kalk_final_unit_time = final_time/kalk.kalk_naklada;
      kalk.kalk_final_time = final_time;

      kalk.kalk_final_sum_price = final_sum;
      kalk.kalk_final_unit_price = final_sum / kalk.kalk_naklada;


      kalk.kalk_final_alat_price = final_alat_price;


      var price_start = kalk.kalk_final_unit_price;
      var price_final = null;


      // ako nema niti maržu niti popust 
      if ( !kalk.kalk_markup && !kalk.kalk_discount ) price_final = price_start;
      // ima maržu nema popust
      if ( kalk.kalk_markup && !kalk.kalk_discount ) price_final = price_start * (100 + kalk.kalk_markup )/100;
      // ima popust nema maržu
      if ( !kalk.kalk_markup && kalk.kalk_discount ) price_final = price_start * (100 - kalk.kalk_discount )/100;

      // ima i maržu i popust
      if ( kalk.kalk_markup && kalk.kalk_discount ) {

        // startna cijena je sada s popustom
        price_start = kalk.kalk_final_unit_price * (100 + kalk.kalk_markup )/100;
        price_final = price_start * (100 - kalk.kalk_discount )/100;
      };


      kalk.kalk_final_unit_price_dis_mark = price_final;
      kalk.kalk_final_sum_price_dis_mark = price_final * kalk.kalk_naklada;

      var copy_kalk_button = ``; 
      var delete_kalk_button = ``;


      // obrisati kalkulaciju se može samo ako je unutar OFFER kalkulacija !!!!
      // obrisati kalkulaciju se može samo ako je unutar OFFER kalkulacija !!!!
      // obrisati kalkulaciju se može samo ako je unutar OFFER kalkulacija !!!!


      var pill_reserv = ``;
      var pill_sample = ``;
      var pill_for_pro = ``;


      if ( kalk_type == "offer_kalk" && kalk.kalk_reserv ) pill_reserv = `<div class="pill_reserv">PK</div>`;
      if ( kalk_type == "offer_kalk" && kalk.kalk_sample ) pill_for_pro = `<div class="pill_sample">UZORAK ${kalk.sample_sifra}</div>`;
      if ( kalk_type == "offer_kalk" && kalk.kalk_for_pro ) pill_for_pro = `<div class="pill_for_pro">NK</div>`



      if ( kalk_type == "offer_kalk" ) {


        copy_kalk_button = 
`
<button data-kalk_sifra="${kalk.kalk_sifra}"
      data-kalk_type="${kalk_type}"
      class="cit_tooltip blue_btn btn_small_text copy_kalk_btn" 
      data-toggle="tooltip" data-placement="top" data-html="true" title="Kopiraj ovu kalkulaciju"
      style="margin: 0; box-shadow: none; padding: 0 10px; float: right; margin-right: 30px;">
<i class="far fa-copy" style="font-size: 16px;"></i>
</button>

`;        

      };



    // mogu DELETE NA SVA TRI KALKA TIPA
    // mogu DELETE NA SVA TRI KALKA TIPA
if ( kalk_type == "offer_kalk" || kalk_type == "pro_kalk" || kalk_type == "post_kalk" ) {

        delete_kalk_button = 
`
<button data-kalk_sifra="${kalk.kalk_sifra}"
      data-kalk_type="${kalk_type}"
      class="cit_tooltip blue_btn btn_small_text delete_kalk_btn" 
      data-toggle="tooltip" data-placement="top" data-html="true" title="Obriši cijelu kalkulaciju !!!"
      style="margin: 0; box-shadow: none; padding: 0 10px; background: #bf0060; float: right;">
<i class="fas fa-trash-alt" style="font-size: 16px;"></i>
</button>

`;


      };



      var kalk_style = "";

      if ( kalk_type == `offer_kalk` ) kalk_style = "background: #d6eaf7;";
      if ( kalk_type == `pro_kalk` ) kalk_style = "background: #e2ede5;";
      if ( kalk_type == `post_kalk` ) kalk_style = "background: #f3e6e9;";


      // if ( kalk.kalk_reserv || kalk.kalk_for_pro || kalk.kalk_radni_nalog ) kalk_style += ` pointer-events: none;`;

      var reserv_switches = ``;
      ;

      if ( kalk_type == `offer_kalk` )  {

        reserv_switches = 
    `
    <div class="col-md-1 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_kalk_reserv`, valid.kalk_reserv, kalk.kalk_reserv, null, `pointer-events: all;`) }
    </div>

    <div class="col-md-1 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_kalk_sample`, valid.kalk_sample, kalk.kalk_sample, null, `pointer-events: all;` ) }
    </div>


    <div class="col-md-1 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_kalk_for_pro`, valid.kalk_for_pro, kalk.kalk_for_pro , null, `filter: hue-rotate(-110deg); pointer-events: all;` ) }
    </div>



    `;

      };


      var rn_switch = ``

      if ( kalk_type == `pro_kalk` ) {
        rn_switch = 
        `<div class="col-md-2 col-sm-12">
          ${ cit_comp( kalk.kalk_sifra + `_kalk_radni_nalog`, valid.kalk_radni_nalog, kalk.kalk_radni_nalog , null, `filter: hue-rotate(70deg); pointer-events: all;` ) }
        </div>`; 
      };



      var fixed_style = ``;

      // ako je fixirana marža ili popust
      if ( kalk.kalk_fixed_mark || kalk.kalk_fixed_discount ) {
        fixed_style = `pointer-events: none;`
      };

      var kalk_html = 
  `



  <div  id="${kalk.kalk_sifra}_kalkulacija"
      data-kalk_sifra="${kalk.kalk_sifra}"
      data-kalk_type="${kalk_type}"
      class="container-fluid kalk_box ${ kalk.closed ? 'cit_closed' : '' }"
      style="${ kalk_style }"
      >



  ${delete_kalk_button}
  ${copy_kalk_button}

  <div class="fixed_price_box" >
    ${ kalk_type == `offer_kalk` ? cit_comp( kalk.kalk_sifra + `_kalk_fixed_price`, valid.kalk_fixed_price, kalk.kalk_fixed_price ? true : false, "", `opacity: 1;` ) : "" }
  </div>

  <div class="show_in_offer_box">
    ${ kalk_type == `offer_kalk` ? cit_comp( kalk.kalk_sifra + `_show_in_offer`, valid.show_in_offer, kalk.show_in_offer ? true : false ) : "" }
  </div>

  <div class="show_in_offer_box">
    ${ kalk_type == `offer_kalk` ? cit_comp( kalk.kalk_sifra + `_kalk_fixed_mark`, valid.kalk_fixed_mark, kalk.kalk_fixed_mark ? true : false ) : "" }
  </div>

  <div class="show_in_offer_box">
    ${ kalk_type == `offer_kalk` ? cit_comp( kalk.kalk_sifra + `_kalk_fixed_discount`, valid.kalk_fixed_discount, kalk.kalk_fixed_discount ? true : false ) : "" }
  </div>



  ${pill_reserv}
  ${pill_sample}
  ${pill_for_pro}


  <!--
  <button data-kalk_sifra="${kalk.kalk_sifra}"
          data-kalk_type="${kalk_type}"
          class="cit_tooltip blue_btn btn_small_text new_proces_kalk_btn" 
          data-toggle="tooltip" data-placement="top" data-html="true" title="Dodaj novi proces !!!"
          style="margin: 0 20px 0 0; box-shadow: none; padding: 0 10px; background: #3f6ad8; float: right;">

    <i class="fas fa-cog" style="font-size: 17px; margin-top: 2px;" ></i>
  </button>

  -->

  <i class="fal fa-angle-down kalk_arrow" style="pointer-events: all;" ></i>

  <h5 class="kalk_naslov" >
    ${ create_kalk_naslov(data, kalk) }
  </h5>

  <h5 class="title_komentar">${ kalk.kalk_komentar || "&nbsp;&nbsp;&nbsp;&nbsp;" }</h5>

  <div class="row" >
    <div class="col-md-12 col-sm-12">
      ${ cit_comp(
        kalk.kalk_sifra+`_kalk_komentar`, 
        valid.kalk_komentar,
        kalk.kalk_komentar || "",
        null,
        `height: 34px; min-height: 0; font-size: 16px; line-height: 19px; font-weight: 700; letter-spacing: 0.05rem; pointer-events: all;` 
      ) }
    </div>
  </div>

  <div class="row" style="display: ${ kalk_type == `offer_kalk` ? "flex" : "none" }" >
    <div class="col-md-12 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_add_fixed_price_contract`, valid.add_fixed_price_contract, null, "", `pointer-events: all !important;` ) }
    </div>
  </div>

  <div class="row" style="display: ${ kalk_type == `offer_kalk` ? "flex" : "none" }" >
    <div  class="col-md-12 col-sm-12 cit_result_table ugovor_or_tender_box result_table_inside_gui" 
          id="${kalk.kalk_sifra}_ugovor_or_tender_box" style="padding-top: 0; background: transparent; pointer-events: all;">
      <!-- OVDJE SAMO JEDAN RED UGOVOR ILI TENDER NA OSNOVU KOJEG JE FIKSNA CIJENA -->
    </div> 
  </div>


  <div class="row" style="display: ${ kalk_type == `offer_kalk` ? "flex" : "none" }" >
    <div class="col-md-12 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_add_mark_or_disc_contract`, valid.add_mark_or_disc_contract, null, "", `pointer-events: all !important;` ) }
    </div>
  </div>

  <div class="row" style="display: ${ kalk_type == `offer_kalk` ? "flex" : "none" }" >
    <div  class="col-md-12 col-sm-12 cit_result_table mark_disc_contract_box result_table_inside_gui" 
          id="${kalk.kalk_sifra}_mark_disc_contract_box" style="padding-top: 0; background: transparent; pointer-events: all;">
      <!-- OVDJE SAMO JEDAN RED UGOVOR ILI TENDER NA OSNOVU KOJEG JE FIKSNA MARŽA ILI POPUST -->
    </div> 
  </div>


  <div class="row" >

    <div class="col-md-1 col-sm-12">
      <button 
      data-toggle="tooltip" data-placement="top" data-html="true" title="Izračunaj prirez!"
      class="cit_tooltip blue_btn btn_small_text kalk_prirez_btn" style="margin: 17px auto 0 10px; box-shadow: none;">
        PRIREZ
      </button>
    </div>


    <div class="col-md-1 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_kalk_polov`, valid.kalk_polov, kalk.kalk_polov) }
    </div>

    <div class="col-md-1 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_prirez_val`, valid.prirez_val, kalk.prirez_val || "" ) }
    </div>

    <div class="col-md-1 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_prirez_kontra`, valid.prirez_kontra, kalk.prirez_kontra || "" ) }
    </div>

    <div class="col-md-1 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_kalk_naklada`, valid.kalk_naklada, kalk.kalk_naklada ) }
    </div>

    <div class="col-md-1 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_kalk_ploca_mat`, valid.kalk_ploca_mat, kalk.kalk_ploca_mat, kalk.kalk_ploca_mat?.naziv || "" ) }
    </div>  

    <div class="col-md-1 col-sm-12">

      <button 
        data-kalk_sifra="${kalk.kalk_sifra}" 
        data-kalk_type="${kalk_type}" 
        class="blue_btn btn_small_text find_best_plate" style="margin: 17px auto 0; box-shadow: none;">
        PRONAĐI PLOČE
      </button>

    </div>
    
    
    <div class="col-md-1 col-sm-12">
        <button 
        data-kalk_sifra="${kalk.kalk_sifra}" 
        data-kalk_type="${kalk_type}" 
        class="blue_btn btn_small_text estimate_skart_btn" style="margin: 17px auto 0; box-shadow: none;">
        PRIKAŽI ŠKART %
      </button>
      
    </div> 
    
            
<!--    

    <div class="col-md-1 col-sm-12">
        <button 
        data-kalk_sifra="${kalk.kalk_sifra}" 
        data-kalk_type="${kalk_type}" 
        class="blue_btn btn_small_text add_extra_btn" style="margin: 17px auto 0; box-shadow: none;">
        DODAJ EXTRA ZA ŠKART
      </button>
      
    </div>  
    -->
    
      <div class="col-md-1 col-sm-12">
        <button 
        data-kalk_sifra="${kalk.kalk_sifra}" 
        data-kalk_type="${kalk_type}" 
        class="blue_btn btn_small_text add_extra_reverse_btn" style="margin: 17px auto 0; box-shadow: none;">
        EXTRA ZA ŠKART
      </button>
      
    </div> 


    ${ reserv_switches }

    ${ rn_switch }



  </div>


  <div class="row" >   

    <div class="col-md-1 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_for_proces`, valid.for_proces,  "" ) }
    </div>

    <div class="col-md-11 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_add_radna_stanica`, valid.add_radna_stanica, null, "" ) }
    </div> 
  </div>

  <!-- ${kalk_type == 'pro_kalk' ? 'pointer-events: none;' : '' } -->

  <div class="row" >

    <div class="col-md-1 col-sm-12">
      ${ cit_comp( kalk.kalk_sifra+`_broj_klapni`, valid.broj_klapni, kalk.broj_klapni) }
    </div>

    <div class="col-md-3 col-sm-12">
      <button 
        data-kalk_sifra="${kalk.kalk_sifra}" 
        data-kalk_type="${kalk_type}" 
        class="blue_btn btn_small_text kalk_mont_plates_btn" style="margin: 17px auto 0; box-shadow: none;">
        KALKULACIJA MONTAŽNIH PLOČA
      </button>
    </div>


    <div class="col-md-2 col-sm-12">
      ${ cit_comp( kalk.kalk_sifra+`_allow_mont_visak`, valid.allow_mont_visak, kalk.allow_mont_visak) }
    </div>


    <div class="col-md-3 col-sm-12">
      <button 
        data-kalk_sifra="${kalk.kalk_sifra}" 
        data-kalk_type="${kalk_type}" 
        class="blue_btn btn_small_text schema_procesa_btn" style="margin: 17px auto 0; box-shadow: none;">
        SCHEMA PROCESA U OVOJ KALK.
      </button>




    </div>


  </div>


  <div class="row" >

    <div class="col-md-1 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_kalk_final_unit_time`, valid.kalk_final_unit_time, kalk.kalk_final_unit_time || "") }
    </div>

    <div  class="col-md-1 col-sm-12 cit_tooltip" 
          data-toggle="tooltip" data-placement="top" data-html="true" 
          title="${ kalk.kalk_final_time ? cit_h(kalk.kalk_final_time) : "" }" >
      ${ cit_comp(kalk.kalk_sifra+`_kalk_final_time`, valid.kalk_final_time, kalk.kalk_final_time || "") }
    </div>

    <div class="col-md-1 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_kalk_final_unit_price`, valid.kalk_final_unit_price, kalk.kalk_final_unit_price || "") }
    </div>
    <div class="col-md-1 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_kalk_final_sum_price`, valid.kalk_final_sum_price, kalk.kalk_final_sum_price || "" ) }
    </div>


    <div class="col-md-1 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_kalk_discount`, valid.kalk_discount, kalk.kalk_discount || "", null, fixed_style ) }
    </div>
    <div class="col-md-1 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_kalk_markup`, valid.kalk_markup, kalk.kalk_markup || "", null, fixed_style ) }
    </div>
    <div class="col-md-1 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_kalk_discount_money`, valid.kalk_discount_money, kalk.kalk_discount_money || "", null, fixed_style ) }
    </div>
    <div class="col-md-1 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_kalk_markup_money`, valid.kalk_markup_money, kalk.kalk_markup_money || "", null, fixed_style  ) }
    </div>



    <div class="col-md-2 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_kalk_final_unit_price_dis_mark`, valid.kalk_final_unit_price_dis_mark, kalk.kalk_final_unit_price_dis_mark || "") }
    </div>


    <div class="col-md-2 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_kalk_final_sum_price_dis_mark`, valid.kalk_final_sum_price_dis_mark, kalk.kalk_final_sum_price_dis_mark || "") }
    </div>


  </div>

  <div class="row" style="margin-bottom: 30px; position: relative;">


    <div class="up_down_kalk_scroll_box">

        <div  id="${kalk.kalk_sifra}_scroll_kalk_up"
              data-kalk_sifra="${kalk.kalk_sifra}"
              data-kalk_type="${kalk_type}"
              class="blue_btn scroll_kalk_up"
              style="pointer-events: all;" >

          <i class="fal fa-angle-up"></i>
        </div>


        <div  id="${kalk.kalk_sifra}_scroll_kalk_down"
              data-kalk_sifra="${kalk.kalk_sifra}"
              data-kalk_type="${kalk_type}"
              class="blue_btn scroll_kalk_down"
              style="pointer-events: all;" >

          <i class="fal fa-angle-down"></i>
        </div>

    </div>





    <div class="col-md-1 col-sm-12">
      ${ cit_comp(kalk.kalk_sifra+`_kalk_final_alat_price`, valid.kalk_final_alat_price, kalk.kalk_final_alat_price || "") }
    </div>

    <!--
    
    ZA SADA SAM SAKRIO OVAJ GUMB !!!!!!!!
    
    TODO -------- VJEROJATNO KASNIJE OVAJ GUMB TREBAM PRIKAZATI !!!!!!!! :) 
    
    <div class="col-md-11 col-sm-12">
      <button 
        data-kalk_sifra="${kalk.kalk_sifra}" 
        data-kalk_type="${kalk_type}" 
        class="blue_btn btn_small_text check_kalk_btn" style="margin: 17px auto 0; box-shadow: none; background: #000;">
        !!! PROVJERI STANJA ROBE !!!
      </button>
    </div>
    -->


    <div class="col-md-11 col-sm-12">


    </div>



  </div>


<!--container-fluid-->

  <div id="${kalk.kalk_sifra}_proces_scroll_box" class="proces_scroll_box">

    <div class="kalk_arrow_container">
      <div  id="${kalk.kalk_sifra}_scroll_kalk_left"
            data-kalk_sifra="${kalk.kalk_sifra}"
            data-kalk_type="${kalk_type}"
            class="scroll_kalk_arrow cit_left"
            style="pointer-events: all;" >

        <i class="fal fa-angle-left"></i>
      </div>
    </div>

    <div id="${kalk.kalk_sifra}_center_scroll_box" class="center_scroll_box">

      ${ make_all_procesi( kalk.procesi, kalk ) }


    </div>  

    <div class="kalk_arrow_container">

      <div  id="${kalk.kalk_sifra}_scroll_kalk_right"
            data-kalk_sifra="${kalk.kalk_sifra}"
            data-kalk_type="${kalk_type}"
            class="scroll_kalk_arrow cit_right" 
            style="pointer-events: all;" >

        <i class="fal fa-angle-right"></i>
      </div>

    </div>



  </div>

  </div> 
  <!-- KRAJ CIJELE KALKULACIJE ( npr jedna plava traka) -->


  `;


      all_kalks += kalk_html;


      wait_for( `$("#${kalk.kalk_sifra}_add_radna_stanica").length > 0`, function() {

        this_module.make_ugovor_or_tender_list(data, kalk);

        this_module.make_mark_disc_contract_list(data, kalk);

        register_new_kalk_events( this_module, data, kalk.kalk_sifra, kalk_type );

        $('.cit_tooltip').tooltip();

      }, 20*1000 );  



    }); // kraj loopa po kalks


    // postavljam scroll top i scroll left 
    setTimeout( function() {

      if ( start_kalk_sifra ) {
        
        
        // nemoj naštimavati scroll ili focus na polje ako je user kliknuo na strelicu za sažimanje kalka
        if ( window.kalk_arrow_is_clicked == true ) {
          // vrati nazad stanje
          window.kalk_arrow_is_clicked = false;
          return;
        };
        

        $(`#` + start_kalk_sifra + `_center_scroll_box`)[0].scrollTop = scroll_top;
        $(`#` + start_kalk_sifra + `_center_scroll_box`)[0].scrollLeft = scroll_left;

        // vrati fokus nazad na polje na kojem je kursor bio kada je  make kalks počeo !!!
        if ( kalk_input_elem_id ) {

          
          $(`#`+kalk_input_elem_id).trigger(`click`); 
          $(`#`+kalk_input_elem_id).trigger(`focus`);
          $(`#`+kalk_input_elem_id).trigger(`select`);
          
          
        }; // ako sam napravio na početku ----->  kalk input elem_id

      }; // ako sam dobio kalk sifru na početku make kalks

    }, 0 );


    return all_kalks;
    
    
  };
  this_module.run_generate_kalk = run_generate_kalk;
  

  function make_kalk_proces( data, proces, kalk, kalk_type, tip_proizvoda, proces_index ) {

    var valid = this_module.valid;
    
    function make_proces_ulazi() {
      
      var ulazi_headers = 

  `
  <div class="row ulazi_headers" >
    
    <div class="kalk_cell" style="flex: 0;">

      <button 
        data-row_sifra="${proces.row_sifra}"
        data-kalk_sifra="${kalk.kalk_sifra}"
        data-kalk_type="${kalk_type}"

        class="kalk_btn cit_tooltip new_kalk_ulaz_btn" 
        style="background: #3f6ad8; margin: 0;"
        data-toggle="tooltip" data-placement="top" data-html="true" title="Dodaj novi ulaz!" >
        <i class="fas fa-plus"></i>
      </button>

    </div>
    
    
    <div class="kalk_cell" style="flex: 1;">
      UK. CIJENA
    </div>
    
    <div class="kalk_cell" style="flex: 1;">
      JED. CIJENA
    </div>
    
    <div class="kalk_cell" style="flex: 3;">
      SIROVINA
    </div>
    
    <div class="kalk_cell" style="flex: 3;">
      FORMA
    </div>
    
    <div class="kalk_cell" style="flex: 3;">
      ELEMENT
    </div>
    
    <div class="kalk_cell" style="flex: 1">
      MATER.
    </div>
    
    <div class="kalk_cell" style="flex: 1;">
     JED. KOL.
    </div>
    
    <div class="kalk_cell" style="flex: 1;">
      KOLIČINA
    </div> 
    
    <div class="kalk_cell" style="flex: 1;">
      EXTRA. KOL.
    </div> 
    
    <div class="kalk_cell" style="flex: 1;">
      VAL
    </div> 
      
    <div class="kalk_cell" style="flex: 1;">
      KONT. VAL
    </div>

    <div class="kalk_cell" style="flex: 1;">
      NOŽ/BIG
    </div>  
    
    <div class="kalk_cell" style="flex: 1;">
      GNJEZDA
    </div> 
    
    
    

    <div class="kalk_cell" style="width: 80px;">
      LJEP. / mm 
    </div>
    
    <div class="kalk_cell" style="width: 80px;">
      TOČAKA LJEP. 
    </div>
    
    <div class="kalk_cell" style="width: 80px;">
      LJEP. BRZINA mm/s 
    </div>
    
    <div class="kalk_cell" style="width: 100px;">
      LJEP. TLAK
    </div>
    
    <div class="kalk_cell" style="width: 100px;">
      LJEP. DIZNA
    </div>
    
    <div class="kalk_cell" style="width: 100px;">
      MAX. VELIČINA
    </div> 
   
   
   
    <div class="kalk_cell" style="flex: 1;">
      CYAN
    </div>   
    <div class="kalk_cell" style="flex: 1;">
      MAGENTA
    </div>   
    <div class="kalk_cell" style="flex: 1;">
      YELLOW
    </div>   
    <div class="kalk_cell" style="flex: 1;">
      KEY
    </div> 
    
    <div class="kalk_cell" style="flex: 1;">
      WHITE
    </div> 
    
    <div class="kalk_cell" style="flex: 1;">
      VARNISH
    </div> 
    
   
    
    
  </div>

  `;
        
        
  var ulazi_footer = 
        
`
<div  class="row" >

  <div class="col-md-12 col-sm-12" style="display: flex; align-items: center; justify-content: center;" >

    <button 
      data-toggle="tooltip" data-placement="top" data-html="true" title="Dodaj novi ulaz"
      
      data-row_sifra="${proces.row_sifra}"
      data-kalk_sifra="${kalk.kalk_sifra}"
      data-kalk_type="${kalk_type}"
      
      class="kalk_btn_circle cit_tooltip new_kalk_ulaz_btn" style="background: #3f6ad8;">
      <i class="fas fa-plus"></i>

    </button>

  </div>

</div>        
        
`;      


      
      var ulazi_html = ``;
      
      $.each(proces.ulazi, function(u_ind, ulaz) {
        
        // prvo provjeri jel OVERRIDE cijena u extra data
        // prvo provjeri jel OVERRIDE cijena u extra data
        
        var ulaz_price_value = null;
        
        var extra_ulaz_price = find_extra_data_ulaz_price( proces.extra_data, ulaz.sifra );
        
        // ako IMAM EXTRA CIJENU
        if ( extra_ulaz_price !== null ) {
          ulaz_price_value = extra_ulaz_price;
        } else {
          // ako NEMAM EXTRA CIJENU ONDA UZMI KALK PRICE
          ulaz_price_value = ulaz.kalk_price !== null ? ulaz.kalk_price : ``;
        };
        
        
        ulazi_html += 
  `
  <div  class="row ulaz_row ${ ulaz.deficit == true ? 'cit_deficit' : '' }"
        data-ulaz_sifra="${ulaz.sifra}"
        data-row_sifra="${proces.row_sifra}"
        data-kalk_sifra="${kalk.kalk_sifra}"
        data-kalk_type="${kalk_type}" 
        >
      
      
      
    <div class="kalk_cell" style="flex: 0;">
      <button 
        data-ulaz_sifra="${ulaz.sifra}" 
        data-row_sifra="${proces.row_sifra}"
        data-kalk_sifra="${kalk.kalk_sifra}"
        data-kalk_type="${kalk_type}"
        class="kalk_btn delete_ulaz cit_tooltip" style="background: #bf0060; margin: 0;"
        data-toggle="tooltip" data-placement="top" data-html="true" title="Obriši ovaj ulaz!" >
        <i class="fas fa-minus"></i>
      </button>
     </div> 
     
     
    <div class="kalk_cell" style="flex: 1;" style="flex: 1; pointer-events: ${ ulaz.izlaz_sifra ? "none;": "all;" }">
      ${ cit_comp(
          kalk.kalk_rand  + `_` +  ulaz.sifra + `_ulaz_price`, 
          valid.ulaz_price,
          ulaz_price_value,
          null,
          ( extra_ulaz_price !== null ? "background: #ffc107;" : "" )
        )}
    </div> 
     
    <div class="kalk_cell" style="flex: 1;" style="flex: 1; pointer-events: ${ ulaz.izlaz_sifra ? "none;": "all;" }">
      ${ cit_comp(kalk.kalk_rand  + `_` + ulaz.sifra+`_ulaz_unit_price`, valid.ulaz_unit_price, (ulaz.real_unit_price || ulaz.kalk_unit_price || "") ) }
    </div>
     
      
    <div  class="kalk_cell cit_tooltip" 
          style="flex: 3;" 
          data-toggle="tooltip"
          data-placement="top" data-html="true" title="${ ulaz.kalk_full_naziv || "" }"
          data-full_name="${ ulaz.kalk_full_naziv || "" }">
          
      ${ cit_comp(
          kalk.kalk_rand  + `_` + ulaz.sifra + `_kalk_find_sirov`,
          valid.kalk_find_sirov, 
          ulaz || null,
          ulaz._id ? ulaz.kalk_full_naziv || "" : ""
        ) } 
      
    </div>
    
    <div  class="kalk_cell cit_tooltip" 
          style="flex: 3;" 
          data-toggle="tooltip"
          data-placement="top" data-html="true" title="${ ulaz.kalk_element ? "" : ulaz.kalk_plate_naziv || "" }"
          data-full_name="${ ulaz.kalk_element ? "" : ulaz.kalk_plate_naziv || "" }">
          <!-- AKO U ULAZU POSTOJI KALKE ELEMENT ONDA NEMOJ PRIKAZIVATI PLATE TJ MONTAŽNU PLOČU !!!! -->
      ${ cit_comp(
          kalk.kalk_rand  + `_` + ulaz.sifra + `_kalk_find_plate`,
          valid.kalk_find_plate,
          ulaz || null,
          ulaz.kalk_element ? "" : ulaz.kalk_plate_naziv || ""
        ) } 
      
    </div>
    
    
    <div  class="kalk_cell cit_tooltip"  
          style="flex: 3;"
          data-toggle="tooltip" data-placement="top" data-html="true"
          title="${ ulaz.kalk_element?.elem_opis || "" }"
          data-full_name="${ ulaz.kalk_element?.elem_opis || "" }" >

      ${ cit_comp(kalk.kalk_rand  + `_` + ulaz.sifra + `_ulaz_element`, valid.ulaz_element, ulaz || null, ulaz.kalk_element?.elem_opis || "" ) } 
    </div>
    
    
    <div class="kalk_cell " style="flex: 1;" >
      ${ cit_comp(kalk.kalk_rand  + `_` + ulaz.sifra + `_ulaz_mat`, valid.ulaz_mat, ulaz || null, get_ulaz_mat(ulaz)?.naziv || "" ) }
    </div>
    
    <!--
    <div class="kalk_cell" style="flex: 1;" style="pointer-events: ${ ulaz.izlaz_sifra ? "none;": "all;" }">
      ${ cit_comp(kalk.kalk_rand  + `_` + ulaz.sifra+`_ulaz_kom_in_product`, valid.ulaz_kom_in_product, ulaz.kalk_kom_in_product || "") }
    </div>
    -->

    <div class="kalk_cell" style="flex: 1;" style="flex: 1; pointer-events: ${ ulaz.izlaz_sifra ? "none;": "all;" }">
      ${ cit_comp(kalk.kalk_rand  + `_` + ulaz.sifra+`_ulaz_kom_in_sirovina`, valid.ulaz_kom_in_sirovina, ulaz.kalk_kom_in_sirovina || 1 ) }
    </div>

    <div class="kalk_cell" style="flex: 1;" style="flex: 1; pointer-events: ${ ulaz.izlaz_sifra ? "none;": "all;" }">
      ${ cit_comp(kalk.kalk_rand  + `_` +  ulaz.sifra+`_ulaz_sum_sirovina`, valid.ulaz_sum_sirovina, ulaz.kalk_sum_sirovina || "") }
    </div> 
    
    <div class="kalk_cell" style="flex: 1;" style="flex: 1; pointer-events: ${ ulaz.izlaz_sifra ? "none;": "all;" }">
      ${ cit_comp(kalk.kalk_rand  + `_` +  ulaz.sifra+`_ulaz_extra_kom`, valid.ulaz_extra_kom, ulaz.kalk_extra_kom || "") }
    </div> 

    <div class="kalk_cell" style="flex: 1;" style="flex: 1; pointer-events: ${ ulaz.izlaz_sifra ? "none;": "all;" }">
      ${ cit_comp(kalk.kalk_rand  + `_` + ulaz.sifra+`_ulaz_val`, valid.ulaz_val, ulaz.kalk_po_valu || "" ) }
    </div>
    
    <div class="kalk_cell" style="flex: 1;" style="flex: 1; pointer-events: ${ ulaz.izlaz_sifra ? "none;": "all;" }">
      ${ cit_comp(kalk.kalk_rand  + `_` + ulaz.sifra+`_ulaz_kontra`, valid.ulaz_kontra, ulaz.kalk_po_kontravalu || "" ) }
    </div>


    <div class="kalk_cell" style="flex: 1;" style="flex: 1; pointer-events: ${ ulaz.izlaz_sifra ? "none;": "all;" }">
      ${ cit_comp(kalk.kalk_rand  + `_` + ulaz.sifra+`_ulaz_noz_big`, valid.ulaz_noz_big, ulaz.kalk_noz_big || "" ) }
    </div>
    
    <div class="kalk_cell" style="flex: 1;" style="flex: 1; pointer-events: ${ ulaz.izlaz_sifra ? "none;": "all;" }">
      ${ cit_comp(kalk.kalk_rand  + `_` + ulaz.sifra+`_kalk_nest_count`, valid.kalk_nest_count, ulaz.kalk_nest_count || "" ) }
    </div>

    
    <div  class="kalk_cell cit_tooltip" style="width: 80px;" >
      ${ cit_comp( kalk.kalk_rand + `_` + ulaz.sifra + `_kalk_glue_mm`, valid.kalk_glue_mm, ulaz.kalk_glue_mm || 0 ) }
    </div>  
    
    <div  class="kalk_cell cit_tooltip" style="width: 80px;" >
      ${ cit_comp( kalk.kalk_rand + `_` + ulaz.sifra + `_kalk_glue_points`, valid.kalk_glue_points, ulaz.kalk_glue_points || 1 ) }
    </div>  
    
        
    <div  class="kalk_cell cit_tooltip" style="width: 80px;" >
      ${ cit_comp( kalk.kalk_rand + `_` + ulaz.sifra + `_kalk_glue_speed`, valid.kalk_glue_speed, ulaz.kalk_glue_speed || 0 ) }
    </div> 
    
    <div  class="kalk_cell cit_tooltip" style="width: 100px;" >
      ${ cit_comp(
        kalk.kalk_rand + `_` + ulaz.sifra + `_kalk_glue_tlak`, 
        valid.kalk_glue_tlak,
        ulaz.kalk_glue_tlak || null,
        ulaz.kalk_glue_tlak?.naziv || ""
      ) }
    </div> 
  
    <div  class="kalk_cell cit_tooltip" style="width: 100px;" >
      ${ cit_comp(
        kalk.kalk_rand  + `_` + ulaz.sifra + `_kalk_glue_dizna`, 
        valid.kalk_glue_dizna,
        ulaz.kalk_glue_dizna || null,
        ulaz.kalk_glue_dizna?.naziv || ""
      ) }
    </div> 
    
    <div  class="kalk_cell cit_tooltip" style="width: 100px;" >
      ${ cit_comp(
        kalk.kalk_rand  + `_` + ulaz.sifra + `_kalk_glue_m2`, 
        valid.kalk_glue_m2,
        ulaz.kalk_glue_m2 || null,
        ulaz.kalk_glue_m2?.naziv || ""
      ) }
    </div>
    
    <div class="kalk_cell" style="flex: 1;" style="flex: 1; pointer-events: ${ ulaz.izlaz_sifra ? "none;": "all;" }">
      ${ cit_comp(kalk.kalk_rand  + `_` + ulaz.sifra+`_kalk_color_C`, valid.kalk_color_C, ulaz.kalk_color_C || "" ) }
    </div>
    <div class="kalk_cell" style="flex: 1;" style="flex: 1; pointer-events: ${ ulaz.izlaz_sifra ? "none;": "all;" }">
      ${ cit_comp(kalk.kalk_rand  + `_` + ulaz.sifra+`_kalk_color_M`, valid.kalk_color_M, ulaz.kalk_color_M || "" ) }
    </div>
    <div class="kalk_cell" style="flex: 1;" style="flex: 1; pointer-events: ${ ulaz.izlaz_sifra ? "none;": "all;" }">
      ${ cit_comp(kalk.kalk_rand  + `_` + ulaz.sifra+`_kalk_color_Y`, valid.kalk_color_Y, ulaz.kalk_color_Y || "" ) }
    </div>
    <div class="kalk_cell" style="flex: 1;" style="flex: 1; pointer-events: ${ ulaz.izlaz_sifra ? "none;": "all;" }">
      ${ cit_comp(kalk.kalk_rand  + `_` + ulaz.sifra+`_kalk_color_K`, valid.kalk_color_K, ulaz.kalk_color_K || "" ) }
    </div>
    <div class="kalk_cell" style="flex: 1;" style="flex: 1; pointer-events: ${ ulaz.izlaz_sifra ? "none;": "all;" }">
      ${ cit_comp(kalk.kalk_rand  + `_` + ulaz.sifra + `_kalk_color_W`, valid.kalk_color_W, ulaz.kalk_color_W || "" ) }
    </div>
    <div class="kalk_cell" style="flex: 1;" style="flex: 1; pointer-events: ${ ulaz.izlaz_sifra ? "none;": "all;" }">
      ${ cit_comp(kalk.kalk_rand  + `_` + ulaz.sifra + `_kalk_color_V`, valid.kalk_color_V, ulaz.kalk_color_V || "" ) }
    </div>
    
  </div>
  

  `;
    
        // odabrao sam ovaj input bezveze  - bitno je da se pojavi u html-u ---> to mi je triger da registriram evente !!!
        wait_for( `$("#${kalk.kalk_rand}_${ulaz.sifra}_kalk_find_sirov").length > 0`, function() {
          this_module.register_kalk_ulaz_events(data, ulaz.sifra, proces, kalk, kalk_type, tip_proizvoda );
        }, 5*1000 ); 

        
        
      }); // kraj loopa po svim ulazima u ovom procesu

      return ulazi_headers + ulazi_html; //  + ulazi_footer;

    };

    function make_action_data() {
      
      
    
    var action_headers = 
`
  <div class="row action_headers">
  
  
    <div class="kalk_cell" style="flex: 0;">
    
    <button 
      data-toggle="tooltip" data-placement="top" data-html="true" title="Dodaj radnika"
      data-row_sifra="${proces.row_sifra}"
      data-kalk_sifra="${kalk.kalk_sifra}"
      data-kalk_type="${kalk_type}"
      class="kalk_btn cit_tooltip new_kalk_worker_btn" style="background: #3f6ad8;">
      <i class="fas fa-user-hard-hat"></i>

    </button>
    
    
    
    </div>
    
    
    <div class="kalk_cell" style="width: 100px;">
      CIJENA PRIPREMA+RAD
    </div>
    
    <div class="kalk_cell" style="width: 100px;">
      JED. CIJENA
    </div>
    
    <div class="kalk_cell" style="width: 100px;">
      JED. VRIJEME / min
    </div>
  
  
    <div class="kalk_cell" style="width: 500px;">
      PROCES
    </div>
    

    <div class="kalk_cell" style="width: 100px;">
      TRAJANJE<br>PRIPREME / min
    </div>

    <div class="kalk_cell" style="width: 100px;">
      TRAJANJE<br>RADA / min
    </div> 
    
    <div class="kalk_cell" style="width: 100px;">
      SATNICA<br>PRIPREME
    </div> 
    
    <div class="kalk_cell" style="width: 100px;">
      SATNICA<br>RADA STROJA
    </div> 
    
    <div class="kalk_cell" style="width: 100px;">
     BROJ<br>RADNIKA - PRIPREMA
    </div> 
    
    <div class="kalk_cell" style="width: 100px;">
     BROJ<br>RADNIKA - RAD
    </div> 
    
    <div class="kalk_cell" style="width: 100px;">
     SATNICA<br>RADNIKA (osnovna)
    </div> 
    
    <div class="kalk_cell" style="width: 100px;">
      SCITEX<br>MULTILOAD
    </div>
    
    <div class="kalk_cell" style="width: 100px;">
     KAŠIRIRANIH<br>ARAKA
    </div>
    


  </div>

`;  
        

  var workers_footer = 
        
`
<div class="row">

  <div class="col-md-12 col-sm-12" style="display: flex; align-items: center; justify-content: center;" >

    <button 
      data-toggle="tooltip" data-placement="top" data-html="true" title="Dodaj radnika"
      data-row_sifra="${proces.row_sifra}"
      data-kalk_sifra="${kalk.kalk_sifra}"
      data-kalk_type="${kalk_type}"
      class="kalk_btn_circle cit_tooltip new_kalk_worker_btn" style="background: #3f6ad8;">
      <!-- <i class="fas fa-plus"></i> -->
      <i class="fas fa-user-hard-hat"></i>

    </button>

  </div>

</div>        
        
`;     

      
      // ------------------ STROJ SATNICA ------------------
      var satnica_stroj_value = null; 
      if ( cit_number(proces.extra_data?.satnica_stroj) !== null ) {
        satnica_stroj_value = proces.extra_data.satnica_stroj;
      };
      if ( !satnica_stroj_value && cit_number(proces.action?.prices?.stroj) !== null ) {
        satnica_stroj_value = proces.action.prices.stroj;
      };
      
      // ------------------ PREP SATNICA ------------------
      var satnica_prep_value = null; 
      if ( cit_number(proces.extra_data?.satnica_prep) !== null ) {
        satnica_prep_value = proces.extra_data.satnica_prep;
      };
      if ( !satnica_prep_value && cit_number(proces.action?.prices?.prep) !== null ) {
        satnica_prep_value = proces.action.prices.prep;
      };
      
    
      // ------------------ RADNIK SATNICA ------------------
      var satnica_radnik_value = null; 
      if ( cit_number(proces.extra_data?.satnica_radnik) !== null ) {
        satnica_radnik_value = proces.extra_data.satnica_radnik;
      };
      if ( !satnica_radnik_value && cit_number(proces.action?.prices?.radnik) !== null ) {
        satnica_radnik_value = proces.action.prices.radnik;
      };

      
          
      // ------------------ BROJ RADNIKA NA POSLU------------------
      var radnik_count_value = null; 
      if ( cit_number(proces.extra_data?.radnik_count) !== null ) {
        radnik_count_value = proces.extra_data.radnik_count;
      };
      if ( !radnik_count_value && cit_number(proces.action?.radnik_count) !== null ) {
        radnik_count_value = proces.action.radnik_count;
      };

      
      // ------------------ BROJ RADNIKA NA POSLU------------------
      var radnik_count_value_prep = null; 
      if ( cit_number(proces.extra_data?.radnik_count_prep ) !== null ) {
        radnik_count_value_prep = proces.extra_data.radnik_count_prep;
      };
      if ( !radnik_count_value_prep && cit_number(proces.action?.radnik_count_prep) !== null ) {
        radnik_count_value_prep = proces.action.radnik_count_prep;
      };
      
      
      
      // ------------------  VRIJEME RADA ------------------
      var work_time_value = null; 
      if ( cit_number(proces.extra_data?.action_work_time ) !== null ) {
        work_time_value = proces.extra_data.action_work_time;
      };
      if ( !work_time_value && cit_number(proces.action?.action_work_time) !== null ) {
        work_time_value = proces.action.action_work_time;
      };
      
      
      // ------------------  VRIJEME PRIPREME ------------------
      var prep_time_value = null; 
      if ( cit_number(proces.extra_data?.action_prep_time ) !== null ) {
        prep_time_value = proces.extra_data.action_prep_time;
      };
      if ( !prep_time_value && cit_number(proces.action?.action_prep_time) !== null ) {
        prep_time_value = proces.action.action_prep_time;
      };
      
      
      
      // ------------------  JEDINIČNO VRIJEME  ------------------
      var unit_time_value = null; 
      if ( cit_number(proces.extra_data?.action_unit_time ) !== null ) {
        unit_time_value = proces.extra_data.action_unit_time;
      };
      if ( !unit_time_value && cit_number(proces.action?.action_unit_time) !== null ) {
        unit_time_value = proces.action.action_unit_time;
      };
      
      
      
      var action_html =
  `
  <div  class="row action_row"
        data-row_sifra="${proces.row_sifra}"
        data-kalk_sifra="${kalk.kalk_sifra}"
        data-kalk_type="${kalk_type}" >
  
  
    <div class="kalk_cell" style="width: 100px; margin-left: 26px;" >
      ${ cit_comp( kalk.kalk_rand + `_` + proces.action.action_sifra + `_action_est_price`, valid.action_est_price, proces.action.action_est_price || "") }
    </div>
    
    <div class="kalk_cell" style="width: 100px;">
      ${ cit_comp( kalk.kalk_rand + `_` + proces.action.action_sifra + `_action_unit_price`, valid.action_unit_price, proces.action.action_unit_price || "") }
    </div>
    
    <div class="kalk_cell" style="width: 100px;">
      ${ cit_comp( 
        kalk.kalk_rand + `_` + proces.action.action_sifra + `_action_unit_time`,
        valid.action_unit_time, 
        unit_time_value || "",
        null,
        null
      ) }
    </div>
    
  
  
    <div  class="kalk_cell cit_tooltip" 
          style="width: 500px;" 
          data-toggle="tooltip" data-placement="top" data-html="true" title="${ proces.action.action_radna_stn?.naziv || "" }">

      ${ cit_comp(
        kalk.kalk_rand + `_` + proces.action.action_sifra+`_action_radna_stn`,
        valid.action_radna_stn, 
        proces.action.action_radna_stn || null,
        proces.action.action_radna_stn?.naziv || ""
      ) } 
      
    </div>
    
    
    <div class="kalk_cell" style="width: 100px;">
      ${ cit_comp( 
        kalk.kalk_rand + `_` + proces.action.action_sifra + `_action_prep_time`,
        valid.action_prep_time, 
        prep_time_value || "",
        null,
        ( proces.extra_data?.action_prep_time ? "background: #ffc107;" : "" )
      ) }
    </div>
  
    <div  class="kalk_cell cit_tooltip" style="width: 100px;" 
          data-toggle="tooltip" data-placement="top" data-html="true" 
          title="${ proces.action.action_work_time ? cit_h(proces.action.action_work_time) : "" }" >
      ${ cit_comp( 
        kalk.kalk_rand + `_` + proces.action.action_sifra + `_action_work_time`,
        valid.action_work_time,
        work_time_value || "",
        null,
        ( proces.extra_data?.action_work_time ? "background: #ffc107;" : "" )
      )}
    </div>

    <div  class="kalk_cell cit_tooltip" style="width: 100px;" >
      ${ cit_comp( 
        kalk.kalk_rand + `_` + proces.action.action_sifra + `_satnica_prep`,
        valid.satnica_prep,
        satnica_prep_value
      ) }
    </div>
    
    <div  class="kalk_cell cit_tooltip" style="width: 100px;" >
      ${ cit_comp( 
        kalk.kalk_rand + `_` + proces.action.action_sifra + `_satnica_stroj`,
        valid.satnica_stroj,
        satnica_stroj_value
      ) }
    </div>
    
    <div  class="kalk_cell cit_tooltip" style="width: 100px;" >
      ${ cit_comp( 
        kalk.kalk_rand + `_` + proces.action.action_sifra + `_radnik_count`,
        valid.radnik_count, 
        radnik_count_value
      ) }
    </div>    
    
    
    <div  class="kalk_cell cit_tooltip" style="width: 100px;" >
      ${ cit_comp( 
        kalk.kalk_rand + `_` + proces.action.action_sifra + `_radnik_count_prep`,
        valid.radnik_count_prep, 
        radnik_count_value_prep
      ) }
    </div>  
    
        
    <div  class="kalk_cell cit_tooltip" style="width: 100px;" >
      ${ cit_comp( 
        kalk.kalk_rand + `_` + proces.action.action_sifra + `_satnica_radnik`,
        valid.satnica_radnik, 
        satnica_radnik_value
      ) }
    </div>
    
    
    
    <div  class="kalk_cell cit_tooltip" style="width: 100px;" >
      ${ cit_comp( kalk.kalk_rand + `_` + proces.action.action_sifra + `_action_multiload`, valid.action_multiload, proces.action.action_multiload || 1 ) }
    </div>
    
    
    <div  class="kalk_cell cit_tooltip" style="width: 100px;" >
      ${ cit_comp( kalk.kalk_rand + `_` + proces.action.action_sifra + `_action_kasir_num`, valid.action_kasir_num, proces.action.action_kasir_num || 1 ) }
    </div>
    

  </div>
  


  `;
      
      
      wait_for( `$("#${kalk.kalk_rand}_${proces.action.action_sifra}_action_radna_stn").length > 0`, function() {
        this_module.register_action_events( kalk, proces.action.action_sifra, proces.row_sifra, kalk.kalk_sifra, kalk_type );
      }, 5*1000 );   
      
      return action_headers + action_html;  //  + workers_footer;

    }; // kraj make_action_data
    
    function make_proces_izlazi() {
      
      
    var izlazi_headers = 
    
  `
  <div class="row izlazi_headers">
    
    <div class="kalk_cell" style="flex: 0;">
      <button 
        data-toggle="tooltip" data-placement="top" data-html="true" title="Dodaj novi izlaz"

        data-row_sifra="${proces.row_sifra}"
        data-kalk_sifra="${kalk.kalk_sifra}"
        data-kalk_type="${kalk_type}"

        class="kalk_btn cit_tooltip new_kalk_izlaz_btn" style="background: #3f6ad8;">
        <i class="fas fa-plus"></i>

      </button>
    
    </div>
    
    
    
    <div class="kalk_cell" style="width: 500px;">
    IZLAZ
    </div>
    
      
    <div class="kalk_cell" style="width: 160px;">
      ELEM U PROIZVODU
    </div>
    
    <div class="kalk_cell" style="width: 160px;">
      ELEM IZ PLOČE
    </div>

    <div class="kalk_cell" style="width: 160px;">
     KOMADA
    </div> 
    
    
    <div class="kalk_cell" style="width: 160px;">
      PO VALU
    </div> 
      
    <div class="kalk_cell" style="width: 160px;">
      PO KONTRAVALU
    </div>
    
     
    <div class="kalk_cell" style="flex: 0;">
    <!-- OVO JE SAMO ZA GUMB DELETE IZLAZ -->
     &nbsp;
    </div> 
    

  </div>

  `;      
     
     
  var izlazi_footer = 
        
`
<div  class="row" >

  <div class="col-md-12 col-sm-12" style="display: flex; align-items: center; justify-content: center;" >

    <button 
      data-toggle="tooltip" data-placement="top" data-html="true" title="Dodaj novi izlaz"
      
      data-row_sifra="${proces.row_sifra}"
      data-kalk_sifra="${kalk.kalk_sifra}"
      data-kalk_type="${kalk_type}"
      
      class="kalk_btn_circle cit_tooltip new_kalk_izlaz_btn" style="background: #3f6ad8;">
      <i class="fas fa-plus"></i>

    </button>

  </div>

</div>        
        
`;           
     
      
      

      var izlazi_html = ``;
      
      $.each(proces.izlazi, function(u_ind, izlaz) {
        
        
        izlazi_html += 
  `
  <div  class="row izlaz_row"
        data-izlaz_sifra="${izlaz.sifra}"
        data-row_sifra="${proces.row_sifra}"
        data-kalk_sifra="${kalk.kalk_sifra}"
        data-kalk_type="${kalk_type}" >
        
        
    <div class="kalk_cell" style="flex: 0;" >

      <button 
        data-izlaz_sifra="${izlaz.sifra}" 
        data-row_sifra="${proces.row_sifra}"
        data-kalk_sifra="${kalk.kalk_sifra}"
        data-kalk_type="${kalk_type}"
        
        class="kalk_btn delete_izlaz cit_tooltip" style="background: #bf0060; margin: 0 0 0 auto;"
        data-toggle="tooltip" data-placement="top" data-html="true" title="Obriši ovaj izlaz!" >
        <i class="fas fa-minus"></i>
        
      </button>
   
    </div> 
    
    <div  class="kalk_cell cit_tooltip" 
          style="width: 500px;"  
          data-toggle="tooltip" data-placement="top" data-html="true"
          title="${ izlaz.kalk_element?.elem_opis || "" }"
          data-full_name="${ izlaz.kalk_element?.elem_opis || "" }" >
          
      ${ cit_comp(kalk.kalk_rand  + `_` + izlaz.sifra+`_izlaz_element`, valid.izlaz_element, izlaz || null, izlaz.kalk_element?.elem_opis || "" ) } 
    </div>


    <div class="kalk_cell" style="width: 160px;">
      ${ cit_comp(kalk.kalk_rand  + `_` + izlaz.sifra+`_izlaz_kom_in_product`, valid.izlaz_kom_in_product, izlaz.kalk_kom_in_product || "") }
    </div> 

    <div class="kalk_cell" style="width: 160px;">
      ${ cit_comp(kalk.kalk_rand  + `_` + izlaz.sifra+`_izlaz_kom_on_plate`, valid.izlaz_kom_on_plate, izlaz.kalk_element?.elem_plate_count || "") }
    </div>

    <div class="kalk_cell" style="width: 160px;">
     ${ cit_comp( 
          kalk.kalk_rand  + `_` + izlaz.sifra+`_izlaz_sum_sirovina`,
          valid.izlaz_sum_sirovina, 
          proces.extra_data?.izlaz_kom ? proces.extra_data?.izlaz_kom.kom : (izlaz.kalk_sum_sirovina || ""), 
          null,
          ( proces.extra_data?.izlaz_kom ? "background: #ffc107;" : "" )
        ) }
    </div>  
    
    
    <div class="kalk_cell" style="width: 160px;" >
      ${ cit_comp(kalk.kalk_rand  + `_` + izlaz.sifra+`_izlaz_val`, valid.izlaz_val, izlaz.kalk_po_valu || "" ) }
    </div>
    
    <div class="kalk_cell" style="width: 160px;" >
      ${ cit_comp(kalk.kalk_rand  + `_` + izlaz.sifra+`_izlaz_kontra`, valid.izlaz_kontra, izlaz.kalk_po_kontravalu || "" ) }
    </div>

    
  </div>

  `;
        
        
      wait_for( `$("#${kalk.kalk_rand}_${izlaz.sifra}_izlaz_element").length > 0`, function() {
        this_module.register_kalk_izlaz_events(data, izlaz.sifra, proces, kalk, kalk_type, tip_proizvoda );
      }, 5*1000 );     
        

      }); // kraj loop po svim izlazima !!!!


      return izlazi_headers + izlazi_html; //  + izlazi_footer;

    };

    var all_proces_pills = ``;
    
    if ( proces.extra_data && Object.keys( proces.extra_data ).length > 0 ) {
      
      $.each( proces.extra_data, function( extra_key, extra_value ) {
        
        if ( extra_value || extra_value === 0 || extra_value === "" ) {
          var proces_pill = make_extra_data_pill( data, kalk, kalk_type, proces_index, extra_key, extra_value );
          all_proces_pills += proces_pill;  
        };
        
      });
      
    };
    
    
    var switch_proces_radni_nalog = ``;
    
    // prikaži samo ako još uvijek nije kreiran radni nalog za 
    if ( kalk_type == `pro_kalk` && !proces.rn_created ) {
    
      switch_proces_radni_nalog = `
      <div class="col-md-1 col-sm-12 proces_radni_nalog">
        ${ cit_comp( 
          kalk.kalk_rand + `_` + proces.row_sifra + `_proces_radni_nalog`,
          valid.proces_radni_nalog, 
          proces.extra_data?.proces_radni_nalog || null,
          null,
          `filter: hue-rotate(70deg); pointer-events: all;`
        ) }
      </div>`;
      
      };
      
    
    var switch_proces_drop = ``;
    
    if ( kalk_type == `pro_kalk` ) {
      
      switch_proces_drop = `
      <div class="col-md-1 col-sm-12 proces_drop">
        ${ cit_comp( 
        kalk.kalk_rand + `_` + proces.row_sifra + `_proces_drop`,
        valid.proces_drop,
        proces.extra_data?.proces_drop || null,
        null,
        `filter: hue-rotate(160deg); pointer-events: all;` 
      ) }
      </div>`;
    
    };
    
    
    var switch_alat_apart = ``;
    
    if ( kalk_type == `offer_kalk` ) {
      
      switch_alat_apart = `
      <div class="col-md-1 col-sm-12 alat_apart">
        ${ cit_comp( 
        kalk.kalk_rand + `_` + proces.row_sifra + `_alat_apart`,
        valid.alat_apart,
        proces.extra_data?.alat_apart || null,
        null,
        `filter: hue-rotate(160deg); pointer-events: all;` 
      ) }
      </div>`;
    
    };
    
    
    
    var proc_koment_style =`
height: 34px;
min-height: 34px;
padding: 4px;
font-size: 16px;
font-weight: 700;
letter-spacing: 0.03rem;
`;


  var proces_row =

  `

  <!-- style="${kalk_type == 'pro_kalk' ? 'pointer-events: none;' : '' }" -->  
  
  <div  class="container-fluid proces_row" 
  
        data-row_sifra="${proces.row_sifra}"
        data-kalk_sifra="${kalk.kalk_sifra}"
        data-kalk_type="${kalk_type}" 
        >

    
    <div class="row">
      <div class="col-md-12 col-sm-12 extra_data_box" >
        ${ all_proces_pills }
      </div>
    </div>
  
  
    <!--
    AKO ŽELIM FORSIRATI DA JE PROC KOMENTAR FULL WIDTH 
    style="min-width: 100%;"
    -->
    
    <div class="row">
      <div class="col-md-10 col-sm-12 proc_koment_box" >
        ${ cit_comp( kalk.kalk_rand + `_` + proces.row_sifra + `_proc_koment`, valid.proc_koment, proces.proc_koment, "", proc_koment_style ) }
      </div>
      

    ${switch_proces_radni_nalog}
    ${switch_proces_drop}
    ${switch_alat_apart}
      
      
    </div>

    <div class="row">

      <div class="proces_order_number">${ proces_index + 1 + '.' + ( proces.proces_skart ? (  `PROCES - ŠKART ${ cit_format( proces.proces_skart*100, 1 ) }%`  ) : '' )  }</div>

      <div class="col-md-12 col-sm-12 kalk_ulaz_box" >

        ${ make_proces_ulazi() }

      </div>

      <div class="col-md-12 col-sm-12 kalk_action_box" >

        ${ make_action_data() }

      </div>

      <div class="col-md-12 col-sm-12 kalk_izlaz_box" >

        ${ make_proces_izlazi() }

      </div>



      <button data-row_sifra="${proces.row_sifra}"
              data-kalk_sifra="${kalk.kalk_sifra}"
              data-kalk_type="${kalk_type}"
              class="kalk_btn delete_proces_row cit_tooltip" style="background: #bf0060; margin: 0;"
              data-toggle="tooltip" data-placement="top" data-html="true" title="Obriši ovaj proces!" >

        <i class="fas fa-trash-alt"></i>

      </button>  
    
    </div>
  
      

  </div>


  `;    

    
    wait_for( `$("#${kalk.kalk_rand}_${proces.row_sifra}_proc_koment").length > 0`, function() {
      this_module.register_proces_events( data, proces, kalk, kalk_type, tip_proizvoda );
    }, 10*1000 );  
    

    return proces_row;


  };
  this_module.make_kalk_proces = make_kalk_proces;
  
  
  
  function update_kalk( kalk, product_data, arg_proces_index, changed_ulaz, changed_izlaz, changed_action, arg_silent ) {
  
    var dobiveno_kom_ulaz = 0;
    var work_data_error = false;
    
    $.each( kalk.procesi, function(p_ind, proc) {
      
      
      
      // -------------------- loopaj samo ako nema niti jedne greške --------------------
      // -------------------- loopaj samo ako nema niti jedne greške --------------------
      
      if ( work_data_error == false && p_ind >= arg_proces_index ) { 
        
        // započni loop samo od procesa koji je promjenjen pa na dalje
        
        
        var func_name = proc.action?.action_func || null;
        var radna_stn = proc.action?.action_radna_stn || null;
        var naklada = kalk.kalk_naklada || null;
        
        
        
        // if ( !arg_silent ) {
        
          if ( !proc.ulazi || proc.ulazi?.length == 0 ) {
            popup_warn(`Proces ${p_ind + 1} mora imati barem jedan ULAZ !!!`);
            work_data_error = true;
            return null;
          };

          if ( !proc.izlazi || proc.izlazi?.length == 0 ) {
            popup_warn(`Proces ${p_ind + 1} mora imati barem jedan IZLAZ !!!`);
            work_data_error = true;
            return null;
          };


          

          if ( !func_name ) {
            popup_warn(`Proces ${p_ind + 1} nema definiranu funkciju procesa !!!`);
            work_data_error = true;
            return null;
          };
        
          
        
          if ( !radna_stn ) {
            popup_warn(`Proces ${p_ind + 1} nema radnu stanicu !!!`);
            work_data_error = true;
            return null;
          };
        
          
          if ( !naklada ) {
            popup_warn(`Morate upisati nakladu !!!`);
            work_data_error = true;
            return null;
          };
          
        // }; // kraj ako NIJE SILENT
          

        
        
        var work_data = VELPROCES[func_name](
          this_module,
          naklada,
          proc.ulazi,
          
          null, // izlazi
          null, // action
          radna_stn,
          product_data,
          kalk,

          proc.proc_koment,
          proc.extra_data,
          arg_curr_proces_index = p_ind,
        );
        
        
       
        if ( 
          p_ind > 0 // samo ako proces nije prvi proces u lancu ---_> mora biti drugi ili veći
          && 
          kalk.procesi[p_ind-1]?.extra_data?.forme // i proces PRIJE OVOG proces IMA extra data forme
        ) {
          
          // loop po svim ulazima curr procesa
          $.each( kalk.procesi[p_ind].ulazi, function(u_ind, proc_ulaz) {
            
            if ( proc_ulaz.kalk_plate_naziv ) {
              // samo ako ima plate ---> to znači da je u prethodnom procesu ovaj element nastao iz montažne forme/ploče
              
              if ( !kalk.procesi[p_ind].extra_data ) kalk.procesi[p_ind].extra_data = {}; // ako ovaj proces nema extra data onda ga napravi
              if ( !kalk.procesi[p_ind].extra_data.forme ) kalk.procesi[p_ind].extra_data.forme = []; // napravi property forme
              
              // dodaj naziv forme  ---- izvuci ime forme iz kalk_plate_naziv
              kalk.procesi[p_ind].extra_data.forme = insert_uniq( kalk.procesi[p_ind].extra_data.forme, proc_ulaz.kalk_plate_naziv.split(` ---`)[0] )
            };
          });
          

          
        };
        
        
        if ( !work_data ) {
          console.log(`----------- error u work data`);
          work_data_error = true;
          return null;
        };

        
        // spremi postojecu sifru actiona
        var curr_action_sifra = proc.action.action_sifra;
        var new_action = work_data.proces.action;
        kalk.procesi[p_ind].action = new_action;
        // vrati nazad staru sifru action-a u novi action pošto sam je prepisao kod kreiranja actiona sa work data
        kalk.procesi[p_ind].action.action_sifra = curr_action_sifra;



        var curr_izlaz_sifra = proc.izlazi[0].sifra;
        var new_izlazi = work_data.proces.izlazi;
        kalk.procesi[p_ind].izlazi = new_izlazi;
        
        if (proc.izlazi.length == 1) {
          kalk.procesi[p_ind].izlazi[0].sifra = curr_izlaz_sifra; // za svaki slučaj kopiram sifru tako da ostane ista
        };

        
        // ako na primjer dodajem ALAT ili neke dodatne ulaze dinamično !!!
        var new_ulazi = work_data.proces.ulazi;
        kalk.procesi[p_ind].ulazi = new_ulazi;
        
        
        this_module.copy_izlaze_to_ulaze( kalk, p_ind );
        
        
          /*
          kalk.procesi[p_ind].action.action_prep_time = work_data.prep_time;
          kalk.procesi[p_ind].action.action_work_time = work_data.time;
          kalk.procesi[p_ind].action.action_est_price = work_data.price;
          */

          // samo ako je jedan jedini izlaz 
          // ako ima više izlaza onda je to montažna ploča 
          // i tada user određuje izlaze prilikom upisivanja u product data ELEMENTE
          // if ( kalk.procesi[p_ind].izlazi.length == 1 ) kalk.procesi[p_ind].izlazi = work_data.proces.izlazi;
          
        // }; // kraj u kojem je procesu ovisno o tome jel promjenjen ulaz, izlaz ili akcija
     
      }; // kraj ako NEMA error u work data i ako je index procesa veći ili jedank indexu u argumentu
      
      
    }); // kraj loopa po svim procesima
    
    
  };
  this_module.update_kalk = update_kalk;
  
  
  function copy_izlaze_to_ulaze( kalk, p_ind ) {
    
    
    var koliko_ulaza_od_izlaza = {};
    
    var current_izlaz_sifra = null;
    
    $.each( kalk.procesi[p_ind].izlazi, function(iz_ind, work_izlaz) {

      var current_izlaz_sifra = work_izlaz.sifra;
      var current_izlaz_count = work_izlaz.kalk_sum_sirovina;
     
      // traži po svim procesima
      $.each( kalk.procesi, function(find_proc_ind, find_proces) {
        
        // traži po svim ulazima
        $.each( find_proces.ulazi, function(find_ulaz_ind, find_ulaz) {

          // ako u procesu imam ULAZ koji je isti kao current izlaz  
          if (
            find_ulaz.izlaz_sifra == current_izlaz_sifra // ako bilo koji od ulaza ima izlaz sifru koji je isti kao ovaj trenutni izlaz
            &&
            find_proc_ind > p_ind  // ali ulaz mora biti u procesu koji je poslje ovog kojeg sad kopiram
          ) {

              // ako još uvijek NEMAM property u counter_object za ovaj IZLAZ
              if ( !koliko_ulaza_od_izlaza[current_izlaz_sifra] ) koliko_ulaza_od_izlaza[current_izlaz_sifra] = {};
              koliko_ulaza_od_izlaza[current_izlaz_sifra].count = current_izlaz_count;

              var current_ulaz_sifra = find_ulaz.sifra;
              koliko_ulaza_od_izlaza[current_izlaz_sifra][current_ulaz_sifra] = find_ulaz.kalk_sum_sirovina;
            
          };

        }); // loop po svim ulazima u procesu

      }); // loop po svim procesima

    }); // lop po svim izlazima u procesu
    
    
    if ( Object.keys(koliko_ulaza_od_izlaza).length > 0 ) {

      // ovo je OBJEKT A NE ARRAY
      $.each( koliko_ulaza_od_izlaza, function(key_izlaz_sifra, izlaz_objekt) {

        var current_izlaz_sifra = key_izlaz_sifra;
        koliko_ulaza_od_izlaza[key_izlaz_sifra].sum = 0;
        
        // OVO JE OBJEKT A NE ARRAY
        $.each( koliko_ulaza_od_izlaza[key_izlaz_sifra], function( key, value_ulaza ) {
          // zbroji sve ulaze koji su nastali od current izlaza
          if ( key !== "count" && key !== "sum" ) koliko_ulaza_od_izlaza[key_izlaz_sifra].sum += value_ulaza;
        });
        
      
      });
      
    } 
    else {
      return;
      
    };
    
    
    // ---------------------------- AKO IMAM VIŠE ULAZA OD JEDNOG IZLAZA ONDA RASPOREĐUJEM RAVNOMJERNO
    // ---------------------------- AKO IMAM VIŠE ULAZA OD JEDNOG IZLAZA ONDA RASPOREĐUJEM RAVNOMJERNO
    
    
    // -------------- AKO NPR IMAM 100 IZLAZA 
    //                i ONDA IMAM 50 TIH IZLAZA KAO ULAZA U JEDNOM PROCESU 
    //                i DRUGIH 50 IZLAZA KAO ULAZ U DRUGOM PROCESU
    
    // koliko_ulaza_od_izlaza[izlaz111].sum  mi je u ovom slučaju 100 kom
    // koliko_ulaza_od_izlaza[izlaz111][ulaz222] mi je u ovom slučaju 50 kom za jedan proces
    // koliko_ulaza_od_izlaza[izlaz111][ulaz333] mi je u ovom slučaju 50 kom za drugi proces
    
    
    // opet ponavljam loop po svim izlazima u p_ind procesu
    
    $.each( kalk.procesi[p_ind].izlazi, function(iz_ind, work_izlaz) {

      var current_izlaz_sifra = work_izlaz.sifra;
      
      // var koliko_ulaza_od_izlaza = 0;
      
      // traži po svim procesima
      $.each( kalk.procesi, function(find_proc_ind, find_proces) {

        // ako u procesu imam ULAZ koji je isti kao current izlaz
        $.each( find_proces.ulazi, function(find_ulaz_ind, find_ulaz) {

          
          if (
            find_ulaz.izlaz_sifra == current_izlaz_sifra // ako bilo koji od ulaza ima izlaz sifra property koji je isti kao ovaj trenutni
            &&
            find_proc_ind > p_ind  // ali ulaz mora biti u preocesu koji je poslje ovog kojeg sad kopiram
          ) {

            var current_ulaz_sifra = find_ulaz.sifra;
            var kolicina_izlaza = work_izlaz.kalk_sum_sirovina; // ovaj broj je već upisan u .count prop unutar svako
            var kolicina_ulaza = kolicina_izlaza; // default je da količina u sljedećem ulazu bude jednaka izlazu
            
            if ( koliko_ulaza_od_izlaza[current_izlaz_sifra][current_ulaz_sifra] ) {
              // ako nema sume neka suma bude taj isti broju u brojniku (gornji dio razlomka) -----> tako da će udio biti jedan
              var suma = koliko_ulaza_od_izlaza[current_izlaz_sifra].sum || koliko_ulaza_od_izlaza[current_izlaz_sifra][current_ulaz_sifra];
              
              // ovo je postotak koliko nekog izlaza ima u ovom ulazu
              var udio_ovog_ulaza = koliko_ulaza_od_izlaza[current_izlaz_sifra][current_ulaz_sifra] / suma;
              kolicina_ulaza = kolicina_izlaza * udio_ovog_ulaza;
            };
            
            var new_ulaz = {
      
              ...work_izlaz,
              
              _id: null,
              prodon_sirov_id: work_izlaz.prodon_sirov_id || null,
              
              izlaz_sifra: work_izlaz.sifra,
              
              sifra: current_ulaz_sifra, // ostavi istu sifru koja je bila prije upadate
              kalk_unit_area: ( work_izlaz.kalk_po_valu || 0 ) * (work_izlaz.kalk_po_kontravalu || 0 ) / 1000000,
              kalk_sum_sirovina: kolicina_ulaza,
              
              kalk_unit_price: 0,
              real_unit_price: 0,
              kalk_price: 0,
              kalk_ostatak_val: null,
              kalk_ostatak_kontra: null,
              
              
            };

            kalk.procesi[find_proc_ind].ulazi[find_ulaz_ind] = new_ulaz;

          }; // ako je nasao ulaz koj iima sifru izlaza

        }); // loop po svim ulazima

      }); // loop po svim procesima

    }); // loop po svim izlazima (samo od trenutnog procesa ---> p_index )
    
    
    return kalk;
    
  };
  this_module.copy_izlaze_to_ulaze = copy_izlaze_to_ulaze;
  
  
  // ------------START-------------- EVENI --------------------------
  // ------------START-------------- EVENI --------------------------
  
  // register kalk events sam prebacio u kalk util

  async function register_action_events(kalk_obj, action, proces, kalk, type) {
    
    $(`#` + kalk_obj.kalk_sifra + `_center_scroll_box .kalk_action_box .cit_input`).off(`click`);
    $(`#` + kalk_obj.kalk_sifra + `_center_scroll_box .kalk_action_box .cit_input`).on(`click`, function() {
      window.current_input_id = this.id;
      console.log(` ------------------ KLICK register_action_events ------------------------ ` + window.current_input_id);
    });
    
    
    $(`#` + kalk_obj.kalk_rand + `_` + action + `_action_radna_stn`).data('cit_props', {
      
      desc: 'odabir akcije to radne stanice unutar reda kalkulacije : ' + action,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: `radne_stanice`,
      cit_run: async function(state) {
        
        
        
        var this_input = $('#'+current_input_id)[0];
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          return;
        };
        
        
        var action_sifra = action;
        var proces_sifra = proces;
        var kalk_sifra = kalk;
        var kalk_type = type;
        
      
        console.log( `--------------------------------`, action_sifra);
        console.log( `--------------------------------`, proces_sifra);
        console.log( `--------------------------------`, kalk_sifra);
        console.log( `--------------------------------`, kalk_type);
        
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        var product_id = $('#'+data.id).closest('.product_comp')[0].id;
        var product_data =  this_module.product_module.cit_data[product_id];
        
        
        var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
        var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, proces_sifra, null, null, null );
        var curr_action = this_module.kalk_search(data, kalk_type, kalk_sifra, proces_sifra, null, null, action_sifra );
        
        var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );  
        
        
        console.log(`-----------------curr_action-----------------------`);
        console.log(curr_action);
        
        // ako postoji type radnje u action type i ako sada ovaj state ima neki posve drugi tip radnje
        
        /*
        
        if ( 
          curr_proces.action.action_type &&
          curr_proces.action.action_type.sifra &&
          curr_proces.action.action_type.sifra !== state.type 
        ) {
          popup_warn(`Ne možete mijenjati tip radnje - ako je npr. bilo rezanje mora biti radna stanica za rezanje.<br><br> Ako želite potpuno novu radnju onda obrišite ovaj proces i napravite novi !!!!`);
          return;
        };
        
        */
        
        // --------------------------------AKO JE IZABRAN SCITEX WORK ----------------------------------------
        if ( state.sifra.indexOf(`SCITEX`) > -1 ) {
          curr_proces = await add_scitex_colors(curr_proces, product_data);
        };
        // ----------------------------------------------------------------------------------------------
        
        
        // --------------------------------AKO JE IZABRAN LJEPLJENJE STALKA  S RUČNIM PIŠTOLJEM ----------------------------------------
        if ( state.sifra.indexOf(`RADGLUE`) > -1 ) {
          curr_proces = add_glue( curr_proces, product_data, this_module );
          popup_warn("Obavezno izaberite ljepilo u praznom redu !!!");
        };
        // ----------------------------------------------------------------------------------------------

        
        curr_proces.action.action_func = state.func;
        curr_proces.action.action_type = { sifra: state.type, naziv: state.proces_type_naziv };
        curr_proces.action.action_radna_stn = cit_deep(state);
        
        $('#'+current_input_id).val(state.naziv);
        
        
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, null, null, changed_action=true );
        
        
        console.log(`--------------- make kalk kod odabira _action_radna_stn ----------------`);
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
        // reset_tooltips();

      },
     
     
    });
    
    
    $(`#` + kalk_obj.kalk_rand + `_` + action + `_action_prep_time`).off(`blur`);
    $(`#` + kalk_obj.kalk_rand + `_` + action + `_action_prep_time`).on(`blur`, function() {
      
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var action_row = $(this_input).closest('.action_row');

      var kalk_type = action_row.attr(`data-kalk_type`);
      var kalk_sifra = action_row.attr(`data-kalk_sifra`);
      var row_sifra = action_row.attr(`data-row_sifra`);
      
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      
      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );

      
      var input_value = cit_number( $(this_input).val() );
      if ( !curr_proces.extra_data ) curr_proces.extra_data = {};
      
      if ( $(this_input).val() == `` ) {
        
        curr_proces.action.action_prep_time = null;
        curr_proces.action.custom_prep_time = null;
        if ( curr_proces.extra_data?.action_prep_time ) delete curr_proces.extra_data.action_prep_time;
        
      };
      
      
      if ( input_value !== null ) {
          
        $(this_input).val( cit_format( input_value, 2 ) );
        
        curr_proces.action.action_prep_time = input_value;
        curr_proces.action.custom_prep_time = true;
        
        curr_proces.extra_data.action_prep_time = input_value;
        
      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
        
        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);
        return;
      };
      
            
      this_module.update_kalk( curr_kalk, product_data, curr_proces_index, null, null, changed_action=true );
      var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
      // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      
      
    });
    
    
    $(`#` + kalk_obj.kalk_rand + `_` + action + `_action_work_time`).off(`blur`);
    $(`#` + kalk_obj.kalk_rand + `_` + action + `_action_work_time`).on(`blur`, function() {
      
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var action_row = $(this_input).closest('.action_row');

      var kalk_type = action_row.attr(`data-kalk_type`);
      var kalk_sifra = action_row.attr(`data-kalk_sifra`);
      var row_sifra = action_row.attr(`data-row_sifra`);
      
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_action = curr_proces.action;
      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
      
      var input_value = cit_number( $(this_input).val() );
      
      
      if ( !curr_proces.extra_data ) curr_proces.extra_data = {};
      var same = cit_is_same( input_value, curr_proces.extra_data.action_work_time );
      
      
      if ( $(this_input).val() == `` ) {
        
        curr_action.action_work_time = null;
        curr_proces.action.custom_work_time = null;
        if ( curr_proces.extra_data?.action_work_time ) delete curr_proces.extra_data.action_work_time;
      };

      
      
            
      if ( input_value !== null ) {
        // curr_proces.action.action_work_time = input_value;
        curr_proces.action.custom_work_time = true;
        curr_proces.extra_data.action_work_time = input_value;
        
        $(this_input).val( cit_format( input_value, 2 ) );
        
      } 
      else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
        
        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);
        return;
        
      };
      
      // napravi update samo ako je promjena value-a
      if ( !same ) {
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, null, null, changed_action=true );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };


      
      
    });
 
    
    $(`#` + kalk_obj.kalk_rand + `_` + action + `_satnica_stroj`).off(`blur`);
    $(`#` + kalk_obj.kalk_rand + `_` + action + `_satnica_stroj`).on(`blur`, function() {
      
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      var action_row = $(this_input).closest('.action_row');

      var kalk_type = action_row.attr(`data-kalk_type`);
      var kalk_sifra = action_row.attr(`data-kalk_sifra`);
      var row_sifra = action_row.attr(`data-row_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
      
      var curr_action = curr_proces.action;
      
      var input_value = cit_number( $(this_input).val() );
      
      
      if ( !curr_proces.extra_data ) curr_proces.extra_data = {}; 
      if ( !curr_action.prices ) curr_action.prices = {}; 
      
      var same = cit_is_same( input_value, curr_proces.extra_data.satnica_stroj );
      
      if ( $(this_input).val() == `` ) {
        curr_proces.extra_data.satnica_stroj = null;
      };
      
      if ( input_value !== null ) {
        
        curr_proces.extra_data.satnica_stroj = input_value;
        $(this_input).val( cit_format( input_value, 2 ) );

      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);
        return;
      };
      
      if ( !same ) {
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, null, null, changed_action=true );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };
      
      
    });
    
    
    $(`#` + kalk_obj.kalk_rand + `_` + action + `_satnica_prep`).off(`blur`);
    $(`#` + kalk_obj.kalk_rand + `_` + action + `_satnica_prep`).on(`blur`, function() {
      
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      var action_row = $(this_input).closest('.action_row');

      var kalk_type = action_row.attr(`data-kalk_type`);
      var kalk_sifra = action_row.attr(`data-kalk_sifra`);
      var row_sifra = action_row.attr(`data-row_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
      
      var curr_action = curr_proces.action;
      
      var input_value = cit_number( $(this_input).val() );
      
      if ( !curr_proces.extra_data ) curr_proces.extra_data = {}; 
      if ( !curr_action.prices ) curr_action.prices = {}; 
      
      var same = cit_is_same( input_value, curr_proces.extra_data.satnica_prep );
      
      if ( $(this_input).val() == `` ) {
        curr_proces.extra_data.satnica_prep = null;
      };
      
      
      if ( input_value !== null ) {
        
        curr_proces.extra_data.satnica_prep = input_value;
        $(this_input).val( cit_format( input_value, 2 ) );
        
      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
        
        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);
        return;
      };
      
      if ( !same ) {
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, null, null, changed_action=true );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };
      
    });
    
    
    $(`#` + kalk_obj.kalk_rand + `_` + action + `_satnica_radnik`).off(`blur`);
    $(`#` + kalk_obj.kalk_rand + `_` + action + `_satnica_radnik`).on(`blur`, function() {
      
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      var action_row = $(this_input).closest('.action_row');

      var kalk_type = action_row.attr(`data-kalk_type`);
      var kalk_sifra = action_row.attr(`data-kalk_sifra`);
      var row_sifra = action_row.attr(`data-row_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
      
      var curr_action = curr_proces.action;
      
      var input_value = cit_number( $(this_input).val() );
      
      if ( !curr_proces.extra_data ) curr_proces.extra_data = {}; 
      if ( !curr_action.prices ) curr_action.prices = {}; 
      
      var same = cit_is_same( input_value, curr_proces.extra_data.satnica_radnik );
      
      
      if ( $(this_input).val() == `` ) {
        curr_proces.extra_data.satnica_radnik = null;
      };
      
      if ( input_value !== null ) {
        
        curr_proces.extra_data.satnica_radnik = input_value;
        $(this_input).val( cit_format( input_value, 2 ) );
        
      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
        
        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);
        return;
      };
      
      if ( !same ) {
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, null, null, changed_action=true );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };
      
    });
    
    
    $(`#` + kalk_obj.kalk_rand + `_` + action + `_radnik_count`).off(`blur`);
    $(`#` + kalk_obj.kalk_rand + `_` + action + `_radnik_count`).on(`blur`, function() {
      
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      var action_row = $(this_input).closest('.action_row');

      var kalk_type = action_row.attr(`data-kalk_type`);
      var kalk_sifra = action_row.attr(`data-kalk_sifra`);
      var row_sifra = action_row.attr(`data-row_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
      
      var curr_action = curr_proces.action;
      
      var input_value = cit_number( $(this_input).val() );
      
      if ( !curr_proces.extra_data ) curr_proces.extra_data = {}; 
      var same = cit_is_same( input_value, curr_proces.extra_data.radnik_count );
      
      
      if ( $(this_input).val() == `` ) {
        curr_proces.extra_data.radnik_count = null;
      };
      
      if ( input_value !== null ) {
        
        curr_proces.extra_data.radnik_count = input_value;
        $(this_input).val( cit_format( input_value, 2 ) );
        
      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
        
        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);
        return;
      };
      
      if ( !same ) {
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, null, null, changed_action=true );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };
      
    });
        
    
    
    $(`#` + kalk_obj.kalk_rand + `_` + action + `_radnik_count_prep`).off(`blur`);
    $(`#` + kalk_obj.kalk_rand + `_` + action + `_radnik_count_prep`).on(`blur`, function() {
      
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      var action_row = $(this_input).closest('.action_row');

      var kalk_type = action_row.attr(`data-kalk_type`);
      var kalk_sifra = action_row.attr(`data-kalk_sifra`);
      var row_sifra = action_row.attr(`data-row_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
      
      var curr_action = curr_proces.action;
      
      var input_value = cit_number( $(this_input).val() );
      
      if ( !curr_proces.extra_data ) curr_proces.extra_data = {}; 
      var same = cit_is_same( input_value, curr_proces.extra_data.radnik_count_prep );
      
      if ( $(this_input).val() == `` ) {
        curr_proces.extra_data.radnik_count_prep = null;
      };
      
      if ( input_value !== null ) {
        
        curr_proces.extra_data.radnik_count_prep = input_value;
        $(this_input).val( cit_format( input_value, 2 ) );
        
      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
        
        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);
        return;
      };
      
      if ( !same ) {
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, null, null, changed_action=true );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };
      
      
    });
        

    
    $(`#` + kalk_obj.kalk_rand + `_` + action + `_action_multiload`).off(`blur`);
    $(`#` + kalk_obj.kalk_rand + `_` + action + `_action_multiload`).on(`blur`, function() {
      
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      var action_row = $(this_input).closest('.action_row');

      var kalk_type = action_row.attr(`data-kalk_type`);
      var kalk_sifra = action_row.attr(`data-kalk_sifra`);
      var row_sifra = action_row.attr(`data-row_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
      
      var curr_action = curr_proces.action;
      
      if ( !curr_proces.extra_data ) curr_proces.extra_data = {};
      
      var input_value = cit_number( $(this_input).val() );
      var same = cit_is_same( input_value, curr_action.action_multiload );
      
      if ( $(this_input).val() == `` ) {
        curr_action.action_multiload = null;
        if ( curr_proces.extra_data?.multiload ) curr_proces.extra_data.multiload = null;  
      };
      
      if ( input_value !== null ) {
        
        curr_action.action_multiload = input_value;
        curr_proces.extra_data.multiload = input_value;
        
        $(this_input).val( cit_format( input_value, 2 ) );
        
      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
        
        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);
        return;
      };
      
      if ( !same ) {
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, null, null, changed_action=true );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };
      
    });
        
    
    $(`#` + kalk_obj.kalk_rand + `_` + action + `_action_kasir_num`).off(`blur`);
    $(`#` + kalk_obj.kalk_rand + `_` + action + `_action_kasir_num`).on(`blur`, function() {
      
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      var action_row = $(this_input).closest('.action_row');

      var kalk_type = action_row.attr(`data-kalk_type`);
      var kalk_sifra = action_row.attr(`data-kalk_sifra`);
      var row_sifra = action_row.attr(`data-row_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
      
      var curr_action = curr_proces.action;
      
      if ( !curr_proces.extra_data ) curr_proces.extra_data = {};

      var input_value = cit_number( $(this_input).val() );
      var same = cit_is_same( input_value, curr_action.action_kasir_num );
      
      
      if ( $(this_input).val() == `` ) {
        curr_action.action_kasir_num = null;
        if ( curr_proces.extra_data?.kasir_num ) curr_proces.extra_data.kasir_num = null;  
      };

      
      if ( input_value !== null ) {
        
        curr_action.action_kasir_num = input_value;
        curr_proces.extra_data.kasir_num = input_value;
        $(this_input).val( cit_format( input_value, 2 ) );
        
      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
        
        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);
        return;
      };
      
      if ( !same ) {
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, null, null, changed_action=true );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };
        
    });

    
  };
  this_module.register_action_events = register_action_events;
  
  
  async function register_kalk_ulaz_events(data, ulaz_sifra, proces, kalk, kalk_type, tip_proizvoda ) {
    
    var sirov_valid = this_module.sirov_module.valid;
    
    
    
    $(`#` + kalk.kalk_sifra + `_center_scroll_box .kalk_ulaz_box .cit_input`).off(`click`);
    $(`#` + kalk.kalk_sifra + `_center_scroll_box .kalk_ulaz_box .cit_input`).on(`click`, function() {
      window.current_input_id = this.id;
      console.log(` ------------------ KLICK register_kalk_ulaz_events ------------------------ ` + window.current_input_id);
    });
    
    
    $(`#`+kalk.kalk_rand+`_`+ ulaz_sifra + `_ulaz_price`).off(`blur`);
    $(`#`+kalk.kalk_rand+`_`+ ulaz_sifra + `_ulaz_price`).on(`blur`, async function() {
      
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var ulaz_row = $(this_input).closest('.ulaz_row');

      var kalk_type = ulaz_row.attr(`data-kalk_type`);
      var kalk_sifra = ulaz_row.attr(`data-kalk_sifra`);
      var row_sifra = ulaz_row.attr(`data-row_sifra`);
      var ulaz_sifra = ulaz_row.attr(`data-ulaz_sifra`);
      
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null ); 
      
      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
      
      var input_value = cit_number( $(this_input).val() );
      
      if ( !curr_proces.extra_data ) curr_proces.extra_data = {};
      if ( !curr_proces.extra_data.ulaz_prices ) curr_proces.extra_data.ulaz_prices = {};
      
      var same = cit_is_same( input_value, curr_proces.extra_data.ulaz_prices[curr_ulaz.sifra] );
      
      
      if ( $(this_input).val() == `` ) {
        delete curr_proces.extra_data.ulaz_prices[curr_ulaz.sifra]; // posve obriši ovaj property 
      };
      
      if ( input_value !== null ) {
        
        curr_proces.extra_data.ulaz_prices[curr_ulaz.sifra] = input_value;
        $(this_input).val( cit_format( input_value, 2 ) );
        
      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
        
        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);
        return;
      };      
      
      if ( !same ) {
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, changed_ulaz=true, null, null );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };
      
    });
 
    
    
    
    
    $(`#` + kalk.kalk_rand + `_` + ulaz_sifra + `_kalk_find_sirov`).data('cit_props', {
      // !!!findsirov!!!
      desc: 'pretraga sirovine u modulu kalkulacija za sifru ' + ulaz_sifra,
      local: false,
      url: '/find_sirov',
      find_in: [
        
        "sirovina_sifra", 
        "naziv",
        "full_naziv",
        "stari_naziv",
        "povezani_nazivi",
        "detaljan_opis",
        "grupa_flat",  

        "kvaliteta_1",
        "kvaliteta_2", 
        "kvaliteta_mix",

        "kontra_tok_x_tok",
        "val_x_kontraval",
        "alat_prirez",


        "dobavljac_flat", 
        "dobav_naziv", 
        "dobav_sifra", 
        "fsc_flat", 
        "povezani_kupci_flat", 
        "boja",   
        "zemlja_pod_flat",
        "specs_flat",
        
      ],
      return: {},
      /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */
      col_widths: [
        1, // "sifra_link",
        2, // "full_naziv",
        2, // "full_dobavljac",
        1, // grupa_naziv
        1, // "boja",
        1, // "cijena",
        1, // "kontra_tok",
        1, // "tok",
        1, // "po_valu",
        1, // "po_kontravalu",
        2, // "povezani_nazivi",
        
        
        1, // "pk_kolicina",
        1, // "nk_kolicina",
        1, // "rn_kolicina",
        1, // "order_kolicina",
        1, // "sklad_kolicina",
        1, // "forcast_kolicina",
        1, // last status
        
        4, //  `spec_docs`,
      ],
      show_cols: [
        
        "sifra_link",
        "full_naziv",
        "full_dobavljac",
        "grupa_naziv",
        "boja",
        "cijena",
        "kontra_tok",
        "tok",
        "po_valu",
        "po_kontravalu",
        "povezani_nazivi",
        
        `pk_kolicina`,
        `nk_kolicina`,
        `rn_kolicina`,
        `order_kolicina`,
        `sklad_kolicina`,
        `forcast_kolicina`,
        
        `last_status`,
        `spec_docs`,
        
      ],
      custom_headers: [
        
        `ŠIFRA`,  // "sirovina_sifra",
        `NAZIV`,  // "full_naziv",
        `DOBAV.`,  // "full_dobavljac",
        `GRUPA`,  // "grupa_naziv",
        `BOJA`,  // "boja",
        `CIJENA`,  // "cijena",
        `KONTRA TOK`,  // "kontra_tok",
        `TOK`,  // "tok",
        `VAL`,  // "po_valu",
        `KONTRA VAL`,  // "po_kontravalu",
        `POVEZANO`,  // "povezani_nazivi",
              
        
        `PK RESERV`,  // "pk_kolicina",
        `NK RESERV`,  // "nk_kolicina",
        `RN RESERV`, // "rn_kolicina"
        `NARUČENO`,  // "order_kolicina",
        `SKLADIŠTE`,  // "sklad_kolicina",
        `DOSTUPNO`, // forcast kolicina
        `STATUS`,
        `DOCS`,
        
        
      ],
      format_cols: {
        "sirovina_sifra": "center",
        "grupa_naziv": "center",
        "boja": "center",
        "cijena": sirov_valid.cijena.decimals,
        "kontra_tok": sirov_valid.kontra_tok.decimals,
        "tok": sirov_valid.tok.decimals,
        "po_valu": sirov_valid.po_valu.decimals,
        "po_kontravalu": sirov_valid.po_kontravalu.decimals,
        
        "pk_kolicina": sirov_valid.pk_kolicina.decimals,
        "nk_kolicina": sirov_valid.nk_kolicina.decimals,
        
        "rn_kolicina": sirov_valid.rn_kolicina.decimals,
        
        "order_kolicina": sirov_valid.order_kolicina.decimals,
        "sklad_kolicina": sirov_valid.sklad_kolicina.decimals,
        "forcast_kolicina": 2,
        
      },
      
      query: function () { 
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        var product_id = $('#'+current_input_id).closest('.product_comp')[0].id;
        var product_data =  this_module.product_module.cit_data[product_id];
        
        return { 
          on_date: product_data?.rok_za_def || null 
        };
      
      },

      filter: this_module.find_sirov.find_sirov_filter, //  možda kasnije dodati -----> this_module.filter_glues,

      show_on_click: false,
      list_width: 700,
      cit_run: this_module.select_find_sirov,
    });  
    
    
    $(`#` + kalk.kalk_rand + `_` + ulaz_sifra + `_kalk_find_plate`).data('cit_props', {

      desc: 'odabir montažne ploče/forme za specifičan ulaz u kalkulacijama ' + ulaz_sifra,
      local: true,

      show_cols: [ 
        'naziv',
        'sirov_naziv', // to je zapravo sirov.full_naziv

        'color_C',
        'color_M',
        'color_Y',
        'color_K',
        'color_W',
        'color_V',

        'noz_big',
        'nest_count'
      ],
      custom_headers: [ 
        'MONT. PLOČA',
        'SIROVINA',
        'CYAN',
        'MAGENTA',
        'YELLOW',
        'KEY',
        'WHITE',
        'VARNISH',
        'NOŽ/BIG metara',
        'BROJ GNJEZDA'
      ],
      col_widths: [
        2, // 'naziv',
        4, // 'sirov_naziv', // to je zapravo sirov.full_naziv

        1, // 'color_C',
        1, // 'color_M',
        1, // 'color_Y',
        1, // 'color_K',
        1, // 'color_W',
        1, // 'color_V',

        1, // 'noz_big',
        1, // 'nest_count'
      ],

      return: {},
      show_on_click: true,
      list: function() {

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id = data.rand_id;
        
        var product_id = $('#'+current_input_id).closest('.product_comp')[0].id;
        var product_data =  this_module.product_module.cit_data[product_id];
        
        
        var ulaz_row = $('#'+current_input_id).closest('.ulaz_row');
     
        
        var kalk_type = ulaz_row.attr(`data-kalk_type`);
        var kalk_sifra = ulaz_row.attr(`data-kalk_sifra`);
        var row_sifra = ulaz_row.attr(`data-row_sifra`);
        var ulaz_sifra = ulaz_row.attr(`data-ulaz_sifra`);
        
        var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
        var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null );
        var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );

        var mont_ploce_for_list = [];
        
        $.each( product_data.mont_ploce, function( p_ind, plate ) {
          
        
          var plate_already_used = false;
          $.each(curr_kalk.procesi, function(proc_ind, find_proc) {
            $.each(find_proc.ulazi, function(proc_ul_ind, find_proc_ulaz) {
              if ( find_proc_ulaz.kalk_plate_id == plate._id ) {
                plate_already_used = true;
              };
            });
          });
        
          
          // -----START------- AKO PLATE NIJE VEĆ IZABRAN
          
          if ( plate_already_used == false ) {
            
            var {
              sifra,
              naziv,
              sirov,

              color_C,
              color_M,
              color_Y,
              color_K,
              color_W,
              color_V,

              noz_big,
              nest_count,
            } = plate;


            mont_ploce_for_list.push({ 
              plate_id: plate._id,
              sifra,
              naziv,
              sirov,
              sirov_naziv: sirov.full_naziv, 
              
              color_C,
              color_M,
              color_Y,
              color_K,
              color_W,
              color_V,

              noz_big,
              nest_count,
            });
            
          };
          // -----END------- AKO PLATE NIJE VEĆ IZABRAN


        });

        return mont_ploce_for_list;

      },
      cit_run: function(state) {
        
        var this_input = $('#'+current_input_id)[0];
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        var ulaz_row = $('#'+current_input_id).closest('.ulaz_row');
        
        var product_id = $('#'+current_input_id).closest('.product_comp')[0].id;
        var product_data =  this_module.product_module.cit_data[product_id];
        
        
        var kalk_type = ulaz_row.attr(`data-kalk_type`);
        var kalk_sifra = ulaz_row.attr(`data-kalk_sifra`);
        var row_sifra = ulaz_row.attr(`data-row_sifra`);
        var ulaz_sifra = ulaz_row.attr(`data-ulaz_sifra`);
        
        var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
        var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null );
        var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra`);
        
        var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null );
        
        if ( state == null ) {
          // --------------------------------------------------------------------------------------------------------------
          // NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL
          // --------------------------------------------------------------------------------------------------------------
          
          $('#'+current_input_id).val(``);
          
          popup_warn(`Ako želite obrisati sirovinu onda kliknite na MINUS gumb na početku reda !!!`);
   
          /*
          var data_full_name = $('#'+current_input_id).parent().attr(`data-full_name`);
          $('#'+current_input_id).val(data_full_name);
          */
          
          var curr_plate_name = null;
          if ( curr_ulaz.kalk_plate_naziv ) curr_plate_name = curr_ulaz.kalk_plate_naziv.split(` ---`)[0];
          
          curr_ulaz.kalk_plate_naziv = null;
          curr_ulaz.kalk_plate_id = null;
          
          // ako postoji neko ime od montažne forme
          // i ako extra data u sebi ima forme array
          if ( curr_plate_name && curr_proces.extra_data?.forme.length > 0 ) {
            // izbaci ovaj naziv od ove forme iz extra data!!!!
            curr_proces.extra_data.forme = delete_item( curr_proces.extra_data.forme, curr_plate_name);
          };
          

         
          var postojeca_sifra = curr_ulaz.sifra; // spremi postojeću sifru
          var curr_ulaz = this_module.fresh_ulaz();
          curr_ulaz.sifra = postojeca_sifra; // vrati tu sifru nazad (prepisi novo nastalu sifrus)
          
          curr_proces.ulazi = upsert_item( curr_proces.ulazi, curr_ulaz, `sifra`);
          
          var alati = find_elem_alate( product_data, curr_proces, curr_ulaz );
          
          // delete sve alate koji su povezani sa ovim plate
          $.each( alati, function(alat_ind, alat) {
            curr_proces.ulazi = delete_item( curr_proces.ulazi, alat._id , `_id`);
          });
          
          this_module.update_kalk( curr_kalk, product_data, curr_proces_index, changed_ulaz=true, null, null );
        
          
          // setTimeout( function() {
            console.log(`--------------- make kalk IN ULAZ KALK FIND PLATE ----------------`);
            var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null, `return_html` );
            $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
            reset_tooltips();
          
          // }, 300);
            
            
          return;
          
          // --------------------------------------------------------------------------------------------------------------
          // NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL NULL
          // --------------------------------------------------------------------------------------------------------------
          
          
        };
        
        
        
        // napravi kopiju
        state = cit_deep(state);
        

        // ako je ljepenka
        var sirov_area = (state.sirov.po_valu || 0) * (state.sirov.po_kontravalu || 0);

        // ako je papir
        if ( !sirov_area ) sirov_area = (state.sirov.kontra_tok || 0) * (state.sirov.tok || 0);

        var new_ulaz = { 
          
          original_sirov: reduce_sir(state.sirov),
          
          sifra: curr_ulaz.sifra, // već sam napravio ulaz objekt (pa makar i bio prazan) ali ima već šifru

          _id: state.sirov.sirov_id,
          kalk_full_naziv: null,

          kalk_element: null,

          kalk_po_valu: state.sirov.po_valu || state.sirov.kontra_tok || null, // ljepenka i papir imaju različita mjesta dimenzija
          kalk_po_kontravalu: state.sirov.po_kontravalu || state.sirov.tok || null,

          uzimam_po_valu: null,
          uzimam_po_kontravalu: null,

          kalk_ostatak_val: null,
          kalk_ostatak_kontra: null,

          kalk_skart: null,

          kalk_unit_area: sirov_area/1000000,

          kalk_unit: state.sirov.kalk_unit || null,

          kalk_unit_price: state.sirov.cijena || null,
          real_unit_price: state.sirov.cijena || null,

          kalk_price: state.sirov.cijena * (curr_ulaz.kalk_sum_sirovina || 0),
          kalk_kom_in_product: null,
          kalk_kom_in_sirovina: 1,

          kalk_kom_val: 1,
          kalk_kom_kontra: 1,

          kalk_sum_sirovina: curr_ulaz.kalk_sum_sirovina || 0,
          kalk_extra_kom: null,
          kalk_skart: null,
          
          kalk_plate_naziv: state.naziv + ` --- ` + state.sirov.full_naziv, // spoji naziv forme i naziv ploče za tu formu
          kalk_plate_id: state.plate_id,

          kalk_noz_big: state.noz_big,
          kalk_nest_count: state.nest_count,
          kalk_color_cover: null,
          
          kalk_color_C:  state.color_C || 0,
          kalk_color_M:  state.color_M || 0,
          kalk_color_Y:  state.color_Y || 0,
          kalk_color_K:  state.color_K || 0,
          kalk_color_W:  state.color_W || 0,
          kalk_color_V:  state.color_V || 0,
          
          
          vrsta_materijala: state.sirov.vrsta_materijala || null,
          klasa_sirovine: state.klasa_sirovine || null,
          tip_sirovine: state.tip_sirovine || null,

          
          kvaliteta_1: state.sirov.kvaliteta_1 || null,
          kvaliteta_2: state.sirov.kvaliteta_2 || null,
          debljina_mm: state.sirov.debljina_mm || null,
          gramaza_g: state.sirov.gramaza_g || null,
          
          
        };
        
        curr_ulaz.kalk_element = null;
        
        curr_proces.ulazi = upsert_item( curr_proces.ulazi, new_ulaz, `sifra`);
        
        
        if ( !curr_proces.extra_data ) curr_proces.extra_data = {};
        if ( !curr_proces.extra_data.forme ) curr_proces.extra_data.forme = [];
        
        $.each( curr_proces.ulazi, function(u_ind, proc_ulaz) {
          if ( proc_ulaz.kalk_element && proc_ulaz.kalk_plate_naziv ) {
            curr_proces.extra_data.forme = insert_uniq( curr_proces.extra_data.forme, proc_ulaz.kalk_plate_naziv.split(` ---`)[0] )
          };
        });
        
        curr_proces.extra_data.forme = insert_uniq( curr_proces.extra_data.forme, state.naziv );
        
        var full_plate_name = new_ulaz.kalk_plate_naziv;
        $('#'+current_input_id).val(full_plate_name);
        $('#'+current_input_id).parent().attr(`data-full_name`, full_plate_name);
        
        // obriši text iz polja za sirovinu i za element
        
        $(`#` + curr_kalk.kalk_rand + `_` + curr_ulaz.sifra + `_kalk_find_sirov`).val(``);
        $(`#` + curr_kalk.kalk_rand + `_` + curr_ulaz.sifra + `_ulaz_element`).val(``);
        
        console.log(curr_proces.ulazi);
        
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, changed_ulaz=true, null, null );
        
        // setTimeout( function() {
        console.log(`--------------- make kalk IN ULAZ PLATE SELECT ----------------`);
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null, `return_html` );
        $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
        reset_tooltips();

        // }, 300);
        // this_module.write_prirez(null, this_comp_id, product_id, kalk_type, curr_kalk.kalk_sifra );
        

      },

    }); // kraj od kalk_find_plate

    
    // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
    var product_id = $('#'+data.id).closest('.product_comp')[0].id;
    var product_data =  this_module.product_module.cit_data[product_id];
    
    
    $(`#`+kalk.kalk_rand+`_`+ ulaz_sifra + `_ulaz_element`).data('cit_props', {
      
      show_on_click: true,
      
      desc: 'odabir ulaza ali od definiranih elemenata u kalkulaciji : ' + ulaz_sifra,
      local: true,
      show_cols: [
        "elem_opis",
        "elem_on_product",
      ],
      col_widths:[
      
        3, // "elem_opis",
        1, //"elem_on_product",
      ],
      return: {},
      
      list: function() {
        
        var product_id = $('#'+current_input_id).closest('.product_comp')[0].id;
        var product_data =  this_module.product_module.cit_data[product_id];
        return product_data.elements;
          
      },
      filter: function(elements) {
        
        var filtered_list = [];
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        var ulaz_row = $('#'+current_input_id).closest('.ulaz_row');
        
        var kalk_type = ulaz_row.attr(`data-kalk_type`);
        var kalk_sifra = ulaz_row.attr(`data-kalk_sifra`);
        var row_sifra = ulaz_row.attr(`data-row_sifra`);
        var ulaz_sifra = ulaz_row.attr(`data-ulaz_sifra`);
        
        var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
        var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null );
        var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
        
        
        
        $.each( elements, function(e_ind, elem) {
          
          var curr_elem = elem;
          var elem_already_in_kalk = false;
          
          // ako je user već negdje izabrao ovaj element onda ga više nemoj prikazivati u drop listi
          // ZATO ŠTO NE MOŽEŠ IZABRATI JEDAN TE ISTI ELEMENT KAO ULAZ NA DVA RAZLIČITA PROCESA TJ RADNE STANICE !!!!
          
          $.each( curr_kalk.procesi, function(p_ind, proc) {
            
            $.each( proc.ulazi, function(u_ind, ulaz) {
              if ( ulaz.kalk_element?.sifra == curr_elem.sifra ) elem_already_in_kalk = true;
            });
            
            /*
            $.each( proc.izlazi, function(i_ind, izlaz) {
              if ( izlaz.kalk_element?.sifra == curr_elem.sifra ) elem_already_in_kalk = true;
            });
            */
            
          });
          
          // curr_elem.elem_tip_naziv = curr_elem.elem_tip?.naziv || "";
          
          if ( elem_already_in_kalk == false ) filtered_list.push(curr_elem);
          
        });
        
        var all_proces_elems = [];
        
        
        // moram ubaciti sve elemente koji su nastali kao izlaz iz procesa
        // mora paziti da element izlaza  NIJE do sada već dodan kao neki od ulaza
        // moram paziti da su ti elementi napravljeni PRIJE trenutnog procesa
        // DAKLE ---> nije moguće ubaciti element u neki ulaz  ako je taj element nastao u kasnijem procesu on onoga u kojeg ubacujem
        
        $.each( curr_kalk.procesi, function(p_ind, proc) {
          
          $.each( proc.izlazi, function(iz_ind, izlaz) {
            
            // ako ima kalk = true onda je element nastao unutar procesa
            if ( izlaz.kalk_element?.elem_tip?.kalk == true ) {
              
              var izlaz_elem = izlaz.kalk_element;
              
              var curr_izlaz = izlaz;
              var izlaz_vec_u_ulazu = false;
              
              $.each( curr_kalk.procesi, function(find_p_ind, find_proc) {
                $.each( find_proc.ulazi, function(find_ul_ind, find_ulaz) {
                  // ovaj izlaz element je već negdje ulaz u nekom drugom procesu
                  if ( find_ulaz.izlaz_sifra == curr_izlaz.sifra ) izlaz_vec_u_ulazu = true;
                });
              });
              
              // ako izlazni element nije NIJE NIGDJE U ULAZU
              // proces izlaznog elementa mora biti PRIJE TRENUTNOG ELEMENTA 
              // (ne smjem izabrati kalk element koji je nastao POSLJE ovog procesa jer onda nema smisla !!! )
              if ( izlaz_vec_u_ulazu == false && p_ind < curr_proces_index ) all_proces_elems.push(izlaz_elem);
              
            };
            
          }); // loop izlazi procesa
          
        }); // loop procesi
      
        // return [...filtered_list, ...all_proces_elems ];
        
        return all_proces_elems;
        
      },
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        var ulaz_row = $('#'+current_input_id).closest('.ulaz_row');
        
        var product_id = $('#'+current_input_id).closest('.product_comp')[0].id;
        var product_data =  this_module.product_module.cit_data[product_id];
        
        
        var kalk_type = ulaz_row.attr(`data-kalk_type`);
        var kalk_sifra = ulaz_row.attr(`data-kalk_sifra`);
        var row_sifra = ulaz_row.attr(`data-row_sifra`);
        var ulaz_sifra = ulaz_row.attr(`data-ulaz_sifra`);
        
        var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
        var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null );
        var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
        
        
        var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null );
        
        if ( state == null ) {
          
          $('#'+current_input_id).val(``);

          curr_ulaz.izlaz_sifra = null;
          curr_ulaz.kalk_element = null;
          curr_ulaz._id = null;
          curr_ulaz.kalk_full_naziv = "";
          curr_ulaz.kalk_po_valu = null;
          curr_ulaz.kalk_po_kontravalu = null;
          curr_ulaz.kalk_plate_naziv = null;
          curr_ulaz.kalk_plate_id = null;
          // stavljam ISTI curr ulaz ali sam samo obrisao ove propse iznad -----> u biti je ulaz ali bez ID-ja
          curr_proces.ulazi = upsert_item( curr_proces.ulazi, curr_ulaz, `sifra`);
          return;
          
        };
        
        // napravi kopiju
        state = cit_deep(state);
        
        curr_ulaz.kalk_element = state;
        
        // reducaj sve alate pošto su alati sirovine imaju previše nepotrebnih podataka pa su objekti preveliki
        if ( curr_ulaz.kalk_element?.elem_alat?.length > 0 ) {
          $.each( curr_ulaz.kalk_element.elem_alat, function(alat_ind, alat) {
            curr_ulaz.kalk_element.elem_alat[alat_ind] = reduce_sir(alat);
          });
        };
        
        // obriši polja za formu i običnu sirovinu ako je nešto bilo pisalo od prije
        $(`#` + curr_kalk.kalk_rand + `_` + curr_ulaz.sifra + `_kalk_find_sirov`).val(``);
        $(`#` + curr_kalk.kalk_rand + `_` + curr_ulaz.sifra + `_kalk_find_plate`).val(``);
        
        
        curr_ulaz._id = null;
        curr_ulaz.kalk_full_naziv = "";
        
        curr_ulaz.kalk_plate_naziv = null;
        curr_ulaz.kalk_plate_id= null;
        
        curr_ulaz.kalk_po_valu = null;
        curr_ulaz.kalk_po_kontravalu = null;
        
        curr_ulaz.uzimam_po_valu = null;
        curr_ulaz.uzimam_po_kontravalu = null;

        curr_ulaz.kalk_ostatak_val = null,
        curr_ulaz.kalk_ostatak_kontra = null,

          
        curr_proces.ulazi = upsert_item( curr_proces.ulazi, curr_ulaz, `sifra`);
        
        // pogledaj u svim procesima ali samo ako je proces manji tj ispred trenutnog procesa
        
        $.each( curr_kalk.procesi, function(p_ind, proces) {
          
          // mora biti proces ispred trenutnog
          if ( p_ind < curr_proces_index ) {
            
            // prvo pronadji kojem IZLAZU ovaj element pripada
            // i onda kopiraj podatke od tog IZLAZA
            $.each( proces.izlazi, function(iz_ind, work_izlaz) {
              
              if ( work_izlaz.kalk_element?.sifra == state.sifra ) {
                
                var izlaz_kom = work_izlaz.kalk_sum_sirovina;
                
                // ako ima izlaz kom  ----> i samo ako je taj proces s jednim izlazom !!!!
                if ( proces.extra_data?.izlaz_kom && proces.izlazi?.length == 1 ) {
                  izlaz_kom = proces.extra_data?.izlaz_kom?.kom || 0;
                };
                
                
                // kad nadjes kojem IZLAZU odabrani element pripada onda od tog izlaza kreiraj novi ulaz
                var new_ulaz = {

                  izlaz_sifra: work_izlaz.sifra,

                  sifra: curr_ulaz.sifra, // ostavi istu sifru koja je bila prije

                  _id: null,
                  prodon_sirov_id: work_izlaz.prodon_sirov_id || null,
                  kalk_full_naziv: work_izlaz.kalk_full_naziv,

                  
                  kalk_element: work_izlaz.kalk_element,
                  

                  kalk_po_valu: work_izlaz.kalk_po_valu || null,
                  kalk_po_kontravalu: work_izlaz.kalk_po_kontravalu || null,

                  uzimam_po_valu: work_izlaz.kalk_po_valu || null, // ovo je isto kao i kalk po valu
                  uzimam_po_kontravalu: work_izlaz.kalk_po_kontravalu || null, // ovo je isto kao i kalk po kontravalu 

                  kalk_ostatak_val: null,
                  kalk_ostatak_kontra: null,

                  kalk_unit_area: ( work_izlaz.kalk_po_valu || 0 ) * (work_izlaz.kalk_po_kontravalu || 0 ) / 1000000,
                  kalk_unit: "kom",
                  
                  kalk_unit_price: 0,
                  real_unit_price: 0,
                  
                  kalk_price: 0,

                  kalk_kom_in_product: work_izlaz.kalk_kom_in_product,
                  kalk_kom_in_sirovina: 1, 
                  
                  kalk_kom_val: 1,
                  kalk_kom_kontra: 1,
                  
                  kalk_sum_sirovina: izlaz_kom,
                  kalk_extra_kom: null,
                  kalk_skart: null,

                  kalk_plate_naziv: work_izlaz.kalk_plate_naziv,
                  kalk_plate_id: work_izlaz.kalk_plate_id,

                  kalk_noz_big:  work_izlaz.kalk_noz_big || null,
                  kalk_nest_count: work_izlaz.kalk_nest_count || null,
                  kalk_color_cover: work_izlaz.kalk_color_cover || null,

                  kalk_color_C: work_izlaz.kalk_color_C || null,
                  kalk_color_M: work_izlaz.kalk_color_M || null,
                  kalk_color_Y: work_izlaz.kalk_color_Y || null,
                  kalk_color_K: work_izlaz.kalk_color_K || null,
                  kalk_color_W: work_izlaz.kalk_color_W || null,
                  kalk_color_V: work_izlaz.kalk_color_V || null,
                  
                  
                  vrsta_materijala: work_izlaz.vrsta_materijala || null,
                  klasa_sirovine: work_izlaz.klasa_sirovine || null,
                  tip_sirovine: work_izlaz.tip_sirovine || null,
                  
                  
                  kvaliteta_1: work_izlaz.kvaliteta_1 || null,
                  kvaliteta_2: work_izlaz.kvaliteta_2 || null,
                  debljina_mm: work_izlaz.debljina_mm || null,
                  gramaza_g: work_izlaz.gramaza_g || null,
         
                };
                
                // SADA ZAMJENI POSTOJEĆI ULAZ SA ELEMENTOM IZ IZLAZA !!!!
                // bti će OVERRIDE JER SAM MU DAO ISTU ŠIFRU !!!
                curr_proces.ulazi = upsert_item( curr_proces.ulazi, new_ulaz, `sifra` );
                
              }; // ako je izlaz koji ima element sa istom sifrom kao odabrani state

            }); // loop po izlazima od svakog procesa ( koji je prije trenutnog )
            
          }; // samo ako je izlaz u procesu koji je prije current procesa
          
        }); // loop po svim procesima
        
        
        $('#'+current_input_id).val( state.elem_opis );

        console.log(curr_proces.ulazi);
        
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, changed_ulaz=true, null, null );
        
        console.log(`--------------- make kalk IN ULAZ ELEMENT  ----------------`);
        var this_input = $('#'+current_input_id)[0];
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null, `return_html` );
        $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
        reset_tooltips();
        
        // this_module.write_prirez(null, this_comp_id, product_id, kalk_type, curr_kalk.kalk_sifra );
        
      },
      
    });
    
    
    $(`#`+kalk.kalk_rand+`_`+ ulaz_sifra + `_ulaz_mat`).data('cit_props', {
      
      desc: 'odabir ulaz materijala unutar ulaza u procesu : ' + ulaz_sifra,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: `mat_mix`,
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        var ulaz_row = $('#'+current_input_id).closest('.ulaz_row');
        
        var product_id = $('#'+current_input_id).closest('.product_comp')[0].id;
        var product_data =  this_module.product_module.cit_data[product_id];
        
        var kalk_type = ulaz_row.attr(`data-kalk_type`);
        var kalk_sifra = ulaz_row.attr(`data-kalk_sifra`);
        var row_sifra = ulaz_row.attr(`data-row_sifra`);
        var ulaz_sifra = ulaz_row.attr(`data-ulaz_sifra`);
        
        var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
        var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null );
        var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
        
        var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null );
        
        
        if ( state == null || $('#'+current_input_id).val() == `` ) {
          
          $('#'+current_input_id).val(``);
          
          curr_ulaz.vrsta_materijala = null;
          curr_ulaz.kvaliteta_1 = null;
          curr_ulaz.kvaliteta_2 = null;
          curr_ulaz.debljina_mm = null;
          curr_ulaz.gramaza_g = null;

          return;
        };
        
        $('#'+current_input_id).val(state.naziv);
        
        curr_ulaz.vrsta_materijala = state.vrsta_materijala;
        curr_ulaz.kvaliteta_1 = state.kvaliteta_1;
        curr_ulaz.kvaliteta_2 = state.kvaliteta_2;
        curr_ulaz.debljina_mm = state.debljina_mm;
        curr_ulaz.gramaza_g = state.gramaza_g;


        console.log( curr_ulaz );
        
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, changed_ulaz=true, null, null );
        
        console.log(`--------------- make kalk IN ULAZ MATERIJAL ----------------`);
        var this_input = $('#'+current_input_id)[0];
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null, `return_html` );
        $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
        reset_tooltips();
        
        
      },
      
    });
    
    $(`#${kalk.kalk_sifra}_kalkulacija .delete_ulaz`).off(`click`);
    $(`#${kalk.kalk_sifra}_kalkulacija .delete_ulaz`).on(`click`, function() {
      
      var this_input = this;
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var kalk_type = $(this).attr(`data-kalk_type`);
      var kalk_sifra = $(this).attr(`data-kalk_sifra`);
      var row_sifra = $(this).attr(`data-row_sifra`);
      var ulaz_sifra = $(this).attr(`data-ulaz_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null );
      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
      
      
      if ( kalk_type !== `offer_kalk` ) {
        popup_warn(`Moguće je brisati samo ulaze u kalkulaciji za PONUDU !!!`);
        return;
      };
      
      if ( kalk_type == `offer_kalk` && data.pro_kalk?.length > 0 ) {
        popup_warn(`Ne možete brisati ulaze u kalkulaciji za PONUDU, ako ste već odredili kalkulaciju za NK!!!`);
        return;
      };
      
      
      console.log( curr_proces.ulazi );
      
      curr_proces.ulazi = delete_item( curr_proces.ulazi, ulaz_sifra , `sifra` );
        
      var ulaz_row = $(this).closest(`.ulaz_row`);
      
      ulaz_row.remove();
      
      console.log( curr_proces.ulazi );
      
      this_module.update_kalk( curr_kalk, product_data, curr_proces_index, changed_ulaz=true, null, null );

      console.log(`--------------- make kalk kalda kliknem na delete_ulaz ----------------`);
      var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null, `return_html` );
      $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      reset_tooltips();
      
      // alert(`kliknuo na novi ulaz btn`);
      
    });
    
    
    $(`#`+kalk.kalk_rand+`_`+ ulaz_sifra + `_ulaz_kom_in_sirovina`).off(`blur`);
    $(`#`+kalk.kalk_rand+`_`+ ulaz_sifra + `_ulaz_kom_in_sirovina`).on(`blur`, async function() {
      
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var ulaz_row = $(this_input).closest('.ulaz_row');

      var kalk_type = ulaz_row.attr(`data-kalk_type`);
      var kalk_sifra = ulaz_row.attr(`data-kalk_sifra`);
      var row_sifra = ulaz_row.attr(`data-row_sifra`);
      var ulaz_sifra = ulaz_row.attr(`data-ulaz_sifra`);
      
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null ); 
      
      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
      
      
      var input_value = cit_number( $(this_input).val() );
      var same = cit_is_same( input_value, curr_ulaz.kalk_kom_in_sirovina );
      
      
      if ( $(this_input).val() == `` ) {
        curr_ulaz.kalk_kom_in_sirovina = null;
        popup_warn(`Ako želite obrisati ovaj ulaz kliknite na CRVENI MINUS na početku reda !!!`);
      };
      
      if ( input_value !== null ) {
      
        curr_ulaz.kalk_kom_in_sirovina = input_value;
        
        // ako je ovo prava sirovina, a ne element i ako nije pomoćni element !!!!
        // ZA SADA RAČUNAJ SVE  ---------> && curr_ulaz.original_sirov?.tip_sirovine?.sifra !== `TSIR11`
        if ( curr_ulaz._id && !same ) {
          
          var DB_sirovina = await ajax_find_query( 
            { _id: { $in: [curr_ulaz._id] }, on_date: (product_data?.rok_za_def || null)  },
            `/find_sirov`,
            this_module.find_sirov.find_sirov_filter 
          );
          // pošto mi vraća array uzmi samo prvi (i jedini ) item
          if ( DB_sirovina?.length > 0 ) DB_sirovina = DB_sirovina[0];
          
          var used_in_kalk = all_other_kolicina(curr_kalk, curr_ulaz);
          
          if ( DB_sirovina.forcast_kolicina < kom_plus_sum_plus_extra(curr_ulaz) + used_in_kalk ) {
            
            var already_used_text = ``;
            if ( used_in_kalk ) already_used_text = `<br>U ovoj kalkulaciji već je iskorišteno ${cit_format(used_in_kalk, 2) }`;
            
            
            curr_ulaz.kalk_kom_in_sirovina = null;
            
            $(`[data-ulaz_sifra="${curr_ulaz.sifra}"]`).addClass(`cit_error`);
            popup_warn(`Nema dovoljno robe!!!<br>Trenutno dostupna količina = ${ cit_format(DB_sirovina.forcast_kolicina - used_in_kalk, 2) }${already_used_text}`);
            return;
            
          };
          
          
          curr_ulaz.kalk_price = kom_plus_sum_plus_extra(curr_ulaz) * (curr_ulaz.real_unit_price || curr_ulaz.kalk_unit_price);
          $( '#' + curr_kalk.kalk_rand  + `_` +  curr_ulaz.sifra + `_ulaz_price` ).val( cit_format( curr_ulaz.kalk_price, 2 ) );
          
        };
        
        
        $(`[data-ulaz_sifra="${curr_ulaz.sifra}"]`).removeClass(`cit_error`);
        // OVERRIDE AKO SE ALAT NAPLAĆUJE ODVOJENO
        // OVERRIDE AKO SE ALAT NAPLAĆUJE ODVOJENO
        // OVERRIDE AKO SE ALAT NAPLAĆUJE ODVOJENO
        // KS5 znači alat
        if ( curr_ulaz.klasa_sirovine?.sifra == `KS5` && curr_proces.extra_data?.alat_apart == true ) curr_ulaz.kalk_price = 0;
        
        $(this_input).val( cit_format( input_value, this_module.valid.ulaz_kom_in_sirovina.decimals ) );
        
        // this_module.cut_plate( this_input );

        
      }
      else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);
        return;
      };
      
      if ( !same ) {
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, changed_ulaz=true, null, null );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };
            
      
      
    });
 
    
    $(`#`+kalk.kalk_rand+`_`+ ulaz_sifra + `_ulaz_sum_sirovina`).off(`blur`);
    $(`#`+kalk.kalk_rand+`_`+ ulaz_sifra + `_ulaz_sum_sirovina`).on(`blur`, async function() {
      
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var ulaz_row = $(this_input).closest('.ulaz_row');

      var kalk_type = ulaz_row.attr(`data-kalk_type`);
      var kalk_sifra = ulaz_row.attr(`data-kalk_sifra`);
      var row_sifra = ulaz_row.attr(`data-row_sifra`);
      var ulaz_sifra = ulaz_row.attr(`data-ulaz_sifra`);
      
      var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null ); 
      
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null ); 
      
      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
      
      
      var input_value = cit_number( $(this_input).val() );
      var same = cit_is_same( input_value, curr_ulaz.kalk_sum_sirovina );
      
      
      if ( $(this_input).val() == `` ) {
        curr_ulaz.kalk_sum_sirovina = null;
        popup_warn(`Ako želite obrisati ovaj ulaz kliknite na CRVENI MINUS na početku reda !!!`);
      };
      
      if ( input_value !== null ) {
      
        curr_ulaz.kalk_sum_sirovina = input_value;
        
        // ako je ovo prava sirovina, a ne element i ako nije pomoćni element !!!!
        // ZA SADA RAČUNAJ SVE  ---------> && curr_ulaz.original_sirov?.tip_sirovine?.sifra !== `TSIR11`
        if ( curr_ulaz._id && !same ) {
          
          var DB_sirovina = await ajax_find_query( 
            { _id: { $in: [curr_ulaz._id] }, on_date: (product_data?.rok_za_def || null)  },
            `/find_sirov`,
            this_module.find_sirov.find_sirov_filter 
          );
          // pošto mi vraća array uzmi samo prvi (i jedini ) item
          if ( DB_sirovina?.length > 0 ) DB_sirovina = DB_sirovina[0];
          
          var used_in_kalk = all_other_kolicina(curr_kalk, curr_ulaz);
          
          // OBRIŠI AKO POSTOJI DEFICIT  ----> TO SAM IZRAČUNAO AKD SE NA PRIMJER KOPIRA PROIZVOD 
          // ---> to je u func    change_elem_i_kalk_sifre      u PRODUCT modulu
          curr_ulaz.deficit = false;
          
          if ( DB_sirovina.forcast_kolicina < kom_plus_sum_plus_extra(curr_ulaz) + used_in_kalk ) {
            
            var already_used_text = ``;
            if ( used_in_kalk ) already_used_text = `<br>U ovoj kalkulaciji već je iskorišteno ${cit_format(used_in_kalk, 2) }`;
            
            
            curr_ulaz.kalk_sum_sirovina = null;
            
            $(`[data-ulaz_sifra="${curr_ulaz.sifra}"]`).addClass(`cit_error`);
            popup_warn(`Nema dovoljno robe!!!<br>Trenutno dostupna količina = ${ cit_format(DB_sirovina.forcast_kolicina - used_in_kalk, 2) }${already_used_text}`);
            return;
            
          };
          
          
          curr_ulaz.kalk_price = kom_plus_sum_plus_extra(curr_ulaz) * (curr_ulaz.real_unit_price || curr_ulaz.kalk_unit_price);
          $( '#' + curr_kalk.kalk_rand  + `_` +  curr_ulaz.sifra + `_ulaz_price` ).val( cit_format( curr_ulaz.kalk_price, 2 ) );
          
        };
        
        
        $(`[data-ulaz_sifra="${curr_ulaz.sifra}"]`).removeClass(`cit_error`);
        // OVERRIDE AKO SE ALAT NAPLAĆUJE ODVOJENO
        // OVERRIDE AKO SE ALAT NAPLAĆUJE ODVOJENO
        // OVERRIDE AKO SE ALAT NAPLAĆUJE ODVOJENO
        // KS5 znači alat
        if ( curr_ulaz.klasa_sirovine?.sifra == `KS5` && curr_proces.extra_data?.alat_apart == true ) curr_ulaz.kalk_price = 0;
        
        $(this_input).val( cit_format( input_value, this_module.valid.ulaz_sum_sirovina.decimals ) );
        
        // this_module.cut_plate( this_input );

        
      }
      else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);
        return;
      };
      
      if ( !same ) {
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, changed_ulaz=true, null, null );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };
      
    });
    
    
    $(`#`+kalk.kalk_rand+`_`+ ulaz_sifra + `_ulaz_extra_kom`).off(`blur`);
    $(`#`+kalk.kalk_rand+`_`+ ulaz_sifra + `_ulaz_extra_kom`).on(`blur`, async function() {
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var ulaz_row = $(this_input).closest('.ulaz_row');

      var kalk_type = ulaz_row.attr(`data-kalk_type`);
      var kalk_sifra = ulaz_row.attr(`data-kalk_sifra`);
      var row_sifra = ulaz_row.attr(`data-row_sifra`);
      var ulaz_sifra = ulaz_row.attr(`data-ulaz_sifra`);
      
      var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null ); 
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null ); 
      
      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
      
      var input_value = cit_number( $(this_input).val() );
      var same = cit_is_same( input_value, curr_ulaz.kalk_extra_kom );
      
      if ( $(this_input).val() == `` ) {
        curr_ulaz.kalk_extra_kom = null;
      };
      
      if ( input_value !== null ) {
      
        curr_ulaz.kalk_extra_kom = input_value;
        
        // ako je ovo prava sirovina, a ne element i ako nije pomoćni element !!!!
        // ZA SADA RAČUNAJ SVE  ---------> && curr_ulaz.original_sirov?.tip_sirovine?.sifra !== `TSIR11`
        if ( curr_ulaz._id && !same ) {
          
          var DB_sirovina = await ajax_find_query( 
            { _id: { $in: [curr_ulaz._id] }, on_date: (product_data?.rok_za_def || null)  },
            `/find_sirov`,
            this_module.find_sirov.find_sirov_filter 
          );
          // pošto mi vraća array uzmi samo prvi (i jedini ) item
          if ( DB_sirovina?.length > 0 ) DB_sirovina = DB_sirovina[0];
          
          
          var used_in_kalk = all_other_kolicina(curr_kalk, curr_ulaz);
          if ( DB_sirovina.forcast_kolicina < kom_plus_sum_plus_extra(curr_ulaz) + used_in_kalk ) {
            
            var already_used_text = ``;
            if ( used_in_kalk ) already_used_text = `<br>U ovoj kalkulaciji već je iskorišteno ${cit_format(used_in_kalk, 2) }`;
            
            curr_ulaz.kalk_extra_kom = null;
            $(`[data-ulaz_sifra="${curr_ulaz.sifra}"]`).addClass(`cit_error`);
            popup_warn(`Nema dovoljno robe!!!<br>Trenutno dostupna količina = ${ cit_format(DB_sirovina.forcast_kolicina - used_in_kalk, 2) }${already_used_text}`);
            return;
            
          };
          
          curr_ulaz.kalk_price = kom_plus_sum_plus_extra(curr_ulaz) * (curr_ulaz.real_unit_price || curr_ulaz.kalk_unit_price);
          $( '#' + curr_kalk.kalk_rand  + `_` +  curr_ulaz.sifra + `_ulaz_price` ).val( cit_format( curr_ulaz.kalk_price, 2 ) );
          
        };
        
        
        $(`[data-ulaz_sifra="${curr_ulaz.sifra}"]`).removeClass(`cit_error`);
        
        // OVERRIDE AKO SE ALAT NAPLAĆUJE ODVOJENO
        // OVERRIDE AKO SE ALAT NAPLAĆUJE ODVOJENO
        // OVERRIDE AKO SE ALAT NAPLAĆUJE ODVOJENO
        // KS5 znači alat
        if ( curr_ulaz.klasa_sirovine?.sifra == `KS5` && curr_proces.extra_data?.alat_apart == true ) curr_ulaz.kalk_price = 0;
        
        $(this_input).val( cit_format( input_value, this_module.valid.ulaz_extra_kom.decimals ) );

        // this_module.cut_plate( this_input );
        
      }
      else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);
        return;

      };
      
      
      if ( !same ) {
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, changed_ulaz=true, null, null );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };

    });
    
    
    $(`#`+kalk.kalk_rand+`_`+ ulaz_sifra + `_ulaz_noz_big`).off(`blur`);
    $(`#`+kalk.kalk_rand+`_`+ ulaz_sifra + `_ulaz_noz_big`).on(`blur`, async function() {
      
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var ulaz_row = $(this_input).closest('.ulaz_row');

      var kalk_type = ulaz_row.attr(`data-kalk_type`);
      var kalk_sifra = ulaz_row.attr(`data-kalk_sifra`);
      var row_sifra = ulaz_row.attr(`data-row_sifra`);
      var ulaz_sifra = ulaz_row.attr(`data-ulaz_sifra`);
      
      var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null ); 
      
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null ); 
      
      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
      
      var input_value = cit_number( $(this_input).val() );
      var same = cit_is_same( input_value, curr_ulaz.kalk_noz_big );
      
      
      if ( $(this_input).val() == `` ) {
        curr_ulaz.kalk_noz_big = null;
      };
      
      if ( input_value !== null ) {
      
        curr_ulaz.kalk_noz_big = input_value;
        $(this_input).val( cit_format( input_value, 2 ) );
        
      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
        
        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);
        return;
        
      };
      
      
      if ( !same ) {
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, changed_ulaz=true, null, null );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };
      
      
    });


    $(`#`+kalk.kalk_rand+`_`+ ulaz_sifra + `_kalk_nest_count`).off(`blur`);
    $(`#`+kalk.kalk_rand+`_`+ ulaz_sifra + `_kalk_nest_count`).on(`blur`, async function() {
      
      
      var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var ulaz_row = $(this_input).closest('.ulaz_row');

      var kalk_type = ulaz_row.attr(`data-kalk_type`);
      var kalk_sifra = ulaz_row.attr(`data-kalk_sifra`);
      var row_sifra = ulaz_row.attr(`data-row_sifra`);
      var ulaz_sifra = ulaz_row.attr(`data-ulaz_sifra`);
      
      var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null ); 
      
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null ); 
      
      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
      
      var input_value = cit_number( $(this_input).val() );
      var same = cit_is_same( input_value, curr_ulaz.kalk_nest_count );
      
      if ( $(this_input).val() == `` ) {
        curr_ulaz.kalk_nest_count = null;
      };
      
      if ( input_value !== null ) {
        curr_ulaz.kalk_nest_count = input_value;
        $(this_input).val( cit_format( input_value, 2 ) );
        
      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!
        
        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);
        return;
      };
      
      if ( !same ) {
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, changed_ulaz=true, null, null );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };
      
    });

    
    
    
    

    $(`#` + kalk.kalk_rand + `_` + ulaz_sifra + `_kalk_glue_mm`).off(`blur`);
    $(`#` + kalk.kalk_rand + `_` + ulaz_sifra + `_kalk_glue_mm`).on(`blur`, function() {


      var this_input = this;

      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var ulaz_row = $(this_input).closest('.ulaz_row');

      var kalk_type = ulaz_row.attr(`data-kalk_type`);
      var kalk_sifra = ulaz_row.attr(`data-kalk_sifra`);
      var row_sifra = ulaz_row.attr(`data-row_sifra`);
      var ulaz_sifra = ulaz_row.attr(`data-ulaz_sifra`);

      var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null ); 


      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null ); 

      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );

      if ( $(this_input).val() == `` ) {
        curr_ulaz.kalk_glue_mm = null;
      };

      var input_value = cit_number( $(this_input).val() );
      var same = cit_is_same( input_value, curr_ulaz.kalk_glue_mm );
      
      if ( input_value !== null ) {

        curr_ulaz.kalk_glue_mm = input_value;
        $(this_input).val( cit_format( input_value, 0 ) );
        
      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!

        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);
        return;
      };
      
      if ( !same ) {
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, changed_ulaz=true, null, null );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };

    });

    
    $(`#` + kalk.kalk_rand + `_` + ulaz_sifra + `_kalk_glue_points`).off(`blur`);
    $(`#` + kalk.kalk_rand + `_` + ulaz_sifra + `_kalk_glue_points`).on(`blur`, function() {


      var this_input = this;

      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var ulaz_row = $(this_input).closest('.ulaz_row');

      var kalk_type = ulaz_row.attr(`data-kalk_type`);
      var kalk_sifra = ulaz_row.attr(`data-kalk_sifra`);
      var row_sifra = ulaz_row.attr(`data-row_sifra`);
      var ulaz_sifra = ulaz_row.attr(`data-ulaz_sifra`);

      var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null ); 


      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null ); 

      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );

      if ( $(this_input).val() == `` ) {
        curr_ulaz.kalk_glue_points = null;
      };

      var input_value = cit_number( $(this_input).val() );
      var same = cit_is_same( input_value, curr_ulaz.kalk_glue_points );

      if ( input_value !== null ) {

        curr_ulaz.kalk_glue_points = input_value;
        $(this_input).val( cit_format( input_value, 0 ) );

      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!

        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);

      };
      if ( !same ) {
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, changed_ulaz=true, null, null );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };
      

    });
    

    $(`#` + kalk.kalk_rand + `_` + ulaz_sifra + `_kalk_glue_speed`).off(`blur`);
    $(`#` + kalk.kalk_rand + `_` + ulaz_sifra + `_kalk_glue_speed`).on(`blur`, function() {

      var this_input = this;

      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var ulaz_row = $(this_input).closest('.ulaz_row');

      var kalk_type = ulaz_row.attr(`data-kalk_type`);
      var kalk_sifra = ulaz_row.attr(`data-kalk_sifra`);
      var row_sifra = ulaz_row.attr(`data-row_sifra`);
      var ulaz_sifra = ulaz_row.attr(`data-ulaz_sifra`);

      var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null ); 


      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null ); 

      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );


      if ( $(this_input).val() == `` ) {
        curr_ulaz.kalk_glue_speed = null;
      };

      var input_value = cit_number( $(this_input).val() );
      var same = cit_is_same( input_value, curr_ulaz.kalk_glue_speed );

      if ( input_value !== null ) {

        curr_ulaz.kalk_glue_speed = input_value;
        $(this_input).val( cit_format( input_value, 2 ) );

      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!

        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);

      };
      
      if ( !same ) {
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, changed_ulaz=true, null, null );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };

    });


    $(`#` + kalk.kalk_rand + `_` + ulaz_sifra + `_kalk_glue_tlak`).data('cit_props', {

      desc: 'odabir tlaka unutar sirovine ljepilo ako je doradno ljepljenje : ' + ulaz_sifra,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: `glue_tlak_list`,
      cit_run: async function(state) {
        
        var this_input = $('#'+current_input_id)[0];

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        var ulaz_row = $('#'+current_input_id).closest('.ulaz_row');

        var product_id = $('#'+current_input_id).closest('.product_comp')[0].id;
        var product_data =  this_module.product_module.cit_data[product_id];


        var kalk_type = ulaz_row.attr(`data-kalk_type`);
        var kalk_sifra = ulaz_row.attr(`data-kalk_sifra`);
        var row_sifra = ulaz_row.attr(`data-row_sifra`);
        var ulaz_sifra = ulaz_row.attr(`data-ulaz_sifra`);

        var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
        var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null );
        var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );


        var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null );

        var same = cit_is_same( state, curr_ulaz.kalk_glue_tlak, `sifra` );

        if ( state == null ) {
          $(this_input).val(``);
          curr_ulaz.kalk_glue_tlak = null;
          return;
        };
        
        curr_ulaz.kalk_glue_tlak = state;

        $(this_input).val( state.naziv );

        if ( !same ) {
          this_module.update_kalk( curr_kalk, product_data, curr_proces_index, changed_ulaz=true, null, null );
          var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
          // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
        };
        
        
      },


    });


    $(`#` + kalk.kalk_rand + `_` + ulaz_sifra + `_kalk_glue_dizna`).data('cit_props', {

      desc: 'odabir DIZNE unutar sirovine ljepilo ako je doradno ljepljenje : ' + ulaz_sifra,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: `glue_dizna_list`,
      cit_run: async function(state) {
        
        var this_input = $('#'+current_input_id)[0];

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        var ulaz_row = $('#'+current_input_id).closest('.ulaz_row');

        var product_id = $('#'+current_input_id).closest('.product_comp')[0].id;
        var product_data =  this_module.product_module.cit_data[product_id];


        var kalk_type = ulaz_row.attr(`data-kalk_type`);
        var kalk_sifra = ulaz_row.attr(`data-kalk_sifra`);
        var row_sifra = ulaz_row.attr(`data-row_sifra`);
        var ulaz_sifra = ulaz_row.attr(`data-ulaz_sifra`);

        var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
        var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null );
        var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );


        var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null );

        var same = cit_is_same( state, curr_ulaz.kalk_glue_dizna, `sifra` );

        if ( state == null ) {
          $(this_input).val(``);
          curr_ulaz.kalk_glue_dizna = null;
          return;
        };

        curr_ulaz.kalk_glue_dizna = state;

        $(this_input).val( state.naziv );

        if ( !same ) {
          this_module.update_kalk( curr_kalk, product_data, curr_proces_index, changed_ulaz=true, null, null );
          var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
          // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
        };
        
        
      },


    });


    $(`#` + kalk.kalk_rand + `_` + ulaz_sifra + `_kalk_glue_m2`).data('cit_props', {

      desc: 'odabir površine unutar sirovine ljepilo ako je doradno ljepljenje : ' + ulaz_sifra,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: `glue_m2_list`,
      cit_run: async function(state) {
        
        var this_input = $('#'+current_input_id)[0];

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        var ulaz_row = $('#'+current_input_id).closest('.ulaz_row');

        var product_id = $('#'+current_input_id).closest('.product_comp')[0].id;
        var product_data =  this_module.product_module.cit_data[product_id];


        var kalk_type = ulaz_row.attr(`data-kalk_type`);
        var kalk_sifra = ulaz_row.attr(`data-kalk_sifra`);
        var row_sifra = ulaz_row.attr(`data-row_sifra`);
        var ulaz_sifra = ulaz_row.attr(`data-ulaz_sifra`);

        var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
        var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null );
        var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );


        var curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null );
        
        var same = cit_is_same( state, curr_ulaz.kalk_glue_m2, `sifra` );
        
        if ( state == null ) {
          $(this_input).val(``);
          curr_ulaz.kalk_glue_m2 = null;
          return;
        };

        curr_ulaz.kalk_glue_m2 = state;

        $(this_input).val( state.naziv );
        
        
        if ( !same ) {
          this_module.update_kalk( curr_kalk, product_data, curr_proces_index, changed_ulaz=true, null, null );
          var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
          // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
        };
        
        
      },


    });

    
  };
  this_module.register_kalk_ulaz_events = register_kalk_ulaz_events;
  
  
  async function register_kalk_izlaz_events(data, izlaz_sifra, proces, kalk, kalk_type, tip_proizvoda ) {
    
 
    // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
    var product_id = $('#'+data.id).closest('.product_comp')[0].id;
    var product_data =  this_module.product_module.cit_data[product_id];
    
    
    $(`#` + kalk.kalk_sifra + `_center_scroll_box .kalk_izlaz_box .cit_input`).off(`click`);
    $(`#` + kalk.kalk_sifra + `_center_scroll_box .kalk_izlaz_box .cit_input`).on(`click`, function() {
      window.current_input_id = this.id;
      console.log(` ------------------ KLICK register_kalk_izlaz_events ------------------------ ` + window.current_input_id);
    });
    
    
    
    $(`#` + kalk.kalk_rand  + `_` + izlaz_sifra + `_izlaz_element`).data('cit_props', {
      
      desc: 'odabir IZLAZA ali od definiranih elemenata u productu : ' + izlaz_sifra,
      local: true,
      show_cols: [
        "elem_opis",
        "elem_on_product",
      ],
      col_widths:[
        3, // "elem_opis",
        1, //"elem_on_product",
      ],
      return: {},
      show_on_click: true,
      list: product_data.elements,
      
      filter: function(elements) {
        
        var filtered_list = [];
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        var izlaz_row = $('#'+current_input_id).closest('.izlaz_row');
        
        var kalk_type = izlaz_row.attr(`data-kalk_type`);
        var kalk_sifra = izlaz_row.attr(`data-kalk_sifra`);
        var row_sifra = izlaz_row.attr(`data-row_sifra`);
        var izlaz_sifra = izlaz_row.attr(`data-izlaz_sifra`);
        
        var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
        
        $.each( elements, function(e_ind, elem) {
          
          var curr_elem = elem;
          var elem_already_in_kalk = false;
          
          $.each( curr_kalk.procesi, function(p_ind, proc) {
            
            
            // $.each( proc.ulazi, function(u_ind, ulaz) {
            //   if ( ulaz.kalk_element?.sifra == curr_elem.sifra ) elem_already_in_kalk = true;
            // });
            
            $.each( proc.izlazi, function(i_ind, izlaz) {
              if ( izlaz.kalk_element?.sifra == curr_elem.sifra ) elem_already_in_kalk = true;
            });
            
          });
          
          
          // curr_elem.elem_tip_naziv = curr_elem.elem_tip?.naziv || "";
          
          if ( elem_already_in_kalk == false ) filtered_list.push(curr_elem);
          
        });
        
        return filtered_list;
          
        
        
      },
      
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        
        var izlaz_row = $('#'+current_input_id).closest('.izlaz_row');
        
        var kalk_type = izlaz_row.attr(`data-kalk_type`);
        var kalk_sifra = izlaz_row.attr(`data-kalk_sifra`);
        var row_sifra = izlaz_row.attr(`data-row_sifra`);
        var izlaz_sifra = izlaz_row.attr(`data-izlaz_sifra`);
        
        
        var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
        var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null );
        var curr_izlaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, izlaz_sifra, null );

        
        
        if ( state == null ) {
          
          $('#'+current_input_id).val(``);
          
          curr_izlaz.kalk_element = null;
          curr_proces.izlazi = upsert_item( curr_proces.izlazi, empty_curr_izlaz, `sifra`);
          return;
        };
        
        curr_izlaz.kalk_element = state;
        
        curr_izlaz._id = null;
        curr_izlaz.kalk_full_naziv = "";
        curr_izlaz.kalk_po_valu = null;
        curr_izlaz.kalk_po_kontravalu = null;
        
        
        curr_proces.izlazi = upsert_item( curr_proces.izlazi, curr_izlaz, `sifra`);
        
        $('#'+current_input_id).val( ( state.elem_tip?.naziv || state.elem_tip?.naziv ) + ( state.elem_opis ? "--"+state.elem_opis : "" )  );

        console.log(curr_proces.izlazi);
        
        
        console.log(`--------------- make kalk IN IZLAZ ELEMENT SELECT ----------------`);
        var this_input = $('#'+current_input_id)[0];
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
        
      },
    });
    


    $(`#`+kalk.kalk_rand+`_`+ izlaz_sifra + `_izlaz_sum_sirovina`).off(`blur`);
    $(`#`+kalk.kalk_rand+`_`+ izlaz_sifra + `_izlaz_sum_sirovina`).on(`blur`, async function() {

      var this_input = this;

      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];

      var izlaz_row = $(this_input).closest('.izlaz_row');

      var kalk_type = izlaz_row.attr(`data-kalk_type`);
      var kalk_sifra = izlaz_row.attr(`data-kalk_sifra`);
      var row_sifra = izlaz_row.attr(`data-row_sifra`);
      var izlaz_sifra = izlaz_row.attr(`data-izlaz_sifra`);

      var curr_izlaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, izlaz_sifra, null ); 

      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_izlaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, izlaz_sifra, null ); 

      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );

      var input_value = cit_number( $(this_input).val() );
      
      if ( !curr_proces.extra_data ) curr_proces.extra_data = {};
      
      var same = cit_is_same( input_value, curr_izlaz.kalk_sum_sirovina );
      

      if ( $(this_input).val() == `` ) {
        popup_warn(`Nema smisla postaviti izlaznu količinu na NULA !!!`);
        if ( curr_proces.extra_data?.izlaz_kom ) delete curr_proces.extra_data.izlaz_kom;
        setTimeout( function() { $(this_input).val( cit_format(curr_izlaz.kalk_sum_sirovina, 0) ) }, 200 );
      };

      if ( input_value !== null ) {

        curr_proces.extra_data.izlaz_kom = { sifra: curr_izlaz.sifra, kom: input_value };
        curr_izlaz.kalk_sum_sirovina = input_value;
        $(this_input).val( cit_format( input_value, this_module.valid.izlaz_sum_sirovina.decimals) );

      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!

        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);
        return;
      };

      
      if ( !same ) {
        
        // UPDATE SUM SIROVINA AKO SE OVAJ IZLAZ NEGDJE POJAVLJUJE NA ULAZU
        $.each( curr_kalk.procesi, function( p_ind, proc ) {
          if ( p_ind > curr_proces_index ) {
            $.each( proc.ulazi, function( u_ind, ulaz ) {
              if ( ulaz.izlaz_sifra == curr_izlaz.sifra ) {
                curr_kalk.procesi[p_ind].ulazi[u_ind].kalk_sum_sirovina = curr_proces.extra_data?.izlaz_kom?.kom || null;
              };
            });
          };
        });
        
        
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, null, changed_izlaz=true, null );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };
      

    });

    
    
    $(`#` + kalk.kalk_rand + `_` + izlaz_sifra + `_izlaz_val`).off(`blur`);
    $(`#` + kalk.kalk_rand + `_` + izlaz_sifra + `_izlaz_val`).on(`blur`, async function() {


      var this_input = this;

      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];


      var izlaz_row = $(this_input).closest('.izlaz_row');

      var kalk_type = izlaz_row.attr(`data-kalk_type`);
      var kalk_sifra = izlaz_row.attr(`data-kalk_sifra`);
      var row_sifra = izlaz_row.attr(`data-row_sifra`);
      var izlaz_sifra = izlaz_row.attr(`data-izlaz_sifra`);

      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_izlaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, izlaz_sifra, null ); 

      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );


      var input_value = cit_number( $(this_input).val() );
      if ( !curr_proces.extra_data ) curr_proces.extra_data = {};

      var same = cit_is_same( input_value, curr_proces.extra_data.izlaz_val );

      if ( $(this_input).val() == `` ) {
        curr_izlaz.kalk_po_valu = null;
        if ( curr_proces.extra_data?.izlaz_val ) curr_proces.extra_data.izlaz_val = null;  
      };

      if ( input_value !== null ) {

        curr_izlaz.kalk_po_valu = input_value;
        curr_proces.extra_data.izlaz_val = input_value;
        $(this_input).val( cit_format( input_value, 0 ) );

      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!

        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);
        return;
      };
      
      if ( !same ) {
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, null, changed_izlaz=true, null );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };

    });
    
    
    $(`#` + kalk.kalk_rand + `_` + izlaz_sifra + `_izlaz_kontra`).off(`blur`);
    $(`#` + kalk.kalk_rand + `_` + izlaz_sifra + `_izlaz_kontra`).on(`blur`, async function() {


      var this_input = this;

      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];


      var izlaz_row = $(this_input).closest('.izlaz_row');

      var kalk_type = izlaz_row.attr(`data-kalk_type`);
      var kalk_sifra = izlaz_row.attr(`data-kalk_sifra`);
      var row_sifra = izlaz_row.attr(`data-row_sifra`);
      var izlaz_sifra = izlaz_row.attr(`data-izlaz_sifra`);

      var curr_izlaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, izlaz_sifra, null ); 


      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      var curr_izlaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, izlaz_sifra, null ); 

      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );



      var input_value = cit_number( $(this_input).val() );
      if ( !curr_proces.extra_data ) curr_proces.extra_data = {};
      
      var same = cit_is_same( input_value, curr_proces.extra_data.izlaz_kontra );

      if ( $(this_input).val() == `` ) {
        curr_izlaz.kalk_po_kontravalu = null;
        if ( curr_proces.extra_data?.izlaz_kontra ) curr_proces.extra_data.izlaz_kontra = null;  
      };

      if ( input_value !== null ) {

        curr_izlaz.kalk_po_kontravalu = input_value;

        curr_proces.extra_data.izlaz_kontra = input_value;
        $(this_input).val( cit_format( input_value, 0 ) );
        
      } else if ( $(this_input).val() !== `` ) { // ako cit number jeste null ali polje nije prazno !!

        popup_warn(`Nije broj :-P`);
        $(this_input).val(``);
        return;
      };

      
      if ( !same ) {
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, false, changed_izlaz=true, null );
        var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      };

    });

    
    $(`#${kalk.kalk_sifra}_kalkulacija .delete_izlaz`).off(`click`);
    $(`#${kalk.kalk_sifra}_kalkulacija .delete_izlaz`).on(`click`, function() {


      var this_input = this;
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];


      var kalk_type = $(this).attr(`data-kalk_type`);
      var kalk_sifra = $(this).attr(`data-kalk_sifra`);
      var row_sifra = $(this).attr(`data-row_sifra`);
      var izlaz_sifra = $(this).attr(`data-izlaz_sifra`);

      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null );
      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
      
      
      if ( kalk_type !== `offer_kalk` ) {
        popup_warn(`Moguće je brisati samo izlaze u kalkulaciji za PONUDU !!!`);
        return;
      };
      
      if ( kalk_type == `offer_kalk` && data.pro_kalk?.length > 0 ) {
        popup_warn(`Ne možete brisati izlaze u kalkulaciji za PONUDU, ako ste već odredili kalkulaciju za NK!!!`);
        return;
      };
      

      console.log( curr_proces.izlazi );

      curr_proces.izlazi = delete_item( curr_proces.izlazi, izlaz_sifra , `sifra` );

      var izlaz_row = $(this).closest(`.izlaz_row`);

      izlaz_row.remove();

      console.log( curr_proces.izlazi );

      console.log(`--------------- make kalk kada kliknem na delete_izlaz ----------------`);
      this_module.update_kalk( curr_kalk, product_data, curr_proces_index, null, changed_izlaz=true, null );
      var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
      // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);


      reset_tooltips();

      // alert(`kliknuo na novi izlaz btn`);

    });
    
    
  };
  this_module.register_kalk_izlaz_events = register_kalk_izlaz_events;
  
  
  async function register_proces_events(data, proces, kalk, kalk_type, tip_proizvoda ) {
    
 
    // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
    var product_id = $('#'+data.id).closest('.product_comp')[0].id;
    var product_data =  this_module.product_module.cit_data[product_id];
    
    
    $(`#` + kalk.kalk_rand  + `_` + proces.row_sifra + `_proc_koment`).off(`blur`);
    $(`#` + kalk.kalk_rand  + `_` + proces.row_sifra + `_proc_koment`).on(`blur`, function() {
       
       var this_input = this;
      
      var this_comp_id = $(this_input).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var proces_row = $(this_input).closest('.proces_row');
      
      
      var kalk_type = proces_row.attr(`data-kalk_type`);
      var kalk_sifra = proces_row.attr(`data-kalk_sifra`);
      var row_sifra = proces_row.attr(`data-row_sifra`);
      
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      
      if ( $(this_input).val() == `` ) {
        curr_proces.proc_koment = null;
      } else {
        curr_proces.proc_koment = $(this_input).val();
      };
      
      console.log(curr_kalk);
      
    });
     
       
    $(`#${kalk.kalk_sifra}_kalkulacija .new_kalk_ulaz_btn`).off(`click`);
    $(`#${kalk.kalk_sifra}_kalkulacija .new_kalk_ulaz_btn`).on(`click`, function() {
      
      var this_input = this;
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var kalk_type = $(this).attr(`data-kalk_type`);
      var kalk_sifra = $(this).attr(`data-kalk_sifra`);
      var row_sifra = $(this).attr(`data-row_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null );
      
      console.log( curr_proces );
      
      var new_ulaz = this_module.fresh_ulaz();
      
      curr_proces.ulazi.push(new_ulaz);
      

      console.log(`--------------- make kalk kalda kliknem na new_kalk_ulaz_btn ----------------`);
      var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
      // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      
      
      reset_tooltips();
      
      // alert(`kliknuo na novi ulaz btn`);
      
    });
    
    
    
    $(`#${kalk.kalk_sifra}_kalkulacija .new_kalk_izlaz_btn`).off(`click`);
    $(`#${kalk.kalk_sifra}_kalkulacija .new_kalk_izlaz_btn`).on(`click`, function() {
      
      var this_input = this;
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      
      
      var kalk_type = $(this).attr(`data-kalk_type`);
      var kalk_sifra = $(this).attr(`data-kalk_sifra`);
      var row_sifra = $(this).attr(`data-row_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null );
      
      console.log( curr_proces );
      
      var new_izlaz = this_module.fresh_izlaz();
      
      
      curr_proces.izlazi.push(new_izlaz);
      

      console.log(`--------------- make kalk kalda kliknem na new_kalk_izlaz_btn ----------------`);
      var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
      // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      
      
      reset_tooltips();
      
      // alert(`kliknuo na novi ulaz btn`);
      
    });
    
    
    $(`#${kalk.kalk_sifra}_kalkulacija .delete_extra_pill`).off(`click`);
    $(`#${kalk.kalk_sifra}_kalkulacija .delete_extra_pill`).on(`click`, function() {
      
      var this_input = this;
      
      var this_comp_id = $(this).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
          
      var proces_row = $(this).closest('.proces_row');
      
      var kalk_type = proces_row.attr(`data-kalk_type`);
      var kalk_sifra = proces_row.attr(`data-kalk_sifra`);
      var row_sifra = proces_row.attr(`data-row_sifra`);
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null );
      
      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
      
      console.log( curr_proces );
      
      var pill = $(this).closest('.extra_data_pill');
      
      var extra_data_prop = null;
      $.each( map_extra_data_to_text, function( extra_key, extra_text ) {
        if ( pill.hasClass( extra_key ) ) extra_data_prop = extra_key;
      });
        
      
      // curr_proces.extra_data[extra_data_prop] = null;
      // posve obriši cijeli prop unutar extra data objekta    
      if ( curr_proces.extra_data && curr_proces.extra_data[extra_data_prop] )  delete curr_proces.extra_data[extra_data_prop];
        
      $(`#${kalk.kalk_sifra}_kalkulacija .proces_row`).eq(curr_proces_index).find(`.` + extra_data_prop).remove();
      
      
      console.log(`--------------- make kalk kalda kliknem na delete_extra_pill ----------------`);
      var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null );
      // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
    
      reset_tooltips();
      
      // alert(`kliknuo na novi ulaz btn`);
      
    });
    
        
    $(`#${kalk.kalk_sifra}_kalkulacija .delete_proces_row`).off(`click`);
    $(`#${kalk.kalk_sifra}_kalkulacija .delete_proces_row`).on(`click`, async function() {
      
      
      var this_button = this;
      
      var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var product_id = $('#'+data.id).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];


      var kalk_type = $(this_button).attr(`data-kalk_type`);
      var kalk_sifra = $(this_button).attr(`data-kalk_sifra`);
      var row_sifra = $(this_button).attr(`data-row_sifra`);

      
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null );
      
      
      if ( 
        kalk_type !== `offer_kalk`
        && 
        curr_proces.rn_created // ako je kreiran radni nalog 
      ) {
        popup_warn(`Morate prvo odustati od ovog radnog naloga !!!`);
        return;
      };
      
      
      if ( kalk_type == `offer_kalk` && data.pro_kalk?.length > 0 ) {
        popup_warn(`Ne možete brisati procese u kalkulaciji za PONUDU, ako ste već odredili kalkulaciju za NK!!!`);
        return;
      };
      
      
      var popup_text = `Jeste li sigurni da želite OBRISATI OVAJ PROCES?`;
      
      
      function delete_proces_yes() {
        
        var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
        var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null );

        console.log( curr_proces );

        curr_kalk.procesi = delete_item( curr_kalk.procesi, row_sifra , `row_sifra` );

        $(`.proces_row[data-row_sifra="${row_sifra}"]`).remove();

        console.log(`--------------- make kalk kada kliknem na delete proces row ----------------`);
        var this_kalk = this_module.make_kalks( this_button, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);

        reset_tooltips();

      }; 
      
      
      function delete_proces_no() {
        
        show_popup_modal(false, popup_text, null );
        
      };

      
      
      
      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', delete_proces_yes, delete_proces_no, null);
    
      
      
    });
    
    
    $(`#` + kalk.kalk_rand  + `_` + proces.row_sifra + '_proces_radni_nalog').data(`cit_run`, async function( state, this_elem ) { 
      
  
      var this_comp_id = $(this_elem).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var kalk_box = $(this_elem).closest('.kalk_box');

      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
      var proces_row = $(this_elem).closest('.proces_row');

      var kalk_type = proces_row.attr(`data-kalk_type`);
      var kalk_sifra = proces_row.attr(`data-kalk_sifra`);
      var row_sifra = proces_row.attr(`data-row_sifra`);
      
      
      var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
   
        
      var curr_reserv_sifra = curr_kalk.kalk_sifra;
      var curr_reserv_state = curr_kalk.kalk_reserv;
      
      
      var popup_text = "";
      
      if ( state == true ) {
        popup_text = 
`Jeste li sigurni da želite NAPRAVITI DODATNI RADNI NALOG SAMO ZA OVAJ PROCES?`;
      };
      
      if ( state == false ) {
        popup_text = 
`Jeste li sigurni da želite STORNIRATI RADNI NALOG OD OVOG PROCESA ?`;
      };
       
      async function proces_nalog_yes() {
        
        
                
        
        if ( state == true ) {
          
          // ------------------------------ RADNI NALOG TRUE ------------------------------
          if ( !curr_proces.extra_data ) curr_proces.extra_data = {};
          
          curr_proces.extra_data.proces_radni_nalog = true;
          curr_proces.rn_created = true;
          
          
          
          
          var proces_copy = cit_deep(curr_proces);

          $.each( proces_copy.ulazi, function(u_ind, ulaz) {
            proces_copy.ulazi[u_ind].kalk_sum_sirovina = 0;
          });

          $.each( proces_copy.izlazi, function(i_ind, izlaz) {
            proces_copy.izlazi[i_ind].kalk_sum_sirovina = 0;
          });

          proces_copy.action.action_prep_time = 0;
          proces_copy.action.action_work_time = 0;


          data.post_kalk[0].procesi.push(proces_copy);
          
          
          // ovo je kad rezerviram RN za specifičan proces
          var record_result = await this_module.rn_reserv_record( this_elem, curr_proces );
          
          this_module.make_radni_nalog( this_elem, curr_proces );
          
        } 
        else {
          
          // ------------------------------ RADNI NALOG FALSE ------------------------------
          popup_warn(`Ako želiš odustati od radnog naloga potrebno je kliknuti na prekidač ODUSTANI !!!`);
          
        };
        
      }; // kraj yes funkcije

      function proces_nalog_no() {
        
        
        reverse_switch(state, this_elem);
        
        show_popup_modal(false, popup_text, null );
        
      };

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', proces_nalog_yes, proces_nalog_no, null);
    
    }); 
    // ------------------------------------ kraj proces_radni_nalog switch
    
    $(`#` + kalk.kalk_rand  + `_` + proces.row_sifra + '_proces_drop').data(`cit_run`, async function( state, this_elem ) { 
      
      
      if ( !window.cit_user ) {
        popup_warn(`Niste se ulogirali !!!`);
        return;
      };
  
      var this_comp_id = $(this_elem).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var kalk_box = $(this_elem).closest('.kalk_box');

      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);

      var proces_row = $(this_elem).closest('.proces_row');

      var kalk_type = proces_row.attr(`data-kalk_type`);
      var kalk_sifra = proces_row.attr(`data-kalk_sifra`);
      var row_sifra = proces_row.attr(`data-row_sifra`);
      
      var curr_kalk = this_module.kalk_search( data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search( data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
     
      var curr_reserv_sifra = curr_kalk.kalk_sifra;
      var curr_reserv_state = curr_kalk.kalk_reserv;
      
      var popup_text = `Jeste li sigurni da želite ODUSTATI OD OVOG RADNOG PROCESA?`;
      
      // ako je bilo false i sada je true
      if ( 
        state == true
        && 
        !curr_proces.extra_data.proces_drop 
        && 
        !curr_proces.extra_data.proces_radni_nalog 
      ) {
        
        if ( !curr_proces.proc_koment ) {
          popup_warn(`Obavezno upisati razlog odustajanja u komentar procesa i kliknuti ponovo !!!`);
          
          setTimeout( function() { 
            // vrati nazad na false
            $(this_elem).removeClass(`on`).addClass(`off`);
          }, 200 );
          
          return;
        };
        
      };
      
      // ako je bilo false a sada je true
      if ( state == false && curr_proces.extra_data.proces_drop == true ) {
        
        popup_warn(`Kada PRVI PUTA odustanete od radnog procesa više ga nije moguće PONOVO pokrenuti nego morate napraviti NOVI !!!`);
        
        setTimeout( function() { 
          // vrati nazad da je user već odustao od ovog procesa !!! tj vrati da je switch TRUE
          $(this_elem).removeClass(`off`).addClass(`on`);  
        }, 200 );
        
        return;
        
      };
      
      
       
      async function proces_drop_yes() {
        
        if ( state == true ) {
          
          // ------------------------------ RADNI NALOG TRUE ------------------------------
          if ( !curr_proces.extra_data ) curr_proces.extra_data = {};
          
          
          
          curr_proces.extra_data.proces_drop = true;
          
          
                    
          
          var proces_copy = cit_deep(curr_proces);

          
          // pronadji taj proces u post kalk i također ga označi da je droped
          $.each( data.post_kalk[0].procesi, function(p_ind, proc) {
            if ( proc.row_sifra == proces_copy.row_sifra ) {
              if ( !data.post_kalk[0].procesi[p_ind].extra_data ) data.post_kalk[0].procesi[p_ind].extra_data = {};
              data.post_kalk[0].procesi[p_ind].extra_data.proces_drop = true;
            };
          });



          data.post_kalk[0].procesi.push(proces_copy);
          
          
          
          // alert(`ODUSTALI OD RADNOG PROCESA !!!`);
          
          // return;
          
          var rn_updated_statuses = await this_module.ajax_proces_rn_update_statuses( data._id, curr_proces.row_sifra, `rn_droped` );
          
          
          // setTimeout( function() { $(`#${data.id} .save_offer_kalk_btn`).trigger(`click`); }, 200 );
          // setTimeout( function() { $(`#${data.id} .save_pro_kalk_btn`).trigger(`click`); }, 300 );
          setTimeout( function() { $(`#${data.id} .save_post_kalk_btn`).trigger(`click`); }, 400 );
          
        };
        
      }; // kraj yes funkcije

      
      function proces_drop_no() {
        
        
        reverse_switch(state, this_elem);
        
        show_popup_modal(false, popup_text, null );
        
      };

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', proces_drop_yes, proces_drop_no, null);
      
      
      
    
    }); 

    
    $(`#` + kalk.kalk_rand  + `_` + proces.row_sifra + '_alat_apart').data(`cit_run`, async function( state, this_elem ) { 


      if ( !window.cit_user ) {
        popup_warn(`Niste se ulogirali !!!`);
        return;
      };

      var this_comp_id = $(this_elem).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      
      var product_id = $(this_elem).closest('.product_comp')[0].id;
      var product_data =  this_module.product_module.cit_data[product_id];
      

      var kalk_box = $(this_elem).closest('.kalk_box');

      var kalk_type = kalk_box.attr(`data-kalk_type`);
      var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);

      var proces_row = $(this_elem).closest('.proces_row');

      var kalk_type = proces_row.attr(`data-kalk_type`);
      var kalk_sifra = proces_row.attr(`data-kalk_sifra`);
      var row_sifra = proces_row.attr(`data-row_sifra`);

      var curr_kalk = this_module.kalk_search( data, kalk_type, kalk_sifra, null, null, null, null ); 
      var curr_proces = this_module.kalk_search( data, kalk_type, kalk_sifra, row_sifra, null, null, null ); 
      
      
      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );
      
      

      var curr_reserv_sifra = curr_kalk.kalk_sifra;
      var curr_reserv_state = curr_kalk.kalk_reserv;

      var popup_text = "";

      // ako je bilo false 
      if ( state == true && !curr_proces.extra_data?.alat_apart ) {

        popup_text = `Jeste li sigurni da želite ODVOJENO PRIKAZATI CIJENU ALATA?`;

      };
      
      // ako je bilo true
      if ( state == false && curr_proces.extra_data?.alat_apart ) {

        popup_text = `Jeste li sigurni da želite URAČUNATI CIJENU ALATA U CIJENU PROIZVODA?`;

      };


      async function alat_apart_yes() {

        if ( state == true ) {

          // ------------------------------ ALAT ODVOJENO TRUE ------------------------------
          if ( !curr_proces.extra_data ) curr_proces.extra_data = {};
          curr_proces.extra_data.alat_apart = true;

        } else {
          // ako je false
          if (  curr_proces.extra_data?.alat_apart ) delete curr_proces.extra_data.alat_apart;
        };
        
        
        this_module.update_kalk( curr_kalk, product_data, curr_proces_index, changed_ulaz=true, null, null );

        console.log(`--------------- make kalk kod odabira ALAT APART ----------------`);
        var this_kalk = this_module.make_kalks( this_elem, data, data[kalk_type], kalk_type, data.tip || null );
        // $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
        

      }; // kraj yes funkcije

      function alat_apart_no() {

        reverse_switch(state, this_elem);
        show_popup_modal(false, popup_text, null );

      };

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, popup_text, null, 'yes/no', alat_apart_yes, alat_apart_no, null);

    }); 

    
    
  };
  this_module.register_proces_events = register_proces_events;

    
  // ----------- END --------------- EVENI --------------------------
  // ----------- END --------------- EVENI --------------------------

  
  function kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, izlaz_sifra, action_sifra ) {

    if ( !data ) return null;
    if ( !kalk_type ) return null;
    
    // ako je samko kalk sifra
    if ( kalk_sifra && !row_sifra && !ulaz_sifra && !izlaz_sifra && !action_sifra ) {
      var kalk_index = find_index( data[kalk_type], kalk_sifra , `kalk_sifra` );  
      return data[kalk_type][kalk_index];
    };
    
    
    // ako je PORCES ROW 
    if ( kalk_sifra && row_sifra && !ulaz_sifra && !izlaz_sifra && !action_sifra ) {
      var kalk_index = find_index( data[kalk_type], kalk_sifra , `kalk_sifra` );
      var proces_index = find_index( data[kalk_type][kalk_index].procesi, row_sifra , `row_sifra` );
      return data[kalk_type][kalk_index].procesi[proces_index];
    };
    
    
    // ako je ULAZ
    if ( kalk_sifra && row_sifra && ulaz_sifra && !izlaz_sifra && !action_sifra ) {
      var kalk_index = find_index( data[kalk_type], kalk_sifra , `kalk_sifra` );
      var proces_index = find_index( data[kalk_type][kalk_index].procesi, row_sifra , `row_sifra` );
      var ulaz_index = find_index( data[kalk_type][kalk_index].procesi[proces_index].ulazi, ulaz_sifra , `sifra` );
      return data[kalk_type][kalk_index].procesi[proces_index].ulazi[ulaz_index];
    };
    
    
    // ako je IZLAZ
    if ( kalk_sifra && row_sifra && !ulaz_sifra && izlaz_sifra && !action_sifra ) {
      var kalk_index = find_index( data[kalk_type], kalk_sifra , `kalk_sifra` );
      var proces_index = find_index( data[kalk_type][kalk_index].procesi, row_sifra , `row_sifra` );
      var izlaz_index = find_index( data[kalk_type][kalk_index].procesi[proces_index].izlazi, izlaz_sifra , `sifra` );
      return data[kalk_type][kalk_index].procesi[proces_index].izlazi[izlaz_index];
    };    
    
    
        // ako je ACTION
    if ( kalk_sifra && row_sifra && !ulaz_sifra && !izlaz_sifra && action_sifra ) {
      var kalk_index = find_index( data[kalk_type], kalk_sifra , `kalk_sifra` );
      var proces_index = find_index( data[kalk_type][kalk_index].procesi, row_sifra , `row_sifra` );
      return data[kalk_type][kalk_index].procesi[proces_index].action;
    };
    
    
  };
  this_module.kalk_search = kalk_search;

  
  function calculate_prirez_F201( product_data, curr_kalk ) {
    
    var prirez_val = null;
    var prirez_kontra = null;
    var jaska_sloter = null;
    var bilokalnik_sloter = null;
    
    if ( product_data.tip?.sifra == 'TP20' ) {
    
      var duz = product_data.duzina;
      var sir = product_data.sirina;
      var vis = product_data.sirina;

      var jaska_sloter = "";
      var bilokalnik_sloter = "";

      var slojevi = 3;
      var dodaci_val = 5 + 6 + 5;
      var dodaci_kontra = 6 + 6 + 6 + 3;

      if ( curr_kalk.kalk_polov == true ) dodaci_kontra = 6 + 3;


      var klapna = 35;
      var extra_kontra = 20;

      var dodatak_half_sir = 5;
      var dodatak_vis = 6;

      
      var prirez_mat = curr_kalk.kalk_ploca_mat;

      if ( !prirez_mat ) {
        popup_warn(`Potrebno upisati Kvaliteta/slojevi sirovine !!!!`);
        return;
      };

      if ( 
        prirez_mat.naziv.toLowerCase().indexOf(`/bc`) > -1 
        ||
        prirez_mat.naziv.toLowerCase().indexOf(`/eb`) > -1 
      ) {

        slojevi = 5;
        dodaci_val = 8 + 10 + 8;
        dodaci_kontra = 10 + 10 + 10 + 5;

        dodatak_half_sir = 8;
        dodatak_vis = 10;

        if ( curr_kalk.kalk_polov == true ) dodaci_kontra = 10 + 5;

      };


      prirez_val = Math.ceil( vis + sir + dodaci_val );
      
      prirez_kontra = Math.ceil( 2*duz + 2*sir + dodaci_kontra + klapna + extra_kontra );
      
      if ( curr_kalk.kalk_polov == true ) prirez_kontra = Math.ceil( duz + sir + dodaci_kontra + klapna + extra_kontra);

      jaska_sloter = (sir/2 + dodatak_half_sir) + " / " + (vis + dodatak_vis) + " / " + (sir/2 + dodatak_half_sir) + " X " +  prirez_kontra;
      bilokalnik_sloter = prirez_kontra + " X " + (sir/2 + dodatak_half_sir) + " / " + (vis + dodatak_vis) + " / " + (sir/2 + dodatak_half_sir);
      
    };

    
    return {
      val: prirez_val,
      kontra: prirez_kontra,
      jaska_sloter,
      bilokalnik_sloter,
    }
    
    
    
  };
  this_module.calculate_prirez_F201 = calculate_prirez_F201;
  
  
  function calculate_prirez_F200( product_data, curr_kalk ) {
    
    var prirez_val = null;
    var prirez_kontra = null;
    var jaska_sloter = null;
    var bilokalnik_sloter = null;
    
    if ( product_data.tip?.sifra == 'TP20B' ) {
    
      var duz = product_data.duzina;
      var sir = product_data.sirina;
      var vis = product_data.sirina;

      var jaska_sloter = "";
      var bilokalnik_sloter = "";

      var slojevi = 3;
      var dodaci_val = 5 + 6;
      var dodaci_kontra = 6 + 6 + 6 + 3;

      if ( curr_kalk.kalk_polov == true ) dodaci_kontra = 6 + 3;


      var klapna = 35;
      var extra_kontra = 20;

      var dodatak_half_sir = 5;
      var dodatak_vis = 6;


      var prirez_val = null;
      var prirez_kontra = null;

      var prirez_mat = curr_kalk.kalk_ploca_mat;
      
      if ( !prirez_mat ) {
        popup_warn(`Potrebno upisati Kvaliteta/slojevi sirovine !!!!`);
        return;
      };
      

      if ( 
        prirez_mat.naziv.toLowerCase().indexOf(`/bc`) > -1 
        ||
        prirez_mat.naziv.toLowerCase().indexOf(`/eb`) > -1 
      ) {

        slojevi = 5;
        dodaci_val = 8 + 10;
        dodaci_kontra = 10 + 10 + 10 + 5;

        dodatak_half_sir = 8;
        dodatak_vis = 10;

        if ( curr_kalk.kalk_polov == true ) dodaci_kontra = 10 + 5;

      };


      prirez_val = Math.ceil( vis + sir/2 + dodaci_val );
      prirez_kontra = Math.ceil( 2*duz + 2*sir + dodaci_kontra + klapna + extra_kontra );

      if ( curr_kalk.kalk_polov == true ) prirez_kontra = Math.ceil( duz + sir + dodaci_kontra + klapna + extra_kontra);
      
      
      jaska_sloter = (sir/2 + dodatak_half_sir) + " / " + (vis + dodatak_vis) + " / " + " X " +  prirez_kontra;
      bilokalnik_sloter = prirez_kontra + " X " + (vis + dodatak_vis) + " / " + (sir/2 + dodatak_half_sir);


    };

    
    return {
      val: prirez_val,
      kontra: prirez_kontra,
      jaska_sloter,
      bilokalnik_sloter,
    }
    
    
  };
  this_module.calculate_prirez_F200 = calculate_prirez_F200;
  
  function calculate_prirez_F203( product_data, curr_kalk ) {
    
    var prirez_val = null;
    var prirez_kontra = null;
    var jaska_sloter = null;
    var bilokalnik_sloter = null;
    
    
    if ( product_data.tip?.sifra == 'TP20C' ) {
    
      var duz = product_data.duzina;
      var sir = product_data.sirina;
      var vis = product_data.sirina;

      var jaska_sloter = "";
      var bilokalnik_sloter = "";

      var slojevi = 3;
      var dodaci_val = 5 + 6 + 5;
      var dodaci_kontra = 6 + 6 + 6 + 3;

      if ( curr_kalk.kalk_polov == true ) dodaci_kontra = 6 + 3;


      var klapna = 35;
      var extra_kontra = 20;

      var dodatak_half_sir = 5;
      var dodatak_vis = 6;


      var prirez_val = null;
      var prirez_kontra = null;

      var prirez_mat = curr_kalk.kalk_ploca_mat;

      if ( !prirez_mat ) {
        popup_warn(`Potrebno upisati Kvaliteta/slojevi sirovine !!!!`);
        return;
      };

      if ( 
        prirez_mat.naziv.toLowerCase().indexOf(`/bc`) > -1 
        ||
        prirez_mat.naziv.toLowerCase().indexOf(`/eb`) > -1 
      ) {

        slojevi = 5;
        dodaci_val = 8 + 10 + 8;
        dodaci_kontra = 10 + 10 + 10 + 5;

        dodatak_half_sir = 8;
        dodatak_vis = 10;

        if ( curr_kalk.kalk_polov == true ) dodaci_kontra = 10 + 5;

      };



      prirez_val = Math.ceil( vis + sir*2 + dodaci_val );
      prirez_kontra = Math.ceil( 2*duz + 2*sir + dodaci_kontra + klapna + extra_kontra );


      if ( curr_kalk.kalk_polov == true ) prirez_kontra = Math.ceil( duz + sir + dodaci_kontra + klapna + extra_kontra);

      jaska_sloter = (sir + dodatak_half_sir) + " / " + (vis + dodatak_vis) + " / " + (sir + dodatak_half_sir) + " X " +  prirez_kontra;
      bilokalnik_sloter = prirez_kontra + " X " + (sir + dodatak_half_sir) + " / " + (vis + dodatak_vis) + " / " + (sir + dodatak_half_sir);

    };

    
    return {
      val: prirez_val,
      kontra: prirez_kontra,
      jaska_sloter,
      bilokalnik_sloter,
    }
    
    
  };
  this_module.calculate_prirez_F203 = calculate_prirez_F203;
  
  
  function get_prirez(product_data, curr_kalk) {
    
    var prirez_objekt = null;
    
    // f 201    
    if ( product_data.tip?.sifra == 'TP20' ) {
      prirez_objekt = this_module.calculate_prirez_F201( product_data, curr_kalk );
    };

    // f 200
    if ( product_data.tip?.sifra == 'TP20B' ) {
      prirez_objekt = this_module.calculate_prirez_F200( product_data, curr_kalk );
    };

    // f 203
    if ( product_data.tip?.sifra == 'TP20C' ) {
      prirez_objekt = this_module.calculate_prirez_F203( product_data, curr_kalk );
    };
    
    
    // ako nemam funkciju za izračun prireza
    // onda pogledaj jel user možda ručno upisao dimenzije prireza u polja
    // ako jeste onda ti values su već upisani kao propertiji u current kalk
    if ( prirez_objekt == null ) {
      
      if ( curr_kalk.prirez_val && curr_kalk.prirez_kontra ) {

        prirez_objekt = {
          val: curr_kalk.prirez_val,
          kontra: curr_kalk.prirez_kontra,
          jaska_sloter: "",
          bilokalnik_sloter: "",
        }
        
      };
      
    };
    
    
    return prirez_objekt;
    
  };
  this_module.get_prirez = get_prirez;
  
  
  async function write_prirez(e, arg_data_id, arg_product_id, arg_kalk_type, arg_kalk_sifra) {

    var this_button = null;
    var data = null;
    var product_data = null;
    
    if ( e ) {
    
      this_button = e.target;

      var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
      data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      
      var product_id = $(this_button).closest('.product_comp')[0].id;
      product_data =  this_module.product_module.cit_data[product_id];

    } 
    else {
      
      data = this_module.cit_data[arg_data_id];
      product_data =  this_module.product_module.cit_data[arg_product_id];
      
    };
    
    
    if ( 
        product_data.tip?.sifra !== 'TP20'  // F201
        && 
        product_data.tip?.sifra !== 'TP20B' // F200
        &&
        product_data.tip?.sifra !== 'TP20C' // F203
      
      ) {
      
      return;

    };
    


    console.log("product_data-----");
    console.log(product_data);


    var kalk_type = arg_kalk_type || $(this_button).closest(`.kalk_box`).attr(`data-kalk_type`);
    var kalk_sifra = arg_kalk_sifra || $(this_button).closest(`.kalk_box`).attr(`data-kalk_sifra`);

    var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );

    console.log(curr_kalk);
    
    var prirez_mat = curr_kalk.kalk_ploca_mat;  // $('#'+kalk_sifra+'_kalk_ploca_mat').val();

    if ( !prirez_mat) {
      popup_warn(`Potrebno upisati Kvaliteta/slojevi sirovine !!!!`);
      return;
    };


    // ------------------------------------------------------ ako je F201 AMERIKANKA
    if ( product_data.tip?.sifra == 'TP20' ) {

      var prirez_objekt = this_module.calculate_prirez_F201( product_data, curr_kalk );
      var prirez_val = prirez_objekt.val;
      var prirez_kontra = prirez_objekt.kontra;

      
      var jaska_sloter = prirez_objekt.jaska_sloter;
      var bilokalnik_sloter = prirez_objekt.bilokalnik_sloter;


      var naslov_type = "";
      if ( kalk_type == `offer_kalk` ) naslov_type = ""; // "KALKULACIJA ZA PONUDU";
      if ( kalk_type == `pro_kalk` ) naslov_type = ""; // "KALKULACIJA ZA PROIZVODNJU";
      if ( kalk_type == `post_kalk` ) naslov_type = ""; // "POST KALKULACIJA";

      var title_sloter = `JAKŠ-MOD-DUN: &nbsp; ${jaska_sloter} &nbsp;&nbsp; BILO: &nbsp; ${bilokalnik_sloter}`;
      $(`#${kalk_sifra}_kalkulacija .title_sloter`).html(title_sloter);


      curr_kalk.prirez_val = prirez_val;
      curr_kalk.prirez_kontra = prirez_kontra;

      $('#'+kalk_sifra+'_prirez_val').val( cit_format(prirez_val, 0) );
      $('#'+kalk_sifra+'_prirez_kontra').val( cit_format(prirez_kontra, 0) );

      
      return;
      
      
    }; 
    // ------------------------------------------------------ kraj ako je F201 AMERIKANKA

    // ------------------------------------------------------ ako je F200 AMERIKANKA BEZ POKLOPCA
    if ( product_data.tip?.sifra == 'TP20B' ) {

      var prirez_objekt = this_module.calculate_prirez_F200( product_data, curr_kalk );
      var prirez_val = prirez_objekt.val;
      var prirez_kontra = prirez_objekt.kontra;
      
      var jaska_sloter = prirez_objekt.jaska_sloter;
      var bilokalnik_sloter = prirez_objekt.bilokalnik_sloter;


      var naslov_type = "";
      if ( kalk_type == `offer_kalk` ) naslov_type = ""; // "KALKULACIJA ZA PONUDU";
      if ( kalk_type == `pro_kalk` ) naslov_type = ""; // "KALKULACIJA ZA PROIZVODNJU";
      if ( kalk_type == `post_kalk` ) naslov_type = ""; // "POST KALKULACIJA";

      var title_sloter = `JAKŠ-MOD-DUN: &nbsp; ${jaska_sloter} &nbsp;&nbsp; BILO: &nbsp; ${bilokalnik_sloter}`;
      $(`#${kalk_sifra}_kalkulacija .title_sloter`).html(title_sloter);
      

      curr_kalk.prirez_val = prirez_val;
      curr_kalk.prirez_kontra = prirez_kontra;

      $('#'+kalk_sifra+'_prirez_val').val( cit_format(prirez_val, 0) );
      $('#'+kalk_sifra+'_prirez_kontra').val( cit_format(prirez_kontra, 0) );


      return;
      
    }; 
    // ------------------------------------------------------ kraj ako je F200 AMERIKANKA BEZ POKLOPCA

    // ------------------------------------------------------ ako je F203 AMERIKANKA SA KLAPNOM PREKO CIJELE ŠIRINE
    if ( product_data.tip?.sifra == 'TP20C' ) {

      
      var prirez_objekt = this_module.calculate_prirez_F203( product_data, curr_kalk );
      var prirez_val = prirez_objekt.val;
      var prirez_kontra = prirez_objekt.kontra;
      

      var jaska_sloter = prirez_objekt.jaska_sloter;
      var bilokalnik_sloter = prirez_objekt.bilokalnik_sloter;


      var naslov_type = "";
      if ( kalk_type == `offer_kalk` ) naslov_type = ""; // "KALKULACIJA ZA PONUDU";
      if ( kalk_type == `pro_kalk` ) naslov_type = ""; // "KALKULACIJA ZA PROIZVODNJU";
      if ( kalk_type == `post_kalk` ) naslov_type = ""; // "POST KALKULACIJA";

      var title_sloter = `JAKŠ-MOD-DUN: &nbsp; ${jaska_sloter} &nbsp;&nbsp; BILO: &nbsp; ${bilokalnik_sloter}`;
      $(`#${kalk_sifra}_kalkulacija .title_sloter`).html(title_sloter);
      
      curr_kalk.prirez_val = prirez_val;
      curr_kalk.prirez_kontra = prirez_kontra;

      $('#'+kalk_sifra+'_prirez_val').val( cit_format(prirez_val, 0) );
      $('#'+kalk_sifra+'_prirez_kontra').val( cit_format(prirez_kontra, 0) );

      
      return;

    }; 
    // ------------------------------------------------------ kraj ako je F203 AMERIKANKA SA KLAPNOM PREKO CIJELE ŠIRINE


    // F411 JOŠ UVIJEK NIJE GOTOVO !!!
    // F411 JOŠ UVIJEK NIJE GOTOVO !!!
    // F411 JOŠ UVIJEK NIJE GOTOVO !!!
    // ------------------------------------------------------ ako je F411 AMERIKANKA KAO PLAŠT

    
    
    
    // prikaži poruku samo ako je ova func pozvana sa klikom usera
    // prikaži poruku samo ako je ova func pozvana sa klikom usera
    // prikaži poruku samo ako je ova func pozvana sa klikom usera
    if ( e ) popup_warn(`Aplikacija nema operaciju za računanje prireza za ovaj tip proizvoda !!!`);
    
      
  };
  this_module.write_prirez = write_prirez;
  
  
  function cit_forecast( sirovina, this_button, arg_product_data ) {
    
    
    var product_id = null;
    var product_data = null;
    
    if ( this_button ) {
      product_id = $(this_button).closest('.product_comp')[0].id;
      product_data =  this_module.product_module.cit_data[product_id];
    };
    
    if ( arg_product_data ) product_data = arg_product_data;

    // START kolicina
    var forecast_kolicina = sirovina.sklad_kolicina;
    var curr_time = Date.now();
    
    $.each(sirovina.records, function( r_ind, record ) {
      
      if ( 
        record.record_est_time >= curr_time // novije je od danasnjeg datuma
        &&
        /*
        record.record_est_time <= product_data?.rok_za_def // mora biti starije od roka za def tj procjene početka proizvodnje
        &&
        za sada neću gledati rok za def nego sve do zadnjeg recorda
        */
        record.record_est_time <= curr_time + (1000*60*60*24*90) // recorde koji su dalji od 3 mjeseca u budoćnosti NE GLEDAJ
        &&
        !record.storno_time // nije storno !!!
        
        // za sada ću ipak uzimati u obzir i rezervacije po ponudi kupca !!!!!!
        // &&
        // !record.pk_kolicina // nije rezervirano po ponudi
        
      ) {
        
        // može biti po NK ili po RN
        var minus_kolicina = record.pk_kolicina || record.nk_kolicina || record.rn_kolicina || 0;
        
        forecast_kolicina = forecast_kolicina - minus_kolicina; 
        // + (record.order_kolicina || 0); -----> NEĆU VIŠE DODAVATI KOLIČINU U FORCAST (kao da MRAV uopće ne zna da dolazi roba )
        // na taj način ću zadovoljit Radmilin zahtjev da uopće ne uzimam u obzir dolazak robe u budućnosti
        // što se MRAV forcasta tiče roba se samo magično pojavi na skladištu :)
        
      };
      
    });
    
    return forecast_kolicina;
    
  };
  this_module.cit_forecast = cit_forecast;
  
  
  
  function gen_mat_mix_query(mater_string) {


    var sirovine_query = null;

    // ako u sebi materijal ima slash to znači da je valovita ljepenka  !!!
    // ako u sebi materijal ima slash to znači da je valovita ljepenka  !!!
    if ( mater_string.indexOf(`/`) > -1 ) {


      // ^ i $ znači da mora točno početi i završiti kako piše
      // ^ i $ znači da mora točno početi i završiti kako piše
      // ^ i $ znači da mora točno početi i završiti kako piše

      sirovine_query = { kvaliteta_mix: { "$regex": "^" + mater_string + "$", "$options": "im" } }; 


      if ( mater_string.indexOf(`329g`) > -1 ) {
        mater_string = mater_string.replace(` standard 329g`, ``);
        sirovine_query = { kvaliteta_mix: { "$regex": "^" + mater_string + "$", "$options": "im" }, gramaza_g: 329 }; 
      };


      if ( mater_string.indexOf(`382g`) > -1 ) {
        mater_string = mater_string.replace(` medium 382g`, ``);
        sirovine_query = { kvaliteta_mix: { "$regex": "^" + mater_string + "$", "$options": "im" }, gramaza_g: 382 }; 
      };


      if ( mater_string.indexOf(`422g`) > -1 ) {
        mater_string = mater_string.replace(` super 422g`, ``);
        sirovine_query = { kvaliteta_mix: { "$regex": "^" + mater_string + "$", "$options": "im" }, gramaza_g: 422 }; 
      };


      if ( mater_string.indexOf(`808g`) > -1 ) {
        mater_string = mater_string.replace(` 808g`, ``);
        sirovine_query = { kvaliteta_mix: { "$regex": "^" + mater_string + "$", "$options": "im" }, gramaza_g: 808 }; 
      };


    };


    if ( mater_string.toLowerCase().indexOf(`siva ljepenka`) > -1 ) {
      if ( mater_string.toLowerCase().indexOf(`1mm`) > -1 ) sirovine_query = { "vrsta_materijala.sifra": "VM2", debljina_mm: 1 };
      if ( mater_string.toLowerCase().indexOf(`1,5mm`) > -1 ) sirovine_query = { "vrsta_materijala.sifra": "VM2", debljina_mm: 1.5 };
      if ( mater_string.toLowerCase().indexOf(`2mm`) > -1 ) sirovine_query = { "vrsta_materijala.sifra": "VM2", debljina_mm: 2 };
      if ( mater_string.toLowerCase().indexOf(`3mm`) > -1 ) sirovine_query = { "vrsta_materijala.sifra": "VM2", debljina_mm: 3 };
    };

    if ( mater_string.toLowerCase().indexOf(`polipropilen`) > -1 ) {
      if ( mater_string.toLowerCase().indexOf(`3,5mm`) > -1 ) sirovine_query = { "vrsta_materijala.sifra": "VM12", debljina_mm: 3.5 };
      if ( mater_string.toLowerCase().indexOf(`5mm`) > -1 ) sirovine_query = { "vrsta_materijala.sifra": "VM12", debljina_mm: 5 };
    };

    if ( mater_string.toLowerCase().indexOf(`forex`) > -1 ) {
      if ( mater_string.toLowerCase().indexOf(`1mm`) > -1 ) sirovine_query = { "vrsta_materijala.sifra": "VM14", debljina_mm: 1 };
      if ( mater_string.toLowerCase().indexOf(`2mm`) > -1 ) sirovine_query = { "vrsta_materijala.sifra": "VM14", debljina_mm: 2 };
      if ( mater_string.toLowerCase().indexOf(`3mm`) > -1 ) sirovine_query = { "vrsta_materijala.sifra": "VM14", debljina_mm: 3 };
      if ( mater_string.toLowerCase().indexOf(`5mm`) > -1 ) sirovine_query = { "vrsta_materijala.sifra": "VM14", debljina_mm: 5 };
      if ( mater_string.toLowerCase().indexOf(`10mm`) > -1 ) sirovine_query = { "vrsta_materijala.sifra": "VM14", debljina_mm: 10 };
    };

    if ( mater_string.toLowerCase().indexOf(`kapaline`) > -1 ) {
      if ( mater_string.toLowerCase().indexOf(`5mm`) > -1 ) sirovine_query = { "vrsta_materijala.sifra": "VM15", debljina_mm: 5 };
      if ( mater_string.toLowerCase().indexOf(`10mm`) > -1 ) sirovine_query = { "vrsta_materijala.sifra": "VM15", debljina_mm: 10 };
    };

    return sirovine_query;

  };
  this_module.gen_mat_mix_query = gen_mat_mix_query;
  
  
  function gen_mat_mix_query_2(mater) {

    var sirovine_query = null;
    
    if ( mater.sifra_mat == `VM3` /* to je valovita ljepenka */) {

      sirovine_query = { kvaliteta_1: mater.kvaliteta_1, kvaliteta_2: mater.kvaliteta_2 };
      
      // ako valovita ljepenka ima definidranu gramažu onda je dodaj u query
      if ( mater.gramaza_g !== null ) {
        sirovine_query = { kvaliteta_1: mater.kvaliteta_1, kvaliteta_2: mater.kvaliteta_2, gramaza_g: mater.gramaza_g }; 
      };

    } else {
      // za sve ostale materijale trebam vrstu materijala tj sifru
      // i debljinu !!!!
      // polipropilen je sifra     VM12
      // forex je sifra            VM14
      // KAPALINE-KAPAFIX je sifra VM15
      
      sirovine_query = { "vrsta_materijala.sifra": mater.sifra_mat, debljina_mm: mater.debljina_mm || null };
      
    };

    return sirovine_query;

  };
  this_module.gen_mat_mix_query_2 = gen_mat_mix_query_2;

  
  async function best_plate_find(
    e,
    arg_data_id,
    arg_product_id,
    arg_kalk_type,
    arg_kalk_sifra,
    arg_curr_kalk, 
    arg_sirovine_ids,
    arg_proces_index,
    arg_prirez_val,
    arg_prirez_kontra,
    arg_mat, 
  ) {
      
      

      // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);

      var this_button = null;

      var this_comp_id = null;
      var data = null;
      var rand_id = null;

      var product_id = null;
      var product_data = null;


      if ( e ) {

        this_button = e.target;

        this_comp_id = $(this_button).closest('.cit_comp')[0].id;
        data = this_module.cit_data[this_comp_id];
        rand_id =  this_module.cit_data[this_comp_id].rand_id;


        product_id = $(this_button).closest('.product_comp')[0].id;
        product_data =  this_module.product_module.cit_data[product_id];

      } 
      else {

        data = this_module.cit_data[arg_data_id];
        product_data =  this_module.product_module.cit_data[arg_product_id];

      };


      var kalk_type  = arg_kalk_type  || $(this_button).closest(`.kalk_box`).attr(`data-kalk_type`);
      var kalk_sifra = arg_kalk_sifra || $(this_button).closest(`.kalk_box`).attr(`data-kalk_sifra`);
      var curr_kalk = arg_curr_kalk || this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );

      if ( 
          product_data.tip?.sifra !== 'TP20'  // F201
          && 
          product_data.tip?.sifra !== 'TP20B' // F200
          &&
          product_data.tip?.sifra !== 'TP20C' // F203

        ) {

        return null;

      };

      var db_result = null;
      var sirovine_query = {};

      if ( !arg_sirovine_ids ) {

        console.log("product_data-----");
        console.log(product_data);
        console.log(curr_kalk);

        var prirez_mat = arg_mat || curr_kalk.kalk_ploca_mat; // uzmi ili argument materijala ili ga uzmi iz zajedničkog matrijala od cijele kalkulacije
        
        if ( !prirez_mat ) {
          popup_warn(`Potrebno upisati Kvaliteta/slojevi sirovine !!!!`);
          return false;
        };

        sirovine_query = this_module.gen_mat_mix_query_2(prirez_mat);

      } 
      else {
        // ako sam dao arg sirovine onda samo gledaj te sirovine
        sirovine_query = { _id: { $in: arg_sirovine_ids } };
      };
      
      sirovine_query = { ...sirovine_query, on_date: product_data?.rok_za_def || null }
      
      //!!!findsirov!!!

      var props = {
        filter: null,
        url: '/find_sirov',
        query: sirovine_query,
        desc: `pretraga u svih ploča sa određenom kvalitetom / slojevima unutar modula kalkulacija ili ako sam na silu dao arg sirovine ids onda samo te specifične sirovine traži u bazi`,
      };


      try {
        db_result = await search_database(null, props );
      } catch (err) {
        console.log(err);
        return false;
      };

      if ( db_result && $.isArray(db_result) ) {
        console.log(db_result);
      };


      var prirez_val = arg_prirez_val || curr_kalk.prirez_val;
      var prirez_kontra = arg_prirez_kontra || curr_kalk.prirez_kontra;


      if ( !prirez_val || !prirez_kontra ) {
        popup_warn(`Nedostaje podatak o prirezu !!!!`);
        return false;
      };

      var naklada = curr_kalk.kalk_naklada;
      if ( !curr_kalk.kalk_naklada ) {
        popup_warn(`Morate upisati nakladu !!!!`);
        return false;
      };

      var ulazi = [];

      // izracunaj povrsinu za svaku plocu
      $.each(db_result, function(s_ind, sirovina) {
        db_result[s_ind].area = sirovina.po_valu * sirovina.po_kontravalu;
      });
      
      // ---------------START---------------------------------- UPISIVANJE DODATNIH PROPERTIJA U SIROVINU -------------------------------------------------
      // ---------------START---------------------------------- UPISIVANJE DODATNIH PROPERTIJA U SIROVINU -------------------------------------------------

      $.each(db_result, function(s_ind, sirovina) {

        sirovina.forecast_kolicina = this_module.cit_forecast( sirovina, this_button, product_data);

        // DODAJEM OVE PROPERTIJE TAKO DA MI POSLUŽE UNUTER FNKCIJE KALK REAL PLATE 
        // DODAJEM OVE PROPERTIJE TAKO DA MI POSLUŽE UNUTER FNKCIJE KALK REAL PLATE 
        // DODAJEM OVE PROPERTIJE TAKO DA MI POSLUŽE UNUTER FNKCIJE KALK REAL PLATE 

        sirovina.kalk_unit = sirovina.osnovna_jedinica.jedinica;
        sirovina.kalk_po_valu = sirovina.po_valu;
        sirovina.kalk_po_kontravalu = sirovina.po_kontravalu;
        sirovina.kalk_unit_price = sirovina.cijena;

        var real = this_module.kalk_real_plate( sirovina.po_valu, sirovina.po_kontravalu, prirez_val, prirez_kontra, sirovina, curr_kalk );

        /*
        kalk_real_plate MI VRAĆA:
        ------------------------------------------------
        return {
          real_val: real_val,
          real_kontra: real_kontra,
          area: real_unit_area,
          ostatak_val: ostatak_val >= 300 ? ostatak_val : null,
          ostatak_kontra: ostatak_kontra >= 400 ? ostatak_kontra : null,
          kom: kalk_kom,
          kom_val: kom_val,
          kom_kontra: kom_kontra,
          price: real_unit_price,
        }
        ------------------------------------------------
        */
        
        
        if ( !real ) return;


        var ostaci = this_module.make_ostatak(real, sirovina);
        // izracunaj površinu ukupnog ostatka za svaku plocu

        var ostatak_area = (ostaci.val?.area || 0) + (ostaci.kontra?.area || 0);
        db_result[s_ind].ostatak_area = ostatak_area;

      });

      // ---------------END---------------------------------- UPISIVANJE DODATNIH PROPERTIJA U SIROVINU -------------------------------------------------
      // ---------------END---------------------------------- UPISIVANJE DODATNIH PROPERTIJA U SIROVINU -------------------------------------------------

    // prvo sortiraj po površini ploče
    // zatim sortiraj po ukupnoj površini ostataka
    var criteria = [ 'area','ostatak_area' ];
    multisort(db_result, criteria);

    // koliko sam UKUPNO uzeo komada
    var kalk_uzeo_komada = 0;

    var new_all_ulazi = [];

    $.each(db_result, function(s_ind, sirovina) {

      // resetiraj za svaku novu sirovinu
      var uzeo_ploca = 0;

      if ( 
        sirovina.po_valu >= prirez_val 
        && 
        sirovina.po_kontravalu >= prirez_kontra
        &&
        sirovina.forecast_kolicina > 0
        &&
        kalk_uzeo_komada < naklada // sve dok nisam uzeo dovoljno komada da bude jednako nakladi !!!
      ) {

        var real = this_module.kalk_real_plate( sirovina.po_valu, sirovina.po_kontravalu, prirez_val, prirez_kontra, sirovina, curr_kalk );

        if ( !real ) return;

        /*
        OVO DOBIJEM OD KALK REAL PLATE
          real_val: real_val,
          real_kontra: real_kontra,
          area: kalk_unit_area,
          ostatak_val: ostatak_val >= 300 ? ostatak_val : null,
          ostatak_kontra: ostatak_kontra >= 400 ? ostatak_kontra : null,
          kom: kalk_kom,
          price: kalk_unit_price, 
        */

        var kalk_unit_area = null;
        var kalk_unit = "m2";

        var kalk_unit_price = null;
        var real_unit_price = null;

        var kalk_price = null;

        var kalk_kom_in_product = null;
        var kalk_kom_in_sirovina = null;
        var kalk_kom_val = null;
        var kalk_kom_kontra = null;

        var kalk_sum_sirovina = null;
        var kalk_extra_kom = null;


        var koliko_trebam = naklada - kalk_uzeo_komada;

        // npr od jedne ploče mogu napraviti 2 proizvoda 
        // ako trebam 100 proizvoda , a napravio sam 25 proizvoda onda ----> ( 100 - 25 ) / 2 = 75/2 = 37,5 ---> 38 ploča mi je još potrebno !! 
        var koliko_ploca_jos_treba = Math.ceil( (naklada - kalk_uzeo_komada) / real.kom );

        // ako je ima sirovine
        if ( sirovina.forecast_kolicina >= koliko_ploca_jos_treba  ) {
          kalk_uzeo_komada = kalk_uzeo_komada + (koliko_ploca_jos_treba * real.kom /* koliko komada tog elementa ću dobiti  */);
          uzeo_ploca = koliko_ploca_jos_treba;
        } 
        else if ( sirovina.forecast_kolicina < koliko_ploca_jos_treba ) {
          kalk_uzeo_komada = kalk_uzeo_komada + (sirovina.forecast_kolicina * real.kom);
          uzeo_ploca = sirovina.forecast_kolicina;
        };

        // sirovina
        db_result[s_ind].forecast_kolicina = sirovina.forecast_kolicina - uzeo_ploca;

        // this_module.save_sirov_in_kalk(new_sirov);

        var ostaci = this_module.make_ostatak(real, sirovina);

        var new_ulaz = { 

          sifra: "ulaz"+cit_rand(),
          _id: sirovina._id,
          kalk_full_naziv: (sirovina.sirovina_sifra + "--" + sirovina.full_naziv + "--" + sirovina.dobavljac?.naziv ),

          kalk_element: null,

          kalk_po_valu: sirovina.po_valu || sirovina.kontra_tok || null,
          kalk_po_kontravalu: sirovina.po_kontravalu || sirovina.tok || null,

          uzimam_po_valu: real.real_val,
          uzimam_po_kontravalu: real.real_kontra,

          kalk_ostatak_val: ostaci.val,
          kalk_ostatak_kontra: ostaci.kontra,

          kalk_skart: false,

          kalk_unit_area: real.area,

          kalk_unit: sirovina.osnovna_jedinica.jedinica,

          kalk_unit_price: sirovina.cijena,
          real_unit_price: real.price,

          kalk_price: real.price * uzeo_ploca,

          kalk_kom_in_product: null,
          kalk_kom_in_sirovina: 1, // real.kom,
          kalk_kom_val: real.kom_val,
          kalk_kom_kontra: real.kom_kontra,

          kalk_sum_sirovina: uzeo_ploca,
          kalk_skart: null,
          kalk_extra_kom: null,
          
          vrsta_materijala: sirovina.vrsta_materijala || null,
          klasa_sirovine: sirovina.klasa_sirovine || null,
          tip_sirovine: sirovina.tip_sirovine || null,
          
          kvaliteta_1: sirovina.kvaliteta_1 || null,
          kvaliteta_2: sirovina.kvaliteta_2 || null,
          debljina_mm: sirovina.debljina_mm || null,
          gramaza_g: sirovina.gramaza_g || null,

        };

        new_all_ulazi.push(new_ulaz);


      }; // kraj ako barem jedan prirez stane u ploču / 

    }); // loop po rezultatima iz baze


    return new_all_ulazi;

  };
  this_module.best_plate_find = best_plate_find;

  
  async function get_sirov_for_mont_plates(sirov_ids, product_data) {
    
    
    var this_button = e.target;

    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    var product_id = $(this_button).closest('.product_comp')[0].id;
    var product_data =  this_module.product_module.cit_data[product_id];
    
    
    return new Promise( async function(resolve, reject) {
    
    var all_sirovs = [];
    var all_sirovs_query = { _id: { $in: sirov_ids }, on_date: product_data?.rok_za_def || null };
      //!!!findsirov!!!

      var props = {
        filter: null, // status_mod.statuses_filter,
        url: `/find_sirov`, // /${ hash_data.statuses_page || "none" }`,
        query: all_sirovs_query,
        desc: `pretraga svih sirovina za kalkulaciju montažnih ploča`,
      };

      try {
        all_sirovs = await search_database(null, props );
      } catch (err) {
        console.log(err);
      };
      
      if (
        !all_sirovs  // ako nema resultata
        ||
        ($.isArray( all_sirovs ) && all_sirovs.length == 0 ) // ili je resultat prazan array
      ) {
        popup_error(`Došlo je do greške prilikom učitavanja sirovina za montažne ploče!!!`);
        
        resolve(null);
        return;
      };
      
      
      
      resolve(all_sirovs);
    
    }); // kraj promisa  
      
    
  };
  this_module.get_sirov_for_mont_plates = get_sirov_for_mont_plates;
  


  async function select_find_sirov( state, upsert, arg_data, arg_product_id, arg_kalk, arg_proces ) {

    console.log(state);

    var state = reduce_sir(state);

    var this_comp_id = null;
    var data = null;
    var rand_id = null;


    var product_id = null;
    var product_data = null;


    var ulaz_row = null;

    var kalk_type = null;
    var kalk_sifra = null;
    var row_sifra = null;
    var ulaz_sifra = null;


    var curr_kalk = null
    var curr_proces = null;
    var curr_ulaz = null;

    // dakle ako je upsert undefined ili je true -----> NE SMJE BITI FALSE 
    if ( upsert !== false ) {

      this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
      data = this_module.cit_data[this_comp_id];
      rand_id =  this_module.cit_data[this_comp_id].rand_id;


      product_id = $('#'+data.id).closest('.product_comp')[0].id;
      product_data =  this_module.product_module.cit_data[product_id];


      ulaz_row = $('#'+current_input_id).closest('.ulaz_row');
      kalk_type = ulaz_row.attr(`data-kalk_type`);
      kalk_sifra = ulaz_row.attr(`data-kalk_sifra`);
      row_sifra = ulaz_row.attr(`data-row_sifra`);
      ulaz_sifra = ulaz_row.attr(`data-ulaz_sifra`);


      curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
      curr_proces = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, null, null, null );
      curr_ulaz = this_module.kalk_search(data, kalk_type, kalk_sifra, row_sifra, ulaz_sifra, null, null );

      console.log( `ovo je stara sirovina za ovaj ulaz`, curr_ulaz );

    } 
    else {

      // ponekad može biti više istih sirovina ali su montažni arci isti
      // zato trebam ubaciti u ulaz nekoliko istih ploča 
      // pa zato ne želim raditi UPSERT jer će to zamjeniti sirovinu sa prethodnom

      data = arg_data;

      product_id = arg_product_id;
      product_data = arg_product_data;

      curr_kalk = arg_kalk;
      curr_proces = arg_proces;

    };


    console.log( cit_deep( curr_proces ) );

    
    // stani ako je user obrisao sve u inputu !!!!
    if ( state == null ) {
      
      // PRIKAŽI PORUKU SAMO ako je upsert undefined ili true
      if ( upsert !== false ) {
        popup_warn(`Ako želite obrisati sirovinu onda kliknite na MINUS gumb na početku reda !!!`);
      };

      // UZMI POSVE SVJEŽI ULAZ !!!!
      // UZMI POSVE SVJEŽI ULAZ !!!!
      var postojeca_sifra = curr_ulaz.sifra; // ------> ali ostavi postojeću šifru zbog poveznica i reference
      var curr_ulaz = this_module.fresh_ulaz();
      curr_ulaz.sifra = postojeca_sifra;
      // UZMI POSVE SVJEŽI ULAZ !!!!
      // UZMI POSVE SVJEŽI ULAZ !!!!

      curr_proces.ulazi = upsert_item( curr_proces.ulazi, curr_ulaz, `sifra`);

      /*
      var data_full_name = $('#'+current_input_id).parent().attr(`data-full_name`);
      $('#'+current_input_id).val(data_full_name);
      */

      return;
    };


    
    // ------------------------------ AKO NIJE STATE NULL
    var full_name = state.sirovina_sifra + "--" + state.full_naziv + "--" + state.dobavljac?.naziv;
    $('#'+current_input_id).val(full_name);
    $('#'+current_input_id).parent().attr(`data-full_name`, full_name);

    // ako je ljepenka
    var sirov_area = (state.po_valu || 0) * (state.po_kontravalu || 0);
    // ako je papir
    if ( !sirov_area ) sirov_area = (state.kontra_tok || 0) * (state.tok || 0);
    
    var new_ulaz = convert_db_sirov_to_kalk_sirov( state, curr_ulaz );
    
    
    // kada koristim ovu funkciju unutar kalkulacije montažnih araka onda ne želim da se ovdje 
    // raditi upsert jer se može dogoditi da uzimam neoliko istih sirovina zaredom

    if ( upsert !== false ) {  

      curr_proces.ulazi = upsert_item( curr_proces.ulazi, new_ulaz, `sifra`);

      var curr_proces_index = find_index( curr_kalk.procesi, curr_proces.row_sifra , `row_sifra` );

      $(`#` + curr_kalk.kalk_rand + `_` + curr_ulaz.sifra + `_kalk_find_plate`).val(``);
      $(`#` + curr_kalk.kalk_rand + `_` + curr_ulaz.sifra + `_ulaz_element`).val(``);

      // --------------------------------------------------------------------------------
      // BEST PLATE FIND SAMO AKO ZNAM FORMULU ZA IZRAČUNAVANJE PRIREZA  ----> na primjer kod amerikanki !!!
      // --------------------------------------------------------------------------------
      if ( curr_proces_index == 0 ) { // traži best plates samo ako je prvi proces !!!

        var sirovine_ids = [];
        $.each( curr_proces.ulazi, function(u_ind, ulaz) {
          var sir_id = ulaz._id || null; // ako ima _id znači da je prava sirovina
          if ( sir_id ) sirovine_ids.push(sir_id);
        });

        var best_plate_ulazi = await this_module.best_plate_find( 
          null,
          this_comp_id,
          product_id,
          kalk_type,
          kalk_sifra,
          curr_kalk,
          sirovine_ids,
          proces_index=null
        );

        if ( best_plate_ulazi ) curr_kalk.procesi[0].ulazi = best_plate_ulazi;

      };

      this_module.update_kalk( curr_kalk, product_data, curr_proces_index, changed_ulaz=true, null, null );

      console.log(`--------------- make kalk kod odabira _kalk_find_sirov ----------------`);
      
      var this_input = $('#'+current_input_id)[0];
      var this_kalk = this_module.make_kalks( this_input, data, data[kalk_type], kalk_type, data.tip || null, `return_html` );
      $(`#${data.id} .${kalk_type}_container`).html(this_kalk);
      reset_tooltips();
      
      // this_module.write_prirez(null, this_comp_id, product_id, kalk_type, curr_kalk.kalk_sifra );

    } 
    else {
      // ovo koristim za kalkulaciju ulaza kada je MONTAŽNA PLOČA U PITANJU !!!!
      return new_ulaz;
    };

  };
  this_module.select_find_sirov = select_find_sirov;

  
  
  function kalk_plates(e, arg_data_id, arg_product_id, arg_kalk_type, arg_kalk_sifra, arg_curr_kalk ) {
    
    
    var this_button = null;
    
    var this_comp_id = null;
    var rand_id = null;
    
    var product_id = null;
    var product_data = null;
    
    var data = null;
    var product_data = null;
    
    
    if ( e ) {
    
      this_button = e.target;

      this_comp_id = $(this_button).closest('.cit_comp')[0].id;
      data = this_module.cit_data[this_comp_id];
      rand_id =  this_module.cit_data[this_comp_id].rand_id;

      
      product_id = $(this_button).closest('.product_comp')[0].id;
      product_data =  this_module.product_module.cit_data[product_id];

    } 
    else {
      
      data = this_module.cit_data[arg_data_id];
      product_data = this_module.product_module.cit_data[arg_product_id];
      
    };
    
    
    var kalk_type  = arg_kalk_type  || $(this_button).closest(`.kalk_box`).attr(`data-kalk_type`);
    var kalk_sifra = arg_kalk_sifra || $(this_button).closest(`.kalk_box`).attr(`data-kalk_sifra`);

    var curr_kalk = arg_curr_kalk || this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
    
    
    var product_elements = {};
    var plates_elements = [];
    
    
    
    var original_elems = [];
    // ne smije biti element nastao kalkulacijom nego originalni koji je user definirao !!!
    $.each( product_data.elements, function( e_ind, elem ) {
      
      if ( !elem.elem_tip.kalk ) {
        //samo originalni elems
        original_elems.push(elem);
        // popuni objekt product elems sa svim djelovima
        // property key je sifra elementa a value je koliko elemenata ima u productu
        product_elements[elem.sifra] = elem.elem_on_product;
      };
      
    });
    
    
    
    // loop po originalnim elementima
    $.each( original_elems, function( o_ind, o_elem ) {
      
      // ako postoji elem plates i ako je array
      if ( o_elem.elem_plates?.length > 0 ) {
      
        // loop po elem plates
        $.each( o_elem.elem_plates, function( pe_ind, plate ) {
        
          var already_has_plate = false;
        
          // provjeri jel već imam taj plate
          $.each( plates_elements, function( find_plate_ind, find_plate ) {
            if ( find_plate.naziv == plate.naziv ) already_has_plate = true;
          });
          
          // ubaci ako NEMAM taj plate
          if ( already_has_plate == false ) {
            
            var plate_obj = { naziv: plate.naziv, sirov_id: plate.sirov.sirov_id };
            
            // svaki plate objekt ima sve elemente na pocetku je svaki element  count nula
            $.each( original_elems, function( s_ind, s_elem ) {
              plate_obj[s_elem.sifra] = 0;
            });
    
            plates_elements.push(plate_obj);
          };
      
        }); // lopp po svakom elem plate unutar original elems
        
      }; // ako ima elem plate
      
    }); // kraj loopa po orig elems
    
    
    
    // sada imam plates array sa objektima i u svakom objektu svi elementi su value nula
    $.each( plates_elements, function( c_ind, c_plate ) {
      
      $.each( original_elems, function( o_ind, o_elem ) {
        
        // ako postoji elem plates i ako je array
        if ( o_elem.elem_plates?.length > 0 ) {
          
          $.each( o_elem.elem_plates, function( pe_ind, plate ) {
            
            // ako sam nasao plate u ovom elementu koji ima isti naziv kao u plate objektu
            if ( plate.naziv == c_plate.naziv ) {
              // povecaj count
              plates_elements[c_ind][o_elem.sifra] += plate.count;
              
            };
            
            
          }); // loop po svim plateovima koje su u elementu
          
        }; // kraj ako elem ima array plates
        
      }); // kraj loopa po orig elems
      
      
    });
    
    
     

    var ideal_koefs = {};  
    var comb_koefs = [];
    var found_perfect_combo = false;

    function check_combo( this_plate_comb , product_elements, plates_elements) {

      var this_plate_comb = cit_deep(this_plate_comb);
      
      var new_koef_object = {
        comb: this_plate_comb, 
        koefs: {},
        diffs: {},
        average_diff: null,
        plates: {},
      };

      /*
      if ( this_plate_comb[0] == 4 && this_plate_comb[1] == 2 ) {
        console.log('dobra kombinacija');
      };
      */

      // this_plate_comb je array na primjer [0, 3, 6]
      // to znaci da svaki element u montaznoj ploci pomnozim s brojem u arrayu 
      $.each(this_plate_comb, function(index, multi_value) {
        
        $.each(product_elements, function(key, count) {
          
          var add_plates = plates_elements[index][key] * multi_value;
          
          // ako ovaj key jos ne postoji stavi da je nula
          if ( !new_koef_object.plates[key] ) new_koef_object.plates[key] = 0;
          
          new_koef_object.plates[key] += add_plates;
          
        });
      });


      var plates_elem_sum = 0;
      $.each(new_koef_object.plates, function(element, count) {
        plates_elem_sum += count;
      });


      $.each(product_elements, function(key, count) {

        $.each(product_elements, function(key_2, count) {

          var current_koef = new_koef_object.plates[key] / ( new_koef_object.plates[key_2] || 1);
          
          new_koef_object.koefs[key+"-"+key_2] = current_koef
          new_koef_object.diffs[key+"-"+key_2] = current_koef - ideal_koefs[key+"-"+key_2];
        });


      });  

      var diffs_sum = 0;
      $.each(new_koef_object.diffs, function(key, diff_val) {
        diffs_sum += diff_val;
      });

      new_koef_object.average_diff = diffs_sum/Object.keys(new_koef_object.diffs).length;
      
      
      
      
      if ( found_perfect_combo == false ) {
        // ---------------------------------------------------
        
        comb_koefs.push(new_koef_object);

        var perfect = true;
        $.each(new_koef_object.diffs, function(key, diff_val) {
          if ( diff_val !== 0 ) perfect = false;
        });

        if ( perfect == true ) found_perfect_combo = true;
        
        // ---------------------------------------------------
        
      }; // kraj ako nije nasao perfect comb
      
      
    }; //  kraj check combo func
    
    function calc_plate_numbers(product_elements, plates_elements) { 

      // ovo je broj koliko maximalno stavljam jednu vrstu ploče u kombinatoriku
      // npr ako je max == 5 i ako je broj ploča 2

      const max = 10;

      // na početku resetiram varijablu jesam li našao savršenu kombinaciju
      found_perfect_combo = false;

      // resetiraj array svih kombinacija
      comb_koefs = [];
      // resetiraj ideal koefs
      ideal_koefs = {};

      if ( !product_elements ) {

        product_elements = {
          back_i_sides: 1,
          footer: 1,
          header: 1,
          shelfs: 4
        };

      };

      if ( !plates_elements ) {

        plates_elements = [
          { back_i_sides: 2, footer: 1, header: 0, shelfs: 0 }, 
          { back_i_sides: 0, footer: 2, header: 4, shelfs: 10 },
          /* { back_i_sides: 0, footer: 0, header: 0, shelfs: 6 },*/
          /* { back_i_sides: 3, footer: 0, header: 0, shelfs: 2 } */
        ];

      };

      var elem_sum = 0;
      // izračunaj sumu svih djelova u productu
      $.each(product_elements, function(key, count) {
        elem_sum += count;
      });


      var ideal_koefs_sum = 0;
      // izračunaj postotak pojedinog elementa unutar produkta

      $.each(product_elements, function(key, count) {

        $.each(product_elements, function(key_2, count) {

          ideal_koefs[key+"-"+key_2] = product_elements[key] / (product_elements[key_2] || 1);

          ideal_koefs_sum += ideal_koefs[key]; // ovo je uvijek 1 !!!!! tj sum svih parcijala mora biti 1

        });

      });

      var ideal_koefs_length = Object.keys(ideal_koefs).length;

      ideal_koefs.average = ideal_koefs_sum/ideal_koefs_length;

      var plate_combs = [];

      // koliko ima plate objekata toliko ima nula u plate combs

      $.each(plates_elements, function(p_ind, plate_obj) {
        plate_combs.push(0);
      });

      // npr imam tri pozicije 
      // [0,0,0] i svaki broj može ići max do na primjer broja 5
      // to znači da ima 6 mogućnosti tj od 0-5 == 6 na 3 == 216 kombinacija
      for (i = 1; i <= Math.pow(max + 1, plate_combs.length); i++) {

        var power_exp = 0;

        if ( found_perfect_combo == false ) {

          // idem od zadnje pozicije do prve pozicije u arraju [0,0,0]
          for ( poz = plate_combs.length-1; poz >= 0; poz--) {

            power_exp += 1;

            // console.log(i, poz);

            // ako je i > 6 na 1 i ako je modulus i u odnosu na 6 == 1 ( u biti ako je i == 7 )  
            if ( i > Math.pow(max + 1, power_exp) && i % Math.pow(max + 1, power_exp) == 1) {
              // onda resetiraj poziciju na drugom mjestu dakle da ne bude [0,6] nego vrati na [0,0]
              plate_combs[poz] = 0;
            } else {
              // ako nije 7. iteracija
              if ( poz == plate_combs.length-1 ) {
                // i ako je zadnja pozicija tj u primjeru to je treća nula u [0,0,0]
                // i ako je i veći od 1 tj nije prva iteracija
                if ( i > 1 ) plate_combs[poz] += 1; // onda povećaj zadnji broj za +1
              } else {
                // ako nije zadnja pozicija
                // onda provjeri da li je iteracija veća od 6 na 0
                // i također provjeri da li je i modulus od 6 na 0 == 1
                if ( i > Math.pow(max + 1, power_exp-1) && i % Math.pow(max + 1, power_exp-1) == 1 ) {
                  plate_combs[poz] += 1;
                };
              }
            }; // else kraj uvjeta ako nije 7. iteracija

            // console.log(poz, plate_combs[poz]);

          }; // kraj loopa po pozicijama arraja

          check_combo( plate_combs, product_elements, plates_elements);

        }; // kraj ako nije nasao perfect combo

      }; // kraj loopa sa svim kombinacijama multiplajera


      if ( found_perfect_combo == true ) {

        // console.log( comb_koefs[comb_koefs.length-1] );

        return {
          plate_comb: comb_koefs[comb_koefs.length-1],
          product_elements,
          plates_elements,
        }

      } else {

        // removaj sve kombinacije koje u sebi imaju bilo koji element da je nula komada
        // idem UNAZAD po arraju zato što brišem iteme 
        // u protivnom bih smrdao indexe u arrajy pošto brišem iteme
        var c_index;
        var remove_inds = [];

        for ( c_index = comb_koefs.length-1; c_index >= 0; c_index-- ) {
          var already_found_zero = false;
          $.each(comb_koefs[c_index].plates, function(element, count) {
            if ( count == 0 && already_found_zero == false ) {
              remove_inds.push(c_index);
              already_found_zero = true;
            };
          });
        };

        for ( r_ind = 0; r_ind <= remove_inds.length-1; r_ind++ ) {
          comb_koefs.splice(remove_inds[r_ind], 1);
        };

        // DELETE sve multipliere na primjer [2,3] ima mulitplier [4,6] i tako dalje !!!!
        //cjelim prve brojeve pa onda druge brojeve  pa trece brojeve i tako do kraja
        // 4/2 = 2
        // 6/3 = 2
        
        // AKO SU SVI RECULTATI DJELJENJA ISTI BROJ TO ZNAČI DA JE U BIT ISTA KOMBINACIJA !!!!!!
        
        $.each(comb_koefs, function(comb_ind, comb) {
          var original_comb = comb.comb;

          $.each(comb_koefs, function(comparison_ind, comparison_comb) {

            // moram izbjeći da uspoređuje iste iteme
            // to izbjegnem takoda da NE SMIJU biti isti indexi !!!!!
            if ( comparison_ind !== comb_ind ) {
              
              var compare = comparison_comb.comb;
              var integer_count = 0;
              var past_divide_result = null;

              $.each(original_comb, function(o_ind, o_value) {

                if ( Number.isInteger( compare[o_ind] / o_value) ) {

                  if ( past_divide_result == null ) {
                    // ako je nulti loop onda podjeli i spremi u past_divide_result
                    past_divide_result = compare[o_ind]/o_value;
                    integer_count += 1;
                  } else {
                    // ako NIJE nulti loop... djeljenje onda usporedi sa prošlim djelenjem !!!
                    if ( compare[o_ind]/o_value == past_divide_result ) {
                      // past_divide_result = compare[o_ind]/o_value;
                      integer_count += 1;
                    }
                  };

                };

              });

              // ako su svi rezultati djeljenja isti broj
              if ( integer_count == compare.length ) {
                // stavi property da je to multiplajer
                comb_koefs[comparison_ind].multiplier_for = comb_ind;
              };

            }; // izbjegni iste indexe !!!

          });

        });

        // DELETE sve multipliere na primjer [2,3] ima mulitplier [4,6] i tako dalje !!!!
        for ( c_index = comb_koefs.length-1; c_index >= 0; c_index-- ) {
          if ( comb_koefs[c_index].hasOwnProperty('multiplier_for') == true ) {
            comb_koefs.splice(c_index, 1);
          };
        };


        /*
        $.each(comb_koefs, function(comb_ind, comb) {

          var diff_graf_points = [];
          var x_pos = 0;
          $.each(comb.diffs, function(element, diff_val) {
            diff_graf_points.push( {x: x_pos, y: diff_val});
            x_pos += 1;
          });

          // loop sve points koje sam napravio
          var p;
          var distance = 0;
          for( p = 0; p <= diff_graf_points.length-2; p++) {

            var kateta_1 = diff_graf_points[p].x - diff_graf_points[p+1].x;
            var kateta_2 = diff_graf_points[p].y - diff_graf_points[p+1].y;
            var hipotenuza = Math.sqrt( kateta_1*kateta_1 + kateta_2*kateta_2 );
            distance += hipotenuza;
          };

          comb_koefs[comb_ind].distance = distance;

        });
        multisort(comb_koefs, ['distance']);
        */

        
        
        $.each(comb_koefs, function(comb_ind, comb) {

          var zero_diff_count = 0;
          $.each(comb.diffs, function(key, diff ) {
            if ( diff == 0 ) zero_diff_count += 1;
          });    
          comb_koefs[comb_ind].zero_diff_count = zero_diff_count;
        });
        
        multisort(comb_koefs, ['~zero_diff_count']);

        // console.log(JSON.stringify(comb_koefs));
        console.log(comb_koefs);

        var just_combs = [];
        $.each(comb_koefs, function(comb_ind, comb) {
          just_combs.push( comb.comb );
        });
        console.log(just_combs);

        // vrati prvi objekt koji je zapravo onaj koji najmanje odstupa od idealnog jer ima najveci broj nula u diffovima !!!!
        return {
          plate_comb: comb_koefs[0],
          product_elements,
          plates_elements,
        };
          


      }; // kraj od else ( ako nije našao perfect combo )

    }; // kraj glavne calc plate nums
    
    return calc_plate_numbers(product_elements, plates_elements);
    
    // return calc_plate_numbers(null, null);

    
  };
  this_module.kalk_plates = kalk_plates;
  
  
  
  // ---------------------------------------------- SAVE SIROV OSTATAK ----------------------------------------------
  // ----------------------START------------------- SAVE SIROV OSTATAK ----------------------------------------------
  // -----ZA SADA NE KORISTIM OVO !!!!!
  // -----ZA SADA NE KORISTIM OVO !!!!!

  function save_sirov_in_kalk(new_sirov) {

    return new Promise( function(resolve, reject) {

      var this_comp_id = $('#save_sirov_btn').closest('.cit_comp')[0].id; // useo sam ovaj gumb bezveze  - nije bitno koji id

      toggle_global_progress_bar(true);

      $.each( new_sirov, function( key, value ) {
        // ako je objekt ili array onda ga flataj
        if ( $.isPlainObject(value) || $.isArray(value) ) {
          new_sirov[key+'_flat'] = JSON.stringify(value);
        };
      });


      if ( 
        new_sirov.kvaliteta_1 
        && 
        new_sirov.kvaliteta_2
      ) {

        new_sirov.kvaliteta_mix = new_sirov.kvaliteta_1 + "/" + new_sirov.kvaliteta_2;
      };


      if ( 
        new_sirov.kontra_tok !== null
        && 
        new_sirov.tok !== null
      ) {

        new_sirov.kontra_tok_x_tok = "P" + new_sirov.kontra_tok + "X" + new_sirov.tok;
      };


      if ( 
        new_sirov.po_valu !== null
        && 
        new_sirov.po_kontravalu !== null
      ) {

        new_sirov.val_x_kontraval = new_sirov.po_valu + "X" + new_sirov.po_kontravalu;
      };


      var data = new_sirov;

      var full_naziv = 
            (data.klasa_sirovine ? data.klasa_sirovine.naziv+" " : "") +
            (data.tip_sirovine ? data.tip_sirovine.naziv+" " : "") +
            (data.vrsta_materijala ? data.vrsta_materijala.naziv+" " : "") +
            (data.naziv ? data.naziv + " " : "") +
            (data.kvaliteta_1 ? data.kvaliteta_1 + "/" : "") +
            (data.kvaliteta_2 ? data.kvaliteta_2 + " " : "") +
            (data.po_valu ? data.po_valu + "X" : "") +
            (data.po_kontravalu ? data.po_kontravalu : "");

      new_sirov.full_naziv = full_naziv;


      // ---------------------------- PRETVORI SVE STATUS OBJEKTE U SAMO _id array ----------------------------
      var status_ids = [];
      // obriši statuse jer ću id uzimati ručno !!!!!
      if ( data.statuses && $.isArray(data.statuses) && data.statuses.length > 0 ) {
        $.each(data.statuses, function( ind, status )  {
          status_ids.push(status._id);
        });
      };
      new_sirov.statuses = status_ids;
      // ---------------------------- PRETVORI SVE STATUS OBJEKTE U SAMO _id array ----------------------------


      new_sirov = save_metadata( new_sirov );

      return;



      $.ajax({
        headers: {
          'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
        },
        type: "POST",
        cache: false,
        url: `/save_sirov`,
        data: JSON.stringify( new_sirov ),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done(function (result) {

        console.log(result);
        if ( result.success == true ) {

          new_sirov._id = result.sirov._id;
          new_sirov.sirovina_sifra = (result.sirov.sirovina_sifra || null);
          var rand_id = new_sirov.rand_id;
          $(`#`+rand_id+`_sirovina_sifra`).val( new_sirov.sirovina_sifra );

          cit_toast(`SPREMLJEN NOVI RESTL !!!`);

        } else {
          if ( result.msg ) popup_error(result.msg);
          if ( !result.msg ) popup_error(`Greška prilikom spremanja restl-a !`);
        };

      })
      .fail(function (error) {

        console.log(error);
        popup_error(`Došlo je do greške na serveru prilikom spremanja!`);
      })
      .always(function() {

        toggle_global_progress_bar(false);

      });


    }); // kraj promisa

  };
  this_module.save_sirov_in_kalk = save_sirov_in_kalk;
  
  // ---------------------------------------------- SAVE SIROV OSTATAK ----------------------------------------------
  // -----------------------END-------------------- SAVE SIROV OSTATAK ----------------------------------------------
  // ---------------------------------------------- SAVE SIROV OSTATAK ----------------------------------------------
  
  
  async function get_product_module() {
    this_module.product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);  
  };
  get_product_module();
  
  
  async function get_project_module() {
    this_module.project_module = await get_cit_module(`/modules/project/project_module.js`, `load_css`);  
  };
  get_project_module();
  
  
  function populate_elements( arg_product_data, arg_data, arg_curr_kalk ) {
    
    var this_comp_id = null;
    var data = null;
    var rand_id = null;
    
    var product_id = null;
    var product_data =  null;

    var kalk_type = null;
    var kalk_sifra = null;
    var kalk_box = null;
    
    if ( !arg_product_data ) {
    
      this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
      data = this_module.cit_data[this_comp_id];
      rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
      product_id = $('#'+current_input_id).closest('.product_comp')[0].id;
      product_data =  this_module.product_module.cit_data[product_id];
    
      kalk_box = $('#'+current_input_id).closest('.kalk_box');
      kalk_type = kalk_box.attr(`data-kalk_type`);
      kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
      
    } else {
      
      product_data = arg_product_data;
      data = arg_data;
      
    };
    
    var curr_kalk = arg_curr_kalk || this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
    
    
    
    // PRVO OBRIŠI SVE KALK ELEMENTA ( to su elementi nastali prilikom kalkulacije u nekom procesu )
    for( i = product_data.elements.length-1; i==0; i-- ) {
      if ( product_data.elements[i].elem_tip.kalk == true ) product_data.elements.splice(i, 1);
    };
    
    
    // loop po svim procesima
    $.each( curr_kalk.procesi, function(p_ind, proc) {
      
      // UPSERT ITEM ULAZ ELEMENT
      $.each( proc.ulazi, function(u_ind, ulaz) {
        if ( ulaz.kalk_element ) {
          product_data.elements = upsert_item(product_data.elements, ulaz.kalk_element, `sifra`);
        };
      });
      
      
      // UPSERT ITEM IZLAZ ELEMENT
      $.each( proc.izlazi, function(i_ind, izlaz) {
        if ( izlaz.kalk_element ) {
          product_data.elements = upsert_item(product_data.elements, izlaz.kalk_element, `sifra`);
        };
      });
      
    });
    
    // samo ako NISAM već dao product data kao argument tj
    // ako JOŠ ne postoje elementi u html-u onda nemoj praviti element listu
    // if ( !arg_product_data )
      
    this_module.product_module.make_element_list(product_data);
    
    return product_data;
    
    
  };
  this_module.populate_elements = populate_elements;
  
  
  function gen_ulazi_from_izlazi(arg_izlazi) {
    
    var ulazi = [];
    
    // KREIRAM ulaz od prethodnoh izlaza  i zapisujem sifru izlaza u property izlaz sifra
    // KREIRAM ulaz od prethodnoh izlaza  i zapisujem sifru izlaza u property izlaz sifra
    // KREIRAM ulaz od prethodnoh izlaza  i zapisujem sifru izlaza u property izlaz sifra

    $.each( arg_izlazi, function( i_ind, work_izlaz ) {
      
      var new_ulaz = {
      
        ...work_izlaz,

        _id: null,
        prodon_sirov_id: work_izlaz.prodon_sirov_id || null,
        
        izlaz_sifra: work_izlaz.sifra,

        sifra: "ulaz"+cit_rand(),
        
        kalk_unit_area: ( work_izlaz.kalk_po_valu || 0 ) * (work_izlaz.kalk_po_kontravalu || 0 ) / 1000000,
        kalk_sum_sirovina: (work_izlaz.kalk_kom_in_sirovina || 1) * (work_izlaz.kalk_sum_sirovina || 1 ),

        kalk_kom_in_product: 1,
        kalk_kom_in_sirovina: 1,
        kalk_unit_price: 0,
        real_unit_price: 0,
        kalk_price: 0,
        kalk_ostatak_val: null,
        kalk_ostatak_kontra: null,

      };

      ulazi.push(new_ulaz);

    });
    
    return ulazi;
    
  };
  this_module.gen_ulazi_from_izlazi = gen_ulazi_from_izlazi;
  
  
  function gen_izlazi( 
  
  arg_just_first,
   
  arg_changed_props, // svi property koje sam izmjenio u nekom procesu sa work funkcijom  ---> njih ću samo dodati sa tri točkice na kraju izlaz objekta
  proces_index,
   
  curr_kalk,
  ulazi,
  arg_radna_stn,
   
  ukupno_dobiveno_iz_ulaza,
   
  arg_prirez_val,
  arg_prirez_kontra,

  arg_vrsta_materijala,
  arg_kvaliteta_1,
  arg_kvaliteta_2,
  arg_debljina_mm,
  arg_gramaza_g

) {
   
    
    // provjeri jel PRVI ulaz ima ove propertije
    var vrsta_materijala_ulaz = ulazi[0]?.vrsta_materijala;
    var kvaliteta_1_ulaz = ulazi[0]?.kvaliteta_1;
    var kvaliteta_2_ulaz = ulazi[0]?.kvaliteta_2;
    var debljina_mm_ulaz = ulazi[0]?.debljina_mm;
    var gramaza_g_ulaz = ulazi[0]?.gramaza_g;
    
    var ulazi_su_isti_materijal = true;
    
    var izlazi = [];
    
    // ako bilo koji ulaz osim prvog ima bilo koje druge propse 
    // onda naravno materijal nije isti !!! u svim ulazima
    
    $.each( ulazi, function(u_ind, ulaz) {
      
      // --------------------- AKO NIJE POMOĆNI ELEMENT ---------------------
      // --------------------- AKO NIJE POMOĆNI ELEMENT ---------------------
      // --------------------- AKO NIJE POMOĆNI ELEMENT ---------------------
      
      // NE SMJE BITI  ------> tip sirovine je naziv: "POMOĆNI ELEMENT"
      if ( ulaz.tip_sirovine?.sifra !== `TSIR11` ) {
      

        if ( 
            ulaz.vrsta_materijala !== vrsta_materijala_ulaz || 
            ulaz.kvaliteta_1      !== kvaliteta_1_ulaz      ||
            ulaz.kvaliteta_2      !== kvaliteta_2_ulaz      || 
            ulaz.debljina_mm      !== debljina_mm_ulaz      ||
            ulaz.gramaza_g        !== gramaza_g_ulaz

          ) {

          ulazi_su_isti_materijal = false;

        };
        
 
        

        var proces_index_exists = false;
        
        if ( proces_index || proces_index === 0 ) proces_index_exists = true;

        var izlaz_elem = {

          sifra: 'elem'+cit_rand(),

          add_elem: null,

          elem_val: null,
          elem_kontra: null,

          elem_parent: null,

          elem_tip: { sifra: arg_radna_stn.elem_sifra, naziv: arg_radna_stn.elem_naziv, proces: arg_radna_stn.type, kalk: true },
          elem_opis: `P${ proces_index_exists ? (proces_index+1) + `-${u_ind+1}` : `???` } IZLAZ nakon ${ arg_radna_stn.proces_type_naziv } na ${ arg_radna_stn.naziv }`,

          elem_cmyk: ``,
          elem_pantone: ``,
          elem_vrsta_boka: null,
          elem_plates: null,

          elem_on_product: 1,

          elem_RB: null,
          elem_polica_razmak: null,
          elem_polica_pregrade: null,
          elem_polica_nosivost: null,
          elem_front_orient: null,
          elem_plate_count: 1,

        };

        // var copy_ulaz = null;

        // if ( ulazi.length == 1 ) copy_ulaz = ulazi[0];


        var ulaz_copy = cit_deep(ulaz);
        if ( ulaz._id || ulaz.sirov_id ) ulaz_copy = reduce_sir(ulaz_copy);
        
        
        var proc_skart = curr_kalk.procesi[proces_index].proces_skart ? (1 - curr_kalk.procesi[proces_index].proces_skart) : 1;
        
        izlazi.push({ 

          /* ...copy_ulaz, */

          ...ulaz_copy,

          sifra: "izlaz"+cit_rand(),

          kalk_full_naziv: arg_radna_stn.elem_naziv,
          kalk_element: izlaz_elem,


          kalk_kom_in_product: izlaz_elem.elem_on_product || 1,
          kalk_sum_sirovina: Math.ceil( ukupno_dobiveno_iz_ulaza * proc_skart ),
          kalk_extra_kom: null,
          kalk_skart: null,

          kalk_po_valu: arg_prirez_val || null,
          kalk_po_kontravalu: arg_prirez_kontra || null,
          kalk_skart: null,

          /*

          ako ako su svi materijali isti u svakom ulazu onda mogu u ovaj ejdan jedini izlaz staviti te properije
          postoji još opcija da predam ovoj funkciji sve te propertije za materijal pute argumenata !!!!!!

          */

          vrsta_materijala: ulazi_su_isti_materijal ? vrsta_materijala_ulaz : arg_vrsta_materijala || null,

          kvaliteta_1: ulazi_su_isti_materijal ? kvaliteta_1_ulaz : arg_kvaliteta_1 || null,
          kvaliteta_2: ulazi_su_isti_materijal ? kvaliteta_2_ulaz : arg_kvaliteta_2 || null,
          debljina_mm: ulazi_su_isti_materijal ? debljina_mm_ulaz : arg_debljina_mm || null,
          gramaza_g: ulazi_su_isti_materijal ? gramaza_g_ulaz : arg_gramaza_g || null,



          ...arg_changed_props,

          izlaz_sifra: null, // UVIJEK TREBA BITI NULL JER SAMO ULAZI TREBAJU IMATI OVAJ PROP !!!!

        });
        
        

      };
      
      // --------------------- AKO NIJE POMOĆNI ELEMENT ---------------------
      // --------------------- AKO NIJE POMOĆNI ELEMENT ---------------------
      // --------------------- AKO NIJE POMOĆNI ELEMENT ---------------------
      
      
    });  
    // kraj loopa po svim ulazima
        
        
    if ( arg_just_first ) {
      return [ izlazi[0] ]; // vrati samo prvi
    }
    else {
      return izlazi;
    };

    
  };
  this_module.gen_izlazi = gen_izlazi;
  
  
  function gen_action( arg_change_action_props, curr_kalk, prep_time, work_time, unit_time, time, unit_price, price, radna_stn, arg_curr_proces_index, arg_worker_num, arg_workers ) {
    
    var action_type = null;
    var action = null;
    
    var custom_prep_time = null;
    var custom_work_time = null;
    var custom_unit_time = null;
    
    // ako postoji arg proces index
    var extra_data = ( arg_curr_proces_index || arg_curr_proces_index == 0) ? curr_kalk.procesi[arg_curr_proces_index].extra_data : null;
    
    if ( extra_data ) {
      custom_prep_time = curr_kalk.procesi[arg_curr_proces_index].extra_data.action_prep_time || null;
      custom_work_time = curr_kalk.procesi[arg_curr_proces_index].extra_data.action_work_time || null;
      custom_unit_time = curr_kalk.procesi[arg_curr_proces_index].extra_data.action_unit_time || null;
    };
    
    
    return { 
      
      action_func: radna_stn.func,
      action_sifra: `action`+cit_rand(),
      action_type: { sifra: radna_stn.type, naziv: radna_stn.proces_type_naziv },
      action_radna_stn: radna_stn,

      action_prep_time: custom_prep_time || prep_time,
      action_work_time: custom_work_time || work_time,
      action_unit_time: custom_unit_time || unit_time,
      
      action_multiload: 1,
      action_kasir_num: 1,
      
      /*
      action_glue_mm: null,
      action_glue_speed: null,
      action_glue_tlak: null,
      action_glue_dizna: null,
      action_glue_max_size: null,
      */
      
      
      action_unit_price: unit_price,
      action_est_price: price,
      
      custom_prep_time: null,
      custom_work_time: null,
      
      action_worker_num: arg_worker_num || 0,

      action_workers: arg_workers || [],
      
      
      ...arg_change_action_props
      
    };

    
  };
  this_module.gen_action = gen_action;
  

  
  async function get_partner(partner_id) {
    
    return new Promise( async function(resolve, reject) {
    
      var db_partner = null;

      var props = {
        filter: null,
        url: '/find_partner',
        query: { _id: partner_id },
        desc: `pretraga PARTNERA / KUPCA da dobijem sve podatke kako bih mogao generirati dokument u modulu kalk  !!!`,
      };

      try {
        db_partner = await search_database(null, props );
      } catch (err) {
        console.log(err);
        resolve(null);

      };

      // ako je našao nešto onda uzmi samo prvi član u arraju
      if ( db_partner && $.isArray(db_partner) && db_partner.length > 0 ) {
        db_partner = db_partner[0];
        resolve(db_partner);
      } else {
        resolve(null);
      };
    
    }); // kraj promisa
    
    
  };
  this_module.get_partner = get_partner;
  
  
  /*
  -------------------------------------------------------
  ZA SADA NE KORISTIM OVO
  -------------------------------------------------------
  
  function get_last_sirov_records( sirovs ) {
    
    
    $.ajax({
      headers: {
        'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
      },
      type: "POST",
      cache: false,
      url: `/get_last_sirov_records`,
      data: JSON.stringify( sirovs ),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done(function (result) {
      
      console.log(result);
      
      if ( result.success == true ) {

        
        
      } else {
        if ( result.msg ) popup_error(result.msg);
        if ( !result.msg ) popup_error(`Greška prilikom spremanja!`);
      };

    })
    .fail(function (error) {
      
      console.log(error);
      popup_error(`Došlo je do greške na serveru prilikom spremanja kalkulacije !!!`);
    })
    .always(function() {
      
      toggle_global_progress_bar(false);
      $(this_button).css("pointer-events", "all");
      
    });
    
    
  };
  this_module.get_last_sirov_records = get_last_sirov_records;
  
  
  -------------------------------------------------------

  */
  
  function save_sirov_records(records) {
    
    return new Promise( function(resolve, reject) {

      toggle_global_progress_bar(true);

      $.ajax({
          headers: {
            'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
          },
          type: "POST",
          cache: false,
          url: `/save_sirov_records`,
          data: JSON.stringify( records ),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
        .done(function (result) {

          console.log(result);

          if ( result.success == true ) {
            
            cit_toast(`ZAPISI SPREMLJENI U BAZU SIROVINA !`);
            
            // vrati mu nazad te iste recorde jer inako su recordi koje sam zapisao u bazu posve isti kao i ovi lokalni
            resolve(records);

          } else {
            
            if ( result.msg ) popup_error(result.msg);
            if ( !result.msg ) popup_error(`Greška prilikom spremanja rezervacija robe !!!`);
            
            resolve(null);

          };

        })
        .fail(function (error) {

          console.log(error);
          popup_error(`Došlo je do greške na serveru prilikom spremanja rezervacija robe !!!`);
          resolve( null );

        })
        .always(function() {
          toggle_global_progress_bar(false);
        });

      
    }); // kraj promisa  
    
    
  };
  this_module.save_sirov_records = save_sirov_records;
  
  
  function ajax_storno_sirov_records(records) {
    
    return new Promise( function(resolve, reject) {

      toggle_global_progress_bar(true);

      $.ajax({
          headers: {
            'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
          },
          type: "POST",
          cache: false,
          url: `/storno_sirov_records`,
          data: JSON.stringify( records ),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
        .done(function (result) {

          console.log(result);

          if ( result.success == true ) {
            
            cit_toast(`REZERVACIJE SU OBRISANE!`);
            
            resolve(true);

          } else {
            
            if ( result.msg ) popup_error(result.msg);
            if ( !result.msg ) popup_error(`Greška prilikom spremanja BRISANJA reservacije robe !!!`);
            
            resolve(null);

          };

        })
        .fail(function (error) {

          console.log(error);
          popup_error(`Došlo je do greške na serveru prilikom BRISANJA reservacije robe !!!`);
          resolve( null );

        })
        .always(function() {
          toggle_global_progress_bar(false);
        });

      
    }); // kraj promisa  
    
    
  };
  this_module.ajax_storno_sirov_records = ajax_storno_sirov_records;
  
  
  async function storno_reserv( arg_kalk_sifra, this_elem, arg_sirov_arr, doc_type  ) {
    
    var this_comp_id = $(this_elem).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    var product_id = $(this_elem).closest('.product_comp')[0].id;
    var product_data =  this_module.product_module.cit_data[product_id];
    

    var kalk_box = $(this_elem).closest('.kalk_box');

    var kalk_type = kalk_box.attr(`data-kalk_type`);
    var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);

    var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );


    var curr_reserv_sifra = curr_kalk.kalk_sifra;
    var curr_reserv_state = curr_kalk.kalk_reserv;
    var curr_for_pro_state = curr_kalk.kalk_for_pro;
    
    var sirovine = [];
    //!!!findsirov!!!
    // pronađi sve sirovine koje imaju u recordu ovu kalk sifru
    var props = {
      filter: null,
      url: '/find_sirov',
      query: { "records.record_kalk.kalk_sifra" : arg_kalk_sifra, "records.type": doc_type },
      desc: `pretraga za svih recorda rezervacije da bi napravio storno reservacije robe ${arg_kalk_sifra} --- cit_rand ${ cit_rand() }`,
    };

    
    try {
      sirovine = await search_database(null, props );
    } catch (err) {
      
      console.log(err);
      return false;
    };


    var storno_result = null;
    
    var all_records_for_storno = []; // ovdje stavljam stavljam samo sifru i sirov id

    
    if ( sirovine?.length > 0 ) {
      
      $.each( sirovine, function( s_ind, sirov) {
        
        $.each( sirov.records, function( r_ind, record) {
          
          // pronađi samo one recorde koji već nisu stornirani
          if ( record.record_kalk?.kalk_sifra == arg_kalk_sifra && record.storno_time == null ) {
            
            all_records_for_storno.push( { sifra: record.sifra, sirov_id: record.sirov_id } );
            
          };
          
        });
        
      });
      
      
      
      console.log( all_records_for_storno );

      if ( all_records_for_storno.length == 0 ) {
        popup_warn(`Već su sve rezervacije obrisane!!!`);
        return;
      };

      storno_result = await this_module.ajax_storno_sirov_records(all_records_for_storno);
      
      
    } else {
      
      popup_warn(`U bazi ne postoji zapis za rezervaciju robe po ovoj kalkulaciji!!! `);
      return;
      
    };
    
    
    
    return storno_result;
    
  };
  this_module.storno_reserv = storno_reserv;
  
  
  function fresh_ulaz() {
    
    var ulaz =  { 

      sifra: "ulaz"+cit_rand(),
      _id: null,
      kalk_full_naziv: null,

      kalk_element: null,

      kalk_po_valu: null,
      kalk_po_kontravalu: null,

      uzimam_po_valu:  null,
      uzimam_po_kontravalu: null,

      kalk_ostatak_val: null,
      kalk_ostatak_kontra: null,

      kalk_unit_area: null,
      kalk_unit: null,

      kalk_unit_price: null,
      real_unit_price: null,

      kalk_price: null,

      kalk_kom_in_product: null,
      kalk_kom_in_sirovina: 1,
      kalk_kom_val: null,
      kalk_kom_kontra: null,
      
      kalk_sum_sirovina: null,
      kalk_extra_kom: null,
      kalk_skart: null,
      

      kalk_noz_big: null,
      kalk_nest_count: null,
      kalk_color_cover: null,
      
      kalk_color_C: null,
      kalk_color_M: null,
      kalk_color_Y: null,
      kalk_color_K: null,
      kalk_color_W: null,
      kalk_color_V: null,
      
      
      vrsta_materijala: null,
      klasa_sirovine: null,
      tip_sirovine: null,
      
      
      kvaliteta_1: null,
      kvaliteta_2: null,
      debljina_mm: null,
      gramaza_g: null,
      

    };
    
    return ulaz;
    
  };
  this_module.fresh_ulaz = fresh_ulaz;
  
  
  
  function fresh_izlaz() {
    
    
    
    var izlaz_elem = {

      sifra: 'elem'+cit_rand(),

      add_elem: null,
      
      elem_val: null,
      elem_kontra: null,


      elem_parent: null,

      elem_tip: null,
      elem_opis: null,

      elem_cmyk: ``,
      elem_pantone: ``,
      elem_vrsta_boka: null,
      elem_plates: null,

      elem_on_product: 1,

      elem_RB: null,
      elem_polica_razmak: null,
      elem_polica_pregrade: null,
      elem_polica_nosivost: null,
      elem_front_orient: null,

    };


    var new_izlaz = { 
      
        sifra: "izlaz"+cit_rand(),

        kalk_full_naziv: null,
        kalk_element: null,

        kalk_kom_in_product: 1,
        kalk_sum_sirovina: null,
        kalk_extra_kom: null,

        kalk_po_valu: null,
        kalk_po_kontravalu: null,
        kalk_skart: null,
      
        vrsta_materijala: null,
        klasa_sirovine: null,
        tip_sirovine: null,
      
      
        kvaliteta_1: null,
        kvaliteta_2: null,
        debljina_mm: null,
        gramaza_g: null,
      

      };

    
    return new_izlaz;
    
    
    
  };
  this_module.fresh_izlaz = fresh_izlaz;
  
  
  function fresh_proces(arg_radna_stanica) {
    
    var ulazi = [

      this_module.fresh_ulaz()

    ];


    var izlaz_elem = {

      sifra: 'elem'+cit_rand(),

      add_elem: null,
      
      elem_val: null,
      elem_kontra: null,

      elem_parent: null,

      elem_tip: null,
      elem_opis: null,

      elem_cmyk: ``,
      elem_pantone: ``,
      elem_vrsta_boka: null,
      elem_plates: null,

      elem_on_product: 1,

      elem_RB: null,
      elem_polica_razmak: null,
      elem_polica_pregrade: null,
      elem_polica_nosivost: null,
      elem_front_orient: null,

    };


    var izlazi = [
      { 
        sifra: "izlaz"+cit_rand(),

        kalk_full_naziv: null,
        kalk_element: izlaz_elem,

        kalk_kom_in_product: 1,
        kalk_sum_sirovina: null,
        kalk_extra_kom: null,

        kalk_po_valu: null,
        kalk_po_kontravalu: null,
        kalk_skart: null,
        
        vrsta_materijala: null,
        klasa_sirovine: null,
        tip_sirovine: null,
        
        kvaliteta_1: null,
        kvaliteta_2: null,
        debljina_mm: null,
        gramaza_g: null,

      },

    ];

    var action = { 
      action_func: arg_radna_stanica?.func || null,
      action_sifra: `action`+cit_rand(),
      action_type: arg_radna_stanica ? { sifra: arg_radna_stanica.type, naziv: arg_radna_stanica.proces_type_naziv } : null,
      action_radna_stn: arg_radna_stanica || null,

      action_prep_time: null,
      action_work_time: null,
      
      action_multiload: 1,
      action_kasir_num: 1,
      
      
      /*
      action_glue_mm: null,
      action_glue_speed: null,
      action_glue_tlak: null,
      action_glue_dizna: null,
      action_glue_max_size: null,
      */
      
      action_unit_price: null,
      action_est_price: null,
      
      action_worker_num: 0,
      action_workers: [],
    };

    var proces = {

        extra_data: {},

        proc_koment: "",
        row_sifra: `procesrow`+cit_rand(),
        ulazi: ulazi,
        action: action,
        izlazi: izlazi, 
      };
    
    return proces;
    
  }
  this_module.fresh_proces = fresh_proces;
  
  
  function ajax_proces_rn_update_statuses(kalk_id, proces_sifra, arg_rn_type) {
    
    return new Promise( function(resolve, reject) {
    
      toggle_global_progress_bar(true);

      $.ajax({
        headers: {
          'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
        },
        type: "POST",
        cache: false,
        url: `/proces_rn_update_statuses`,
        data: JSON.stringify({

          rn_type: arg_rn_type, 
          kalk_id: kalk_id,
          proces_id: proces_sifra,
          user: {

            time: Date.now(),
            _id: window.cit_user._id,
            user_number: window.cit_user.user_number,
            full_name: window.cit_user.full_name,
            email: window.cit_user.email
          }
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done(function (result) {

        console.log(result);
        if ( result.success == true ) {

          resolve( result.statuses );
          
          cit_toast(`UPDATE RADNOG PROCESA UPISAN !!!`);

        } else {
          resolve( null );
          if ( result.msg ) popup_error(result.msg);
          if ( !result.msg ) popup_error(`Greška prilikom spremanja odustajanja od radnog procesa !`);
        };

      })
      .fail(function (error) {

        resolve( null );
        console.log(error);
        popup_error(`Došlo je do greške na serveru prilikom spremanja!`);
      })
      .always(function() {

        toggle_global_progress_bar(false);

      });

    }); // kraj promisa
    
  };
  this_module.ajax_proces_rn_update_statuses = ajax_proces_rn_update_statuses;
  
  
  function make_ugovor_or_tender_list(data, curr_kalk) {

    var ugovor_or_tender_props = {

      desc: 'samo za kreiranje tablice JEDNOG ugovora ili tendera u offer kalkulaciji' + data.rand,
      local: true,

      list: curr_kalk.kalk_fixed_price,

      show_cols: [ 
        "naziv",
        "date",
        "rok",
        "status_naziv",
        "start",
        "end",
        "komentar", 
        "doc_links",
        "button_delete",
      ],
      format_cols: { date: "date", rok: "date", start: "date", end: "date" },

      col_widths: [
          2, // "naziv",
          1, // "date",
          1, // "rok",
          2, // "status_naziv",
          1, // "start",
          1, // "end",
          3, // "komentar", 
          4, // "doc_links"
          1, // "button_delete"

      ],
      parent: `#${curr_kalk.kalk_sifra}_ugovor_or_tender_box`,
      return: {},
      show_on_click: false,
      cit_run: function(state) { console.log(state); },

      button_delete: async function(e) {

        e.stopPropagation();
        e.preventDefault();

        var this_button = this;

        console.log("kliknuo DELETE za UGOVOR ILI TENDER UNUTAR KALKULACIJE ");

        var super_editori = get_super_editors();
    
        // ako nije NITI JEDAN OD EDITORA onda ne može brisati ugovor ili tender za fiznu cijenu
        if ( super_editori.indexOf( window.cit_user.user_number ) == -1 ) {
          popup_warn(`Nemate ovlasti za brisanje fiksne cijene!!!`);
          return;
        };
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          
          curr_kalk.kalk_fixed_price = null;

          $(`#`+parent_row_id).remove();
          
          $(`#`+curr_kalk.kalk_sifra + `_kalk_fixed_price`).addClass(`off`).removeClass(`on`);
          
        };


        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj Tender?`, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj Tender?`, null, 'yes/no', delete_yes, delete_no, null);


      },

    };
    if ( curr_kalk.kalk_fixed_price?.length > 0 ) create_cit_result_list( curr_kalk.kalk_fixed_price, ugovor_or_tender_props );
  };
  this_module.make_ugovor_or_tender_list = make_ugovor_or_tender_list;
  
  
  function make_mark_disc_contract_list(data, curr_kalk) {

    var mark_disc_contract_props = {

      desc: 'samo za kreiranje tablice JEDNOG ugovora ili tendera ZA FIXNU MARŽU ILI POPUST u offer kalkulaciji' + data.rand,
      local: true,

      list: curr_kalk.mark_disc_contract_list || [],

      show_cols: [ 
        "naziv",
        "date",
        "rok",
        "status_naziv",
        "start",
        "end",
        "komentar", 
        "doc_links",
        "button_delete",
      ],
      format_cols: { date: "date", rok: "date", start: "date", end: "date" },

      col_widths: [
          2, // "naziv",
          1, // "date",
          1, // "rok",
          2, // "status_naziv",
          1, // "start",
          1, // "end",
          3, // "komentar", 
          4, // "doc_links"
          1, // "button_delete"

      ],
      parent: `#${curr_kalk.kalk_sifra}_mark_disc_contract_box`,
      return: {},
      show_on_click: false,
      cit_run: function(state) { console.log(state); },

      button_delete: async function(e) {

        e.stopPropagation();
        e.preventDefault();

        var this_button = this;

        console.log("kliknuo DELETE za UGOVOR ILI TENDER ZA MARŽU ILI POPUST ");

        
        var super_editori = get_super_editors();
        
        // ako nije NITI JEDAN OD EDITORA onda ne može brisati ugovor ili tender za fiznu cijenu
        if ( super_editori.indexOf( window.cit_user.user_number ) == -1 ) {
          popup_warn(`Nemate ovlasti za brisanje fiksne marže ili popusta!!!`);
          return;
        };
        

        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          
          curr_kalk.mark_disc_contract_list = null;
          
          $(`#`+parent_row_id).remove();
          
          // $(`#`+curr_kalk.kalk_sifra + `_kalk_fixed_price`).addClass(`off`).removeClass(`on`);
          
        };

        function delete_no() {
          show_popup_modal( false, `Jeste li sigurni da želite obrisati ovaj ugovor za popust/maržu?`, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj ugovor za popust/maržu?`, null, 'yes/no', delete_yes, delete_no, null);


      },

    };
    
    if ( curr_kalk.mark_disc_contract_list?.length > 0 ) create_cit_result_list( curr_kalk.mark_disc_contract_list, mark_disc_contract_props );
    
  };
  this_module.make_mark_disc_contract_list = make_mark_disc_contract_list;
  
  
  async function create_sample_product( this_sample_switch ) {

    var this_comp_id = $(this_sample_switch).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    var product_id = $(this_sample_switch).closest('.product_comp')[0].id;
    var product_data =  this_module.product_module.cit_data[product_id];

    var project_comp_id =  $(this_sample_switch).closest('.cit_comp.project_comp')[0].id; 
    var project_data = this_module.project_module.cit_data[project_comp_id];

    var kalk_box = $(this_sample_switch).closest('.kalk_box');

    var kalk_type = kalk_box.attr(`data-kalk_type`);
    var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);

    var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );

    console.log(product_data);

    var sample_product = cit_deep(product_data);

    // napravi novi component id
    sample_product.id = `prod`+cit_rand();

    sample_product.variant = null;
    sample_product.sifra = null;
    sample_product.project_id = project_data._id || null; // ako već ima onda spremi njega
    sample_product.proj_sifra = project_data.proj_sifra || null; // ako već ima onda spremi njega
    sample_product.item_sifra = null;

    var product_order_num = sample_product.order_num;
    var sample_order_num = 0;
    var last_product_or_sample_id = product_data.id;


    $.each( project_data.items, function( item_ind, item ) {
      if ( 
        item.sample_for_id == product_data._id // loop po productima koji imaju _id od glavnog producta ---> tako znam da su uzorci od istog proizvoda
        &&
        item.order_num > sample_order_num // ako je order_num veći od trenutnog sample order numbera
      ) {
        sample_order_num = item.order_num;
        last_product_or_sample_id = item.id;
      };

    });

    // order num uzorka se povećavaju za 1 dok se order num za glavne proizvode povećavaju za 100;
    // ako je ovo prvi uzorak onda će sample order biti nula pa zato uzimam order num od producta 
    // i onda će na primjer 1. uzorak od 1. proizvoda imati redni broj 101
    sample_product.order_num = ( sample_order_num || product_order_num ) + 1; 

    // trebam napraviti temp div koji mi služi da znam gdje postaviti novu verziju producta !!!
    var div_after_product = `<div id="sample_product_after"></div>`;
    $( `#` + last_product_or_sample_id ).after( div_after_product );

    var new_product_data = {

      ...product_data,
      ...sample_product, // ovo će prepisati sve iste propertije od product data
      mont_ploce: [],
      statuses: [], // resetiraj sve statuse
      sample_for_id: product_data._id,
      sample_for_kalk: curr_kalk.kalk_sifra,
      sample_sifra: curr_kalk.sample_sifra,
      is_copy: true,
      
      id: `prod`+cit_rand(), // napravi novi component id
      
    };
    

    // VAŽNO _id PROPERTY NE SMJE POSTOJATI  - čak i kad ima value null onda će se upisati kao null u bazu !!!!
    // VAŽNO _id PROPERTY NE SMJE POSTOJATI  - čak i kad ima value null onda će se upisati kao null u bazu !!!!
    // VAŽNO _id PROPERTY NE SMJE POSTOJATI  - čak i kad ima value null onda će se upisati kao null u bazu !!!!
    if ( new_product_data._id || new_product_data._id == null || typeof new_product_data._id == `undefined` ) delete new_product_data._id;

    // POSVE OBRIŠI SVE KALKULACIJE JER JE OVO UZORAK !!!
    new_product_data.kalk = null;

    this_module.product_module.create( new_product_data, $(`#sample_product_after`), `before`);
    // remove ovaj div kojeg sam napravio ispod HTML od producta
    setTimeout( function() { $(`#sample_product_after`).remove();  }, 200 );

    console.log(sample_product);

  };
  this_module.create_sample_product = create_sample_product;

  
  async function offer_reserv_record( switch_elem ) {

    var this_comp_id = $(switch_elem).closest('.cit_comp.kalk_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    var kalk_box = $(switch_elem).closest('.kalk_box');
    var kalk_type = kalk_box.attr(`data-kalk_type`);
    var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
    
    var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
    
    var product_div = $(switch_elem).closest('.product_comp');
    var product_id = product_div[0].id;
    var product_data =  this_module.product_module.cit_data[product_id];
    
    var project_id = $(switch_elem).closest('.project_comp')[0].id;
    var project_data =  this_module.project_module.cit_data[project_id];
    

    var status_module = await get_cit_module(`/modules/status/status_module.js`, `load_css`);
    var status_comp_id = product_div.find(`.status_comp`)[0].id;
    var status_data = status_module.cit_data[status_comp_id];

    
    var parent_record = null;
    var status_offer_reserv = null;
    var reply_for = null;
    var elem_parent_object = null;

    var all_sirov_records = [];
    var arg_storno_data = null;

    var link = ``;
    var komentar = ``;

    var status_tip = null;
    
    var db_sirovine = get_all_kalk_ulazi_sirovs(curr_kalk);


    // ------------ START ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )
    // ------------ START ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )
    // ------------ START ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )
    


    $.each( db_sirovine, function( s_ind, sirov ) {
 

      var new_record = {
        
        type: `offer_reserv`,
        

        sirov_id: sirov._id,

        count: sirov.sum_kolicina,

        record_parent: null,

        time: Date.now(),

        sifra: `record`+cit_rand(),

        doc_sifra: null,
        pdf_name: null,
        doc_link: null,


        record_statuses: null,

        record_kalk: {
          _id: data._id || null,
          proj_sifra: data.proj_sifra,
          item_sifra: data.item_sifra,
          variant: data.variant,
          kalk_sifra: curr_kalk.kalk_sifra, // samo uzmi sifru
          full_product_name: product_data.full_product_name,

        },

        record_est_time: product_data?.rok_za_def || null, // procjena vremena kada će doći do promjene

        storno_time: null,
        storno_kolicina: null,

        pk_kolicina: sirov.sum_kolicina,
        nk_kolicina: null,
        rn_kolicina: null,
        order_kolicina: null,
        sklad_kolicina: null,

        partner_name: project_data.kupac.naziv,
        partner_adresa: project_data.doc_adresa,
        partner_id: project_data.kupac._id,

      };

      all_sirov_records.push(new_record);

    });
    // kraj loopa po svim sirovinama u kalkulaciji

    // ------------ END ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )
    // ------------ END ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )
    // ------------ END ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )

    var records_result = await this_module.save_sirov_records(all_sirov_records);
    
    
    setTimeout( function() {  $(`#` + data.id + ` .save_offer_kalk_btn` ).trigger(`click`); }, 200);
    // setTimeout( function() {  $(`#` + data.id + ` .save_pro_kalk_btn` ).trigger(`click`); }, 300);
    // setTimeout( function() {  $(`#` + data.id + ` .save_post_kalk_btn` ).trigger(`click`); }, 400);

    
    
    return records_result;

  };
  this_module.offer_reserv_record = offer_reserv_record;
  
  
  async function order_reserv_record( switch_elem ) {

    var this_comp_id = $(switch_elem).closest('.cit_comp.kalk_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    var kalk_box = $(switch_elem).closest('.kalk_box');
    var kalk_type = kalk_box.attr(`data-kalk_type`);
    var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
    
    var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
    
    var product_div = $(switch_elem).closest('.product_comp');
    var product_id = product_div[0].id;
    var product_data =  this_module.product_module.cit_data[product_id];
    
    var project_id = $(switch_elem).closest('.project_comp')[0].id;
    var project_data =  this_module.project_module.cit_data[project_id];

    var status_module = await get_cit_module(`/modules/status/status_module.js`, `load_css`);
    var status_comp_id = product_div.find(`.status_comp`)[0].id;
    var status_data = status_module.cit_data[status_comp_id];
    
    var parent_record = null;
    var status_offer_reserv = null;
    var reply_for = null;
    var elem_parent_object = null;

    var all_sirov_records = [];
    var arg_storno_data = null;

    var link = ``;
    var komentar = ``;

    var status_tip = null;
    
    var db_sirovine = get_all_kalk_ulazi_sirovs(curr_kalk);


    // ------------ START ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )
    // ------------ START ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )
    // ------------ START ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )

    $.each( db_sirovine, function( s_ind, sirov ) {

      
      var new_record = {
        
        type: `order_reserv`,

        sirov_id: sirov._id,

        count: sirov.sum_kolicina,

        record_parent: null,

        time: Date.now(),

        sifra: `record`+cit_rand(),

        doc_sifra: null,
        pdf_name: null,
        doc_link: null,


        record_statuses: null,

        record_kalk: {
          _id: data._id || null,
          proj_sifra: data.proj_sifra,
          item_sifra: data.item_sifra,
          variant: data.variant,
          kalk_sifra: curr_kalk.kalk_sifra, // samo uzmi sifru ------> to je sifra od offer kalka !!!!!!!!!!!!!!
          full_product_name: product_data.full_product_name,

        },

        record_est_time: product_data?.rok_za_def || null, // procjena vremena kada će doći do promjene

        storno_time: null,
        storno_kolicina: null,

        pk_kolicina: null,
        nk_kolicina: sirov.sum_kolicina,
        rn_kolicina: null,
        order_kolicina: null,
        sklad_kolicina: null,

        partner_name: project_data.kupac.naziv,
        partner_adresa: project_data.doc_adresa,
        partner_id: project_data.kupac._id,

      };

      all_sirov_records.push(new_record);

    });
    // kraj loopa po svim sirovinama u kalkulaciji

    // ------------ END ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )
    // ------------ END ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )
    // ------------ END ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )

    var records_result = await this_module.save_sirov_records(all_sirov_records);

    setTimeout( function() {  $(`#` + data.id + ` .save_offer_kalk_btn` ).trigger(`click`); }, 200);
    
    return records_result;
    
    

  };
  this_module.order_reserv_record = order_reserv_record;
  
  
  async function rn_reserv_record( switch_elem, arg_proces ) {

    var this_comp_id = $(switch_elem).closest('.cit_comp.kalk_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    var kalk_box = $(switch_elem).closest('.kalk_box');
    var kalk_type = kalk_box.attr(`data-kalk_type`);
    var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);
    
    
    var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );
    
    var product_div = $(switch_elem).closest('.product_comp');
    var product_id = product_div[0].id;
    var product_data =  this_module.product_module.cit_data[product_id];
    
    var project_id = $(switch_elem).closest('.project_comp')[0].id;
    var project_data =  this_module.project_module.cit_data[project_id];
    

    var status_module = await get_cit_module(`/modules/status/status_module.js`, `load_css`);
    var status_comp_id = product_div.find(`.status_comp`)[0].id;
    var status_data = status_module.cit_data[status_comp_id];
    
    
    var parent_record = null;
    var status_offer_reserv = null;
    var reply_for = null;
    var elem_parent_object = null;

    var all_sirov_records = [];
    var arg_storno_data = null;

    var link = ``;
    var komentar = ``;

    var status_tip = null;
    
    var db_sirovine = get_all_kalk_ulazi_sirovs(curr_kalk, arg_proces);

    var for_pro_kalk_sifra = null;
    // svaki record ima u sebi sifru od korjenskog offer kalka -----> dakle record ima offer kalk sifru A NE ORDER KALK SIFRU ( to je zbog kontinuiteta )
    $.each(data.offer_kalk, function(o_ind, offer) {
      if ( offer.kalk_for_pro == true ) {
        for_pro_kalk_sifra = offer.kalk_sifra;
      };
    });
    
    
    // ------------ START ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )
    // ------------ START ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )
    // ------------ START ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )

    $.each( db_sirovine, function( s_ind, sirov ) {
      
   
      var new_record = {
        
        type: `radni_nalog`,
        
        
        sirov_id: sirov._id,

        count: sirov.sum_kolicina,

        record_parent: null,
        time: Date.now(),
        sifra: `record`+cit_rand(),
        doc_sifra: null,
        pdf_name: null,
        doc_link: null,

        
        record_statuses: null,

        record_kalk: {
          _id: data._id || null,
          kalk_sifra: for_pro_kalk_sifra, // ovo je sifra kalk koji je izabran za prizvodnju a to je zapravo sifra jednog od offer kalka !!!
          proj_sifra: data.proj_sifra,
          item_sifra: data.item_sifra,
          variant: data.variant,
          full_product_name: product_data.full_product_name,

        },

        record_est_time: product_data?.rok_za_def || null, // procjena vremena kada će doći do promjene

        storno_time: null,
        storno_kolicina: null,

        pk_kolicina: null,
        nk_kolicina: null,
        rn_kolicina: sirov.sum_kolicina,
        order_kolicina: null,
        sklad_kolicina: null,

        partner_name: project_data.kupac.naziv,
        partner_adresa: project_data.doc_adresa,
        partner_id: project_data.kupac._id,

      };

      all_sirov_records.push(new_record);

    });
    // kraj loopa po svim sirovinama u kalkulaciji

    // ------------ END ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )
    // ------------ END ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )
    // ------------ END ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )

    var records_result = await this_module.save_sirov_records(all_sirov_records);
    
    setTimeout( function() {  $(`#` + data.id + ` .save_pro_kalk_btn` ).trigger(`click`); }, 200);
    

    return records_result;

  };
  this_module.rn_reserv_record = rn_reserv_record;
  
  
  
  
  async function save_RN_records(this_module, data_for_doc) {
    
    
    var switch_elem = $(`#` + data_for_doc.pro_kalk.kalk_sifra + `_kalk_radni_nalog` )[0];
    
    var this_comp_id = $(switch_elem).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    var product_id = $(switch_elem).closest('.product_comp')[0].id;
    var product_data =  this_module.product_module.cit_data[product_id];

    var full_product_name = this_module.product_module.generate_full_product_name(product_data);
    
    var kalk_box = $(switch_elem).closest('.kalk_box');
    var kalk_type = kalk_box.attr(`data-kalk_type`);
    var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);

    var curr_kalk = this_module.kalk_search( data, kalk_type, kalk_sifra, null, null, null, null );

    
  
    // ------------------------------ RADNI NALOG TRUE ------------------------------
    curr_kalk.kalk_radni_nalog = true;

    $.each( curr_kalk.procesi, function(p_ind, proces) {  
      curr_kalk.procesi[p_ind].rn_created = true;
    });
    

    $(`.proces_radni_nalog`).remove(); // skloni sve prekidače za dodatne radne naloge

    // uzmi kalk šifru od offer kalk koji us sebi ima kalk_for_pro

    var for_pro_kalk_sifra = null;

    // tražim šifru kalkulacije koja je označena za proizvodnju 
    $.each(data.offer_kalk, function(o_ind, offer) {
      if ( offer.kalk_for_pro == true ) {
        for_pro_kalk_sifra = offer.kalk_sifra;
      };
    });

    // storniranje RESERVACIJA PO NK !!!
    var for_pro_switch = $(`#` + for_pro_kalk_sifra + `_kalk_for_pro`)[0];

    var storno_result = await this_module.storno_reserv( for_pro_kalk_sifra, for_pro_switch, null, `order_reserv` );

    
    // ovo je RN za sve procese ODJEDNOM
    var record_result = await this_module.rn_reserv_record( switch_elem, null );


    var post_kalk = this_module.make_kalks( switch_elem, data, data.post_kalk, `post_kalk`, data.tip || null );
    // $(`#${data.id} .post_kalk_container`).html(post_kalk);

    setTimeout( function() { $(`#${data.id} .save_pro_kalk_btn`).trigger(`click`); }, 400 );
    setTimeout( function() { $(`#${data.id} .save_post_kalk_btn`).trigger(`click`); }, 500 );

    
    
  };
  this_module.save_RN_records = save_RN_records;
  
  
  async function make_radni_nalog( switch_elem, arg_proces ) {
    

    var this_comp_id = $(switch_elem).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    var product_id = $(switch_elem).closest('.product_comp')[0].id;
    var product_data =  this_module.product_module.cit_data[product_id];

    var full_product_name = this_module.product_module.generate_full_product_name(product_data);
    
    
    var rok_isporuke_text = ``;

    if ( product_data.rok_isporuke ) {
      rok_isporuke_text = ` (isporuka: ${ cit_dt( product_data.rok_isporuke ).date })`;
    }; 
    

    var project_id = $(switch_elem).closest('.project_comp')[0].id;
    var project_data =  this_module.project_module.cit_data[project_id];

    
    var curr_kalk = null;
    
    $.each( data.offer_kalk, function(k_ind, kalk) {
      if ( kalk.kalk_for_pro ) curr_kalk = kalk;
    });
    

    // refresh_simple_cal( $('#'+data.id+`_doc_rok_time`) );

    
    if ( !project_data.kupac ) {
      popup_warn(`Potrebno izabrati kupca u projektu!`);
      toggle_cit_switch(false, $(switch_elem) );
      return;
    };
    

    /*
    if ( !data.doc_rok_time ) {
      popup_warn(`Potrebno izabrati rok dokumenta!`);
      toggle_cit_switch(false, $(switch_elem) );
      return;
    };
    */
    
    /*    
    if ( !data.doc_valuta ) {
      popup_warn(`Izaberite valutu dokumenta!`);
      return;
    };
    */

    
    var preview_mod = await get_cit_module('/modules/previews/cit_previews.js', `load_css`);

    console.log(`PODACI OD KUPCA ----------------- !!!!! `);

    var db_partner = null;
    var kupac_id = project_data.kupac?._id ? project_data.kupac._id : null;

    if( !kupac_id ) {
      popup_warn(`Nedostaje info o kupcu unutar projekta !!!`);
      toggle_cit_switch(false, $(switch_elem) );
      return;
    };


    if ( kupac_id ) {
      db_partner = await this_module.get_partner( kupac_id );
    };

    if ( !db_partner ) {
      popup_error(`Greška prilikom pretrage kupca u bazi !!!!`);
      toggle_cit_switch(false, $(switch_elem) );
      return;
    };
    

    
    var product_info = null;
  
    var price_start = curr_kalk.kalk_final_unit_price;
    var price_final = null;

    // postoji popust u kunama onda izračunaj popust u %
    if ( curr_kalk.kalk_discount_money ) {
      curr_kalk.kalk_discount = 100 * curr_kalk.kalk_discount_money / (price_start * curr_kalk.kalk_naklada);
    };
    // ako mi fali popust u kn onda ga postavi na nula
    if ( !curr_kalk.kalk_discount_money ) curr_kalk.kalk_discount_money = 0;
    
    // ako marža u kunama onda izračunaj maržu u %
    if ( curr_kalk.kalk_markup_money ) {
      curr_kalk.kalk_markup = 100 * curr_kalk.kalk_markup_money / (price_start * curr_kalk.kalk_naklada);
    };
    // ako mi fali marža u parama on je postavi na nula
    if ( !curr_kalk.kalk_markup_money ) curr_kalk.kalk_markup_money = 0;

    // startna cijena je sada s popustom
    price_start = curr_kalk.kalk_final_unit_price * (100 + (curr_kalk.kalk_markup || 0) )/100;
    price_final = price_start * (100 - (curr_kalk.kalk_discount || 0) )/100;


    product_info = {

      product_id: data.product_id,

      proj_sifra : data.proj_sifra || null,
      item_sifra : data.item_sifra || null,
      variant : data.variant || null,

      naziv: full_product_name + rok_isporuke_text, 
      jedinica: "kom",

      unit_price: price_start,
      discount: curr_kalk.kalk_discount,
      dis_unit_price: price_final,

      count: curr_kalk.kalk_naklada,
      dis_price: price_final * curr_kalk.kalk_naklada,
      decimals: 2,
      rok_isporuke: product_data?.rok_isporuke || null,

      dostav_adresa: product_data.dostav_adresa.full_adresa || null,

    };

    


    
    // ----------------------------------- NAPRAVI POST KALKS -----------------------------------
    // ----------------------------------- NAPRAVI POST KALKS -----------------------------------
    // ----------------------------------- NAPRAVI POST KALKS -----------------------------------

    var kalk_copy = cit_deep(curr_kalk);


    // obavezno postavi novu kalk sifru !!!
    kalk_copy.kalk_sifra = `kalk`+cit_rand();
    if ( kalk_copy.kalk_rand ) delete kalk_copy.kalk_rand;


    $.each( kalk_copy.procesi, function(p_ind, proces) {  

      $.each( proces.ulazi, function(u_ind, ulaz) {
        kalk_copy.procesi[p_ind].ulazi[u_ind].kalk_sum_sirovina = 0;
      });

      $.each( proces.izlazi, function(i_ind, izlaz) {
        kalk_copy.procesi[p_ind].izlazi[i_ind].kalk_sum_sirovina = 0;
      });

      kalk_copy.procesi[p_ind].action.action_prep_time = 0;
      kalk_copy.procesi[p_ind].action.action_work_time = 0;
      
      
      if ( proces.extra_data ) {
        
        kalk_copy.procesi[p_ind].extra_data.action_prep_time = 0;
        kalk_copy.procesi[p_ind].extra_data.action_work_time = 0;
        
        kalk_copy.procesi[p_ind].extra_data.radnik_count = 0;
        kalk_copy.procesi[p_ind].extra_data.radnik_count_prep = 0;
        
        
      };

    }); // kraj loopa po procesima 
    

    data.post_kalk = [ kalk_copy ];
    
    // ----------------------------------- NAPRAVI POST KALKS -----------------------------------
    // -------------KRAJ------------------ NAPRAVI POST KALKS -----------------------------------
    // ----------------------------------- NAPRAVI POST KALKS -----------------------------------
    
    
    
    var selected_kalk_PRO = cit_deep( data.pro_kalk[0] );
    
    selected_kalk_PRO.full_record_name = selected_kalk_PRO.kalk_naklada + " kom -- " + product_data.full_product_name;
    selected_kalk_PRO.proj_sifra = product_data.proj_sifra;
    selected_kalk_PRO.item_sifra = product_data.item_sifra;
    selected_kalk_PRO.variant = product_data.variant;
    selected_kalk_PRO.product_id = product_data._id;
    
    var selected_kalk_POST = cit_deep( data.post_kalk[0] );
    
    selected_kalk_POST.full_record_name = selected_kalk_POST.kalk_naklada + " kom -- " + product_data.full_product_name;
    selected_kalk_POST.proj_sifra = product_data.proj_sifra;
    selected_kalk_POST.item_sifra = product_data.item_sifra;
    selected_kalk_POST.variant = product_data.variant;
    selected_kalk_POST.product_id = product_data._id;
    
    
    var sirov_ulazi = [];

    
    $.each( selected_kalk_PRO.procesi, function( p_ind, proces ) {

      $.each( proces.ulazi, function( u_ind, ulaz ) {

        if ( ulaz._id ) {

          var sirov_obj = {
            
            _id: ulaz._id,
            kalk_full_naziv: ulaz.kalk_full_naziv,
            kalk_kom_in_sirovina: ulaz.kalk_kom_in_sirovina,
            kalk_sum_sirovina: ulaz.kalk_sum_sirovina,
            kalk_extra_kom: ulaz.kalk_extra_kom,
            kalk_skart: null,
            
            vrsta_materijala: ulaz.vrsta_materijala || null,
            klasa_sirovine: ulaz.klasa_sirovine || null,
            tip_sirovine: ulaz.tip_sirovine || null,
            
            kvaliteta_1: ulaz.kvaliteta_1 || null,
            kvaliteta_2: ulaz.kvaliteta_2 || null,
            debljina_mm: ulaz.debljina_mm || null,
            gramaza_g: ulaz.gramaza_g || null,
            
            
          };
          
          sirov_ulazi.push(sirov_obj);

        }; // kraj ako ima _id

      });

    });
    
    
    var rn_items = [];

    $.each( selected_kalk_PRO.procesi, function(p_ind, proces) {
      
      

      var ulazi_text = ``;
      
      $.each(proces.ulazi, function(u_ind, ulaz) {
        
        var this_ulaz_text = ``;
        
        var kom_in_sirov = ulaz.kalk_kom_in_sirovina ? (` X ` + ulaz.kalk_kom_in_sirovina) : "";
        
        
        // ------------------------------------------------ OBIČNA SIROVINA ------------------------------------------------
        if ( ulaz._id && ulaz.kalk_full_naziv ) {
          this_ulaz_text =  ulaz.kalk_full_naziv + `: ` + ulaz.kalk_sum_sirovina + kom_in_sirov + " " + (ulaz.kalk_unit || "") + `<br><br>`;
        };
        
        // ------------------------------------------------ MONTAŽNA FORMA ------------------------------------------------
        if ( !ulaz.kalk_full_naziv && ulaz.kalk_plate_naziv ) {
          this_ulaz_text =  ("FORMA " + ulaz.kalk_plate_naziv) + `: ` + ulaz.kalk_sum_sirovina + kom_in_sirov + " " + (ulaz.kalk_unit || "") + `<br><br>`;
        };
        
        // ------------------------------------------------ ELEMENT ------------------------------------------------
        if ( ulaz.kalk_element && ulaz.kalk_element.elem_opis ) {
          this_ulaz_text =  ulaz.kalk_element.elem_opis + `: ` + ulaz.kalk_sum_sirovina + ` ` + (ulaz.kalk_unit || "") + `<br><br>`;
        };
        
        ulazi_text += this_ulaz_text;
        
      });
      

      var izlazi_text = ``;
      $.each(proces.izlazi, function(u_ind, izlaz) {

        if ( izlaz._id && izlaz.kalk_full_naziv ) {
          izlazi_text +=  izlaz.kalk_full_naziv + `: ` + izlaz.kalk_sum_sirovina + " kom" + `<br><br>`; 
        };
        
        if ( izlaz.kalk_element && izlaz.kalk_element.elem_opis ) {
          izlazi_text +=  izlaz.kalk_element.elem_opis + `: ` + proces.extra_data?.izlaz_kom?.kom || izlaz.kalk_sum_sirovina + " kom" + `<br><br>`; 
        };

      });


      rn_items.push({
        
        ulazi: ulazi_text,
        proces: `PROCES BR. ${p_ind+1} : ` + proces.action.action_radna_stn.naziv,
        izlazi: izlazi_text,
      });
  
    });
    
    
    var data_for_doc = {
      
      doc_type: `radni_nalog`,

      sifra: "dfd"+cit_rand(),
      for_module: `kalk`,
      time: Date.now(),

      proj_sifra : product_data.proj_sifra || null,
      item_sifra : product_data.item_sifra || null,
      variant : product_data.variant || null,
      status: null,


      sirov_id: null,
      full_record_name: product_data.full_product_name,


      doc_valuta: ( data.doc_valuta?.valuta || "HRK" ), // default je HRK
      doc_valuta_manual: (data.doc_valuta_manual || null ), // manual tečaj
      referent: window.cit_user.full_name,
      place: `Velika Gorica`,

      
      doc_num: `---`,
      
      oib: (db_partner.oib || db_partner.vat_num || ""),
      partner_name: db_partner.naziv,
      partner_adresa: "", // adresa NIJE BITNA ZA RADNI NALOG !!!!!!
      partner_id: db_partner._id,


      /*
      partner_place: `67122 ALTRIP`,
      partner_country: `Germany`,
      */

      rok: data.doc_rok_time || product_data?.rok_za_def || null,
      dobav_ponuda_sifra: ``,

      rn_items: rn_items,
      
      items: [product_info],
      
      doc_sirovs: [
        {
          
          out_roba: true,
            
          _id: "", // još uvijek nemam id od sirovine jer još uvijek ovaj RN nije izlazna roba ---> nije još proizvedeno i nije upisano kao roba u RESURSIMA
          sirovina_sifra: "", // još uvijek nemam niti sifru sirovine iz istog razloga :)
          naziv: product_info.naziv,

          product_id: product_info.product_id,
          proj_sifra : product_info.proj_sifra || null,
          item_sifra : product_info.item_sifra || null,
          variant : product_info.variant || null,

          doc_kolicine: [
            {
              
              sifra: `dockol` + cit_rand(),

              count: product_info.count,
              price: product_info.dis_price,

              next_price: null,
              next_count: null,

              next_approved: false,
              next_doc: false,
              
            }
          ],
          
        }
        
      ],
      
      sirov_ulazi: sirov_ulazi,

      kalk: selected_kalk_POST, 
      pro_kalk: selected_kalk_PRO,
      
      kalk_id: data._id || null,
      proces: arg_proces || null,
      
      
      naklada: data.post_kalk[0].kalk_naklada,
      prirez_val: data.post_kalk[0].prirez_val || "",
      prirez_kontra: data.post_kalk[0].prirez_kontra || "",
      polov: data.post_kalk[0].kalk_polov || "",
      broj_klapni: data.post_kalk[0].broj_klapni || "",
      
      switch_elem: switch_elem,
      
      orient: `landscape`,
      
      doc_komentar: selected_kalk_PRO.kalk_komentar || null,
      
      
    };

    // return;

    preview_mod.generate_cit_doc( data_for_doc, data.doc_lang?.sifra || "hr" ); // jezik radnog naloga je uvijek HRVATSKI (za sada :) 

  };
  this_module.make_radni_nalog = make_radni_nalog;
  

  async function make_radni_nalog_statuses( data_for_doc, arg_pdf ) {


    var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
    var status_module = await get_cit_module(`/modules/status/status_module.js`, `load_css`);

    var product_id = $(`#`+ data_for_doc.pro_kalk.kalk_sifra + `_kalkulacija` ).closest('.product_comp')[0].id;
    var product_data =  product_module.cit_data[product_id];

    var this_comp_id = $(`#`+ data_for_doc.pro_kalk.kalk_sifra + `_kalk_radni_nalog` ).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    var product_id = $(data_for_doc.switch_elem).closest('.product_comp')[0].id;
    var product_data = product_module.cit_data[product_id];

    var full_product_name = product_module.generate_full_product_name(product_data);

    var kalk_type = `post_kalk`;

    // pošto je ovo jedan jedini kalk jer u post kalk nema više kalkova :)
    var post_kalk = data_for_doc.kalk;


    // prvo nadjem parent product component pa onda pomoću njega nadjem status component
    var status_comp_id = $(data_for_doc.switch_elem).closest('.product_comp').find(`.status_comp`)[0].id;
    var status_data = status_module.cit_data[status_comp_id];


    var sirov_ids = []; 
    var db_sirovine = [];

    var parent_record = null;
    var status_offer_reserv = null;
    var reply_for = null;
    var elem_parent_object = null;

    var all_sirov_records = [];
    var arg_storno_data = null;

    var link = ``;
    var link_in_koment = ``;
    var komentar = ``;

    var status_tip = null;


    var time_now = Date.now();


    if ( !window.cit_user ) {
      popup_warn(`Niste se ulogirali !!!`);
      return;
    };


    var all_prodon_statusi = [];

    // -------------- START --------------------------------------- START LOOPA PO SVIM POST KALK PROCESIMA  
    // -------------- START --------------------------------------- START LOOPA PO SVIM POST KALK PROCESIMA  
    // -------------- START --------------------------------------- START LOOPA PO SVIM POST KALK PROCESIMA  


    function check_is_for_one_proces( this_proces ) {
      var is_ok = false;
      if ( !data_for_doc.proces ) { is_ok = true; }
      else {
        if ( this_proces.row_sifra == data_for_doc.proces.row_sifra ) is_ok = true;
      };
      return is_ok;  
    };


    $.each( post_kalk.procesi, function( p_ind, proces ) {

      // ------------------- PROVJERA JEL ZA SVE PROCESE ILI SAMO ZA JEDAN NAKNADNI !!! -------------------
      // ------------------- PROVJERA JEL ZA SVE PROCESE ILI SAMO ZA JEDAN NAKNADNI !!! -------------------
      // ------------------- PROVJERA JEL ZA SVE PROCESE ILI SAMO ZA JEDAN NAKNADNI !!! -------------------

      if (  check_is_for_one_proces(proces)  ) {

        var rn_extra = null;

        if ( proces.extra_data?.proces_radni_nalog ) {

          rn_extra = {
            time: Date.now(),
            _id: window.cit_user._id,
            user_number: window.cit_user.user_number,
            full_name: window.cit_user.full_name,
            email: window.cit_user.email
          };

        };

        var new_status_sifra = `status`+cit_rand();

        var link_to_post_kalk_proces = 
            `#project/${data.proj_sifra || null}/item/${data.item_sifra || null}/variant/${data.variant || null}/kalk/${post_kalk.kalk_sifra || null}/proces/${proces.row_sifra}`;


        komentar = 
  `RADNI PROCES BR. ${p_ind+1}: 
  <br>
  <a href="${link_to_post_kalk_proces}" 
      target="_blank"
      onClick="event.stopPropagation();"> 
      ${ full_product_name + "<br><br>" + proces.action.action_radna_stn.naziv }
  </a>

  `;

        link = `#production/${new_status_sifra}/proces/${proces.row_sifra}`;  

        // naziv" : "PROIZVODNI PROCES - RADNI NALOG",
        status_tip = cit_deep( find_item( window.admin_conf.status_conf, `IS16430378755748118`, `sifra`) ); 

        var status_docs = [
          {
            sifra: `doc`+cit_rand(),
            time: arg_pdf.time,
            link: `<a href="/docs/${time_path(arg_pdf.time)}/${arg_pdf.ime_filea}" target="_blank" onClick="event.stopPropagation();" >${ arg_pdf.ime_filea }</a>`,
          }
        ];
        
        var prodon_status = {

          "full_product_name" : full_product_name,

          "product_id" : data.product_id,
          "link" : link,
          "proj_sifra" : data.proj_sifra,
          "item_sifra" : data.item_sifra,
          "variant" : data.variant,

          "sifra" : new_status_sifra,
          "status" : new_status_sifra,
          "elem_parent" : null, // ako curr status već ima parent onda uzmi njega
          "elem_parent_object" : null,
          "time" : time_now,
          "work_times" : null,
          "docs" : status_docs,
          "est_deadline_hours" : null,

          "est_deadline_date" : (product_data.rok_proiz || data_for_doc.rok || null),

          "start" : time_now,
          "end" : null,
          "old_status_tip" : null,


          "status_tip" : status_tip,


          kalk: data._id || null,

          proces_id: proces.row_sifra,


          "dep_from" : {
              "sifra" : window.cit_user.dep.sifra,
              "naziv" : window.cit_user.dep.naziv,
          },
          "from" : {
              "_id" : window.cit_user._id,
              "name" : window.cit_user.name,
              "user_number" : window.cit_user.user_number,
              "full_name" : window.cit_user.full_name,
              "email" : window.cit_user.email,
          },

          "dep_to" : {
            sifra: "PRO", 
            naziv: "Proizvodnja/Work",
          },
          "to" : {
              "_id" : "61f8fc181039d39764a9eb54",
              "name" : "radnici",
              "user_number" : 301,
              "full_name" : "Radnici Proizvodnja",
              "email" : "production@velprom.hr",
          },
          "old_komentar" : null,
          "komentar" : komentar || null,
          "seen" : null,
          "reply_for" : null,
          "responded" : null,
          "done_for_status" : null,
          "work_finished" : null,
          "sirov" : null,
          "order_sirov_count" : null,
          "record_statuses" : [],

          "rok_isporuke" : product_data.rok_isporuke || null,
          "potrebno_dana_proiz" : product_data.potrebno_dana_proiz || null,
          "potrebno_dana_isporuka" : product_data.rok_isporuke || null,
          "rok_proiz" : product_data.rok_proiz || null,
          "rok_za_def" : product_data?.rok_za_def || null,
          "branch_done" :  false, 

          rn_extra: rn_extra,


        };

        // napravi flat objekte i arraye
        $.each( prodon_status, function( key, value ) {
          // ako je objekt ili array onda ga flataj
          if ( $.isPlainObject(value) || $.isArray(value) ) {
            prodon_status[key+'_flat'] = JSON.stringify(value);
          };
        });    

        all_prodon_statusi.push(prodon_status);

      }; 
      // ---------END---------- PROVJERA JEL ZA SVE PROCESE ILI SAMO ZA JEDAN NAKNADNI !!! -------------------
      
      
      
    }); // loop po svim procesima unutar post kalkulacije


    // ------------------------------------------------------ KRAJ LOOPA PO SVIM POST KALK PROCESIMA
    // ------------------------------------------------------ KRAJ LOOPA PO SVIM POST KALK PROCESIMA
    // ------------------------------------------------------ KRAJ LOOPA PO SVIM POST KALK PROCESIMA



    function save_all_prodon_statuses(all_prodon_statusi, p_index) {

        $.ajax({
          headers: {
            'X-Auth-Token': window.cit_user.token
          },
          type: "POST",
          cache: false,
          url: `/save_status`,
          data: JSON.stringify( all_prodon_statusi[p_index] ),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
        .done( async function (result) {

          console.log(result);

          if ( result.success == true ) {

            cit_toast(`STATUTS ZA RADNI PROCES JE SPREMLJEN!`);
            cit_toast(`Poslan status prema ${ all_prodon_statusi[p_index].to.full_name}`, `background: #232324;`);
            cit_socket.emit('new_status', all_prodon_statusi[p_index] );
            animate_badge( $(`#cit_task_from_me`), 1 );

            all_prodon_statusi[p_index]._id = result.status._id;

            product_data.statuses = upsert_item( product_data.statuses, all_prodon_statusi[p_index], `sifra` );
            status_data.statuses = upsert_item( status_data.statuses, all_prodon_statusi[p_index], `sifra` );


            // --------------------------------

            // na primjer ako array ima 5 itema od 0-4

            if ( p_index < all_prodon_statusi.length - 1 ) {
              // onda p index smije biti 3 ili manji
              save_all_prodon_statuses(all_prodon_statusi, p_index+1);

            } else {

              status_module.make_status_list(status_data);
              status_module.remove_header_arrows_and_blink(status_data);

              /*
              setTimeout( function() {  $(`#` + data.id + ` .save_offer_kalk_btn` ).trigger(`click`); }, 200);
              setTimeout( function() {  $(`#` + data.id + ` .save_pro_kalk_btn` ).trigger(`click`); }, 300);
              setTimeout( function() {  $(`#` + data.id + ` .save_post_kalk_btn` ).trigger(`click`); }, 400);
              */
              
              var kalk_save_button = $(`#` + product_data.id + ` .save_offer_kalk_btn`)[0];

              var kalk_DB_id = await this_module.save_kalk( null, kalk_save_button, `save_all` );
              
              
              setTimeout( function() {  $(`#`+ product_data.id + ` .save_product_btn` ).trigger(`click`); }, 300);

            };


          } else {

            // ako result NIJE SUCCESS = true

            if ( result.msg ) console.error(result.msg);
            if ( !result.msg ) console.error(`Greška prilikom spremanja STATUSA!`);

          };

        })
        .fail(function (error) {
          console.error(`Došlo je do greške na serveru prilikom spremanja STATUSA!`);
          console.log(error);
        })
        .always(function() {
          toggle_global_progress_bar(false);
        });



    };


    if ( all_prodon_statusi.length > 0 ) save_all_prodon_statuses(all_prodon_statusi, 0);


  };
  this_module.make_radni_nalog_statuses = make_radni_nalog_statuses;

  



  function check_is_extra_added( arg_kalk_type, arg_kalk_sifra ) {


    var i;
    // loop unazad i obriši sve druge zapise koji nisu baš ovaj trenutni !!!
    for( i=window.kalk_extra_added.length - 1; i >= 0; i-- ) {
      var extra_obj = window.kalk_extra_added[i];
      // obriši objekt ako nije točno isti kalk type i kalk šifra iz argumenata !!!
      if ( extra_obj.kalk_type !== arg_kalk_type || extra_obj.kalk_sifra !== arg_kalk_sifra ) {
        window.kalk_extra_added.splice(i, 1);
      };
    };

    // UPSERT OVAJ TRENUTNI ZAPIS
    var new_extra_obj =  { kalk_type: arg_kalk_type, kalk_sifra: arg_kalk_sifra };

    var is_inserted = false;
    $.each(window.kalk_extra_added, function(ext_index, extra_obj ) {

      if (
        is_inserted == false 
        &&
        extra_obj.kalk_type == new_extra_obj.kalk_type 
        && 
        extra_obj.kalk_sifra == new_extra_obj.kalk_sifra
        ) {
          // već postoji obaj objecte unutra
          is_inserted = true;
        };

    });

    // ako ne postoji ovaj object u arrayu onda ga ubaci
    if ( is_inserted == false ) window.kalk_extra_added.push(new_extra_obj);


  };
  this_module.check_is_extra_added = check_is_extra_added;


  
  async function get_all_modules() {
    
    this_module.find_sirov = await get_cit_module(`/modules/sirov/find_sirov.js`, null);
    this_module.sirov_module = await get_cit_module(`/modules/sirov/sirov_module.js`, 'load_css');
  };
  this_module.get_all_modules = get_all_modules;

  get_all_modules();
  
  
  
  
  
  
  
  
  
  
  this_module.cit_loaded = true;
 
} // end of module scripts
};
