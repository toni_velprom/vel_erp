

VELPROCES.krajser_work = function krajser_work( this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index ) {


  // { "sifra": "RADS1", type: "PRC9", "naziv": "REZANJE KRAJŠER 1", unit: "m", interval: "h", speed: 700, price: 250 },
  var radna_stn = arg_radna_stn || find_item( window.cit_local_list.radne_stanice, `RADS1`, `sifra`);

  var krajser_prep_price = find_item(window.cit_local_list.pro_const, `PCONS24`, `sifra`).value;
  var prep_time_per_disk = find_item(window.cit_local_list.pro_const, `PCONS24T`, `sifra`).value;
  // { "sifra": "PCONS12", "naziv": "CIJENA SATA / kn", "value": 250 },
  var satnica_radnik = find_item(window.cit_local_list.pro_const, `PCONS12`, `sifra`).value; 

  var krajser_speed = radna_stn.speed; // !!! pazi ovdje je speed po metru ne po komadu !!! pazi ovdje je speed po metru ne po komadu !!!
  var krajser_price = radna_stn.price;

  if ( cit_number(arg_extra_data?.satnica_stroj) !== null ) krajser_price = arg_extra_data.satnica_stroj;
  if ( cit_number(arg_extra_data?.satnica_prep) !== null ) krajser_prep_price = arg_extra_data.satnica_prep;
  if ( cit_number(arg_extra_data?.satnica_radnik) !== null ) satnica_radnik = arg_extra_data.satnica_radnik;
  
  /*
  $(`#` + curr_kalk.kalk_rand + `_` + arg_action.action_sifra + `_satnica_stroj` ).val( cit_format(krajser_price, 2) );
  $(`#` + curr_kalk.kalk_rand + `_` + arg_action.action_sifra + `_satnica_prep` ).val( cit_format(krajser_prep_price, 2) );
  $(`#` + curr_kalk.kalk_rand + `_` + arg_action.action_sifra + `_satnica_radnik` ).val( cit_format(satnica_radnik, 2) );
  */

  var avg_ploca_val = 0;
  var avg_ploca_kontra = 0;

  var sum_ploca_val = 0;
  var sum_ploca_kontra = 0;

  var avg_cut_val = 0;
  var avg_cut_kontra = 0;

  var cut_sum_val = 0;
  var cut_sum_kontra = 0;

  var krajser_val_prep_time = 0;
  var krajser_kontra_prep_time = 0;

  var prirez_val = null;
  var prirez_kontra = null;
  

  if ( !arg_ulazi || arg_ulazi.length == 0 ) {
    popup_warn(`Nedostaju ulazne sirovine ili elementi !!!!`);
    return null;
  };
  

  // prvo pogledaj jel val i kontra val postoji unutar extra data
  // prvo pogledaj jel val i kontra val postoji unutar extra data

  if ( arg_extra_data && arg_extra_data.izlaz_val )  prirez_val = arg_extra_data.izlaz_val;
  if ( arg_extra_data && arg_extra_data.izlaz_kontra )  prirez_kontra = arg_extra_data.izlaz_kontra;

  // ako mi fali i jedan od njih !!!
  if ( !prirez_val || !prirez_kontra ) {
    var prirez_objekt = this_module.get_prirez(product_data, curr_kalk);
    if ( prirez_objekt ) {
      prirez_val = prirez_objekt.val;
      prirez_kontra = prirez_objekt.kontra;
    };
  }; // kraj ako nije nasao prirez podatke u extra data
  
  
  if ( !prirez_val ) {
    popup_warn(`Nedostaju podaci prireza.<br>Upišite prirez po valu i prirez po kontra valu u polja na izlazu krajšera !!!!`);
    warn_popup = true;
    return;
  };
  
  if ( !prirez_kontra ) {
    // popup samo ako već NIJE bio popup za prirez val
    if ( warn_popup == false) popup_warn(`Nedostaju podaci prireza.<br>Upišite prirez po valu i prirez po kontra valu u polja na izlazu krajšera !!!!`);
    return;
  };
  
  // ponovo resetiraj warn popup varijablu za kasnije
  warn_popup = false;

  var krajser_error = false;
  
  var krajser_kom_sum = 0;
  var plate_kom_prosjek = 0;
  var broj_razlicitih_ploca = 0;
  var suma_komada_po_ploci = 0;

  // izracunaj površinu ukupnog ostatka za svaku plocu
  $.each( arg_ulazi, function( check_ulaz_ind, check_ulaz ) {

    if ( krajser_error == false ) {

      if ( !check_ulaz.kalk_po_valu || !check_ulaz.kalk_po_kontravalu ) {
        popup_warn(`Ulazna roba ${ check_ulaz.kalk_full_naziv || "" } nema potrebne dimenzije !!!`);
        krajser_error = true;
        return;
      };

      var real = this_module.kalk_real_plate( 
        check_ulaz.kalk_po_valu,
        check_ulaz.kalk_po_kontravalu,
        prirez_val,
        prirez_kontra,
        check_ulaz,
        curr_kalk 
      );

      var ostaci = null;
      if ( real ) ostaci = this_module.make_ostatak(real, check_ulaz);

      if ( real && real.kom == 0 ) {
        popup_warn(`Ulaz ` + check_ulaz.kalk_full_naziv + ` nema dovoljno velike dimenzije!!!` );
      };

      var ostatak_area = (ostaci?.val?.area || 0) + (ostaci?.kontra?.area || 0);

      arg_ulazi[check_ulaz_ind].uzimam_po_valu = real.real_val;
      arg_ulazi[check_ulaz_ind].uzimam_po_kontravalu = real.real_kontra;

      arg_ulazi[check_ulaz_ind].kalk_ostatak_val = ostaci?.val || null;
      arg_ulazi[check_ulaz_ind].kalk_ostatak_kontra = ostaci?.kontra || null;

      arg_ulazi[check_ulaz_ind].kalk_unit_area = real?.area || (check_ulaz.kalk_po_valu * check_ulaz.kalk_po_kontravalu)/1000000;

      arg_ulazi[check_ulaz_ind].real_unit_price = real.price || check_ulaz.kalk_unit_price || null;
      arg_ulazi[check_ulaz_ind].kalk_price = (real.price || check_ulaz.kalk_unit_price) * sum_plus_extra(check_ulaz);

      // arg_ulazi[check_ulaz_ind].kalk_kom_in_sirovina = real.kom || 1;
      arg_ulazi[check_ulaz_ind].kalk_kom_val = real.kom_val || 1;
      arg_ulazi[check_ulaz_ind].kalk_kom_kontra = real.kom_kontra || 1;
      
      if ( real && real.kom > 0 ) {
        krajser_kom_sum += real.kom * kom_plus_sum_plus_extra(check_ulaz);
        suma_komada_po_ploci += real.kom;
        broj_razlicitih_ploca += 1;
      };

      
    } // ako nije krajser error 

  }); // kraj loopa po svim ulazima


  plate_kom_prosjek = suma_komada_po_ploci / (broj_razlicitih_ploca || 1);

  // F201
  if ( product_data.tip?.sifra == 'TP20' ) {
    krajser_val_prep_time = 6 * prep_time_per_disk;
    krajser_kontra_prep_time = 3.5 * prep_time_per_disk;
  };

  // F200
  if ( product_data.tip?.sifra == 'TP20B' ) {
    krajser_val_prep_time = 4 * prep_time_per_disk;
    krajser_kontra_prep_time = 3.5 * prep_time_per_disk;
  };


  // F203
  if ( product_data.tip?.sifra == 'TP20C' ) {
    krajser_val_prep_time = 6 * prep_time_per_disk;
    krajser_kontra_prep_time = 3.5 * prep_time_per_disk;
  };


  var sum_ulaz_kom = 0;

  // tražim prosjek veličine ploče koliko god bilo ploča na ulazu u proces za krajšer
  // to mi treba da odredim koliko će krajser dugo raditi jer radi 700 metara u sat vremena
  $.each( arg_ulazi, function( u_ind, ulaz ) {

    sum_ploca_val += ulaz.kalk_po_valu * sum_plus_extra(ulaz);
    sum_ploca_kontra += ulaz.kalk_po_kontravalu * sum_plus_extra(ulaz);

    cut_sum_val += ulaz.kalk_kom_val + 2; // dodatni 2 diska na krajevima ploče
    cut_sum_kontra += ulaz.kalk_kom_kontra + 2; // dodatni 2 diska na krajevima ploče

    sum_ulaz_kom += sum_plus_extra(ulaz);

  });


  // ako još uvijek nemam broj noževa
  if ( krajser_val_prep_time == 0 ) krajser_val_prep_time = ( cut_sum_val/arg_ulazi.length ) * prep_time_per_disk;
  if ( krajser_kontra_prep_time == 0 ) krajser_kontra_prep_time = ( cut_sum_kontra/arg_ulazi.length ) * prep_time_per_disk;

  var ukupno_dobiveno_iz_ulaza = gen_dobiveno_ulaz(arg_ulazi);


  var krajser_prep_time_sum = krajser_val_prep_time + krajser_kontra_prep_time;

  var krajser_val_work_time = sum_ploca_val / 1000 / krajser_speed;

  // ovo nije realno jer kada krajser odreže po jednoj strani onda bi po drugoj strani trebalo biti malo kraće 
  // ovo nije realno jer kada krajser odreže po jednoj strani onda bi po drugoj strani trebalo biti malo kraće 
  // ovo nije realno jer kada krajser odreže po jednoj strani onda bi po drugoj strani trebalo biti malo kraće 
  // ALI TO NIJE TOLIKO BITNO !!!!
  // ALI TO NIJE TOLIKO BITNO !!!!

  var krajser_kontra_work_time = sum_ploca_kontra / 1000 / krajser_speed; 

  var prep_time = krajser_prep_time_sum * 60;

  var unit_time = ( krajser_val_work_time + krajser_kontra_work_time )*60/sum_ulaz_kom;
  var time = unit_time * sum_ulaz_kom;

  var unit_price = ( (krajser_val_work_time + krajser_kontra_work_time)/sum_ulaz_kom * krajser_price ) + ( krajser_prep_time_sum * krajser_prep_price/sum_ulaz_kom );
  var price = (krajser_val_work_time + krajser_kontra_work_time)*krajser_price + ( krajser_prep_time_sum * krajser_prep_price );
  
  
  var ulazi = arg_ulazi || this_module.gen_ulazi_from_izlazi(arg_ulazi);


  var arg_changed_props = null;
  var izlazi = arg_izlazi || this_module.gen_izlazi( "just_first", arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, radna_stn, ukupno_dobiveno_iz_ulaza, prirez_val, prirez_kontra );
  
  // VAŽNO !!!!!! -----> prepiši ove propse po proju komada koje dobijem iz real funkcije tj koliko komada mogu dobiti iz jedne ploče
  izlazi[0].kalk_element.elem_plate_count = plate_kom_prosjek || 1;
  izlazi[0].kalk_sum_sirovina = krajser_kom_sum;

  var change_action_props = {
    prices: { stroj: krajser_price || null, prep: krajser_prep_price || null, radnik: satnica_radnik || null }
  };
  
  var action = arg_action || this_module.gen_action( change_action_props, curr_kalk, prep_time, work_time, unit_time, time, unit_price, price, radna_stn, arg_curr_proces_index );


  var proces = {
    extra_data: arg_extra_data || {},
    proc_koment: arg_proc_koment || "",
    row_sifra: `procesrow`+cit_rand(),
    ulazi: ulazi,
    action: action,
    izlazi: izlazi, 
  }; // kraj procesa

  return {
    prep_time,
    work_time,
    
    unit_time,
    time,
    unit_price,
    price,
    proces
  };

};


// broj boja mi fali ---> 
VELPROCES.sloter_work = function sloter_work( this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index )
{

  /*  
  {
   "sifra": "RADS10",
   "naziv": "SLOTER GANDOSSI & FOSSATI GFM 180 - USJECANJE + FOLIOTISAK",
   "unit": "kom",
   "interval": "h",
   "speed": 1500,
   "price": 10,
   "func": "dummy_work",
   "type": "PRC31",
   "proces_type_naziv": "Usjecanje/Foliotisak",
   "elem_sifra": "SEL46",
   "elem_naziv": "Usječeni element s folio tiskom"
  },
  */

  var radna_stn = arg_radna_stn || find_item(window.cit_local_list.radne_stanice, `RADS10`, `sifra`);

  var sloter_print_koef = 1;

  // { "sifra": "PCONS12", "naziv": "CIJENA SATA", "value": 250 },
  var cijena_sata = find_item(window.cit_local_list.pro_const, `PCONS12`, `sifra`).value;

   // { "sifra": "PCONS15", "naziv": "TRAJANJE PRIPREME SLOTERA BEZ TISKA / minute", "value": 0.5 },
  var sloter_prep_time = find_item(window.cit_local_list.pro_const, `PCONS15`, `sifra`).value; 

  // ako je rok veći od 15 dana onda naručujemo sloter !!!
  if ( product_data.rok_isporuke && work_minutes_forward( Date.now(), product_data.rok_isporuke )/(60*24) >= 15 ) {

    // VANJSKI SLOTER BEZ TISKA
    radna_stn = find_item(window.cit_local_list.radne_stanice, `RADS10C`, `sifra`);
  };

  
  if ( cit_number(arg_extra_data?.satnica_stroj) !== null ) krajser_price = arg_extra_data.satnica_stroj;
  if ( cit_number(arg_extra_data?.satnica_prep) !== null ) krajser_prep_price = arg_extra_data.satnica_prep;
  if ( cit_number(arg_extra_data?.satnica_radnik) !== null ) satnica_radnik = arg_extra_data.satnica_radnik;
  
  /*
  $(`#` + curr_kalk.kalk_rand + `_` + arg_action.action_sifra + `_satnica_stroj` ).val( cit_format(krajser_price, 2) );
  $(`#` + curr_kalk.kalk_rand + `_` + arg_action.action_sifra + `_satnica_prep` ).val( cit_format(krajser_prep_price, 2) );
  $(`#` + curr_kalk.kalk_rand + `_` + arg_action.action_sifra + `_satnica_radnik` ).val( cit_format(satnica_radnik, 2) );
  */
  /*

  var sum_ulaz_kom = 0;
  $.each( arg_ulazi, function( u_ind, ulaz ) {
    sum_ulaz_kom += ulaz.kalk_sum_sirovina;
  });

  */

  var sum_ulaz_kom = sum_ulaz_kom_val_kontra(arg_ulazi).kom;


  var broj_boja = 0;

  // uzimam CMYK ZAPIS OD PRVOG ELEMENTA  U PRODUCTU !!!!
  if ( product_data.elements[0].elem_cmyk.indexOf(`1/`) > -1 ) broj_boja = 1;
  if ( product_data.elements[0].elem_cmyk.indexOf(`2/`) > -1 ) broj_boja = 2;
  if ( product_data.elements[0].elem_cmyk.indexOf(`3/`) > -1 ) broj_boja = 3;
  if ( product_data.elements[0].elem_cmyk.indexOf(`4/`) > -1 ) broj_boja = 4;


  if ( broj_boja && broj_boja > 0 ) {

    // { "sifra": "PCONS16", "naziv": "TRAJANJE PRIPREME SLOTERA S TISKOM / minute", "value": 1 },
    sloter_prep_time = find_item(window.cit_local_list.pro_const, `PCONS16`, `sifra`).value; // u satima


    // { "sifra": "RADS9",  type: "PRC17",  "naziv": "Sloter S TISKOM do 500X1300", "unit": "kom", "interval": "h", "speed": 1000, price: 250, modulator: true },

    // sloter s tiskom VELPROM
    radna_stn = find_item(window.cit_local_list.radne_stanice, `RADS9`, `sifra`);

    if ( product_data.rok_isporuke && work_minutes_forward( Date.now(), product_data.rok_isporuke )/(60*24) >= 15 ) {
      // vanski sloter s tiskom
      radna_stn = find_item(window.cit_local_list.radne_stanice, `RADS10B`, `sifra`);
    };

    if ( naklada < 100 ) sloter_print_koef = 1.3;
    if ( naklada >= 100 && naklada < 200 ) sloter_print_koef = 1.2;
    if ( naklada >= 200 && naklada < 500) sloter_print_koef = 1.1;
    if ( naklada >= 500 ) sloter_print_koef = 0.8;

  };


  // na svaku novu boju dodajem 10%
  if ( broj_boja && broj_boja > 1 ) sloter_prep_time = sloter_prep_time * (broj_boja * 1.1); 


  var prep_time = sloter_prep_time * 60;

  var unit_time_work = 1/radna_stn.speed * sloter_print_koef;

  var unit_time = unit_time_work*60;
  var time = unit_time * sum_ulaz_kom;
  var unit_price = ( sloter_prep_time * cijena_sata )/sum_ulaz_kom + ( unit_time_work * radna_stn.price );
  var price = unit_price * sum_ulaz_kom;


  var ulazi = arg_ulazi || this_module.gen_ulazi_from_izlazi(arg_ulazi);
  var ukupno_dobiveno_iz_ulaza = gen_dobiveno_ulaz(ulazi);

  var arg_changed_props = null;
  var izlazi = arg_izlazi || this_module.gen_izlazi( "just_first", arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, radna_stn, ukupno_dobiveno_iz_ulaza );

  var change_action_props = null;
  var action = arg_action || this_module.gen_action( change_action_props, curr_kalk, prep_time, work_time, unit_time, time, unit_price,  price, radna_stn, arg_curr_proces_index );


  var proces = {
    extra_data: arg_extra_data || {},
    proc_koment: arg_proc_koment || "",
    row_sifra: `procesrow`+cit_rand(),
    ulazi: ulazi,
    action: action,
    izlazi: izlazi,

  }; // kraj procesa


  // time u minutama !!!!
  return {
    prep_time, // VEĆ JE U MINUTAMA
    work_time,
    
    unit_time,
    time,
    unit_price,
    price,
    proces,
  }

};

VELPROCES.ljep_klapna_work = function ljep_klapna_work( this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index ) 
{


  // { "sifra": "RADS11",  type: "PRC6", "naziv": "LJEPNJENJE KLAPNE STROJNO", "unit": "kom", "interval": "h", "speed": 2000, price: 400 }
  var radna_stn = arg_radna_stn || find_item(window.cit_local_list.radne_stanice, `RADS11`, `sifra`);


  var ljep_prep_price = find_item(window.cit_local_list.pro_const, `PCONS17`, `sifra`).value;
  var ljep_work_price = radna_stn.price;


  var ljep_prep_time = null;

  if ( naklada < 5 ) ljep_prep_time = 40/ljep_prep_price;
  if ( naklada >= 5 && naklada < 10 ) ljep_prep_time = 9/ljep_prep_price;
  if ( naklada >= 10 && naklada < 20 ) ljep_prep_time = 8/ljep_prep_price;
  if ( naklada >= 20 && naklada < 30 ) ljep_prep_time = 6/ljep_prep_price;
  if ( naklada >= 30 && naklada < 50 ) ljep_prep_time = 4/ljep_prep_price;

  if ( naklada >= 50 ) {

    // OVO NIJE DOBRO - prirepa ljepilice mora biti vremenski unit tj u minutama  !!!
    // Treba pitati Zorana jel to nije priprema nego jel to možda neki koeficijent koji dodajemo ako je naklada veća

    ljep_prep_time = ljep_prep_price / naklada;


    // OVERRIDE
    // OVERRIDE
    // OVERRIDE
    ljep_prep_time = 0.5; // ovo je time u satima 

  };


  var ljep_work_time = null;

  var duz_klapne = product_data.visina;

  if (duz_klapne < 100 ) ljep_work_time = 0.35/ljep_work_price;
  if (duz_klapne >= 100 && duz_klapne < 200 ) ljep_work_time = 0.3667/ljep_work_price;
  if (duz_klapne >= 200 && duz_klapne < 300 ) ljep_work_time = 0.434/ljep_work_price;
  if (duz_klapne >= 300 && duz_klapne < 400 ) ljep_work_time = 0.5/ljep_work_price;
  if (duz_klapne >= 400 && duz_klapne < 500 ) ljep_work_time = 0.567/ljep_work_price;
  if (duz_klapne >= 500 && duz_klapne < 600 ) ljep_work_time = 0.634/ljep_work_price;
  if (duz_klapne >= 600 && duz_klapne < 700 ) ljep_work_time = 0.7/ljep_work_price;
  if (duz_klapne >= 700 && duz_klapne < 800 ) ljep_work_time = 0.9/ljep_work_price;
  if (duz_klapne >= 800 && duz_klapne < 900 ) ljep_work_time = 1.25/ljep_work_price;
  if (duz_klapne >= 900 && duz_klapne < 1000 ) ljep_work_time = 1.525/ljep_work_price;
  if (duz_klapne >= 1000 && duz_klapne < 1100 ) ljep_work_time = 1.8/ljep_work_price;
  if (duz_klapne >= 1100 && duz_klapne < 1200 ) ljep_work_time = 2.075/ljep_work_price;
  if (duz_klapne >= 1200 && duz_klapne < 1300 ) ljep_work_time = 2.35/ljep_work_price;
  if (duz_klapne >= 1300 && duz_klapne < 1400 ) ljep_work_time = 2.625/ljep_work_price;
  if (duz_klapne >= 1400 && duz_klapne < 1500 ) ljep_work_time = 2.9/ljep_work_price;
  if (duz_klapne >= 1500 && duz_klapne < 1600 ) ljep_work_time = 3.567/ljep_work_price;
  if (duz_klapne >= 1600 && duz_klapne < 1700 ) ljep_work_time = 4.234/ljep_work_price;
  if (duz_klapne >= 1700 ) ljep_work_time = 4.8/ljep_work_price;

  // uvijek u satima !!!!


  // FALI LOGIKA POVEĆANJA TRAJANJA ZA SVAKU SLJEDEĆU KLAPNU !!!!!
  // FALI LOGIKA POVEĆANJA TRAJANJA ZA SVAKU SLJEDEĆU KLAPNU !!!!!
  // FALI LOGIKA POVEĆANJA TRAJANJA ZA SVAKU SLJEDEĆU KLAPNU !!!!!
  // FALI LOGIKA POVEĆANJA TRAJANJA ZA SVAKU SLJEDEĆU KLAPNU !!!!!


  // curr_kalk.broj_klapni

  var prep_time = ljep_prep_time * 60;
  var unit_time = ljep_work_time * 60;
  var time = ljep_work_time * 60 * naklada;
  var unit_price = ( ljep_prep_time/naklada * ljep_prep_price ) + (ljep_work_time * ljep_work_price);
  var price = ( ljep_prep_time * ljep_prep_price ) + ( ljep_work_time * ljep_work_price * naklada );


  var ulazi = arg_ulazi || this_module.gen_ulazi_from_izlazi(arg_ulazi);
  var ukupno_dobiveno_iz_ulaza = gen_dobiveno_ulaz(ulazi);

  var arg_changed_props = null;
  var izlazi = arg_izlazi || this_module.gen_izlazi( null, arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, radna_stn, ukupno_dobiveno_iz_ulaza );

  var change_action_props = null;
  var action = arg_action || this_module.gen_action( change_action_props, curr_kalk, prep_time, work_time, unit_time, time, unit_price,  price, radna_stn, arg_curr_proces_index );

  var proces = {
    extra_data: arg_extra_data || {},
    proc_koment: arg_proc_koment || "",
    row_sifra: `procesrow`+cit_rand(),
    ulazi: ulazi,
    action: action,
    izlazi: izlazi,

  }; // kraj procesa

  return {
    prep_time,
    work_time,
    
    unit_time,
    
    time,
    unit_price,
    price,
    proces,
  }

}; 


VELPROCES.usjek_work = function usjek_work( this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index ) 
{


  // { "sifra": "RADS18",  type: "PRC19", "naziv": "USJECANJE STROJ 1", "unit": "kom", "interval": "h", "speed": 240, price: 250, modulator: true },  
  var radna_stn = arg_radna_stn || find_item(window.cit_local_list.radne_stanice, `RADS18`, `sifra`);

  var prirez_objekt = this_module.get_prirez(product_data, curr_kalk);

  if ( !prirez_objekt ) {
    popup_warn(`Nedostaju podaci o prirezu!!`);
    return;
  };

  var prirez_val = prirez_objekt.val;
  var prirez_kontra = prirez_objekt.kontra;


  var usjek_prep_price = find_item(window.cit_local_list.pro_const, `PCONS17B`, `sifra`).value;
  var usjek_work_price = radna_stn.price;

  var povrsina_prirez = prirez_val * prirez_kontra / 1000000;

  var usjek_prep_time = 1; // jedan sat



  if ( naklada < 5 ) usjek_prep_time = 40/usjek_prep_price;
  if ( naklada >= 5 && naklada < 10 ) usjek_prep_time = 9/usjek_prep_price;
  if ( naklada >= 10 && naklada < 20 ) usjek_prep_time = 8/usjek_prep_price;
  if ( naklada >= 20 && naklada < 30 ) usjek_prep_time = 6/usjek_prep_price;
  if ( naklada >= 30 && naklada < 50 ) usjek_prep_time = 4/usjek_prep_price;

  if ( naklada >= 50 ) usjek_prep_time = (usjek_prep_price / naklada) / usjek_prep_price; // ovo sam ja stavio po svojoj volji !!!!


  var usjek_work_time = naklada / radna_stn.speed; // koliko kom u 1 sat

  var usjek_work_koef = 1;

  if ( povrsina_prirez < 1) usjek_work_koef = 1;
  if ( povrsina_prirez >= 1 && povrsina_prirez < 1.25) usjek_work_koef = 1.2;
  if ( povrsina_prirez >= 1.25 && povrsina_prirez < 1.5) usjek_work_koef = 1.54;
  if ( povrsina_prirez >= 1.5 && povrsina_prirez < 2) usjek_work_koef = 2;
  if ( povrsina_prirez >= 2) usjek_work_koef = 2.34;



  var prep_time = usjek_prep_time * 60 * naklada; 

  var unit_time = usjek_work_time/naklada * 60;
  var time = usjek_work_time * 60;
  var unit_price = (usjek_prep_time * usjek_prep_price) + usjek_work_time/naklada * radna_stn.price;
  var price = unit_price * naklada;

  var ulazi = arg_ulazi || this_module.gen_ulazi_from_izlazi(arg_ulazi);
  var ukupno_dobiveno_iz_ulaza = gen_dobiveno_ulaz(ulazi);

  var arg_changed_props = null;
  var izlazi = arg_izlazi || this_module.gen_izlazi( null, arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, radna_stn, ukupno_dobiveno_iz_ulaza );

  var change_action_props = null;
  var action = arg_action || this_module.gen_action( change_action_props, curr_kalk, prep_time, work_time, unit_time, time, unit_price,  price, radna_stn, arg_curr_proces_index );



  var proces = {
    extra_data: arg_extra_data || copy_extra_data_forma,

    proc_koment: arg_proc_koment || "",
    row_sifra: `procesrow`+cit_rand(),
    ulazi: ulazi,
    action: action,
    izlazi: izlazi,

  }; // kraj procesa


  return {
    prep_time,
    work_time,
    
    unit_time,
    
    time,
    unit_price,
    price,
    proces,
  }


};


VELPROCES.pakir_work = function pakir_work( this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index ) 
{

  // { "sifra": "RADS13",  type: "PRC13", "naziv": "PAKIRANJE - Složeno u bunt na paletu", "unit": "kom", "interval": "h", "speed": 2500, price: 250 }, 
  var radna_stn = arg_radna_stn || find_item(window.cit_local_list.radne_stanice, `RADS13`, `sifra`);

  var pakir_prep_price = find_item(window.cit_local_list.pro_const, `PCONS19PRIPREMA`, `sifra`).value;
  var pakir_work_price = radna_stn.price;


  var pakir_prep_time = 0.5; // pola sata


  var pakir_work_speed = radna_stn.speed;


  // ovo je vrijeme za cijelu nakladu !!!!
  var pakir_work_time = naklada/radna_stn.speed; 



  var prep_time = pakir_prep_time * 60;
  var unit_time = 1/radna_stn.speed * 60;
  var time = pakir_work_time * 60;
  var unit_price = (pakir_prep_time * pakir_prep_price)/naklada + (pakir_work_time * pakir_work_price)/naklada;
  var price = (pakir_prep_time * pakir_prep_price) + ( pakir_work_time * pakir_work_price );


  var ulazi = arg_ulazi || this_module.gen_ulazi_from_izlazi(arg_ulazi);
  var ukupno_dobiveno_iz_ulaza = gen_dobiveno_ulaz(ulazi);

  var arg_changed_props = null;
  var izlazi = arg_izlazi || this_module.gen_izlazi( null, arg_changed_props, arg_curr_proces_index, curr_kalk,  ulazi, radna_stn, ukupno_dobiveno_iz_ulaza );

  var change_action_props = null;
  var action = arg_action || this_module.gen_action( change_action_props, curr_kalk, prep_time, work_time, unit_time, time, unit_price,  price, radna_stn, arg_curr_proces_index );


 var proces = {
    extra_data: arg_extra_data || {},
    proc_koment: arg_proc_koment || "",
    row_sifra: `procesrow`+cit_rand(),
    ulazi: ulazi,
    action: action,
    izlazi: izlazi,

  }; // kraj procesa


  return {
    prep_time,
    work_time,
    
    unit_time,
    
    time,
    unit_price,
    price,
    proces,
  }

}; 


VELPROCES.sklad_work = function sklad_work( this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index ) 
{


  // { "sifra": "RADS12",  type: "PRC18", "naziv": "SKLADIŠTENJE RUČNO", "unit": "kom", "interval": "h", "speed": 4000, price: 250 },
  var radna_stn = arg_radna_stn || find_item(window.cit_local_list.radne_stanice, `RADS12`, `sifra`);

  var prep_sat_price = find_item(window.cit_local_list.pro_const, `SKLADPREPPRICE`, `sifra`).value;

  var sklad_sat_cijena = radna_stn.price;

  var prirez_objekt = this_module.get_prirez(product_data, curr_kalk);

  if ( !prirez_objekt ) {
    popup_warn(`Nedostaju podaci o prirezu!!`);
    return;
  };

  var prirez_val = prirez_objekt.val;
  var prirez_kontra = prirez_objekt.kontra;

  var povrsina_prirez = prirez_val * prirez_kontra / 1000000;

  var sklad_trajanje = 0.5/sklad_sat_cijena;

  if (povrsina_prirez < 0.5 ) sklad_trajanje = 0.02/sklad_sat_cijena;
  if (povrsina_prirez >= 0.5 && povrsina_prirez < 0.75 ) sklad_trajanje = 0.035/sklad_sat_cijena;
  if (povrsina_prirez >= 0.75 && povrsina_prirez < 1 ) sklad_trajanje = 0.075/sklad_sat_cijena;
  if (povrsina_prirez >= 1 && povrsina_prirez < 1.25 ) sklad_trajanje = 0.1/sklad_sat_cijena;
  if (povrsina_prirez >= 1.25 && povrsina_prirez < 1.5 ) sklad_trajanje = 0.15/sklad_sat_cijena;
  if (povrsina_prirez >= 1.5 && povrsina_prirez < 2 ) sklad_trajanje = 0.25/sklad_sat_cijena;
  if (povrsina_prirez > 2 ) sklad_trajanje = 0.5/sklad_sat_cijena;


  var prep_time = 0.5 * 60;
  var unit_time = sklad_trajanje * 60;
  var time = unit_time * naklada;
  var unit_price = (prep_time/60 * prep_sat_price)/naklada +  (sklad_trajanje * sklad_sat_cijena);
  var price = (prep_time/60 * prep_sat_price) + (sklad_trajanje * sklad_sat_cijena * naklada);

  var ulazi = arg_ulazi || this_module.gen_ulazi_from_izlazi(arg_ulazi);
  var ukupno_dobiveno_iz_ulaza = gen_dobiveno_ulaz(ulazi);

  var arg_changed_props = null;
  var izlazi = arg_izlazi || this_module.gen_izlazi( null, arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, radna_stn, ukupno_dobiveno_iz_ulaza );

  var change_action_props = null;
  var action = arg_action || this_module.gen_action( change_action_props, curr_kalk, prep_time, work_time, unit_time, time, unit_price,  price, radna_stn, arg_curr_proces_index );

  var proces = {
    extra_data: arg_extra_data || {},
    proc_koment: arg_proc_koment || "",
    row_sifra: `procesrow`+cit_rand(),
    ulazi: ulazi,
    action: action,
    izlazi: izlazi,
   }; // kraj procesa


  return {
    prep_time,
    work_time,
    
    unit_time,
    
    time,
    unit_price,
    price,
    proces,
  }

};


VELPROCES.dostav_work = function dostav_work( this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index )
{


  // { "sifra": "RADS100",  type: "PRC14", "naziv": "DOSTAVA", "unit": "kom", "interval": "h", "speed": 100, price: 250 },
  var radna_stn = arg_radna_stn || find_item(window.cit_local_list.radne_stanice, `RADS100`, `sifra`);


  // { "sifra": "PCONS31", "naziv": "CIJENA DOSTAVE PO PALETI ZA ZAGREB / kn", "value": 70 }, 
  var cijena_dostave = find_item(window.cit_local_list.pro_const, `PCONS31`, `sifra`).value;


  var prirez_mat = curr_kalk.kalk_ploca_mat;

  if ( !prirez_mat ) return null;

  var m2_na_paleti = prirez_mat.m2_na_paleti;

  var povrsina_prirez = curr_kalk.prirez_val * curr_kalk.prirez_kontra / 1000000;

  var povrsina_naklada = povrsina_prirez * naklada

  var dostav = 50;

  var koliko_paleta = povrsina_naklada / m2_na_paleti;

  var koliko_paleta_int = parseInt(koliko_paleta);

  if ( koliko_paleta <= 1) koliko_paleta = 1;

  if ( koliko_paleta > 1 && (koliko_paleta - koliko_paleta_int > 0.5) ) koliko_paleta = koliko_paleta_int + 1;
  if ( koliko_paleta > 1 && (koliko_paleta - koliko_paleta_int <= 0.5) ) koliko_paleta = koliko_paleta_int + 0.5;

  dostav = cijena_dostave * koliko_paleta

  var umanjena_dostava = dostav - ( 12.5 * (koliko_paleta - 1) );


  var prep_time = 0.5;
  var prep_price = 250;


  var unit_time = 200/naklada;
  var time =  200;
  var unit_price = (prep_time * prep_price) +  umanjena_dostava / naklada;
  var price = (prep_time * prep_price)/naklada + umanjena_dostava;

  var ulazi = arg_ulazi || this_module.gen_ulazi_from_izlazi(arg_ulazi);
  var ukupno_dobiveno_iz_ulaza = gen_dobiveno_ulaz(ulazi);

  var arg_changed_props = null;
  var izlazi = arg_izlazi || this_module.gen_izlazi( null, arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, radna_stn, ukupno_dobiveno_iz_ulaza );

  var change_action_props = null;
  var action = arg_action || this_module.gen_action( change_action_props, curr_kalk, prep_time, work_time, unit_time, time, unit_price,  price, radna_stn, arg_curr_proces_index );

  var proces = {
    extra_data: arg_extra_data || {},
    proc_koment: arg_proc_koment || "",
    row_sifra: `procesrow`+cit_rand(),
    ulazi: ulazi,
    action: action,
    izlazi: izlazi,
   }; // kraj procesa


  return {
    prep_time,
    work_time,
    
    unit_time,
    
    time,
    unit_price,
    price,
    proces,
  }

};



VELPROCES.scitex_work = function scitex_work( this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index )
{

  
  var norma_boje = 10; // u mili litrama po kvadratnom metru
  

  /*

    RADNA STANICA PRIMJER
    ----------------------------------------
    {
      "sifra": "RADS2",
      "naziv": "SCITEX - SAMPLE",
      "unit": "kom",
      "interval": "h",
      "speed": 32,
      "price": 1250,
      "func": "scitex_work",
      "type": "PRC17",
      "proces_type_naziv": "Unutranji Tisak",
      "elem_sifra": "SEL18",
      "elem_naziv": "Otisnuti element (VELPROM tisak)"
    },

 */

  var radna_stn = arg_radna_stn || find_item(window.cit_local_list.radne_stanice, `RADS2`, `sifra`);

  var prep_scitex_time = find_item(window.cit_local_list.pro_const, `SCITEXPREPTIME`, `sifra`).value; // u satima
  var prep_scitex_price = find_item(window.cit_local_list.pro_const, `SCITEXPREPPRICE`, `sifra`).value; // u kunama / h

  var work_scitex_speed = radna_stn.speed; // pazi ovdje je speed po metru ne po komadu !!!
  var work_scitex_price = radna_stn.price;

  var sum_plate = 0;
  var sum_boja = 0;

  var sum_scitex_work_time = 0;
  var sum_color_price = 0;

  var missing_plate = false;
  
  
  var missing_color = false;
  var multiload = 1;

  // ako postoji multiload u extra data onda uzmi taj broj
  if ( arg_extra_data && arg_extra_data.multiload ) {
    multiload = arg_extra_data.multiload;
  };


  $.each( arg_ulazi, function( u_ind, ulaz ) {

    var {
        kalk_color_C,
        kalk_color_M,
        kalk_color_Y,
        kalk_color_K,
        kalk_color_W,
        kalk_color_V,
      } = ulaz;

    if (

        (
          kalk_color_C ||
          kalk_color_M ||
          kalk_color_Y ||
          kalk_color_K ||
          kalk_color_W ||
          kalk_color_V
        )
      &&
      missing_color == false
      && 
      ulaz.klasa_sirovine?.sifra !== `KSSBO` // ne smjem uvrstiti scitex boje  ---> te boje ignoriram  u ovom loopu jer njih upisujem

    ) {

      // više ne koristim ovo ---> zato što računam cijenu i utrošak za svaku sirovinu
      // više ne koristim ovo
      // var color_price = calc_color( kalk_color_C, kalk_color_M, kalk_color_Y, kalk_color_K, kalk_color_W, kalk_color_V, `scitex_work` );
      
      // POLA STAVI U CYAN
      if ( kalk_color_C ) {
        
        var color_index = find_index( arg_ulazi, window.scitex_colors_ids.cyan , `_id` );
        
        // stavi da količina boje bude ista kao količina forme ili ploče
        arg_ulazi[color_index].kalk_sum_sirovina = ulaz.kalk_sum_sirovina || 0;
        
        arg_ulazi[color_index].kalk_kom_in_sirovina = norma_boje * kalk_color_C/1000000;
        
        // POLA STAVI U CYAN A POLA STAVI U LIGHT CYAN
        arg_ulazi[color_index].kalk_kom_in_sirovina = arg_ulazi[color_index].kalk_kom_in_sirovina / 2; 
        
        arg_ulazi[color_index].real_unit_price = arg_ulazi[color_index].kalk_kom_in_sirovina * arg_ulazi[color_index].original_sirov.cijena;
        arg_ulazi[color_index].kalk_price = arg_ulazi[color_index].real_unit_price * sum_plus_extra(arg_ulazi[color_index]);
        
      };
      
      // POLA STAVI U LIGHT CYAN
      if ( kalk_color_C ) {
        var color_index = find_index( arg_ulazi, window.scitex_colors_ids.light_cyan , `_id` );
        
        // stavi da količina boje bude ista kao količina forme ili ploče
        arg_ulazi[color_index].kalk_sum_sirovina = ulaz.kalk_sum_sirovina || 0;
        
        arg_ulazi[color_index].kalk_kom_in_sirovina = norma_boje * kalk_color_C/1000000;
        
        // POLA STAVI U CYAN A POLA STAVI U LIGHT CYAN
        arg_ulazi[color_index].kalk_kom_in_sirovina = arg_ulazi[color_index].kalk_kom_in_sirovina / 2; 
        
        arg_ulazi[color_index].real_unit_price = arg_ulazi[color_index].kalk_kom_in_sirovina * arg_ulazi[color_index].original_sirov.cijena;
        arg_ulazi[color_index].kalk_price = arg_ulazi[color_index].real_unit_price * sum_plus_extra(arg_ulazi[color_index]);
        
      };
      
      // POLA STAVI U MAGENTA
      if ( kalk_color_M ) {
        var color_index = find_index( arg_ulazi, window.scitex_colors_ids.magenta , `_id` );
        
        // stavi da količina boje bude ista kao količina forme ili ploče
        arg_ulazi[color_index].kalk_sum_sirovina = ulaz.kalk_sum_sirovina || 0;
        
        arg_ulazi[color_index].kalk_kom_in_sirovina = norma_boje * kalk_color_M/1000000;
        
        // POLA STAVI U CYAN AA POLA STAVI U LIGHT CYAN
        arg_ulazi[color_index].kalk_kom_in_sirovina = arg_ulazi[color_index].kalk_kom_in_sirovina / 2; 
        
        arg_ulazi[color_index].real_unit_price = arg_ulazi[color_index].kalk_kom_in_sirovina * arg_ulazi[color_index].original_sirov.cijena;
        arg_ulazi[color_index].kalk_price = arg_ulazi[color_index].real_unit_price * sum_plus_extra(arg_ulazi[color_index]);
        
      };
      
      // POLA STAVI U LIGHT MAGENTA
      if ( kalk_color_M ) {
        var color_index = find_index( arg_ulazi, window.scitex_colors_ids.light_magenta , `_id` );
        
        // stavi da količina boje bude ista kao količina forme ili ploče
        arg_ulazi[color_index].kalk_sum_sirovina = ulaz.kalk_sum_sirovina || 0;
        
        arg_ulazi[color_index].kalk_kom_in_sirovina = norma_boje * kalk_color_M/1000000;
        
        // POLA STAVI U CYAN AA POLA STAVI U LIGHT CYAN
        arg_ulazi[color_index].kalk_kom_in_sirovina = arg_ulazi[color_index].kalk_kom_in_sirovina / 2; 
        
        arg_ulazi[color_index].real_unit_price = arg_ulazi[color_index].kalk_kom_in_sirovina/1000 * arg_ulazi[color_index].original_sirov.cijena;
        arg_ulazi[color_index].kalk_price = arg_ulazi[color_index].real_unit_price * sum_plus_extra(arg_ulazi[color_index]);
        
      };
      
      // YELLOW
      if ( kalk_color_Y ) {
        var color_index = find_index( arg_ulazi, window.scitex_colors_ids.yellow , `_id` );
        
        // stavi da količina boje bude ista kao količina forme ili ploče
        arg_ulazi[color_index].kalk_sum_sirovina = ulaz.kalk_sum_sirovina || 0;
        
        arg_ulazi[color_index].kalk_kom_in_sirovina = norma_boje * kalk_color_Y/1000000;
        
        arg_ulazi[color_index].real_unit_price = arg_ulazi[color_index].kalk_kom_in_sirovina * arg_ulazi[color_index].original_sirov.cijena;
        arg_ulazi[color_index].kalk_price = arg_ulazi[color_index].real_unit_price * sum_plus_extra(arg_ulazi[color_index]);
        
      };
      
      // KEY
      if ( kalk_color_K ) {
        
        var color_index = find_index( arg_ulazi, window.scitex_colors_ids.black , `_id` );
        
        // stavi da količina boje bude ista kao količina forme ili ploče
        arg_ulazi[color_index].kalk_sum_sirovina = ulaz.kalk_sum_sirovina || 0;
        
        arg_ulazi[color_index].kalk_kom_in_sirovina = norma_boje * kalk_color_K/1000000;
        
        arg_ulazi[color_index].real_unit_price = arg_ulazi[color_index].kalk_kom_in_sirovina * arg_ulazi[color_index].original_sirov.cijena;
        arg_ulazi[color_index].kalk_price = arg_ulazi[color_index].real_unit_price * sum_plus_extra(arg_ulazi[color_index]);
        
      };
      
      
      sum_scitex_work_time +=  sum_plus_extra(ulaz) / work_scitex_speed / multiload;

    }
    else {

      if ( missing_color == false && ulaz.klasa_sirovine?.sifra !== `KSSBO` ) { // nemoj gladati ako je ulaz zaista prava boja SCITEX BOJA ( KSSBO ) 
        popup_warn(`Potrebno upisati barem površinu za jednu boju za ploče koje ulaze u SCITEX proces !!!`); 
        missing_color = true;
      };

      return;
    };

  });  // kraj loopa po svim ulazima u ovom procesu


  var sum_ulaz_kom = sum_ulaz_kom_val_kontra(arg_ulazi).kom;
  
  
  
  

  var prep_time = prep_scitex_time * 60;

  var unit_time = ( sum_scitex_work_time/sum_ulaz_kom ) * 60;
  var time = sum_scitex_work_time * 60;
  var unit_price = (prep_scitex_time/sum_ulaz_kom * prep_scitex_price) + (sum_scitex_work_time/sum_ulaz_kom * work_scitex_price);
  var price = (prep_scitex_time * prep_scitex_price) + (sum_scitex_work_time * work_scitex_price);

  var ulazi = arg_ulazi || this_module.gen_ulazi_from_izlazi(arg_ulazi);
  var ukupno_dobiveno_iz_ulaza = gen_dobiveno_ulaz(ulazi);

  var izlaz_val = null;
  var izlaz_kontra = null;

  if ( ulazi.length == 1 ) {
    izlaz_val = ulazi[0].kalk_po_valu;
    izlaz_kontra = ulazi[0].kalk_po_kontravalu;
  }; 

  var arg_changed_props = null;
  
  // namjerno ću anulirati sve vrijednosti boja NAKON SCITEX PROCESA
  // zato što te verijable više nisu potrebne nakon printanja i ne želim ih dalje kopirati na daljnje izlaze
  var arg_changed_props = {

    kalk_color_C: null,
    kalk_color_M: null,
    kalk_color_Y: null,
    kalk_color_K: null,
    kalk_color_W: null,
    kalk_color_V: null,

    /*
    kalk_noz_big: null,
    kalk_nest_count: null,

    kalk_po_valu: null,
    kalk_po_kontravalu: null,
    */

  };

  var izlazi = arg_izlazi || this_module.gen_izlazi( null, arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, radna_stn, ukupno_dobiveno_iz_ulaza, izlaz_val, izlaz_kontra );



  var change_action_props = {
    action_multiload: multiload,
  };
  
  
  var action = arg_action || this_module.gen_action( change_action_props, curr_kalk, prep_time, work_time, unit_time, time, unit_price, price, radna_stn, arg_curr_proces_index );

  var proces = {
    extra_data: arg_extra_data || {},
    proc_koment: arg_proc_koment || "",
    row_sifra: `procesrow`+cit_rand(),
    ulazi: ulazi,
    action: action,
    izlazi: izlazi, 
  }; // kraj procesa

  return {
    prep_time,
    work_time,
    
    unit_time,
    
    time,
    unit_price,
    price,
    proces
  };

};



VELPROCES.zund_work = function zund_work( 
this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index 
) {


  // { "sifra": "RADS1B", type: "PRC9", "naziv": "REZANJE ZUND", unit: "m", interval: "h", speed: 100, price: 250 },
  var radna_stn = arg_radna_stn || find_item(window.cit_local_list.radne_stanice, `RADS1B`, `sifra`);


  var prep_zund_time = find_item(window.cit_local_list.pro_const, `PCONS9TP`, `sifra`).value; // u satima
  var prep_zund_price = find_item(window.cit_local_list.pro_const, `PCONS9SAT`, `sifra`).value;


  var zund_speed = radna_stn.speed; // pazi ovdje je speed po metru ne po komadu !!!
  var zund_price = radna_stn.price;



  var sum_plate = 0;
  var sum_zund_work_time = 0;

  var missing_noz_big = false;

  // gledam prosjek svih duljina noža / biga na svim poločama ulaza
  $.each( arg_ulazi, function( u_ind, ulaz ) {

    if ( ulaz.kalk_noz_big && missing_noz_big == false ) {

      // speed koef može biti od 1 pa do 3,5 ovisno o kojem je materijalu riječ 
      var zund_speed_koef = get_ulaz_mat(ulaz).zund || 1;
      sum_zund_work_time += zund_speed_koef * ( ulaz.kalk_noz_big * ulaz.kalk_sum_sirovina) / zund_speed;

      sum_plate += sum_plus_extra(ulaz);
    }
    else {
      missing_noz_big = true;
      popup_warn(`Potrebno upisati dužinu nož/big u metrima za sve ploče koje ulaze u ZUND proces !!!`);
      return;
    };

  });

  var prep_time = prep_zund_time * 60;

  var unit_time = ( sum_zund_work_time/sum_plate )*60;
  var time = sum_zund_work_time*60;
  var unit_price = (prep_zund_time/sum_plate * prep_zund_price) + (sum_zund_work_time/sum_plate * zund_price);
  var price = (prep_zund_time * prep_zund_price) + (sum_zund_work_time * zund_price);

  var ulazi = arg_ulazi || this_module.gen_ulazi_from_izlazi(arg_ulazi);
  var ukupno_dobiveno_iz_ulaza = gen_dobiveno_ulaz(ulazi);

  var prirez_val = null;
  var prirez_kontra = null;

  // ako je samo jedan ulaz onda sigurno znam da imam na izlazu isti val i kontra val
  if ( ulazi.length == 1 ) {
    prirez_val = ulazi[0].kalk_po_valu;
    prirez_kontra = ulazi[0].kalk_po_kontravalu;
  };

  var arg_changed_props = {

    kalk_color_C: null,
    kalk_color_M: null,
    kalk_color_Y: null,
    kalk_color_K: null,
    kalk_color_W: null,
    kalk_color_V: null,


    /*
    kalk_noz_big: null,
    kalk_nest_count: null,

    kalk_po_valu: null,
    kalk_po_kontravalu: null,
    */

  };

  var izlazi = arg_izlazi || this_module.gen_izlazi( null, arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, radna_stn, ukupno_dobiveno_iz_ulaza, prirez_val, prirez_kontra );

  var change_action_props = null;
  var action = arg_action || this_module.gen_action( change_action_props, curr_kalk, prep_time, work_time, unit_time, time, unit_price,  price, radna_stn, arg_curr_proces_index );

  var proces = {
    extra_data: arg_extra_data || {},
    proc_koment: arg_proc_koment || "",
    row_sifra: `procesrow`+cit_rand(),
    ulazi: ulazi,
    action: action,
    izlazi: izlazi, 
  }; // kraj procesa

  return {
    prep_time,
    work_time,
    
    unit_time,
    time,
    unit_price,
    price,
    proces
  };

};



VELPROCES.clean_work = function clean_work( 
this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index ) 
{
  
  /*
  JAKO BITNO !!!!!!!
  
  SADA OVA FUNC RADI TAKO DA IMA SAMO JEDAN ULAZ NA I TAJ ULAZ MOŽE BITI SAMO MONTAŽNA PLOČA !!!
  SADA OVA FUNC RADI TAKO DA IMA SAMO JEDAN ULAZ NA I TAJ ULAZ MOŽE BITI SAMO MONTAŽNA PLOČA !!!
  
  */

  /*
  { 
    "sifra": "RADS1CLEAN", "naziv": "OPTRGAVANJE NAKON ŠTANCE ILI ZUND-a", 
    "type": "PRC23", "proces_type_naziv": "Rezanje", 
    "elem_sifra": "SEL38", "elem_naziv": "Rezani element",
    "unit": "m", "interval": "h", "speed": 5090, "price": 300, "func": "zund_work",
  },
  */

  var radna_stn = arg_radna_stn || find_item(window.cit_local_list.radne_stanice, `RADS1CLEAN`, `sifra`);

  // { "sifra": "PCONSTANCATEAROFFTIME", "naziv": "VRIJEME PRIPREME ZA OPTRGAVANJE ZA ŠTANCU", "value": 1 },
  var prep_time_za_optrgavanje = find_item(window.cit_local_list.pro_const, `PCONSTANCATEAROFFTIME`, `sifra`).value; // u satima
  
  // { "sifra": "PCONS800", "naziv": "SATNICA ČIŠĆENJE kn/h ", "value": 150 },
  var prep_price_za_ciscenje = prep_zund_price = find_item(window.cit_local_list.pro_const, `PCONS800`, `sifra`).value;

  // { "sifra": "PCONS8", "naziv": "TRAJANJE ČIŠĆENJA ZUND PO METRU NOŽA/BIGA", "value": 0.03/150 }, // Zoran je rekao 0,03 kn po metru
  var clean_time_per_noz_big = find_item(window.cit_local_list.pro_const, `PCONS8`, `sifra`).value; // u satima

  // { "sifra": "PCONS800", "naziv": "SATNICA ČIŠĆENJE kn/h ", "value": 150 },  
  var clean_price = find_item(window.cit_local_list.pro_const, `PCONS800`, `sifra`).value;

  var sum_plate = 0;
  var sum_clean_work_time = 0;

  var missing_noz_big = false;

  // gledam prosjek svih duljina noža / biga na svim pločama ulaza
  $.each( arg_ulazi, function( u_ind, ulaz ) {

    if ( ulaz.kalk_noz_big && missing_noz_big == false ) {
      sum_clean_work_time += ( ulaz.kalk_noz_big * sum_plus_extra(ulaz) ) * clean_time_per_noz_big;
      sum_plate += sum_plus_extra(ulaz);
    }
    else {
      missing_noz_big = true;
      popup_warn(`Potrebno upisati dužinu nož/big u metrima za sve ploče koje ulaze u proces ZUND ČIŠĆENJA !!!`);
      return;
    };

  });

  var prep_time = prep_time_za_optrgavanje * 60;

  var unit_time = ( sum_clean_work_time/sum_plate )*60;
  var time = sum_clean_work_time * 60;
  var unit_price = (prep_time_za_optrgavanje/sum_plate * prep_price_za_ciscenje) + (sum_clean_work_time/sum_plate * clean_price);
  var price = (prep_time_za_optrgavanje * prep_price_za_ciscenje) + (sum_clean_work_time * clean_price);


  var ulazi = arg_ulazi || this_module.gen_ulazi_from_izlazi(arg_ulazi);
  var ukupno_dobiveno_iz_ulaza = gen_dobiveno_ulaz(ulazi);

  var prirez_val = null;
  var prirez_kontra = null;

  // ako je samo jedan ulaz onda sigurno znam da imam na izlazu isti val i kontra val
  if ( ulazi.length == 1 ) {
    prirez_val = ulazi[0].kalk_po_valu;
    prirez_kontra = ulazi[0].kalk_po_kontravalu;
  };

  var arg_changed_props = null;

  // var izlazi = arg_izlazi || this_module.gen_izlazi( null, arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, radna_stn, ukupno_dobiveno_iz_ulaza, prirez_val, prirez_kontra );

  var all_izlazi = [];
  if ( !ulazi[0].kalk_plate_id ) {
    popup_warn(`Nije moguće odrediti izlaze jer niste definirali formu na ulazu i program ne zna koji elementi nastaju nakog optrgavanja !!!`);
    return;
  };
  $.each( product_data.elements, function(e_ind, elem) {

    $.each( elem.elem_plates, function(plate_ind, plate) {

      if ( plate.plate_id == ulazi[0].kalk_plate_id ) {

        var elem_copy = cit_deep(elem);


        // upiši u kalk element točan broj koliko tih elemenata stane na baš ovu specifičnu montažnu ploču !!!!
        elem_copy.elem_plate_count = plate.count;
        elem_copy.elem_opis = `P${ arg_curr_proces_index+1 } IZLAZ ${ elem_copy.elem_opis }`,
        elem_copy.elem_tip.kalk = true;

        // reducaj sve alate koji su zapravo objekti sirovina
        // reducaj sve alate koji su zapravo objekti sirovina
        if ( elem_copy.elem_alat?.length > 0 ) {
          $.each( elem_copy.elem_alat, function(alat_ind, alat) {
            elem_copy.elem_alat[alat_ind] = reduce_sir(alat); // reducaj sve alate koji su zapravo objekti sirovina
          });
        };

        var old_izlazi_arr = curr_kalk.procesi[arg_curr_proces_index].izlazi;

        function find_same_element_in_izlazi(izlaz_arr, elem_copy) {

          var found_izlaz = null;
          $.each( izlaz_arr, function(iz_ind, old_exit) {

            // ako nađem izlaz koji ima isti tip elementa kao ovaj element koji sada trebam
            // i ako ima isti redni broj element onda prekopiraj tu istu sifru elementa od izlaza
            // jer se može dogoditi da obrišem stari element i ponovo ga napravim ali da bude isti tip elementa

            if ( 
              old_exit.kalk_element?.elem_tip?.sifra == elem_copy.elem_tip.sifra 
              &&
              /* mora također biti isti redni broj elementa jer npr police su isti tip ali različiti su redni brojevi kao i npr pregrade */
              (old_exit.kalk_element.elem_RB || null) == (elem_copy.elem_RB || null) 
            ) {

              found_izlaz = old_exit;
            };

          });
          return found_izlaz;
        };


        var old_izlaz = find_same_element_in_izlazi(old_izlazi_arr, elem_copy);

        // ako sam našao da ovaj element već postoji u izlazima onda uzmi tu postojeću šifru elementa
        // A NE NOVU ŠIFRU ELEMENTA !!!!

        if ( old_izlaz ) elem_copy.sifra = old_izlaz.kalk_element.sifra;

        var arg_changed_props = {

          kalk_sum_sirovina: ( plate.count * sum_plus_extra(ulazi[0]) ),
          kalk_element: elem_copy,
          kalk_kom_in_product: elem_copy.elem_on_product || 1,

          kalk_color_C: null,
          kalk_color_M: null,
          kalk_color_Y: null,
          kalk_color_K: null,
          kalk_color_W: null,
          kalk_color_V: null,

          kalk_noz_big: null,
          kalk_nest_count: null,

          kalk_po_valu: elem_copy.elem_val || null,
          kalk_po_kontravalu: elem_copy.elem_kontra || null,


          kalk_plate_naziv: ulazi[0].kalk_plate_naziv || null, // plate.naziv  + ` --- ` + plate.sirov.full_naziv,
          kalk_plate_id: ulazi[0].kalk_plate_id || null,  // plate.plate_id,

        };

        // također kopiraj šifru cijelog izlaza (već sam kopirao šifu elementa)
        if ( old_izlaz ) arg_changed_props.sifra = old_izlaz.sifra;


        // jako je bitno da sada dajem samo jedan ulaz  [ ulazi[0] ] !!!!
        // ako ima više ulaza tj više montažnih ploča ova funkcija neće raditi kako treba
        // ako ima više ulaza tj više montažnih ploča ova funkcija neće raditi kako treba 
        var jedan_izlaz_arr = 
            this_module.gen_izlazi( null, arg_changed_props, arg_curr_proces_index, curr_kalk, [ ulazi[0] ], radna_stn, ukupno_dobiveno_iz_ulaza, null, null );

        all_izlazi.push( jedan_izlaz_arr[0] );

      }; // kraj ako je isti plate id u element i u current ulazu

    });  // loop po pločama koje na sebi imaju ovaj element

  }); // loop po svim elementima 

  var change_action_props = null;
  var action = arg_action || this_module.gen_action( change_action_props, curr_kalk, prep_time, work_time, unit_time, time, unit_price, price, radna_stn, arg_curr_proces_index );

  var proces = {
    extra_data: arg_extra_data || {},
    proc_koment: arg_proc_koment || "",
    row_sifra: `procesrow`+cit_rand(),
    ulazi: ulazi,
    action: action,
    izlazi: all_izlazi, 
  }; // kraj procesa

  return {
    prep_time,
    work_time,
    
    unit_time,
    time,
    unit_price,
    price,
    proces
  };

};


/*
stao sam ovdje kod definiranja optrgavanja tj čišćenja za svaku štancu
mislim da je najbolje računati čišćenje po metrima noža / biga
*/
  
VELPROCES.stanca_work = function stanca_work( this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index ) {

  // { "sifra": "RADS1B", type: "PRC9", "naziv": "REZANJE ZUND", unit: "m", interval: "h", speed: 100, price: 250 },
  var radna_stn = arg_radna_stn || find_item(window.cit_local_list.radne_stanice, `RAD136`, `sifra`);
  var tear_off_time_per_nest = find_item(window.cit_local_list.pro_const, `PCONCLEANPERNEST`, `sifra`).value;
  
  var stanca_speed = radna_stn.speed; // pazi ovdje je speed po metru ne po komadu !!!
  var stanca_price = radna_stn.price;
    
  
  
  // { "sifra": "PCONS800", "naziv": "SATNICA ČIŠĆENJE kn/h ", "value": 125 },  
  var stanca_prep_price = find_item(window.cit_local_list.pro_const, `PCONS800`, `sifra`).value; // u kunama po satu
  
  // { "sifra": "PCONSTANCATEAROFFTIME", "naziv": "VRIJEME PRIPREME ZA OPTRGAVANJA ZA ŠTANCU", "value": 1 }, // 1 sat
  var tear_off_prep_time = find_item(window.cit_local_list.pro_const, `PCONSTANCATEAROFFTIME`, `sifra`).value;
  var prep_stanca_time_po_noz_big = find_item(window.cit_local_list.pro_const, `PCONSTANCAPREP`, `sifra`).value;
  
  
  var curr_proces = curr_kalk.procesi[arg_curr_proces_index];
  

  var sum_plate = 0;
  var alat_num = 0;
  
  var sum_prep_noz_big = 0;
  
  var sum_tear_off_time = 0;
  var sum_stanca_work_time = 0;

  var missing_noz_big = false;
  var missing_nest_count = false;
  
  if ( !arg_ulazi ) arg_ulazi = [];

  var alati = find_elem_alate( product_data, curr_proces, arg_ulazi[0] || null );
  
  $.each( alati, function(alat_ind, alat) {
    
    var alat_ulaz = convert_db_sirov_to_kalk_sirov(alat);
    if ( curr_proces.extra_data?.alat_apart ) alat_ulaz.kalk_price = 0;

    $.each(  arg_ulazi, function(fu_ind, find_ulaz) {

      // uvijek stavi da je količine = 1 za alate
      if ( find_ulaz.klasa_sirovine?.sifra == `KS5`) arg_ulazi[fu_ind].kalk_sum_sirovina = 1;

      // OVERRIDE AKO SE ALAT NAPLAĆUJE ODVOJENO
      // KS5 znači alat
      if ( find_ulaz.klasa_sirovine?.sifra == `KS5` && curr_proces.extra_data?.alat_apart == true ) arg_ulazi[fu_ind].kalk_price = 0;
    });

    arg_ulazi = insert_uniq(arg_ulazi, alat_ulaz, `_id`);
    
  });

  
  

  $.each( product_data.elements, function(e_ind, elem) {

    $.each( elem.elem_plates, function(plate_ind, plate) {

      if ( 
        plate.plate_id == arg_ulazi[0]?.kalk_plate_id
        &&
        elem.elem_alat?.length > 0 // ako taj element ima u sebi jedan ili više alata
      ) {

        $.each(  elem.elem_alat, function(alat_ind, alat) {
          
          var alat_ulaz = convert_db_sirov_to_kalk_sirov(alat);
          if ( curr_proces.extra_data?.alat_apart ) alat_ulaz.kalk_price = 0;
          
          $.each(  arg_ulazi, function(fu_ind, find_ulaz) {
           
            // uvijek stavi da je količine = 1 za alate
            if ( find_ulaz.klasa_sirovine?.sifra == `KS5`) arg_ulazi[fu_ind].kalk_sum_sirovina = 1;

            // OVERRIDE AKO SE ALAT NAPLAĆUJE ODVOJENO
            // OVERRIDE AKO SE ALAT NAPLAĆUJE ODVOJENO
            // OVERRIDE AKO SE ALAT NAPLAĆUJE ODVOJENO
            // KS5 znači alat
            if ( find_ulaz.klasa_sirovine?.sifra == `KS5` && curr_proces.extra_data?.alat_apart == true ) arg_ulazi[fu_ind].kalk_price = 0;
          });
          
          arg_ulazi = insert_uniq(arg_ulazi, alat_ulaz, `_id`);
          
        }); // lopp po svim alatima za ovaj element

      }; // jel plate unuter elementa je isti plate koji je izabran u štanci

    });  // po svim plate unuter nekog elementa

  }); // lopp po svim elementima u productu

  
  $.each(  arg_ulazi, function(fu_ind, find_ulaz) {
    if ( find_ulaz.klasa_sirovine?.sifra == `KS5`) alat_num += 1;
  });
  
  
  if ( !alat_num ) {
    popup_warn(`Nedostaje ALAT za štancanje !!!`);
    return;
  };
  
  
  function dodatak_stanca_per_nest(nest) {
    return ( 1 + (nest - 1)*0.05 ) / stanca_price;
  };
  
  
  // gledam prosjek svih duljina noža / biga na svim poločama ulaza
  $.each( arg_ulazi, function( u_ind, ulaz ) {
    
    // ------------------------------------ AKO NIJE ALAT !!!! ------------------------------------
    if ( ulaz.klasa_sirovine?.sifra !== `KS5` ) {

      if ( 
        ulaz.kalk_noz_big 
        &&
        ulaz.kalk_nest_count 
        &&
        missing_noz_big == false 
        &&
        missing_nest_count == false 
      ) {

        sum_prep_noz_big += ulaz.kalk_noz_big * prep_stanca_time_po_noz_big

        sum_tear_off_time += (tear_off_time_per_nest * ulaz.kalk_nest_count) + radna_stn.tear_off_time;
        sum_stanca_work_time += dodatak_stanca_per_nest(ulaz.kalk_nest_count) + ( sum_plus_extra(ulaz) / stanca_speed  );


        sum_plate += ((ulaz.kalk_sum_sirovina || 0) + (ulaz.kalk_extra_kom || 0));
      }
      else {

        if ( !ulaz.kalk_noz_big ) {
            missing_noz_big = true;
          popup_warn(`Nedostaje NOŽ/BIG za štancu !!!`);
          return;
        };

        if ( !ulaz.kalk_nest_count ) {
          missing_nest_count = true;
          popup_warn(`Nedostaje BROJ GNJEZDA za štancu !!!`);
          return;
        };

      };
      
    };
    // ------------------------------------ AKO NIJE ALAT !!!! ------------------------------------
  
  });

  var prep_time = (tear_off_prep_time + sum_prep_noz_big) * 60;

  var unit_time = ( sum_stanca_work_time/sum_plate ) * 60;
  var time = sum_stanca_work_time*60;
  
  var unit_price = 
      ( (tear_off_prep_time/sum_plate + sum_prep_noz_big/sum_plate ) * stanca_prep_price ) +
      (sum_stanca_work_time/sum_plate * stanca_price) + 
      sum_tear_off_time/sum_plate * stanca_prep_price;
  
  
  var price =
      ( (tear_off_prep_time + sum_prep_noz_big ) * stanca_prep_price ) +
      (sum_stanca_work_time * stanca_price) + 
      sum_tear_off_time * stanca_prep_price;
  

  var ulazi = arg_ulazi || this_module.gen_ulazi_from_izlazi(arg_ulazi);
  
  var ukupno_dobiveno_iz_ulaza = gen_dobiveno_ulaz(ulazi);

  var prirez_val = null;
  var prirez_kontra = null;

  
  // ako je samo jedan ulaz onda sigurno znam da imam na izlazu isti val i kontra val
  if ( ulazi.length == 1 ) {
    prirez_val = ulazi[0].kalk_po_valu;
    prirez_kontra = ulazi[0].kalk_po_kontravalu;
  };

  var arg_changed_props = {

    kalk_color_C: null,
    kalk_color_M: null,
    kalk_color_Y: null,
    kalk_color_K: null,
    kalk_color_W: null,
    kalk_color_V: null,


    /*
    kalk_noz_big: null,
    kalk_nest_count: null,

    kalk_po_valu: null,
    kalk_po_kontravalu: null,
    */

  };
  

  var izlazi = arg_izlazi || this_module.gen_izlazi( null, arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, radna_stn, ukupno_dobiveno_iz_ulaza, prirez_val, prirez_kontra );

  var change_action_props = null;
  var action = arg_action || this_module.gen_action( change_action_props, curr_kalk, prep_time, work_time, unit_time, time, unit_price,  price, radna_stn, arg_curr_proces_index );
  
  var proces = {
    extra_data: arg_extra_data || {},
    proc_koment: arg_proc_koment || "",
    row_sifra: `procesrow`+cit_rand(),
    ulazi: ulazi,
    action: action,
    izlazi: izlazi, 
  }; // kraj procesa

  return {
    prep_time,
    work_time,
    
    unit_time,
    time,
    unit_price,
    price,
    proces
  };

};


VELPROCES.kasir_work = function kasir_work( 
this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index ) 
{
    
    /*
    
      { 
        "sifra": "RAD135", "naziv": "RUČNA KAŠIRKA - KAŠIRANJE", "unit": "kom", "func": "",
        "type": "PRC2", "proces_type_naziv": "Kaširanje", 
        "elem_sifra": "SEL21", "elem_naziv": "Kaširani element",
        "interval": "h", "speed": 125, "price": 300,
      },
    
    */
    
/*    

STROJNO B1 ARAK  NA B1 VAL. LJEP.	700	1000	1,5
STROJNO B0 ARAK  NA B0 VAL. LJEP.	1000	1400	1,5
RUČNO B1 ARAK NA VAL. LJEP.	700	1000	4
RUČNO B0 ARAK NA VAL. LJEP.	1000	1400	6
    
*/
    
    var radna_stn = arg_radna_stn || find_item(window.cit_local_list.radne_stanice, `RAD135`, `sifra`);
    // { "sifra": "PCONS5TIME", "naziv": "TRAJANJE PRIPREME KAŠIRANJA", "value": 1 },
    var kasir_prep_time = find_item(window.cit_local_list.pro_const, `PCONS5TIME`, `sifra`).value;
    // { "sifra": "PCONS5PRICE", "naziv": "PRIPREMA KAŠIRANJA SATNICA CIJENA", "value": 200 },
    var kasir_prep_price = find_item(window.cit_local_list.pro_const, `PCONS5PRICE`, `sifra`).value;
    
    
    var kasir_num = 1;
    
    if ( arg_extra_data && arg_extra_data.kasir_num ) kasir_num = arg_extra_data.kasir_num;
    
    var sum_ljepenka = 0;
    var sum_papir = 0;
    var sum_papir_area = 0;
    
    var sum_work_time = 0;

    var missing_ljepenka = false;
    var missing_papir = false;
    
    
    if ( arg_ulazi.length < 2 )  {
      popup_warn(`Za ovaj proces potrebno je dodati minimalno 2 ulaza !!!`);
      return;
    };
    
    $.each( arg_ulazi, function( u_ind, ulaz ) {
      
      var mater = ulaz.vrsta_materijala?.sifra ? find_item(window.cit_local_list.vrsta_mater, ulaz.vrsta_materijala.sifra, `sifra`) : null;
      
      // bilo koji pločasti materijal ALI DA NIJE PAPIR
      if ( mater && mater.sifra !== `VM1` ) {
        sum_ljepenka += sum_plus_extra(ulaz);
      };
      
      
      // TO JE PAPIR !!!
      if ( mater && mater.sifra == `VM1` ) {

        if ( !ulaz.kalk_po_valu || !ulaz.kalk_po_kontravalu ) {
          popup_warn(`Nedostaju kontra tok i tok dimenzije araka !!!`);
          return;
        };
        
        sum_papir += ((ulaz.kalk_sum_sirovina || 0) + (ulaz.kalk_extra_kom || 0));
        var area = ulaz.kalk_po_valu * ulaz.kalk_po_kontravalu / 1000000;
        sum_papir_area += sum_plus_extra(ulaz) * area;
        
      };
      
      
    }); // loop po ulazima
    
    
    if ( sum_ljepenka == 0 || sum_papir == 0 )  {
      popup_warn(`Za ovaj proces je potrebno dodati ploču na koju se kašira i papir koji se lijepi na ploču !!!`);
      return;
    };
    

    var prep_time = kasir_prep_time * 60;

    var unit_time = 1/radna_stn.speed * 60 + 1/radna_stn.speed * ( (kasir_num - 1) * 0.15 * 60 ); // dodaj 15 posto za svaki novi arak
    var time = unit_time * sum_papir;
    
    
    var unit_price = (kasir_prep_time/sum_papir * kasir_prep_price) + ( unit_time/60 * radna_stn.price );
    var price = (kasir_prep_time * kasir_prep_price) + ( time/60 * radna_stn.price );

    
    var ulazi = arg_ulazi || this_module.gen_ulazi_from_izlazi(arg_ulazi);
    
    var ukupno_dobiveno_iz_ulaza = sum_ljepenka; // uzmi samo koliko je ploča ljepenke kaširano !!!
    
  
  
    
    var arg_changed_props = null;
    var izlazi = this_module.gen_izlazi( null, arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, radna_stn, ukupno_dobiveno_iz_ulaza, null, null );
    
    
    var change_action_props = {
      action_kasir_num: kasir_num,
    };
    
    var action = arg_action || this_module.gen_action( change_action_props, curr_kalk, prep_time, work_time, unit_time, time, unit_price, price, radna_stn, arg_curr_proces_index );

    var proces = {
      extra_data: arg_extra_data || {},
      proc_koment: arg_proc_koment || "",
      row_sifra: `procesrow`+cit_rand(),
      ulazi: ulazi,
      action: action,
      izlazi: izlazi, 
    }; // kraj procesa

    return {
      prep_time,
      work_time,
      
      unit_time,
      time,
      unit_price,
      price,
      proces
    };

  };


VELPROCES.gun_glue_work = function gun_glue_work( 
  this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index ) 
{


  /*  
    { 
      "sifra": "RADGLUEGUN", "naziv": "LJEPLJENJE - RUČNI PIŠTOLJ", "func": "gun_glue_work",
      "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
      "elem_sifra": "SELDORADA", "elem_naziv": "Zaljepljeni elementi/proizvod",
      "unit": "kom", "interval": "h", "speed": 1, "price": 150,
    },
  */
  
  
  // TODO promjeniti kada mi BRUNO kaže prave veličine 
  // TODO promjeniti kada mi BRUNO kaže prave veličine 
  // TODO promjeniti kada mi BRUNO kaže prave veličine 
  // ------------------------------------------------------------------------------
  var norma_ljepila = 6; // 6 grama po metru  tj 1000 milimetara
  var unit_time = 20/60; // to je 20 sekundi po metru ----> odmah sam izrazio vrijeme u minutama
  
  // ------------------------------------------------------------------------------
  // TODO promjeniti kada mi BRUNO kaže prave veličine 
  // TODO promjeniti kada mi BRUNO kaže prave veličine 
  // TODO promjeniti kada mi BRUNO kaže prave veličine 


  var radna_stn = arg_radna_stn || find_item(window.cit_local_list.radne_stanice, `RADGLUEGUN`, `sifra`);
  var glue_polica_prep_time = find_item(window.cit_local_list.pro_const, `PCONSGLUEPREPTIME`, `sifra`).value; // u satima
  var glue_polica_prep_price = find_item(window.cit_local_list.pro_const, `PCONS4GLUEPREPPRICE`, `sifra`).value; // u 150 kuna
  
  var glue_count = 0;
  var shelf_count = 0;
  var element_count = 0;
  
  var elem_opis = ``;
  
  // samo brojim sve varijable koje trebam
  $.each( arg_ulazi, function( u_ind, ulaz ) {
    
    // { sifra: `TSIR11`, naziv: `POMOĆNI ELEMENT` },
    if ( ulaz.tip_sirovine?.sifra !== `TSIR11` ) {
      element_count += 1;
      elem_opis = `${ ulaz.kalk_element.elem_tip.short || ( "(" + ulaz.kalk_element.elem_opis + ")" ) } + ` + elem_opis;
    };

    if ( ulaz.klasa_sirovine?.sifra == "KS111" ) { // ovo je shelf lock
      shelf_count += 1
      elem_opis = `LOCK + ` + elem_opis;
    };
    
    if ( ulaz.klasa_sirovine && find_item( window.glue_classes, ulaz.klasa_sirovine.sifra, "sifra") ) { // ovo su sva moguća ljepila
      glue_count += 1;
    };
  
  });
  
  // ako ima bilo što u elem opisu ----> dodaj na početak na primjer P3-1 PIŠTOLJ LJEP.
  if ( elem_opis ) elem_opis = `P${ (arg_curr_proces_index+1) + `-1` } IZLAZ PIŠTOLJ // `+ elem_opis;

  // obriši zadnja dva stringa tj + znak na kraju
  elem_opis = elem_opis.slice(0, -2);
  
  
  if ( glue_count == 0 ) {
    popup_warn(`Za ovaj proces je potrebno barem jedno ljepilo !!!!`);
    return;
  };
  
  if ( element_count < 2 )  {
    popup_warn(`Za ovaj proces potrebno je dodati minimalno 2 elementa koja treba zaljepiti !!!`);
    return;
  };
  
  
  if ( !arg_extra_data?.izlaz_kom ) {
    popup_warn(`Potrebno upisati koliko komada izlazi nakon ljepljenja !!!`);
    return;
  };
  
  
  // --------------------------------------------------------------------------------------------------------
  // KALK SIROVINE SAMO LJEPILA
  // --------------------------------------------------------------------------------------------------------
  $.each( arg_ulazi, function( u_ind, ulaz ) {
    
    var missing_glue_data = false;
    
    // preskoči ako je već bila greška 
    if (  
      missing_glue_data == false
      &&
      ulaz.klasa_sirovine && find_item( window.glue_classes, ulaz.klasa_sirovine.sifra, "sifra") 
    ) { // ovo su sva moguća ljepila
      
      // ako nema točaka ljepljenja neka bude uvijek 1 točka !!!
      if ( !ulaz.kalk_glue_points ) ulaz.kalk_glue_points = 1;
      
      if (
          ( !ulaz.kalk_glue_mm || !ulaz.kalk_glue_dizna || !ulaz.kalk_glue_m2 ) // mora imati duzinu ljepljenja / diznu / okvirno najveći komad kvadratno
          &&
          missing_glue_data == false // nemoj prokazati popup ako sam već prikazao tj prikaži samo kod prve greške
        ) {
        missing_glue_data = true;
        popup_warn(`Za izračun je potrebno unjeti:<br>dužinu ukupnog ljepljenja u mm,<br>veličinu dizne<br>i max. veličinu elementa.`);
        return;
      };
      
      if (
          ( !ulaz.kalk_sum_sirovina ) // mora biti upisano koliko ovih ljepljenja ima u ovom procesu
          &&
          missing_glue_data == false // nemoj prokazati popup ako sam već prikazao tj prikaži samo kod prve greške
        ) {
        missing_glue_data = true;
        popup_warn(`Za izračun je upisati koliko puta koristite svako pojedino ljepilo u ovom procesu (POLJE KOLIČINA).`);
        return;
      };
      
      
      // if ( ulaz.original_sirov?.osnovna_jedinica?.jedinica == "kg" ) {
      arg_ulazi[u_ind].kalk_kom_in_sirovina = norma_ljepila * ulaz.kalk_glue_mm/1000 * (ulaz.kalk_glue_dizna.num || 1); // ovo je u gramima
      // moram pretvoriti grame u kile
      // arg_ulazi[u_ind].kalk_kom_in_sirovina = ulaz.kalk_kom_in_sirovina / 1000;
      arg_ulazi[u_ind].real_unit_price = arg_ulazi[u_ind].kalk_kom_in_sirovina/1000 * arg_ulazi[u_ind].original_sirov.cijena;
      arg_ulazi[u_ind].kalk_price = arg_ulazi[u_ind].real_unit_price * sum_plus_extra( arg_ulazi[u_ind] );
      
    };

  });
  
  
  // --------------------------------------------------------------------------------------------------------
  // KALK SIROVINE SAMO SHELF LOCKOVI
  // --------------------------------------------------------------------------------------------------------
  $.each( arg_ulazi, function( u_ind, ulaz ) {
    
    var missing_elem_data = false;
    
    // preskoči ako je već bila greška 
    if (  
      missing_elem_data == false
      &&
      ulaz.klasa_sirovine?.sifra == "KS111" // shelf
    ) { 
    
      if (
          ( !ulaz.kalk_sum_sirovina || !ulaz.kalk_kom_in_sirovina || !ulaz.kalk_sum_sirovina == 1 ) // ne smije biti samo 1 jedini shelf
          &&
          missing_elem_data == false // nemoj prokazati popup ako sam već prikazao tj prikaži samo kod prve greške
        ) {
        missing_elem_data = true;
        popup_warn(`Upišite JED. KOLIČINU I KOLIČINU ZA SHELF LOCKOVE !!!`);
        return;
      };

      arg_ulazi[u_ind].real_unit_price = arg_ulazi[u_ind].kalk_kom_in_sirovina * arg_ulazi[u_ind].original_sirov.cijena;
      arg_ulazi[u_ind].kalk_price = arg_ulazi[u_ind].real_unit_price * sum_plus_extra(arg_ulazi[u_ind]);
      
      
    };

  });
  
  
  // --------------------------------------------------------------------------------------------------------
  // KALK VREMENA
  // --------------------------------------------------------------------------------------------------------
  $.each( arg_ulazi, function( u_ind, ulaz ) {
    
    // ako ulaz ima dužinu ljepljenja u milimetrima
    if ( ulaz.kalk_glue_mm ) {
      // ako ljepi 1 točku onda je samo * 1 ---> ako ima 2 točke ljep. onda je  1 + 1*0.1 = 1.1
      // ako je 3 točke onda je  ----> 1 + 3*0.1 = 1.3
      // dakle za svaku novu točku ljepljenja dodaj po 10 % extra
      var glue_points_time_extra = 1 + (ulaz.kalk_glue_points-1)*0.1;
      unit_time = unit_time * glue_points_time_extra * ulaz.kalk_glue_mm/1000; 
    };
    

    // za svaki shelf dodaj 5 sekundi
    if ( ulaz.klasa_sirovine?.sifra == "KS111" ) {
    
      unit_time = unit_time + ( 5/60 * ulaz.kalk_sum_sirovina );
    };
    
    // dodaj koeficijent za maximalnu veličinu elementa
    // na primjer ako je najveći element od svih elementa koje ljepim izmadju 1 - 1.5 m2 onda pomnoži vrijeme sa 1.1
    // TODO ----> potrebno naštimati koeficijente u suradnji s Brunom ----> nalaze se u vel_liste u prop glue_m2_list
    if ( ulaz.kalk_glue_m2 ) {
      unit_time = unit_time * ulaz.kalk_glue_m2.num;
    };
    
  });

  /*
    kalk_glue_mm: null,
    kalk_glue_points: null,
    kalk_glue_speed: null,
    kalk_glue_tlak: null,
    kalk_glue_dizna: null,
    kalk_glue_max_size: null,
  */
  
  var prep_time = glue_polica_prep_time * 60;
  var izlaz_kom = arg_extra_data.izlaz_kom.kom;
  var time = unit_time * izlaz_kom;
  
  var unit_price = (prep_time/60/izlaz_kom * glue_polica_prep_price) + ( unit_time/60 * radna_stn.price );
  var price = (prep_time/60 * glue_polica_prep_price) + ( unit_time/60 * izlaz_kom * radna_stn.price );
  
  var ulazi = arg_ulazi || this_module.gen_ulazi_from_izlazi(arg_ulazi);
  
  var ukupno_dobiveno_iz_ulaza = izlaz_kom;
  
  
  var arg_changed_props = {
    kalk_element: {
        
      sifra: 'elem'+cit_rand(),
      add_elem: null,
      elem_val: null,
      elem_kontra: null,

      elem_parent: null,

      elem_tip: { sifra: arg_radna_stn.elem_sifra, naziv: arg_radna_stn.elem_naziv, proces: arg_radna_stn.type, kalk: true },
      elem_opis: elem_opis,

      elem_cmyk: ``,
      elem_pantone: ``,
      elem_vrsta_boka: null,
      elem_plates: null,

      elem_on_product: 1,

      elem_RB: null,
      elem_polica_razmak: null,
      elem_polica_pregrade: null,
      elem_polica_nosivost: null,
      elem_front_orient: null,
      elem_plate_count: 1,
    },
  };
  
  // just_first znači da vrati samo prvi izlaz
  var izlazi = this_module.gen_izlazi( "just_first", arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, radna_stn, ukupno_dobiveno_iz_ulaza, null, null );

  var change_action_props = null;
  var action = arg_action || this_module.gen_action( change_action_props, curr_kalk, prep_time, work_time, unit_time, time, unit_price, price, radna_stn, arg_curr_proces_index );

  var proces = {
    extra_data: arg_extra_data || {},
    proc_koment: arg_proc_koment || "",
    row_sifra: `procesrow`+cit_rand(),
    ulazi: ulazi,
    action: action,
    izlazi: izlazi, 
  }; // kraj procesa

  return {
    prep_time,
    work_time,
    
    unit_time,
    time,
    unit_price,
    price,
    proces
  };

};
  

VELPROCES.piton_glue_work = function piton_glue_work( 
  this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index ) 
{

  
  /*  


  { 
    "sifra": "RADGLUEPITON", "naziv": "LJEPLJENJE - PITON", "func": "piton_glue_work",
    "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
    "elem_sifra": "SELDORADA", "elem_naziv": "Zaljepljeni elementi/proizvod",
    "unit": "kom", "interval": "h", "speed": 1, "price": 150,
  },


  */
  
  
  // TODO promjeniti kada mi BRUNO kaže prave veličine 
  // TODO promjeniti kada mi BRUNO kaže prave veličine 
  // TODO promjeniti kada mi BRUNO kaže prave veličine 
  // ------------------------------------------------------------------------------
  var norma_ljepila = 8; // 8 grama po metru  tj 1000 milimetara
  var unit_time = 20/60; // to je 20 sekundi po metru ----> odmah sam izrazio vrijeme u minutama
  
  // ------------------------------------------------------------------------------
  // TODO promjeniti kada mi BRUNO kaže prave veličine 
  // TODO promjeniti kada mi BRUNO kaže prave veličine 
  // TODO promjeniti kada mi BRUNO kaže prave veličine 


  var radna_stn = arg_radna_stn || find_item(window.cit_local_list.radne_stanice, `RADGLUEPITON`, `sifra`);
  var glue_polica_prep_time = find_item(window.cit_local_list.pro_const, `PCONSGLUEPREPTIME`, `sifra`).value; // u satima
  var glue_polica_prep_price = find_item(window.cit_local_list.pro_const, `PCONS4GLUEPREPPRICE`, `sifra`).value; // u 150 kuna
  
  var glue_count = 0;
  var shelf_count = 0;
  var element_count = 0;
  
  var elem_opis = ``;
  
  // samo brojim sve varijable koje trebam
  $.each( arg_ulazi, function( u_ind, ulaz ) {
    
    // { sifra: `TSIR11`, naziv: `POMOĆNI ELEMENT` },
    if ( ulaz.tip_sirovine?.sifra !== `TSIR11` ) {
      element_count += 1;
      elem_opis = `${ ulaz.kalk_element.elem_tip.short || ( "(" + ulaz.kalk_element.elem_opis + ")" ) } + ` + elem_opis;
    };

    if ( ulaz.klasa_sirovine?.sifra == "KS111" ) { // ovo je shelf lock
      shelf_count += 1
      elem_opis = `LOCK + ` + elem_opis;
    };
    
    if ( ulaz.klasa_sirovine && find_item( window.glue_classes, ulaz.klasa_sirovine.sifra, "sifra") ) { // ovo su sva moguća ljepila
      glue_count += 1;
    };
  
  });
  
  // ako ima bilo što u elem opisu ----> dodaj na početak na primjer P3-1 PIŠTOLJ LJEP.
  if ( elem_opis ) elem_opis = `P${ (arg_curr_proces_index+1) + `-1` } IZLAZ PIŠTOLJ // `+ elem_opis;

  // obriši zadnja dva stringa tj + znak na kraju
  elem_opis = elem_opis.slice(0, -2);
  
  
  if ( glue_count == 0 ) {
    popup_warn(`Za ovaj proces je potrebno barem jedno ljepilo !!!!`);
    return;
  };
  
  if ( element_count < 2 )  {
    popup_warn(`Za ovaj proces potrebno je dodati minimalno 2 elementa koja treba zaljepiti !!!`);
    return;
  };
  
  
  if ( !arg_extra_data?.izlaz_kom ) {
    popup_warn(`Potrebno upisati koliko komada izlazi nakon ljepljenja !!!`);
    return;
  };
  
  
  // --------------------------------------------------------------------------------------------------------
  // KALK SIROVINE SAMO LJEPILA
  // --------------------------------------------------------------------------------------------------------
  $.each( arg_ulazi, function( u_ind, ulaz ) {
    
    var missing_glue_data = false;
    
    // preskoči ako je već bila greška 
    if (  
      missing_glue_data == false
      &&
      ulaz.klasa_sirovine && find_item( window.glue_classes, ulaz.klasa_sirovine.sifra, "sifra") 
    ) { // ovo su sva moguća ljepila
      
      // ako nema točaka ljepljenja neka bude uvijek 1 točka !!!
      if ( !ulaz.kalk_glue_points ) ulaz.kalk_glue_points = 1;
      
      if (
          ( !ulaz.kalk_glue_mm || !ulaz.kalk_glue_dizna || !ulaz.kalk_glue_m2 ) // mora imati duzinu ljepljenja / diznu / okvirno najveći komad kvadratno
          &&
          missing_glue_data == false // nemoj prokazati popup ako sam već prikazao tj prikaži samo kod prve greške
        ) {
        missing_glue_data = true;
        popup_warn(`Za izračun je potrebno unjeti:<br>dužinu ukupnog ljepljenja u mm,<br>veličinu dizne<br>i max. veličinu elementa.`);
        return;
      };
      
      if (
          ( !ulaz.kalk_sum_sirovina ) // mora biti upisano koliko ovih ljepljenja ima u ovom procesu
          &&
          missing_glue_data == false // nemoj prokazati popup ako sam već prikazao tj prikaži samo kod prve greške
        ) {
        missing_glue_data = true;
        popup_warn(`Za izračun je upisati koliko puta koristite svako pojedino ljepilo u ovom procesu (POLJE KOLIČINA).`);
        return;
      };
      
      
      // if ( ulaz.original_sirov?.osnovna_jedinica?.jedinica == "kg" ) {
      arg_ulazi[u_ind].kalk_kom_in_sirovina = norma_ljepila/1000 * ulaz.kalk_glue_mm * (ulaz.kalk_glue_dizna.num || 1); // ovo je gramima
      // moram pretvoriti grame u kile
      arg_ulazi[u_ind].kalk_kom_in_sirovina = ulaz.kalk_kom_in_sirovina / 1000;
      arg_ulazi[u_ind].real_unit_price = arg_ulazi[u_ind].kalk_kom_in_sirovina * arg_ulazi[u_ind].original_sirov.cijena;
      arg_ulazi[u_ind].kalk_price = arg_ulazi[u_ind].real_unit_price * sum_plus_extra(arg_ulazi[u_ind]);
      // };
      
    };

  });
  
  
  // --------------------------------------------------------------------------------------------------------
  // KALK SIROVINE SAMO LOCKOVI
  // --------------------------------------------------------------------------------------------------------
  $.each( arg_ulazi, function( u_ind, ulaz ) {
    
    var missing_elem_data = false;
    
    // preskoči ako je već bila greška 
    if (  
      missing_elem_data == false
      &&
      ulaz.klasa_sirovine?.sifra == "KS111" // shelf
    ) { 
    
      if (
          ( !ulaz.kalk_sum_sirovina || !ulaz.kalk_kom_in_sirovina || !ulaz.kalk_sum_sirovina == 1 ) // ne smije biti samo 1 jedini shelf
          &&
          missing_elem_data == false // nemoj prokazati popup ako sam već prikazao tj prikaži samo kod prve greške
        ) {
        missing_elem_data = true;
        popup_warn(`Upišite JED. KOLIČINU I KOLIČINU ZA SHELF LOCKOVE !!!`);
        return;
      };

      arg_ulazi[u_ind].real_unit_price = arg_ulazi[u_ind].kalk_kom_in_sirovina * arg_ulazi[u_ind].original_sirov.cijena;
      arg_ulazi[u_ind].kalk_price = arg_ulazi[u_ind].real_unit_price * sum_plus_extra(arg_ulazi[u_ind]);
      // };
      
    };

  });
  
  
  // --------------------------------------------------------------------------------------------------------
  // KALK VREMENA
  // --------------------------------------------------------------------------------------------------------
  
  
  $.each( arg_ulazi, function( u_ind, ulaz ) {
    
    // ako ulaz ima dužinu ljepljenja u milimetrima
    if ( ulaz.kalk_glue_mm ) {
      // ako ljepi 1 točku onda je samo * 1 ---> ako ima 2 točke ljep. onda je  1 + 1*0.1 = 1.1
      // ako je 3 točke onda je  ----> 1 + 3*0.1 = 1.3
      // dakle za svaku novu točku ljepljenja dodaj po 10 % extra
      var glue_points_time_extra = 1 + (ulaz.kalk_glue_points-1)*0.1;
      unit_time = unit_time * glue_points_time_extra * ulaz.kalk_glue_mm/1000; 
    };
    
    // za svaki shelf dodaj 5 sekundi
    if ( ulaz.klasa_sirovine?.sifra == "KS111" ) {
      unit_time = unit_time + ( 5/60 * ulaz.kalk_sum_sirovina );
    };
    
    // dodaj koeficijent za maximalnu veličinu elementa
    // na primjer ako je najveći element od svih elementa koje ljepim izmadju 1 - 1.5 m2 onda pomnoži vrijeme sa 1.1
    // TODO ----> potrebno naštimati koeficijente u suradnji s Brunom ----> nalaze se u vel_liste u prop glue_m2_list
    if ( ulaz.kalk_glue_m2 ) {
      unit_time = unit_time * ulaz.kalk_glue_m2.num;
    };
    
  });

  /*
    kalk_glue_mm: null,
    kalk_glue_points: null,
    kalk_glue_speed: null,
    kalk_glue_tlak: null,
    kalk_glue_dizna: null,
    kalk_glue_max_size: null,
  */
  
  var prep_time = glue_polica_prep_time * 60;
  var izlaz_kom = arg_extra_data.izlaz_kom.kom;
  var time = unit_time * izlaz_kom;
  
  var unit_price = (prep_time/60/izlaz_kom * glue_polica_prep_price) + ( unit_time/60 * radna_stn.price );
  var price = (prep_time/60 * glue_polica_prep_price) + ( unit_time/60 * izlaz_kom * radna_stn.price );
  
  var ulazi = arg_ulazi || this_module.gen_ulazi_from_izlazi(arg_ulazi);
  
  var ukupno_dobiveno_iz_ulaza = izlaz_kom;
  
  
  var arg_changed_props = {
    kalk_element: {
        
      sifra: 'elem'+cit_rand(),
      add_elem: null,
      elem_val: null,
      elem_kontra: null,

      elem_parent: null,

      elem_tip: { sifra: arg_radna_stn.elem_sifra, naziv: arg_radna_stn.elem_naziv, proces: arg_radna_stn.type, kalk: true },
      elem_opis: elem_opis,

      elem_cmyk: ``,
      elem_pantone: ``,
      elem_vrsta_boka: null,
      elem_plates: null,

      elem_on_product: 1,

      elem_RB: null,
      elem_polica_razmak: null,
      elem_polica_pregrade: null,
      elem_polica_nosivost: null,
      elem_front_orient: null,
      elem_plate_count: 1,
    },
  };
  // combine znači da vrati samo prvi izlaz
  var izlazi = this_module.gen_izlazi( "just_first", arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, radna_stn, ukupno_dobiveno_iz_ulaza, null, null );

  var change_action_props = null;
  var action = arg_action || this_module.gen_action( change_action_props, curr_kalk, prep_time, work_time, unit_time, time, unit_price, price, radna_stn, arg_curr_proces_index );

  var proces = {
    extra_data: arg_extra_data || {},
    proc_koment: arg_proc_koment || "",
    row_sifra: `procesrow`+cit_rand(),
    ulazi: ulazi,
    action: action,
    izlazi: izlazi, 
  }; // kraj procesa

  return {
    prep_time,
    work_time,
    
    unit_time,
    time,
    unit_price,
    price,
    proces
  };

};
  
/*
-------------------------------------------------------
OCTO JE ZAPRAVO ISTO KAO PITON !!!! (samo ima 4 crijeva, apiton ima sam o dva crijeva )
-------------------------------------------------------

*/

VELPROCES.octo_glue_work = VELPROCES.piton_glue_work;


VELPROCES.cnc_glue_work = function cnc_glue_work( 
  this_module, naklada, arg_ulazi, arg_izlazi, arg_action, arg_radna_stn, product_data, curr_kalk, arg_proc_koment, arg_extra_data, arg_curr_proces_index ) 
{

  
  /*  


{ 
  "sifra": "RADGLUECNC", "naziv": "LJEPLJENJE - CNC STROJ", "func": "cnc_glue_work",
  "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
  "elem_sifra": "SELDORADA", "elem_naziv": "Zaljepljeni elementi/proizvod",
  "unit": "kom", "interval": "h", "speed": 1, "price": 150,
},

  */
  
  
  // TODO promjeniti kada mi BRUNO kaže prave veličine 
  // TODO promjeniti kada mi BRUNO kaže prave veličine 
  // TODO promjeniti kada mi BRUNO kaže prave veličine 
  // ------------------------------------------------------------------------------
  var norma_ljepila = 1; // 8 grama po metru  tj 1000 milimetara
  
  
  
  
  
  
  var unit_time = null;
  
  // ------------------------------------------------------------------------------
  // TODO promjeniti kada mi BRUNO kaže prave veličine 
  // TODO promjeniti kada mi BRUNO kaže prave veličine 
  // TODO promjeniti kada mi BRUNO kaže prave veličine 


  var radna_stn = arg_radna_stn || find_item(window.cit_local_list.radne_stanice, `RADGLUEPITON`, `sifra`);
  var glue_polica_prep_time = find_item(window.cit_local_list.pro_const, `PCONSGLUEPREPTIME`, `sifra`).value; // u satima
  var glue_polica_prep_price = find_item(window.cit_local_list.pro_const, `PCONS4GLUEPREPPRICE`, `sifra`).value; // u 150 kuna
  
  var glue_count = 0;
  var shelf_count = 0;
  var element_count = 0;
  
  var elem_opis = ``;
  
  // samo brojim sve varijable koje trebam
  $.each( arg_ulazi, function( u_ind, ulaz ) {
    
    // { sifra: `TSIR11`, naziv: `POMOĆNI ELEMENT` },
    if ( ulaz.tip_sirovine?.sifra !== `TSIR11` ) {
      element_count += 1;
      elem_opis = `${ ulaz.kalk_element.elem_tip.short || ( "(" + ulaz.kalk_element.elem_opis + ")" ) } + ` + elem_opis;
    };

    if ( ulaz.klasa_sirovine?.sifra == "KS111" ) { // ovo je shelf lock
      shelf_count += 1
      elem_opis = `LOCK + ` + elem_opis;
    };
    
    if ( ulaz.klasa_sirovine && find_item( window.glue_classes, ulaz.klasa_sirovine.sifra, "sifra") ) { // ovo su sva moguća ljepila
      glue_count += 1;
    };
  
  });
  
  // ako ima bilo što u elem opisu ----> dodaj na početak na primjer P3-1 PIŠTOLJ LJEP.
  if ( elem_opis ) elem_opis = `P${ (arg_curr_proces_index+1) + `-1` } IZLAZ PIŠTOLJ // `+ elem_opis;

  // obriši zadnja dva stringa tj + znak na kraju
  elem_opis = elem_opis.slice(0, -2);
  
  
  if ( glue_count == 0 ) {
    popup_warn(`Za ovaj proces je potrebno barem jedno ljepilo !!!!`);
    return;
  };
  
  if ( element_count < 2 )  {
    popup_warn(`Za ovaj proces potrebno je dodati minimalno 2 elementa koja treba zaljepiti !!!`);
    return;
  };
  
  
  if ( !arg_extra_data?.izlaz_kom ) {
    popup_warn(`Potrebno upisati koliko komada izlazi nakon ljepljenja !!!`);
    return;
  };

  
  // --------------------------------------------------------------------------------------------------------
  // KALK SIROVINE SAMO LJEPILA
  // --------------------------------------------------------------------------------------------------------
  $.each( arg_ulazi, function( u_ind, ulaz ) {
    
    var missing_glue_data = false;
    
    // preskoči ako je već bila greška 
    if (  
      missing_glue_data == false
      &&
      ulaz.klasa_sirovine && find_item( window.glue_classes, ulaz.klasa_sirovine.sifra, "sifra") // ovo su sva moguća ljepila 
    ) { 
      
      // ako nema točaka ljepljenja neka bude uvijek 1 točka !!!
      if ( !ulaz.kalk_glue_points ) ulaz.kalk_glue_points = 1;
      
      if (
          ( !ulaz.kalk_glue_mm || !ulaz.kalk_glue_dizna || !ulaz.kalk_glue_m2 || !ulaz.kalk_glue_tlak  || !ulaz.kalk_glue_speed ) 
          &&
          missing_glue_data == false // nemoj prokazati popup ako sam već prikazao tj prikaži samo kod prve greške
        ) {
        missing_glue_data = true;
        popup_warn(`Za izračun je potrebno unjeti:<br>dužinu ukupnog ljepljenja u mm,<br>veličinu dizne<br>max. veličinu elementa<br>tlak na CNC stroju<br> i BRZINU ljepljenja.`);
        return;
        
      } else {
        
        unit_time += (ulaz.kalk_glue_mm / ulaz.kalk_glue_speed) / 60;
        
        
      };
      
      
      
      if (
          ( !ulaz.kalk_sum_sirovina ) // mora biti upisano koliko ovih ljepljenja ima u ovom procesu
          &&
          missing_glue_data == false // nemoj prokazati popup ako sam već prikazao tj prikaži samo kod prve greške
        ) {
        missing_glue_data = true;
        popup_warn(`Za izračun je upisati koliko puta koristite svako pojedino ljepilo u ovom procesu (POLJE KOLIČINA).`);
        return;
      };
      
      
      // if ( ulaz.original_sirov?.osnovna_jedinica?.jedinica == "kg" ) {
      arg_ulazi[u_ind].kalk_kom_in_sirovina = 
        norma_ljepila/1000 * ulaz.kalk_glue_mm * (ulaz.kalk_glue_dizna.num || 1) * (ulaz.kalk_glue_tlak || 1) / (ulaz.kalk_glue_speed || 1); // ovo je gramima
      
      
      // moram pretvoriti grame u kile
      arg_ulazi[u_ind].kalk_kom_in_sirovina = ulaz.kalk_kom_in_sirovina / 1000;
      arg_ulazi[u_ind].real_unit_price = arg_ulazi[u_ind].kalk_kom_in_sirovina * arg_ulazi[u_ind].original_sirov.cijena;
      arg_ulazi[u_ind].kalk_price = arg_ulazi[u_ind].real_unit_price * sum_plus_extra(arg_ulazi[u_ind]);
      // };
      
    };

  });
  
  
  // --------------------------------------------------------------------------------------------------------
  // KALK SIROVINE SAMO LOCKOVI
  // --------------------------------------------------------------------------------------------------------
  $.each( arg_ulazi, function( u_ind, ulaz ) {
    
    var missing_elem_data = false;
    
    // preskoči ako je već bila greška 
    if (  
      missing_elem_data == false
      &&
      ulaz.klasa_sirovine?.sifra == "KS111" // shelf
    ) { 
    
      if (
          ( !ulaz.kalk_sum_sirovina || !ulaz.kalk_kom_in_sirovina || !ulaz.kalk_sum_sirovina == 1 ) // ne smije biti samo 1 jedini shelf
          &&
          missing_elem_data == false // nemoj prokazati popup ako sam već prikazao tj prikaži samo kod prve greške
        ) {
        missing_elem_data = true;
        popup_warn(`Upišite JED. KOLIČINU I KOLIČINU ZA SHELF LOCKOVE !!!`);
        return;
      };

      arg_ulazi[u_ind].real_unit_price = arg_ulazi[u_ind].kalk_kom_in_sirovina * arg_ulazi[u_ind].original_sirov.cijena;
      arg_ulazi[u_ind].kalk_price = arg_ulazi[u_ind].real_unit_price * sum_plus_extra(arg_ulazi[u_ind]);
      
    };

  });
  
  
  // --------------------------------------------------------------------------------------------------------
  // KALK VREMENA
  // --------------------------------------------------------------------------------------------------------
  
  
  $.each( arg_ulazi, function( u_ind, ulaz ) {
    
    // ako ulaz ima dužinu ljepljenja u milimetrima
    if ( ulaz.kalk_glue_mm ) {
      // ako ljepi 1 točku onda je samo * 1 ---> ako ima 2 točke ljep. onda je  1 + 1*0.1 = 1.1
      // ako je 3 točke onda je  ----> 1 + 3*0.1 = 1.3
      // dakle za svaku novu točku ljepljenja dodaj po 10 % extra
      var glue_points_time_extra = 1 + (ulaz.kalk_glue_points-1)*0.1;
      unit_time = unit_time * glue_points_time_extra * ulaz.kalk_glue_mm/1000; 
    };
    

    // za svaki shelf dodaj 5 sekundi
    if ( ulaz.klasa_sirovine?.sifra == "KS111" ) {
    
      unit_time = unit_time + ( 5/60 * ulaz.kalk_sum_sirovina );
    };
    
    // dodaj koeficijent za maximalnu veličinu elementa
    // na primjer ako je najveći element od svih elementa koje ljepim izmadju 1 - 1.5 m2 onda pomnoži vrijeme sa 1.1
    // TODO ----> potrebno naštimati koeficijente u suradnji s Brunom ----> nalaze se u vel_liste u prop glue_m2_list
    if ( ulaz.kalk_glue_m2 ) {
      unit_time = unit_time * ulaz.kalk_glue_m2.num;
    };
    
  });

  /*
    kalk_glue_mm: null,
    kalk_glue_points: null,
    kalk_glue_speed: null,
    kalk_glue_tlak: null,
    kalk_glue_dizna: null,
    kalk_glue_max_size: null,
  */
  
  var prep_time = glue_polica_prep_time * 60;
  var izlaz_kom = arg_extra_data.izlaz_kom.kom;
  var time = unit_time * izlaz_kom;
  
  var unit_price = (prep_time/60/izlaz_kom * glue_polica_prep_price) + ( unit_time/60 * radna_stn.price );
  var price = (prep_time/60 * glue_polica_prep_price) + ( unit_time/60 * izlaz_kom * radna_stn.price );
  
  var ulazi = arg_ulazi || this_module.gen_ulazi_from_izlazi(arg_ulazi);
  
  var ukupno_dobiveno_iz_ulaza = izlaz_kom;
  
  
  var arg_changed_props = {
    kalk_element: {
        
      sifra: 'elem'+cit_rand(),
      add_elem: null,
      elem_val: null,
      elem_kontra: null,

      elem_parent: null,

      elem_tip: { sifra: arg_radna_stn.elem_sifra, naziv: arg_radna_stn.elem_naziv, proces: arg_radna_stn.type, kalk: true },
      elem_opis: elem_opis,

      elem_cmyk: ``,
      elem_pantone: ``,
      elem_vrsta_boka: null,
      elem_plates: null,

      elem_on_product: 1,

      elem_RB: null,
      elem_polica_razmak: null,
      elem_polica_pregrade: null,
      elem_polica_nosivost: null,
      elem_front_orient: null,
      elem_plate_count: 1,
    },
  };
  // combine znači da vrati samo prvi izlaz
  var izlazi = this_module.gen_izlazi( "just_first", arg_changed_props, arg_curr_proces_index, curr_kalk, ulazi, radna_stn, ukupno_dobiveno_iz_ulaza, null, null );

  var change_action_props = null;
  var action = arg_action || this_module.gen_action( change_action_props, curr_kalk, prep_time, work_time, unit_time, time, unit_price, price, radna_stn, arg_curr_proces_index );

  var proces = {
    extra_data: arg_extra_data || {},
    proc_koment: arg_proc_koment || "",
    row_sifra: `procesrow`+cit_rand(),
    ulazi: ulazi,
    action: action,
    izlazi: izlazi, 
  }; // kraj procesa

  return {
    prep_time,
    work_time,
    
    unit_time,
    time,
    unit_price,
    price,
    proces
  };

};
 






