var module_object = {
  
create: function( data, parent_element, placement ) {

  
  var { id } = data;
  
  var dummy_sklad = [
    
    {
      
      sifra: "sklad34354354354354",
      mat: { "sifra": "MAT2", "naziv": "2T/B standard 329g" },
      naziv_sirovine: "Ljepenka",
      val: 2060,
      kontra_val: 1550,
      kontra_tok: null,
      tok: null,
      pozicija: "P2-S3-L1",
      sklad_count: 2100,
      count: 2100 - 150,  // ako imam offer i order ----> onda zanemarim offer rezervaciju i samo upisujem order rezervaciju
      time: ( Date.now() - 1000*60*60*24*6 ),
      last_state: "Toni Kutlić / Narudžba Dobavljača",
      
      history: [
        
        {
          ss: 2564,
          storno: true,
          sifra: `sklhis`+cit_rand(),
          user: {
            "user_number" : 58,
            "full_name" : "Šaravanja Martina",
            "email" : "martina@velprom.hr"
          },
          type: { "sifra": "SKT1", "naziv": "Narudžba dobavljača" },
          time: Date.now() - (1000*60*60*24*3),
          doc_sifra: `ND 8569`,
          komentar: `Naručila sam ljepenku`,
          count: 10000,
          est_date: Date.now() + (1000*60*60*24*3),
        },
        {
          ss: 5960,
          storno: 2564,
          sifra: `sklhis`+cit_rand(),
          user: {
            "user_number" : 58,
            "full_name" : "Pero Perić",
            "email" : "martina@velprom.hr"
          },
          type: { "sifra": "SKT5", "naziv": "Utrošak" },
          time: Date.now() - (1000*60*60*24*3),
          doc_sifra: `RN 8569`,
          komentar: `Potrošeno je`,
          count: 59,
          est_date: Date.now(),
        }
        
      ]
      
    },
    {
      
      sifra: "sklad"+cit_rand(),
      mat: { "sifra": "MAT9", "naziv": "2BŠ/BC" },
      naziv_sirovine: "Papir",
      val: null,
      kontra_val: null,
      kontra_tok: 700,
      tok: 500,
      pozicija: "S1-S20-L1",
      sklad_count: 250,
      count: 20,  // ako imam offer i order ----> onda zanemarim offer rezervaciju i samo upisujem order rezervaciju
      time: ( Date.now() - 1000*60*60*24*5 ),
      last_state: "Renato Glibo / Utrošak",
      
      history: [
        
        {
          ss: 5863,
          storno: null,
          sifra: `sklhis`+cit_rand(),
          user: {
            "user_number" : 58,
            "full_name" : "Šaravanja Martina",
            "email" : "martina@velprom.hr"
          },
          type: { "sifra": "SKT5", "naziv": "Utrošak" },
          time: Date.now() - 1000*60*60*24*5,
          doc_sifra: `RN 11524`,
          komentar: `Potrošio sam papir`,
          count: 300,
          est_date: null,
        }
        
      ]
      
    },
    
  ];
  
  
  data = { ...data, elements: dummy_sklad }
  
  
  this_module.set_init_data(data);
  var rand_id = `sklad`+cit_rand();
  this_module.cit_data[id].rand_id = rand_id;
  
  
  var component_id = data.id;

  var w = window;


  var component_html =
`

<div id="${data.id}" class="cit_comp">
  <section>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="sklad_table_box">

              <!-- OVDJE IDE LISTA SKLADIŠTA -->    

        </div> 
      
      </div>
    </div>
    
  </section>
  
</div>

`;


  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };


  wait_for( `$('#${data.id}').length > 0`, async function() {
    
    
    toggle_global_progress_bar(false);
    
    data.elements = [];
    
    var all_sklads = await this_module.get_all_sklads();
    
    if ( all_sklads ) data.elements = all_sklads;
    
    this_module.make_sklad_list(data);
    
    
    $('#'+rand_id+'_sklad_date').data(`cit_run`, this_module.gen_sklad_date );
    
    

    $(`#close_cit_preview`).off(`click`);
    $(`#close_cit_preview`).on(`click`, function(e) {
      
      e.preventDefault();
      e.stopPropagation();
      
      $(`#cit_preview_modal`).removeClass(`cit_show`);
      
    });
    
    
    $(`#print_cit_preview`).off(`click`);
    $(`#print_cit_preview`).on(`click`, function(e) {
      
      e.preventDefault();
      e.stopPropagation();
      
      var printBOX = document.getElementById("printiFrame").contentWindow;
      
      printBOX.focus();
      printBOX.print(); 
      
    });
    

    $(`#gen_pdf`).off(`click`);
    $(`#gen_pdf`).on(`click`, async function(e) {
      
      e.preventDefault();
      e.stopPropagation();
      

      var this_button = this;
      var pop_text = `Jeste li sigurni da želite kreirati novu Narudžbu dobavljača?`;

      function delete_yes() {

        this_module.save_pdf();

      };

      function delete_no() {
        show_popup_modal(false, pop_text, null );
      };

      var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
      var show_popup_modal = pop_mod.show_popup_modal;
      show_popup_modal(`warning`, pop_text, null, 'yes/no', delete_yes, delete_no, null);
      
      
      
    });
    
    
  }, 5*1000 );

  

  return {
    html: component_html,
    id: component_id
  } 
  


},
scripts: function () {
  
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};

  
  function create_valid() {
  
    var valid = {

      sklad_type: { element: "single_select", type: "simple", lock: false, visible: true, label: "Vrsta Zapisa" }, 
      sklad_est_date: { element: "simple_calendar", type: "date_time", lock: false, visible: true, "label": "Procjena Datuma" },
      doc_sifra: { "element": "input", "type": "string", "lock": false, "visible": true, "label": "Šifra Povezanog dokumenta" },
      sklad_komentar: { "element": "text_area", "type": "string", "lock": false, "visible": true, "label": "Kratki Opis" },
      
    };
    
    
    this_module.valid = valid;
  
  };
  
  create_valid();
  

  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
  };
  this_module.set_init_data = set_init_data;

  

  
  function get_all_sklads() {

    
    toggle_global_progress_bar(true);
    
    return new Promise( function(resolve, reject) {
      

      $.ajax({
        headers: {
          'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
        },
        type: "POST",
        cache: false,
        url: `/all_sklads`,
        data: JSON.stringify( {} ),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done(function (result) {

        console.log(result);
        if ( result.success == true ) {
          
          resolve(result.data);
          
        } else {
          if ( result.msg ) popup_error(result.msg);
          if ( !result.msg ) popup_error(`Greška prilikom dobivanja podataka skladišta!`);
          resolve(null)
        };

      })
      .fail(function (error) {

        console.log(error);
        popup_error(`Došlo je do greške na serveru prilikom dobivanja podataka iz skladišta!`);
        resolve(null);
      })
      .always(function() {

        toggle_global_progress_bar(false);

      });
      
      
    }); // kraj promisa

  }  
  this_module.get_all_sklads = get_all_sklads;


  function make_sklad_list(data) {

    // ADRESE TABLICA

    var sklad_za_table = [];
    if ( data.elements && data.elements.length > 0 ) {

      $.each(data.elements, function(index, sklad_obj ) {

        var {
        sifra,
        mat,
        naziv_sirovine,
        val,
        kontra_val,
        kontra_tok,
        tok,
        pozicija,
        sklad_count,
        count,
        time,
        last_state,
        history,

        } = sklad_obj;


        var mat_naziv = mat.naziv;
        
        
        sklad_za_table.push({ 
          sifra,
          mat,
          mat_naziv,
          naziv_sirovine,
          val,
          kontra_val,
          kontra_tok,
          tok,
          pozicija,
          sklad_count,
          count,
          time,
          last_state,
          history
        });

      }); // kraj loopa svih skladova

    };
    var sklad_props = {

      desc: 'samo za kreiranje tablice svih sirovina u skladištu u SKLAD MODULU',
      local: true,

      list: sklad_za_table,

      show_cols: [
        "mat_naziv",
        "naziv_sirovine",
        "val",
        "kontra_val",

        "pozicija",

        "kontra_tok",
        "tok",

        "sklad_count",
        "count",
        "time",
        "last_state",
      ],
      col_widths: [

        2, // "mat_naziv",
        4, // "naziv_sirovine",
        1, // "val",
        1, // "kontra_val",

        2, // "pozicija",

        1, // "kontra_tok",
        1, // "tok",

        1, // "sklad_count",
        1, // "count",
        3, // "time",
        3, // "last_state",

      ],
      custom_headers: [

        "MATER.", // "mat",
        "NAZIV", // "naziv_sirovine",

        "VAL", // "val",
        "K. VAL", // "kontra_val",

        "POZICIJA", // "pozicija",

        "K. TOK", // "kontra_tok",
        "TOK", // "tok",

        "REAL S.", // "sklad_count",
        "KALK S.", // "count",
        "ZADNJE", // "time",
        "TKO/ŠTO", // "last_state",

      ],

      format_cols: {
        mat_naziv: `center`,
        time: "date_time",
        start: "date_time",
        end: "date_time",

        val: 0,
        kontra_val: 0,

        kontra_tok: 0,
        tok: 0,

        pozicija: `right`,

        sklad_count: 0,
        count: 0,

      },

      parent: "#sklad_table_box",
      return: {},
      show_on_click: false,
      
      
      cit_run: this_module.gen_sklad_history,

    };
    if (sklad_za_table.length > 0 ) create_cit_result_list(sklad_za_table, sklad_props );



  };
  this_module.make_sklad_list = make_sklad_list;


  function gen_sklad_history(state) {
    
    
    
    
    var this_comp_id = clicked_result_red.closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    var valid = this_module.valid;
    
    
    var history = state.history;



    var sklad_history_html =

`
<div id="sklad_history_box" class="container-fluid" >

  <div class="row">

    <div class="col-md-2 col-sm-12">
      ${ cit_comp(rand_id+`_sklad_type`, valid.sklad_type, null, "") }
    </div>

    <div class="col-md-2 col-sm-12" id="sklad_est_date_box">
      ${ cit_comp(rand_id+`_sklad_est_date`, valid.sklad_est_date, null, "") }
    </div>

    <div class="col-md-2 col-sm-12">
     ${ cit_comp(rand_id+`_doc_sifra`, valid.doc_sifra, "") }
    </div>

    <div class="col-md-5 col-sm-12">
     ${ cit_comp(rand_id+`_sklad_komentar`, valid.sklad_komentar, "", null, `min-height: 80px;` ) }
    </div>
    
    
    <div class="col-md-1 col-sm-12">
     
        <button class="blue_btn btn_small_text" id="save_curr_history" style="margin: 20px 0 0 auto; box-shadow: none;">
          SPREMI 
        </button> 

    </div>
    
      
  </div> 
  

  <div class="row">
    <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" id="history_list_box">
      <!-- OVDJE IDE LISTA HISTORY LISTA OD SPECIFIČNE SIROVINE -->    
    </div>
  </div>  

  <div class="row">
    <div class="col-md-12 col-sm-12">
    
      <div class="cit_graf_box">
        
        <div class="cit_graf">

          <div class="cit_graf_top">  

            <div class="ver_scale">
              <div class="ver_scale_item">100</div>
              <div class="ver_scale_item">200</div>
              <div class="ver_scale_item">300</div>

            </div>
            <div class="cit_graf_body">

            </div>

          </div>
          <div class="hor_scale">
          <div class="hor_scale_item">15.11.</div>
          <div class="hor_scale_item">15.11.</div>
          
          </div>

        </div>
      </div>
    </div>
  </div>
  
</div>
`;      

    
    $(`#sklad_history_box`).remove();
    
    clicked_result_red.after(sklad_history_html);
    
    
    setTimeout( function() { this_module.make_history_list(state); }, 100 );
    
    
    
    wait_for(`$("#sklad_history_box").length > 0`, function() {
      
      
      
      $(`#save_curr_history`).off('click');
      $(`#save_curr_history`).on('click', this_module.save_current_history );

      
      $('#'+rand_id+'_sklad_est_date').data(`cit_run`, this_module.choose_sklad_est_date );

      $(`#`+rand_id+`_sklad_type`).data('cit_props', {
        desc: 'odabir banke za upisivanje ibana',
        local: true,
        show_cols: ['naziv'],
        return: {},
        show_on_click: true,
        list: 'sklad_action',
        cit_run: function(state) {
          
          
              
/* 
{ "sifra": "SKT1", "naziv": "Narudžba dobavljača" },
{ "sifra": "SKT2", "naziv": "Primka" },
{ "sifra": "SKT3", "naziv": "Ponuda" },
{ "sifra": "SKT4", "naziv": "Narudžba kupca" },
{ "sifra": "SKT5", "naziv": "Utrošak" },
{ "sifra": "SKT6", "naziv": "Škart" },
{ "sifra": "SKT7", "naziv": "Među-skladišnica" },
*/
    

          console.log(state);

          var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          
          if ( !data.current_sklad_history ) data.current_sklad_history = {};
          

          if ( state == null ) {
            $('#'+current_input_id).val(``);
            data.current_sklad_history.type = null;
            return;
          };
          
                    
          if (
            state.sifra == `SKT1` // ND
            ||
            state.sifra == `SKT3` // PONUDA
            ||
            state.sifra == `SKT4` // PONUDA
          
          ) {
            
            $(`#sklad_est_date_box`).css(`display`, `block`);
            
          } else {
            $(`#sklad_est_date_box`).css(`display`, `none`);
            
          };
          
          $('#'+current_input_id).val(state.naziv);
          data.current_sklad_history.type = state;

        },
        
      });

    
    }, 5*1000); 
    
    
    
    
    

  };
  this_module.gen_sklad_history = gen_sklad_history;  

  
  
  
  function make_history_list(sklad) {

    // ADRESE TABLICA

    var history_za_table = [];
    if ( sklad.history && sklad.history.length > 0 ) {

      $.each(sklad.history, function(index, history_obj ) {

      var {
        storno,
        ss,
        sifra,
        user,
        type,
        time,
        doc_sifra,
        komentar,
        count,
        est_date,
        } = history_obj;


        var type_naziv = type.naziv;
        var user_full_name = user.full_name
        var ss_text = `SS`+ ss;
        var btn_disable = "";
        
        
       
        
        var storno_btn =  
        `<button class="blue_btn btn_small_text" style="margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;">
          <i class="fas fa-minus-circle" style="margin-right: 5px;" ></i> STORNO
        </button>`;
       
        if ( storno ) storno_btn = null;
        var storno_ss = storno !== true ? `SS` + storno : "STORNIRANO";
        if ( storno == null ) storno_ss = null;
        
        history_za_table.push({ 
          storno,
          storno_ss,
          ss_text,
          ss,
          sifra,
          type_naziv,
          user_full_name,
          user,
          type,
          time,
          doc_sifra,
          komentar,
          count,
          est_date,
          storno_btn,
        });

      }); // kraj loopa svih historija

    };
    var history_props = {

      desc: 'samo za kreiranje tablice svih historija od jedne sirovine u SKLAD MODULU',
      local: true,

      list: history_za_table,

      show_cols: [
        
        "ss_text",
        "est_date",
        "type_naziv",
        "count",
        "user_full_name",
        "time",
        "doc_sifra",
        "komentar",
        "storno_ss",
        "storno_btn",
       
      ],
      col_widths: [
        1, // "ss_text",
        2, // "est_date",
        3, // "type_naziv",
        1, // "count",
        2, // "user_full_name",
        2, // "time",
        1, // "doc_sifra",
        3, // "komentar",
        1, // "storno_ss",
        2, // "storno_btn",
        

      ],
      custom_headers: [
        
        "SS",
        "Procjenjeno vrijeme", // "est_date",
        "Vrsta Zapisa", // "type_naziv",
        "Koliko", // "count",
        "Tko", // "user_full_name",
        "Kada", // "time",
        "Po dokumentu", // "doc_sifra",
        "Komentar", // "komentar",
        "Storno od", // "storno_ss",
        "&nbsp;",
        
      ],

      format_cols: {
        
        est_date: "date_time",
        time: "date_time",
        count: 0,

      },

      parent: "#history_list_box",
      return: {},
      show_on_click: false,
      
      cit_run: function(state) { console.log(state) },
      

    };
    if (history_za_table.length > 0 ) create_cit_result_list(history_za_table, history_props );



  };
  this_module.make_history_list = make_history_list;

  
  
  function get_new_counter(doc_type) {
    
    toggle_global_progress_bar(true);

    return new Promise( function(resolve, reject) {

      $.ajax({
        headers: {
          'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
        },
        type: "POST",
        cache: false,
        url: `/get_new_counter`,
        data: JSON.stringify({
          doc_type: doc_type,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done(function (result) {

        console.log(result);

        if ( result.success == true ) {

          
          resolve(result.number);

        } else {

          if ( result.msg ) popup_error( `Greška prilikom generiranja broja ZA STANJE SKLADIŠTA!<br>` + result.msg );
          if ( !result.msg ) popup_error( `Greška prilikom generiranja broja ZA STANJE SKLADIŠTA!` );

          resolve(false);

        };

      })
      .fail(function (error) {

        resolve(false);

        console.log(error);
        popup_error(`Došlo je do greške na serveru prilikom generiranja broja ZA STANJE SKLADIŠTA!`);
      })
      .always(function() {

        toggle_global_progress_bar(false);

      });

    }); // kraj promisa

  }; 
  this_module.get_new_counter = get_new_counter;

  
  function choose_sklad_est_date(state, this_input) {
    
    var this_comp_id = this_input.closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    if ( !data.current_sklad_history ) data.current_sklad_history = {};
 
    
    if ( state == null || this_input.val() == `` || ( state !== null && state < Date.now() )  ) {
      
      this_input.val(``);
      data.current_sklad_history.est_date = null;
      console.log(`est_date: `, null );
    
      if ( state !== null && state < Date.now() )  {
        popup_warn(`Procjenjeni datum mora biti u budućnosti !!!`);
        
      };
      return;
    
    };
    
    
    data.current_sklad_history.est_date = state;
    console.log(`new est_date: `, new Date(state));
    
  };
  this_module.choose_sklad_est_date = choose_sklad_est_date;
  
  
  
  
  async function save_current_history(e) {
    
    e.stopPropagation();
    e.preventDefault();
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    
    if ( !window.cit_user ) {
      popup_warn(`Niste se prijavili u aplikaciju !!!`);
      return;
    };
    
    
    if ( !data.current_sklad_history ) data.current_sklad_history = {};
    
    if (
      
      !data.current_sklad_history.type
      ||
      $('#'+rand_id+'_sklad_komentar').val() == ""
      
      ||
      
      ( $(`#sklad_est_date_box`).css(`display`) == `block` && data.current_sklad_history.est_date == null )
      
    ) {
      
      popup_warn(`Potrebno je popuniti sva polja !!!!`);
      return;
      
    };
    
    
    
    
    
    return;
    
    
    
    /*
    {
          ss: 2564,
          storno: true,
          sifra: `sklhis`+cit_rand(),
          user: {
            "user_number" : 58,
            "full_name" : "Šaravanja Martina",
            "email" : "martina@velprom.hr"
          },
          type: { "sifra": "SKT1", "naziv": "Narudžba dobavljača" },
          time: Date.now() - (1000*60*60*24*3),
          doc_sifra: `ND 8569`,
          komentar: `Naručila sam ljepenku`,
          count: 10000,
          est_date: Date.now() + (1000*60*60*24*3),
        },
    */
    

    var new_ss_number = this_module.get_new_counter(`ss`);
    
    if ( !new_ss_number ) return;
    
    var new_history_obj = {
      
          ...data.current_sklad_history,
      
          ss: new_ss_number,
          storno: false,
          sifra: `sklhis`+cit_rand(),
          user: {
            "user_number" : window.cit_user.user_number,
            "full_name" : window.cit_user.full_name,
            "email" : window.cit_user.email,
          },
        
      
          type: { "sifra": "SKT1", "naziv": "Narudžba dobavljača" },
          time: Date.now() - (1000*60*60*24*3),
          doc_sifra: `ND 8569`,
          komentar: `Naručila sam ljepenku`,
          count: 10000,
          est_date: Date.now() + (1000*60*60*24*3),
        };
    
    console.log(this);
    
    
    return;
    
    
    
    refresh_simple_cal( $('#'+rand_id+`_sklad_est_date`) );
    
    
    
    
  };
  this_module.save_current_history = save_current_history;
  
  
  
  
  this_module.cit_loaded = true;  
 
}
  
  
};