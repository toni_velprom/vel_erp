var module_object = {
create: ( data, parent_element, placement )  => {
  var this_module = window[module_url]; 
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };

  
  /*
  TODO - 
  
Naziv dobavljača:*	VELPROM d.o.o.
Kod proizvoda:*	104903422
Opis proizvoda:*	NESQUIK COPACK TIN TRANSPORT BOX SLO
Broj narudžbenice:*	3200821038
Broj komada po kartonu (kom):*	20
Neto težina po kartonu (kg):*	0,5
Bruto težina po kartonu (kg):*	1
Dimenzije kartona - DxŠxV (m):*	1200x800x1750
Broj kartona na sloju palete:*	4
Broj slojeva na paleti:*	130
Broj kartona na EUR paleti:*	520
Ukupan broj kutija:*	2600
Ukupan broj komada:*	2.600
Ukupan broj EUR paleta:*	5
Podrijetlo proizvoda:*	Hrvatska
Datum dolaska na skladište:*	08.02.2021.
Vrsta materijala (papir, plastika, karton,…):*	karton
Preferencijal/Izjava o porijeklu/EUR:*	Made in Croatia
Plant:	4297
Item type:	SLO-NESQUIK
  
  */
  
  
  var valid = {
  

    alat_sifra: { element: "input", type: "string", lock: false, visible: true, label: "Šifra Alata" },  

    proj_sifra: { element: "input", type: "string", lock: true, visible: true, label: "Šifra Projekta" },
    item_sifra: { element: "input", type: "string", lock: true, visible: true, label: "Item Šifra" },
    variant: { element: "input", type: "string", lock: true, visible: true, label: "Varijanta" },
    
    kalk_docs: { "element": "upload", "type": "multiple", "lock": false, "visible": true, "label": "Izaberi dokumente" },  
    
    
    nacrt_sifra: { element:"input", type:"string", lock:true, visible:true, label:"Šifra Nacrta"},
    
    nacrt_tip: { element: "single_select", type: "simple", lock: false, visible: true, label: "Tip Nacrta" },
    nacrt_komentar: { element: "input", type: "string", lock: false, visible: true, label: "Nacrt komentar" },
    nacrt_docs: { "element": "upload", "type": "multiple", "lock": false, "visible": true, "label": "Izaberi dokumente nacrta" },  
    nacrt_find: { element: "single_select", type: "", lock: false, visible: true, label: "Ubaci nacrt" },
    
    
    graf_sifra: { element:"input", type:"string", lock:true, visible:true, label:"Šifra GP"},
    
    graf_komentar: { element: "input", type: "string", lock: false, visible: true, label: "GP komentar" },
    graf_docs: { "element": "upload", "type": "multiple", "lock": false, "visible": true, "label": "Izaberi dokumente za GP" },  
    graf_find: { element: "single_select", type: "", lock: false, visible: true, label: "Ubaci postojeći GP" },
    

    sifra: { element: "input", type: "string", lock: true, visible: true, label: "Šifra Proizvoda" },

    active: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Proizvod aktivan" },  

    tip: { element: "single_select", type: "simple", lock: false, visible: true, label: "Tip proizvoda" },

    find_variant_of: { element: "single_select", type: "", lock: false, visible: true, label: "Samo kopiraj sve podatke od nekog proizvoda ( NOVI SLIČAN PROIZVOD )" },
    
    find_original: { element: "single_select", type: "", lock: false, visible: true, label: "Nova naklada za postojeći proizvod ( IDENTIČAN PROIZVOD )" },

    /* -----------START----------- BITNI DATUMI ----------------------- */  
    rok_isporuke: { element: "simple_calendar", type: "date_time", lock: false, visible: true, label: "Rok isporuke", required: true },

    potrebno_dana_proiz: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Potrebno dana za proizvodnju", required: true },
    potrebno_dana_isporuka: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Potrebno dana za isporuku", required: true },

    rok_proiz: { element: "simple_calendar", type: "date_time", lock: false, visible: true, label: "Rok proizvodnje", required: true },  
    rok_za_def: { element: "simple_calendar", type: "date_time", lock: false, visible: true, label: "Početak proiz. / ROK ZA DEF. NK", required: true },

    /* -----------END----------- BITNI DATUMI ----------------------- */

    prod_specs_komentar: { element:"input", type:"string", lock:false, visible:true, label:"Opis dokumenata"},
    

    add_elem: { element: "single_select", type: "simple", lock: false, visible: true, label: "Dodaj element" },
    elem_val: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "VAL" },
    elem_kontra: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "KONTRA VAL" },
    

    naziv: { element: "input", type: "string", lock: false, visible: true, label: "Ime proizvoda", required: true },

    kupac_naziv: { element: "input", type: "string", lock: false, visible: true, label: "Ime kod kupca" },
    kupac_sifra: { element: "input", type: "string", lock: false, visible: true, label: "Šifra kod kupca" },

    mater: { element: "single_select", type: "simple", lock: false, visible: true, label: "Vrsta materijala (pretežno)" },
    komentar: { element: "text_area", type: "string", lock: false, visible: true, label: "Komentar za kupca" },
    interni_komentar: { element: "text_area", type: "string", lock: false, visible: true, label: "Interni komentar" },

    duzina: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Dužina" },
    sirina: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Širina" },
    visina: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Visina" },


    out_duzina: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Vanjska dužina" },
    out_sirina: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Vanjska širina" },
    out_visina: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Vanjska visina" },
    
  
    join_type: { element: "single_select", type: "simple", lock: false, visible: true, label: "Način spajanja" },
  
    exten: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Ima ekstenziju" },
    
    kut_count: { 
      element: "input", type: "number", decimals: 0, lock: false, visible: true, 
      label: "Broj kuteva ( npr: <span style='font-size: 18px; line-height: 1.3rem; position: relative;top: 3px;'>&#x25FC;</span>= 4, &#x2B22; = 6)" 
    },
    
    
    broj_polica: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Broj polica" },

    visina_header_ext: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Visina sa headerom i ext." },

    /* TODO ------> staviti u svaki proizvod */
    tenders: { element: "multi_select", type: "simple", lock: false, visible: true, label: "Tender" },

    elem_parent: { element: "single_select", type: "simple", lock: true, visible: true, label: "Pod-element od" },     
    elem_tip:  { element: "single_select", type: "simple", lock: true, visible: true, label: "Tip elementa" },  
    
    // elem_opis: { element: "input", type: "string", lock: false, visible: true, "label": "Opis elementa" },  

    elem_opis: { element: "text_area", type: "string", lock: false, visible: true, label: "Opis elementa" },
    
    elem_noz_big:  { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Dužina nož big" },

    
    
    elem_cmyk: { element: "input", type: "string", lock: false, visible: true, label: "CMYK" },
    
    elem_pantone: { element: "input", type: "string", lock: false, visible: true, label: "PANTONE" },

    

    elem_on_product: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Broj na proizvodu" },  


    /* SAMO ZA STALKE */  
    elem_vrsta_boka: { element: "single_select", type: "simple", lock: false, visible: true, label: "Vrsta boka" }, 
   


    /* SAMO ZA POLICE  */
    elem_RB: { element: "input", type: "number", lock: false, visible: true, label: "Redni broj od poda" },
    elem_polica_razmak: { element: "input", type: "number", lock: false, visible: true, label: "Unutarnji razmak do gornje police" },
    elem_polica_pregrade: { element: "input", type: "number", lock: false, visible: true, label: "Broj Pregrada" },
    elem_polica_nosivost: { element: "input", type: "number", lock: false, visible: true, label: "Nosivost police" },

    elem_front_orient: { element: "single_select", type: "simple", lock: false, visible: true, label: "Front gore/dolje" },   

    elem_find_sirov: { element: "single_select", type: "", lock: false, visible: true, label: "Izaberi ploču sirovine" },  
    
    elem_find_alat: { element: "single_select", type: "", lock: false, visible: true, label: "Izaberi alat" },  

    plate_C: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "CYAN / mm2" },
    plate_M: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "MAGENTA / mm2" },
    plate_Y: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "YELLOW / mm2" },
    plate_K: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "KEY(BLACK) / mm2" },
    plate_W: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "WHITE / mm2" },
    plate_V: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "VARNISH / mm2" },
    
    
    plate_noz_big: { element: "input", type: "number", decimals: 2, lock: false, visible: true, label: "Metara nož/big" },
    plate_nest_count: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Broj gnjezda na ploči" },

    elem_plate_count: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Broj elementata na mont. ploči" },
    
    elem_choose_plate: { element: "single_select", type: "", lock: false, visible: true, label: "Izaberi montažnu ploču" },  
    elem_choose_press: { element: "multi_select", type: "simple", lock: false, visible: true, label: "Vrsta Tiska" }, 
    elem_choose_alat: { element: "single_select", type: "", lock: false, visible: true, label: "Odabit Alata" }, 
    
    
    OLD_pregrada: { element: "switch", type: "bool", lock: false, visible: true, label: "Polica ima pregradu" },

    status: { element: "single_select", type: "simple", lock: false, visible: true, label: "Proces" }, 

    naklada: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Naklada / kom", required: true  },  

    produced: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Proizvedeno / kom" },  

    uzima_visak: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Kupac uzima višak" },
    placa_visak: { "element": "switch", "type": "bool", "lock": false, "visible": true, "label": "Kupac plaća višak" },
    popust_visak: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Popust na višak %", },  


    komada_visak: { element: "input", type: "number", decimals: 0, lock: false, visible: true, label: "Komada višak",   },  

    
    // ------------ ovo sam ubacio sam da omogućim kreiranje custom elementa 
    // elem_plates: { element: "single_select", type: "simple", lock: false, visible: true, label: "Montažne ploče" },        
    // elem_press: { element: "single_select", type: "simple", lock: false, visible: true, label: "Montažne ploče" },        
    
    choose_doc_adresa: { element: "single_select", type: "", lock: false, visible: true, label: "Adresa primatelja na dokumentu" },
    choose_dostav_adresa: { element: "single_select", type: "", lock: false, visible: true, label: "Adresa dostave" },
    
  };


  this_module.new_product_obj = {
    
    order_num: null,
    
    sample_for_id: null,
    sample_for_kalk: null,
    sample_sifra: null,
    
    prod_specs: [],
    
    nacrt_specs: [],
    graf_specs: [],
    alat_sifra: null,
    
    nac_gp_sifra: null,
    
    original: null, /* -------------- OVO JE SIFRA PROIZVODA KOJI JE GLAVNI I OVO JE KOPIJA OD NJEGA !!!!!! -------------- */
    variant_of: null, /* -------------- OVO JE SIFRA PROIZVODA OD KOJEG SAM SAMO KOPIRAO PODATKE !!!!!! -------------- */
     
    sample_for: null, 
    
    project_id: null,
     
    proj_sifra: ( data.proj_sifra || null ),
    tip: data.tip,
    
    rok_proiz: null,    
    rok_isporuke: null,
    
    potrebno_dana_proiz: null,
    potrebno_dana_isporuka: null,
    
    rok_za_def: null,

    /* ------------------------- ovo kreiram sa counterom kad save novi product  ------------------------- */
    item_sifra: data.item_sifra || null,
    variant: null,
    sifra: null,
    /* ------------------------- ovo kreiram sa counterom kad save novi product  ------------------------- */
    
    naklada: null,
    produced: null,
    
    naziv: null,
    
    full_product_name: null,
    kupac_naziv: null,
    kupac_sifra: null,
    
    mater: null,
    start: null,
    end: null,
    
    komentar: null,
    interni_komentar: null,
    
    duzina: null,
    out_duzina: null,
    
    sirina: null,
    out_sirina: null,
    
    visina: null,
    out_visina: null,
    visina_header_ext: null,

    
    broj_polica: null,
    kut_count: null,
    exten: false,
    join_type: null,
    
    
    
    mont_ploce: [],
    
    tenders: [],
  
    elements: [],
    
    alati: [],
    
    statuses: [],

    kalk: null,
    
    active: true,
    
    is_original: false,
        
    uzima_visak: true,
    placa_visak: true,
    popust_visak: null,
    komada_visak: null,
        
    
    plate_C: null,
    plate_M: null,
    plate_Y: null,
    plate_K: null,
    plate_W: null,
    plate_V: null,
    
    plate_noz_big: null,
    plate_nest_count: null,
    
    full_kupac_name: null,
    
    doc_adresa: null,
    dostav_adresa: null,
    

}; // kraj new product obj
  
  
  if ( !data.is_copy && !data.item_sifra ) {
    
    data = { 
      ...data,
      ...this_module.new_product_obj,
      
      // prepiši ove podatke iz podataka projekta !!!!
      rok_isporuke: data.rok_isporuke,
      potrebno_dana_proiz: data.potrebno_dana_proiz,
      potrebno_dana_isporuka: data.potrebno_dana_isporuka,
      rok_proiz: data.rok_proiz,
      rok_za_def: data.rok_za_def,
    };

  };
  
  
  
  // ako je kopija onda obriši taj prop jer nema smisla da ostaje !!! 
  // ionako je to samo temp varijabla za kreiranje kopije prilikom odabira originala
  
  if ( data.is_copy == true ) delete data.is_copy;
  
  var criteria = [
    'sifra',
    'rb'
  ];

  // multisort(data.elements, criteria);

  
  // UVIJEK RESETIRAJ OVE BOJE NA NULL TAKO DA NEMAM PROBLEMA ZA PRIKAZIVANJE UNUTAR INPUT POLJA !!!
  
  data.plate_C = null;
  data.plate_M = null;
  data.plate_Y = null;
  data.plate_K = null;
  data.plate_W = null;
  data.plate_V = null;
  
  data.plate_noz_big = null;
  data.plate_nest_count = null;
  
  
  if ( !data.mater ) data.mater = null;
  if ( !data.duzina ) data.duzina = null;
  if ( !data.sirina ) data.sirina = null;
  if ( !data.visina ) data.visina = null;
  if ( !data.out_duzina ) data.out_duzina = null;
  if ( !data.out_sirina ) data.out_sirina = null;
  if ( !data.broj_polica ) data.broj_polica = null;
  
  /* --------------OSNOVNI PODACI---------------------- NOVI PROPS ZA PRODUCT ------------------------------------*/
  
  if ( !data.out_visina ) data.out_visina = null;
  if ( !data.kut_count ) data.kut_count = null;
  if ( !data.exten ) data.exten = null;
  if ( !data.join_type ) data.join_type = null;
  

  this_module.set_init_data(data);
  
  
  var { id } = data;
  this_module.valid = valid;  
  var rand_id = `productrand`+cit_rand();
  this_module.cit_data[id].rand_id = rand_id;
  // data.rand_id = rand_id;
  
  
  data.elements_events_registered = false;
  
  var link = `#project/${data.proj_sifra || null }/item/${data.item_sifra || null }/variant/${ data.variant || null }/status/null`;
  
  var blink = "";

  if ( 
    data.goto_item     &&
    data.goto_variant  &&
    
    !data.goto_status  &&
    !data.goto_kalk    &&
    !data.goto_proces
  ) {
    
    if (data.item_sifra == data.goto_item && data.variant == data.goto_variant) {
      blink = "blink_bg";
      setTimeout( function() {  $(`#${id}_title_bar`).removeClass(`blink_bg`);  }, 1*1000 );
    };
    
  };
  
  
  var sample_class = data.sample_sifra ? `kalk_sample` : ``;
  
  var component_html =

`

<div  id="${id}" class="cit_comp product_comp"  style="margin-bottom: 5px;" 
      data-link="${link}" 
      data-product_id="${ data._id || '' }" 
      data-product_tip="${ data.tip.sifra || '' }" 
      data-proj_sifra="${data.proj_sifra || '' }"
      data-item_sifra="${data.item_sifra || '' }"
      data-variant="${ data.variant || '' }"
      data-sifra="${ data.sifra || '' }" >
 
  <section id="${id}_accord_parent" class="menu_cat accord_parent" style="margin-top: 0;">
    

    <!-- AKO ŽELIM DA BUDE OTVOREN TREBAM DODATI CLASS cit_active -->
    <a id="${id}_title_bar" class="menu_cat_title cit_accord cit_active ${blink} ${sample_class}" >
     
      <div class="product_name_placeholder" style="width: 40%; pointer-events: none;">${ this_module.generate_full_product_name(data) }</div>
      <div class="product_for_pro_title" style="width: 40%; pointer-events: none;">&nbsp;&nbsp;</div>
      
      <div style="width: 20%">
      
        <button   class="blue_btn small_btn cit_tooltip delete_product_btn" style="margin-right: 50px; float: right; background: #bf0060;"
                  data-toggle="tooltip" data-placement="top" data-html="true" title="Obriši ovaj proizvod!">
          <i class="fas fa-trash-alt"></i>
        </button>
        
        <button   class="blue_btn small_btn cit_tooltip duplicate_product_btn" style="margin-right: 20px; float: right; color: #27527b; background: #fff;"
                  data-toggle="tooltip" data-placement="top" data-html="true" title="Kopiraj ovaj proizvod!">
          <i class="far fa-copy"></i>
        </button>
        
        <button   class="blue_btn small_btn cit_tooltip save_product_btn" style="margin-right: 20px; float: right; background: #103253;"
                  data-product_comp_id="${ data.id || '' }"
                  data-toggle="tooltip" data-placement="top" data-html="true" title="SPREMI PROIZVOD">
          <i class="fas fa-save"></i>
        </button>
        
        <!--
        <button   class="blue_btn small_btn cit_tooltip" 
                  style="  margin-right: 50px;
                            float: right;
                            width: auto;
                            padding: 0 10px;"
                            
                  data-toggle="tooltip" data-placement="top" data-html="true" title="Spremi ovu stavku">
                  
          SPREMI

        </button>
        -->
        
      </div>

      <i class="fal fa-angle-down"></i>
    </a>

   
    <!-- KADA NIJE CIT ACTIVE CLASS NA TITLE BAR ONDA TREBAM DODATI OVAJ STYLE style="height: 0px; overflow: hidden;" -->
    <div id="${id}_cit_panel" class="cat_items cit_panel" >
    
    
      <div class="cit_tab_strip">
        <div id="${rand_id}_cit_tab_btn_podaci" class="cit_tab_btn cit_active">PODACI</div>
        <div id="${rand_id}_cit_tab_btn_elementi" class="cit_tab_btn">ELEMENTI</div>
        <div id="${rand_id}_cit_tab_btn_statusi" class="cit_tab_btn">STATUSI</div>
        <div id="${rand_id}_cit_tab_btn_kalk" class="cit_tab_btn kalkulacija_tab_btn">KALKULACIJA</div>
        <div id="${rand_id}_cit_tab_btn_utrosak" class="cit_tab_btn utrosak_tab_btn">UTROŠAK</div>
      </div>
    
      <div class="cit_tab_box" style="padding-right: 10px;">
        
        <div class="row" style="margin-top: 30px; margin-bottom: 30px;">
         
          <div class="col-md-2 col-sm-12">
          </div>
          <div class="col-md-5 col-sm-12">
            <b>SPREMLJENO:</b> <span id="${rand_id}_user_saved">${data.user_saved?.full_name || "" }</span>
            &nbsp;&nbsp;
            <span id="${rand_id}_saved">${ data.saved ? cit_dt(data.saved).date_time : "" }</span>
          </div>
          <div class="col-md-5 col-sm-12">
            <b>EDITIRANO:</b> <span id="${rand_id}_user_edited">${data.user_edited?.full_name || "" }</span>
            &nbsp;&nbsp;
            <span id="${rand_id}_edited">${ data.edited ? cit_dt(data.edited).date_time : "" }</span>
          </div>
          <div class="col-md-2 col-sm-12">
          </div>
          
        </div>    
        
               
        <div class="row" >
        
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_sifra`, valid.sifra, data.sifra) }
          </div> 
          
        
          <div class="col-md-2 col-sm-12">
             ${cit_comp(rand_id+`_rok_isporuke`, valid.rok_isporuke, data.rok_isporuke )}
          </div>
        
          <div class="col-md-2 col-sm-12">
            ${cit_comp(rand_id+`_potrebno_dana_proiz`, valid.potrebno_dana_proiz, data.potrebno_dana_proiz)}
          </div>

          <div class="col-md-2 col-sm-12">
            ${cit_comp(rand_id+`_potrebno_dana_isporuka`, valid.potrebno_dana_isporuka, data.potrebno_dana_isporuka)}
          </div>
          
          <div class="col-md-2 col-sm-12">
             ${cit_comp(rand_id+`_rok_proiz`, valid.rok_proiz, data.rok_proiz )}
          </div>

          <div class="col-md-2 col-sm-12">
            ${cit_comp(rand_id+`_rok_za_def`, valid.rok_za_def, data.rok_za_def)}
          </div>
          
          
        </div>

        <div class="row">  

          <div class="col-md-5 col-sm-12">
            ${ cit_comp(rand_id+`_naziv`, valid.naziv, data.naziv) }
          </div> 

          <div class="col-md-5 col-sm-12">
            ${ cit_comp(rand_id+`_kupac_naziv`, valid.kupac_naziv, data.kupac_naziv) }
          </div> 

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_kupac_sifra`, valid.kupac_sifra, data.kupac_sifra) }
          </div> 

        </div>
        
       
               
               
                
        <div class="row" >
         
          <div class="col-md-11 col-sm-12">
            ${ cit_comp(rand_id+`_find_variant_of`, valid.find_variant_of, data.variant_of, data.variant_of?.full_product_name || "" ) }
          </div>
          
          <div class="col-md-1 col-sm-12" >
            <a  href="#" target="_blank" onclick="event.stopPropagation();"
                class="blue_btn btn_small_text small_btn_link_left link_to_variant_of" 
                style="box-shadow: none; margin: 20px auto 0 0; width: 28px; height: 28px;" >

              <i style="font-size: 18px;" class="far fa-external-link-square-alt"></i>

            </a>
          </div>
          
        </div>
        
        
        <div class="row">
         
          <div class="col-md-11 col-sm-12">
            ${ cit_comp(rand_id+`_find_original`, valid.find_original, data.original, data.original?.full_product_name || "" ) }
          </div>
          
          <div class="col-md-1 col-sm-12" >
            <a  href="#" target="_blank" onclick="event.stopPropagation();"
                class="blue_btn btn_small_text small_btn_link_left link_to_original" 
                style="box-shadow: none; margin: 20px auto 0 0; width: 28px; height: 28px;" >

              <i style="font-size: 18px;" class="far fa-external-link-square-alt"></i>

            </a>
          </div>
          
          
        </div>
        

        <div class="row">

          
          <div class="col-md-2 col-sm-12">
             ${ cit_comp(rand_id+`_active`, valid.active, data.active) }
          </div>
          

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_tip`, valid.tip, data.tip, data.tip?.naziv || "" ) }
          </div>

          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_naklada`, valid.naklada, data.naklada ) }
          </div>
          
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_produced`, valid.produced, data.produced ) }
          </div>

        </div> 
        
        
        <div class="row">
         
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_uzima_visak`, valid.uzima_visak, data.uzima_visak ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_placa_visak`, valid.placa_visak, data.placa_visak ) }
          </div>
          
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_popust_visak`, valid.popust_visak, data.popust_visak ) }
          </div>
          
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_komada_visak`, valid.komada_visak, data.komada_visak ) }
          </div>
          
        </div>
        
        
        <div class="row">
          <div class="col-md-12 col-sm-12">
            ${cit_comp(rand_id+`_choose_dostav_adresa`,
                       valid.choose_dostav_adresa,
                       data.dostav_adresa,
                       data.dostav_adresa?.full_adresa ? ( data.dostav_adresa?.kontakt + " --- " + data.dostav_adresa?.full_adresa ) : ""
                      )}
          </div>
        </div>
        

       
       
    <!-- -----------------START--------------------- POLJA ZA PROIZVOD -------------------------------------- -->   
    
    <div class="row" style="margin-top: 0; margin-bottom: 0;">
      <h4 style="font-size: 15px; margin: 15px 0 -13px 23px; font-weight: 700;">OSNOVNI PODACI PROIZVODA</h4>
    </div> 
    
    
    <div class="row spec_row" style="border-bottom: none; border-top-width: 6px; margin-bottom: 0; padding-bottom: 0; "> 
     
      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_mater`, valid.mater, data.mater, data.mater?.naziv || "" ) }
      </div> 
      
      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_join_type`, valid.join_type, data.join_type, data.join_type?.naziv || "" ) }
      </div> 
      
      
      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_kut_count`, valid.kut_count, data.kut_count ) }
      </div>
      

    </div>

    <div class="row" >

      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_duzina`, valid.duzina, data.duzina) }
      </div>

      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_sirina`, valid.sirina, data.sirina) }
      </div>

      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_visina`, valid.visina, data.visina) }
      </div>


      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_out_duzina`, valid.out_duzina, data.out_duzina || "") }
      </div> 

      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_out_sirina`, valid.out_sirina, data.out_sirina || "") }
      </div>

      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_out_visina`, valid.out_visina, data.out_visina || "") }
      </div>


    </div>

    <div class="row spec_row" style="border-top: none;  border-bottom-width: 6px; margin-top: 0; margin-bottom: 0;  padding-top: 0; ">
      
      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_exten`, valid.exten, data.exten) }
      </div>
     
      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_visina_header_ext`, valid.visina_header_ext, data.visina_header_ext) }
      </div>

      <div class="col-md-2 col-sm-12">
        ${ cit_comp(rand_id+`_broj_polica`, valid.broj_polica, data.broj_polica) }
      </div>
      
    </div>
    
    
    <!-- -----------------END--------------------- POLJA ZA PROIZVOD -------------------------------------- -->       
       
       
       
        
        
        
   
        <div class="row" style="margin-top: 30px;" >
        
         
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_nacrt_find`, valid.nacrt_find, null, "" ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_nacrt_tip`, valid.nacrt_tip, null, "" ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_nacrt_sifra`, valid.nacrt_sifra, null, "" ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_graf_sifra`, valid.graf_sifra, null, "" ) }
          </div>
          
          
    
          
          
        </div>
        
        
        <div class="row" style="margin-top: 30px;" >

          
          <div class="col-md-6 col-sm-12">
            ${ cit_comp(rand_id+`_nacrt_komentar`, valid.nacrt_komentar, "" ) }
          </div>
          
          <div class="col-md-6 col-sm-12">
            ${ cit_comp(rand_id+`_graf_komentar`, valid.graf_komentar, "" ) }
          </div>


        </div>

      
        <div class="row">
          
          
          <div class="col-md-5 col-sm-12">
            ${ cit_comp(rand_id+`_nacrt_docs`, valid.nacrt_docs, "", "", `margin: 20px auto 0 0;` ) }
          </div>
          
          <div class="col-md-5 col-sm-12">
            ${ cit_comp(rand_id+`_graf_docs`, valid.graf_docs, "", "", `margin: 20px auto 0 0;` ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
          
            <button class="blue_btn btn_small_text save_nacrt_specs" style="margin: 20px auto 0 0; box-shadow: none;">
              SPREMI NACRT
            </button>
            
            
            <button class="violet_btn btn_small_text update_nacrt_specs" style="margin: 20px auto 0 0; box-shadow: none; display: none;">
              AŽURIRAJ NACRT
            </button>
            
            
          </div>
          
          
        </div>
        
        
        <div class="row">
          <div  class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" 
                id="${rand_id}_nacrt_specs_box" style="padding-top: 20px;">
              <!-- OVDJE IDE LISTA SVIH NACRTA ZA OVAJ PROIZVOD -->    
          </div> 
        </div>
        
        

        <div class="row">                
          <div class="col-md-6 col-sm-12">
            ${ cit_comp(rand_id+`_komentar`, valid.komentar, data.komentar) }
          </div>

          <div class="col-md-6 col-sm-12">
            ${ cit_comp(rand_id+`_interni_komentar`, valid.interni_komentar, data.interni_komentar) }
          </div>

        </div>
        
        <div class="row">
        
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_alat_sifra`, valid.alat_sifra, data.alat_sifra) }
          </div>


        </div>

        

     
        
        <div class="row" style="margin-top: 30px;" >

          <div class="col-md-8 col-sm-12">
            ${ cit_comp(rand_id+`_prod_specs_komentar`, valid.prod_specs_komentar, "" ) }
          </div>

          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_kalk_docs`, valid.kalk_docs, "", "", `margin: 20px auto 0 0;` ) }
          </div>

          <div class="col-md-2 col-sm-12">

            <button class="blue_btn btn_small_text save_prod_specs" style="margin: 20px auto 0 0; box-shadow: none;">
              SPREMI DOKUMENTE
            </button>

            <button class="violet_btn btn_small_text update_prod_specs" style="margin: 20px auto 0 0; box-shadow: none; display: none;">
              AŽURIRAJ DOKUMENTE
            </button>

          </div>

        </div>
        
        
        <div class="row">
          <div  class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui" 
                id="${rand_id}_prod_specs_box" style="padding-top: 20px;">
              <!-- OVDJE IDE LISTA SVIH KALK SPECS ZA OVAJ PROIZVOD -->    
          </div> 
        </div>

      </div>
      <!-- KRAJ TABA PODACI -->
     
      <div class="cit_tab_box" style="display: none;">

        
        <div class="row">

          <div class="col-md-6 col-sm-12">
            ${ cit_comp(rand_id+`_elem_find_sirov`, valid.elem_find_sirov, null, "" ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_plate_noz_big`, valid.plate_noz_big, "" ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_plate_nest_count`, valid.plate_nest_count, "" ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            <button class="blue_btn btn_small_text save_mont_plate_btn" style="margin: 20px 0 0 auto; box-shadow: none; transform-origin: 100% 50%;">
                SPREMI MONT. FORMU
            </button>
            
             <button class="violet_btn btn_small_text update_mont_plate_btn" style="display: none; margin: 20px 0 0 auto; box-shadow: none; transform-origin: 100% 50%;">
                UREDI MONT. FORMU
            </button>
            
          </div> 
          
          

        </div>
        
        <div class="row">
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_plate_C`, valid.plate_C, "" ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_plate_M`, valid.plate_M, "" ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_plate_Y`, valid.plate_Y, "" ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_plate_K`, valid.plate_K, "" ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_plate_W`, valid.plate_W, "" ) }
          </div>
          
          <div class="col-md-2 col-sm-12">
            ${ cit_comp(rand_id+`_plate_V`, valid.plate_V, "" ) }
          </div>
          

        </div>
        
        <div class="row">
        
          <div class="col-md-12 col-sm-12 cit_result_table result_table_inside_gui mont_plate_list_box" style="padding-top: 20px;">
            <!-- OVDJE IDE LISTA MONTAŽNI PLOČA KOJE JE USER DEFINIRAO ZA OVAJ PRODUCT -->    
          </div> 

        </div>
        
        
        <div class="row">

          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_add_elem`, valid.add_elem, data.add_elem, data.add_elem?.naziv || "" ) }
          </div>  


        </div>

        <div class="row">
          <div class="col-md-12 col-sm-12 cit_result_table cit_excel elem_list_box" id="${rand_id}_elem_list_box" style="padding-top: 20px;">
            <!-- OVDJE IDE LISTA ELEMENATA OVOG PROIZVODA -->    
          </div> 

        </div>
      

      
     
      </div>
     
      <div class="cit_tab_box" style="display: none;"> 
        <div class="row" id="${id}_statuses_box" style="padding: 0; margin: 0;" >

          <!--OVDJE UBACUJEM STATUSE-->

        </div>
      </div>

      <div class="cit_tab_box" style="display: none;" id="${id}_kalkulacija_box" >
        
          <!-- OVDJE IDU SVE KALKULACIJE ZA TAJ PROIZVOD -->

      </div>
   
   
      <div class="cit_tab_box" style="display: none;" id="${id}_utrosak_box" >
        
          <!-- OVDJE IDE TABLICA UTROŠAKA ZA OVAJ PROIZVOD -->

      </div>
      
   
   
    </div>

  
  </section>
</div>

`;

  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };
  
  
  wait_for( `$('#${data.id}').length > 0`, async function() {
    
    console.log(data.id + 'component injected into html');
    $('.cit_tooltip').tooltip();
    
    
    // ako postoji goto item i goto variant
    // ALI NE POSTOJI GOTO STATUS ili goto kalk ILI GOTO PROCES !!!!
    if ( data.goto_item && data.goto_variant && !data.goto_status && !data.goto_kalk && !data.goto_proces ) {
      
      var product_comp = $(`.product_comp[data-sifra="${ data.goto_item + "-" + data.goto_variant }"]`);
      var product_title_bar = product_comp.find(`.menu_cat_title`);
      var scroll_top = product_comp.offset().top;
      $(`html`)[0].scrollTop = scroll_top - 200;
      
    };
    
    // ------------------------------------------------------------------------------------------
    // START LOAD STATUSA
    // ------------------------------------------------------------------------------------------
    
    var status_mod = await get_cit_module(`/modules/status/status_module.js`, `load_css`);
    
    var kalk_mod = await get_cit_module(`/modules/kalk/kalk_module.js`, `load_css`);
    
    
    
          
      
      if ( data.kalk ) {
        
        data.kalk = await populate_kalk_original_sirovs(data.kalk);
        
      };
      
    
    function condition_for_status() {
      var cond = false;
      if ( data.elements_events_registered == true ) cond = true;
      return cond;
    };

    // prvo sačekaj da se registriraju eventi za glavne podatke od producta
    // TEK ONDA KRENI RADITI HTML OD STATUSA
    // to radim zato jer mogu napraviti override nekih evenata samo za statuse
    
    // ------------------------------------------------------------------------------------------
    // START LOAD STATUSA
    // ------------------------------------------------------------------------------------------
    
    wait_for( condition_for_status, function() {

      var status_data = {

        id: `status_module`+cit_rand(),
        for_module: `product`,
        
        product_id: data._id || null,
        

        proj_sifra: data.proj_sifra,
        item_sifra: data.item_sifra,
        variant: data.variant,
        full_product_name: this_module.generate_full_product_name(data),
        
        statuses: data.statuses,

        goto_item: (data.goto_item || null),
        goto_variant: (data.goto_variant || null),
        goto_status: (data.goto_status || null),
        goto_kalk: (data.goto_kalk || null),
        goto_proces: (data.goto_proces || null),

      };

      status_mod.create( status_data, $(`#${data.id}_statuses_box`) );
      
      
      if ( data.kalk && data.kalk?.pro_kalk?.length > 0 ) {
        var pro_kalk_title = create_kalk_naslov( data.kalk, data.kalk.pro_kalk[0] );
        $(`#` + data.id + ` .product_for_pro_title` ).html(pro_kalk_title);
      };
      
      // uvijek napravi objekt kalk 
      var kalk_id = `kalkmodule`+cit_rand(); 
      
      this_module.cit_data[data.id].kalk = {
        
        ...data.kalk, // ako je null ili undefined neće se niti upisati !!!
        
        original_kalk: data.kalk ? cit_deep(data.kalk) : null,
        
        id: kalk_id,
        
        for_module: `product`,

        proj_sifra: data.proj_sifra,
        item_sifra: data.item_sifra,
        variant: data.variant,
        
        product_id: data._id || null,
        tip: data.tip || null,
        
        full_product_name: this_module.generate_full_product_name(data),
        
        goto_item: (data.goto_item || null),
        goto_variant: (data.goto_variant || null),
        goto_status: (data.goto_status || null),
        goto_kalk: (data.goto_kalk || null),
        goto_proces: (data.goto_proces || null),
        
        
      };
      
      
      kalk_mod.create( this_module.cit_data[data.id].kalk, $(`#${data.id}_kalkulacija_box`) );
      
      
      this_module

    }, 20*1000);
    
    // ------------------------------------------------------------------------------------------
    // END LOAD STATUSA
    // ------------------------------------------------------------------------------------------
    
    $(`#${data.id} .save_prod_specs`).off('click');
    $(`#${data.id} .save_prod_specs`).on('click', this_module.save_current_prod_specs );
        
    $(`#${data.id} .update_prod_specs`).off('click');
    $(`#${data.id} .update_prod_specs`).on('click', this_module.save_current_prod_specs );
    
    $('#'+rand_id+'_kalk_docs').data(`this_module`, this_module );
    $('#'+rand_id+'_kalk_docs').data(`cit_run`, this_module.update_kalk_docs );
    
    
    $(`#${data.id} .save_nacrt_specs`).off('click');
    $(`#${data.id} .save_nacrt_specs`).on('click', this_module.save_current_nacrt_specs );
        
    $(`#${data.id} .update_nacrt_specs`).off('click');
    $(`#${data.id} .update_nacrt_specs`).on('click', this_module.save_current_nacrt_specs );
    
    $('#'+rand_id+'_nacrt_docs').data(`this_module`, this_module );
    $('#'+rand_id+'_nacrt_docs').data(`cit_run`, this_module.update_nacrt_docs );
    
        
    $('#'+rand_id+'_graf_docs').data(`this_module`, this_module );
    $('#'+rand_id+'_graf_docs').data(`cit_run`, this_module.update_graf_docs );
    
    this_module.make_prod_specs_list(data);
    
    this_module.make_nacrt_specs_list(data);
    
    
    $('#'+rand_id+'_active').data(`cit_run`, function(state) { data.active = state; } );
    
    $('#'+rand_id+'_uzima_visak').data(`cit_run`, function(state) { data.uzima_visak = state; } );
    $('#'+rand_id+'_placa_visak').data(`cit_run`, function(state) { data.placa_visak = state; } );
    
    
    $(`#${data.id} .cit_input.number`).off(`blur`);
    $(`#${data.id} .cit_input.number`).on(`blur`, function() {
      
      var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id = data.rand_id;
      
      
      // ovo je tip proizvoda F201 AMERKIKANKA
      if ( data.tip?.sifra == 'TP20' ) {
        
        if ( this.id.indexOf(`_duzina`) > -1 || this.id.indexOf(`_sirina`) > -1 ) {
          
          var duz = Number($('#'+rand_id+'_duzina').val()) || 0;
          var sir = Number($('#'+rand_id+'_sirina').val()) || 0;
          
          if ( duz < sir ) {
            popup_warn(`Ovaj proizvod ne može imati dužinu manju od širine !!!!`);
            $('#'+rand_id+'_duzina').val("");
            return;
          };
          
        }; // kraj ako je blur na poljima duzine ili sirine
      }; // ako je amerikanka
      
      set_input_data(this, data);
      
      this_module.update_rokove(this);
      
    });
    
    $(`#${data.id} .cit_input.string`).off(`blur`);
    $(`#${data.id} .cit_input.string`).on(`blur`, function() {
      
      var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id = data.rand_id;
      
      set_input_data(this, data);
    });
    
    $(`#${data.id} .cit_text_area`).off(`blur`);
    $(`#${data.id} .cit_text_area`).on(`blur`, function() {
      
      var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id = data.rand_id;
      
      set_input_data(this, data);
    });
    
    
    $(`#${data.id} .save_mont_plate_btn`).off('click');
    $(`#${data.id} .save_mont_plate_btn`).on('click', this_module.save_mont_plate );
    
    $(`#${data.id} .update_mont_plate_btn`).off('click');
    $(`#${data.id} .update_mont_plate_btn`).on('click', this_module.save_mont_plate );
    
    
    $(`#${data.id} .duplicate_product_btn`).off(`click`);
    $(`#${data.id} .duplicate_product_btn`).on(`click`, this_module.duplicate_product );
    
        
    $(`#${data.id} .delete_product_btn`).off(`click`);
    $(`#${data.id} .delete_product_btn`).on(`click`, this_module.delete_product );
    
    
    
    $(`#${data.id} .save_product_btn`).off(`click`);
    $(`#${data.id} .save_product_btn`).on(`click`, this_module.save_product );
    
    
    $('#'+rand_id+'_rok_isporuke').data(`cit_run`, this_module.update_rok_isporuke );
    $('#'+rand_id+'_rok_proiz').data(`cit_run`, this_module.update_rok_proiz );
    $('#'+rand_id+'_rok_za_def').data(`cit_run`, this_module.update_rok_za_def );

    
    $(`#`+rand_id+`_choose_dostav_adresa`).data('cit_props', {
      desc: 'odabir adrese iz liste kontakata za generiranje dokumenta unutar product modula ',
      local: true,
      show_cols: ['kontakt', 'pozicija_kontakta', 'full_adresa'],
      return: {},
      show_on_click: true,
      list: function() { return $(`#svi_kontakti_box`).data(`cit_result`) || []; },
      cit_run: function(state) {
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.dostav_adresa = null;
        } else {
          $('#'+current_input_id).val( state.kontakt + ` --- ` + state.full_adresa );
          data.dostav_adresa = state;
        };
        
      },
    });  
    
    
    $(`#`+rand_id+`_nacrt_tip`).data('cit_props', {
      desc: 'odabir tipa nacrta u product modulu  ' + rand_id,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: `nacrt_tip`,
      cit_run: function(state) {


        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        console.log(state);

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          if ( !data.current_nacrt_specs ) data.current_nacrt_specs = {};
          data.current_nacrt_specs.tip = null;
          return;
        };
        
        
        $('#'+current_input_id).val(state.naziv);
        
        if ( !data.current_nacrt_specs ) data.current_nacrt_specs = {};
        data.current_nacrt_specs.tip = state;

        console.log(data);
        
      },
      
      
    });

    
    /*
    
    ----------------------------------- NE KORISTIM OVO JE STAVLJA GRAF PRIPREMU U NACRTE -----------------------------------
    ----------------------------------- NE KORISTIM OVO JE STAVLJA GRAF PRIPREMU U NACRTE -----------------------------------
    
    $(`#`+rand_id+`_graf_tip`).data('cit_props', {
      desc: 'odabir tipa graf pripreme za nacrt u product modulu  ' + rand_id,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: `nacrt_tip`,
      cit_run: function(state) {

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        console.log(state);

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          if ( !data.current_graf_specs ) data.current_graf_specs = {};
          data.current_graf_specs.tip = null;
          return;
        };


        $('#'+current_input_id).val(state.naziv);

        if ( !data.current_graf_specs ) data.current_graf_specs = {};
        data.current_graf_specs.tip = state;

        console.log(data);

      },


    });
    
    ----------------------------------- NE KORISTIM OVO JE STAVLJA GRAF PRIPREMU U NACRTE -----------------------------------
    ----------------------------------- NE KORISTIM OVO JE STAVLJA GRAF PRIPREMU U NACRTE -----------------------------------
    
    */
    
    $(`#`+rand_id+`_mater`).data('cit_props', {
      desc: 'odabir materijala (PRETEŽNO) u product za id ' + rand_id + `_mater`,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: `mat_mix`, // prije je bilo u grubo `vrsta_mater`,
      cit_run: function(state) {


        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        console.log(state);

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.mater = null;
          return;
        };
        $('#'+current_input_id).val(state.naziv);
        data.mater = state;

        console.log(data);
        
      },
      
      
    });

    
    $(`#`+rand_id+`_join_type`).data('cit_props', {
      desc: 'odabir join type  u product za id ' + rand_id ,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: `join_type`, // prije je bilo u grubo `vrsta_mater`,
      cit_run: function(state) {


        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        console.log(state);

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.join_type = null;
          return;
        };
        
        
        $('#'+current_input_id).val(state.naziv);
        data.join_type = state;

        console.log(data);
        
      },
      
      
    });
    
    
        
    $(`#`+rand_id+`_tip`).data('cit_props', {
      desc: 'odabir tipa proizvoda  product za id ' + rand_id,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: `tip_proizvoda`,
      cit_run: function(state) {


        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        console.log(state);

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.tip = null;
          return;
        };
        $('#'+current_input_id).val(state.naziv);
        data.tip = state;

        console.log(data);
        
      },
      
      
    });
    
    
    $(`#`+rand_id+`_find_original`).data('cit_props', {
      
      desc: 'pretraga originalnog producta u modulu product :) ',
      local: false,
      url: '/find_product',
      
      find_in: [
        "sifra",
        "full_product_name",
        "full_kupac_name",
        "mater",
        "komentar",
        "interni_komentar",
        
        "prod_specs_flat",
        "nacrt_specs_flat",
        
        
      ],
      return: {},
      /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */
      show_cols: [
        "sifra",
        "full_product_name",
        "full_kupac_name",
        "komentar",
        "interni_komentar",
      ],
      col_widths: [
        1, // "sifra",
        3, // "full_product_name",
        3, // "full_kupac_name",
        3, // "komentar",
        3, // "interni_komentar",
      ],
      format_cols: {
        sifra: "center",
      },
      query: { variant: `1` }, // variant 1 znači da je prva verzija
      
      filter: null,
      
      show_on_click: false,
      /*list_width: 700,*/
      
      cit_run: this_module.open_original, // kraj cit run
      
      
    });  
    
    /*
    
    ------------------------------------------------------------------------------------
    ZAMJENIO SAM OVO SA cit props ISPOD
    ------------------------------------------------------------------------------------
    
    
    $(`#`+rand_id+`_find_variant_of`).data('cit_props', {
      
      desc: 'pretraga producta na osnovu kojeg radim trenutni produkt  - koji je varijacija :) ',
      local: false,
      url: '/find_product',
      
      find_in: [
        "sifra",
        "full_product_name",
        "full_kupac_name",
        "mater",
        "komentar",
        "interni_komentar",
      ],
      return: {},
      // * ne upisujem širinu za _id jer sam napravio da ga preskočim posve 
      show_cols: [
        "sifra",
        "full_product_name",
        "full_kupac_name",
        "komentar",
        "interni_komentar",
      ],
      col_widths: [
        1, // "sifra",
        3, // "full_product_name",
        3, // "full_kupac_name",
        3, // "komentar",
        3, // "interni_komentar",
      ],
      format_cols: {
        sifra: "center",
      },
      query: {},
      
      filter: null,
      
      show_on_click: false,
      
      // list_width: 700,
      
      cit_run: this_module.open_variant_of, // kraj cit run
      
      
    });  
    
    ------------------------------------------------------------------------------------
    ZAMJENIO SAM OVO SA cit props ISPOD
    ------------------------------------------------------------------------------------
        
    */
    
    $(`#`+rand_id+`_find_variant_of`).data('cit_props', {

      desc: `find nacrte unutar svih producta ${rand_id}`,
      local: false,
      url: '/find_product',

      find_in: [
        "sifra",
        "full_product_name",
        "full_kupac_name",
        "mater",
        "komentar",
        "interni_komentar",
        "prod_specs_flat",
        "nacrt_specs_flat",
      ],
      return: {},
      /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */
      show_cols: [
        "sifra",
        "template_nacrta",
        
        "full_product_name",
        "full_kupac_name",
        "interni_komentar",
        
        "nacrt_komentar",
        "nacrt_docs",
        
        "graf_komentar",
        "graf_docs",
      ],
      col_widths: [
        1, // "sifra",
        1, // "template_nacrta",
        
        3, // "full_product_name",
        3, // "full_kupac_name",
        3, // "interni_komentar",
        
        3, // "nacrt_komentar",
        8, // "nacrt_docs"
        
        3, // "graf_komentar"
        8, // "graf_docs"
      ],
      format_cols: {
        sifra: "center",
        template_nacrta: "center",
      },
      query: {},

      filter: this_module.nacrt_find_filter,

      show_on_click: false,
      list_width: 700,

      cit_run: this_module.open_variant_of,

    });  
    
    
    $(`#`+rand_id+`_nacrt_find`).data('cit_props', {

      desc: `find  i ubaci samo specijalno nacrte unutar  producta ${rand_id}`,
      local: false,
      url: '/find_product',

      find_in: [
        "sifra",
        "full_product_name",
        "full_kupac_name",
        "mater",
        "komentar",
        "interni_komentar",
        "prod_specs_flat",
        "nacrt_specs_flat",
      ],
      return: {},
      /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */
      show_cols: [
        "sifra",
        "template_nacrta",
        
        "full_product_name",
        "full_kupac_name",
        "interni_komentar",
        
        "nacrt_komentar",
        "nacrt_docs",
        
        "graf_komentar",
        "graf_docs",
      ],
      col_widths: [
        1, // "sifra",
        1, // "template_nacrta",
        
        3, // "full_product_name",
        3, // "full_kupac_name",
        3, // "interni_komentar",
        
        3, // "nacrt_komentar",
        8, // "nacrt_docs"
        
        3, // "graf_komentar"
        8, // "graf_docs"
      ],
      format_cols: {
        sifra: "center",
        template_nacrta: "center",
      },
      query: {},

      filter: this_module.nacrt_find_filter,

      show_on_click: false,
      list_width: 700,

      cit_run: function (selected_product) {

        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        // kada user ručno obriše polje za variant of
        if ( selected_product == null ) {
          $('#'+current_input_id).val(``);
        } 
        else {

          console.log(selected_product);

          var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var selected_nacrt = null;

          // pronađi i kopiraj template
          $.each( selected_product.nacrt_specs, function(n_ind, prod_spec ) {
            if ( prod_spec.template == selected_product.template_nacrta ) selected_nacrt = cit_deep( prod_spec );
          });

          
          var found_same_nacrt = 0;
          // provjeri jel možda već ubacio ovaj isti template
          if ( data.nacrt_specs?.length > 0 ) {
            
            $.each( data.nacrt_specs, function(data_n_ind, data_spec ) {
              if ( found_same_nacrt == 0 && selected_nacrt.template == data_spec.template ) {
                found_same_nacrt += 1;
                popup_warn(`Već ste ubacili ovaj template nacrta!!!`);
              };
            });
            
          };
          
          if ( found_same_nacrt !== 0 ) return;

          data.nacrt_specs.push( selected_nacrt );
          
          this_module.make_nacrt_specs_list(data);
          
        }; // kraj od else (ako state nije null )

      },


    });  
    
    
    
    this_module.sirov_module = await get_cit_module(`/modules/sirov/sirov_module.js`, 'load_css');
    this_module.sirov_valid = this_module.sirov_module.valid;
    
    this_module.find_sirov = await get_cit_module(`/modules/sirov/find_sirov.js`, null);
    
    
    $(`#`+rand_id+`_elem_find_sirov`).data('cit_props', {
      // !!!findsirov!!!
      desc: 'pretraga i odabir sirovine u modulu product za definiranje montažne ploče ' + rand_id,
      local: false,
      url: '/find_sirov',
      find_in: [
        "sirovina_sifra", 
        "naziv",
        "full_naziv",
        "stari_naziv",
        "povezani_nazivi",
        "detaljan_opis",
        "grupa_flat",  

        "kvaliteta_1",
        "kvaliteta_2", 
        "kvaliteta_mix",

        "kontra_tok_x_tok",
        "val_x_kontraval",
        "alat_prirez",

        "dobavljac_flat", 
        "dobav_naziv", 
        "dobav_sifra", 
        "fsc_flat", 
        "povezani_kupci_flat", 
        "boja",   
        "zemlja_pod_flat",
        "specs_flat",
      ],
      
      query: function () {  
        
        // var data = get_comp_data( $(`#`+rand_id+`_elem_find_sirov`), null, this_module );
        
        // var this_comp_id = $(`#`+rand_id+`_elem_find_sirov`).closest('.cit_comp')[0].id;
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        var query = { on_date: data.rok_za_def || null };
        
        return query;
      },

      filter: this_module.find_sirov.find_sirov_filter,
      
      return: {},
      /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */
      col_widths: [
        1, // "sifra_link",
        2, // "full_naziv",
        2, // "full_dobavljac",
        1, // grupa_naziv
        1, // "boja",
        1, // "cijena",
        1, // "kontra_tok",
        1, // "tok",
        1, // "po_valu",
        1, // "po_kontravalu",
        2, // "povezani_nazivi",
        
        
        1, // "pk_kolicina",
        1, // "nk_kolicina",
        1, // "rn_kolicina",
        1, // "order_kolicina",
        1, // "sklad_kolicina",
        1, // "forcast_kolicina",
        1, // last status
        
        4, //  `spec_docs`,

      ],
      show_cols: [
        "sifra_link",
        "full_naziv",
        "full_dobavljac",
        "grupa_naziv",
        "boja",
        "cijena",
        "kontra_tok",
        "tok",
        "po_valu",
        "po_kontravalu",
        "povezani_nazivi",
        
        `pk_kolicina`,
        `nk_kolicina`,
        `rn_kolicina`,
        `order_kolicina`,
        `sklad_kolicina`,
        `forcast_kolicina`,
        
        `last_status`,
        `spec_docs`,
        
      ],
      
      custom_headers: [
        `ŠIFRA`,  // "sirovina_sifra",
        `NAZIV`,  // "full_naziv",
        `DOBAV.`,  // "full_dobavljac",
        `GRUPA`,  // "grupa_naziv",
        `BOJA`,  // "boja",
        `CIJENA`,  // "cijena",
        `KONTRA TOK`,  // "kontra_tok",
        `TOK`,  // "tok",
        `VAL`,  // "po_valu",
        `KONTRA VAL`,  // "po_kontravalu",
        `POVEZANO`,  // "povezani_nazivi",
        
        `PK RESERV`,  // "pk_kolicina",
        `NK RESERV`,  // "nk_kolicina",
        `RN RESERV`, // "rn_kolicina"
        `NARUČENO`,  // "order_kolicina",
        `SKLADIŠTE`,  // "sklad_kolicina",
        `DOSTUPNO`, // forcast kolicina
        `STATUS`,
        `DOCS`,
      ],
      
      format_cols: {
        "sifra_link": "center",
        "grupa_naziv": "center",
        "boja": "center",
        "cijena": this_module.sirov_valid.cijena.decimals,
        "kontra_tok": this_module.sirov_valid.kontra_tok.decimals,
        "tok": this_module.sirov_valid.tok.decimals,
        "po_valu": this_module.sirov_valid.po_valu.decimals,
        "po_kontravalu": this_module.sirov_valid.po_kontravalu.decimals,
        
        "pk_kolicina": this_module.sirov_valid.pk_kolicina.decimals,
        "nk_kolicina": this_module.sirov_valid.nk_kolicina.decimals,
        "order_kolicina": this_module.sirov_valid.order_kolicina.decimals,
        "sklad_kolicina": this_module.sirov_valid.sklad_kolicina.decimals,
        "forcast_kolicina": 2,
        
      },
      
      show_on_click: false,
      
      cit_run: async function(state) {

        console.log(state);
        
        var state = cit_deep(state);
        
        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;
        
        if ( !data.current_mont_plate ) data.current_mont_plate = {};
        
        console.log(state);
        
        if ( state == null ) {
          $('#'+current_input_id).val(``);
          data.current_mont_plate.sirov = null; 
          return;
        };

        var po_valu = null;
        var po_kontravalu = null;
        
        if ( state.po_valu == null || state.po_kontravalu == null ) {
          if ( state.kontra_tok == null || state.tok == null ) {
            popup_warn(`Za montažnu ploču možete izabrati samo pločasti materijal ili papir`);
            return;
          };
        };
        
        // ako je ljepenka onda uzmi dimenzije
        if ( state.po_valu !== null && state.po_kontravalu !== null ) {
          po_valu = state.po_valu;
          po_kontravalu = state.po_kontravalu;
        };
        
        // ako je PAPIR onda uzmi dimenzije ALI IH ZAMJENI !!!
        // ako je PAPIR onda uzmi dimenzije ALI IH ZAMJENI !!!
        // ako je PAPIR onda uzmi dimenzije ALI IH ZAMJENI !!!
        if ( state.kontra_tok !== null && state.tok !== null ) {
          po_valu = state.kontra_tok;
          po_kontravalu = state.tok;
        };
        
        var sirov_long_name = ( state.sirovina_sifra + "--" + state.full_naziv + "--" + state.dobavljac?.naziv );
        
        $('#'+current_input_id).val(sirov_long_name);
        
        // delete sve dodatne podatke koji nisu bitni
        var sir = cit_deep(state);
        if ( sir.records?.length > 0 ) delete sir.records;
        if ( sir.statuses?.length > 0 ) delete sir.statuses;
        if ( sir.locations?.length > 0 ) delete sir.locations;
        if ( sir.sirov_pics?.length > 0 ) delete sir.sirov_pics;
        
        if ( sir.dobavljac ) sir.dobavljac = { _id: sir.dobavljac._id || null, naziv: sir.dobavljac.naziv || null };
        
        
        // obriši sve flat propertije u sirovini
        // obriši sve flat propertije u sirovini
        // obriši sve flat propertije u sirovini
        $.each(sir, function(sir_key, key_data) {
          if (sir_key.indexOf(`_flat`) > -1) delete sir[sir_key];
        })
        
        
        data.current_mont_plate.sirov = {
          
          ...sir,
          
          full_naziv: sirov_long_name,
          sirov_id: state._id,
          
          po_valu,
          po_kontravalu,
          
          kontra_tok: state.kontra_tok,
          tok: state.tok,
          
          kalk_unit: state.osnovna_jedinica?.jedinica || null,
          cijena: get_price_m2(state).price_m2,
          
          vrsta_materijala: state.vrsta_materijala || null,
          kvaliteta_1: state.kvaliteta_1 || null,
          kvaliteta_2: state.kvaliteta_2 || null,
          debljina_mm: state.debljina_mm || null,
          gramaza_g: state.gramaza_g || null,
          
        };
        
      },
      
    });  
    
    
    
    
    if ( data.original ) {
      
      $('#'+ data.id + ' .link_to_original').attr(
        `href`, 
        `#project/${data.original.proj_sifra || null }/item/${data.original.item_sifra || null }/variant/${ data.original.variant || null }/status/null`
      );    
      
    };
    
    
    if ( data.variant_of ) {
      
      $('#'+ data.id + ' .link_to_variant_of').attr(
        `href`, 
        `#project/${data.variant_of.proj_sifra || null }/item/${data.variant_of.item_sifra || null }/variant/${ data.variant_of.variant || null }/status/null`
      );    
      
    };
    
    
    $(`#`+rand_id+`_add_elem`).data('cit_props', {
      
      desc: 'odabir novog elementa u product za id ' + rand_id + `_add_elem`,
      local: true,
      show_cols: ['naziv'],
      return: {},
      show_on_click: true,
      list: `add_elem`,
      cit_run: function(state) {


        var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        console.log(state);

        if ( state == null ) {
          $('#'+current_input_id).val(``);
          return;
        };

        this_module.add_elem(state, null);  // kad je drugi arg null to znači da nema parenta

      },
    });
    
    
    this_module.make_mont_plate_list(data);
    
    this_module.make_element_list(data);
    
    
  }, 20*1000 );

  return {
    html: component_html,
    id: data.id
  };

},
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};
  
  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
  };
  this_module.set_init_data = set_init_data;
  
  
  
  function add_elem(state, parent_id) {
    
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    
    // if ( data.tip?.sifra == 'TP1' ) {
      
    var new_elem = {

      add_elem: null,
      elem_val: null,
      elem_kontra: null,

      elem_parent: (parent_id || null),
      
      elem_opis: `${state.naziv}`,
      elem_noz_big: null,
      
      sifra: 'elem'+cit_rand(),
      
      elem_tip: state,
      
      elem_press: null,
      elem_alat: null,
      
      elem_cmyk: ``,
      elem_pantone: ``,
      elem_vrsta_boka: null,
      elem_plates: null,

      elem_on_product: 1,

      elem_RB: null,
      elem_polica_razmak: null,
      elem_polica_pregrade: null,
      elem_polica_nosivost: null,
      elem_front_orient: null,

    };
      
    // };
    
    
    // ova šifra znači da je to baza police
    // zato moram izračunati novi redni broj police !!!
    if ( new_elem.elem_tip.sifra == `SEL6`) {
      var max_polica_num = 0;
      $.each(data.elements, function(e_ind, elem) {
        if ( elem.elem_tip.sifra == `SEL6` && elem.elem_RB && elem.elem_RB > max_polica_num ) max_polica_num = elem.elem_RB;
      });
      new_elem.elem_RB = max_polica_num + 1 ;
     };
    
    data.elements.push(new_elem);
    
    this_module.make_element_list(data);
    
  };
  this_module.add_elem = add_elem;
  

  function make_element_list(data) {
    
    // !!!!!!! ----- prvo obriši sve levele koji služe za uvlačenje podelemenata
    // !!!!!!! ----- prvo obriši sve levele koji služe za uvlačenje podelemenata
    
    $.each( data.elements, function( ind, elem ) {
      if ( data.elements[ind].level ) delete data.elements[ind].level;
    });
    
    data.elements = sort_parents( data.elements, null, `down` );
    // console.log( JSON.stringify(data.elements) );
  
    var valid = this_module.valid;
    
    var elements_for_table = [];
        
    $.each(data.elements , function(e_index, element ) {
      
      
      // ako nedostaju ovi propsi onda ih ubaci kao NULL
      if ( !element.elem_val ) element.elem_val = null;
      if ( !element.elem_kontra ) element.elem_kontra = null;
      
      // ovo je dummy data za testiranje     
      /*
      var element = {
        
        sifra: 'elem'+cit_rand(),
        elem_tip: { sifra: "SEL2", naziv: "Tijelo desnog boka" },
        elem_opis: `detalji Tijelo desnog boka`,
        elem_press: [ { "sifra": "TVR1", "naziv": "Offset" } ],
        elem_alat: [ { _id: "sfsdfsdf",  } ],
        elem_cmyk: `4/0`,
        elem_pantone: `1/0 NN 2536`, 
        elem_on_product: 1,
        elem_plates: [ { sifra: (`PLT`+cit_rand()), naziv: '2021-05-06-300', count: 3} ],
        elem_vrsta_boka: { sifra: "VBOK1", naziv: "Ravni" }, 

        elem_RB: null,
        elem_polica_razmak: null,
        elem_polica_pregrade: null,
        elem_parent: null,
      };
      */
 
      // ako ima unutar elem tipa property kalk onda je to element nastao dinamičiki putem nekog procesa
      // i to znači da ga ne trebam ubaciti u listu elemenata
      var is_kalk = element.elem_tip?.kalk ? true : false;

      if ( is_kalk == false ) {
        
        var elem_obj = {};

        $.each(element , function(elem_key, elem_value ) {



          var elem_html = "";

          var input_styles = {
            "elem_opis": "min-height: 50px; height: 100%;",
            "elem_press": "text-align: center;",
            "elem_cmyk": "text-align: center;",
            "elem_pantone": "text-align: center;",
            "elem_on_product": "text-align: center;",
            "elem_vrsta_boka": "text-align: center;",
            "elem_front_orient": "text-align: center;",
            "elem_RB": "text-align: center;",
            "elem_polica_nosivost": "text-align: center;",
            "elem_polica_razmak": "text-align: center;",
            "elem_polica_pregrade": "text-align: center;",
          };

          var special_cols = {

            elem_plates: function(plates, element_sifra) {
              //  ovo je primjer elem plate array:
              // elem_plates: [ { sifra: (`PLT`+cit_rand()), naziv: '2021-05-06-265', count: 3} ],

              var all_plates = "";
              $.each(plates, function(ind, plate) {

                var plate_html = 
                `
                <div style="display: flex; width: 100%; font-size: 12px; margin-top: 3px;">

                  <div style="width: 50%; margin-right: 5%;">${plate.naziv}</div>
                  <div style="width: 30%;">kom: ${plate.count}</div>
                  <div style="width: 20%;">
                    <button 
                        id="${element_sifra}_${plate.sifra}_delete_elem_plate"
                        data-element_sifra="${element_sifra}"
                        class="delete_elem_plate cit_tooltip" style="background: #bf0060; margin: 0 0 3px 0;" >

                      <i class="fas fa-trash-alt"></i>
                    </button> 


                  </div>
                </div>
                `;

                all_plates += plate_html;
              });


              var html = `<div class="custom_result_box">

                <div style="display: flex; width: 100%;">
                  <div style="width: 20%;">
                    ${ cit_comp( data.rand_id + '_' + element.sifra + `_elem_plate_count`, valid.elem_plate_count, "", null, `border: 1px solid #ced4da;` ) }
                  </div>
                  <div style="width: 80%;">
                    ${ cit_comp(data.rand_id + '_' + element.sifra + `_elem_choose_plate`, valid.elem_choose_plate, null, "", `border: 1px solid #ced4da;` )  } 
                  </div>
                </div>

                ${all_plates}

              </div>`;
              return html;  
            },

            elem_press: function(press_arr, element_sifra)  {
              //  ovo je primjer elem plate press_arr:
              // elem_press: [   { "sifra": "TVR1", "naziv": "Offset" }, ],

              var all_press = "";
              $.each(press_arr, function(ind, press) {

                var press_html = 
                `
                <div style="display: flex; width: 100%; font-size: 12px; margin-top: 3px;">

                  <div style="width: 70%; margin-right: 5%;">${press.naziv}</div>
                  <div style="width: 25%;">
                    <button 
                        id="${element_sifra}_${press.sifra}_delete_elem_press"
                        data-element_sifra="${element_sifra}"
                        class="delete_elem_press cit_tooltip" style="background: #bf0060; margin: 0 0 3px 0;" >

                      <i class="fas fa-trash-alt"></i>
                    </button> 


                  </div>
                </div>
                `;

                all_press += press_html;
              });


              var html = 
                
                `<div class="custom_result_box">

                <div style="display: flex; width: 100%;">
       
                  <div style="width: 100%;">
                    ${ cit_comp(data.rand_id + '_' + element.sifra + `_elem_choose_press`, valid.elem_choose_press, null, "", `border: 1px solid #ced4da;` )  } 
                  </div>
                </div>

                ${all_press}

              </div>`;
              
              
              return html;  
              
              
            },
            
            elem_alat: function(alati_arr, element_sifra) {
              
              // ovo je primjer elem plate alati_arr:
              // elem_alat: [  { _id: state._id, sifra: `elemalat`+cit_rand(), sirovina_sifra: state.sirovina_sifra, full_naziv, last_status, }, .... ],

              var all_alati = "";
              $.each(alati_arr, function(ind, alat) {

                var alat_html = 
                `
                <div style="display: flex; width: 100%; font-size: 12px; margin-top: 3px;">

                  <div style="width: 70%; margin-right: 5%;">
                    <a href="#sirov/${alat._id}/status/null" 
                      target="_blank"
                      onClick="event.stopPropagation();"> 
                      ${alat.sirovina_sifra}
                    </a>
                  </div>
                  <div style="width: 25%;">
                    <button 
                        id="${element_sifra}_${alat.sifra}_delete_elem_alat"
                        data-element_sifra="${element_sifra}"
                        class="delete_elem_alat cit_tooltip" style="background: #bf0060; margin: 0 0 3px 0;" >

                      <i class="fas fa-trash-alt"></i>
                    </button> 

                  </div>
                </div>
                `;

                all_alati += alat_html;
                
              });


              var html = 
                
                `<div class="custom_result_box">

                <div style="display: flex; width: 100%;">
       
                  <div style="width: 100%;">
                    ${ cit_comp(data.rand_id + '_' + element.sifra + `_elem_choose_alat`, valid.elem_choose_alat, null, "", `border: 1px solid #ced4da;` )  } 
                  </div>
                </div>

                ${all_alati}

              </div>`;
              
              
              return html;  
              
              
            },
            
            
          };


          // nemoj raditi nikakve elemente za sifru

          if ( elem_key !== "sifra" ) { 

            var indent_div = "";
            if ( elem_key == "elem_tip" && element.level > 0 ) indent_div = `<div style="width: ${element.level}px; background: #89b0d4;"></div>`;

            // ako postoji valid za ovaj objekt

            if ( special_cols[elem_key] ) {
              elem_html = special_cols[elem_key]( elem_value, element.sifra);
            }
            else if ( valid[elem_key]?.element == "single_select" ) {
              // AKO JE SINGLE SELECT TJ DROP LIST
              elem_html = indent_div + cit_comp( data.rand_id + '_' + element.sifra+`_`+elem_key, valid[elem_key], elem_value || null, elem_value?.naziv || "", input_styles[elem_key] || "" );  
            } else if ( valid[elem_key] ) {
              // AKO JE BILO ŠTO DRUGO
              elem_html =  cit_comp( data.rand_id + '_' + element.sifra+`_`+elem_key, valid[elem_key], elem_value || null, null, input_styles[elem_key] || "" );    
            };


          }; // kraj ako nije property sifra


          // element ne smije imati prop kalk jer je to temp element za kalkulacije 
          // element ne smije imati prop kalk jer je to temp element za kalkulacije 

          elem_obj[elem_key] = elem_html || elem_value;

        }); // kraj loopa po svim propertijim elementa
        
        elements_for_table.push(elem_obj);
         
      }; // kraj ifa ako nema kalk unutar elem_tip   
      
      
      
    }); // kraj loopa po svim arg elementima
    
    
    var elements_props = {
      
      desc: `samo za kreiranje tablice ${data.rand_id} elemenata proizvoda u modulu projekti` ,
      local: true,
      hide_sort_arrows: true,
      
      list: elements_for_table,
      
      show_cols: [
        
        
        "elem_tip",
        
        
        "add_elem",
        "elem_val",
        "elem_kontra",
        
        
        "elem_opis",
        "elem_noz_big",
        
        "elem_press",
        "elem_alat",
        
        "elem_cmyk",
        "elem_pantone",
        "elem_on_product",
        "elem_plates",
        "elem_vrsta_boka",
        "elem_RB",
        "elem_polica_nosivost",
        "elem_front_orient",
        "elem_polica_razmak",
        "elem_polica_pregrade",
        'button_delete' 
      ],
      
      custom_headers: [
        "Tip",
        "Pod-element",
        
        "VAL",
        "KONTRA",
        
        "Opis",
        "Nož/big",
        "TISAK",
        "ALATI",
        "CMYK",
        "PANTONE",
        "Na proizvodu",
        "Montažne ploče",
        "Vrsta boka",
        "R. Broj",
        "Nosivost",
        "Front G/D",
        "Polica razmak",
        "Broj pregrada",
        'BRIŠI' 
      ],
      
      /* format_cols: { rok: "date", start: "date", end: "date" }, */
      col_widths: [
        3, // "elem_tip",
        1.5, // "add_elem",
        1, // elem_val
        1, // elem_kontra
        
        4, // "elem_opis",
        1, // "elem_noz_big",
        
        3, // "elem_press",
        3, // elem_alat
        
        1, // "elem_cmyk",
        2.3, // "elem_pantone",
        1, // "elem_on_product",
        4, // "elem_plates",
        1, // "elem_vrsta_boka",
        1, // "elem_RB",
        1, // "elem_polica_nosivost",
        1, // "elem_front_orient",
        1, // "elem_polica_razmak",
        1, // "elem_polica_pregrade",
        1, // 'button_delete'
      ],
   
      parent:  `#${data.rand_id}_elem_list_box` , // `
      return: {},
      show_on_click: false,
      cit_run: function(state) { console.log(state); },
      
      button_delete: async function(e) {
        
        e.stopPropagation();
        e.preventDefault();
        
        var this_button = this;
        
        console.log("kliknuo DELETE za ELEMENT");
        
        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
          
          function recursive_delete(sifra) {
            data.elements = delete_item(data.elements, sifra , `sifra` ); 
            // find red koji trebam obrisati
            $(`#${data.rand_id}_elem_list_box .search_result_table .search_result_row`).each( function() {
              if ( this.id.indexOf(`_list_item_`+sifra ) > -1 ) $(this).remove();
            });
            // pogledaj je u element s ovom sifrom postoji u nekom propertiju elem_parent
            // ako ova sifra stoji negdje kao elem parent - to je onda djete od ovog elementa
            var index = find_index(data.elements, sifra , `elem_parent` );
            if ( index !== null ) recursive_delete( data.elements[index].sifra );
          };
          
          recursive_delete(sifra);
          
        };
        
        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj Element?`, null );
        };
        
        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj Element?`, null, 'yes/no', delete_yes, delete_no, null);
        
      },
      
    };
            

    if (elements_for_table.length > 0 ) {
      
      // $(`#${data.rand_id}_elem_list_box .search_result_row`).html("");
      
      create_cit_result_list(elements_for_table, elements_props );
      
      // čekaj da se pojavi tablica s elementima 
      wait_for(`$("#${data.rand_id}_elem_list_box .search_result_row").length > 0`, function() {
        
        // remove_sort_arrows( $(`#${data.rand_id}_elem_list_box`) );
        
        $(`#${data.rand_id}_elem_list_box .single_select`).each( function() {
          
          var this_input = this;
          
          // var elem_key = this_input.id.substr( this_input.id.indexOf("_")+1, 10000000);
          
          var under_score_arr = this.id.split(`_`);
          var random = under_score_arr[0];
          var sifra = under_score_arr[1];
          var elem_key = this.id.replace( random + `_` + sifra + `_`, `` );
          
          
          $(this_input).data('cit_props', {
            desc: 'odabir single_select za id ' + this_input.id,
            local: true,
            show_cols: ['naziv'],
            return: {},
            show_on_click: true,
            list: elem_key,
            cit_run: function(state) {
              
              var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;

              var parent_row_id = $('#'+current_input_id).closest('.search_result_row')[0].id;
              var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
              
              // var current_prop = current_input_id.replace( sifra+`_`, ``);
              
              var index = find_index(data.elements, sifra , `sifra` );
              
              console.log(state);
              
              if ( state == null ) {
                $('#'+current_input_id).val(``);
                data.elements[index][elem_key] = null;
                return;
              };
              
              if ( elem_key == "add_elem" ) {
                this_module.add_elem(state, sifra);
              } else {
                $('#'+current_input_id).val(state.naziv);
                data.elements[index][elem_key] = state;
              };
                
            },
            
            
          });
          
        });
        
        $(`#${data.rand_id}_elem_list_box .cit_input.string`).each( function(){
          
          $(this).off(`blur`);
          $(this).on(`blur`, function() {
            
            var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
            var data = this_module.cit_data[this_comp_id];
            var rand_id = data.rand_id;
            
            
            var under_score_arr = this.id.split(`_`);
            var random = under_score_arr[0];
            var sifra = under_score_arr[1];
            var key = this.id.replace( random + `_` + sifra + `_`, `` );
            
            /*
            var first_underscore = this.id.indexOf(`_`);
            var sifra = this.id.substring(0, first_underscore);
            var key = this.id.substring(first_underscore+1);
            
            */
            var index = find_index(data.elements, sifra , `sifra` );
            
            if ( index !== null ) {
              
              console.log(data.elements[index]);
              data.elements[index][key] = $(this).val() || null;
              console.log(data.elements[index]);
            };
            
          });
          
        });

        $(`#${data.rand_id}_elem_list_box .cit_input.number`).each( function(){
          
          $(this).off(`blur`);
          $(this).on(`blur`, function() {
            
            
            var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
            var data = this_module.cit_data[this_comp_id];
            var rand_id = data.rand_id;
            
            
            var under_score_arr = this.id.split(`_`);
            var random = under_score_arr[0];
            var sifra = under_score_arr[1];
            var key = this.id.replace( random + `_` + sifra + `_`, `` );
            
            
            // var first_underscore = this.id.indexOf(`_`);
            // var sifra = this.id.substring(0, first_underscore);
            // var key = this.id.substring(first_underscore+1);
            
            var index = find_index(data.elements, sifra , `sifra` );
            
            if ( index !== null ) {
              
              if ( $(this).val() == `` ) {
                data.elements[index][key] = null;
                
              } else {
                
                var decimal_num = 2;
                
                if ( this_module.valid[key]?.decimals || this_module.valid[key]?.decimals === 0 ) decimal_num = this_module.valid[key]?.decimals;
                
                var new_value = cit_number( $(this).val() );
                format_number_input(new_value, this, decimal_num )
                data.elements[index][key] = new_value;
              };
              
              console.log(data.elements[index]);
              
            };
            
          });
          
        });
        
        
        $(`#${data.rand_id}_elem_list_box .cit_text_area`).each( function(){
          
          $(this).off(`blur`);
          $(this).on(`blur`, function() {
            
            var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
            var data = this_module.cit_data[this_comp_id];
            var rand_id = data.rand_id;
            
            var under_score_arr = this.id.split(`_`);
            var random = under_score_arr[0];
            var sifra = under_score_arr[1];
            var key = this.id.replace( random + `_` + sifra + `_`, `` );
            
            /*
            var first_underscore = this.id.indexOf(`_`);
            var sifra = this.id.substring(0, first_underscore);
            var key = this.id.substring(first_underscore+1);
            
            */
            var index = find_index(data.elements, sifra , `sifra` );
            
            if ( index !== null ) {
              
              console.log(data.elements[index]);
              data.elements[index][key] = $(this).val() || null;
              console.log(data.elements[index]);
            };
            
          });
          
        });
        
        
        
        $.each( data.elements, function(e_ind, element) {
          
          
          
          // ------------------------ ELEM PLATES EVENTS ------------------------
          
          $(`#` + data.rand_id + `_` + element.sifra+`_elem_choose_plate`).data('cit_props', {
            
            desc: 'odabir montažne ploče za specifičan element ' + element.sifra,
            local: true,
            
            show_cols: [ 
              'naziv',
              'sirov_naziv',

              'color_C',
              'color_M',
              'color_Y',
              'color_K',
              'color_W',
              'color_V',

              'noz_big',
              'nest_count'
            ],
            custom_headers: [ 
              'MONT. PLOČA',
              'SIROVINA',
              'CYAN',
              'MAGENTA',
              'YELLOW',
              'KEY',
              'WHITE',
              'VARNISH',

              'NOŽ/BIG metara',
              'BROJ GNJEZDA'
            ],
            col_widths: [
              2, // 'naziv',
              4, // 'sirov_naziv',

              1, // 'color_C',
              1, // 'color_M',
              1, // 'color_Y',
              1, // 'color_K',
              1, // 'color_W',
              1, // 'color_V',

              1, // 'noz_big',
              1, // 'nest_count'
            ],

            return: {},
            show_on_click: true,
            list: function() {
              
              var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id = data.rand_id;
              
              var mont_ploce_for_list = [];

              $.each( data.mont_ploce, function( p_ind, plate ) {
                
                var {
                  _id, 
                  sifra,
                  naziv,
                  sirov,
                  
                  color_C,
                  color_M,
                  color_Y,
                  color_K,
                  color_W,
                  color_V,
                  
                  noz_big,
                  nest_count,
                } = plate;

                mont_ploce_for_list.push({ 
                  
                  plate_id: _id,
                  sifra,
                  naziv,
                  sirov,
                  sirov_naziv: sirov.full_naziv,

                  color_C,
                  color_M,
                  color_Y,
                  color_K,
                  color_W,
                  color_V,

                  noz_big,
                  nest_count,
                });

               
              });
              
              return mont_ploce_for_list;
              
            },
            cit_run: function(state) {
              
              var state = cit_deep(state);
              
              var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;
              
              var elem_sifra = current_input_id.replace(`_elem_choose_plate`, ``);
              elem_sifra = elem_sifra.replace(data.rand_id + `_`, ``);
              
              
              if ( state == null ) {
                $('#'+current_input_id).val(``);
                return;
              };
              
              var count_input = $(`#` + data.rand_id + `_` + elem_sifra + `_elem_plate_count`);
              
              if ( count_input.val() == `` ) {
                popup_warn(`Potrebno prvo upisati koliko stane elemenata na tu ploču !!!!`);
                return;
              };
              
              var count_input_num = cit_number( count_input.val() ); // ako nije broj onda returna null
              
              if ( count_input_num == null ) {
                
                popup_warn(`Broj elemenata na ploči nije numerička vrijednost !!!`);
                count_input.val(``);
                return;
                
              };
              
              var this_elem_index = find_index( data.elements, elem_sifra , `sifra` );  
              
              // ako ne postoji elem plates onda ga napravi
              if ( !data.elements[this_elem_index].elem_plates ) data.elements[this_elem_index].elem_plates = [];

              console.log( cit_deep( data.elements[this_elem_index].elem_plates ) );
              
              state.count = count_input_num;
              
              // reset polja
              count_input.val(``);
              
              data.elements[this_elem_index].elem_plates = upsert_item( data.elements[this_elem_index].elem_plates, state, `sifra`);
              
              console.log(state);
              
              console.log( cit_deep( data.elements[this_elem_index].elem_plates ) );
              
              this_module.make_element_list(data);
              

            },

          });
          
          $(`#`+element.sifra+`_elem_plate_count`).off(`blur`);
          $(`#`+element.sifra+`_elem_plate_count`).on(`blur`, function(){

            var this_comp_id = $(`#`+current_input_id).closest('.cit_comp')[0].id;
            var data = this_module.cit_data[this_comp_id];
            var rand_id =  this_module.cit_data[this_comp_id].rand_id;
            
            var elem_sifra = current_input_id.replace(`_elem_plate_count`, ``);

            var this_elem_index = find_index( data.elements, elem_sifra , `sifra` );  

           

          });
          
          $.each(element.elem_plates, function(e_ind, plate) {
        
            $(`#`+element.sifra + `_` + plate.sifra + `_delete_elem_plate`).off(`click`);
            $(`#`+element.sifra + `_` + plate.sifra + `_delete_elem_plate`).on(`click`, function() {


              var this_comp_id = $(this).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;

              var plate_sifra = this.id.replace(`_delete_elem_plate`, ``);
              var elem_sifra = $(this).attr(`data-element_sifra`);
              
              // dodatno obrisi elem sifru iz idja 
              plate_sifra = plate_sifra.replace( elem_sifra + `_`, ``);
              
              
              var this_elem_index = find_index( data.elements, elem_sifra , `sifra` );
              var this_plate_index = find_index( data.elements[this_elem_index].elem_plates, plate_sifra , `sifra` );
              

              data.elements[this_elem_index].elem_plates.splice(this_plate_index, 1);
              
              this_module.make_element_list(data);

            }); // kraj click eventa
            
          }); // loop po plates od elementa
          
          
          
          
          // ------------------------ ELEM PRESS EVENTS ------------------------
          
          $(`#` + data.rand_id + `_` + element.sifra + `_elem_choose_press`).data('cit_props', {
            
            desc: 'odabir multiple vrste tiska za specifičan element ' + element.sifra,
            local: true,
            show_cols: [ 'naziv' ],
            return: {},
            show_on_click: true,
            list: `elem_press`,
            cit_run: function(state) {
              
              var state = cit_deep(state);
              
              var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;
              
              var elem_sifra = current_input_id.replace(`_elem_choose_press`, ``);
              elem_sifra = elem_sifra.replace(data.rand_id + `_`, ``);
              
              
              if ( state == null ) {
                $('#'+current_input_id).val(``);
                return;
              };
              
              
              var this_elem_index = find_index( data.elements, elem_sifra , `sifra` );  
              
              // ako ne postoji elem press array onda ga napravi
              if ( !data.elements[this_elem_index].elem_press ) data.elements[this_elem_index].elem_press = [];

              console.log( cit_deep( data.elements[this_elem_index].elem_press ) );
              
              
              data.elements[this_elem_index].elem_press = upsert_item( data.elements[this_elem_index].elem_press, state, `sifra`);
              
              console.log(state);
              
              console.log( cit_deep( data.elements[this_elem_index].elem_press ) );
              
              this_module.make_element_list(data);
              

            },

          });
          
          $.each(element.elem_press, function(e_ind, press) {
        
            $(`#`+element.sifra + `_` + press.sifra + `_delete_elem_press`).off(`click`);
            $(`#`+element.sifra + `_` + press.sifra + `_delete_elem_press`).on(`click`, function() {


              var this_comp_id = $(this).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;

              var press_sifra = this.id.replace(`_delete_elem_press`, ``);
              var elem_sifra = $(this).attr(`data-element_sifra`);
              
              // dodatno obrisi elem sifru iz idja 
              press_sifra = press_sifra.replace( elem_sifra + `_`, ``);

              var this_elem_index = find_index( data.elements, elem_sifra , `sifra` );
              var this_press_index = find_index( data.elements[this_elem_index].elem_press, press_sifra , `sifra` );
              

              data.elements[this_elem_index].elem_press.splice(this_press_index, 1);
              
              this_module.make_element_list(data);

            }); // kraj click eventa
            
          }); // loop po press arr od elementa
          
          
          
          // ------------------------ ELEM ALAT EVENTS ------------------------
          
          // posve isto kao isto kao i elem find sirov ali sa dodatnim filterom da traži samo alate
          // posve isto kao isto kao i elem find sirov ali sa dodatnim filterom da traži samo alate
          
          $(`#` + data.rand_id + `_` + element.sifra + `_elem_choose_alat`).data('cit_props', {
            // !!!findsirov!!! ----> isto kao _elem_choose_alat iznad osim što ima query da traži samo alate
            desc: 'pretraga za odabir alata unutar pojedinog elementa ' + data.rand_id,
            local: false,
            url: '/find_sirov',
            find_in: [
              "sirovina_sifra", 
              "naziv",
              "full_naziv",
              "stari_naziv",
              "povezani_nazivi",
              "detaljan_opis",
              "grupa_flat",  

              "kvaliteta_1",
              "kvaliteta_2", 
              "kvaliteta_mix",

              "kontra_tok_x_tok",
              "val_x_kontraval",
              "alat_prirez",

              "dobavljac_flat", 
              "dobav_naziv", 
              "dobav_sifra", 
              "fsc_flat", 
              "povezani_kupci_flat", 
              "boja",   
              "zemlja_pod_flat",

              "specs_flat",

            ],
            query: { "klasa_sirovine.sifra": 'KS5' }, // traži samo alate !!!!!!!  
            return: {},

            /* ne upisujem širinu za _id jer sam napravio da ga preskočim posve */

            col_widths: [
              1, // "sirovina_sifra",
              2, // "full_naziv",
              2, // "full_dobavljac",
              1, // grupa_naziv
              1, // "boja",
              1, // "cijena",
              1, // "kontra_tok",
              1, // "tok",
              1, // "po_valu",
              1, // "po_kontravalu",
              2, // "povezani_nazivi",
              
              1, // "pk_kolicina",
              1, // "nk_kolicina",
              1, // "order_kolicina",
              1, // "sklad_kolicina"
              1, // last_status
              
              4, // spec_docs

            ],
            show_cols: [
              "sifra_link",
              "full_naziv",
              "full_dobavljac",
              "grupa_naziv",
              "boja",
              "cijena",
              "kontra_tok",
              "tok",
              "po_valu",
              "po_kontravalu",
              "povezani_nazivi",
              
              `pk_kolicina`,
              `nk_kolicina`,
              `order_kolicina`,
              `sklad_kolicina`,
              `last_status`,
              
              `spec_docs`, 

            ],
            custom_headers: [
              `ŠIFRA`,  // "sirovina_sifra",
              `NAZIV`,  // "full_naziv",
              `DOBAV.`,  // "full_dobavljac",
              `GRUPA`,  // "grupa_naziv",
              `BOJA`,  // "boja",
              `CIJENA`,  // "cijena",
              `KONTRA TOK`,  // "kontra_tok",
              `TOK`,  // "tok",
              `VAL`,  // "po_valu",
              `KONTRA VAL`,  // "po_kontravalu",
              `POVEZANO`,  // "povezani_nazivi",
              
              `PK RESERV`,  // "pk_kolicina",
              `NK RESERV`,  // "nk_kolicina",
              `NARUČENO`,  // "order_kolicina",
              `SKLADIŠTE`,  // "sklad_kolicina",
              `STATUS`,
              
              `DOCS`, 

            ],

            format_cols: {

              "sifra_link": "center",
              "grupa_naziv": "center",
              "boja": "center",
              "cijena": this_module.sirov_valid.cijena.decimals,
              "kontra_tok": this_module.sirov_valid.kontra_tok.decimals,
              "tok": this_module.sirov_valid.tok.decimals,
              "po_valu": this_module.sirov_valid.po_valu.decimals,
              "po_kontravalu": this_module.sirov_valid.po_kontravalu.decimals,

              "pk_kolicina": this_module.sirov_valid.pk_kolicina.decimals,
              "nk_kolicina": this_module.sirov_valid.nk_kolicina.decimals,
              "order_kolicina": this_module.sirov_valid.order_kolicina.decimals,
              "sklad_kolicina": this_module.sirov_valid.sklad_kolicina.decimals,

            },

            filter: this_module.find_sirov.find_sirov_filter,

            show_on_click: false,
            cit_run: async function(state) {

              
              var sir_specs = null;
              // trebam specs za alat zato što se tu nalaze filovi koji trebaju za radni nalog !!!!
              if ( state.specs ) sir_specs = cit_deep(state.specs);
              
              
              // obriši nepotrebne propse !!!
              var state = reduce_sir(state);
              
              var new_alat = {
                
                ...state, // stavio sam ipak cijelu sirovinu
                
                
                _id: state._id,
                sifra: `elemalat`+cit_rand(),
                sirovina_sifra: state.sirovina_sifra,
                full_naziv: state.full_naziv,
                last_status: state.last_status,
                
                specs: sir_specs || null,
                
                
              };
              
              

              var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;

              var elem_sifra = current_input_id.replace(`_elem_choose_alat`, ``);
              elem_sifra = elem_sifra.replace(data.rand_id + `_`, ``);

              if ( state == null ) {
                $('#'+current_input_id).val(``);
                return;
              };

              var this_elem_index = find_index( data.elements, elem_sifra , `sifra` );  

              // ako ne postoji elem alat array onda ga napravi
              if ( !data.elements[this_elem_index].elem_alat ) data.elements[this_elem_index].elem_alat = [];
              
              // console.log( cit_deep( data.elements[this_elem_index].elem_alat ) );
              
              /* umjesto  new_alat samo neki proprtije stavio sam state tj cijeli objekt alata */
              data.elements[this_elem_index].elem_alat = upsert_item( data.elements[this_elem_index].elem_alat, new_alat, `_id`); 

              // console.log(state);
              // console.log( cit_deep( data.elements[this_elem_index].elem_alat ) );

              this_module.make_element_list(data);

            },
          });
          
          $.each(element.elem_alat, function(a_ind, alat) {
        
            
            
            $(`#`+element.sifra + `_` + alat.sifra + `_delete_elem_alat`).off(`click`);
            $(`#`+element.sifra + `_` + alat.sifra + `_delete_elem_alat`).on(`click`, function() {


              var this_comp_id = $(this).closest('.cit_comp')[0].id;
              var data = this_module.cit_data[this_comp_id];
              var rand_id =  this_module.cit_data[this_comp_id].rand_id;

              var alat_sifra = this.id.replace(`_delete_elem_alat`, ``);
              var elem_sifra = $(this).attr(`data-element_sifra`);
              
              // dodatno obrisi elem sifru iz idja 
              alat_sifra = alat_sifra.replace( elem_sifra + `_`, ``);
              
              var this_elem_index = find_index( data.elements, elem_sifra , `sifra` );
              var this_alat_index = find_index( data.elements[this_elem_index].elem_alat, alat_sifra , `sifra` );
              

              data.elements[this_elem_index].elem_alat.splice(this_alat_index, 1);
              
              this_module.make_element_list(data);

            }); // kraj click eventa
            
          }); // loop po alat arr od elementa
          
          
          
        }); // loop po elementima
        
        data.elements_events_registered = true;
        
        
      }, 20*1000 );
      

    } else {
      // ako nema elemenata onda odmah trigeriraj da su eventi registrirani
      data.elements_events_registered = true;
    };
    
  };
  this_module.make_element_list = make_element_list;
  
    
  function update_rok_proiz( state, this_input ) {
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      data.rok_proiz = null;
      console.log(`update_rok_proiz: `, null );
      return;
    };
    
    data.rok_proiz = state;
    console.log("update_rok_proiz = ", new Date(state));
    
    this_module.update_rokove( this_input[0] );
    
    
  };
  this_module.update_rok_proiz = update_rok_proiz;  
  
    
  function update_rok_isporuke(state, this_input ) {
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      data.rok_isporuke = null;
      console.log(`update_rok_isporuke: `, null );
      return;
    };
    
    data.rok_isporuke = state;
    console.log("update_rok_isporuke = ", new Date(state));
    
    this_module.update_rokove(this_input[0]);
    
  };
  this_module.update_rok_isporuke = update_rok_isporuke;
  
  
  function update_rok_za_def(state, this_input ) {
      
    var this_comp_id = this_input.closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    if ( state == null || this_input.val() == ``) {
      this_input.val(``);
      data.rok_za_def = null;
      console.log(`rok_za_def: `, null );
      return;
    };
    
    data.rok_za_def = state;
    console.log("rok_za_def = ", new Date(state));
    
  };
  this_module.update_rok_za_def = update_rok_za_def;
  

  async function save_product() {

    var this_button = this;
    
    var kalk_mod = await get_cit_module(`/modules/kalk/kalk_module.js`, `load_css`);

    var this_comp_id = $(this_button).closest('.cit_comp')[0].id; // useo sam ovaj gumb bezveze  - nije bitno koji id
    var product_data = this_module.cit_data[this_comp_id];
    
    var project_comp_id =  $(this_button).closest('.cit_comp.project_comp')[0].id; 
    var project_data = this_module.project_module.cit_data[project_comp_id];
    
    // dodaj kupaca u prop partner unutar producta
    if ( project_data.kupac?._id ) this_module.cit_data[this_comp_id].partner = project_data.kupac;

    if ( !project_data._id ) {
      popup_warn(`Prvo morate spremiti ovaj Projekt !!!`);
      return;
    };

    
    product_data.project_id = project_data._id;
    
    
    $(this_button).css("pointer-events", "none");
    toggle_global_progress_bar(true);


    $.each( product_data, function( key, value ) {
      // ako je objekt ili array onda ga flataj
      if ( $.isPlainObject(value) || $.isArray(value) ) {
        product_data[key+'_flat'] = JSON.stringify(value);
      };
    });

    this_module.cit_data[this_comp_id] = save_metadata( product_data );
    product_data = this_module.cit_data[this_comp_id];
    
    // dodaj full ime producta !!!
    product_data.full_product_name = this_module.generate_full_product_name( product_data );
    product_data.full_kupac_name = project_data.kupac?.partner_sifra ? this_module.project_module.gen_full_kupac_name(project_data.kupac) : null;
    
    
    
    // -------------------------------- ako je kalk objekt i NEMA _id ONDA SPREMI KALK !!!!
    // -------------------------------- ako je kalk objekt i NEMA _id ONDA SPREMI KALK !!!!
    // -------------------------------- ako je kalk objekt i NEMA _id ONDA SPREMI KALK !!!!
    if ( 
      product_data.kalk // ako postoji
      &&
      product_data.kalk !== null // ako nije null
      &&
      typeof product_data.kalk !== "string" // ako nije string tj nije samo _id (u slučaju kada je već pretvoren u _id i  više nije objekt )
      &&
      !product_data.kalk?._id // ako nema _id ( ... ali je objekt )
    ) {
      
      var kalk_save_button = $(`#` + product_data.id + ` .save_offer_kalk_btn`)[0];
      
      var kalk_DB_id = await kalk_mod.save_kalk( null, kalk_save_button, `save_all` );
      
      product_data.kalk._id = kalk_DB_id;
      
    };
    
    
    // ažusriraj ime producta u kalkulaciji 
    if ( product_data._id ) {
      this_module.update_local_kalk_name(product_data);
      var updated_kalk_name = await kalk_mod.update_kalk_product_name( product_data._id, product_data.full_product_name );
    };
    
    var max_order_num = 0;
    $.each( project_data.items, function( item_ind, item ) {
      if ( item.order_num > max_order_num ) max_order_num = item.order_num;
    });
    
    if ( !product_data.order_num ) product_data.order_num = max_order_num + 100;

    
    var product_copy = cit_deep(product_data);
    

    
    // u product kopiju stavi samo partner / kupac _id a NE CIJELI OBJEKT
    if ( product_copy.partner?._id ) product_copy.partner = product_copy.partner._id;
    
    
    
    if ( product_copy.elements?.length > 0 ) {

      $.each( product_copy.elements, function( elem_ind, elem ) {
        
        // reducaj alate koji su takodjer objekti sirovina
        if ( elem.elem_alat?.length > 0 ) {
          $.each( elem.elem_alat, function( alat_ind, alat ) {
            product_copy.elements[elem_ind].elem_alat[alat_ind] = reduce_sir(alat);
          });  
        };
        

        // reducaj sirovinu unutar plates tj montažnih formi
        if ( elem.elem_plates?.length > 0 ) {
          $.each( elem.elem_plates, function( plate_ind, plate ) {
            if (plate.sirov) product_copy.elements[elem_ind].elem_plates[plate_ind].sirov = reduce_sir(plate.sirov);
          });  
        };

        
        
      });
      
    };
    
    
    
    
    // -------------------------------- UZMI SAMO IDJEVE OD STATUSA I KALKA
    // -------------------------------- UZMI SAMO IDJEVE OD STATUSA I KALKA
    // -------------------------------- UZMI SAMO IDJEVE OD STATUSA I KALKA

    var just_status_ids = [];
    $.each( product_copy.statuses, function(s_ind, status) {
      just_status_ids.push(status._id);
    });
    
    product_copy.statuses = just_status_ids;
    
    var no_proces_elements = [];
    $.each( product_copy.elements, function(e_ind, elem) {
      if ( !elem.elem_tip?.kalk ) no_proces_elements.push(elem);
    });
    
    product_copy.elements = no_proces_elements;
    
    
    var required = cit_required( this_module.valid, product_copy );

    if ( required.counter > 0 ) {
      popup_warn(`Niste upisali ove obavezne podatke:<br>${required.podaci}`);
      toggle_global_progress_bar(false);
      $(this_button).css("pointer-events", "all");
      return;
    };

    // ako je kalk objekt i ima _id to znači da je  već spremljen !!!
    // uzmi samo _id od kalk
    if ( product_copy.kalk !== null && product_copy.kalk?._id ) product_copy.kalk = product_copy.kalk._id;
    
    // uzmi samo _id od variant of
    if ( product_copy.variant_of && product_copy.variant_of._id  ) product_copy.variant_of = product_copy.variant_of._id;
    
    // uzmi samo _id od ORIGINAL
    if ( product_copy.original && product_copy.original._id  ) product_copy.original = product_copy.original._id;
    
    // -------------------------------- UZMI SAMO IDJEVE OD STATUSA I KALKA
    // -------------------------------- UZMI SAMO IDJEVE OD STATUSA I KALKA
    // -------------------------------- UZMI SAMO IDJEVE OD STATUSA I KALKA
    
    
    $.ajax({
      headers: {
        'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
      },
      type: "POST",
      cache: false,
      url: `/save_product`,
      data: JSON.stringify( product_copy ),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done(function (result) {

      console.log(result);
      
      if ( result.success == true ) {
        
        
        
        this_module.cit_data[this_comp_id]._id = result.product._id;
        this_module.cit_data[this_comp_id].proj_sifra = ( result.product.proj_sifra || null);
        this_module.cit_data[this_comp_id].item_sifra = ( result.product.item_sifra || null);
        this_module.cit_data[this_comp_id].variant = ( result.product.variant || null);
        this_module.cit_data[this_comp_id].sifra = ( result.product.sifra || null);
        
        // generiraj novo ime na osnovu nove sifre i varinat
        var new_full_name = this_module.generate_full_product_name( this_module.cit_data[this_comp_id] );
        this_module.cit_data[this_comp_id].full_product_name = new_full_name;
        
        $(`#${this_module.cit_data[this_comp_id].id} .product_name_placeholder`).text(new_full_name);
        
        
        var rand_id = this_module.cit_data[this_comp_id].rand_id;

        var variant = result.product.variant;
        $(`#`+rand_id+`_sifra`).val( result.product.sifra );

        // dodaj novo-nastali _id u html tj u data attribute
        var product_element = $(this_button).closest('.cit_comp'); 

        product_element.attr('data-product_id', result.product._id );
        product_element.attr('data-product_tip', result.product.tip.sifra );

        product_element.attr('data-proj_sifra', result.product.proj_sifra );
        product_element.attr('data-item_sifra', result.product.item_sifra );
        product_element.attr('data-variant', result.product.variant );
        product_element.attr('data-sifra', result.product.sifra );

        // pronađi kalkulaciju koja je za ovaj product unutar globalnog objekta i NAPRAVI UPDATE SVIH potrebnih informacija
        $.each( kalk_mod.cit_data, function( kalk_id_key, kalk_obj ) {
          
          if ( kalk_obj.product_id == this_module.cit_data[this_comp_id]._id ) {

            kalk_mod.cit_data[kalk_id_key].full_product_name = this_module.cit_data[this_comp_id].full_product_name;
            kalk_mod.cit_data[kalk_id_key].proj_sifra = this_module.cit_data[this_comp_id].proj_sifra;
            kalk_mod.cit_data[kalk_id_key].item_sifra = this_module.cit_data[this_comp_id].item_sifra;
            kalk_mod.cit_data[kalk_id_key].variant = this_module.cit_data[this_comp_id].variant;
            kalk_mod.cit_data[kalk_id_key].product_id = this_module.cit_data[this_comp_id]._id;

          };

        });
        
        // ubaci ovaj novi ili ažurirani product u array items unutar PROJEKTA
        project_data.items = upsert_item( project_data.items, result.product, `_id`);
        
        
        
        
        this_module.project_module.make_product_shortcuts(project_data);
        
        
        cit_toast(`PODACI SU SPREMLJENI!`);

        
        // spremi projekt
        setTimeout( function() { $(`#save_proj_btn`).trigger(`click`); }, 200 );
       
      } else {
        
        if ( result.msg ) popup_error(result.msg);
        if ( !result.msg ) popup_error(`Greška prilikom spremanja!`);
        
      };

    })
    .fail(function (error) {

      console.log(error);
      popup_error(`Došlo je do greške na serveru prilikom spremanja!`);
    })
    .always(function() {

      toggle_global_progress_bar(false);
      $(this_button).css("pointer-events", "all");

    });

  };
  this_module.save_product = save_product;

  
  
  function update_local_kalk_name(product_data) {

    if ( window[`/modules/kalk/kalk_module.js`] ) {
      // cit data nije array nego objekt i svaki key je HTML ID od svake učitane kalkulacije
      $.each( window[`/modules/kalk/kalk_module.js`].cit_data, function( kalk_id_key, kalk_data ) {
        if ( kalk_data.product_id == product_data._id ) {
          window[`/modules/kalk/kalk_module.js`].cit_data[kalk_id_key].full_product_name = this_module.generate_full_product_name( product_data );
        };
      });
    }; // ako postoji kalk module učitan u window

    
  };
  this_module.update_local_kalk_name = update_local_kalk_name;
  
  
  
  function delete_local_kalk(product_data) {
    

    if ( window[`/modules/kalk/kalk_module.js`] ) {
      
      if ( product_data._id ) {
        // cit data nije array nego objekt i svaki key je HTML ID od svake učitane kalkulacije
        
        $.each( window[`/modules/kalk/kalk_module.js`].cit_data, function( kalk_id_key, kalk_data ) {
          
          // ako product ima _id onda je taj _id isto upisan i u kalk cit data
          if ( kalk_data.product_id == product_data._id ) {
            delete window[`/modules/kalk/kalk_module.js`].cit_data[kalk_id_key];
          };

        });
      }
      else if ( product_data.id ) {
          
        var child_kalk_id = $(`#${product_data.id}_kalkulacija_box`).find(`.kalk_comp`)[0].id;
        // cit data nije array nego je objekt i samo trebam naći key koji odgovara HTML ID od child kalka unutra ovog producta kojeg brišem
        delete window[`/modules/kalk/kalk_module.js`].cit_data[child_kalk_id];
        
      };
        
      
    }; // kraj ako postoji kalk module učitan u window
    
  };
  this_module.delete_local_kalk = delete_local_kalk;
    
  
  
  function generate_full_product_name(data) {
    
    var nac_gp_sifra = ``;
    var template_nacrta = ``;

    if ( data.nacrt_specs && $.isArray(data.nacrt_specs) && data.nacrt_specs.length > 0 ) {

      var criteria = [ '!time' ];

      if ( typeof window == "undefined" ) {
        global.multisort( data.nacrt_specs, criteria );
      } else {
        multisort( data.nacrt_specs, criteria );
      };
      
      template_nacrta = data.nacrt_specs[0].template;

      nac_gp_sifra = template_nacrta;
      
      if ( data.nacrt_specs[0].graf_sifra ) {
        nac_gp_sifra = template_nacrta + "-" + data.nacrt_specs[0].graf_sifra;
      };
      
    };
    
    
    var full_product_name = 
        (data.sample_sifra ? data.sample_sifra+" " : "" ) +
        (data.sifra ? data.sifra+" " : "" ) +
        (nac_gp_sifra ? nac_gp_sifra+" " : "") + 
        (data.tip?.naziv || "" ) + " " +
        (data.duzina || "" ) + "x" +
        (data.sirina || "" ) + "x" +
        (data.visina || "" ) + " / " +
        (data.naziv || "" ) + " / " +
        (data.kupac_naziv || "" ) + " / " +
        (data.kupac_sifra || "" );
      
    return full_product_name;
  
  };
  this_module.generate_full_product_name = generate_full_product_name;
  

  
  async function open_variant_of( product ) {
    
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    var this_input = $('#'+current_input_id)[0];
    
    // kada user ručno obriše polje za variant of
    if ( product == null ) {
      
      $('#'+current_input_id).val(``);
      data.variant_of = null;
      
    } 
    else {
    
    var pop_text = 
`Jeste li sigurni da želite prekopirati podatke od ovog proizvoda?
<br>
PAZITE !!! - vremena roka isporuke će također biti iskopirana !!!
<br>
Svi podaci koje ste do sada upisali će biti obrisani !!!
`;
    
    async function variant_of_yes() {

      console.log(product);
      
      var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var project_comp_id =  $('#'+current_input_id).closest('.cit_comp.project_comp')[0].id; 
      var project_data = this_module.project_module.cit_data[project_comp_id];
      

      product.variant_of = cit_deep(product);

      
      product.variant = null;
      product.sifra = null;
      product.project_id = project_data._id || null; // ako već ima onda spremi njega
      product.proj_sifra = project_data.proj_sifra || null; // ako već ima onda spremi njega
      product.item_sifra = null;
      
      $('#'+current_input_id).val(product.full_product_name);
      
      $('#'+ data.id + ' .link_to_variant_of').attr(
        `href`, 
        `#project/${product.proj_sifra || null }/item/${product.item_sifra || null }/variant/${ product.variant || null }/status/null`
      );
      
      // trebam napraviti temp div koji mi služi da znam gdje postaviti novu verziju producta !!!
      var div_after_product = `<div id="${data.id}_after"></div>`;
      $(`#` + data.id).after( div_after_product );
      
      // obriši postojeći html od producta
      $(`#` + data.id).remove();
      
      var product_data = {
        
        ...data,
        ...product,
        mont_ploce: [],
        statuses: [], // resetiraj sve statuse
        
        is_copy: true,
        order_num: data.order_num || null,
        
        
        id: `prod`+cit_rand(), // napravi novi component id

      };
      
      // VAŽNO _id PROPERTY NE SMJE POSTOJATI  - čak i kad ima value null onda će se upisati kao null u bazu !!!!
      // VAŽNO _id PROPERTY NE SMJE POSTOJATI  - čak i kad ima value null onda će se upisati kao null u bazu !!!!
      // VAŽNO _id PROPERTY NE SMJE POSTOJATI  - čak i kad ima value null onda će se upisati kao null u bazu !!!!
      if ( product_data.hasOwnProperty('_id') ) delete product_data._id;
      
      
      product_data = await this_module.change_elem_i_kalk_sifre( 
        product_data,
        true, // remove_pro
        true, // remove_post
        false  // original_product_copy
      );
      
      
      this_module.create( product_data, $(`#` + data.id + `_after`), `before`);
      // remove ovaj div kojeg sam napravio ispod HTML od producta
      setTimeout( function() { $(`#` + data.id + `_after`).remove();  }, 200 );
      
      console.log(data);
      
    }; // kraj od yes function od popup

    function variant_of_no() {
      
      show_popup_modal(false, pop_text, null );
      $('#'+current_input_id).val(``);
      data.original = null;
      
    };

    var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
    var show_popup_modal = pop_mod.show_popup_modal;
    show_popup_modal(`warning`, pop_text, null, 'yes/no', variant_of_yes, variant_of_no, null);
      
    }; // kraj od elese (ako state nije null )
    
  };
  this_module.open_variant_of = open_variant_of;
  
  
  
  async function open_original(product) {
    
    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    if ( product == null ) {
      
      $('#'+current_input_id).val(``);
      data.original = null;
      
    } 
    else {
    
    var pop_text = `Jeste li sigurni da želite ponoviti nakladu za ovaj proizvod?`;
    
    async function original_yes() {

      console.log(product);
      
      var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var project_comp_id =  $('#'+current_input_id).closest('.cit_comp.project_comp')[0].id; 
      var project_data = this_module.project_module.cit_data[project_comp_id];
      
      var new_variant = await get_product_variant( product.item_sifra );
      
      if ( !new_variant.success ) {
        
        return;
      };
      
      product.original = cit_deep(product);
      
      // ----- moram resetirati sve datume za isporuku
      product.rok_isporuke = null;
      product.potrebno_dana_proiz = null;
      product.potrebno_dana_isporuka = null;
      product.rok_proiz = null;
      product.rok_za_def = null;

      
      product.variant = new_variant.variant;
      product.sifra = product.item_sifra + "-" + product.variant + ".";
      
      product.project_id = project_data._id || null;
      product.proj_sifra = project_data.proj_sifra || null;
      
      // ITEM ŠIFRA OSTAJE ISTA !!!!!!!!!
      // ITEM ŠIFRA OSTAJE ISTA !!!!!!!!!
      // ITEM ŠIFRA OSTAJE ISTA !!!!!!!!!
      // product.proj_sifra = product.item_sifra;
      
      
      $('#'+current_input_id).val(product.full_product_name);
      
      $('#'+ data.id + ' .link_to_original').attr(
        `href`, 
        `#project/${product.proj_sifra || null }/item/${product.item_sifra || null }/variant/${ product.variant || null }/status/null`
      );
      
      
      // trebam napraviti temp div koji mi služi da znam gdje postaviti novu verziju producta !!!
      var div_after_product = `<div id="${data.id}_after"></div>`;
      $(`#` + data.id).after( div_after_product );
      
      // obriši postojeći html od producta
      $(`#` + data.id).remove();
      
      
      var product_data = {
        
        ...data,
        ...product,
        mont_ploce: [],
        statuses: [], // resetiraj sve statuse
        
        is_copy: true,
        order_num: data.order_num || null,
        
        id: `prod`+cit_rand(),  // novi component id

      };
      
      // VAŽNO _id PROPERTY NE SMJE POSTOJATI  - čak i kad ima value null onda će se upisati kao null u bazu !!!!
      // VAŽNO _id PROPERTY NE SMJE POSTOJATI  - čak i kad ima value null onda će se upisati kao null u bazu !!!!
      // VAŽNO _id PROPERTY NE SMJE POSTOJATI  - čak i kad ima value null onda će se upisati kao null u bazu !!!!
      if ( product_data.hasOwnProperty('_id') ) delete product_data._id;
      
      product_data = await this_module.change_elem_i_kalk_sifre( 
        product_data,
        true, // remove_pro
        true, // remove_post
        true  // original_product_copy
      );
      
      this_module.create( product_data, $(`#` + data.id + `_after`), `before`);
      
      // obroši ovaj div kojeg sam napravio ispod HTML od producta
      setTimeout( function() { $(`#` + data.id + `_after`).remove(); }, 200 );
      
      console.log(data);
      
    }; // kraj od yes function od popup

    function original_no() {
      
      show_popup_modal(false, pop_text, null );
      $('#'+current_input_id).val(``);
      data.original = null;
      
    };

    var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
    var show_popup_modal = pop_mod.show_popup_modal;
    show_popup_modal(`warning`, pop_text, null, 'yes/no', original_yes, original_no, null);
      
    }; // kraj od elese (ako state nije null )
    
  };
  this_module.open_original = open_original;
  
  
  
  function copy_dodatne_RN( product_data, kalk_type ) {
    
    // kalk_type može biti pro_kalk ili post_kalk
    // kalk_type može biti pro_kalk ili post_kalk
    
    $.each( product_data.kalk[kalk_type][0].procesi, function(proces_ind, POP_proces) { 
      
      
      // POP proces je skraćeno od pro or post proces 
      // zato što kopiram dodatne radne nalog iz pro ili post kalkulacije !!!!
      
      var POP_proces_sifra = POP_proces.row_sifra;
      var proces_exists_in_offer = false;

      $.each( product_data.kalk.offer_kalk, function(offer_kalk_ind, offer_kalk) {

        // ako je ovo odabrana kalkulacija za proizvodnju
        if ( offer_kalk.kalk_for_pro == true ) {

          // usporedi sifru procesa iz pro_kalk sa procesom u offer_kalk
          $.each( offer_kalk.procesi, function( offer_proces_ind, offer_proces ) {

            if ( offer_proces.row_sifra == POP_proces_sifra ) proces_exists_in_offer = true;
          });

          // ako nije našao taj ISTI proces unutar offer kalkulacije onda ubaci taj proces
          if ( proces_exists_in_offer == false ) {

            product_data.kalk.offer_kalk[offer_kalk_ind].procesi.push( cit_deep(POP_proces) );
          };

        }; // ako je nasao offer kalk koji je za proizvodnju

      }); // loop po svim offer kalks

    }); // loop po  kalk PRO    


    
  };
  
  async function change_elem_i_kalk_sifre( product_data, remove_pro, remove_post, original_product_copy ) {
    
    var kalk_mod = await get_cit_module(`/modules/kalk/kalk_module.js`, `load_css`);
    
    if ( !original_product_copy ) {
      // obriši sve propertije unutar svakog elementa osim naslova tj tipa i šifri parenta i šifri elementa
      $.each( product_data.elements, function(e_ind, elem) {

        var new_elem = {

          add_elem: null,

          elem_val: null,
          elem_kontra: null,

          elem_parent: elem.elem_parent || null,

          elem_opis: ``,
          elem_noz_big: null,

          sifra: elem.sifra,
          elem_tip: elem.elem_tip,

          elem_press: null,
          elem_alat: null,

          elem_cmyk: ``,
          elem_pantone: ``,
          elem_vrsta_boka: null,
          elem_plates: null,

          elem_on_product: 1,

          elem_RB: null,
          elem_polica_razmak: null,
          elem_polica_pregrade: null,
          elem_polica_nosivost: null,
          elem_front_orient: null,

        };

        // prepiši postojeći element s ovim novim elementom
        product_data.elements[e_ind] = new_elem;

      });

    };
    
    
    
    
    // ------------------------- promjeni sve sifre na elementima 
    // ------------------------- promjeni sve sifre na elementima 
    // ------------------------- promjeni sve sifre na elementima 
    
    
    
    // --------------------------------- MOŽDA IPAK NE BI TREBAO BRISATI SIFRE ELEMENTATA ????
    // --------------------------------- MOŽDA IPAK NE BI TREBAO BRISATI SIFRE ELEMENTATA ????
    // --------------------------------- MOŽDA IPAK NE BI TREBAO BRISATI SIFRE ELEMENTATA ????
    
    /*
    
    $.each( product_data.elements, function(e_ind, elem) {

      var curr_elem_sifra = elem.sifra;
      var new_elem_sifra = `sel`+cit_rand();

      // promjeni sifre svih elemenata i sve parent sifre
      $.each( product_data.elements, function( e_ind_2, elem_2 ) {
        if ( elem_2.sifra == curr_elem_sifra ) product_data.elements[e_ind_2].sifra = new_elem_sifra;
        if ( elem_2.elem_parent == curr_elem_sifra ) product_data.elements[e_ind_2].elem_parent = new_elem_sifra;
      });
    
    }); // loop po svim elementima producta
    
    
    
    */
    
    // --------------------------------- MOŽDA IPAK NE BI TREBAO BRISATI SIFRE ELEMENTATA ????
    // --------------------------------- MOŽDA IPAK NE BI TREBAO BRISATI SIFRE ELEMENTATA ????
    // --------------------------------- MOŽDA IPAK NE BI TREBAO BRISATI SIFRE ELEMENTATA ????
    
    
    
    
    // ako product ima kalk
    if ( product_data.kalk ) {

      product_data.kalk.full_product_name = product_data.full_product_name;
      
      product_data.kalk.product_id = null;
      product_data.kalk.variant = product_data.variant;
      
      // samo vrati product data ako uopće ne postoji offer kalk !!!
      // samo vrati product data ako uopće ne postoji offer kalk !!!
      if ( product_data.kalk.offer_kalk?.length == 0 ) return product_data;
      
      // obriši ovaj kalk id tako da spremi novi kalk koji je kopija !!!
      // obriši ovaj kalk id tako da spremi novi kalk koji je kopija !!!
      // obriši ovaj kalk id tako da spremi novi kalk koji je kopija !!!
      delete product_data.kalk._id;
      
      if ( product_data.kalk?.meta_offer_kalk ) delete product_data.kalk.meta_offer_kalk;

      $.each( product_data.kalk.offer_kalk, function(offer_ind, off_kalk) {

        // POSTAVI NOVU ŠIFRU KALKA !!!!
        product_data.kalk.offer_kalk[offer_ind].kalk_sifra = `kalk`+cit_rand();

        
        /*
        if ( off_kalk.kalk_reserv ) {
          product_data.kalk.offer_kalk[offer_ind].old_reserv = true;
          product_data.kalk.offer_kalk[offer_ind].kalk_komentar = ` --- STARI PK --- ` + (off_kalk.kalk_komentar || "");
        };
        */

        if ( off_kalk.kalk_for_pro ) {
          product_data.kalk.offer_kalk[offer_ind].old_for_pro = true;
          product_data.kalk.offer_kalk[offer_ind].kalk_komentar = ` --- STARI NK --- ` + (off_kalk.kalk_komentar || "");
        };


        
      }); // loop po  kalk offer
      

      // pro kalk je samo jedan !!!
      if ( product_data.kalk?.pro_kalk?.length > 0 ) {
        
        // POSTAVI NOVU ŠIFRU KALKA !!!!
        product_data.kalk.pro_kalk[0].kalk_sifra = `kalk`+cit_rand();
        // UBACUJEM DODATNE PROCESE (ako ih ima) IZ PRO KALK U OFFER KALK
        
        copy_dodatne_RN( product_data, `pro_kalk`);
        
      };
      
      // post kalk je samo jedan !!!  
      if ( product_data.kalk?.post_kalk?.length > 0 ) {
        
        // POSTAVI NOVU ŠIFRU KALKA !!!!
        product_data.kalk.post_kalk[0].kalk_sifra = `kalk`+cit_rand();
        
        copy_dodatne_RN( product_data, `post_kalk`);
      };  
      
      
    }; // jel postoji kalk u productu

    
    if ( product_data.kalk?.meta_pro_kalk ) delete product_data.kalk.meta_pro_kalk;
    if ( product_data.kalk?.meta_post_kalk ) delete product_data.kalk.meta_post_kalk;

    
    if ( remove_pro ) {
      if ( product_data.kalk?.pro_kalk ) product_data.kalk.pro_kalk = [];
    };
    
    if ( remove_post ) {
      if ( product_data.kalk?.post_kalk  ) product_data.kalk.post_kalk = []; 
    };
    
    var ulaz_sir_ids = [];
    
    if ( product_data.kalk?.offer_kalk?.length > 0 ) {
      
      $.each( product_data.kalk.offer_kalk, function( k_ind, of_kalk ) {
    
        if ( of_kalk.kalk_for_pro ) {

          $.each( of_kalk.procesi, function( op_ind, op_proces ) {

            $.each( op_proces.ulazi, function( ul_ind, op_ulaz ) {
              if ( op_ulaz._id ) ulaz_sir_ids.push(op_ulaz._id);
            }); // loop po ulazima

          }); // loop po procesima

        }; // jel ovo kalk za proizvodnju

      }); // loop po svim offer kalks
      
    };
    
    var db_sirovine = null;
    
    var db_sirovine = await ajax_find_query( 
        { _id: { $in: ulaz_sir_ids }, on_date: (product_data?.rok_za_def || null)  },
        `/find_sirov`,
        this_module.find_sirov.find_sirov_filter 
      );
    
    
    var deficit_msg = false;
   
    
    $.each( db_sirovine, function(sir_ind, sirovina) {
      
      sirovina.forecast_kolicina = kalk_mod.cit_forecast( sirovina, null, product_data);
      
      if ( product_data.kalk?.offer_kalk?.length > 0 ) {
        // loop po svim offer kalkulacijama ------------------------------------------------------------------------
        $.each( product_data.kalk.offer_kalk, function( k_ind, of_kalk ) {

          // ako je offer plavi kalk za ODABRAN za proizvodnju
          if ( of_kalk.kalk_for_pro ) {
            // loop po svim offer plavim procesima za Koji su odabrani za proizvodnju
            $.each( of_kalk.procesi, function( op_ind, op_proces ) {

              // loop po svim ulazima
              $.each( op_proces.ulazi, function( ul_ind, op_ulaz ) {
                if (
                  op_ulaz._id == sirovina._id // ako ovaj ulaz ima current sirovinu u loopu
                  &&
                  kom_plus_sum_plus_extra(op_ulaz) < sirovina.forecast_kolicina // ako nema dovoljno količine
                ) {

                  // ako nema dovoljno sirovine onda stavi prop deficit
                  // ako nema dovoljno sirovine onda stavi prop deficit
                  product_data.kalk.offer_kalk[k_ind].procesi[op_ind].ulazi[ul_ind].deficit = true;
                  
                  if ( !deficit_msg ) {

                    deficit_msg = true; // stavljam ovo na true tako da mi se ova poruka ne ponavlja više puta !!!
                    popup_warn(
  `Nema dovoljno sirovina da bi se ponovila ista kalkulacija!!!<br><br>
  Sve sirovine kojih nema dovoljno su označene crvenom bojom.<br>
  Kopirajte kalkulaciju i probajte pronaći slične sirovine :)
  `);

                  };

                } else {
                  
                  product_data.kalk.offer_kalk[k_ind].procesi[op_ind].ulazi[ul_ind].deficit = false;
                  
                };

              }); // loop po ulazima

            }); // loop po procesima u kalkulaciji za proizvodnju

          }; // jel ovo kalk za proizvodnju

        }); // loop po svim offer kalks
        // ------------------------------------------------------------------------
      };
        
    }); // loop po sirovinama
    
    // samo poništi kalk reserv i kalk for pro ... i kalk_sample
    // samo poništi kalk reserv i kalk for pro ... i kalk_sample
    // samo poništi kalk reserv i kalk for pro ... i kalk_sample
    
    if ( product_data.kalk?.offer_kalk?.length > 0 ) {
      
      $.each( product_data.kalk.offer_kalk, function(offer_ind, off_kalk) {

        if ( off_kalk.kalk_reserv ) {
          product_data.kalk.offer_kalk[offer_ind].kalk_reserv = false;
        };

        if ( off_kalk.kalk_for_pro ) {
          product_data.kalk.offer_kalk[offer_ind].kalk_for_pro = false;
        };
        
        
        if ( off_kalk.kalk_sample ) {
          product_data.kalk.offer_kalk[offer_ind].kalk_sample = false;
          product_data.kalk.offer_kalk[offer_ind].sample_sifra = null;
        };

      }); // loop po kalk offer
      
    };

    return product_data;

  };
  this_module.change_elem_i_kalk_sifre = change_elem_i_kalk_sifre;
  
  
  
  function polja_za_f201(data) {
    
    var valid = this_module.valid;
    var rand_id =  data.rand_id;
    
    var html = `
  
          <div class="row">

          <div class="col-md-6 col-sm-12">
            <div class="row">

              <div class="col-md-4 col-sm-12">
                ${ cit_comp(rand_id+`_duzina`, valid.duzina, data.duzina) }
              </div>
              
              
              <div class="col-md-4 col-sm-12">
                ${ cit_comp(rand_id+`_sirina`, valid.sirina, data.sirina) }
              </div>
              
              <div class="col-md-4 col-sm-12">
                ${ cit_comp(rand_id+`_visina`, valid.visina, data.visina) }
              </div>

            </div>
          </div>
          
        </div> 
        
      `;
    
    return html;
    
  };
  this_module.polja_za_f201 = polja_za_f201;
  
  
  function polja_za_podni_stalak(data) {
    
    var valid = this_module.valid;
    var rand_id =  data.rand_id;
    
    var html = `
    
    

  
  `;    
    
    return html;  
    
    
  };
  this_module.polja_za_podni_stalak = polja_za_podni_stalak;
  
  
  
  function polja_za_podni_stalak(data) {

    var valid = this_module.valid;
    var rand_id =  data.rand_id;

    var html = `

    <div class="row">

      <div class="col-md-6 col-sm-12">
        <div class="row">

          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_duzina`, valid.duzina, data.duzina) }
          </div>

          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_sirina`, valid.sirina, data.sirina) }
          </div>

          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_visina`, valid.visina, data.visina) }
          </div>


        </div>
      </div>

      <div class="col-md-6 col-sm-12">
        <div class="row">


          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_out_duzina`, valid.out_duzina, data.out_duzina) }
          </div> 

          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_out_sirina`, valid.out_sirina, data.out_sirina) }
          </div>

          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_visina_header_ext`, valid.visina_header_ext, data.visina_header_ext) }
          </div>

          <div class="col-md-3 col-sm-12">
            ${ cit_comp(rand_id+`_broj_polica`, valid.broj_polica, data.broj_polica) }
          </div>

        </div>
      </div>

    </div>  
  `;    

  return html;  


};
  this_module.polja_za_podni_stalak = polja_za_podni_stalak;
  
  
  
  async function duplicate_product(e) {
    
    
    e.stopPropagation();
    e.preventDefault();
    
    var this_button = this;
    
    var pop_text = `Jeste li sigurni da želite duplicirati ovaj proizvod?`;
    
    async function duplicate_product_yes() {

      var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;

      var project_comp_id =  $(this_button).closest('.cit_comp.project_comp')[0].id; 
      var project_data = this_module.project_module.cit_data[project_comp_id];

      if ( !data._id ) {
        popup_warn(`Ne možete duplicirati proizvod ako ga niste prvo spremili!!!`);
        return;
      };

      var product = cit_deep(data);

      
      product.variant = null;
      product.sifra = null;


      product.project_id = project_data._id || null; // ako već ima onda spremi njega
      product.proj_sifra = project_data.proj_sifra || null; // ako već ima onda spremi njega
      product.item_sifra = null;

      
      
      // trebam napraviti temp div koji mi služi da znam gdje postaviti novu verziju producta !!!
      var div_after_product = `<div id="${data.id}_after"></div>`;
      $(`#` + data.id).after( div_after_product );
      

      var product_data = {

        ...data,
        ...product,
        mont_ploce: [],
        statuses: [], // resetiraj sve statuse
        
        is_copy: true,
        
        order_num: null,
        
        id: `prod`+cit_rand(), // napravi novi component id

      };


      // VAŽNO _id PROPERTY NE SMJE POSTOJATI  - čak i kad ima value null onda će se upisati kao null u bazu !!!!
      // VAŽNO _id PROPERTY NE SMJE POSTOJATI  - čak i kad ima value null onda će se upisati kao null u bazu !!!!
      // VAŽNO _id PROPERTY NE SMJE POSTOJATI  - čak i kad ima value null onda će se upisati kao null u bazu !!!!
      if ( product_data.hasOwnProperty('_id') ) delete product_data._id;


      product_data = await this_module.change_elem_i_kalk_sifre( 
        product_data,
        true, // remove_pro
        true, // remove_post
        false  // original_product_copy
      );

  
      // this_module.create( product_data, $(`#proj_stavke_line`), `before`);
      
      this_module.create( product_data, $(`#` + data.id + `_after`), `before`);
      // remove ovaj div kojeg sam napravio ispod HTML od producta
      setTimeout( function() { $(`#` + data.id + `_after`).remove();  }, 200 );
      
      
    }; // kraj od yes function od popup

    function duplicate_product_no() {
      
      show_popup_modal(false, pop_text, null );
      
    };

    var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
    var show_popup_modal = pop_mod.show_popup_modal;
    show_popup_modal(`warning`, pop_text, null, 'yes/no', duplicate_product_yes, duplicate_product_no, null);
      
    
    
  };
  this_module.duplicate_product = duplicate_product;
  
  
  async function delete_product(e) {
    
    e.stopPropagation();
    e.preventDefault();
    
    var this_button = this;
    $(this_button).css("pointer-events", "none");
    
    
    var offer_or_pro = false;
    
    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    
    if ( data.kalk?.offer_kalk?.length > 0 ) {
      
      
      $.each( data.kalk.offer_kalk, function(of_ind, of_kalk) {
        if ( of_kalk.kalk_reserv || of_kalk.kalk_for_pro ) offer_or_pro = true;
      });  
    };
    
    
    if ( offer_or_pro ) {
      popup_warn(`Ne možete obrisati proizvod koji ima REZERVACIJU PO PONUDI, NK ILI JE U STADIJU UZORKA ILI STADIJU PROIZVODNJE !!!`);
      $(this_button).css("pointer-events", "all");
      return;
    };
    
    
    var pop_text = `Jeste li sigurni da želite obrisati ovaj Proizvod?`;
    
    async function delete_product_yes() {

      
      var this_comp_id = $(this_button).closest('.cit_comp.product_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      // obriši child kalk koji je unutar ovog producta !!!!
      // obriši child kalk koji je unutar ovog producta !!!!
      this_module.delete_local_kalk(data);
      
      
      function local_delete(data) {
        
          cit_toast(`PROIZVOD JE OBRISAN!`);

          $(`#`+data.id).remove();

          // obriši ovaj cijeli objekt unutar modula
          delete this_module.cit_data[data.id];

          setTimeout( function() {
            $('.tooltip.show').remove();
            $('.cit_tooltip').tooltip('dispose').tooltip({ boundary: 'window' });
          }, 500 );
        
      };
      
      // ako product ima _id onda ga trebam obrisati u bazi
      if ( data._id ) {
        
        
        toggle_global_progress_bar(true);

        $.ajax({
          headers: {
            'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
          },
          type: "POST",
          cache: false,
          url: `/delete_product`,
          data: JSON.stringify( { _id: data._id } ),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
        .done( function (result) {

          console.log(result);

          if ( result.success == true ) {
            
            local_delete(data);

          } else {

            if ( result.msg ) popup_error(result.msg);
            if ( !result.msg ) popup_error(`Greška prilikom brisanja proizvoda!!!`);
          };

        })
        .fail(function (error) {

          resolve(error);
          console.log(error);
          popup_error(`Došlo je do greške na serveru prilikom brisanja proizvoda !!!`);
        })
        .always(function() {

          toggle_global_progress_bar(false);
          $(this_button).css("pointer-events", "all");

        });

        
      // kraj ako product ima _id i trabam ga obrisati u remote bazi
      }
      else {
        local_delete(data);
      }; 
      
    }; // kraj od yes function od popup

    function delete_product_no() {
      
      show_popup_modal(false, pop_text, null );
      $('#'+current_input_id).val(``);
      data.original = null;
      
    };

    var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
    var show_popup_modal = pop_mod.show_popup_modal;
    show_popup_modal(`warning`, pop_text, null, 'yes/no', delete_product_yes, delete_product_no, null);

    
  };
  this_module.delete_product = delete_product;
  
  
  
  
  
  async function save_mont_plate() {
    
    
    var this_button = this;
    
    $(this_button).css("pointer-events", "none");
    
    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    if ( !data.current_mont_plate ) data.current_mont_plate = {};

    if ( !data.current_mont_plate.sirov ) {
      $(this_button).css("pointer-events", "all");
      popup_warn(`Potrebno je izabrati ploču !!!!`);
      return;
    };
    
    // ovako vidim jel gumb za save NEW plate
    // ili je gumb za AŽURIRANJE / UPDATE ( onda ima class update_mont_plate_btn )
    
    if ( $(this_button).hasClass(`save_mont_plate_btn`) ) {
      
      if ( !data.current_mont_plate.sifra ) data.current_mont_plate.sifra = `PLT`+cit_rand();
    
      var counter = await get_new_counter(`plates`, false);

      if (!counter) {
        popup_error(`Greška prilikom kreiranja ID broja za montažnu ploču !!!!`);
        $(this_button).css("pointer-events", "all");
        return;
      };
    
      data.current_mont_plate.naziv = cit_dt( Date.now(), 'y-m-d' ).date + "-" + counter.real_number;
      
    };
    
    data.current_mont_plate.color_C = data.plate_C || null;
    data.current_mont_plate.color_M = data.plate_M || null;
    data.current_mont_plate.color_Y = data.plate_Y || null;
    data.current_mont_plate.color_K = data.plate_K || null;
    data.current_mont_plate.color_W = data.plate_W || null;
    data.current_mont_plate.color_V = data.plate_V || null;
    
    data.current_mont_plate.noz_big = data.plate_noz_big || null;
    data.current_mont_plate.nest_count = data.plate_nest_count || null;
    
    
    // ovo sam već napravio server side ali potrebno je da ubacim ovaj product i lokalno u montažni plate !!!
    // ovo sam već napravio server side ali potrebno je da ubacim ovaj product i lokalno u montažni plate !!!
    if ( !data.current_mont_plate.products ) data.current_mont_plate.products = [];
    
    var this_product = { 
      full_product_name: data.full_product_name,
      product_id: data._id,
    };
    
    data.current_mont_plate.products = upsert_item( data.current_mont_plate.products, this_product, `product_id`);
    
    var result = await save_plate_in_db(data.current_mont_plate, data, this_button);
    
    if ( result.success == true ) {
      
      data.current_mont_plate._id = result.plate._id;
      data.mont_ploce = upsert_item(data.mont_ploce, data.current_mont_plate, `sifra`);
      
      data.mont_ploce = cit_deep(data.mont_ploce);
      
      this_module.make_mont_plate_list(data);
      
      // osvježi i listu elemenata
      this_module.make_element_list(data);
      
      data.current_mont_plate = null;
      
      $('#'+rand_id+`_elem_find_sirov`).val("");
      
      $('#'+rand_id+`_plate_C`).val("");
      $('#'+rand_id+`_plate_M`).val("");
      $('#'+rand_id+`_plate_Y`).val("");
      $('#'+rand_id+`_plate_K`).val("");
      $('#'+rand_id+`_plate_W`).val("");
      $('#'+rand_id+`_plate_V`).val("");
      
      $('#'+rand_id+`_plate_noz_big`).val("");
      $('#'+rand_id+`_plate_nest_count`).val("");
      
      
      $(`#${data.id} .save_mont_plate_btn`).css(`display`, `flex`);
      $(`#${data.id} .update_mont_plate_btn`).css(`display`, `none`);
      
    };
    
  };
  this_module.save_mont_plate = save_mont_plate;
  
  
  function make_mont_plate_list(data) {
    
    data.mont_ploce_for_table = [];

    $.each( data.mont_ploce, function( p_ind, plate ) {

      var {
        sifra,
        naziv,
        sirov,
        
        color_C,
        color_M,
        color_Y,
        color_K,
        color_W,
        color_V,
        
        
        noz_big,
        nest_count,
      } = plate;

      data.mont_ploce_for_table.push({ 
        sifra,
        naziv,
        sirov,
        sirov_naziv: sirov.full_naziv,
        
        color_C,
        color_M,
        color_Y,
        color_K,
        color_W,
        color_V,
        
        noz_big,
        nest_count,
        
      
      });

    });

    var mont_plate_props = {
      desc: 'samo za kreiranje tablice svih montažnih ploča za ovaj product',
      local: true,

      list: data.mont_ploce_for_table,

      show_cols: [ 
        'naziv',
        'sirov_naziv',
        
        'color_C',
        'color_M',
        'color_Y',
        'color_K',
        'color_W',
        'color_V',
        
        'noz_big',
        'nest_count',
        
        'button_edit'
      ],
      custom_headers: [ 
        'MONT. PLOČA',
        'SIROVINA',
        
        'CYAN',
        'MAGENTA',
        'YELLOW',
        'KEY',
        'WHITE',
        'VARNISH',
        
        'NOŽ/BIG metara',
        'BROJ GNJEZDA',
        
        'UREDI'
      ],
      col_widths: [
        2, // 'naziv',
        4, // 'sirov_naziv',
        
        1, // 'color_C',
        1, // 'color_M',
        1, // 'color_Y',
        1, // 'color_K',
        1, // 'color_W',
        1, // 'color_V',
        
        1, // 'noz_big',
        1, // 'nest_count'
        
        1, // 'button_edit'
        
      ],
      parent: `#${data.id} .mont_plate_list_box`,
      return: {},
      show_on_click: false,
      
      format_cols: {
        
        naziv: "center",
        
        
        'color_C': "center",
        'color_M': "center",
        'color_Y': "center",
        'color_K': "center",
        'color_W': "center",
        'color_V': "center",
        
        nest_count: "center",
        noz_big: "center",
      },
      
      button_edit: function(e) { 

        e.stopPropagation(); 
        e.preventDefault();

        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var index = find_index(data.mont_ploce, sifra , `sifra` );

        
        data.current_mont_plate = cit_deep( data.mont_ploce[index] );
        

        $(`#${data.id} .save_mont_plate_btn`).css(`display`, `none`);
        $(`#${data.id} .update_mont_plate_btn`).css(`display`, `flex`);


        data.plate_C = data.current_mont_plate.color_C || null;
        data.plate_M = data.current_mont_plate.color_M || null;
        data.plate_Y = data.current_mont_plate.color_Y || null;
        data.plate_K = data.current_mont_plate.color_K || null;
        data.plate_W = data.current_mont_plate.color_W || null;
        data.plate_V = data.current_mont_plate.color_V || null;

        data.plate_noz_big = data.current_mont_plate.noz_big || null;
        data.plate_nest_count = data.current_mont_plate.nest_count || null;
        
        
        $('#'+rand_id+`_plate_noz_big`).val( data.current_mont_plate.noz_big || "");
        $('#'+rand_id+`_plate_nest_count`).val( data.current_mont_plate.nest_count || "");
        
        $('#'+rand_id+`_plate_C`).val( data.current_mont_plate.color_C || "");
        $('#'+rand_id+`_plate_M`).val( data.current_mont_plate.color_M || "");
        $('#'+rand_id+`_plate_Y`).val( data.current_mont_plate.color_Y || "");
        $('#'+rand_id+`_plate_K`).val( data.current_mont_plate.color_K || "");
        $('#'+rand_id+`_plate_W`).val( data.current_mont_plate.color_W || "");
        $('#'+rand_id+`_plate_V`).val( data.current_mont_plate.color_V || "");
        
        $('#'+rand_id+`_elem_find_sirov`).val( data.current_mont_plate.sirov.full_naziv || "");
        

        console.log("kliknuo edit MONTAŽNE FORME !!!");


      },
      

      cit_run: function(state) { console.log(state); },


    };
    if ( data.mont_ploce_for_table.length > 0 ) create_cit_result_list(data.mont_ploce_for_table, mont_plate_props );

  };
  this_module.make_mont_plate_list = make_mont_plate_list;

  
  
  function save_plate_in_db(plate, data, this_button) {
    
    
    return new Promise( function(resolve, reject) {


      var plate_obj = cit_deep(plate);
      
      plate_obj = save_metadata( plate_obj );


      toggle_global_progress_bar(true);
      
      $(this_button).css("pointer-events", "none");


      $.ajax({
        headers: {
          'X-Auth-Token': ( window.cit_user ? window.cit_user.token : 'pp_request')
        },
        type: "POST",
        cache: false,
        url: `/save_plate`,
        data: JSON.stringify( plate_obj ),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done( function (result) {

        console.log(result);

        if ( result.success == true ) {

          resolve(result);

          cit_toast(`MONTAŽNA PLOČA SPREMLJENA!`);

        } else {
          if ( result.msg ) popup_error(result.msg);
          if ( !result.msg ) popup_error(`Greška prilikom spremanja montažne ploče!`);
          
          resolve(null);
          
        };

      })
      .fail(function (error) {

        resolve(error);
        console.log(error);
        popup_error(`Došlo je do greške na serveru prilikom montažne ploče !!!`);
      })
      .always(function() {

        toggle_global_progress_bar(false);
        $(this_button).css("pointer-events", "all");

      });

      
    }); // kraj promisa  
    
    
  };
  this_module.save_plate_in_db = save_plate_in_db;
  
  
  
  function update_kalk_docs(files, button, arg_time) {
        
    var this_comp_id = $(button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    var new_docs_arr = [];
    
    $.each(files, function(f_index, file) {

      var file_obj = {
        sifra: `doc`+cit_rand(),
        time: Date.now(),
        link: `<a href="/docs/${time_path(arg_time)}/${file.filename}" target="_blank" onClick="event.stopPropagation();" >${ file.originalname }</a>`,
      };
      new_docs_arr.push( file_obj );
    });

    console.log( new_docs_arr );

    // napravi novi objekt ako current ne postoji
    if ( !window.current_prod_specs ) window.current_prod_specs = {};


    if ( $.isArray(window.current_prod_specs.docs) ) {
      // ako postoji docs array onda dodaj nove filove
      window.current_prod_specs.docs = [ ...window.current_prod_specs.docs, ...new_docs_arr ];
    } else {
      // ako ne postoji onda stavi ove nove filove
      window.current_prod_specs.docs = new_docs_arr;
    };    

  };
  this_module.update_kalk_docs = update_kalk_docs;
  
  
  function update_nacrt_docs( files, button, arg_time ) {
    
    
    var this_comp_id = $(button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    

    var new_docs_arr = [];
    
    $.each(files, function(f_index, file) {

      var file_obj = {
        
        user: {
          "_id" : window.cit_user._id,
          "name" : window.cit_user.name,
          "user_number" : window.cit_user.user_number,
          "full_name" : window.cit_user.full_name,
          "email" : window.cit_user.email,
        },
        
        sifra: `doc`+cit_rand(),
        time: Date.now(),
        link: `<a href="/docs/${time_path(arg_time)}/${file.filename}" target="_blank" onClick="event.stopPropagation();" >${ window.cit_user.full_name + ` ` + file.originalname }</a>`,
      };
      new_docs_arr.push( file_obj );
      
    });

    console.log( new_docs_arr );

    // napravi novi objekt ako current ne postoji
    if ( !data.current_nacrt_specs ) data.current_nacrt_specs = {};

    if ( $.isArray(data.current_nacrt_specs.docs) ) {
      // ako postoji docs array onda dodaj nove filove
      data.current_nacrt_specs.docs = [ ...data.current_nacrt_specs.docs, ...new_docs_arr ];
    } else {
      // ako ne postoji onda stavi ove nove filove
      data.current_nacrt_specs.docs = new_docs_arr;
    };    

  };
  this_module.update_nacrt_docs = update_nacrt_docs;
  
  
  function update_graf_docs(files, button, arg_time ) {

    var this_comp_id = $(button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    var new_docs_arr = [];

    $.each(files, function(f_index, file) {

      var file_obj = {

        user: {
          "_id" : window.cit_user._id,
          "name" : window.cit_user.name,
          "user_number" : window.cit_user.user_number,
          "full_name" : window.cit_user.full_name,
          "email" : window.cit_user.email,
        },

        sifra: `doc`+cit_rand(),
        time: Date.now(),
        link: 
        `<a href="/docs/${time_path(arg_time)}/${file.filename}" target="_blank" onClick="event.stopPropagation();" >${ window.cit_user.full_name + ` ` + file.originalname }</a>`,
      };
      new_docs_arr.push( file_obj );

    });

    console.log( new_docs_arr );

    // napravi novi objekt ako current ne postoji
    if ( !data.current_nacrt_specs ) data.current_nacrt_specs = {};

    if ( $.isArray(data.current_nacrt_specs.graf_docs) ) {
      // ako postoji docs array onda dodaj nove filove
      data.current_nacrt_specs.graf_docs = [ ...data.current_nacrt_specs.graf_docs, ...new_docs_arr ];
    } else {
      // ako ne postoji onda stavi ove nove filove
      data.current_nacrt_specs.graf_docs = new_docs_arr;
    };    

  };
  this_module.update_graf_docs = update_graf_docs;

  
  
  function save_current_prod_specs() {
    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    var comp_id = rand_id+`_kalk_docs`;
    
    
    // vrati nazad da gumb za save dokumenata  bude vidljiv
    // ionako ću sakriti parent i cijelu listu obrisati
    $(`#${comp_id}_upload_btn`).css(`display`, `flex`);
    
    // obriši listu
    $(`#${comp_id}_list_items`).html( "" );
    // sakrij listu i button
    $(`#${comp_id}_list_box`).css( `display`, `none` );
    
    
    if ( !window.current_prod_specs ) window.current_prod_specs = {};
    
    if ( !window.current_prod_specs.sifra ) window.current_prod_specs.sifra = `kalkspecs`+cit_rand();
    
    window.current_prod_specs.komentar = $('#'+rand_id+`_prod_specs_komentar`).val();
    
    window.current_prod_specs.time = window.current_prod_specs.time || Date.now(); 
    
    
    if ( !window.current_prod_specs.komentar  ) {
      popup_warn(`Potrebno je upisati kratki opis za dokumente !!!`);
      return;
    };
    
    
    if ( !window.current_prod_specs.docs ) window.current_prod_specs.docs = null;
    
    data.prod_specs = upsert_item(data.prod_specs, window.current_prod_specs, `sifra`);
    
    data.prod_specs_flat = JSON.stringify(data.prod_specs);
    
    this_module.make_prod_specs_list(data);
    
    window.current_prod_specs = null;
    
    $('#'+rand_id+`_prod_specs_komentar`).val("");
    
    if ( $(this).hasClass(`update_prod_specs`) ) {
      $(this).css(`display`, `none`);
      $(`#${data.id} .save_prod_specs`).css(`display`, `flex`);
    };
    
  };
  this_module.save_current_prod_specs = save_current_prod_specs;
  
  
  async function save_current_nacrt_specs() {
    

    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    
    var nacrt_comp_id = rand_id+`_nacrt_docs`;
    // vrati nazad da gumb za save dokumenata  bude vidljiv
    // ionako ću sakriti parent i cijelu listu obrisati
    $(`#${nacrt_comp_id}_upload_btn`).css(`display`, `flex`);
    // obriši listu
    $(`#${nacrt_comp_id}_list_items`).html( "" );
    // sakrij listu i button
    $(`#${nacrt_comp_id}_list_box`).css( `display`, `none` );
    
    
    var graf_comp_id = rand_id+`_graf_docs`;
    // vrati nazad da gumb za save dokumenata  bude vidljiv
    // ionako ću sakriti parent i cijelu listu obrisati
    $(`#${graf_comp_id}_upload_btn`).css(`display`, `flex`);
    // obriši listu
    $(`#${graf_comp_id}_list_items`).html( "" );
    // sakrij listu i button
    $(`#${graf_comp_id}_list_box`).css( `display`, `none` );
    
    
    if ( !data.current_nacrt_specs ) data.current_nacrt_specs = {};
    
    data.current_nacrt_specs.komentar = esc_html( $('#'+rand_id+`_nacrt_komentar`).val() );
    data.current_nacrt_specs.graf_komentar = esc_html( $('#'+rand_id+`_graf_komentar`).val() );
    
    if ( !data.current_nacrt_specs.komentar  ) {
      popup_warn(`Potrebno je upisati kratki opis nacrta !!!`);
      return;
    };


    if ( !data.current_nacrt_specs.tip  ) {
      popup_warn(`Potrebno je upisati TIP nacrta !!!`);
      return;
    };


    if ( !data.current_nacrt_specs.docs  ) {
      popup_warn(`Potrebno je dodati barem jedan dokument za nacrt !!!`);
      return;
    };
    

    if ( !data.current_nacrt_specs.template ) {
      
      var nacrt_tip = ``;
      
      // if ( data.current_nacrt_specs.tip.sifra == `S` ) nacrt_tip = `nacrt_s`;
      // if ( data.current_nacrt_specs.tip.sifra == `K` ) nacrt_tip = `nacrt_k`;
      // if ( data.current_nacrt_specs.tip.sifra == `P` ) nacrt_tip = `nacrt_p`;
      
      var counter_result = await get_new_counter(`nacrt`, false);
      
      data.current_nacrt_specs.template = `NAC` + counter_result.number + data.current_nacrt_specs.tip.sifra + `1`;
      data.current_nacrt_specs.template_num = counter_result.number;
      data.current_nacrt_specs.sub = 1;
      
    };
    
    
    if ( 
      data.current_nacrt_specs.graf_docs            && 
      $.isArray(data.current_nacrt_specs.graf_docs) && 
      data.current_nacrt_specs.graf_docs.length > 0 &&
      !data.current_nacrt_specs.graf_sifra
    ) {
      
      var graf_result = await get_new_counter(`graf`, false);
      data.current_nacrt_specs.graf_sifra = `GP` + graf_result.number;
      
    };
    
    
    if ( data.current_nacrt_specs.new_version ) {
      
      var sub_counter = await get_new_sub_nacrt( data.current_nacrt_specs.template_num, data.current_nacrt_specs.tip.sifra );
      
      data.current_nacrt_specs.template = `NAC` + data.current_nacrt_specs.template_num + data.current_nacrt_specs.tip.sifra + sub_counter.sub;
      data.current_nacrt_specs.template_num = data.current_nacrt_specs.template_num;
      data.current_nacrt_specs.sub = sub_counter.sub;
      
    };
    
    if ( !data.current_nacrt_specs?.sifra ) data.current_nacrt_specs.sifra = `nacrt` + cit_rand();
    

    data.current_nacrt_specs.time = data.current_nacrt_specs.time || Date.now(); 
    

    data.nacrt_specs = upsert_item(data.nacrt_specs, data.current_nacrt_specs, `sifra`);

    data.nacrt_specs_flat = JSON.stringify(data.nacrt_specs);

    this_module.make_nacrt_specs_list(data);
    

    $('#'+rand_id+`_nacrt_tip`).prop( "disabled", false );
    $('#'+rand_id+`_nacrt_tip`).val("");
    
    $('#'+rand_id+`_nacrt_komentar`).val("");
    $('#'+rand_id+`_nacrt_sifra`).val("");
    
    
    $('#'+rand_id+`_graf_komentar`).val("");
    $('#'+rand_id+`_graf_sifra`).val("");
    
    
    if ( $(this).hasClass(`update_nacrt_specs`) ) {
      $(this).css(`display`, `none`);
      $(`#${data.id} .save_nacrt_specs`).css(`display`, `flex`);
    };
    

    setTimeout( function() {
      $(`#${data.id} .save_product_btn`).trigger(`click`);
      data.current_nacrt_specs = null;  
    }, 200 );
    

  };
  this_module.save_current_nacrt_specs = save_current_nacrt_specs;
  
  

  function make_prod_specs_list(data) {

    // SPECS OD KALKULACIJA

    var specs_for_table = [];

    if ( data.prod_specs && data.prod_specs.length > 0 ) {

      $.each(data.prod_specs, function(index, spec_obj ) {

        var { sifra, time, komentar } = spec_obj;

        var all_docs = "";
        if ( spec_obj.docs && $.isArray(spec_obj.docs) ) {


          var criteria = [ '!time' ];
          multisort( spec_obj.docs, criteria );

          $.each(spec_obj.docs, function(doc_index, doc) {
            var doc_html = 
            `
            <div class="docs_row">
              <div class="docs_date">${ cit_dt(doc.time).date_time }</div>
              <div class="docs_link">${ doc.link }</div>
            </div>
            `;

            all_docs += doc_html;

          }); // kraj loopa po docs

        }; // kraj ako ima docs

        specs_for_table.push({ sifra, time, komentar, docs: all_docs });

      }); // kraj loopa svih adresa

    };



    var prod_specs_props = {

      desc: 'samo za kreiranje tablice svih specs kalulacija i ostalih dokumenata unutar producta ',
      local: true,

      list: specs_for_table,

      show_cols: [ "komentar", "time", "docs", 'button_edit', 'button_delete' ],
      format_cols: { time: "date_time" },
      col_widths: [

        4, // "komentar",
        2, // "time",
        4, // "docs",
        1, // 'button_edit',
        1, // 'button_delete'

      ],
      parent: `#${data.rand_id}_prod_specs_box`,
      return: {},
      show_on_click: false,
      cit_run: function(state) { console.log(state); },

      button_edit: function(e) { 

        e.stopPropagation(); 
        e.preventDefault();

        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var index = find_index(data.prod_specs, sifra , `sifra` );

        window.current_prod_specs = cit_deep( data.prod_specs[index] );


        $(`#${data.id} .save_prod_specs`).css(`display`, `none`);
        $(`#${data.id} .update_prod_specs`).css(`display`, `flex`);

        $('#'+rand_id+`_prod_specs_komentar`).val( window.current_prod_specs.komentar || "");

        console.log("kliknuo edit kalk specs !!!");


      },
      
      
      button_delete: async function(e) {

        e.stopPropagation();
        e.preventDefault();

        var this_button = this;

        console.log("kliknuo DELETE kalk specs ");

        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);

          data.prod_specs = delete_item(data.prod_specs, sifra , `sifra` );

          $(`#`+parent_row_id).remove();

        };


        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj dokument?`, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj dokument?`, null, 'yes/no', delete_yes, delete_no, null);


      },

    };
    if ( specs_for_table.length > 0 ) create_cit_result_list(specs_for_table, prod_specs_props );


  };
  this_module.make_prod_specs_list = make_prod_specs_list;

  
  function make_nacrt_specs_list(data) {

    // SPECS OD NACRTA
    
    
    $(`#${data.rand_id}_nacrt_specs_box`).html(``);
    
    var specs_for_table = [];

    if ( data.nacrt_specs && data.nacrt_specs.length > 0 ) {
      
      
      var criteria = [ '!time' ];
      multisort( data.nacrt_specs, criteria );
    

      $.each(data.nacrt_specs, function(index, spec_obj ) {
        
        

        var { template, sifra, time, komentar, graf_komentar, graf_sifra, graf_docs } = spec_obj;

        
        // ------------------------------ NACTI DOCS ------------------------------
        var all_docs = "";
        
        if ( spec_obj.docs && $.isArray(spec_obj.docs) ) {
          var criteria = [ '!time' ];
          multisort( spec_obj.docs, criteria );

          $.each(spec_obj.docs, function(doc_index, doc) {
            var doc_html = 
            `
            <div class="docs_row">
              <div class="docs_date">${ cit_dt(doc.time).date_time }</div>
              <div class="docs_link">${ doc.link }</div>
            </div>
            `;

            all_docs += doc_html;

          }); // kraj loopa po docs

        }; // kraj ako ima docs
        
        // ------------------------------ NACTI DOCS ------------------------------
        
        
        
        
        // ------------------------------ GRAF DOCS ------------------------------
        var graf_docs = "";
        
        if ( spec_obj.graf_docs && $.isArray(spec_obj.graf_docs) ) {
          
          var criteria = [ '!time' ];
          multisort( spec_obj.graf_docs, criteria );

          $.each(spec_obj.graf_docs, function(doc_index, doc) {
            
            var graf_doc_html = 
            `
            <div class="docs_row">
              <div class="docs_date">${ cit_dt(doc.time).date_time }</div>
              <div class="docs_link">${ doc.link }</div>
            </div>
            `;

            graf_docs += graf_doc_html;

          }); // kraj loopa po graf docs

        }; // kraj ako ima graf docs
        
        // ------------------------------ GRAF DOCS ------------------------------
        
        
        
        var button_version = `<div class="result_row_version_btn"><i class="far fa-copy"></i>NOVA VERZIJA</div>`;

        specs_for_table.push({ 
          
          sifra, 
          template,
          tip_naziv: spec_obj.tip.naziv,
          
          time, 
          
          komentar,
          docs: all_docs,
          
          graf_sifra,
          graf_komentar,
          graf_docs,
          
          button_version: button_version,
        });

      }); // kraj loopa svih adresa

    };

    var nacrt_specs_props = {

      desc: 'samo za kreiranje tablice svih specs NACRTA unutar producta ',
      local: true,

      list: specs_for_table,

      show_cols: [ 
        
        "tip_naziv",
        "time",
        
        "template",
        "komentar",
        "docs",
        
        "graf_sifra",
        "graf_komentar",
        "graf_docs",
        
        'button_version',
        'button_edit',
      ],
      
      custom_headers: [
        
        "TIP", // "tip_naziv",
        "TIME", // "time",
        
        "ŠIFRA", // "template",
        "NACRT KOMENTAR", // "komentar",
        "NACRT DOKUMENTI", // "docs",
        
        "GP ŠIFRA", // "graf_sifra",
        "GP KOMENTAR", // "graf_komentar",
        "GP DOKUMENTI", // "graf_docs",
        
        
        "NOVA VERZ.", // 'button_version',
        "EDIT", // 'button_edit',
        
      ],
      
      format_cols: { 
        
        time: "date_time",
        template: "center",
        graf_sifra: "center",
        
        
      },
      
      
      col_widths: [
        
        1, // "tip_naziv",
        1, // "time",
        
        1, // "template",
        3, // "komentar",
        4, // "docs",
        
        1, // "graf_sifra",
        3, // "graf_komentar",
        4, // "graf_docs",
        
        
        2, // 'button_version',
        1, // 'button_edit',
        
      ],
      parent: `#${data.rand_id}_nacrt_specs_box`,
      return: {},
      show_on_click: false,
      cit_run: function(state) { console.log(state); },

      button_edit: function(e) { 

        e.stopPropagation(); 
        e.preventDefault();

        var this_comp_id = $(this).closest('.cit_comp')[0].id;
        var data = this_module.cit_data[this_comp_id];
        var rand_id =  this_module.cit_data[this_comp_id].rand_id;

        var parent_row_id = $(this).closest('.search_result_row')[0].id;
        var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
        var index = find_index(data.nacrt_specs, sifra , `sifra` );

        data.current_nacrt_specs = cit_deep( data.nacrt_specs[index] );

        $(`#${data.id} .save_nacrt_specs`).css(`display`, `none`);
        $(`#${data.id} .update_nacrt_specs`).css(`display`, `flex`);
        
        
        $('#'+rand_id+`_nacrt_komentar`).val( un_esc_html( data.current_nacrt_specs.komentar || "" ) ) ;
        $('#'+rand_id+`_graf_komentar`).val( un_esc_html( data.current_nacrt_specs.graf_komentar || "")  );
        
        
        $('#'+rand_id+`_nacrt_sifra`).val( data.current_nacrt_specs.template || "");
        $('#'+rand_id+`_graf_sifra`).val( data.current_nacrt_specs.graf_sifra || "");
        
        
        
        
        $('#'+rand_id+`_nacrt_tip`).prop( "disabled", true );
        

        console.log("kliknuo edit nacrt specs !!!");


      },
      
      
      
      /*
      ------------------------------------------------------------------------------------------
      button_delete: async function(e) {

        e.stopPropagation();
        e.preventDefault();

        var this_button = this;

        console.log("kliknuo DELETE kalk specs ");

        function delete_yes() {

          var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
          var data = this_module.cit_data[this_comp_id];
          var rand_id =  this_module.cit_data[this_comp_id].rand_id;

          var parent_row_id = $(this_button).closest('.search_result_row')[0].id;
          var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);

          data.nacrt_specs = delete_item(data.nacrt_specs, sifra , `sifra` );

          $(`#`+parent_row_id).remove();

        };


        function delete_no() {
          show_popup_modal(false, `Jeste li sigurni da želite obrisati ovaj NACRT?`, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, `Jeste li sigurni da želite obrisati ovaj NACRT?`, null, 'yes/no', delete_yes, delete_no, null);

      },
      
      ------------------------------------------------------------------------------------------
      */
      
      
      

    };
    if ( specs_for_table.length > 0 ) {
      create_cit_result_list(specs_for_table, nacrt_specs_props );
      
      wait_for(`$("#${data.rand_id}_nacrt_specs_box .result_row_version_btn").length > 0`, function() {
        
        $(`#${data.rand_id}_nacrt_specs_box .result_row_version_btn`).each( function() {
          
          $(this).off(`click`);
          $(this).on(`click`, function(e) {
      
            e.stopPropagation(); 
            e.preventDefault();
            
            console.log("kliknuo button new version");

            var this_comp_id = $(this).closest('.cit_comp')[0].id;
            var data = this_module.cit_data[this_comp_id];
            var rand_id =  this_module.cit_data[this_comp_id].rand_id;

            var parent_row_id = $(this).closest('.search_result_row')[0].id;
            var sifra = parent_row_id.substr( parent_row_id.lastIndexOf("_") + 1, 10000000);
            
            var index = find_index(data.nacrt_specs, sifra , `sifra` );
            data.current_nacrt_specs = cit_deep( data.nacrt_specs[index] );
            
            // jedina razlika od EDIT je da ovdje KOPIRAM NOVI NACRT OBJEKT SA DRUGOM ŠIFROM ALI ISTIM TEMPLATEOM
            // ... I TAKOĐER BRIŠEM SVE DOKUMENTE
            // --------------------------------------------------------------------------------------------------------
            
            data.current_nacrt_specs.new_version = true;
            data.current_nacrt_specs.sifra = `nacrt` + cit_rand();
            
            data.current_nacrt_specs.docs = [];
            data.current_nacrt_specs.graf_docs = [];
            data.current_nacrt_specs.graf_sifra = null;
            data.current_nacrt_specs.graf_komentar = null;
            
            
            data.current_nacrt_specs.time = null; // resertiraj vrijeme tako da upiše novo !!!
            
            $('#'+rand_id+`_nacrt_tip`).val( data.current_nacrt_specs.tip.naziv );
            
            $('#'+rand_id+`_nacrt_sifra`).val( data.current_nacrt_specs.template || "" );
            $('#'+rand_id+`_graf_sifra`).val( data.current_nacrt_specs.graf_sifra || "" );
            
            
            // --------------------------------------------------------------------------------------------------------
            
            
            // $(`#${data.id} .save_nacrt_specs`).css(`display`, `none`);
            // $(`#${data.id} .update_nacrt_specs`).css(`display`, `flex`);

            $('#'+rand_id+`_nacrt_komentar`).val( "NOVA VERZIJA: " );

            
          });
          
          
        });
        
      }, 5*1000);
    };

  };
  this_module.make_nacrt_specs_list = make_nacrt_specs_list;
  
  
  function update_rokove(elem) {
    
    var this_comp_id = $(elem).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    if ( 
      elem.id.indexOf(`_potrebno_dana_proiz`) > -1 
      ||
      elem.id.indexOf(`_potrebno_dana_isporuka`) > -1
    ) {

      if ( 
        data.rok_isporuke !== null
      ) {

        
        var rok_proiz = work_time_reverse( data.rok_isporuke, data.potrebno_dana_isporuka );
        var rok_za_def = work_time_reverse( data.rok_isporuke, (data.potrebno_dana_proiz || 0) + (data.potrebno_dana_isporuka || 0) );
        
        
        $('#'+rand_id+`_rok_proiz`).val( cit_dt(rok_proiz).date_time );
        data.rok_proiz = Number(rok_proiz);
        
        $('#'+rand_id+`_rok_za_def`).val( cit_dt(rok_za_def).date_time );
        data.rok_za_def = Number(rok_za_def);

      } else {

        $('#'+rand_id+`_rok_proiz`).val("");
        data.rok_proiz = null;
        
        $('#'+rand_id+`_rok_za_def`).val("");
        data.rok_za_def = null;

      };

    };
    
    
    if ( 
      elem.id.indexOf(`_rok_isporuke`) > -1 
    ) {

      if ( 
        data.potrebno_dana_proiz    !== null     &&
        data.potrebno_dana_isporuka !== null
        
      ) {

        var rok_za_def = work_time_reverse( data.rok_isporuke, (data.potrebno_dana_proiz || 0) + (data.potrebno_dana_isporuka || 0) );
        
        var rok_proiz = work_time_reverse( data.rok_isporuke, data.potrebno_dana_isporuka );
        
        $('#'+rand_id+`_rok_proiz`).val( cit_dt(rok_proiz).date_time );
        data.rok_proiz = Number(rok_proiz);
        
        $('#'+rand_id+`_rok_za_def`).val( cit_dt(rok_za_def).date_time );
        data.rok_za_def = Number(rok_za_def);

      } else {

        $('#'+rand_id+`_rok_proiz`).val("");
        data.rok_proiz = null;
        
        $('#'+rand_id+`_rok_za_def`).val("");
        data.rok_za_def = null;

      };

    };
    
    
    
    
    
    if ( 
      elem.id.indexOf(`_rok_proiz`) > -1   &&
      
      data.potrebno_dana_proiz    !== null &&
      data.potrebno_dana_isporuka !== null

    ) {

      
      var rok_isporuke = work_time_forward( data.rok_proiz, data.potrebno_dana_isporuka );
        
      var rok_za_def = work_time_reverse( data.rok_proiz, data.potrebno_dana_proiz );
      

      $('#'+rand_id+`_rok_isporuke`).val( cit_dt(rok_isporuke).date_time );
      data.rok_isporuke = Number(rok_isporuke);
      
      $('#'+rand_id+`_rok_za_def`).val( cit_dt(rok_za_def).date_time );
      data.rok_za_def = Number(rok_za_def);
      
    };
    
    
    
  };
  this_module.update_rokove = update_rokove;
  
  
  function nacrt_find_filter(items, jQuery) {

    var $ = jQuery;
    
    function cit_dt(number, date_format) {

    var date = null;

    // ako nije undefined ili null onda napravi datum od broja 
    if ( number || number === 0 ) {
      date = new Date( number );  
    } else {
      date = new Date();  
    };

    var sek = date.getSeconds();
    var min = date.getMinutes();
    var hr = date.getHours();
    var dd = date.getDate();
    var mm = date.getMonth() + 1; // January is 0!
    var yyyy = date.getFullYear();

    // ako je samo jedna znamenka
    if (dd < 10) {
      dd = '0' + dd;
    };
    // ako je samo jedna znamenka
    if (mm < 10) {
      mm = '0' + mm;
    };

    // ako je samo jedna znamenka
    if (sek < 10) {
      sek = '0' + sek;
    };


    // ako je samo jedna znamenka
    if (min < 10) {
      min = '0' + min;
    };

    // ako je samo jedna znamenka
    if (hr < 10) {
      hr = '0' + hr;
    };

    if ( !date_format || date_format == 'd.m.y' ) {
      date =  dd + '.' + mm + '.' + yyyy;
    };


    // ako nije neveden format ili ako je sa crticama
    if ( date_format == 'y_m_d' ) {
      date = yyyy + '_' + mm + '_' + dd;
    };  

    // ako nije neveden format ili ako je sa crticama
    if ( date_format == 'y-m-d' ) {
      date = yyyy + '-' + mm + '-' + dd;
    };


    time = hr + ':' + min + ':' + sek;


    return {
      date: date,
      time: time,
      date_time: date + " " + time,
    }

  };
    function insert_uniq(array, new_item, prop ) {

      var array_copy = cit_deep(array);

      let found_index = null;
      $.each( array_copy, function(index, element) {

          if ( element[prop] == new_item[prop] ) {
            found_index = index;
          };

        });

      // samo ako nije našao objekt s istim propertijem
      if ( found_index == null ) {
        if ( !array_copy || $.isArray(array_copy) == false ) array_copy = [];
        array_copy.push(new_item);
      };

      return array_copy;

    };
    
    var new_items = [];
    
    if ( items && $.isArray(items) && items.length > 0 ) {
      
      var criteria = [ '!saved', '!edited' ];

      if ( typeof window == "undefined" ) {
        global.multisort( items, criteria );
      } else {
        multisort( items, criteria );
      };

      $.each( items, function(index, item) {
      
        if ( 
          item.nacrt_specs 
          &&
          $.isArray(item.nacrt_specs) 
          &&
          item.nacrt_specs.length > 0 
        ) {

          // sortiraj tako da prvi bude najnoviji !!!!

          var criteria = [ '!time' ];

          if ( typeof window == "undefined" ) {
            global.multisort( item.nacrt_specs, criteria );
          } else {
            multisort( item.nacrt_specs, criteria );
          };


          $.each( item.nacrt_specs, function( n_ind, nacrt )  {

            var template_nacrta =  nacrt.template;
            var nacrt_komentar = nacrt.komentar;
            var graf_komentar = nacrt.graf_komentar;
            var nacrt_docs = ``;
            var graf_docs = ``;

            if ( nacrt.docs && $.isArray(nacrt.docs) && nacrt.docs.length > 0 ) {

              var doc_criteria = [ '!time' ];

              if ( typeof window == "undefined" ) {
                global.multisort( nacrt.docs, criteria );
              } else {
                multisort( nacrt.docs, criteria );
              };

              $.each( nacrt.docs, function(doc_index, doc) {
                var doc_html = 
                `
                <div class="docs_row">
                  <div class="docs_date">${ cit_dt(doc.time).date_time }</div>
                  <div class="docs_link">${ doc.link }</div>
                </div>
                `;
                nacrt_docs += doc_html;

              }); // kraj loopa po docs


            }; // kraj ako ima docs u sebi

            if ( nacrt.graf_docs && $.isArray(nacrt.graf_docs) && nacrt.graf_docs.length > 0 ) {

              var doc_criteria = [ '!time' ];

              if ( typeof window == "undefined" ) {
                global.multisort( nacrt.graf_docs, criteria );
              } else {
                multisort( nacrt.graf_docs, criteria );
              };

              $.each( nacrt.graf_docs, function(doc_index, graf_doc) {
                var doc_html = 
                `
                <div class="docs_row">
                  <div class="docs_date">${ cit_dt(graf_doc.time).date_time }</div>
                  <div class="docs_link">${ graf_doc.link }</div>
                </div>
                `;
                graf_docs += doc_html;

              }); // kraj loopa po docs


            }; // kraj ako ima docs u sebi



            var new_item = {  

              ...item,
              nacrt_komentar: nacrt_komentar || null,
              graf_komentar: graf_komentar || null,
              template_nacrta: template_nacrta || null,
              nacrt_docs: nacrt_docs || null,
              graf_docs: graf_docs || null,

            };

            new_items = insert_uniq( new_items, new_item, `template_nacrta` );

          }); // kraj loopa po nacrt specs


          // kraj ifa ako ima nacrt specs 
          
        }
        else {
          
          var new_item = {  
            ...item,
            nacrt_komentar: null,
            graf_komentar: null,
            template_nacrta: null,
            nacrt_docs: null,
            graf_docs: null,

          };
          
          new_items = insert_uniq( new_items, new_item, `_id` );
          
        };
        

      }); // kraj loopa po svim items
    
    }; // kraj ako postoje items i ako su array

    return new_items;

  };
  this_module.nacrt_find_filter = nacrt_find_filter;
  
  
  async function get_modules() {
    this_module.project_module = await get_cit_module(`/modules/project/project_module.js`, `load_css`);
  };
  this_module.get_modules = get_modules;
  get_modules();

  
  
  
  
  this_module.cit_loaded = true;
 
} // end of module scripts
};
