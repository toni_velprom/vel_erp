
var module_object = {
  
  create: function (data, parent_element, placement) {
   
  var this_module = window[module_url]; 
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };
    
  this_module.set_init_data(data);
  
    var { id } = data;

    // samo skraćujem jer mi se neda pisati :)
    var w = window;
    
    var component_html =
`

<div id="${id}">

<!--
  <input class="cit_input" type="file" id="cit_upload" multiple hidden />
  <label class="blue_btn" for="cit_upload">Choose file</label>
-->
  
    <div id="upload_avatar_box">
      <input class="cit_input" type="file" onchange="cit_show_avatar(this);" id="cit_avatar_up" hidden />
      <label id="cit_avatar_up_label" for="cit_avatar_up">
        <i class="fal fa-user-robot"></i>
      </label>
    </div>
    
    <div id="choose_avatar_text" class="cit_label" style="">
      <span id="new_avatar_span">Izaberi svoju ikonu</span>
      <span id="edit_avatar_span" style="display: none;">Promijeni svoju ikonu</span>
    </div> 

    <button class="blue_btn" id="update_avatar_icon" style="display: none; margin-bottom: 50px;">SPREMI NOVU IKONU</button>
    

    <div id="user_settings_box" class="cit_hide">
      <div class="cit_user_buttons" id="user_logout_button">
        <i class="fal fa-power-off"></i>
      </div>
      <div class="cit_user_buttons" id="cit_user_settings">
        <i class="fal fa-user-cog"></i>
      </div>
    </div>    
  

  <div id="cit_login_form" class="user_form" style="height: 0; overflow: hidden;" >   

    <div class="cit_label" id="login_user_full_name_label">Nadimak</div>
    <input class="cit_input" id="login_user_name" type="text" autocomplete="off" required>

    <div class="cit_label" id="login_user_pass_label">Lozinka</div>
    <input class="cit_input" id="login_user_pass" type="password" autocomplete="off" required>
    
    <button class="blue_btn" id="user_login_button">LOGIN</button>

  </div>

  <div id="cit_register_form" class="user_form">  
  
    
    <div class="cit_label" id="register_user_full_name_label">Nadimak</div>
    <input class="cit_input" id="register_user_name" type="text" autocomplete="off" required>

    <div class="cit_label" id="register_user_full_name_label"> Ime i prezime </div>
    <input class="cit_input" id="register_user_full_name" type="text" autocomplete="off" required>

    <div class="cit_label" id="register_user_email_label">E-mail</div>
    <input class="cit_input" id="register_user_email" type="email" autocomplete="off" required>

    <div class="cit_label" id="register_user_pass_label">Lozinka</div>
    <input class="cit_input" id="register_user_pass" type="password" autocomplete="off" required>  

    <div class="cit_label" id="register_repeat_user_pass_label">Ponovite lozinku</div>
    <input class="cit_input" id="register_repeat_user_pass" type="password" autocomplete="off" required>  

    <button class="blue_btn" id="user_register_button"> REGISTRACIJA </button>

    

  </div>

  <div id="cit_reset_pass_form" class="user_form" style="height: 0; overflow: hidden;" >  


    <div class="cit_label" id="reset_pass_user_email_label">E-mail</div>
    <input class="cit_input" id="reset_pass_user_email" type="email" autocomplete="off" required>

    <div class="cit_label" id="reset_pass_user_pass_label">Nova Lozinka</div>
    <input class="cit_input" id="reset_pass_user_pass" type="password" autocomplete="off" required>  

    <div class="cit_label" id="reset_pass_repeat_user_pass_label">Ponovite Novu Lozinku</div>
    <input class="cit_input" id="reset_pass_repeat_user_pass" type="password" autocomplete="off" required>  
   
   
    <button   class="blue_btn cit_tooltip" id="send_email_reset_user_or_pass"
              data-toggle="tooltip" data-placement="bottom" data-html="true" title="Na na gore upisanu email adresu će Vam biti poslan email koji morate potvrditi. Tek nakon klika na gumb u mailu moći ćete koristiti novu lozinku!">
    POŠALJI MAIL ZA POTVRDU
    </button>



  </div>
  
  <button class="blue_btn_text" id="no_account">Nemam račun</button>
  <button class="blue_btn_text" id="have_account">Već imam račun</button>
  <button class="blue_btn_text" id="forgot_pass">Zaboravio sam lozinku</button>
  
  <br>
  <br>
  
  <div class="cit_label" id="login_user_card_label">KARTICA</div>
  <input class="cit_input" id="login_user_card" type="password" autocomplete="off">
    
  <br>
  <br>
  <br> 


</div>
`;         
    
    

  wait_for(`$('#cit_login_form').length > 0`, function () {

    $('.cit_tooltip').tooltip();

    var this_module = window[module_url];

    var popup_modal = window['/preload_modules/site_popup_modal/site_popup_modal.js'];

    var show_popup_modal = popup_modal.show_popup_modal;

    this_module.check_user();

    if ( !window.cit_user) this_module.show_cit_form(`#cit_login_form`);
    
    if (window.cit_user) this_module.monitor_finger_click(true);

    
    
    $('#card_input_select').off('click');
    $('#card_input_select').on('click', function() {
      
      
      // ako je neka poruka VIDLJIVA onda ju sakrij
      if ( $(`#layer_under_cit_popup_modal`).hasClass(`cit_show`) ) {
        show_popup_modal(false, ` `, null, false, null, null, null);
      };
      
      
      focus_on_card_input();
      
      setTimeout( function() {
      
        var popup_modal = window['/preload_modules/site_popup_modal/site_popup_modal.js'];
        var show_popup_modal = popup_modal.show_popup_modal;
        
        
        show_popup_modal(true, `<h1>PRISLONI KARTICU<br>NA ČITAČ</h1>`, null, false, null, null, null);

        // NAKON 1 sec SAKRIJ PORUKU !!!
        setTimeout( function() {
          show_popup_modal(false, `<h1>PRISLONI KARTICU<br>NA ČITAČ</h1>`, null, false, null, null, null); 
        }, 3000);

        
      }, 400 );  
      
      
    });
    
    

    async function update_avatar() {
      
      var avatar_response = await upload_avatar(true);
      
      if ( avatar_response.success == false ) {
        
        if ( avatar_response.msg ) popup_error(avatar_response.msg);
        
      } else {
        
        window.cit_user.avatar = `/img/avatars/${avatar_response.filename}`;
        window.localStorage.setItem('cit_user', JSON.stringify(window.cit_user));
        
        var avatar_img = `<img src="${window.cit_user.avatar}" />`
        $(`#avatar_image_box`).removeClass(`cit_user`);
        setTimeout( function() {
          $(`#avatar_image_box`).html(avatar_img).addClass(`cit_user`);
        }, 300);
        
      };   
      
    };
    
    
    $('#update_avatar_icon').off('click');
    $('#update_avatar_icon').on('click', function (e) {

      e.stopPropagation();
      e.preventDefault();
      
      update_avatar();
        
    });

    
    

    $('#user_login_button').off('click');
    $('#user_login_button').on('click', function (e) {

      e.stopPropagation();
      e.preventDefault();
      this_module.user_login();
    });


    $('#user_register_button').off('click');
    $('#user_register_button').on('click', function (e) {

      e.stopPropagation();
      e.preventDefault();
      this_module.user_registration();
    });

    /* //////////////////////// EVENT LSITENER PRESS ENTER KEY ON PASS FIELD (to login) //////////////////////// */
    $('#login_user_pass').off('keypress');
    $("#login_user_pass").on('keypress', function (e) {

      /*
      e.stopPropagation();
      e.preventDefault();
      */

      if (e.keyCode === 13) this_module.user_login();
    });
    
    
    
    $('#login_user_card').off('keypress');
    $("#login_user_card").on('keypress', function (e) {

      /*
      e.stopPropagation();
      e.preventDefault();
      */
      
      

      if (e.keyCode === 13) {
        
        var card_num = $('#login_user_card').val();
      
        if ( card_num.length !== 10 ) {
          popup_warn(`Krivi unos KARTICE !!!`);
          return;
        };  
        
        // $("#user_logout_button").trigger(`click`);
        
        window.cit_logout_finished = true;
        if ( window.cit_user ) this_module.user_logout(card_num);
        
        wait_for( 
          `window.cit_logout_finished == true`,
          function() { 
            this_module.user_login(card_num); 
          }, 
          5*1000
        );
        
      };
      
    });
    
    
    

    $('#user_logout_button').off('click');
    $("#user_logout_button").on('click', function(e) {

      e.stopPropagation();
      e.preventDefault();
      this_module.user_logout();
    });


    $('#have_account').off('click');
    $("#have_account").on('click', function(e) {

      e.stopPropagation();
      e.preventDefault();
      this_module.show_cit_form(`#cit_login_form`);

    });
    

    $('#no_account').off('click');
    $("#no_account").on('click', function(e) {

      e.stopPropagation();
      e.preventDefault();
      this_module.show_cit_form(`#cit_register_form`);

    });
        
    
    
    $('#forgot_pass').off('click');
    $("#forgot_pass").on('click', function(e) {

      e.stopPropagation();
      e.preventDefault();
      this_module.show_cit_form(`#cit_reset_pass_form`);

    });    
    

    $('#send_email_reset_user_or_pass').off('click');
    $("#send_email_reset_user_or_pass").on('click', function(e) {

      e.stopPropagation();
      e.preventDefault();
      this_module.run_user_pass_reset();
    });


  }, 500000);



    if (parent_element) {
      cit_place_component(parent_element, component_html, placement);
    };

    return {
      html: component_html,
      id: data.id
    };


  },

  scripts: function () {
    
    // VAŽNO !!!!
    // module_url se definira na početku svakog injetktiranja modula u html
    // to se nalazi u main_script.js unutra funkcije get_module  
    var this_module = window[module_url];

    if ( !this_module.cit_data ) this_module.cit_data = {};

    
    function set_init_data(data) {
      this_module.cit_data[data.id] = data;
    };
    this_module.set_init_data = set_init_data;
      

    window.last_cit_user_click = 0;
    function monitor_finger_click(turn_on) {
      
      var monitor_state = window.localStorage.getItem('click_monitor_state');

      function prati_sve_klikove(e) {
        
        window.last_cit_user_click = Date.now();
        
        // ako je klik na bilo koji element unutar account forme
        // onda ignoriraj klik
        if ( $(e.target).closest('#cit_user_form').length == 0 ) {
          // console.log('body klik za monitor !!!!!!!!!!!!!!!!!!!!!!');
          this_module.check_user();
        };
        
      };


      if ( turn_on && monitor_state !== 'on') {

        window.localStorage.setItem('click_monitor_state', 'on');
        $(`body`).on('click.cit_monitor', prati_sve_klikove);

      } else {

        $(`body`).off('click.cit_monitor');
        console.log('STOPIRAN monitor ---------------------------');
        window.localStorage.removeItem('click_monitor_state');
      };

    }; // end of monitor mouse move
    this_module.monitor_finger_click = monitor_finger_click;

    
    function user_login_GUI() {

      window.user_logged = true;

      setTimeout(async function () {
        
        var avatar_img = `<img src="${window.cit_user.avatar}" />`
        $(`#avatar_image_box`).html(avatar_img).addClass(`cit_user`);
        
        if ( $(`#open_right_menu`).hasClass(`cit_hide`) ) $(`#close_right_menu`).trigger(`click`);
        
        
        $(`#avatar_text_box .avatar_name`).html(window.cit_user.full_name);
        $(`#avatar_text_box .avatar_title`).html(window.cit_user.title);

        
        
        $(`#upload_avatar_box`).removeClass(`cit_hide`);
        
        
        $(`#choose_avatar_text`).removeClass(`cit_hide`);
        $(`#new_avatar_span`).css('display', 'none');
        $(`#edit_avatar_span`).css('display', 'flex');
        $(`#update_avatar_icon`).css('display', 'flex');
        
        
        
        $(`#user_settings_box`).removeClass(`cit_hide`);
        

        show_cit_form(`none`);
        
        
        // prikazujem admin link u left menu
        
        var left_menu_mod = await get_cit_module(`/preload_modules/left_menu/cit_left_menu.js`, null)
        left_menu_mod.admin_menu_links();
        
        // selectiraj card input 
        focus_on_card_input();
        
        var popup_modal = window['/preload_modules/site_popup_modal/site_popup_modal.js'];
        var show_popup_modal = popup_modal.show_popup_modal;
        
        
        // samo ako se nalazim na PRODUCTION PAGE !!!!!!!
        // PRIKAŽI VELIKO IME I PREZIME USERA KOJI SE ULAGIRAO
        if ( window.location.hash.indexOf('#production/') > -1 ) {
          
          var popup_text = `<h1>${window.cit_user.full_name}</h1>`;
          show_popup_modal(true, popup_text, null, false, null, null, null);
          setTimeout(function() { show_popup_modal(false, popup_text, null, false, null, null, null);  }, 2*1000);
        };
        
        get_statuses_count();
        
        clearInterval(status_count_interval);
        
        status_count_interval = setInterval(function() {
          
          // ako je prošlo više od 5 sekundi
          // STAVI FOKUS NA CARD INPUT
          if ( 
            window.location.hash.indexOf(`#production/`) > -1  // SAMO ako se nalazim na production stranici
            &&
            $(`#search_autocomplete_container`).css(`opacity`) == "0" // SAMO ako nije aktivna/vidljiva drop lista
            &&
            $(`#layer_under_cit_popup_modal`).hasClass(`cit_show`) == false // biti jedan modal ne smije biti prikazan !!!!
            &&
            Date.now() - window.last_cit_user_click > 50*1000  // SAMO ako je prošlo više od 10 sekundi nakon zadnjeg klika
          ) {
            
            var provuci_karticu_text = 
`TRENUTNO JE PRIJAVLJEN KORISNIK:<br>
<br>
<h1>${window.cit_user.full_name}</h1><br>
AKO ŽELIŠ PROMJENITI KORISNIKA, PROVUCI KARTICU (prvo klikni na OK) !!!`;
            
            show_popup_modal(true, provuci_karticu_text, null, 'ok', null, null, focus_on_card_input);  
            

          };

          
          get_statuses_count();
          
        }, 50*1000);
        
        
      }, 300);


    }; // end of user  loggedIn UI
    this_module.user_login_GUI = user_login_GUI;

    
    function user_logout_GUI() {

      window.user_logged = false;
      
      window.cit_admin = false;
      
      setTimeout( async function () {
         
        var robot_icon = `<i class="fal fa-user-robot"></i>`
        
        $(`#avatar_image_box`).html(robot_icon).removeClass(`cit_user`);
        
        $(`#upload_avatar_box`).removeClass(`cit_hide`);
        
        $(`#avatar_text_box .avatar_name`).html(`NISTE PRIJAVLJENI`);
        $(`#avatar_text_box .avatar_title`).html(`&nbsp;`);

        
        $(`#upload_avatar_box`).removeClass(`cit_hide`);
        
        $(`#choose_avatar_text`).removeClass(`cit_hide`);
        $(`#new_avatar_span`).css('display', 'inline');
        $(`#edit_avatar_span`).css('display', 'none');
        $(`#update_avatar_icon`).css('display', 'none');
        
        
        
        $(`#user_settings_box`).addClass(`cit_hide`);
        
        
        // obriši sav sadržaj u search prozoru
        $(`#search_main_box`).html(`&nbsp;`);
        
        // obriši sav sadržaj u main prozoru
        $(`#cont_main_box`).html(`&nbsp;`);
        
        
        
        //resetiraj hash
        history.pushState({}, null, '/');
        
        show_cit_form(`#cit_login_form`);
        
        var left_menu_mod = await get_cit_module(`/preload_modules/left_menu/cit_left_menu.js`, null)
        left_menu_mod.admin_menu_links();
        
        
      }, 400);


    }; // end of user  loggedIn UI
    this_module.user_logout_GUI = user_logout_GUI;
    

    async function user_registration() {

      // disable gumb za reg da nebude dva klika za redom 
      $("#user_register_button").addClass('cit_disable');

      var popup_modal = window['/preload_modules/site_popup_modal/site_popup_modal.js'];

      var show_popup_modal = popup_modal.show_popup_modal;

      var all_registration_fileds = [
        'register_user_name', 
        'register_user_full_name',
        'register_user_email',
        'register_user_pass', 
        'register_repeat_user_pass',
      ];
      
      var too_short = "";
      var pass_repeat_same = false;

      $('#cit_register_form .cit_input').css('border', '');

      for (var i = 0; i < all_registration_fileds.length; i++) {
        
        if ($('#' + all_registration_fileds[i]).val().length < 6) {
          too_short += all_registration_fileds[i] + ', ';
          $('#' + all_registration_fileds[i]).css('border', '2px solid #ff0058');
        };
        
      };

      if (too_short !== "") {
        popup_sva_polje_za_reg_pass_min_6()

        $("#user_register_button").removeClass('cit_disable');

        return;
      };


      if (

        $('#user_pass').val() == '' ||
        $('#repeat_user_pass').val() == '' ||
        $('#user_pass').val() !== $('#repeat_user_pass').val()

      ) {
        popup_lozinke_nisu_iste();
        $('#cit_register_form .cit_input').css('border', '');
        $('#user_pass').css('border', '1px solid #ff0058');
        $('#repeat_user_pass').css('border', '1px solid #ff0058');

        $("#user_register_button").removeClass('cit_disable');

        return;
      };

      var mail = $('#register_user_email').val();
      var is_email_ok = mail.search(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);

      if (is_email_ok === -1 || mail.indexOf('.') === -1) {

        popup_error(`Upisani email nije ispravan!`);
        
        $('#cit_register_form .cit_input').css('border', '');
        $('#register_user_email').css('border', '1px solid #ff0058');
        
        
        $("#user_register_button").removeClass('cit_disable');
        return;
      };

      var avatar_response = await upload_avatar();
      
      if ( avatar_response.success == false ) {
        if ( avatar_response.msg ) popup_error(avatar_response.msg);
        $("#user_register_button").removeClass('cit_disable');
        return;
      };    
      
      var time_now = Date.now();

      var user_form_data = {

        user_saved: null,
        user_edited: null,

        saved: time_now,
        edited: time_now,
        
        last_login: time_now,
        help_times: null,

        user_address: null,
        user_tel: null,

        name: $(`#register_user_name`).val().trim(),
        user_number: null,

        full_name: $(`#register_user_full_name`).val().trim(),

        hash: null,
        email: $(`#register_user_email`).val().trim(),
        email_confirmed: false,
        welcome_email_id: null,
        roles: {
          info : "rwd",
          sale: true,
          admin: false,
        },

        new_pass_request: null,
        new_pass_email_id: null,
        new_pass_email_confirmed: false,
        
        password: $(`#register_user_pass`).val(),
        avatar: `/img/avatars/${avatar_response.filename}`
        
      };

      $.ajax({
          type: 'POST',
          url: '/create_user',
          data: JSON.stringify(user_form_data),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
        .done(function (user) {


          $('#register_user_pass').val('');
          $('#register_repeat_user_pass').val('');
          $("#user_register_button").removeClass('cit_disable');

          if (user.success == true) {

            window.localStorage.setItem('cit_user', JSON.stringify(user));
            window.cit_user = window.localStorage.getItem('cit_user');
            window.cit_user = JSON.parse(window.cit_user);
            
            is_admin(window.cit_user);
            this_module.user_login_GUI();

            console.log("user is logged in !!!!");
            // START MONITORING MOUSE FOR MOVMENT AND CHECKING USER TOKEN !!!!
            monitor_finger_click(true);
            popup_msg(
`Bok ${user.name}!<br>
Želimo ti ugodan rad u Velprom App :)<br>
Molimo te klikni na gumb POTVRDI koji ti je došao na tvoju email adresu.<br>
U protivnom se nećeš moći ulogirati sljedeći put!
`);

          } else {
            popup_error(user.msg);
          };

        })
        .fail(function (error) {
          popup_error(`DOŠLO JE DO GREŠKE NA SERVERU!<br>MOLIMO PROBAJTE PONOVO :(`);
          console.error(error);
          
        })
        .always(function() {
          $("#user_register_button").removeClass('cit_disable');
          $('#register_user_pass').val('');
          $('#register_repeat_user_pass').val('');
        });

    };
    this_module.user_registration = user_registration;


    function user_login(arg_card_num) {

      $("#user_login_button").addClass('cit_disable');

      var user_form = {
        card_num: arg_card_num || null,
        name: $('#login_user_name').val(),
        password: $('#login_user_pass').val(),
      };


      $.ajax({
          type: 'POST',
          url: '/authenticate',
          data: JSON.stringify(user_form),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
        .done(function (user) {

          $("#user_login_button").removeClass('cit_disable');

          $('#user_pass').val('');
          $('#repeat_user_pass').val('');

          if ( user.success ) {

            
            // set user data to local storage
            window.localStorage.setItem('cit_user', JSON.stringify(user));
            // get user data back from local storage  --- to be sure all is ok
            window.cit_user = window.localStorage.getItem('cit_user');
            // make js object from local storage data
            window.cit_user = JSON.parse(window.cit_user);
            
            is_admin(window.cit_user);
            
            cit_socket.connect();
            
            var socket_user = {
              user_number: window.cit_user.user_number,
              email: window.cit_user.email,
              full_name: window.cit_user.full_name,
            };
            
            cit_socket.emit('check_in', socket_user );


            // save_user_lang_in_db();
            is_admin(window.cit_user);
            this_module.user_login_GUI();

            console.log("user is logged in !!!!");
            // START MONITORING MOUSE FOR MOVMENT AND CHECKING USER TOKEN !!!!
            monitor_finger_click(true);


          } 
          else {

            
            $('#login_user_pass').val('');
            
            if (user.msg) {
              popup_error(user.msg);
            } else {
              popup_error('AUTENTIKACIJA NIJE USPJELA.\n\n KRIVO IME KORISNIKA ILI KRIVA LOZINKA !');
            };
            
            
            // fokusiraj na OK ali sa zakašnjenjem da ne trigeriram popup
            setTimeout(function() {  
              console.log(` //////////////////////////////////////////////// FOCUS NA OK BTN `);
              $(`#cit_popup_modal_OK`).trigger(`focus`); 
            }, 500);
            

          };
        })
        .fail(function (error) {
        
          $("#user_login_button").removeClass('cit_disable');
          popup_error("Greška na serveru prilikom prijave korisnika.<br>ERROR: 003");
        
          // fokusiraj na body ali sa zakašnjenjem da ne trigeriram popup
          setTimeout(function() {  
            console.log(` //////////////////////////////////////////////// FOCUS NA OK BTN `);
            $(`#cit_popup_modal_OK`).trigger(`focus`); 
          }, 500);

        
        });

    }; // end of user login
    this_module.user_login = user_login;

    
    function user_logout(arg_card_num) {
      
      window.cit_logout_finished = false;

      window.user_logged = false;
      monitor_finger_click(false);
      
      // deep copy of user
      var user_copy = JSON.stringify(window.cit_user);
      user_copy = JSON.parse(user_copy);
      
      window.cit_user = null;
      window.localStorage.removeItem('cit_user');

      $('#cit_user_form input[type="password"]').val('');
      
      if ( !arg_card_num ) this_module.show_cit_form('#cit_login_form');
      if ( !arg_card_num ) this_module.user_logout_GUI();
      
      $.ajax({
          type: 'POST',
          url: '/logout_user',
          data: JSON.stringify({ token: user_copy.token }),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
        .done(function (logout_response) {
          console.log(`USER LOGOUT STATUS:`);
          console.log(logout_response);
        
          if ( cit_socket ) cit_socket.disconnect();
        
        })
        .fail(function (error) {
          console.error("Greška kod logout ajax calla !!!!");
        })
        .always( function() {
          user_copy = null;
          window.cit_logout_finished = true;
        
        
        });

    };
    this_module.user_logout = user_logout;
        

    function check_token() {


      return new Promise( function( resolve, reject ) { 

        window.cit_user = window.localStorage.getItem('cit_user');

        if (!window.cit_user) {
          console.error('nema usera unutar check token !!!!!')
          resolve(null);
          return;
        };

        window.cit_user = JSON.parse(window.cit_user);

        // get token from GLOBAL user object
        var token_object = {
          token: window.cit_user.token
        };

        // get expiration from GLOBAL user object
        var token_expiration = Number(window.cit_user.token_expiration);
        // get diff from current time
        var remaining_time = token_expiration - Date.now();

        // if expiration time is in less then 5 min !!!!
        // and greater then 0
        if (remaining_time > 0 && (remaining_time < 5 * 60 * 1000)) {

          // then make special request for prologing token duration
          // actually it is creation of brand new token !!!!!!  
          $.ajax({
              type: 'POST',
              url: '/prolong',
              data: JSON.stringify(token_object),
              contentType: "application/json; charset=utf-8",
              dataType: "json"
            })
            .done(function (new_token_object) {
              // usually when token check fails then instead of token 
              // I get property success: false
              // so I handle that like error !!!!
              if (new_token_object.success == false) {

                popup_warn(`A joooj.<br>Jako mi je žao, ali izgubio sam tvoj broj :).`);
                window.cit_user = null;
                window.localStorage.removeItem('cit_user');
                
                is_admin(window.cit_user);
                
                resolve('expired');

              } else {
                // write new token data to GLOBAL cit_user object
                window.cit_user.token = new_token_object.token;
                window.cit_user.token_expiration = Number(new_token_object.token_expiration);

                // write in local storage user object with new token data
                window.localStorage.setItem('cit_user', JSON.stringify(window.cit_user));

                is_admin(window.cit_user);
                
                resolve('prolonged');

              }
            })
            .fail(function (error) {
              alert("Greška kod produženja korisničkog tokena !!!!");
              resolve(null);
            })

        } 
        else if (remaining_time <= 0) {

          popup_warn(`Vaša sesija je istekla.<br>Ulogirajte se ponovo.`);
          this_module.show_cit_form('#cit_login_form');
          window.cit_user = null;
          window.localStorage.removeItem('cit_user');
          resolve(null);

        } else {
          // ako je sve ok  tj ako token ima više od 5 minuta da zastari
          // onda ne treba raditi ništa
          resolve('ok');

        };

      }); // kraj promisa

    };
    this_module.check_token = check_token;

    
    
    function im_online(is_info) {

      var token_object = {
          token: window.cit_user.token
      };

      $.ajax({
          type: 'POST',
          url: ('/im_online/' + is_info),
          data: JSON.stringify(token_object),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
        .done(function (response) {

          if (response.success == false) {
            console.log("Greška kod im online signala !!!!!");
          } else {
            
            if ( window.cit_user ) {
              
              is_admin(window.cit_user);
              
              var socket_user = {
                user_number: window.cit_user.user_number,
                email: window.cit_user.email,
                full_name: window.cit_user.full_name,
              };

              cit_socket.emit('check_in', socket_user );
              
            };
            
            console.log("Poslan im online signal");
          };
        })
        .fail(function (error) {
          console.log("Greška kod im online signala !!!!!");
        });
      
    };
    this_module.im_online = im_online;
    
    
    var last_im_online = 0;    
    
    
    function check_user() {

      
      // ako nema usera
      if (!window.cit_user) {
        // pogledaj u local storage
        window.cit_user = window.localStorage.getItem('cit_user');
        if (window.cit_user) window.cit_user = JSON.parse(window.cit_user);
      };

      // ako nema usera niti u local storage
      if (!window.cit_user) {
        this_module.show_cit_form(`#cit_login_form`);
        window.user_logged = false;
        return;
      };

      if (!window.cit_user) return;
      
      // ako je prošlo više od 1 min onda prijavi da si online
      if ( Date.now() - last_im_online > 60*1000) {
        last_im_online = Date.now()
        var is_info  = (window.location.hash.indexOf(`#info/`) > -1) ? true : false;
        im_online(is_info);
      };
      
      
      
      // try {
      this_module.check_token().then(function (token_state) {

        if (window.cit_user && token_state == null) {
          this_module.show_cit_form('#cit_login_form');
          window.user_logged = false;
        };

        if (window.cit_user && token_state == 'expired') {
          this_module.show_cit_form('#cit_login_form');
          window.cit_user = null;
          window.localStorage.removeItem('cit_user');
          window.user_logged = false;
          
          is_admin(window.cit_user);
        };

        if ( 
          window.cit_user 
          &&
          window.user_logged !== true 
          &&
          (token_state == 'prolonged' || token_state == 'ok')
        ) {
          wait_for(`$('#cit_user_form').length > 0`, function () {
            
            is_admin(window.cit_user);
            
            this_module.user_login_GUI();
            is_admin(window.cit_user);
          }, 500000);
        };

      });

      /*  
      }
      catch(err) {
        console.log(err);
      }
      */

    }; // end of check user function
    this_module.check_user = check_user;

    
    
    function edit_user() {

    };
    this_module.edit_user = edit_user;


    
    
    function run_user_pass_reset() {

      var mail = $('#reset_pass_user_email').val();
      var is_email_ok = mail.search(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);

      if ( is_email_ok === -1 || mail.indexOf('.') === -1) {
        popup_warn(`Email nije ispravan`);
        $('#cit_reset_pass_form .cit_input').css('border', '');
        $('#reset_pass_user_email').css('border', '1px solid #ff0058');
        return;
      };


      var pass = $('#reset_pass_user_pass').val();

      if (

        $('#reset_pass_user_pass').val() == '' ||
        $('#reset_pass_repeat_user_pass').val() == '' ||
        $('#reset_pass_user_pass').val() !== $('#reset_pass_repeat_user_pass').val()

      ) {
        popup_warn(`Lozinke nisu iste!`);
        $('#cit_reset_pass_form .cit_input').css('border', '');
        $('#reset_pass_user_pass').css('border', '1px solid #ff0058');
        $('#reset_pass_repeat_user_pass').css('border', '1px solid #ff0058');
        return;
      };

      $('#send_email_reset_user_or_pass').addClass('cit_disable');

      var data_for_reset_pass = {
        user_email: mail,
        new_pass: pass
      };


      $('#cit_reset_pass_form .cit_input').css('border', '');

      $.ajax({
          type: 'POST',
          url: '/user_pass_reset',
          data: JSON.stringify(data_for_reset_pass),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
        .done(function (response) {

          if ( response.msg ) popup_msg(response.msg);

          if (response.success == true) {
            $('#user_pass').val('');
            $('#repeat_user_pass').val('');
          };

        })
        .fail(function (error) {
  
          if ( response.msg ) popup_msg(response.msg);
          console.log(error);

        })
        .always(function() {
          $('#send_email_reset_user_or_pass').removeClass('cit_disable');
        });

    };
    this_module.run_user_pass_reset = run_user_pass_reset;


    
    function show_cit_form(form_selector_id) {

      if ( form_selector_id == `none` ) {
        $(`.user_form`).css({ 'overflow': 'hidden' });
        setTimeout(function() {  $(`.user_form`).css({ 'height': `0px` });  }, 100);
        $(`#no_account`).addClass('cit_hide');
        $(`#have_account`).addClass('cit_hide');
        $(`#forgot_pass`).addClass('cit_hide');
        return;
      };
      
      
      var this_form = $(form_selector_id);
      var scroll_height = this_form[0].scrollHeight;
      
      $(`.user_form`).css({ 'overflow': 'hidden' });
      
      setTimeout(function() {  $(`.user_form`).css({ 'height': `0px` });  }, 100);
      
      setTimeout(function() { this_form.css({ 'height': scroll_height + "px" });  }, 150); 
      
      setTimeout(function() {  this_form.css({ 'overflow': 'unset' });   }, 500); 
      
      
      if ( form_selector_id == `#cit_reset_pass_form` ) {
        $(`#forgot_pass`).addClass('cit_hide');
        $(`#no_account`).removeClass('cit_hide');
        $(`#have_account`).removeClass('cit_hide');
        
        $(`#upload_avatar_box`).addClass('cit_hide');
        
        $(`#choose_avatar_text`).addClass('cit_hide');
        $(`#new_avatar_span`).css('display', 'inline');
        $(`#edit_avatar_span`).css('display', 'none');
        $(`#update_avatar_icon`).css('display', 'none');
        
        
        
        $(`#user_settings_box`).addClass('cit_hide');        
        
      };
      
      if ( form_selector_id == `#cit_login_form` ) {
        $(`#forgot_pass`).removeClass('cit_hide');
        $(`#no_account`).removeClass('cit_hide');
        $(`#have_account`).addClass('cit_hide');
        
        $(`#upload_avatar_box`).addClass('cit_hide');
        
        $(`#choose_avatar_text`).addClass('cit_hide');
        $(`#new_avatar_span`).css('display', 'inline');
        $(`#edit_avatar_span`).css('display', 'none');
        $(`#update_avatar_icon`).css('display', 'none');
        
        
        
        $(`#user_settings_box`).addClass('cit_hide');
      };
      
      
      if ( form_selector_id == `#cit_register_form` ) {
        $(`#forgot_pass`).removeClass('cit_hide');
        $(`#no_account`).addClass('cit_hide');
        $(`#have_account`).removeClass('cit_hide');
        
        $(`#upload_avatar_box`).removeClass('cit_hide');
        
        $(`#choose_avatar_text`).removeClass('cit_hide');
        $(`#new_avatar_span`).css('display', 'inline');
        $(`#edit_avatar_span`).css('display', 'none');
        $(`#update_avatar_icon`).css('display', 'none');
        
        
        $(`#user_settings_box`).addClass('cit_hide');
      };
      
      
    };
    this_module.show_cit_form = show_cit_form;
    
    this_module.cit_loaded = true;

  }

};
