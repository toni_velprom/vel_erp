var module_object = {
create: async ( data, parent_element, placement ) => {
  var this_module = window[module_url]; 
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };
  this_module.set_init_data(data);
  
  var { id } = data;
  
  let login_mod = await get_cit_module(`/preload_modules/user_login/cit_user_login_module.js`);
  
  let user_data = {id: `cit_user_form`};
  
  var component_html =
`
<div id="${id}">
${ login_mod.create(user_data).html }
</div>

`;

  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };
  
  wait_for( `$('#${data.id}').length > 0`, function() {
    
    console.log(data.id + 'component injected into html');
    
    // if ( STORE.find(data.id, `all`) == null ) STORE.make(data.id, data);

    /*
    var on_change = function() { 
      console.log(`promijeno sam data unutar ${data.id} `);
      // this_module.create( new_data, $(`#cit_root`) );
      this_module.data_to_html(STORE.find(data.id, 'all' ));
    };
    */
    
    // var component_listener_id = store_listener( data.id, 'all', on_change, data.id);
    /*    
    $(`#${data.id} input`).off('keyup');
    $(`#${data.id} input`).on('keyup', function() {
      var prop = $(this).attr('data-prop');
      var input_value = $(this).val();
      STORE.update(data.id, prop, input_value, `promjena input value ${prop} unutar #${data.id} componente` );
    });
    */
    
  }, 500*1000 );

  return {
    html: component_html,
    id: data.id
  };

},
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};
  
  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
  };
  this_module.set_init_data = set_init_data;
  
  function data_to_html(data) {
    $.each(data, function(prop, value) {
      var elem_to_update = $(`#${data.id}_${prop}_label`);
      if ( elem_to_update.length > 0 && elem_to_update.html() !== value ) {
        elem_to_update.html(value);
      };
    });
  };
  this_module.data_to_html = data_to_html;

  this_module.cit_loaded = true;
 
} // end of module scripts
};
