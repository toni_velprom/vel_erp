var module_object = {
create: ( data, parent_element, placement ) => {
  var this_module = window[module_url]; 
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };
  this_module.set_init_data(data);
  
  var { id, text, name, last_name, street, place, post_num } = data;
    
  
  var component_html =
`
<div id="${id}">
  
  
  <!--
  <div class="menu_cat">
    <a href="#sklad/null" class="menu_cat_title">
      <i class="far fa-warehouse-alt" style="transform: translate(-5%, -52%) rotate(0deg) scale(0.85);" ></i>SKLADIŠTE
    </a>
  </div>   
  -->
  
   
  <div class="menu_cat">
    <a href="#kalendar/null" class="menu_cat_title">
      <i class="fal fa-turtle"></i>Kalendar<i class="fal fa-angle-down"></i>
    </a>
  </div>   
   
    
  <div class="menu_cat">
    <a href="#partner/null/status/null" class="menu_cat_title">
      <i class="fal fa-monkey"></i>Partneri<i class="fal fa-angle-down"></i>
    </a>
  </div>
  
  <div class="menu_cat">
    <a href="#sirov/null/status/null" class="menu_cat_title">
      <i class="fal fa-hat-wizard"></i>Resursi<i class="fal fa-angle-down"></i>
    </a>
  </div>  
  
  
  <div class="menu_cat">
    <a href="#project/null/item/null/variant/null/status/null" class="menu_cat_title">
      <i class="fal fa-radiation-alt"></i>Projekti<i class="fal fa-angle-down"></i>
    </a>
  </div>  

  
  <div class="menu_cat">
    <a href="#production/null/proces/null" class="menu_cat_title">
      <i class="fal fa-bomb"></i>Proizvodnja<i class="fal fa-angle-down"></i>
    </a>
  </div>
  

  <!--
  <div class="menu_cat">
    <a href="#graf/null" class="menu_cat_title">
      <i class="fal fa-user-astronaut"></i>Grafička<i class="fal fa-angle-down"></i>
    </a>
  </div>   
  
  <div class="menu_cat">
    <a href="#cad/null" class="menu_cat_title">
      <i class="fal fa-skull"></i>CAD<i class="fal fa-angle-down"></i>
    </a>
  </div>   
  
  <div class="menu_cat">
    <a href="#prod/null" class="menu_cat_title">
      <i class="fal fa-axe-battle"></i>Proizvodnja<i class="fal fa-angle-down"></i>
    </a>
  </div> 
  --> 


  <div class="menu_cat">
    <a href="#kviz/null" class="menu_cat_title">
      <i class="fal fa-axe"></i>Kviz<i class="fal fa-angle-down"></i>
    </a>
  </div>

  
  <div class="menu_cat accord_parent">
   
   
    <!-- href="#info/null/anchor/null" --> 
    <a class="menu_cat_title cit_accord cit_active" id="btn_for_info">
     <i class="fal fa-toilet-paper"></i>Info<i class="fal fa-angle-down"></i>
    </a>
    
    <div class="cat_items cit_panel">
     
      <a  href="#info/null/anchor/null" 
          class="left_menu_btn" id="left_menu_btn_osnove"><i class="fal fa-baby-carriage"></i>Osnove</a>

      <a  href="#info/null/anchor/procedure_pravila" 
          class="left_menu_btn" id="left_menu_btn_teh_procesi"><i class="fal fa-user-hard-hat"></i>Procedure i pravila</a>
          
      <a  href="#info/null/anchor/info_kupci" 
          class="left_menu_btn" id="left_menu_btn_info_kupci"><i class="fal fa-monkey"></i>Kupci</a>

      <a  href="#info/null/anchor/vrste_materijala_info" 
          class="left_menu_btn" id="left_menu_btn_teh_procesi"><i class="fal fa-layer-group"></i>Vrste Materijala</a>
                                
      <a  href="#info/null/anchor/dimenzije_info" 
          class="left_menu_btn" id="left_menu_btn_teh_procesi"><i class="fal fa-ruler-triangle"></i>Dimenzije i Tok</a>
          
      <a  href="#info/null/anchor/vrste_tiska_info" 
          class="left_menu_btn" id="left_menu_btn_teh_procesi"><i class="fal fa-print"></i>Vrste tiska</a>
          
      <a  href="#info/null/anchor/velprom_tisak_info" 
          class="left_menu_btn" id="left_menu_btn_teh_procesi"><i class="fal fa-print"></i>Tisak strojevi</a>
              
          
      <a  href="#info/null/anchor/info_teh_obrada" 
          class="left_menu_btn" id="left_menu_btn_teh_procesi"><i class="fal fa-conveyor-belt"></i>Obrada strojevi</a>

      <a  href="#info/null/anchor/info_teh_dorada" 
          class="left_menu_btn" id="left_menu_btn_teh_procesi"><i class="fal fa-puzzle-piece"></i>Dorada strojevi</a>

      <a  href="#info/null/anchor/info_dorada_manual" 
          class="left_menu_btn" id="left_menu_btn_teh_procesi"><i class="fal fa-hand-receiving"></i>Dorada ručno</a>
                
      
      <a  href="#info/null/anchor/vanjske_usluge_info" 
          class="left_menu_btn" id="left_menu_btn_teh_procesi"><i class="fal fa-expand-arrows"></i>Vanjske usluge</a>

      
      <a  href="#info/null/anchor/fefco_anchor" 
          class="left_menu_btn" id="left_menu_btn_fefco"><i class="fal fa-box"></i>FEFCO</a>
          
          
      <a  href="#info/null/anchor/pos_pop_products_info" 
          class="left_menu_btn" id="left_menu_btn_fefco"><i class="fal fa-store"></i>POS/POP Proizvodi</a>

      <a  href="#info/null/anchor/package_products_info" 
          class="left_menu_btn" id="left_menu_btn_fefco"><i class="fal fa-box-heart"></i>Ambalažni Proizvodi</a>
     

      <a  href="#info/null/anchor/ostali_proizvodi_info" 
          class="left_menu_btn" id="left_menu_btn_fefco"><i class="fal fa-presentation"></i>Ostali Proizvodi</a>
     
      <a  href="#info/null/anchor/anchor_zastita_na_radu" 
          class="left_menu_btn" id="left_menu_btn_zas"><i class="fal fa-hard-hat"></i>Zaštita na radu</a>
      
      <a  href="#info/null/anchor/anchor_info_piktogrami" 
          class="left_menu_btn" id="left_menu_btn_pikto"><i class="fal fa-skull-crossbones"></i>Piktogrami opasnosti</a>
          
      <a  href="#info/null/anchor/transport_info" 
          class="left_menu_btn" id="left_menu_btn_trans"><i class="fal fa-truck"></i>Logistika</a>
              
      <a  href="#info/null/anchor/anchor_info_skladiste" 
          class="left_menu_btn" id="left_menu_btn_sklad"><i class="fal fa-warehouse-alt"></i>Skladište</a>
          
      <a  href="#info/null/anchor/otpad_info" 
          class="left_menu_btn" id="left_menu_btn_otpad_info"><i class="fal fa-recycle"></i>Otpad</a>    
          
      
      <a  href="#info/null/anchor/sloning" 
          class="left_menu_btn" id="left_menu_btn_sloning"><i class="fal fa-computer-classic"></i>Sloning</a>
          
      <a  href="#info/null/anchor/sustav_ovlaz_info" 
          class="left_menu_btn" id="left_menu_btn_ovlaz"><i class="fal fa-tint"></i>Sustav ovlaživanja</a>
          
      
      
      <a  href="#info/null/anchor/anchor_info_razno" 
          class="left_menu_btn" id="left_menu_btn_osnove"><i class="fal fa-dice-d20"></i>Razne napomene</a>
    
          
    </div>
    
    
  </div>
  
  <!-- KRAJ menu_cat accord_parent -->
  
</div>


`;

  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };
  
  wait_for( `$('#${data.id}').length > 0`, function() {
    
    this_module.admin_menu_links();
    
    console.log(data.id + ' component injected into html');
    
    // if ( STORE.find(data.id, `all`) == null ) STORE.make(data.id, data);

    var on_change = function() { 
      console.log(`promijeno sam data unutar ${data.id} `);
      // this_module.create( new_data, $(`#cit_root`) );
      this_module.data_to_html(STORE.find(data.id, 'all' ));
    };
    
    //var component_listener_id = store_listener( data.id, 'all', on_change, data.id);
    
    /*    
    $(`#${data.id} input`).off('keyup');
    $(`#${data.id} input`).on('keyup', function() {
      var prop = $(this).attr('data-prop');
      var input_value = $(this).val();
      STORE.update(data.id, prop, input_value, `promjena input value ${prop} unutar #${data.id} componente` );
    }); 
    */   
    
    $(`#${data.id} .menu_cat_title`).off('click');
    $(`#${data.id} .menu_cat_title`).on('click', function() {
      // nemoj sakrivati left menu SAMO  I JEDINO kada kliknem na glavni link za INFO page
      if ( this.id !== `btn_for_info` ) $(`#close_left_menu`).trigger(`click`);
    }); 
    
    
    
    
    
  }, 500*1000 );

  return {
    html: component_html,
    id: data.id
  };

},
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};
  
  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
  };
  this_module.set_init_data = set_init_data;
  
  function data_to_html(data) {
    $.each(data, function(prop, value) {
      var elem_to_update = $(`#${data.id}_${prop}_label`);
      if ( elem_to_update.length > 0 && elem_to_update.html() !== value ) {
        elem_to_update.html(value);
      };
    });
  };
  this_module.data_to_html = data_to_html;
  
  
  function admin_menu_links() {
    
      var admin_links = "";
    
    wait_for(`$("#cit_left_menu").length > 0`, function() {
  
      if ( window.cit_admin && $("#left_menu_btn_admin").length == 0 ) {
        
          admin_links = 
          `
          <div id="left_menu_btn_admin" class="menu_cat">
            <a  href="#admin/null" class="menu_cat_title">
              <i class="fas fa-crown"></i>Admin<i class="fal fa-angle-down"></i>
            </a>
          </div>
          
          <div id="left_menu_btn_users" class="menu_cat">
            <a  href="#users/null" class="menu_cat_title">
              <i class="far fa-user-astronaut"></i>Korisnici<i class="fal fa-angle-down"></i>
            </a>
          </div>
          
          
          `;
        $("#cit_left_menu").append(admin_links);
        
      } else if ( is_admin( window.cit_user ) == false ) {
        
        $("#left_menu_btn_admin").remove();
        $("#left_menu_btn_users").remove();
        
      };
      
    }, 5000);
    
  };
  this_module.admin_menu_links = admin_menu_links;
  

  this_module.cit_loaded = true;
 
} // end of module scripts
};
