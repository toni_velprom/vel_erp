var module_object = {
create: ( data, parent_element, placement ) => {
  var this_module = window[module_url]; 
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };
  
  this_module.set_init_data(data);
  
  var { id } = data;
  
  var component_html = `<h3>LOADING LIST ....</h3>`;
  
  if ( data.list ) {
    let user_item_module = window[`/preload_modules/users/user_item_module.js`];
    component_html = ``;
    $.each(data.list, function(ind, user_data) {
      component_html += `${user_item_module.create(user_data).html}`;      
    });
  };
  
  var component_html =
`
<div id="${id}">
  ${component_html}
</div>

`;

  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };
  
  
  wait_for( `$('#${data.id}').length > 0`, function() {
    
    console.log(data.id + `#${data.id} user list component injected into html`);
    
    if ( STORE.find(data.id, `all`) == null ) {
      
      STORE.make(data.id, data);
      var user_list_change = function() { 
        console.log(`promijeno sam data unutar componente #${data.id} `);
        // this_module.create( new_data, $(`#cit_root`) );
        // this_module.data_to_html(STORE.find(data.id, 'all' ));
        this_module.create(data, $(`#${data.id}`));
      };
      var component_listener_id = store_listener( data.id, 'all', user_list_change, data.id);
    };
  
    setTimeout( function() {
      if ( data.list ) return;
      console.log('dodao sam listu usera ------------- !!!!!!!')
      var prop = 'list';
      STORE.update(data.id, prop, init_users_array, `doda sam array u prop: ${prop} unutar #${data.id} componente`);
    }, 2*1000 );
  
    // EVENT LISTENERI: 

  }, 500*1000 );

  return {
    html: component_html,
    id: data.id
  };

},
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};
  
  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
  };
  this_module.set_init_data = set_init_data;
  
  function data_to_html(data) {
    $.each(data, function(prop, value) {
      var elem_to_update = $(`#${data.id}_${prop}_label`);
      if ( elem_to_update.length > 0 && elem_to_update.html() !== value ) {
        elem_to_update.html(value);
      };
    });
  };
  this_module.data_to_html = data_to_html;

  this_module.cit_loaded = true;
 
} // end of module scripts
  
}; // end of module object
