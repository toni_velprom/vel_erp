var module_object = {
create: ( data, parent_element, placement ) => {
  var this_module = window[module_url]; 
  
  if ( !data || !data.id ) {
    console.error('COMPONENT MUST HAVE MINIMUM: data.id !!!!!!!');
    return;
  };
  this_module.set_init_data(data);
  
  var { id } = data;
  
  
  var component_html =
`
<div id="${id}">
 
  <div class="menu_controls" id="left_menu_control_box">
   
    <div id="velprom_logo_box">
      <img src="img/velprom_logo.svg" />
    </div>
    
    <div id="open_left_menu" class="menu_btn"><i class="fal fa-bars"></i></div>
    <div id="close_left_menu" class="menu_btn cit_hide"><i class="fal fa-times"></i></div>
  </div>


    <div class="menu_controls" id="center_menu_control_box">
     
      <div id="badge_box">
      
      
        <div class="cit_badge" id="cit_page_title" >
          <h3>&nbsp;</h3>  
        </div>
        
       
        <!--
        <a class="cit_badge cit_new" id="cit_new" href="#statuses/new" >
          <div class="badge_title">NOVI</div>
          <div class="badge_value">12</div>
        </a>
        -->
         
        <a class="cit_badge cit_my_tasks" id="cit_my_tasks" href="#statuses_page/cit_my_tasks" >
          <div class="badge_title">MENE ČEKAJU</div>
          <div class="badge_value">&nbsp;</div>
        </a>

        <a class="cit_badge cit_task_from_me" id="cit_task_from_me" href="#statuses_page/cit_task_from_me" >
          <div class="badge_title">JA ČEKAM</div>
          <div class="badge_value" >&nbsp;</div>
        </a>

        <a class="cit_badge cit_working" id="cit_working" href="#statuses_page/cit_working" >
          <div class="badge_title">RADIM</div>
          <div class="badge_value" >&nbsp;</div>
        </a>

        <a class="cit_badge" style=" background: #ffffff; color: initial;" id="avg_time"  href="#statuses/user_stats" >
          <div class="badge_title">PROSJEK VREMENA</div>
          <div class="badge_value" style="color: #00abff;" >3:45</div>
        </a>

        <a class="cit_badge" style=" background: #ffffff; color: initial;" id="avg_tezina" href="#statuses/user_stats" >
          <div class="badge_title">PROSJEK TEŽINE</div>
          <div class="badge_value" style="color: #00abff;" >3,88</div>
        </a>
        
      </div>
      <div id="badge_box_arrow">
        STATUS REPORT
        <i class="fal fa-angle-down" style="margin: 10px; font-size: 20px;"></i>
      </div>

    </div>



  <div class="menu_controls" id="right_menu_control_box">
    
    <div id="avatar_box">
      <div id="avatar_image_box">
        <i class="fal fa-user-robot"></i>
      </div>
      <div id="avatar_text_box">
        <div class="avatar_name">NISTE PRIJAVLJENI</div>
        <div class="avatar_title">&nbsp;</div>
      </div>
    </div>

    <div id="open_right_menu" class="menu_btn"><i class="fal fa-bars"></i></div>
    <div id="close_right_menu" class="menu_btn cit_hide"><i class="fal fa-times"></i></div>
  </div>

</div>

`;

  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  };
  
  wait_for( `$('#${data.id}').length > 0`, function() {
    
    console.log(data.id + ' component injected into html');
    
    
    $(`#cit_my_tasks`).off('click');
    $(`#cit_my_tasks`).on('click', function() {
      
      // ako se već nalazim na ovom page-u onda FORSIRAJ ROUTING 
      // NA TAJ NAČIN REFRESHAM LISTU !!!!
      if ( window.location.hash.indexOf('statuses_page/cit_my_tasks') > -1 ) {
        $(`#cont_main_box`).html(``);
        router_listener();
      };
      
    });
    
    $(`#cit_task_from_me`).off('click');
    $(`#cit_task_from_me`).on('click', function() {
      
      // ako se već nalazim na ovom page-u onda FORSIRAJ ROUTING 
      // NA TAJ NAČIN REFRESHAM LISTU !!!!
      if ( window.location.hash.indexOf('statuses_page/cit_task_from_me') > -1 ) {
        $(`#cont_main_box`).html(``);
        router_listener();
      };
      
    });
    
    
    $(`#cit_working`).off('click');
    $(`#cit_working`).on('click', function() {
      
      // ako se već nalazim na ovom page-u onda FORSIRAJ ROUTING 
      // NA TAJ NAČIN REFRESHAM LISTU !!!!
      if ( window.location.hash.indexOf('statuses_page/cit_working') > -1 ) {
        $(`#cont_main_box`).html(``);
        router_listener();
      };
      
    });    
    
    
    
    
    $(`#close_left_menu`).off('click');
    $(`#close_left_menu`).on('click', function() {
      
      window.localStorage.setItem('left_menu_state', "closed");
      
      $(`#cit_left_menu_box`).addClass('cit_hide');
      $(`#cit_main_area_box`).addClass('cit_wide');
      $(`#kalendar_header`).addClass('cit_wide');
      $(this).addClass('cit_hide');
      $(`#open_left_menu`).removeClass('cit_hide');
      
    });
    
    $(`#open_left_menu`).off('click');
    $(`#open_left_menu`).on('click', function() {
      
      window.localStorage.setItem('left_menu_state', "opened");
      
      $(`#cit_left_menu_box`).removeClass('cit_hide');
      $(`#cit_main_area_box`).removeClass('cit_wide');
      $(`#kalendar_header`).removeClass('cit_wide');
      
      $(this).addClass('cit_hide');
      $(`#close_left_menu`).removeClass('cit_hide');
      
    });
    
    
    $(`#close_right_menu`).off('click');
    $(`#close_right_menu`).on('click', function() {
      
      
      $(`#cit_right_menu_box`).addClass('cit_hide');
      $(this).addClass('cit_hide');
      $(`#open_right_menu`).removeClass('cit_hide');
      
      
    });
    
    $(`#open_right_menu`).off('click');
    $(`#open_right_menu`).on('click', function() {
      $(`#cit_right_menu_box`).removeClass('cit_hide');
      
      $(this).addClass('cit_hide');
      $(`#close_right_menu`).removeClass('cit_hide');
    });    
    
    
    $(`#avatar_box`).off('click');
    $(`#avatar_box`).on('click', function() {
      
      var is_right_menu_open = $(`#cit_right_menu_box`).hasClass('cit_hide') ? false : true;
      
      if (is_right_menu_open) {
        $(`#close_right_menu`).trigger('click');
      } else {
        $(`#open_right_menu`).trigger('click');
      };

    });  
    
    
    window.cit_status_report_menu_open = false;
    $(`#badge_box_arrow`).off('click');
    $(`#badge_box_arrow`).on('click', function() {
      

      if ( window.cit_status_report_menu_open ) {
        
        $(this).find(`i`).css({
          "transform": "rotate(0deg)"
        });
        
         $(this).find(`i`).css({
          "transform": "rotate(0deg)"
        });
        
        $(`#badge_box`).removeClass(`cit_open`);
        
        window.cit_status_report_menu_open = false
        
      } else {
                
        $(this).find(`i`).css({
          "transform": "rotate(180deg)"
        });
        
        $(`#badge_box`).addClass(`cit_open`);
        window.cit_status_report_menu_open = true
        
      };

    });  
        
    
    
    
  }, 500*1000 );

  return {
    html: component_html,
    id: data.id
  };

},
scripts: function () {
  
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u global_funcs.js unutar funkcije get_module  
  var this_module = window[module_url]; 
  if ( !this_module.cit_data ) this_module.cit_data = {};
  
  function set_init_data(data) {
    this_module.cit_data[data.id] = data;
  };
  this_module.set_init_data = set_init_data;
  
  function data_to_html(data) {
    $.each(data, function(prop, value) {
      var elem_to_update = $(`#${data.id}_${prop}_label`);
      if ( elem_to_update.length > 0 && elem_to_update.html() !== value ) {
        elem_to_update.html(value);
      };
    });
  };
  this_module.data_to_html = data_to_html;

  this_module.cit_loaded = true;
 
} // end of module scripts
};
