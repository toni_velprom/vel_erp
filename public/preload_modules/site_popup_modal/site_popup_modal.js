var module_object = {
  
  create: function( data, parent_element, placement ) {
    
    var component_id = null;
    
    var w = window;
    
    
    var component_html =
    `
<div id="layer_under_cit_popup_modal">
  <div id="cit_popup_modal">
    <div id="cit_popup_modal_message">
    <!--HERE I PUT POPUP HTML MESSAGES -->
    </div>
    
    <div id="cit_popup_modal_progress_bar">
      <div id="cit_popup_modal_complete_bar">0%</div>
    </div>  
    
    <div id="cit_popup_modal_button_row">
      <button class="cit_popup_modal_button" id="cit_popup_modal_YES">DA</button>
      <button class="cit_popup_modal_button" id="cit_popup_modal_OK">OK</button>
      <button class="cit_popup_modal_button" id="cit_popup_modal_NO">NE</button>
    </div>
    
  </div>
</div>
    `;
   
    
    wait_for( `$('#cit_popup_modal').length > 0`, function() {
      if ( $('.cit_tooltip').length > 0 ) $('.cit_tooltip').tooltip();
    }, 50000000 );
    
    if ( parent_element ) {
      cit_place_component(parent_element, component_html, placement);
    } 
    else {
      return {
        html: component_html,
        id: component_id
      } 
    };
      
  },
 
scripts: function () {
  
  
// VAŽNO !!!!  
// module_url se definira na početku svakog injetktiranja modula u html
// to se nalazi u main_script.js unutra funkcije get_module
var this_module = window[module_url]; 
  

function run_popup(show_popup, popup_text, popup_progress, show_buttons, yes_func, no_func, ok_func) {

  $('#cit_popup_modal_message').removeAttr('style');


  $('#cit_popup_modal_message').html(popup_text || '');

  var progres_text = popup_progress ? (popup_progress + '%') : '';

  $('#cit_popup_modal_complete_bar').text(progres_text);


  /* -------------START--------------------- IF SHOW POPUP ---------------------------------- */

  // && $('#layer_under_cit_popup_modal').hasClass('cit_show') == false   

  if ( show_popup ) {
    
    
    

    // always erase/reset all previous events if any on YES, NO and OK buttons !!!!
    $('#cit_popup_modal_YES').off('click');
    $('#cit_popup_modal_OK').off('click');
    $('#cit_popup_modal_NO').off('click');
    
    
    $('#cit_popup_modal_YES').removeClass('cit_focus');
    $('#cit_popup_modal_OK').removeClass('cit_focus');
    $('#cit_popup_modal_NO').removeClass('cit_focus');
    

    // pokaži layer ispod modala
    $('#layer_under_cit_popup_modal').addClass('cit_show');  

    // pokaži modal
    // $('#cit_popup_modal').addClass('cit_show');

    // remove all classes
    $('#cit_popup_modal_message').removeClass('cit_error');
    $('#cit_popup_modal_message').removeClass('cit_warning');
    $('#cit_popup_modal_message').removeClass('cit_success');
    $('#cit_popup_modal_message').removeClass('cit_pay_success');
    $('#cit_popup_modal_message').removeClass('cit_pay_warning');
    $('#cit_popup_modal_message').removeClass('cit_pay_error');

    if ( show_popup == 'error' )  $('#cit_popup_modal_message').addClass('cit_error');
    if ( show_popup == 'warning' )  $('#cit_popup_modal_message').addClass('cit_warning');
    if ( show_popup == 'pay_success' )  $('#cit_popup_modal_message').addClass('cit_pay_success');
    if ( show_popup == 'pay_warning' )  $('#cit_popup_modal_message').addClass('cit_pay_warning');
    if ( show_popup == 'pay_error' )  $('#cit_popup_modal_message').addClass('cit_pay_error');

    if ( popup_progress ) {
      $('#cit_popup_modal_progress_bar').css({'display': 'block'});
      $('#cit_popup_modal_complete_bar').css('width', popup_progress + '%');
    } else {
      $('#cit_popup_modal_progress_bar').css({'display': 'none'});
    };

    if ( !show_buttons ) {
      $('#cit_popup_modal_button_row').css('display', 'none');
    };


    // resetiraj boju gumba OK
    $('#cit_popup_modal_OK').css({
      'background-color': '',
      'box-shadow': '',
      'color': ''
    });  

    if ( show_popup == 'warning' ) {
      $('#cit_popup_modal_OK').css({
        'background-color': '#ffd300',
        'box-shadow': '0px 3px #af9102',
        'color': '#503601'
      });
    };

    if ( show_popup == 'error' ) {
      $('#cit_popup_modal_OK').css({
        'background-color': '#ff0a00',
        'box-shadow': '0px 3px #bf0b03',
        'color': '#fff'
      });
    };

    if (show_buttons == 'yes/no') {

      $('#cit_popup_modal_button_row').css('display', 'flex');
      $('#cit_popup_modal_button_row').css('justify-content', 'space-between');

      $('#cit_popup_modal_YES').css('display', 'block');
      $('#cit_popup_modal_OK').css('display', 'none');
      $('#cit_popup_modal_NO').css('display', 'block'); 

      
      $(`#cit_popup_modal_YES`).removeClass('cit_focus');
      $('#cit_popup_modal_OK').removeClass('cit_focus');
      
      // fokusiraj na NO
      $('#cit_popup_modal_NO').addClass('cit_focus');
      
      

      $("body").off(`keyup.popup_arrows`);
      $("body").on(`keyup.popup_arrows`, function(e) {
        
        console.log("body keyup event ---------------------", e.which );

        if (e.which == 37 ) { // left
          
          $(`#cit_popup_modal_YES`).addClass('cit_focus');
          $(`#cit_popup_modal_NO`).removeClass('cit_focus');
          
          console.log(`left`);
        }
        else if(e.which == 39) { // right
          
          $(`#cit_popup_modal_YES`).removeClass('cit_focus');
          $(`#cit_popup_modal_NO`).addClass('cit_focus');
          
        } else if ( e.target.id == `` && e.which == 13 ) { // enter

          if ( $(`#cit_popup_modal_YES`).hasClass(`cit_focus`) ) $(`#cit_popup_modal_YES`).trigger(`click`);
          if ( $(`#cit_popup_modal_NO`).hasClass(`cit_focus`) ) $(`#cit_popup_modal_NO`).trigger(`click`);
          
          
        };

      });

      
      

      setTimeout(function() {

        $('#cit_popup_modal_YES').text('DA');
        $('#cit_popup_modal_OK').text('OK');
        $('#cit_popup_modal_NO').text('NE');


        $('#cit_popup_modal_YES').off('click');
        $('#cit_popup_modal_YES').on('click', function() {
          if ( yes_func ) {
            setTimeout( function() { yes_func(); }, 100);
          };
          show_popup_modal(false, popup_text, null, 'ok', null, null); 
        });

        $('#cit_popup_modal_NO').off('click');
        $('#cit_popup_modal_NO').on('click', function() {
         if ( no_func ) {
            setTimeout( function() { no_func(); }, 100);
          };
          show_popup_modal(false, popup_text, null, 'ok', null, null);  
        });
        

      }, 50);

    };

    if (show_buttons == 'ok') {
      
      
      $('#cit_popup_modal_button_row').css('display', 'flex');
      $('#cit_popup_modal_button_row').css('justify-content', 'center');

      $('#cit_popup_modal_YES').css('display', 'none');
      $('#cit_popup_modal_OK').css('display', 'block');
      $('#cit_popup_modal_NO').css('display', 'none'); 
      
      // fokusiraj na OK
      
      $(`#cit_popup_modal_YES`).removeClass('cit_focus');
      $(`#cit_popup_modal_OK`).addClass('cit_focus');
      $('#cit_popup_modal_NO').removeClass('cit_focus');
      
      
      
      $(`#cit_popup_modal_button_row`).off(`click`);
      $(`#cit_popup_modal_button_row`).on(`click` , function() {
        $(`#cit_popup_modal_OK`).trigger(`focus`);
        
      });
      
      
      $("body").off(`keyup.popup_arrows`);
      $("body").on(`keyup.popup_arrows`, function(e) {
        
        console.log("body keyup event ---------------------", e.which );
        if ( e.target.id == `cit_popup_modal_OK` && e.which == 13) { // enter
          $(`#cit_popup_modal_OK`).trigger(`click`);
        };
      });
      
      
      $('#cit_popup_modal_OK').off('click');
      $('#cit_popup_modal_OK').on('click', function() {
        if ( ok_func ) {
          setTimeout( function() { ok_func(); }, 100);
        };
        show_popup_modal(false, popup_text, null, 'ok', null, null, null);  
      });
      
    };  

  }; 

/* -------------END--------------------- IF SHOW POPUP ---------------------------------- */


}
  

function show_popup_modal(show_popup, popup_text, popup_progress, show_buttons, yes_func, no_func, ok_func) {
  
  if ( !show_popup ) {
    
    $("body").off(`keyup.popup_arrows`);
    
    // sakrije layer ispod modala
    $('#layer_under_cit_popup_modal').removeClass('cit_show');
    
    // sakrij modal
    // $('#cit_popup_modal').removeClass('cit_show');
    
    // obriši prvi popup objekt u fifo arraju
    window.cit_popup_fifo.shift();
    
    // ako postoji nešto u arraju to znači da ima još prozora koji se nisu prikazali i "čekaju"
    if ( window.cit_popup_fifo.length > 0) {
      
      setTimeout( function() {  
        var { show_popup, popup_text, popup_progress, show_buttons, yes_func, no_func, ok_func } = window.cit_popup_fifo[0];
        run_popup(show_popup, popup_text, popup_progress, show_buttons, yes_func, no_func, ok_func);
      }, 400);
      
    };

    return;
  };
  
  
  // pretvori sve argumente u objekt i spremi u fifo array
  var popup_data = { show_popup, popup_text, popup_progress, show_buttons, yes_func, no_func, ok_func };
  window.cit_popup_fifo.push(popup_data);
  
  
  // uzmi prvi popup objekt iz fifo arraja i pokreni ga
  // to ne mora biti ovaj trenutni NEGO JE TO PRVI KOJE JE OSTAO U FIFO ARRAYU
  var { show_popup, popup_text, popup_progress, show_buttons, yes_func, no_func, ok_func } = window.cit_popup_fifo[0];
  run_popup(show_popup, popup_text, popup_progress, show_buttons, yes_func, no_func, ok_func );
  
  
};
this_module.show_popup_modal = show_popup_modal;
 
this_module.cit_loaded = true;  
 
}
  
  
};