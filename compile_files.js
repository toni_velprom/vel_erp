var babel = require("@babel/core");
var { spawn } = require("child_process");
var { exec } = require("child_process");
var fs = require('fs');
var jsdom = require("jsdom");
var { JSDOM } = jsdom;







var start_html = fs.readFileSync( global.appRoot + `/public/start.html`, 'utf8');
var { window } = new JSDOM(start_html);
var $ = require("jquery")(window);

const chokidar = require('chokidar');

// Initialize watcher.



// const watcher = chokidar.watch( global.appRoot + '/public/**/*.es6', {
//   ignored: /(^|[\/\\])\../, // ignore dotfiles
//   persistent: true
// });




/*
var watcher_time = 0;
watcher
.on('add', path => {
  console.log(`File ${path} has been added`);
  var file_path = global.appRoot + '/' + path;
  
})
.on('change', path => {
  console.log(`File ${path} has been changed`);
  var file_path = global.appRoot + '/' + path;
  watcher_time = Date.now();
  global.run_babel_and_browserify(file_path, watcher_time);
})
.on('unlink', path => {
  console.log(`File ${path} has been removed`);
});

*/


/*

var compile_this_files = [
  `${global.appRoot}/public/modules/main_layout_module.js`,
  `${global.appRoot}/public/modules/users/user_list_module.js`,
];
*/


var use_babel = false;
var use_source_map = false;

console.log('compile files loaded !!!!!!');

global.compile_all_files = function() {

  var start_html = fs.readFileSync( global.appRoot + `/public/start.html`, 'utf8');
  var { window } = new JSDOM(start_html);
  var $ = require("jquery")(window);
  
  var compile_this_files = [];

  $('script').each( function() {
    if ( $(this).attr('type') == 'module') {
      var module_full_path = global.appRoot + `/public` + this.src;
      start_html = start_html.replace(this.outerHTML, '');
      compile_this_files.push( module_full_path );
    };
  });

  
  var all_precompiled_modules = ``;
  
  $.each( compile_this_files, function(index, url) { 
    
    var module = fs.readFileSync(url, 'utf8');

    var local_url = url.replace( global.appRoot + '/public', ''); // brišem cijeli script tag tj cijelu liniju
    var url_module_id = local_url.replace(/\//g, '_');
    url_module_id = url_module_id.replace(/\./g, '_');

    var module_script_elem = ``;
    
    if ( use_babel ) {
    
      var module_wrapper = create_cit_module(url, module);
      
      var babel_options = {
        presets: ['@babel/preset-env', 'minify'],        
        sourceFileName: local_url,
        sourceMaps: false,
      };
      
      if ( use_source_map ) babel_options.sourceMaps = 'inline'
            
      var babel_result = babel.transformSync(module_wrapper, babel_options);
      console.log( babel_result.code );
      console.log( babel_result.map );

      module_script_elem = `<script id="${url_module_id}" type="text/javascript">${babel_result.code}</script>`;
      
    } 
    else {
      
      module_script_elem = 
`
<script id="${url_module_id}" type="text/javascript">
${ create_cit_module(url, module) }
//# sourceURL=${local_url}
</script>
`;
    };

    
    all_precompiled_modules += module_script_elem;
  });
  
  
  var index_html = start_html.replace('</body>', all_precompiled_modules+'</body>'); // sve script tagove koje sam obrisao sada mijenjam sa modulima

  fs.writeFile( global.appRoot + `/public/index.html`, index_html, function() {
    // console.log(index_html);
    console.log('created index html !!!!!!!');
  });

};

global.compile_all_files();

// global.compile_all_files();

function create_cit_module(url, module) {
  
  var local_url = url.replace( global.appRoot + '/public', '');
  var url_module_id = local_url.replace(/\//g, '_');
  url_module_id = url_module_id.replace(/\./g, '_');

  var module_code =
`
(function () {
  window['${local_url}'] = {};
  var module_url = '${local_url}';
  ${module}
  $.each(module_object, function(key, item) {
    window['${local_url}'][key] = item;
  });
  if ( window['${local_url}'].scripts ) window['${local_url}'].scripts();
}());
`;
  
  return module_code;

};

global.run_babel = function(file_path) {

  exec(
    `npx babel ${global.appRoot}/public --out-file script-compiled.js`, 
    function(error, stdout, stderr) {

    if (error) {
        console.error(`BABEL -----> error: ${error.message}`);
        return;
    };
    if (stderr) {
        console.error(`BABEL -----> stderr: ${stderr}`);
        // return;
    };

    console.log(`BABEL SUCCESS -----> output:`);
    console.log(`${stdout}`);

  }); // kraj exec func

};

global.run_babel_and_browserify = function(file_path, watcher_time) {
  
  var babel_file_path = file_path.replace('.es6', '_babel.js');
  
exec(`npx babel ${file_path} --out-file ${babel_file_path} --presets=@babel/preset-env  --source-maps inline`, function(error, stdout, stderr) {
    
    // nije mi bitno jel error  - samo idem dalje od zadnjeg PID-a do prvog PID-a u arrayu
    if (error) {
      console.log(`error on npx babel ${file_path}  -----> error: ${error.message}`);
      return;
    };
    
    if (stderr) {
      console.log(`std err on npx babel ${file_path}  -----> stderr: ${stderr}`);
      return;
    };
  
    console.log(`npx babel success ${file_path} ----> stdout: ${stdout}`);
    
  var js_out_file_path = babel_file_path.replace('_babel.js', '.js');
  
    exec(`browserify ${babel_file_path} -o ${js_out_file_path} -d`, function(error, stdout, stderr) {

      // nije mi bitno jel error  - samo idem dalje od zadnjeg PID-a do prvog PID-a u arrayu
      if (error) {
        console.log(`error on browserify ${file_path}  -----> error: ${error.message}`);
        return;
      };

      if (stderr) {
        console.log(`std err on browserify ${file_path}  -----> stderr: ${stderr}`);
        return;
      };

      console.log(`browserify success ${file_path} ----> stdout: ${stdout}`);

      console.log(`babel and browserify time: `, (Date.now() - watcher_time) );

    }); // kraj od exec 

    
  }); // kraj od exec 
  
    
};

global.run_compile_init_scripts = function() {

  exec(
    `npx babel ${global.appRoot}/public/js/initial_scripts.js  --out-file ${global.appRoot}/public/js/initial_scripts_babel.js --presets=@babel/preset-env  --source-maps inline`, 
    function(error, stdout, stderr) {

    if (error) {
        console.error(`BABEL -----> error: ${error.message}`);
        return;
    };
    if (stderr) {
        console.error(`BABEL -----> stderr: ${stderr}`);
        // return;
    };

    console.log(`BABEL SUCCESS -----> output:`);
    console.log(`${stdout}`);
      
      
      
    exec(
      `browserify ${global.appRoot}/public/js/initial_scripts_babel.js -o ${global.appRoot}/public/js/bro_initial_scripts.js -d`, 
      function(error, stdout, stderr) {

      if (error) {
          console.error(`BROWSERIFY -----> error: ${error.message}`);
          return;
      };
      if (stderr) {
          console.error(`BROWSERIFY -----> stderr: ${stderr}`);
          return;
      };

      console.log(`BROWSERIFY SUCCESS -----> output:`);
      console.log(`${stdout}`);

    }); // kraj exec func BROWSERIFY     

  }); // kraj exec func BABEL

};

global.run_compile_init_scripts();
