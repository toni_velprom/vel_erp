'use strict';

var cors = require('cors');

var multer  = require('multer');

var sirov_controller = require('../controllers/sirov_controller');


module.exports = function(app) {
  
  
  app.use(cors());
  
  /*
  app.route('/save_partner')
    .post(partner_controller.save_partner);
    
  */
  
  app.route('/find_email')
    .post(sirov_controller.find_email);
  
  
};
