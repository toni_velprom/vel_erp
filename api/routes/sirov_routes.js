'use strict';

var cors = require('cors');

var multer  = require('multer');

var sirov_controller = require('../controllers/sirov_controller');


module.exports = function(app) {
  
  
  app.use(cors());
  
  /*
  app.route('/save_partner')
    .post(partner_controller.save_partner);
    
  */
  
  app.route('/find_sirov')
    .post(sirov_controller.find_sirov);
  
  
  app.route('/find_sirov_location')
    .post(sirov_controller.find_sirov_location);
  

  app.route('/find_sirov_records')
    .post(sirov_controller.find_sirov_records);
  
  
  app.route('/save_sirov')
    .post(sirov_controller.save_sirov);
  
  
  app.route('/upload_sirov_docs')
    .post(sirov_controller.upload_sirov_docs);

  app.route('/find_grupa')
    .post(sirov_controller.find_grupa);

  app.route('/edit_grupa_name')
    .post(sirov_controller.edit_grupa_name);

  
  app.route('/remove_from_grupa')
    .post(sirov_controller.remove_from_grupa);

  
  app.route('/update_grupa')
    .post(sirov_controller.update_grupa);

  app.route('/new_grupa')
    .post(sirov_controller.new_grupa);
  
  app.route('/upsert_sirov_record')
    .post(sirov_controller.upsert_sirov_record);
  
  
  app.route('/offer_reserv_records')
    .post(sirov_controller.offer_reserv_records);
  
  
  app.route('/get_last_sirov_records')
    .post(sirov_controller.get_last_sirov_records);
  
  
  app.route('/save_sirov_records')
    .post(sirov_controller.save_sirov_records);
  
  
  app.route('/storno_sirov_records')
    .post(sirov_controller.storno_sirov_records);
  
  
  app.route('/update_doc_sifra_in_sirov_records')
    .post(sirov_controller.update_doc_sifra_in_sirov_records);
  
  
  app.route('/get_alat_shelfs')
    .post(sirov_controller.get_alat_shelfs);
  
  
  app.route('/get_new_palet_sifre')
    .post(sirov_controller.get_new_palet_sifre);
  
  
  app.route('/update_alat_shelfs')
    .post(sirov_controller.update_alat_shelfs);
  

  
};
