'use strict';

var cors = require('cors');

var multer  = require('multer');

var partner_controller = require('../controllers/partner_controller');


module.exports = function(app) {
  
  
  app.use(cors());
  
  /*
  app.route('/save_partner')
    .post(partner_controller.save_partner);
    
  */
  
  
  app.route('/upload_docs')
    .post(partner_controller.upload_docs);

  app.route('/find_grupacija')
    .post(partner_controller.find_grupacija);

  app.route('/edit_grupacija_name')
    .post(partner_controller.edit_grupacija_name);

  app.route('/update_grupacija')
    .post(partner_controller.update_grupacija);

  app.route('/new_grupacija')
    .post(partner_controller.new_grupacija);
    
  
  app.route('/remove_from_grupacija')
    .post(partner_controller.remove_from_grupacija);
    
  
  app.route('/find_partner')
    .post(partner_controller.find_partner);
  
  
  app.route('/find_docs')
    .post(partner_controller.find_docs);
  
  
  app.route('/save_partner')
    .post(partner_controller.save_partner);
  
  
  app.route('/edit_partner_doc')
    .post(partner_controller.edit_partner_doc);
  
  

};
