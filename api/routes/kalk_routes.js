'use strict';

var cors = require('cors');

var multer  = require('multer');

var kalk_controller = require('../controllers/kalk_controller');


module.exports = function(app) {
  
  app.use(cors());
  
  app.route('/find_kalk')
    .post(kalk_controller.find_kalk);
  
  
  app.route('/save_kalk')
    .post(kalk_controller.save_kalk);


  
  app.route('/update_post_kalk')
    .post(kalk_controller.update_post_kalk);


  
  app.route('/update_kalk_product_name')
    .post(kalk_controller.update_kalk_product_name);
  
  
  
};