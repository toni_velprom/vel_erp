'use strict';

var cors = require('cors');

var multer  = require('multer');

var project_controller = require('../controllers/project_controller');


module.exports = function(app) {
  
  
  app.use(cors());
  
  /*
  app.route('/save_partner')
    .post(partner_controller.save_partner);
    
  */
  
  app.route('/find_proj')
    .post(project_controller.find_proj);
  
  app.route('/save_proj')
    .post(project_controller.save_proj);
  
  
  app.route('/get_status_kupac')
    .post(project_controller.get_status_kupac);


};
