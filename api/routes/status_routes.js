'use strict';

var cors = require('cors');

var multer  = require('multer');

var status_controller = require('../controllers/status_controller');


module.exports = function(app) {
  
  
  app.use(cors());
  
  /*
  app.route('/save_partner')
    .post(partner_controller.save_partner);
    
  */
  
  app.route('/find_status')
    .post(status_controller.find_status);
  
  app.route('/filter_statuses')
    .post(status_controller.find_status);
  
  
  app.route('/find_status_for_production')
    .post(status_controller.find_status);
  
  /*
  app.route('/find_status_for_page/:status_page')
    .post(status_controller.find_status);
  
  */
  app.route('/already_has_this_status_tip_in_branch')
    .post(status_controller.already_has_this_status_tip_in_branch);
  
  
  app.route('/save_status')
    .post(status_controller.save_status);

  
  app.route('/get_statuses_count')
    .post(status_controller.get_statuses_count);
  
  
  app.route('/proces_rn_update_statuses')
    .post(status_controller.proces_rn_update_statuses);
  
  
  app.route('/get_complete_branch')
    .post(status_controller.get_complete_branch);
  
  
  
  
  

};
