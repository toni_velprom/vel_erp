'use strict';

var cors = require('cors');

var plate_controller = require('../controllers/plate_controller');


module.exports = function(app) {
  
  
  app.use(cors());
  
  app.route('/find_plate')
    .post(plate_controller.find_plate);
  
  
  app.route('/save_plate')
    .post(plate_controller.save_plate);
  

};
