'use strict';

var cors = require('cors');

var multer  = require('multer');

var admin_controller = require('../controllers/admin_controller');


module.exports = function(app) {
  
  
  app.use(cors());
  
  /*
  app.route('/save_partner')
    .post(partner_controller.save_partner);
    
  */
  
  app.route('/get_admin_conf')
    .post(admin_controller.get_admin_conf);
  
  
  app.route('/save_admin_conf')
    .post(admin_controller.save_admin_conf);


};