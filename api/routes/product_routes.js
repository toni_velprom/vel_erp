'use strict';

var cors = require('cors');

var multer  = require('multer');

var product_controller = require('../controllers/product_controller');


module.exports = function(app) {
  
  
  app.use(cors());
  
  /*
  app.route('/save_partner')
    .post(partner_controller.save_partner);
    
  */
  
  app.route('/find_product')
    .post(product_controller.find_product);
  
  app.route('/save_product')
    .post(product_controller.save_product);

  app.route('/delete_product')
    .post(product_controller.delete_product);

  app.route('/get_product_new_variant')
    .post(product_controller.get_product_new_variant);


};
