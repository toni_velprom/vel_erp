'use strict';

var cors = require('cors');

var multer  = require('multer');

var upload_controller = require('../controllers/upload_controller');


module.exports = function(app) {
  
  
  app.use(cors());
  
  app.route('/get_new_counter')
    .post(upload_controller.get_new_counter);

  
  app.route('/get_new_sub_nacrt')
    .post(upload_controller.get_new_sub_nacrt);

  
  app.route('/save_pdf')
    .post(upload_controller.save_pdf);

  app.route('/hnb_tecaj')
    .get(upload_controller.hnb_tecaj);

  
  app.route('/send_mail_from_user')
    .post(upload_controller.send_mail_from_user);

  
  
  

};