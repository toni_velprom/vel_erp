'use strict';

var cors = require('cors');

var multer  = require('multer');


module.exports = function(app) {
  
  
  app.use(cors());
  
  var userQueries = require('../controllers/user_controller');
  var findQueries = require('../controllers/find_controller');

  app.route('/create_user')
    .post(userQueries.create_a_user);

  
  app.route('/upload_avatar')
    .post(userQueries.upload_avatar);

  

  
  app.route('/confirm_email/:welcome_email_id')
    .get(userQueries.confirm_user_email); 
    

  
  app.route('/authenticate')
    .post(userQueries.authenticate_a_user);
  
  app.route('/prolong')
    .post(userQueries.prolong_token);  
  
    
  app.route('/logout_user')
    .post(userQueries.logout_user);  
  
  
  app.route('/im_online/:is_info')
    .post(userQueries.im_online);  
  
  
  
  app.route('/user_pass_reset')
    .post(userQueries.user_pass_reset);  
  
  
  app.route('/pass_reset_confirm/:reset_email_id')
    .get(userQueries.pass_reset_confirm);  
  
  
  
  app.route('/search_kal_users')
    .post(userQueries.search_kal_users);  
  
  
  /*
  
  app.route('/get_sve_user_brojeve')
    .get(userQueries.get_sve_user_brojeve);
  
  app.route('/confirm_email/:welcome_email_id')
    .get(userQueries.confirm_user_email); 
  
  

  app.route('/search_customers')
    .post(findQueries.cit_search_remote);  
  
  app.route('/search_regs')
    .post(findQueries.cit_search_remote);  
  


    
  app.route('/pass_for_token')
    .post(userQueries.pass_for_token);  
  
  app.route('/set_user_lang')
    .post(userQueries.set_user_lang);  
  
  app.route('/r1_racun_user_podaci')
    .post(userQueries.r1_racun_user_podaci);  
  
  
  app.route('/get_user_business_data/:user_num')
    .get(userQueries.get_user_business_data);  
  
  
  app.route('/check_jel_nova_rega/:user_number/:reg')
    .get(userQueries.check_jel_nova_rega);  
  

  app.route('/update_user_from_db/:user_number')
    .get(userQueries.update_user_from_db);
    
  */
  
  
  
  app.route('/get_all_users')
    .post(userQueries.get_all_users);
  
  
  app.route('/update_user_as_admin')
    .post(userQueries.update_user_as_admin); 
  
  
  app.route('/save_user_as_admin')
    .post(userQueries.save_user_as_admin);
  
  
  app.route('/get_all_user_tests')
    .post(userQueries.get_all_user_tests);
  
  
  
};
