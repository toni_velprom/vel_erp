'use strict';

var cors = require('cors');

var multer  = require('multer');

var sklad_controller = require('../controllers/sklad_controller');


module.exports = function(app) {
  
  app.use(cors());
  
  app.route('/all_sklads')
    .post(sklad_controller.all_sklads);
  
  /*
  app.route('/save_new_sklad')
    .post(sklad_controller.save_new_sklad);
  
  app.route('/save_sklad_history')
    .post(sklad_controller.save_sklad_history);

  */
};