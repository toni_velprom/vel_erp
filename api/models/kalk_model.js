'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

// var ParkPlaceOwner = mongoose.model('ParkPlaceOwner');

var kalk_obj = { 
  
  id: { type: String, required: false }, // ovo je id za html element !!!!
  
  for_module: { type: String, required: false },
  full_product_name: { type: String, required: false },
  
  
  proj_sifra: { type: String, required: false },
  item_sifra: { type: String, required: false },
  variant: { type: String, required: false },
  
  product_id: { type: String, required: false },
  
  tip: { type: Schema.Types.Mixed, required: false },
  
  
  offer_kalk: { type: [Schema.Types.Mixed], required: false },
  meta_offer_kalk: { type: Schema.Types.Mixed, required: false },
  
  pro_kalk: { type: [Schema.Types.Mixed], required: false },
  meta_pro_kalk: { type: Schema.Types.Mixed, required: false },
  
  post_kalk: { type: [Schema.Types.Mixed], required: false },
  meta_post_kalk: { type: Schema.Types.Mixed, required: false },

  
  
}; 

var kalk_schema = new Schema( 
  kalk_obj,  
  { collection : 'kalkulacije'});  
 
module.exports = mongoose.model('Kalk', kalk_schema);
