'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

// var ParkPlaceOwner = mongoose.model('ParkPlaceOwner');

var grupa_obj = { 

  user_saved: { type: Schema.Types.Mixed, required: false },
  user_edited: { type: Schema.Types.Mixed, required: false },
  saved: { type: Number, required: false },
  edited: { type: Number, required: false },
  
  
  sifra: { type: String, required: false },
  name: { type: String, required: false },
  sirovine: { type: [Schema.Types.Mixed], required: false },
  sirovine_flat: { type: String, required: false },
  
  test: { type: Schema.Types.ObjectId , required: false },
  
  
}; 

var grupa_schema = new Schema( 
  grupa_obj,  
  { collection : 'grupe'});  
 
module.exports = mongoose.model('Grupa', grupa_schema);
