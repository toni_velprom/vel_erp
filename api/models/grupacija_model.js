'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

// var ParkPlaceOwner = mongoose.model('ParkPlaceOwner');

var grup_obj = { 

  user_saved: { type: Schema.Types.Mixed, required: false },
  user_edited: { type: Schema.Types.Mixed, required: false },
  
  saved: { type: Number, required: false },
  edited: { type: Number, required: false },
  
  name: { type: String, required: false },
  companies: { type: [Schema.Types.Mixed], required: false },
  companies_flat: { type: String, required: false },
  
}; 

var grup_schema = new Schema( 
  grup_obj,  
  { collection : 'grupacije'});  
 
module.exports = mongoose.model('Grupacija', grup_schema);
