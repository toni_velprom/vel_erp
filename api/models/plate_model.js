'use strict'; 

var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 


var plate_obj = { 
  
  user_saved: { type: Schema.Types.Mixed, required: false },
  user_edited: { type: Schema.Types.Mixed, required: false },
  saved: { type: Number, required: false },
  edited: { type: Number, required: false },
  
  
  sifra: { type: String, required: false },
  naziv: { type: String, required: false },
  sirov: { type: Schema.Types.Mixed, required: false },
  
  
  color_C: { type: Number, required: false },
  color_M: { type: Number, required: false },
  color_Y: { type: Number, required: false },
  color_K: { type: Number, required: false },
  color_W: { type: Number, required: false },
  color_V: { type: Number, required: false },
  
  noz_big: { type: Number, required: false },
  nest_count: { type: Number, required: false },
  
  products: { type: [Schema.Types.Mixed], required: false },
}; 

var plate_schema = new Schema( 
  plate_obj,  
  { collection : 'plates'});  
 
module.exports = mongoose.model('Plate', plate_schema);
