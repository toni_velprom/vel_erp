'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 


var primjer = {
	"attachments": [],
	"headers": {},
	"headerLines": 
  [
    {
		"key": "return-path",
		"line": "Return-Path: <toni.kutlic@velprom.hr>"
	}, {
		"key": "delivered-to",
		"line": "Delivered-To: app@velprom.hr"
	}, {
		"key": "received",
		"line": "Received: from lin62.mojsite.com\r\n\tby lin62.mojsite.com with LMTP\r\n\tid GMMiGwzKH2HJRREAM3T9aw\r\n\t(envelope-from <toni.kutlic@velprom.hr>)\r\n\tfor <app@velprom.hr>; Fri, 20 Aug 2021 17:28:12 +0200"
	}, {
		"key": "return-path",
		"line": "Return-path: <toni.kutlic@velprom.hr>"
	}, {
		"key": "envelope-to",
		"line": "Envelope-to: app@velprom.hr"
	}, {
		"key": "delivery-date",
		"line": "Delivery-date: Fri, 20 Aug 2021 17:28:12 +0200"
	}, {
		"key": "received",
		"line": "Received: from [127.0.0.1] (port=54900 helo=lin62.mojsite.com)\r\n\tby lin62.mojsite.com with esmtpa (Exim 4.94.2)\r\n\t(envelope-from <toni.kutlic@velprom.hr>)\r\n\tid 1mH6R9-004kdz-7s\r\n\tfor app@velprom.hr; Fri, 20 Aug 2021 17:28:03 +0200"
	}, {
		"key": "mime-version",
		"line": "MIME-Version: 1.0"
	}, {
		"key": "date",
		"line": "Date: Fri, 20 Aug 2021 17:28:03 +0200"
	}, {
		"key": "from",
		"line": "From: =?UTF-8?Q?Toni_Kutli=C4=87?= <toni.kutlic@velprom.hr>"
	}, {
		"key": "to",
		"line": "To: app@velprom.hr"
	}, {
		"key": "subject",
		"line": "Subject: test 1"
	}, {
		"key": "user-agent",
		"line": "User-Agent: Roundcube Webmail/1.4.11"
	}, {
		"key": "message-id",
		"line": "Message-ID: <7be6aa03c066ef9cf1674ebfc711cbc5@velprom.hr>"
	}, {
		"key": "x-sender",
		"line": "X-Sender: toni.kutlic@velprom.hr"
	}, {
		"key": "organization",
		"line": "Organization: Velprom d.o.o. production of packaging and POS POP materials"
	}, {
		"key": "content-type",
		"line": "Content-Type: multipart/alternative;\r\n boundary=\"=_d35100111f575402511e2c823a6b7b61\""
	}, {
		"key": "x-plushosting-mailscanner-information",
		"line": "X-PlusHosting-MailScanner-Information: Please contact the ISP for more information"
	}, {
		"key": "x-plushosting-mailscanner-id",
		"line": "X-PlusHosting-MailScanner-ID: 1mH6R9-004kdz-7s"
	}, {
		"key": "x-plushosting-mailscanner",
		"line": "X-PlusHosting-MailScanner: Found to be clean"
	}, {
		"key": "x-plushosting-mailscanner-spamcheck",
		"line": "X-PlusHosting-MailScanner-SpamCheck: not spam (whitelisted),\r\n\tSpamAssassin (not cached, score=-3.619, required 7,\r\n\tALL_TRUSTED -1.00, BAYES_00 -5.00, HTML_IMAGE_ONLY_24 1.62,\r\n\tHTML_MESSAGE 0.00, KAM_DMARC_NONE 0.25, KAM_DMARC_STATUS 0.01,\r\n\tKAM_NUMSUBJECT 0.50, KAM_SHORT 0.00, URIBL_BLOCKED 0.00)"
	}, {
		"key": "x-plushosting-mailscanner-from",
		"line": "X-PlusHosting-MailScanner-From: toni.kutlic@velprom.hr"
	}, {
		"key": "x-spam-status",
		"line": "X-Spam-Status: No"
	}],
	"html": "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /></head><body style='font-size: 12pt; font-family: Arial,Helvetica,sans-serif'>\n<p>test 1</p>\n<div id=\"signature\">-- <br />\n<p><br /></p>\n<p><span style=\"color: #27527b;\"> <strong> <span style=\"font-size: 14pt;\">Toni&nbsp;Kutlić</span> </strong> </span> <br /><span style=\"font-size: 10pt;\"><em>Sistem Integrator</em></span></p>\n<p>&nbsp; <img src=\"https://help.velprom.hr/img/email/logo_velprom_email.png\" /> <br /><img style=\"margin-top: 5px;\" src=\"https://help.velprom.hr/img/email/velprom_descriptors.png\" /> <br /><span style=\"font-size: 10pt;\"> <em> <a href=\"https://g.page/velprom?share\"> <span style=\"color: #27527b;\">Po&scaron;tanska 7,&nbsp; 10410 Velika Gorica</span> </a> </em> </span></p>\n<p><span style=\"font-size: 10pt;\"> <em>mob:&nbsp; +385 92 3133 015 <br /></em><em>tel:</em> &nbsp;&nbsp;&nbsp; <em>+385 1 621 55 41<br /></em><em>fax: &nbsp;&nbsp; +385 1 2099 483<br /></em><em>e-mail:</em>&nbsp; <span style=\"color: #27527b;\"> <em> <a style=\"color: #27527b;\" href=\"mailto:velprom@velprom.hr\">velprom@velprom.hr</a> <br /></em></span> </span> <span style=\"font-size: 10pt;\"><em>web</em>: </span> <a href=\"https://www.velprom.hr\" rel=\"noopener\"> <em><span style=\"color: #27527b;\"><span style=\"font-size: 9pt;\"><span style=\"font-size: 10pt;\">www.velprom.hr</span></span></span></em> </a></p>\n<p><a style=\"margin-right: 5px;\" href=\"https://www.linkedin.com/company/velprom-d-o-o-/\" rel=\"noopener\"> <img src=\"https://help.velprom.hr/img/email/linkedin_velprom_email.png\" width=\"30\" height=\"30\" /></a> <a style=\"margin-right: 5px;\" href=\"https://velprom.us18.list-manage.com/subscribe?u=96cbda681b4a2eb46a5156f69&amp;id=156a645d85\" rel=\"noopener\"> <img src=\"https://help.velprom.hr/img/email/newsletter_velprom_email.png\" width=\"30\" height=\"30\" /></a>&nbsp;<a style=\"margin-right: 5px;\" href=\"https://www.facebook.com/velprom\" rel=\"noopener\"><img src=\"https://help.velprom.hr/img/email/facebook_velprom_email.png\" width=\"30\" height=\"30\" /></a><a style=\"margin-right: 5px;\" href=\"https://g.page/velprom?share\" rel=\"noopener\"><img src=\"https://help.velprom.hr/img/email/map_velprom_email.png\" width=\"30\" height=\"30\" /></a></p>\n</div>\n</body></html>\n",
	"text": "test 1\n\n-- \n\n Toni Kutlić\n_Sistem Integrator_\n\n _ Poštanska 7,  10410 Velika Gorica [1] _\n\n mob:  +385 92 3133 015\n_tel:_     +385 1 621 55 41\nfax:    +385 1 2099 483\n_e-mail:_  velprom@velprom.hr\n _web_: _www.velprom.hr_ [2]\n\n [3] [4]  [5] [1]\n\nLinks:\n------\n[1] https://g.page/velprom?share\n[2] https://www.velprom.hr\n[3] https://www.linkedin.com/company/velprom-d-o-o-/\n[4] https://velprom.us18.list-manage.com/subscribe?u=96cbda681b4a2eb46a5156f69&amp;id=156a645d85\n[5] https://www.facebook.com/velprom",
	"textAsHtml": "<p>test 1</p><p>--</p><p> Toni Kutli&cacute;<br/>_Sistem Integrator_</p><p> _ Po&scaron;tanska 7,  10410 Velika Gorica [1] _</p><p> mob:  +385 92 3133 015<br/>_tel:_     +385 1 621 55 41<br/>fax:    +385 1 2099 483<br/>_e-mail:_  <a href=\"mailto:velprom@velprom.hr\">velprom@velprom.hr</a><br/> _web_: _www.velprom.hr_ [2]</p><p> [3] [4]  [5] [1]</p><p>Links:<br/>------<br/>[1] <a href=\"https://g.page/velprom?share\">https://g.page/velprom?share</a><br/>[2] <a href=\"https://www.velprom.hr\">https://www.velprom.hr</a><br/>[3] <a href=\"https://www.linkedin.com/company/velprom-d-o-o-/\">https://www.linkedin.com/company/velprom-d-o-o-/</a><br/>[4] <a href=\"https://velprom.us18.list-manage.com/subscribe?u=96cbda681b4a2eb46a5156f69&amp;id=156a645d85\">https://velprom.us18.list-manage.com/subscribe?u=96cbda681b4a2eb46a5156f69&amp;id=156a645d85</a><br/>[5] <a href=\"https://www.facebook.com/velprom\">https://www.facebook.com/velprom</a></p>",
	"subject": "test 1",
	"date": "2021-08-20T15:28:03.000Z",
	"to": {
		"value": [{
			"address": "app@velprom.hr",
			"name": ""
		}],
		"html": "<span class=\"mp_address_group\"><a href=\"mailto:app@velprom.hr\" class=\"mp_address_email\">app@velprom.hr</a></span>",
		"text": "app@velprom.hr"
	},
	"from": {
		"value": [{
			"address": "toni.kutlic@velprom.hr",
			"name": "Toni Kutlić"
		}],
		"html": "<span class=\"mp_address_group\"><span class=\"mp_address_name\">Toni Kutli&#x107;</span> &lt;<a href=\"mailto:toni.kutlic@velprom.hr\" class=\"mp_address_email\">toni.kutlic@velprom.hr</a>&gt;</span>",
		"text": "Toni Kutlić <toni.kutlic@velprom.hr>"
	},
	"messageId": "<7be6aa03c066ef9cf1674ebfc711cbc5@velprom.hr>"
};



var email_obj = { 
  
  saved: { type: Number, required: false },
  
  sifra: { type: String, required: false },
  time: { type: Number, required: false },
  attachments: { type: [Schema.Types.Mixed], required: false },
  date: { type: Number, required: false },
  from: { type: String, required: false },

  
headers: { type: [Schema.Types.Mixed], required: false },
subject:   { type: String, required: false },
from: { type: String, required: false },
  

  
  
  email_data: { type: Schema.Types.Mixed, required: false },
  
}; 


var email_schema = new Schema( 
  email_obj,  
  { collection : 'emails'});  
 
module.exports = mongoose.model('Email', email_schema);
