'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

var shelf_obj = { 
  
  shelfs: { type: [Schema.Types.Mixed], required: false },
  
  user_saved: { type: Schema.Types.Mixed, required: false },
  user_edited: { type: Schema.Types.Mixed, required: false },
  saved: { type: Number, required: false },
  edited: { type: Number, required: false },

  
}; 


var alat_shelf_schema = new Schema( 
  shelf_obj,  
  { collection : 'alatshelfs'});  
 
module.exports = mongoose.model('AlatShelf', alat_shelf_schema);
