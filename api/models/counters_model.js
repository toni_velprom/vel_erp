'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema;  

var userCounterSchema = new Schema({  
  
  "users" : Number,
  "sirovs" : Number,
  
  "grupa" : Number,
  
  "partners" : Number,
  "projects" : Number,
  "products" : Number,
  "plates": Number,
  
  "offer_dobav": Number,
  "order_dobav" : Number,
  "in_record": Number,
  "in_bill": Number,
  
  
  "offer_reserv": Number,
  "order_reserv": Number,
  "ms_reserv": Number,
  "utrosak": Number,
  "skart": Number,
  
  "radni_nalog": Number,
  "in_pro_record": Number,
  
  "out_record": Number,
  "out_bill": Number,
  
  "nacrt": Number,
  
  "graf" : Number,
  
  "sample" : Number,
  
  "month": Number
  
  
}, { collection : 'counters' }); 
 
module.exports = mongoose.model('Counter', userCounterSchema);
