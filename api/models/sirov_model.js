'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

var sirov_obj = { 
  
  user_saved: { type: Schema.Types.Mixed, required: false },
  user_edited: { type: Schema.Types.Mixed, required: false },
  saved: { type: Number, required: false },
  edited: { type: Number, required: false },

  sirovina_sifra: { type: String, required: false },
  
  is_ostatak: { type: Boolean, required: false },

  naziv: { type: String, required: false },

  full_naziv: { type: String, required: false },
  
  cijena: { type: Number, required: false },
  alt_cijena: { type: Number, required: false },
  pro_cijena: { type: Number, required: false },
  
  price_hist: { type: [Schema.Types.Mixed], required: false },
  
  
  stari_naziv:{ type: String, required: false },
  povezani_nazivi: { type: String, required: false },

  detaljan_opis: { type: String, required: false },

  osnovna_jedinica: { type: Schema.Types.Mixed, required: false },
  osnovna_jedinica_out: { type: Schema.Types.Mixed, required: false },
  
  normativ_omjer: { type: Number, required: false },

  
  /*
  kolicina: { type: Number, required: false },
  rezervirana_kolicina:{ type: Number, required: false },
  dostupna_kolicina: { type: Number, required: false },
  */
  
  pk_kolicina: { type: Number, required: false },
  nk_kolicina: { type: Number, required: false },
  order_kolicina: { type: Number, required: false },
  sklad_kolicina: { type: Number, required: false },
  
  

  klasa_sirovine: { type: Schema.Types.Mixed, required: false },
  vrsta_materijala: { type: Schema.Types.Mixed, required: false },
  
  vrsta_papira: { type: Schema.Types.Mixed, required: false },
  premaz_papira: { type: Schema.Types.Mixed, required: false },
  
  
  tip_sirovine: { type: Schema.Types.Mixed, required: false },
  /* POPULATE */
  grupa: { type: Schema.Types.ObjectId, ref: 'Grupa', required: false },

  grupa_flat: { type: String, required: false },    

  kvaliteta_1: { type: String, required: false },
  kvaliteta_2: { type: String, required: false },
  
  
  kvaliteta_mix: { type: String, required: false },
  
  
  dobavljac: { type: Schema.Types.ObjectId, ref: 'Partner', required: false },
  dobavljac_flat: { type: String, required: false },
  
  dobav_naziv: { type: String, required: false },
  dobav_sifra: { type: String, required: false },
  
  fsc: { type: Schema.Types.Mixed, required: false },
  fsc_flat: { type: String, required: false },
  
  fsc_perc: { type: Number, required: false },

  povezani_kupci: { type: [Schema.Types.Mixed], required: false },
  povezani_kupci_flat: { type: String, required: false },


  kontra_tok: { type: Number, required: false },
  tok: { type: Number, required: false },
  
  kontra_tok_x_tok: { type: String, required: false },    
  
  po_valu: { type: Number, required: false },
  po_kontravalu: { type: Number, required: false },
  
  sloj_1: { type: Number, required: false },
  val_1: { type: Number, required: false },
  sloj_2: { type: Number, required: false },
  val_2: { type: Number, required: false },
  sloj_3: { type: Number, required: false },
  

  val_x_kontraval: { type: String, required: false },    
  
  debljina_mm: { type: Number, required: false },
  gramaza_g: { type: Number, required: false },
  
  duzina: { type: Number, required: false },
  sirina: { type: Number, required: false },
  visina: { type: Number, required: false },
  
  promjer: { type: Number, required: false },

  povrsina_m2: { type: Number, required: false },
  volumen_m3: { type: Number, required: false },
  volumen_l: { type: Number, required: false },
  
  masa_kg: { type: Number, required: false },
  
  
  min_order: { type: Number, required: false },
  min_order_unit: { type: Schema.Types.Mixed, required: false },
  
  
  min_kolicina: { type: Number, required: false },
  max_kolicina: { type: Number, required: false },

  boja: { type: String, required: false },  
  premaz: { type: String, required: false },
  
  zemlja_pod: { type: Schema.Types.Mixed, required: false },
  zemlja_pod_flat: { type: String, required: false },  

  average_skart: { type: Number, required: false },

  osnovna_neto_masa_kg: { type: Number, required: false },
  osnovna_bruto_masa_kg: { type: Number, required: false },

  /*
  neto_masa_paketa_kg: { type: Number, required: false },
  bruto_masa_paketa_kg: { type: Number, required: false },
  */
  
  komada_u_pakiranju_kom: { type: Number, required: false },


  paketa_na_paleti_kom: { type: Number, required: false },
  komada_na_paleti_kom: { type: Number, required: false },
  
  /*
  NE TREBAM OVO SPREMATI U GLAVNI OBJEKT SIROVINE
  
  neto_masa_palete_kg: { type: Number, required: false },
  bruto_masa_palete_kg: { type: Number, required: false },
  */
  nosac_cijena: { type: Boolean, required: false },
  kapacitet_kukice: { type: Number, required: false },
  
  rabati: { type: [Schema.Types.Mixed], required: false },
  
  specs: { type: [Schema.Types.Mixed], required: false },
  specs_flat: { type: String, required: false },
  
  
  sirov_pics: { type: [Schema.Types.Mixed], required: false },
  sirov_pics_flat: { type: String, required: false },
  
  
  
  web_pristupi: { type: String, required: false },
  
  ask_kolicina_1: { type: Number, required: false },
  ask_kolicina_2: { type: Number, required: false },
  ask_kolicina_3: { type: Number, required: false },
  doc_valuta_manual: { type: Number, required: false },
  doc_ponuda_num: { type: String, required: false },
  order_price: { type: Number, required: false },
  record_est_time: { type: Number, required: false },

  
  statuses: [{ type: Schema.Types.ObjectId, ref: 'Status', required: false }],

  records: { type: [Schema.Types.Mixed], required: false },
  
  locations: { type: [Schema.Types.Mixed], required: false },
  
  for_search: { type: Boolean, required: false },
  
  
  original: { type: Schema.Types.ObjectId, ref: 'Product', required: false },
  
  
  kalk: { type: Schema.Types.ObjectId, ref: 'Kalk', required: false },
  proces_id: { type: String, required: false },
  
  ulaz_id: { type: String, required: false },
  izlaz_id: { type: String, required: false },
  
  /*  
  NE TREBAM OVO SPREMATI U GLAVNI OBJEKT SIROVINE
  paleta_x: { type: Number, required: false },
  paleta_y: { type: Number, required: false },
  paleta_z: { type: Number, required: false },
  */
  alat_stamp_num: { type: Number, required: false },
  alat_max_stamp: { type: Number, required: false },
  alat_nest_count: { type: Number, required: false },
  
  
  alat_owner: { type: Schema.Types.ObjectId, ref: 'Partner', required: false },
  list_kupaca: [{ type: Schema.Types.ObjectId, ref: 'Partner', required: false }],
  
    
  alat_prirez: { type: String, required: false },
  alat_val: { type: Number, required: false },
  alat_kontra: { type: Number, required: false },
  alat_fi: { type: Number, required: false },
  
  
  alat_shelf: { type: Schema.Types.Mixed, required: false },
  
  rok_dostave: { type: Number, required: false },
  exp_month: { type: Number, required: false },
  
  
  voz_user: { type: Schema.Types.Mixed, required: false },
  voz_marka: { type: String, required: false },
  voz_reg: { type: String, required: false },
  voz_sas: { type: String, required: false }, // šasija
  voz_buy_date: { type: Number, required: false },
  voz_leasing: { type: Boolean, required: false },
  voz_leasing_house: { type: String, required: false },
  
  voz_leasing_exp_date: { type: Number, required: false },
  voz_last_servis_date: { type: Number, required: false },
  voz_reg_exp_date: { type: Number, required: false },
  
  
  
  
  
}; 


var sirov_schema = new Schema( 
  sirov_obj,  
  { collection : 'sirovine'});  
 
module.exports = mongoose.model('Sirov', sirov_schema);
