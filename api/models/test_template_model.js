'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

var test_template_obj = { 

  
  sifra: { type: String, required: false },
  template_sifra: { type: String, required: false },
  template_ime: { type: String, required: false },
  
  deps: { type: [Schema.Types.Mixed], required: false }, // koji odjel
  
  user: { type: Schema.Types.Mixed, required: false },
  
  active_from: { type: Number, required: false },
  active_to: { type: Number, required: false },
  duration: { type: Number, required: false },

  pits: { type: [Schema.Types.Mixed], required: false },
  
  
  started: { type: Number, required: false },
  done: { type: Boolean, required: false },

  send_email: { type: Boolean, required: false },
  
}; 


var test_template_schema = new Schema( 
  test_template_obj,  
  { collection : 'test_templates'});  
 
module.exports = mongoose.model('TestTemplate', test_template_schema);
