'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

// var ParkPlaceOwner = mongoose.model('ParkPlaceOwner');

var project_obj = { 
  
  
  user_saved: { type: Schema.Types.Mixed, required: false },
  user_edited: { type: Schema.Types.Mixed, required: false },
  saved: { type: Number, required: false },
  edited: { type: Number, required: false },
  
  create_time: { type: Number, required: false },
  

  proj_sifra: { type: String, required: false },
  naziv: { type: String, required: false },
  
  referent: { type: Schema.Types.Mixed, required: false },
  referent_flat: { type: String, required: false },
  
  referent_zamjena: { type: Schema.Types.Mixed, required: false },
  referent_zamjena_flat: { type: String, required: false },
  
  kupac: { type: Schema.Types.ObjectId, ref: 'Partner', required: false },
  kupac_flat: { type: String, required: false },
  
  proj_status: { type: Schema.Types.Mixed, required: false },
  proj_status_flat: { type: String, required: false },
  
  komentar: { type: String, required: false },
  interni_komentar: { type: String, required: false },
  
  start: { type: Number, required: false },
  end: { type: Number, required: false },
  
  items: [{ type: Schema.Types.ObjectId, ref: 'Product', required: false }],
  
  
  rok_proiz: { type: Number, required: false },
  rok_isporuke: { type: Number, required: false },
  
  potrebno_dana_proiz: { type: Number, required: false },
  potrebno_dana_isporuka: { type: Number, required: false },
   
  rok_za_def: { type: Number, required: false },

  order_osoba: { type: String, required: false },
  
  free_kontakt: { type: String, required: false },
  order_kontakt: { type: Schema.Types.Mixed, required: false },
  doc_adresa: { type: Schema.Types.Mixed, required: false },
  dostav_adresa: { type: Schema.Types.Mixed, required: false },
  payment: { type: Schema.Types.Mixed, required: false },
  
  proj_specs: { type: [Schema.Types.Mixed], required: false },
  proj_specs_flat: { type: String, required: false },
  
}; 

var project_schema = new Schema( 
  project_obj,  
  { collection : 'projects'});  
 
module.exports = mongoose.model('Project', project_schema);
