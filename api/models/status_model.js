'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

// var ParkPlaceOwner = mongoose.model('ParkPlaceOwner');

var status_obj = { 
  
  
  /* ovo  mi govori u kojem modulu je status spremljen tj koji je parent module od statusa */
  product_model_id: { type: String, required: false },
  partner_model_id: { type: String, required: false },
  sirov_model_id: { type: String, required: false },
  
  // isto kao i običan _id, ali ovo mi služi za generiranje redova u listi statusa !!!!
  // isto kao i običan _id, ali ovo mi služi za generiranje redova u listi statusa !!!!
  status_id: { type: String, required: false },
  
  full_product_name: { type: String, required: false },

  product_id: { type: String, required: false },
  product_tip: { type: Schema.Types.Mixed, required: false },
  
  
  link: { type: String, required: false },
  
  kupac_id: { type: String, required: false },
  kupac_naziv: { type: String, required: false },
  
  proj_sifra: { type: String, required: false },
  item_sifra: { type: String, required: false },
  variant: { type: String, required: false },
  
  sifra: { type: String, required: false },
  status: { type: String, required: false },
  
  elem_parent: { type: String, required: false },
  
  elem_parent_object: { type: Schema.Types.Mixed, required: false },

  time: { type: Number, required: false },

  work_times: { type: [Schema.Types.Mixed], required: false },
  
  
  worker_error_cost: { type: Number, required: false },
  worker_error_users: { type: [Schema.Types.Mixed], required: false },
  worker_error_possible_solution: { type: String, required: false },
  worker_error_solution: { type: String, required: false },
  
  
  cat: { type: String, required: false },

  docs: { type: [Schema.Types.Mixed], required: false },
  docs_flat: { type: String, required: false },
  
  
  rn_files: { type: [Schema.Types.Mixed], required: false },
  rn_files_flat: { type: String, required: false },
  rn_alat: { type: [Schema.Types.Mixed], required: false },
  rn_alat_flat: { type: String, required: false },

  est_deadline_hours: { type: Number, required: false },
  est_deadline_date: { type: Number, required: false },

  start:  { type: Number, required: false },
  end: { type: Number, required: false },

  old_status_tip: { type: Schema.Types.Mixed, required: false },
  
  status_tip: { type: Schema.Types.Mixed, required: false },
  status_tip_flat: { type: String, required: false },

  tezina: { type: Number, required: false },

  dep_from: { type: Schema.Types.Mixed, required: false },
  dep_from_flat: { type: String, required: false },
  
  from: { type: Schema.Types.Mixed, required: false },
  from_flat: { type: String, required: false },
  
  
  dep_to: { type: Schema.Types.Mixed, required: false },
  dep_to_flat: { type: String, required: false },
  
  to: { type: Schema.Types.Mixed, required: false },
  to_flat: { type: String, required: false },

  old_komentar: { type: String, required: false },
  komentar: { type: String, required: false },
  
  seen: { type: Boolean, required: false },
  
  reply_for: { type: String, required: false },
  responded: { type: Boolean, required: false },
  
  done_for_status: { type: String, required: false },
  
  work_finished: { type: Boolean, required: false },
  
  
  sirov: { type: Schema.Types.Mixed, required: false },
  sirov_flat: { type: String, required: false },
  
  order_sirov_count: { type: Number, required: false }, /* ovo je količina neke sirovine koju netko na projektu traži */
  
  ask_kolicina: { type: Number, required: false }, /* ovo je količina sirovine koju Martina želi naručiti ---> to ne mora biti isti broj !!!! */
  
  adresa_order_dobav: { type: Schema.Types.Mixed, required: false },
  lang_order_dobav: { type: Schema.Types.Mixed, required: false },
  
  /* ovo mi služi kada Martina šalje ponude da samo jednom odgovori na zahtjev za sirovinu kada traži ponude */
  /* kasnije Martina može poslati N broj RFQ-a ali će samo JEDNOM poslati odgovor na statuse unutar projekata */
  offer_sirov_id: { type: String, required: false },
  order_sirov_id: { type: String, required: false },
  
  
  records: { type: [Schema.Types.Mixed], required: false },
  
  
  
  kalk: { type: Schema.Types.ObjectId, ref: 'Kalk', required: false },
  
  
  proces_id: { type: String, required: false },
  ulaz_id: { type: String, required: false },
  izlaz_id: { type: String, required: false },
  
  rok_isporuke:  { type: Number, required: false },
  potrebno_dana_proiz:  { type: Number, required: false },
  potrebno_dana_isporuka:  { type: Number, required: false },
  rok_proiz:  { type: Number, required: false },
  rok_za_def:  { type: Number, required: false },
  
  branch_done: { type: Boolean, required: false },
  
  prio:  { type: Number, required: false },
  
  rn_droped: { type: Schema.Types.Mixed, required: false }, // ovoj je samo za radne procese ( također postoji i pod-status za radni proces )
  rn_extra: { type: Schema.Types.Mixed, required: false }, // ovoj je samo za radne procese ( također postoji i pod-status za radni proces )
  
  rn_finished: { type: Schema.Types.Mixed, required: false }, // ovoj je samo za radne procese ( također postoji i pod-status za radni proces )
  rn_verif: { type: Schema.Types.Mixed, required: false }, // ovoj je samo za radne procese ( također postoji i pod-status za radni proces )
  

}; 

var status_schema = new Schema( 
  status_obj,  
  { collection : 'statuses'});  
 
module.exports = mongoose.model('Status', status_schema);
