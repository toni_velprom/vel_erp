'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

var admin_obj = { 
  
  user_saved: { type: Schema.Types.Mixed, required: false },
  user_edited: { type: Schema.Types.Mixed, required: false },
  saved: { type: Number, required: false },
  edited: { type: Number, required: false },

  status_conf: { type: Schema.Types.Mixed, required: false },
  
}; 


var admin_schema = new Schema( 
  admin_obj,  
  { collection : 'admin_conf'});  
 
module.exports = mongoose.model('AdminConf', admin_schema);
