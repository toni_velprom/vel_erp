'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

// var ParkPlaceOwner = mongoose.model('ParkPlaceOwner');

var product_obj = { 
  
  
  id: { type: String, required: false }, // ovo je id za html element !!!!
  
  order_num: { type: Number, required: false },
  
  sample_for_id: { type: String, required: false },
  sample_for_kalk: { type: String, required: false },
  sample_sifra: { type: String, required: false },
  
  
  user_saved: { type: Schema.Types.Mixed, required: false },
  user_edited: { type: Schema.Types.Mixed, required: false },
  saved: { type: Number, required: false },
  edited: { type: Number, required: false },
  
  prod_specs: { type: [Schema.Types.Mixed], required: false },
  prod_specs_flat: { type: String, required: false },
  
  
  nacrt_specs: { type: [Schema.Types.Mixed], required: false },
  nacrt_specs_flat: { type: String, required: false },
  
  
  graf_specs: { type: [Schema.Types.Mixed], required: false },
  graf_specs_flat: { type: String, required: false },
  
  
  alat_sifra: { type: String, required: false },
  
  
  project_id: { type: String, required: false },
  
  proj_sifra: { type: String, required: false },
  item_sifra: { type: String, required: false },
  variant: { type: String, required: false },
  
  sifra: { type: String, required: false },
  
  variant_of: { type: Schema.Types.ObjectId, ref: 'Product', required: false },
  original: { type: Schema.Types.ObjectId, ref: 'Product', required: false },
  
  
  tip: { type: Schema.Types.Mixed, required: false },

  rok_proiz: { type: Number, required: false },
  rok_isporuke: { type: Number, required: false },
  
  potrebno_dana_proiz: { type: Number, required: false },
  potrebno_dana_isporuka: { type: Number, required: false },
   
  rok_za_def: { type: Number, required: false },


  naklada: { type: Number, required: false },
  produced: { type: Number, required: false },
  
  naziv: { type: String, required: false },
  
  
  full_product_name: { type: String, required: false },
  
  full_kupac_name: { type: String, required: false },
  
  partner: { type: Schema.Types.ObjectId, ref: 'Partner', required: false },
  
  kupac_naziv: { type: String, required: false },
  kupac_sifra: { type: String, required: false },

  mater: { type: Schema.Types.Mixed, required: false },
  
  start: { type: Number, required: false },
  end: { type: Number, required: false },

  komentar: { type: String, required: false },
  interni_komentar: { type: String, required: false },

  duzina: { type: Number, required: false },  
  out_duzina: { type: Number, required: false },
  
  sirina: { type: Number, required: false },
  out_sirina: { type: Number, required: false },
  
  
  broj_polica: { type: Number, required: false },
  
  visina: { type: Number, required: false },
  
  visina_header_ext: { type: Number, required: false },

  mont_ploce: [{ type: Schema.Types.ObjectId, ref: 'Plate', required: false }],

  tenders: { type: [Schema.Types.Mixed], required: false },

  elements:  { type: [Schema.Types.Mixed], required: false },
  
  alati: [{ type: Schema.Types.ObjectId, ref: 'Sirov', required: false }],
  
  statuses: [{ type: Schema.Types.ObjectId, ref: 'Status', required: false }],
  
  kalk: { type: Schema.Types.ObjectId, ref: 'Kalk', required: false },
 
  
  active: { type: Boolean, required: false },
  
  is_original: { type: Boolean, required: false },

  uzima_visak: { type: Boolean, required: false },
  placa_visak: { type: Boolean, required: false },
  
  popust_visak: { type: Number, required: false },
  komada_visak: { type: Number, required: false },
  
  
  plate_color_cover: { type: Number, required: false },
  plate_noz_big: { type: Number, required: false },
  plate_nest_count: { type: Number, required: false },
  
  kalk_comp_id: { type: String, required: false },
  
  
  doc_adresa: { type: Schema.Types.Mixed, required: false },
  dostav_adresa: { type: Schema.Types.Mixed, required: false },
  
  
  fixed_prices: { type: [Schema.Types.Mixed], required: false },
    
  
  
}; 

var product_schema = new Schema( 
  product_obj,  
  { collection : 'products'});  
 
module.exports = mongoose.model('Product', product_schema);
