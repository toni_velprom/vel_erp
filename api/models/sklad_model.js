'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

var sklad_obj = { 
  
  sifra: { type: String, required: false },
  mat: { type: Schema.Types.Mixed, required: false },
  naziv_sirovine: { type: String, required: false },
  val: { type: Number, required: false },
  kontra_val: { type: Number, required: false },
  kontra_tok: { type: Number, required: false },
  tok: { type: Number, required: false },
  pozicija: { type: String, required: false },
  sklad_count: { type: Number, required: false },
  count: { type: Number, required: false },
  time: { type: Number, required: false },
  last_state: { type: String, required: false },
  history: { type: [Schema.Types.Mixed], required: false },
  
  
}; 

var sklad_schema = new Schema( 
  sklad_obj,  
  { collection : 'sklad'});  
 
module.exports = mongoose.model('Sklad', sklad_schema);
