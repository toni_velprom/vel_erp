'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

var partner_obj = { 
  
  user_saved: { type: Schema.Types.Mixed, required: false },
  user_edited: { type: Schema.Types.Mixed, required: false },
  saved: { type: Number, required: false },
  edited: { type: Number, required: false },
  
  partner_sifra: { type: String, required: false },
  oib : { type: String, required: false },
  vat_num: { type: String, required: false },
  naziv : { type: String, required: false },
  
  kupac: { type: Boolean, required: false },
  dobavljac: { type: Boolean, required: false },
  fiz_osoba: { type: Boolean, required: false },
  web_kupac: { type: Boolean, required: false },
  ino_kupac: { type: Boolean, required: false },
  
  djelatnost: { type: String, required: false },
  
  grupacija: { type: Schema.Types.ObjectId, ref: 'Grupacija', required: false },
  grupacija_flat : { type: String, required: false },
  
  
  statuses: [{ type: Schema.Types.ObjectId, ref: 'Status', required: false }],
  
  
  ibans: { type: [Schema.Types.Mixed], required: false },
  ibans_flat: { type: String, required: false },
  
  status: { type: Schema.Types.Mixed, required: false },
  status_flat: { type: String, required: false },
  
  komentar_all_adrese: { type: String, required: false },
  komentar_all_kont: { type: String, required: false },
  
  kontakti: { type: [Schema.Types.Mixed], required: false },
  kontakti_flat: { type: String, required: false },
  
  
  adrese : { type: [Schema.Types.Mixed], required: false },
  adrese_flat : { type: String, required: false },


  public_komentar: { type: String, required: false },
  private_komentar: { type: String, required: false },
  skla_komentar: { type: String, required: false },

  web_pristupi: { type: String, required: false },

  nacini_placanja: { type: [Schema.Types.Mixed], required: false },
  
  placanje_kupac: { type: [Schema.Types.Mixed], required: false },
  
  placanje_dobav: { type: [Schema.Types.Mixed], required: false },
  
  
  nacini_dostave: { type: [Schema.Types.Mixed], required: false },
  nacini_dostave_flat: { type: String, required: false },

  valuta_placanja_kupac: { type: Number, required: false },
  valuta_placanja_dobav: { type: Number, required: false },

  def_pays: { type: [Schema.Types.Mixed], required: false },
  def_pays_flat: { type: String, required: false },

  limit_zaduzenja_kupac: { type: Number, required: false },
  limit_zaduzenja_dobav: { type: Number, required: false },
  
  zaduznice_kupac: { type: [Schema.Types.Mixed], required: false },
  zaduznice_dobav: { type: [Schema.Types.Mixed], required: false },
  cassa_sconto_kupac: { type: [Schema.Types.Mixed], required: false },
  cassa_sconto_dobav: { type: [Schema.Types.Mixed], required: false },
  end_year_bonus_kupac: { type: [Schema.Types.Mixed], required: false },
  end_year_bonus_dobav: { type: [Schema.Types.Mixed], required: false },


  ugovori: { type: [Schema.Types.Mixed], required: false },  
  ugovori_flat: { type: String, required: false },

  tenders: { type: [Schema.Types.Mixed], required: false },  
  tenders_flat: { type: String, required: false },
  
  uzima_visak: { type: Boolean, required: false },
  placa_visak: { type: Boolean, required: false },
  popust_visak: { type: Number, required: false },
  
  docs: { type: [Schema.Types.Mixed], required: false },
  docs_flat: { type: String, required: false },
  // orders: [{ type: Schema.Types.ObjectId, ref: 'Product', required: false }],
  
  
}; 

var partner_schema = new Schema( 
  partner_obj,  
  { collection : 'partners'});  
 
module.exports = mongoose.model('Partner', partner_schema);
