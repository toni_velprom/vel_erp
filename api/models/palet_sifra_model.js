'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema;  

var palet_sifra_schema = new Schema({  
  
  "counter" : Number,
  "sifre" : { type: [Schema.Types.Mixed], required: false },
  
}, { collection : 'sifre_pakiranja' }); 
 
module.exports = mongoose.model('PaletSifra', palet_sifra_schema);
