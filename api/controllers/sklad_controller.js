var mongoose = require('mongoose');

// mongoose.set('useFindAndModify', false);

var User = mongoose.model('User');
var Counter = mongoose.model('Counter');
var Grupa = mongoose.model('Grupa');

var Project = mongoose.model('Project');
var Product = mongoose.model('Product');
var Status = mongoose.model('Status');
var Kalk = mongoose.model('Kalk');
var Sklad = mongoose.model('Sklad');

var nodemailer = require('nodemailer');
var multer = require('multer')

var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);


// var id = mongoose.Types.ObjectId('4edd40c86762e0fb12000003');
// var _id = mongoose.mongo.BSONPure.ObjectID.fromHexString("4eb6e7e7e9b7f4194e000001");

var find_controller = require('./find_controller.js');


exports.all_sklads = function(req, res) {

   Sklad.find({})
    .exec(function(err, sklads) {
      if (err) {
        res.send({success: false,  msg: `Greška kod dobivanja svih skladišnih stanja`, error: err });
      } else {
        
      var clean_sklads = global.mng_to_js_array(sklads);
        
      res.send({success: true,  data: clean_sklads });
      };
    });
  
};


function save_new_kalk(kalk_obj, res) {
  
  // --------------------------------------------------------------------------
  var new_kalk = new Kalk(kalk_obj);
  // save to DB
  new_kalk.save(function(err, kalk) {
    
    if (err) {
      console.log(`Error response prilikom SAVE NOVE KALKLULACIJE`, err);
      res.json({success: false, error: err });
      return;
    };
    
    var clean_kalk = global.mng_to_js(kalk);
    
    // DODAJ OVAJ PRODUCT U ITEMS ARRAY U PROJEKTU
    Product.findOneAndUpdate(
      { _id: clean_kalk.product_id },
      { $set: { kalk: clean_kalk._id } },
      { new: true }, function(prod_err, product) {
        
        if (prod_err) {
          console.log(`Error prilikom SET kalk into product document`, err);
          res.json({success: false, error: prod_err });
        } else {
          res.json({success: true, kalk: clean_kalk });
        };
        
      });
    
  }); // kraj save new proj
  // --------------------------------------------------------------------------  
  
};


exports.save_kalk = function(req, res) {

  var kalk_obj = req.body;
      
  var kalk_type = kalk_obj.type;
  
  var set_query = null;
  
  if ( kalk_type == `offer` ) set_query = { $set: { offer_kalk: kalk_obj.offer_kalk, meta_offer_kalk: kalk_obj.meta_offer_kalk } };
  if ( kalk_type == `pro` ) set_query = { $set: { pro_kalk: kalk_obj.pro_kalk, meta_pro_kalk: kalk_obj.meta_pro_kalk } };
  if ( kalk_type == `post` ) set_query = { $set: { post_kalk: kalk_obj.post_kalk, meta_post_kalk: kalk_obj.post_kalk } };
      
  // AKO PRODUCT IMA ID ONDA JE OVO ZAPRAVO UPDATE !!!
  if ( kalk_obj._id ) {
  
    Kalk.findOneAndUpdate(
      { _id: kalk_obj._id },
      set_query,
      { new: true }, function(err, kalk) {

        if (err) {
          console.log(`Error response prilikom UPDATE KALKULACIJE`, err);
          res.json({success: false, error: err, msg: `Greška prilikom UPDATE KALKULACIJE !!!` })
          return;
        };
        
        var clean_kalk = global.mng_to_js(kalk);
        res.json({success: true, kalk: clean_kalk });

      });

    // kraj ako je UPDATE
    
  } 
  else {
    

    save_new_kalk(kalk_obj, res);
    

  }; // kraj od else ( NEMA _id ) 
    

};

