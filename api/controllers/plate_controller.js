var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var nodemailer = require('nodemailer');
var fs = require('fs')
var path = require("path");


var Plate = mongoose.model('Plate');

var User = mongoose.model('User');
var Counter = mongoose.model('Counter');
var Grupa = mongoose.model('Grupa');

var Project = mongoose.model('Project');
var Product = mongoose.model('Product');
var Status = mongoose.model('Status');
var Kalk = mongoose.model('Kalk');

var nodemailer = require('nodemailer');
var multer = require('multer')

var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);


// var id = mongoose.Types.ObjectId('4edd40c86762e0fb12000003');
// var _id = mongoose.mongo.BSONPure.ObjectID.fromHexString("4eb6e7e7e9b7f4194e000001");

var find_controller = require('./find_controller.js');


exports.find_plate = function(req, res) {

  global.cit_search_remote(req, res, Plate)
  .then(function(result) {
    res.json(result) 
  })
  .catch(function(error) {
    res.json(error) 
  });
  
};


function save_new_plate(plate_obj, res) {
  
  // --------------------------------------------------------------------------
  var new_plate = new Plate(plate_obj);
  // save to DB
  new_plate.save(function(err, plate) {
    
    if (err) {
      console.log(`Error response prilikom SAVE MONTAŽNE PLOČE`, err);
      res.json({success: false, error: err, msg: `Error response prilikom SAVE MONTAŽNE PLOČE` });
      return;
    };
    
    var clean_plate = global.mng_to_js(plate);
    
    
    // vrati odgovor da je success true iako još na serveru treba napraviti update na svim productima
    // tj. treba dodati id od ovog plate na svaki product unutar propertija mont ploce
    
    res.json({success: true, plate: clean_plate });
    
    
    $.each( clean_plate.products, function(p_ind, product_obj) {
    
      Product.findOneAndUpdate(
        { _id: product_obj.product_id },
        { $addToSet: { mont_ploce: clean_plate._id } },
        {new: true}, 
        function(prod_err, updated_product) {

          if (prod_err) {
            console.error(`Error prilikom ADD TO SET plate _id into product mont_ploce`, prod_err);
          } else {
            console.log(`Uspješno dodao plate _id into product mont_ploce`);
          }; 

      }); 
        
    });
   
  }); // kraj save new plate
  // --------------------------------------------------------------------------  
  
};


exports.save_plate = function(req, res) {

  var plate_obj = req.body;
  
  
  // obriši statuse jer ću id uzimati ručno !!!!!
  // plate_obj.statuses = [];
  
  if ( plate_obj._id ) {
  
    Plate.findOneAndUpdate(
      { _id: plate_obj._id },
      plate_obj,
      { new: true }, function(err, plate) {

        if (err) {
          console.log(`Error response prilikom UPDATE MONTAŽNE PLOČE`, err);
          res.json({success: false, error: err, msg: `Greška prilikom UPDATE MONTAŽNE PLOČE !!!` })
          return;
        };
        
        var clean_plate = global.mng_to_js(plate);
        res.json({success: true, plate: clean_plate });

      });
      // kraj ako je UPDATE
      
  } else {
    
    save_new_plate(plate_obj, res);

  }; // kraj od else ( NEMA _id ) 
    

};




