
var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);


global.socket_users = [];


function find_socket_user( user_num ) {
  
  var found_socket_user = null;
  $.each( global.socket_users, function(socket_user_index, socket_user_obj) {
    if ( user_num == socket_user_obj.user.user_number ) {
      found_socket_user = socket_user_obj;
    };
  });
  return found_socket_user;
};
global.find_socket_user = find_socket_user;


function remove_socket_user( socket_id ) {
  
  var socket_user_index = null;
  $.each( global.socket_users, function(u_index, socket_user_obj) {
    if ( socket_id == socket_user_obj.socket_id ) {
      socket_user_index = u_index;
    };
  });
  

  if ( socket_user_index !== null ) socket_user_index = global.socket_users.splice(socket_user_index, 1);
    
  return socket_user_index;
};
global.remove_socket_user = remove_socket_user;


function update_socket_user( user_num, new_obj ) {
  
  var found_socket_user_index = null;
  $.each( global.socket_users, function(socket_user_index, socket_user_obj) {
    if ( user_num == socket_user_obj.user.user_number ) {
       found_socket_user_index = socket_user_index;
    };
  });
  
  // ako postoji socket_user s tim imenom onda samo update sa novim idjem
  if ( found_socket_user_index !== null ) {
    global.socket_users[found_socket_user_index] = new_obj;
    console.log(`napravio UPDATE za SOCKET usera ` + new_obj.user.email )
  } else {
    // ako ne postoji ramp s tim imenom onda pushaj 
    global.socket_users.push(new_obj);
    console.log(`DODAO SOCKET NOVOG usera ` + new_obj.user.email )
  };
  
  return found_socket_user_index;
  
};
global.update_socket_user = update_socket_user;


global.cit_io.on('connection', (socket) => {
  // console.log('a user connected');
  // console.log(socket);
  
  global.cit_io.to(`${socket.id}`).emit("check_in", socket.id);
  
  socket.on("check_in", function( user ) {

    console.log('PRIJAVIO se ' + user.email + ' !!!!!!!');

    var novi_socket_obj = {
      user: user,
      socket_id: socket.id,
      time: Date.now()
    };

    console.log( socket.id );
    // napravi novi objekt sa novim socketom ili zamjeni stari socket id sa ovim novim
    update_socket_user( user.user_number, novi_socket_obj );
    
    
    global.cit_io.to(`${socket.id}`).emit("check_in", socket.id);

  });
  
  
  socket.on("new_status", function( status ) {

    console.log('NOVI STATUS');
    var socket_user = find_socket_user(status.to.user_number);
    // šalji socket msg samo ako je našao socket user id
    if ( socket_user ) global.cit_io.to(`${socket_user.socket_id}`).emit("new_status", status);
    
  });
  
  
  socket.on("reply_status", function( status ) {

    console.log('REPLY STATUS');
    var socket_user = find_socket_user(status.to.user_number);
    // šalji socket msg samo ako je našao socket user id
    if ( socket_user ) global.cit_io.to(`${socket_user.socket_id}`).emit("reply_status", status);
    
  });  
  
  
  
  socket.on("disconnecting", (reason) => {
    
    var obrisani =  global.cit_deep( remove_socket_user(socket.id) );
    if ( obrisani !== null ) {
      console.log(`Socket user ${obrisani[0].user.full_name} je obrisan !!!!!`);
    } else {
      console.log(`Ne mogu obrisati socket usera jer ga nisam našao !!!!!`);
    };
    
  });
  
  
  
  socket.on('disconnect', (socket) => {
    
    console.log('user disconnected');
    
  });
  
}); // kraj user connected
