var mongoose = require('mongoose');

// mongoose.set('useFindAndModify', false);

var User = mongoose.model('User');
var Counter = mongoose.model('Counter');
var Grupa = mongoose.model('Grupa');

var Project = mongoose.model('Project');
var Product = mongoose.model('Product');
var Status = mongoose.model('Status');
var Kalk = mongoose.model('Kalk');

var nodemailer = require('nodemailer');
var multer = require('multer')

var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);


// var id = mongoose.Types.ObjectId('4edd40c86762e0fb12000003');
// var _id = mongoose.mongo.BSONPure.ObjectID.fromHexString("4eb6e7e7e9b7f4194e000001");

var find_controller = require('./find_controller.js');


exports.find_product = function(req, res) {

  global.cit_search_remote(req, res, Product)
  .then(function(result) {
    res.json(result) 
  })
  .catch(function(error) {
    res.json(error) 
  });
  
};


function generate_full_product_name(data) {

  var tip_naziv = (data.tip && data.tip.naziv) ? data.tip.naziv : "";
  
  var nac_gp_sifra = ``;
  var template_nacrta = ``;

  if ( data.nacrt_specs && $.isArray(data.nacrt_specs) && data.nacrt_specs.length > 0 ) {

    var criteria = [ '!time' ];

    if ( typeof window == "undefined" ) {
      global.multisort( data.nacrt_specs, criteria );
    } else {
      multisort( data.nacrt_specs, criteria );
    };

    template_nacrta = data.nacrt_specs[0].template;

    nac_gp_sifra = template_nacrta;

    if ( data.nacrt_specs[0].graf_sifra ) {
      nac_gp_sifra = template_nacrta + "-" + data.nacrt_specs[0].graf_sifra;
    };

  };
  
  
  var full_product_name = 
    (data.sample_sifra ? data.sample_sifra+" " : "" ) +
    (data.sifra ? data.sifra+" " : "" ) +
    (nac_gp_sifra ? nac_gp_sifra+" " : "") + 
    tip_naziv + " " +
    (data.duzina || "" ) + "x" +
    (data.sirina || "" ) + "x" +
    (data.visina || "" ) + " / " +
    (data.naziv || "" ) + " / " +
    (data.kupac_naziv || "" ) + " / " +
    (data.kupac_sifra || "" );

  return full_product_name;

};


function save_new_product(product_obj, res) {
  
  product_obj.full_product_name = generate_full_product_name(product_obj);
  
  
  // --------------------------------------------------------------------------
  var new_product = new Product(product_obj);
  // save to DB
  new_product.save(function(err, product) {
    
    if (err) {
      console.log(`Error response prilikom SAVE PRODUCTA`, err);
      res.json({success: false, error: err });
      return;
    };
    
    var clean_product = global.mng_to_js(product);
    
    // DODAJ OVAJ PRODUCT U ITEMS ARRAY U PROJEKTU
    Project.findOneAndUpdate(
      { _id: clean_product.project_id },
      { $addToSet: { items: clean_product._id } },
      {new: true}, 
      function(proj_err, updated_project) {
        
        if (proj_err) {
          console.log(`Error prilikom ADD TO SET product _id into project items`, proj_err);
          res.json({success: false, error: proj_err, msg: `Error prilikom ADD TO SET product _id into project items` });
          
        } else {
          
          
          // DODAJ OVAJ PRODUCT U ITEMS ARRAY U PROJEKTU
          Kalk.findOneAndUpdate(
            { _id: clean_product.kalk },
            { $set: { 
              
              full_product_name: clean_product.full_product_name,
              proj_sifra: clean_product.proj_sifra,
              item_sifra: clean_product.item_sifra,
              variant: clean_product.variant,
              product_id: clean_product._id,
                
              } 
            },
            {new: true}, 
            function(kalk_err, updated_kalk) {

              if (kalk_err) {
                console.log(`Error prilikom ADD TO SET unutar kalk`, kalk_err);
                res.json({success: false, error: proj_err, msg: `Error prilikom ADD TO SET unutar kalk` });
              } else {
                
                var clean_kalk = global.mng_to_js(updated_kalk);
                res.json({ success: true, product: clean_product });
                
              };

            }); 
          
          
        };
        
      });    
    
  }); // kraj save new proj
  // --------------------------------------------------------------------------  
  
};


exports.save_product = function(req, res) {

  var product_obj = req.body;
  
  product_obj.full_product_name = generate_full_product_name(product_obj);
      
  // AKO PRODUCT IMA ID ONDA JE OVO ZAPRAVO UPDATE !!!
  if ( product_obj._id ) {

    Product.findOneAndUpdate(
      { _id: product_obj._id },
      product_obj,
      { new: true }, function(err, product) {

        if (err) {
          console.log(`Error response prilikom UPDATE PRODUCTA`, err);
          res.json({success: false, error: err, msg: `Greška prilikom UPDATE PRODUCTA !!!` })
          return;
        };
        
        var clean_product = global.mng_to_js(product);
        res.json({success: true, product: clean_product });

      });

    // kraj ako je UPDATE
    
  } 
  else {
    
    // NEMA _id
    
    if ( !product_obj.item_sifra ) {
      
      // ako nema _id i isto tako nema item sifra
      // OVO ZNAČI DA JE POSVE NOVI PRODUCT  !!!!!!!
      // ako nema _id i isto tako nema item sifra
      
    
      Counter.findOneAndUpdate({ products: { $ne: null } }, { $inc: { products: 1 } }, { "new": true })
      .exec(function(err, counter) {

        if (err) {
            res.json({ 
              success: false, 
              msg: "Product nije spremljen! Counter problem.",
              product_sifra: null
            });
          return;
        };  

        var clean_count = mng_to_js(counter);

        var new_product_sifra = "" + clean_count.products;
        
        product_obj.item_sifra = new_product_sifra;
        product_obj.variant = "1";
        product_obj.sifra = product_obj.item_sifra + "-" + product_obj.variant;

        
        save_new_product(product_obj, res);
        
      }); // kraj countera

    } 
    else {
      
      // ako nema _id ALI IMA item sifra
      // ako nema _id ALI IMA item sifra
      // ako nema _id ALI IMA item sifra
      
      /*
      --------------------------------------------------------------------------------
      OVO JE SLUČAJ KADA USER KOPIRA POSTOJEĆI PRODUCT TJ ONDA POSTAJE NOVA VARIJANTA
      --------------------------------------------------------------------------------
      */
      
      Product.find({ item_sifra: product_obj.item_sifra })
      .exec(function(err, products) {
        
        if (err) {
          console.log(`Error response prilikom provjere item_sifra u PRODUCTU`, err);
          res.json({success: false, error: err, msg: `Greška prilikom item_sifra u PRODUCTU !!!` })
          return;
        };

        if ( products.length > 0 ) {
          
          var clean_products = global.mng_to_js_array(products);
          var max_variant = 0;
          
          $.each( clean_products, function(ind, prod) {
            if ( max_variant < Number(prod.variant) ) max_variant = Number(prod.variant);
          });
          
          product_obj.variant = (max_variant + 1) + "";
          product_obj.sifra = product_obj.item_sifra + "-" + product_obj.variant;
            
          save_new_product(product_obj, res);
            
        } else {
          save_new_product(product_obj, res);
        };
        
      }); // kraj find product sa istim item sifra     

      
    }; // kraj ako ima ili nema item sifra 
    
  }; // kraj od else ( NEMA _id ) 
    

};



exports.delete_product = function(req, res) {

  var product_id = req.body._id;

  Product.findOneAndDelete(
    { _id: product_id },
    { strict: false }, 
    function(err, deleted_product) {

      if (err) {
        console.log(`Error u bazi prilikom DELETE PRODUCTA`, err);
        res.json({success: false, error: err, msg: `Error u bazi prilikom DELETE PRODUCTA !!!` })
        return;
      };

      
      if ( deleted_product.kalk ) {
        // ------------------ ako product IMA kalk
        
        Kalk.findOneAndDelete(
        { product_id: product_id },
        { strict: false }, 
        function(err, deleted_kalk) {

          if (err) {
            console.log(`Error u bazi prilikom DELETE KALKULACIJE nakon DELETE PRODUCTA`, err);
            res.json({success: false, error: err, msg: `Error u bazi prilikom DELETE KALKULACIJE nakon DELETE PRODUCTA !!!` })
            return;
          };



          var clean_product = global.mng_to_js(deleted_product);
          res.json({ success: true, product: clean_product });

        }); // kraj kalk delete
        
      } 
      else {
        
        // ------------------ ako product NEMA kalk
        var clean_product = global.mng_to_js(deleted_product);
        res.json({ success: true, product: clean_product });
            
      };

    }); // kraj product delete
  

};



exports.get_product_new_variant = function(req, res) {

  var item_sifra = req.body.item_sifra;
  
  Product.find({ item_sifra: item_sifra })
  .exec(function(err, products) {

    if (err) {
      console.log(`Error response prilikom provjere item_sifra u PRODUCTU`, err);
      res.json({success: false, error: err, msg: `Greška prilikom item_sifra u PRODUCTU !!!` });
      return;
    };

    if ( products.length > 0 ) {

      var clean_products = global.mng_to_js_array(products);
      var max_variant = 0;

      $.each( clean_products, function(ind, prod) {
        if ( max_variant < Number(prod.variant) ) max_variant = Number(prod.variant);
      });

      var variant = (max_variant + 1) + "";
      
      res.json({ success: true, variant: variant });
      

    } else {
      
      res.json({success: false, msg: `Greška prilikom item_sifra u PRODUCTU !!!<br>Nije pronađen product u bazi s tom iem sifrom!!!` });
      
    };

  }); // kraj find product sa istim item sifra     


};





