var inspect = require('util').inspect;
var fs = require('fs');
var base64 = require('base64-stream');
var Imap = require('imap');
var path = require("path");

const simpleParser = require('mailparser').simpleParser;


/*

global.vel_email_address = 'app@velprom.hr';
global.vel_mail_setup = {
  
  host: "mail.velprom.hr",
  port: 587,
  secure: false, // use TLS,
  requireTLS: true,
  auth: {
    user: global.vel_email_address,
    pass: 'eukaliptus69'
  },
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false
  }
};

*/


var imap = new Imap({
  user: global.vel_email_address,
  password: global.vel_mail_setup.auth.pass,
  host: global.vel_mail_setup.host,
  port: 993,
  tls: true,
  tlsOptions: {
    rejectUnauthorized: false
  },
  //,debug: function(msg){console.log('imap:', msg);}
});



function findAttachmentParts(struct, attachments) {
  attachments = attachments || [];
  for (var i = 0, len = struct.length, r; i < len; ++i) {
    if (Array.isArray(struct[i])) {
      findAttachmentParts(struct[i], attachments);
    } else {
      if (struct[i].disposition && ['INLINE', 'ATTACHMENT'].indexOf(struct[i].disposition.type) > -1) {
        attachments.push(struct[i]);
      }
    }
  }
  return attachments;
}

function buildAttMessageFunction(attachment) {

  var filename = attachment.params.name;
  var encoding = attachment.encoding;

  return function (msg, seqno) {

    var prefix = '(#' + seqno + ') ';

    msg.on('body', function (stream, info) {

      //Create a write stream so that we can stream the attachment to file;
      console.log(prefix + 'Streaming this attachment to file', filename, info);

      var writeStream = fs.createWriteStream(filename);

      writeStream.on('finish', function () {
        console.log(prefix + 'Done writing to file %s', filename);
      });

      //stream.pipe(writeStream); this would write base64 data to the file.
      //so we decode during streaming using 
      if (encoding === 'BASE64') {
        //the stream is base64 encoded, so here the stream is decode on the fly and piped to the write stream (file)
        stream.pipe(base64.decode()).pipe(writeStream);

      } else {
        //here we have none or some other decoding streamed directly to the file which renders it useless probably
        stream.pipe(writeStream);
      };

    });

    msg.once('end', function () {
      console.log(prefix + 'Finished attachment %s', filename);
    });

  };
  
};

global.run_imap = function () {

  imap.once('ready', function () {

    imap.openBox('INBOX', true, function (err, box) {

      if (err) throw err;

      var last_mails = (box.messages.total > 20) ? 20 : box.messages.total - 1;
      var f = imap.seq.fetch((box.messages.total - last_mails) + ':' + box.messages.total, {
        bodies: '',
        /* 'HEADER.FIELDS (FROM TO SUBJECT DATE)', 'TEXT', */
      });

      f.on('message', function (msg, seqno) {

        console.log('Message #%d', seqno);
        var prefix = '(#' + seqno + ') ';

        // var all = _.find(msg.parts, { "which": "" });


        msg.on('body', function (stream, info) {


          simpleParser(stream /*all.body*/ , (mail_error, mail) => {


            if (mail_error) {
              console.error(`------greška u mail parseru`, mail_error);
              return;
            };

            console.log("------------------------simple parser --------------------------");
            console.log(mail.subject);
            console.log(mail.html);
            console.log(mail.attachments);

            // mail will have everything, create meaningful data from it.
            const mail_fileName = `msg-${seqno}-body.txt`;


            // const fullFilePath = path.join('<path to store>', dir, mail_fileName);
            const emailEnvolope = {};
            
            emailEnvolope.from = mail.from.text;
            emailEnvolope.date = mail.date;
            emailEnvolope.to = mail.to.text;
            emailEnvolope.subject = mail.subject;
            emailEnvolope.text = mail.text;
            emailEnvolope.textAsHtml = mail.textAsHtml;
            
            emailEnvolope.html = mail.html ? mail.html : null;
            
            emailEnvolope.attachments = [];

            // write attachments
            for (let i = 0; i < mail.attachments.length; i += 1) {

              const attachment = mail.attachments[i];
              const { filename } = attachment;
              emailEnvolope.attachments.push(filename);
              fs.writeFile(global.appRoot + '/public/emails/' + filename, attachment.content, err => {
                if (err) {
                  console.error(err)
                  return
                };
                console.log(filename + " je spremljen");
                //file written successfully
              });
            }

            // const contents = JSON.stringify(emailEnvolope);
            // fs.writeFileSync(fullFilePath, contents);
            
            console.log('processing mail done....');
            
            
          }); // kraj simple parser

        }); // kraj on message
        
        msg.on('attributes', function(attrs) {
          console.log( `redni broj poruke: `, seqno );
          console.log('uid = ' + attrs.uid);
        });
        
        f.once('error', function (err) {
          console.log('Fetch error: ' + err);
        });

        f.once('end', function () {
          console.log('Done fetching all messages!');
          imap.end();
        });


      });  // kraj on message

    }); // kraj open box 

  }); // kraj imap ready

  imap.once('error', function (err) {
    console.log(err);
  });

  imap.once('end', function () {
    console.log('Connection ended');
  });

  imap.connect();

};