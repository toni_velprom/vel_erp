var mongoose = require('mongoose');

// mongoose.set('useFindAndModify', false);

var User = mongoose.model('User');
var Counter = mongoose.model('Counter');
var Grupacija = mongoose.model('Grupacija');
var Partner = mongoose.model('Partner');
var nodemailer = require('nodemailer');
var multer = require('multer')

var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);


var find_controller = require('./find_controller.js');

var partners_for_db = require('./new_partners.js');

exports.upload_docs = function(req, res) {
  
  global.upload_docs(req, res);
  
};

exports.find_partner = function(req, res) {
  
  global.cit_search_remote(req, res, Partner)
  .then(function(result) {
    res.json(result) 
  })
  .catch(function(error) {
    res.json(error) 
  });
  
};



exports.find_docs = function(req, res) {
  
  global.cit_search_remote(req, res, Partner)
  .then(function(result) {
    res.json(result) 
  })
  .catch(function(error) {
    res.json(error) 
  });
  
};




exports.find_grupacija = function(req, res) {

  global.cit_search_remote(req, res, Grupacija)
  .then(function(result) {
    res.json(result) 
  })
  .catch(function(error) {
    res.json(error) 
  });
  
};

exports.edit_grupacija_name = function(req, res) {
  
  var new_name = req.body.new_name;
  var id = req.body.id;

  Grupacija.findOneAndUpdate(
    { _id: id },
    { $set: { name: new_name } },
    { new: true }, function(err, grupacija) {

      
      if (err) {
        console.log(`Error response prilikom promjene imena grupacije`, err);
        res.json({success: false, error: err, msg: `Greška prilikom promjene imena grupacije !!!` })
        return;
      };
      
      var clean_grupacija = global.mng_to_js(grupacija);
      
      res.json({success: true, grupacija: clean_grupacija });
      
      Partner.updateMany( { "grupacija": id }, { grupacija_flat: JSON.stringify(clean_grupacija)  }, function(err, res) {
        if (err) { 
         console.log("Greška kod update grupacije na svakoj kompaniji", err);
        } else { 
          console.log("Promjenio sam grupacija_flat na svakoj kompaniji");
        };
      });      
      
    });
  
};


exports.new_grupacija = function(req, res) {

  var name = req.body.name;
  var company_id = req.body.company_id;
  var company_naziv = req.body.company_naziv;
  var old_grupacija_id = req.body.old_grupacija_id;
  

  var company_obj = { _id : company_id, name : company_naziv };
  
  var new_grupacija_obj = {
      name: name,
      companies: [ company_obj ],
      companies_flat: JSON.stringify( [ company_obj ] )
  };
  
  
  var new_grupacija = new Grupacija(new_grupacija_obj);
  // save to DB
  new_grupacija.save(function(err, grupacija) {

    if (err) {
      res.json({success: false, error: err });
      return;
    };
    
    var clean_grupacija = global.mng_to_js(grupacija);
    
    res.json({success: true, grupacija: clean_grupacija });

    if ( old_grupacija_id ) {

      // obriši ovu kompaniju u staroj grupaciji
      Grupacija.findOneAndUpdate(
        { _id: old_grupacija_id },
        { $pull: { companies: { _id: company_id } } },
        { new: true }, function(err, old_grupacija) {

          if (err) {
            console.log(`Error prilikom brisanja companies u STAROJ kompaniji`, err);
            return;
          }; 
          console.log(`Obrisao sam kompaniju iz STARE grupacije: ` + mng_to_js(grupacija)._id );

          var clean_old_grupacija = mng_to_js(old_grupacija);
        
          Partner.updateMany( { "grupacija": clean_old_grupacija._id }, { grupacija_flat: JSON.stringify(clean_old_grupacija) }, function(err, res) {
            if (err) { 
             console.log("Greška kod update grupacija_flat na svakoj kompaniji KOJE IMAJU JOŠ IMAJU STARU GRUPACIJU", err);
            } else { 
              console.log("promjenio sam grupacija_flat na svakoj kompaniji");
            };
          }); 

        }); // kraj  brisanja kompanije unutar grupacije od partnera

    };
    

  }); // kraj save new grupa

};


exports.remove_from_grupacija = function(req, res) {
  
  var partner_id = req.body.partner_id;
  var id = req.body.id;

  Grupacija.findOne(
    { _id: id },
    function(err, grupacija) {

      
      if (err) {
        console.log(`Error response prilikom pretrage za grupaciju`, err);
        res.json({success: false, error: err, msg: `Greška prilikom pretrage grupacije !!!` })
        return;
      };
      
      var clean_grupacija = global.mng_to_js(grupacija);

      clean_grupacija.companies = global.delete_item(clean_grupacija.companies, partner_id, `_id` );
      
      
      Grupacija.findOneAndUpdate(
        { "_id": id },
        clean_grupacija,
        function(err, updated_grupacija) {
        if (err) { 
         console.log("Greška kod update grupacije nakon izbacivanja tvrtke", err);
        } else { 
          
          Partner.updateMany(
            { "grupacija": id },
            { grupacija_flat: JSON.stringify(clean_grupacija) },
            function(err, updated_partners) {
            if (err) { 
             console.log("Greška kod update grupacije na svakom partneru nakon izbacivanja jednog od partnera is grupacije", err);
            } else { 
              console.log("Uspješno sam izbacio partnera iz grupacije !!!!!");
              res.json({success: true, grupacija: clean_grupacija });
            };
              
          }); // napravio update svih partnera
          
        };
          
      }); // napravio update grupacije
      
            
      
    });
  
};


exports.update_grupacija = function(req, res) {
  
  var name = req.body.name;
  var id = req.body._id;
  var companies = req.body.companies;
  var companies_flat = req.body.companies_flat;
  
  
  var old_grupacija_id =  req.body.old_grupacija_id;
  var company_id = req.body.company_id;
  
  // obriši ova dva propertija jer mi ne trabaju kod save grupa
  delete req.body.old_grupacija_id;
  delete req.body.company_id;
  

  // UPDATE GRUPACIJA
  Grupacija.findOneAndUpdate(
    { _id: id },
    
    /* doslovno radim update svih properija u grupaciji */
    req.body,
    
    
    { new: true }, function(err, grupacija) {
      
      var clean_grupacija = null;
      
      if (err) {
        console.log(`Error response prilikom promjene UPDATE grupacije`, err);
        res.json({success: false, error: err, msg: `Greška prilikom UPDATE grupacije !!!` })
      } else {
        clean_grupacija = global.mng_to_js(grupacija);
        res.json({success: true, grupacija: clean_grupacija })
      };


      Partner.updateMany( { "grupacija": clean_grupacija._id }, { grupacija_flat: JSON.stringify(clean_grupacija) }, function(err, res) {
        if (err) { 
         console.log("Greška kod update grupacija_flat na svakoj kompaniji", err);
        } else { 
          console.log("promjenio sam grupacija_flat na svakoj kompaniji");
        };
      });       
      
      
      if ( old_grupacija_id ) {
        
        // obriši ovu kompaniju u staroj grupaciji
        Grupacija.findOneAndUpdate(
          { _id: old_grupacija_id },
          { $pull: { companies: { _id: company_id } } },
          { new: true }, function(err, old_grupacija) {
            
            if (err) {
              console.log(`Error prilikom brisanja companies u STAROJ kompaniji`, err);
              return;
            }; 
            console.log(`Obrisao sam kompaniju iz STARE grupacije: ` + mng_to_js(grupacija)._id );
            
            var clean_old_grupacija = mng_to_js(old_grupacija);
            
            Partner.updateMany( { "grupacija": clean_old_grupacija._id }, { grupacija_flat: JSON.stringify(clean_old_grupacija) }, function(err, res) {
              if (err) { 
               console.log("Greška kod update grupacija_flat na svakoj kompaniji KOJE IMAJU JOŠ IMAJU STARU GRUPACIJU", err);
              } else { 
                console.log("promjenio sam grupacija_flat na svakoj kompaniji");
              };
            }); 
                        
          }); // kraj  brisanja kompanije unutar grupacije od partnera
        
      };
      

    });
  
};


exports.save_partner = function(req, res) {

  var partner_obj = req.body;
  
  partner_obj.grupacija_flat = partner_obj.grupacija ? JSON.stringify(partner_obj.grupacija) : null;
  // spremi samo id od grupacije jer za to radim POPULATE pa mi ne treba cijeli objekt
  partner_obj.grupacija = partner_obj.grupacija ? partner_obj.grupacija._id: null; 
  
  if ( partner_obj._id ) {
    
    Partner.findOne({ _id: partner_obj._id }, function(err, partner) {

      if (err) {
        res.json({ success: false, msg: 'Greška kod FIND ONE Partner prilikom save partner!', err: err });
        return;
      };

      if (!partner) {
        // if no user
        res.json({ success: false, msg: 'Ažuriranje partner nije uspjelo.<br>Sustav nije pronašao partnera u bazi!' });
      } 
      else {

        
        var DB_clean_partner = global.mng_to_js(partner);
        var DB_statuses = DB_clean_partner.statuses || [];
        var DB_docs = DB_clean_partner.docs || [];
        
        DB_statuses = global.cit_deep(DB_statuses);
        DB_docs = global.cit_deep(DB_docs);
        
        
        // prepiši sve propertije u partner objektu
        var clean_partner = {...DB_clean_partner, ...partner_obj };
        
        
        // ------------------------------ UBACUJEM NOVE DOCSE AKO POSTOJE U REQUESTU
        
        if ( !DB_docs ) DB_docs = [];
        
        if ( partner_obj.docs && partner_obj.docs.length > 0 ) {
          $.each( partner_obj.docs, function(d_ind, req_doc) {
            DB_docs = global.upsert_item( DB_docs, req_doc, `sifra`); 
          });
          
        };
        
        clean_partner.docs = DB_docs;
        
        // ------------------------------ UBACUJEM NOVE DOCSE AKO POSTOJE U REQUESTU
        
        
        // ------------------------------ UBACUJEM NOVE STATUSES AKO POSTOJE U REQUESTU
        if ( !DB_statuses ) DB_statuses = [];
        
        if ( partner_obj.statuses && partner_obj.statuses.length > 0 ) {
          $.each( partner_obj.statuses, function(s_ind, req_status) {
            if ( req_status ) DB_statuses = global.upsert_item( DB_statuses, req_status, `_id`); 
          });
        };
        
        clean_partner.statuses = DB_statuses;
        
        // ------------------------------ UBACUJEM NOVE STATUSES AKO POSTOJE U REQUESTU
        
        
        
        Partner.findOneAndUpdate(
        { _id: clean_partner._id },
        clean_partner,
        { new: true }, function(err, partner) {

          if (err) {
            console.log(`Error response prilikom UPDATE partnera`, err);
            res.json({success: false, error: err, msg: `Greška prilikom UPDATE partnera !!!` })
            return;
          };

          var clean_partner = global.mng_to_js(partner);
          res.json({success: true, partner: clean_partner });

        }); 
        
        
      }; // kraj ifa ako je našao partnera po idiju

    }); // end of User.findOne
    
    // --------------------------------------------- kraj ako je UPDATE
    
  } else {
    
    
    Counter.findOneAndUpdate({ partners: { $ne: null } }, { $inc: { partners: 1 } }, { "new": true })
    .exec(function(err, counter) {
      if (err) {
          res.json({ 
            success: false, 
            msg: "Partner nije spremljen! Counter problem.",
            user_number: null
          });
        return;
      };  
      
      var clean_count = mng_to_js(counter);

      var new_partner_sifra = "PAR"+clean_count.partners + ".";
      partner_obj.partner_sifra = new_partner_sifra;
      
      // --------------------------------------------------------------------------
      var new_partner = new Partner(partner_obj);
      // save to DB
      new_partner.save(function(err, partner) {
        if (err) {
          console.log(`Error response prilikom SAVE partnera`, err);
          res.json({success: false, error: err });
          return;
        };
        var clean_partner = global.mng_to_js(partner);
        res.json({success: true, partner: clean_partner });

      }); // kraj save new partner
      
    // --------------------------------------------------------------------------  

    }); // kraj countera
    
    
  }; // --------------------------------------- kraj ako je SAVE 
    

}; // KRAJ SAVE PARTNER


 
global.create_multiple_partners = function(par_index, par_object) {

    $.each( par_object, function( key, value ) {
      // ako je objekt ili array onda ga flataj
      if ( $.isPlainObject(value) || $.isArray(value) ) {
        par_object[key+'_flat'] = JSON.stringify(value);
      };
    });
  
  
    var new_par = new Partner(par_object);
    // save to DB
    new_par.save(function(err, user) {

      if (err) {
        console.error(err);
      };

      // create a token for new user !!!!

      var par_copy = global.mng_to_js(user);

      console.log(`PARTNER ` + par_copy.naziv + ` je spremljen. Ukupno: ` + (par_index+1) + `/` + global.partners_for_db.length );


      if ( (par_index+1) < global.partners_for_db.length ) {
        global.create_multiple_partners( par_index+1, global.partners_for_db[par_index+1] );
      } else {
        console.log(`SPREMIO SVE BULK PARTNERE !!!!!`);            
      };

    }); // end of user save  



}; // end of create_multiple_partners




global.run_update_partners = function(clean_partners_arr, data_index) {

var new_sifra = clean_partners_arr[data_index].partner_sifra + ".";

Partner.findOneAndUpdate(
  { _id: clean_partners_arr[data_index]._id },
  { partner_sifra: new_sifra },
  { new: true, upsert: true }, function(err, new_partner) {

    if (err) {
      console.log(`Error kod upisivanja sifre s točkom za  ${ clean_partners_arr[data_index].full_naziv } `, err);
    };

    console.log(`PARTNER ` + clean_partners_arr[data_index].naziv + ` je updated. Ukupno: ` + (data_index+1) + `/` + clean_partners_arr.length );

    if ( (data_index+1) < clean_partners_arr.length ) {
      global.run_update_partners( clean_partners_arr, data_index+1 );
    } else {
      console.log(`NAPRAVIO UPDATE NA SVIM PARNERIMA !!!!!`);            
    };      

  });



};


global.update_multiple_partners = function(update_data, data_index) {

/*
  .select({
    "_id": 1,
    "kvaliteta_1": 1,
    "kvaliteta_2": 1,
    full_naziv: 1
  }) 
*/

Partner.find({})
.exec(function(err, partners) {

  if (err) {

    console.log(err);

  } else {

    var clean_partners_arr = global.mng_to_js_array(partners);

    global.run_update_partners(clean_partners_arr, data_index);

  };



});



}; // end of update_multiple_partners


exports.edit_partner_doc = function(req, res) {
  
  var data_for_doc = req.body.doc;

    /*
    ------------------------------------------------------------------------------------------------
    trebam zapisati doc u samog partnera 
    to može biti ponuda ili NK
    ------------------------------------------------------------------------------------------------
    */

  
  Partner.findOneAndUpdate(
    { _id: data_for_doc.partner_id, "docs.sifra" : data_for_doc.sifra }, 
    { $set: { "docs.$": data_for_doc } },
    { new: true }, function(update_err, updated_partner) {

    if (update_err) {
      res.json({ success: false, msg: 'Greška kod EDIT doc u PARTNERU ' + updated_partner.partner_sifra, error: update_err, doc: data_for_doc });
      return;
    };

    var clean_updated_partner = global.mng_to_js(updated_partner);

    resolve({ success: true, doc: data_for_doc });  

  });    // kraj update docs in partner


};






