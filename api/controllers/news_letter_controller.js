var nodemailer = require('nodemailer');

global.vel_email_address = 'app@velprom.hr';

global.vel_mail_setup = {
  host: "mail.velprom.hr",
  port: 587,
  secure: false, // use TLS,
  requireTLS: true,
  auth: {
    user: global.vel_email_address,
    pass: 'eukaliptus69'
  },
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false
  }
};




global.nl_data = [

"ivona.davanzo@velprom.hr",
"nikolina.japec@velprom.hr",
"jernej.urbancic@velprom.hr",
"josip.jurkovic@velprom.hr",
"velprom.josipkovacic@gmail.com",
"alen.mahac@velprom.hr",
"maja.zupetic@velprom.hr",
"martina.gavran@velprom.hr",
"aleksandra.martinovic@velprom.hr",
"josip.milic@velprom.hr",
"nancy.knezevic@velprom.hr",
"info@velprom.hr",
"ivana@velprom.hr",
"sanja.lukacic@velprom.hr",
"snjezana.urosevic@velprom.hr",
"martina@velprom.hr",
"renato.sipus@velprom.hr",
"toni.kutlic@velprom.hr",
"zoran@velprom.hr",
"bruno.zgela@velprom.hr",
  
];


function nl_html( user) {
  
  var html = 

`
  
<div style="height: auto;
            text-align: center;
            font-family: Arial, Helvetica, sans-serif;
            padding: 0;
            text-size-adjust: none;
            -webkit-text-size-adjust: none;
            -moz-text-size-adjust: none;
            -ms-text-size-adjust: none;">

    <div style="  height: 700px;
                  background-image: url(https://help.velprom.hr/img/nl/new_users_interna.png);
                  background-repeat: no-repeat;
                  background-position: right top;
                  background-size: auto 740px;
                  background-color: #fff;
                  text-align: center;
                  font-family: Arial, Helvetica, sans-serif;
                  padding: 30px 3px 0;">



      <div style="    
        font-size: 13px;
        margin-top: 40px;
        margin-bottom: 20px;
        color: #06253f;
        font-weight: 400;
        text-align: left;
        line-height: 26px;
        max-width: 400px;
        clear: both;
        width: auto;
        margin: 0 auto;
        padding: 10px;
        height: auto;
        background-color: #fff;
        border-radius: 10px;
        border: 1px solid #e8e6e6;
        letter-spacing: 0.04rem;
        text-align: center;">


 
      <div style=" font-size: 36px;
                    margin-top: 20px;
                    margin-bottom: 20px;
                    color: #06253f;
                    line-height: 22px;
                    letter-spacing: normal;
                    font-weight: 700;">

        Online brošura <br>
      </div>
      


<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div> 

<br style="display: block; clear: both;">


<span style="font-size: 16px;">${user.full_name}</span>
<br>

<span style="font-size: 16px;">Tvoje korisničko ime za internu brošuru je:</span>
<br>

<div style="font-size: 20px;">${user.name}</div>
<br>
<span style="font-size: 16px;">Tvoja lozinka je:</span>
<br>

<div style="font-size: 20px;">${user.pass}</div>
<br>
<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div>
<br>

Ovo je prva verzija web brošure. Naravno, još nije sve gotovo i može biti puno bolje! Molim te da sve greške ili prijedloge zapišeš na Slack kanal: <br>
<a href="https://velprom.slack.com/archives/C026749DDQQ">#o-software-u-i-internoj-brošuri</a>
<br>
<br>
<span style="font-size: 16px;">Web brošuri možeš pristupiti:</span>

  <a style="  background-color: #103152;
              border-radius: 8px;
              height: 60px;
              width: 200px;
              font-size: 14px;
              margin: 20px auto 0;
              text-align: center;
              display: block;
              min-width: 200px;
              color: #fff;
              text-decoration: none;
              line-height: 60px;"
     
     target="_blank"
     
     href="https://help.velprom.hr/">
    
    OVDJE
  </a>


<br>

<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div>

<br style="display: block; clear: both;">
</div>

      <br style="display: block; clear: both;">
      
    </div>


    <div style="height: auto;
                text-align: center;
                font-family: Arial, Helvetica, sans-serif;
                background-color: transparent;
                padding: 0;">

      
      


      <div style="
        font-size: 11px;
        margin-top: 0;
        color: #727272;
        font-weight: 400;
        float: left;
        width: 100%;
        line-height: 18px;
        border-top: 1px solid #eaeaea;
        padding-top: 10px;
        background-color: #fff;
        letter-spacing: 0.06rem;">

        <img style="
          width: 200px;
          height: auto;
          max-width: 200px;
          border-radius: 0;
          margin-top: 100px;
          display: block;
          float: none;
          clear: both;
          margin: 20px auto 0;"
          alt="Velprom Logo" 
          src="https://help.velprom.hr/img/velprom_logo_email.png" />

        
        
<br style="display: block; clear: both;">

<b style="font-size: 16px; font-weight: 700;">Velprom d.o.o.</b><br>
<b>OFFICE:</b>&nbsp;Poštanska 7, 10410 Velika Gorica<br>
<b>OIB:</b>&nbsp;33428207264, <b>MB:</b>&nbsp;3528090<br>
<b>TEL:</b>&nbsp;+385 1 6215 541<br>
<b>FAX:</b>&nbsp;+385 1 2099 483<br>
<b>MAIL:</b>&nbsp;info@velprom.hr<br>
<b>WEB:</b>&nbsp;www.velprom.hr<br>
<br>
<div style="font-size: 10px; border-top: 1px solid #e8e6e6; padding-top: 20px; max-width: 350px; margin: 0 auto;">
  Tvtka je registrirana na Trgovačkom sudu u Zagrebu sa sjedištem: Dalmatinska 27, Gradići; 10410 Velika Gorica, Hrvatska.&nbsp; 
  IBAN : HR7225000091101346755 Addiko Bank d.d., Iznos temeljnog kapitala: 20.000,00
</div>
<br style="display: block; clear: both;">
        
        
        
      </div>        
      
      <br style="display: block; clear: both;">
    </div>  

    
</div>     
  
`;  
  
  
  
  
  
var html = `

  
<div style="height: auto;
            text-align: center;
            font-family: Arial, Helvetica, sans-serif;
            padding: 0;
            text-size-adjust: none;
            -webkit-text-size-adjust: none;
            -moz-text-size-adjust: none;
            -ms-text-size-adjust: none;">

    <div style="  height: auto;
                  background-image: url(https://help.velprom.hr/img/nl/bg_party_2023.png);
                  background-repeat: no-repeat;
                  background-position: right top;
                  background-size: auto 1200px;
                  background-color: #fff;
                  text-align: center;
                  font-family: Arial, Helvetica, sans-serif;
                  padding: 30px 3px 0;">



      <div style="    
        font-size: 13px;
        margin-top: 40px;
        margin-bottom: 20px;
        color: #06253f;
        font-weight: 400;
        text-align: left;
        line-height: 26px;
        max-width: 1000px;
        clear: both;
        width: auto;
        margin: 0 auto;
        padding: 10px;
        height: auto;
        background-color: transparent;
        border-radius: 10px;
        border: none;
        letter-spacing: 0.04rem;
        text-align: center;">


        <img style="
          width: 100%;
          border-radius: 0;

          display: block;
          float: none;
          clear: both;
          margin: 0 auto;"
          alt="Velprom PARTY POZIVNICA ------- VELPROM 
Vas poziva na domjenak
28.01.2023., subota u 18:00
DVD Gradići, Gradićka ulica 33 ------- DOLAZAK POTVRDITI ANTI GUDELJU 
USMENO ILI NA 099 766 9321
NAJKASNIJE DO PONEDJELJKA, 09.01.2023.
" 
          src="https://help.velprom.hr/img/nl/nl_party_2023.png" />


<br>
<br>
<br>
<br>
<br>
<br>


 
      <div style=" font-size: 36px;
                    margin-top: 20px;
                    margin-bottom: 20px;
                    color: #06253f;
                    line-height: 22px;
                    letter-spacing: normal;
                    font-weight: 700;">

        VELPROM PARTY <br>
      </div>
      


<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div> 

<br style="display: block; clear: both;">


<span style="font-size: 20px; font-weight: 700:">VELPROM</span>
<br>

<span style="font-size: 16px;"> 
Vas poziva na domjenak <br>
28.01.2023., subota u 18:00 <br>
DVD Gradići, Gradićka ulica 33</span>
<br>
<br>
<div style="font-size: 16px;">Očekuje se da na domjenak dođu svi pozvani</div>
<br>
<span style="font-size: 16px; font-weight: 700;">
  DOLAZAK POTVRDITI ANTI GUDELJU <br>
  USMENO ILI NA 099 766 9321 <br>
  NAJKASNIJE DO PONEDJELJKA, 09.01.2023. <br>
</span>
<br>

<br>

<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div>

<br style="display: block; clear: both;">
</div>

      <br style="display: block; clear: both;">
      
    </div>


    <div style="height: auto;
                text-align: center;
                font-family: Arial, Helvetica, sans-serif;
                background-color: transparent;
                padding: 0;">

      
      


      <div style="
        font-size: 11px;
        margin-top: 0;
        color: #727272;
        font-weight: 400;
        float: left;
        width: 100%;
        line-height: 18px;
        border-top: 1px solid #eaeaea;
        padding-top: 10px;
        background-color: #fff;
        letter-spacing: 0.06rem;">

        <img style="
          width: 200px;
          height: auto;
          max-width: 200px;
          border-radius: 0;
          margin-top: 100px;
          display: block;
          float: none;
          clear: both;
          margin: 20px auto 0;"
          alt="Velprom Logo" 
          src="https://help.velprom.hr/img/velprom_logo_email.png" />

        
        
<br style="display: block; clear: both;">

<b style="font-size: 16px; font-weight: 700;">Velprom d.o.o.</b><br>
<b>OFFICE:</b>&nbsp;Poštanska 7, 10410 Velika Gorica<br>
<b>OIB:</b>&nbsp;33428207264, <b>MB:</b>&nbsp;3528090<br>
<b>TEL:</b>&nbsp;+385 1 6215 541<br>
<b>FAX:</b>&nbsp;+385 1 2099 483<br>
<b>MAIL:</b>&nbsp;info@velprom.hr<br>
<b>WEB:</b>&nbsp;www.velprom.hr<br>
<br>
<div style="font-size: 10px; border-top: 1px solid #e8e6e6; padding-top: 20px; max-width: 350px; margin: 0 auto;">
  Tvtka je registrirana na Trgovačkom sudu u Zagrebu sa sjedištem: Dalmatinska 27, Gradići; 10410 Velika Gorica, Hrvatska.&nbsp; 
  IBAN : HR7225000091101346755 Addiko Bank d.d., Iznos temeljnog kapitala: 20.000,00
</div>
<br style="display: block; clear: both;">
        
        
        
      </div>        
      
      <br style="display: block; clear: both;">
    </div>  

    
</div>     
  




`;  
  
  
  
  
  return html;
};



var min_sleep = 500;
var max_sleep = 3000;

global.send_nl = function(data_index) {

  return new Promise( function( resolve, reject ) { 

    // Math.floor(Math.random() * (max - min + 1)) + min;
    var sleep = Math.floor(Math.random() * (max_sleep - min_sleep + 1)) + min_sleep;
    
    setTimeout( function() { 
    
      var transporter = nodemailer.createTransport(global.vel_mail_setup);

      var mail_subject = 'VELPROM PAAAAARTY 28.01.2023. !!!';


      var html = nl_html(global.nl_data[data_index]);

      // setup email data
      var mailOptions = {
          from: `"Velprom App" <${global.vel_email_address}>`, // sender address
          to: global.nl_data[data_index], // .email, // list of receivers
          subject: mail_subject, // Subject line
          html:  html // html body
      };

        // send mail with defined transport object
      transporter.sendMail(mailOptions, (error, info) => {

        if (error) {

          var error_obj = { 
            success: false, 
            mail: global.nl_data[data_index],
            msg: "User nije spremljen!<br>Problem u slanju maila za konfirmaciju !!!",
            error: error
          };
          
          console.error(error_obj);

          reject(error_obj);

        };

        console.log(
          ` Poslan NL mail prema: ` + global.nl_data[data_index].email + `, ukupno: ` + (data_index+1) + `/` + global.nl_data.length 
        );

        resolve(true);
      }); // end of send welcome email

    }, sleep); // kraj timeout

  }); // kraj promisa

};
  

global.send_multi_nl = function(data_index) {
  
  global.send_nl(data_index)
  .then( function(response) {
    
      if ( (data_index+1) < global.nl_data.length ) {
        global.send_multi_nl( data_index+1 );
      } else {
        console.log(`Poslao sam sve NEWSLETTERE !!!!!`);            
      };
    
  })
  .catch(function(eror) {
    
    console.log( `catch je trigeriran u send multi nl` );
    
    if ( (data_index+1) < global.nl_data.length ) {
      global.send_multi_nl( data_index+1 );
    } else {
      console.log(`Poslao sam sve NEWSLETTERE !!!!!`);            
    };
    
  });
  
};