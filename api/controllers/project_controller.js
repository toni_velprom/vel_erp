var mongoose = require('mongoose');

// mongoose.set('useFindAndModify', false);

var User = mongoose.model('User');
var Counter = mongoose.model('Counter');
var Grupa = mongoose.model('Grupa');

var Project = mongoose.model('Project');
var Product = mongoose.model('Product');
var Status = mongoose.model('Status');

var Partner = mongoose.model('Partner');



var nodemailer = require('nodemailer');
var multer = require('multer')

var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);


// var id = mongoose.Types.ObjectId('4edd40c86762e0fb12000003');
// var _id = mongoose.mongo.BSONPure.ObjectID.fromHexString("4eb6e7e7e9b7f4194e000001");

var find_controller = require('./find_controller.js');


exports.find_proj = function(req, res) {

  global.cit_search_remote(req, res, Project)
  .then(function(result) {
    res.json(result) 
  })
  .catch(function(error) {
    res.json(error) 
  });
  
};


exports.save_proj = function(req, res) {

  var proj_obj = req.body;

  // spremi samo id od KUPCA jer to radim POPULATE pa mi ne treba cijeli objekt
  proj_obj.kupac_flat = proj_obj.kupac ? JSON.stringify(proj_obj.kupac) : null;
  // proj_obj.kupac = proj_obj.kupac ? proj_obj.kupac._id : null; 
  
  
  
  if ( proj_obj._id ) {
  
    Project.findOneAndUpdate(
      { _id: proj_obj._id },
      proj_obj,
      { new: true }, function(err, proj) {

        if (err) {
          console.log(`Error response prilikom UPDATE PROJEKTA`, err);
          res.json({success: false, error: err, msg: `Greška prilikom UPDATE PROJEKTA !!!` })
          return;
        };
        
        var clean_proj = global.mng_to_js(proj);
        res.json({success: true, proj: clean_proj });

      });    

    // kraj ako je UPDATE
    
  } else {
    
    
    Counter.findOneAndUpdate({ projects: { $ne: null } }, { $inc: { projects: 1 } }, { "new": true })
    .exec(function(err, counter) {
      
      if (err) {
          res.json({ 
            success: false, 
            msg: "Project nije spremljen! Counter problem.",
            proj_sifra: null
          });
        return;
      };  
      
      var clean_count = mng_to_js(counter);
      
      var new_proj_sifra = ""+clean_count.projects;
      proj_obj.proj_sifra = new_proj_sifra;
      
      // --------------------------------------------------------------------------
      var new_proj = new Project(proj_obj);
      // save to DB
      new_proj.save(function(err, proj) {
        if (err) {
          console.log(`Error response prilikom SAVE PROJEKTA`, err);
          res.json({success: false, error: err });
          return;
        };
        var clean_proj = global.mng_to_js(proj);
        res.json({success: true, proj: clean_proj });

      }); // kraj save new proj
      
    // --------------------------------------------------------------------------  

    }); // kraj countera
    
    
  }; // kraj ako je SAVE 
    

};



exports.get_status_kupac = function(req, res) {
  
  var proj_sifra = req.body.proj_sifra;
  
  Project.findOne({proj_sifra : proj_sifra })
  .exec(function(err, project) {

    if (err) {

      console.log(err);
      res.json({success: false, msg: "Došlo je do greške na serveru kod pretrage partnera/kupca za ovaj status", error: err});

    } else {

      var clean_project = global.mng_to_js(project);
      
      
      Partner.findOne({ _id: clean_project.kupac })
      .exec(function(err, partner) {

        if (err) {

          console.log(err);
          res.json({success: false, msg: "Došlo je do greške na serveru kod pretrage partnera/kupca za ovaj status", error: err});

        } else {

          var clean_partner = global.mng_to_js(partner);
          res.json({success: true, partner: partner })

        };


      }); // find partner kraj
      

    }; // ako je nasao project po proj sifri



  });
  
  

  
};



global.run_update_projects = function(clean_project_arr, data_index) {


  $.each( clean_project_arr[data_index], function( key, value ) {
    // ako je objekt ili array onda ga flataj
    if ( $.isPlainObject(value) || $.isArray(value) ) {
      clean_project_arr[data_index][key+'_flat'] = JSON.stringify(value);
    };
  });

  
  
  Project.findOneAndUpdate(
    { _id: clean_project_arr[data_index]._id },
    clean_project_arr[data_index],
    { new: true, upsert: true }, function(err, new_project) {

      if (err) {
        console.log(`Error kod UPDATE project za  ${ clean_project_arr[data_index].naziv } `, err);
      };

      console.log(`Project ` + clean_project_arr[data_index].naziv + ` je updated. Ukupno: ` + (data_index+1) + `/` + clean_project_arr.length );

      if ( (data_index+1) < clean_project_arr.length ) {
        global.run_update_projects( clean_project_arr, data_index+1 );
      } else {
        console.log(`NAPRAVIO UPDATE NA SVIM PROJEKTIMA !!!!!`);            
      };      

    });

  
  
};


global.update_multiple_projects = function(update_data, data_index) {


  /*
  
    .select({
      "_id": 1,
      "kvaliteta_1": 1,
      "kvaliteta_2": 1,
      full_naziv: 1
    }) 
  
  
  */


  Project.find({})
  .exec(function(err, projects) {

    if (err) {

      console.log(err);

    } else {

      var clean_project_arr = global.mng_to_js_array(projects);

      global.run_update_projects(clean_project_arr, data_index);

    };



  });


  
  


}; // end of update_multiple_projects









