var mongoose = require('mongoose');

// mongoose.set('useFindAndModify', false);

var User = mongoose.model('User');
var Counter = mongoose.model('Counter');

var jwt = require('jsonwebtoken');

var scramblePass = require('password-hash-and-salt');

var nodemailer = require('nodemailer');

var email_body = require('./email_body');

var confirm_pass = require('./reset_pass_body');


var multer = require('multer');


var avatar_storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, global.appRoot + '/public/img/avatars')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + `.png`)
  }
});


var docs_storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, global.appRoot + '/public/docs')
  },
  filename: function (req, file, cb) {
    // cb(null, file.originalname + '-' + Date.now() + `.png`)
    cb(null, file.originalname)
  }
});

 


var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);



var find_controller = require('./find_controller.js');


var axios = require('axios').default;
axios.defaults.adapter = require('axios/lib/adapters/http');




exports.authenticate_a_user = function(req, res) {
  
  var cit_pass_timeout = 720;
  
  var pass = req.body.password;
  
  var user_query = { name: req.body.name };
  
  var card_num = null;
  
  if ( req.body.card_num ) {
    card_num = req.body.card_num;
    user_query = { card_num: card_num };
  };
  
  // find the user
  User.findOne(
    user_query,
    function(err, user) {
    
    if (err) throw err;
    
    if (!user) {
      // if no user
      res.json({ success: false, msg: 'Potvrda korisnika nije uspjela.<br>Nema tog korisnika u bazi :(' });
    } 
    else if (user) {
      
      if ( user.email_confirmed !== true ) {
        res.json({ success: false, msg: 'Potvrda korisnika nije uspjela.<br>Nije potvrđena email adresa!' });
        return;
      };
      
      
      // if user is found verify a hash 
      scramblePass(pass).verifyAgainst(user.hash, function(error, verified) {
        
        if (error) {
          throw new Error('Something went wrong with hash checking!');
          return;
        };
        
        
        
        if ( !verified && card_num == null ) {
          // if pass is wrong !!!!!!
          // I SAMO AKO NEMA KARTICE !!!!!
          res.json({ success: false, msg: 'Potvrda korisnika nije uspjela.<br>Kriva lozinka!' });
          return;
          
        } else {
          
          if ( user.deleted === true ) {
            res.json({ success: false, msg: 'Ovaj korisnik više nije aktivan' });
            return;
          };
          
          
          var user_copy = global.mng_to_js(user);
          
          // if user is found and password is right
          // create a token
          
          var sign_obj = {
            _id: user_copy._id,
            hash: user_copy.hash
          };
          
          var token = jwt.sign(sign_obj, 'super_tajna', {
            expiresIn : 60 * cit_pass_timeout // expires in 720 min
          });
          
          
          user_copy.user_id = user._id;
          user_copy.success = true;
          user_copy.msg = 'Enjoy your token!';
          user_copy.token = token;
          user_copy.token_expiration = Date.now() + (1000 * 60 * cit_pass_timeout) - 10000;
          
          
          res.json(user_copy); 

          
        }; // end of ELSE !verified
        
      }); // end of scramblePass
      
    }; // end of user is OK
    
  }); // end of User.findOne
  
}; // end of authenticate_a_user


function record_help_time(user) {
  
  var time_now = Date.now();

  // ako uopće ne postoji help times
  if ( !user.help_times ) {

    user.help_times = [];
    user.help_times.push({in: time_now, out: time_now });

  } else {

    // ako postoji help times
    var last_help_time = 0;
    var last_help_index = null;

    $.each(user.help_times, function(help_ind, help) {
      // ako je last time manji ili jednak out vremenu u ovom objektu
      if ( last_help_time <= help.out ) {
        // onda je to novi last time
        last_help_time = help.out;
        last_help_index = help_ind;
      }
    });

    // ako je zadnje vrijeme manje od 5 min od NOW
    if (time_now - last_help_time < 5*60*1000 ) {
      // onda upiši trenutno vrijeme u taj hel pobjekt
      user.help_times[last_help_index].out = time_now;

    } else {
      // ako je prošlo više od 5 min od zanjeg help out onda napravi novi help objekt
      user.help_times.push({in: time_now, out: time_now });
    };

    // kraj else postoji help_times
  };
  
  return user;
  
};


exports.im_online = function(req, res) {

  var old_token = req.body.token;
  
  var is_info = (req.params.is_info == `true`) ? true : false;
  
  // verifies secret and checks exp
  jwt.verify(old_token, 'super_tajna', function(err, decoded_user) { 
    
      if (err) {
        
        res.json({ success: false, msg: 'Token korisnika nije potvrđen!' });    
        
        return;
        
      } else {
        
        // if everything is OK - make a new token
        // decoded is extrapolated user object from token itself !!!!
        // token always has the scrambled user data inside !!!!!
  
      var time_now = Date.now();  
        

      User.findOne(
        { _id: decoded_user._id }, 
        function(err, user) {

          if ( err || !user ) {
            res.json({ success: false, msg: 'Potvrda korisnika nije uspjela.<br>Nema tog korisnika u bazi ili se dogodila greška na serveru :(' });
            return;
          };

          if ( user ) {

            var clean_user = global.mng_to_js(user);

            if ( is_info ) clean_user = record_help_time(clean_user);

            User.findOneAndUpdate(
              { _id: decoded_user._id },
              { $set: { last_login: time_now, help_times: ( clean_user.help_times || null) }},
              {new: true}, 
              function(err, user) {

                if (err) {
                  res.json({ 
                    success: false,
                    msg: 'Update Im ONLINE kod korisnika num: ' + clean_user.user_number + ' NIJE USPIO!' 
                  });
                  return;
                };

                res.json({ success: true, msg: 'IM ONLINE SUCCESS!' });

              });

          };

        });


    }; // kraj da je token dobar !!
    
    
  }); // kraj provjere tokena !!!
  
  
};


exports.prolong_token = function(req, res) {
  
  var cit_pass_timeout = 720; // in minutes
  
  var old_token = req.body.token;
  // verifies secret and checks exp
  jwt.verify(old_token, 'super_tajna', function(err, decoded_user) {      
      if (err) {
        
        res.json({ success: false, msg: 'Failed to authenticate token.' });    
        return;
        
      } else {
        
        // if everything is OK - make a new token
        // decoded is extrapolated user object from token itself !!!!
        // token always has the scrambled user data inside !!!!!
  
  
      var time_now = Date.now();  
        
      User.findOneAndUpdate(
          { _id: decoded_user._id },
          { $set: { last_login: time_now }},
          {new: true}, 
          function(err, user) {
            if (err) {
              res.send(err);
              return;
            };

            
            var sign_obj = {
              _id: decoded_user._id,
              hash: decoded_user.hash
            };
            
            var new_token = jwt.sign(sign_obj, 'super_tajna', {
                expiresIn : 60 * cit_pass_timeout // expires in 30 min
            });
            res.json({
              token: new_token,
              token_expiration: Date.now() + (1000 * 60 * cit_pass_timeout) - 10000, // minus 10 sec just to be sure
            });

          });        
        
      };
  });
  
  
};


exports.logout_user = function(req, res) {
  
  var old_token = req.body.token;
  // verifies secret and checks exp
  jwt.verify(old_token, 'super_tajna', function(err, decoded_user) {
    
      if (err) {
        return res.json({ success: false, msg: 'Failed to authenticate token.' });  
        
      } else {
        
        // if everything is OK - make a new token
        // decoded is extrapolated user object from token itself !!!!
        // token always has the scrambled user data inside !!!!!
  
      var time_now = Date.now();  
        
      User.findOneAndUpdate(
          { _id: decoded_user._id },
          { $set: { last_login: time_now }},
          {new: true}, 
          function(err, user) {
            
            if (err) {
              console.error(err);
            };

            res.json({
              success: true,
              msg: `LOGOUT SUCCSESS!`
            });

          });   
        
      };
  });

};



/* TODO -- create add role to user */
// exports.add_role_to_user


var allow_emails = [

{ "email": "info@velprom.hr", "title": "SuperAdmin" },
{ "email": "branimir.ormuz@velprom.hr", "title": "Admin/Production" },
{ "email": "hrvoje.cizmic@velprom.hr", "title": "Sales" },
{ "email": "ivana@velprom.hr", "title": "Sales" },
{ "email": "ana.inkret@velprom.hr", "title": "Sales" },
{ "email": "martina@velprom.hr", "title": "Supply" },
{ "email": "marina@velprom.hr", "title": "Financial" },
{ "email": "alen.mahac@velprom.hr", "title": "Development" },
{ "email": "marijan.brekalo@velprom.hr", "title": "Development" },
{ "email": "vesna.tomasevic@velprom.hr", "title": "Sales" },
{ "email": "julija.gregic@velprom.hr", "title": "___" },
{ "email": "renato.sipus@velprom.hr", "title": "___" },
{ "email": "josip.jurkovic@velprom.hr", "title": "___" },
{ "email": "josip.kovacic@velprom.hr", "title": "Production" },
{ "email": "aleksandra.martinović@velprom.hr", "title": "___" },
{ "email": "renato.habijanec@velprom.hr", "title": "___" },
{ "email": "zlatko.srbelj@velprom.hr", "title": "___" },
{ "email": "dejan.gasparovic@velprom.hr", "title": "___" },
{ "email": "katarina.filic@velprom.hr", "title": "___" },
{ "email": "bruno.zgela@velprom.hr", "title": "___" },
{ "email": "renato.glibo@velprom.hr", "title": "___" },
{ "email": "ivanka.andric@velprom.hr", "title": "Administration" },
{ "email": "nikolina.japec@velprom.hr", "title": "Sales" },
{ "email": "ian.bolfek@velprom.hr", "title": "Development" },
{ "email": "toni.kutlic@velprom.hr", "title": "SuperAdmin" },
{ "email": "zoran@velprom.hr", "title": "SuperAdmin" },
{ "email": "antonia.bukovac88@gmail.com", "Production": "Production" },
{ "email": "steficaradic65@gmail.com", "Production": "Production" },
{ "email": "almaradic@gmail.com", "Production": "Production" },
{ "email": "pavisic222@gmail.com", "Production": "Production" },
{ "email": "Kristijangaso@gmail.com", "Production": "Production" },
{ "email": "Meli.mrkonjic@gmail.com", "Production": "Production" },

];




global.bulk_users = 
  
[


{
  "user_saved" : null,
  "user_edited" : null,

  "user_number": 551,

  "saved" : 1675432541116,
  "edited" : 1675432541116.0,
  "last_login" : 1675432541116.0,

  "user_address" : null,
  "user_tel" : null,

  "name" : "lidijada",
  "pass": "nogometjeto!",
  "full_name" : "Lidija Dalić",
  "email": "lidija.dalic@velprom.hr",

  "email_confirmed" : true,
  "welcome_email_id" : "452srislidijadgrisonlidijaccrisolidijap13d2568",
  "roles" : {
      "info" : "rwd",
      "sale" : true,
      "admin" : false
  },
  "new_pass_request" : null,
  "new_pass_email_id" : null,
  "new_pass_email_confirmed" : false,
  "avatar" : "/img/avatars/avatar_image-1622579352769.png",
  "title" : "RegularUser",
  "help_times" : null

},

    
  
  
];



global.just_created_users = [];


global.create_multiple_users = function(user_index, user_object) {
  
  
  
  global.just_created_users = [];
  
  return new Promise( function(resolve, reject) { 
  

    var original_pass = user_object.pass;

    delete user_object.pass;

    scramblePass(original_pass).hash(function(error, new_hash) {

      if (error) {
        console.error({ success: false, msg: 'Something went wrong with pass hash !!!' });
        return;
      };

      // Store hash (incl. algorithm, iterations, and salt) 
      user_object.hash = new_hash;

      var new_user = new User(user_object);
      // save to DB
      new_user.save(function(err, user) {

        if (err) {
          console.error(`--------------- SAVE USER MONGO ERROR ---------------`);
        };

        

        if ( user ) {
          // ako je uspješno spremio usera  
          var user_copy = global.mng_to_js(user);
          global.just_created_users.push(user_copy);
          console.log(`user ` + user.full_name + ` je spremljen. Ukupno: ` + (user_index+1) + `/` + global.bulk_users.length );

        } else {
          
          global.just_created_users.push({ err: err });
          console.error(`user ` + user.full_name + ` NIJE spremljen !!!!. ERROR:`);
          console.error(err);
          
        };

        if ( (user_index+1) < global.bulk_users.length ) {
          
          global.create_multiple_users( user_index+1, global.bulk_users[user_index+1] );
          
        } else {

          resolve( global.just_created_users );
          console.log(`SPREMIO SVE BULK USERE !!!!!`);

        };

      }); // end of user save  

    }); // end of scramble pass
    
    
    
    
  }); // kraj promisa  
    

}; // end of create_multiple_users




exports.get_all_users = async function(req, res) {
  
  var auth_response = await global.handle_auth(req);
  
  
  if ( auth_response.success !== true || auth_response.super_admin !== true ) {
    res.json({success: false, msg: `Došlo je do greške na serveru ili nemate ovlasti za ovu radnju!!!`, response: auth_response });
    return;
  };
  
  
  var user_query = { deleted: { $ne: true } };
  
  if ( req.body.user_ids ) {
    
    user_query = { 
      deleted: { $ne: true }, 
      _id: { $in: req.user_ids } 
    };
    
  };
  
  
  User.find(user_query)
    .exec(function(err, useri) {
    
      if (err) {
        res.json({success: false, msg: `Došlo je do greške prilikom pretrage korisnika u VELPROM bazi !!!`, err: err });
        return;
      };
    
      var clean_useri = [];
      if ( useri && useri.length > 0 ) clean_useri = global.mng_to_js_array(useri);  

      // axios.get('https://api.hnb.hr/tecajn/v2')
      axios.post(
        'https://help.velprom.hr/get_IB_users', 
        { user_ids: req.user_ids || null } 
      )
      .then(function (response) {

        var data = response.data;

        console.log('response data od INTERNE BROŠURE:')
        console.log(data);

        if (response.status == 200 ) {

          res.setHeader('Access-Control-Allow-Origin', '*');
          // Request methods you wish to allow
          res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
          // Request headers you wish to allow
          res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');


          var ib_users = data.users;

          if ( 
            clean_useri && clean_useri.length > 0
            &&
            ib_users && ib_users.length > 0 

          ) {

            $.each(clean_useri, function(user_index, mongo_user) {

              var help_times = null;

              console.log( mongo_user._id );
              
              var ib_user = global.find_item( ib_users, mongo_user._id, `_id`)
              
              if ( ib_user ) help_times = ib_user.help_times || null;
              
              clean_useri[user_index].help_times = help_times;
              
              
              clean_useri[user_index].help_time_sum = 0;
        
              if ( 
                clean_useri[user_index].help_times 
                &&
                clean_useri[user_index].help_times.length > 0 
              ) {

                $.each(clean_useri[user_index].help_times, function(h_ind, help) {
                  // ako nema in onda neka bude nula !!!
                  if ( !help.in ) help.in = 0;
                  //  ako nemam out neka bude isto kao in !!!!
                  if ( !help.out ) help.out = help.in;

                  var min_time = 0;

                  if ( help.out - help.in == 0 ) {
                    min_time = 2000*60
                  };

                  clean_useri[user_index].help_time_sum += (help.out - help.in) || min_time;

                });

                // prikaži vrijeme u minutama !!!
                clean_useri[user_index].help_time_sum = global.cit_round( clean_useri[user_index].help_time_sum / (1000*60), 0 );

              };
              

            });

          };
          
          
          

          
          


          res.json({ success: true, users: clean_useri });
          
          
          

        } else {
          // ako nije uspio dobiti IB usere !!!!!!
          res.json({ success: false, msg: 'get_IB_users ERROR', error: response });
        };

      })
      .catch(function (error) {

        // AXIOS REQ ERROR !!!!
        console.log(error);
        res.json({ success: false, msg: 'get_IB_users AXIOS ERROR', error: error });

      });


    
    
    
    
    
      
    
    
    });
  
  
  
  
};


exports.save_user_as_admin = async function(req, res) {
  
  
  
  var auth_response = await global.handle_auth(req);
  
  
  if ( auth_response.success !== true || auth_response.super_admin !== true ) {
    res.json({success: false, msg: `Došlo je do greške na serveru ili nemate ovlasti za ovu radnju!!!`, err: auth_response });
    return;
  };
  

  User.find(
  { $or: [ 
      { email: req.body.email }, 
      { name: req.body.name } 
    ]
  }, 
  async function(existing_user_err, existing_result) {

    if (existing_user_err) {

      res.json({ 
          success: false, 
          msg: "Greška u bazi prilikom pretrage korisnika",
          err: existing_user_err,
        });
      
      return;

    };

    var existing_users = null; 
    
    if ( existing_result && existing_result.length > 0 ) {
      
      existing_users = global.mng_to_js_array( existing_result );
      
    };

    if ( existing_users && existing_users.length > 0 ) {

      // -------------------------- AKO JE NAŠAO ISTI EMAIL ILI NADIMAK  ----------------------------

      res.json({ 
          success: false, 
          msg: "Već postoji korisnik s ovim emailom ili s ovim nadimkom !!!",
        });

      return;


    } else {


      global.bulk_users = [req.body];

      var new_user_number = await global.new_doc_counter(`users`, false);

      global.bulk_users[0].user_number = new_user_number.real_number;

      var c_users = await global.create_multiple_users( 0, global.bulk_users[0] );

      /* ako se dogodila greška onda ubacim u c_users objekt { err : err } */
      if ( c_users && c_users.length > 0 && !c_users[0].err ) {


        var new_user = c_users[0];

        new_user.help_times = [];

        // axios.get('https://api.hnb.hr/tecajn/v2')
        axios.post(
          'https://help.velprom.hr/make_new_help_user', 
          new_user
        )
        .then(function (response) {

          var data = response.data;

          console.log('response data od INTERNE BROŠURE za make_new_help_user:')
          console.log(data);

          if ( response.status == 200 && data.success == true ) {

            res.setHeader('Access-Control-Allow-Origin', '*');
            // Request methods you wish to allow
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
            // Request headers you wish to allow
            res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');


            res.json({ success: true, user: new_user });

          } else {
            // ako nije uspio dobiti IB usere !!!!!!
            res.json({ success: false, msg: 'make_new_help_user ERROR', error: response });
          };

        })
        .catch(function (error) {

          // AXIOS REQ ERROR !!!!
          console.log(error);
          res.json({ success: false, msg: 'make_new_help_user AXIOS ERROR', error: error });

        });


        // kraj ako ima usera i nije error u arrayu spremljenih usera 
      } 
      else {

        res.json({ success: false, msg: "Novi korisnik NIJE kreiran", user: null  });

      };            


    }; // kraj else ako nije našao usera po email

  }); // end of User.find za sve usere sa istim mailom OR istim nadimkom


  
}; // kraj save_user_as_admin



function gen_pass_hash(req) {
  
  return new Promise( function(resolve, reject) {
  
    var original_pass = req.body.pass;

    scramblePass(original_pass).hash(function(error, new_hash) {

      if (error) {
        resolve({ success: false, msg: 'Dogodila se greška kod upisa HASH-a za lozinku !!!', err: error });
        return;
      };

      // Store hash (incl. algorithm, iterations, and salt) 
      resolve({ success: true, hash: new_hash });

    }); // end of scramble pass
    
    
  }); // kraj promisa  
  
};



exports.update_user_as_admin = async function(req, res) {

  
  if ( req.body.pass ) {
   
    var result = await gen_pass_hash(req);
    
    if ( result.success == true ) {
      
      req.body.hash = result.hash;
      delete req.body.pass;
      
    } else {
      
      // pošalji nazad result u kojem je sada greška
      res.json(result);
      
      return;
      
    };
    
    
  }; // ako sam poslao novi pass
  
  
  global.update_user(req, res);
  
}; // kraj save_user_as_admin





/*

global.update_data = [
  
{ name: "Radmila",  dep: { sifra: "UPR", naziv: "Uprava" }, },
{ name: "tonik123",  dep: { sifra: "INF", naziv: "Informatički odjel" }, },
{ name: "ivanka",  dep: { sifra: "ADM", naziv: "Administracija" }, },
{ name: "ian",  dep: { sifra: "CAD", naziv: "Cad odjel" }, },
{ name: "marina",  dep: { sifra: "FIN", naziv: "Financije" }, },
{ name: "hrvoje",  dep: { sifra: "SAL", naziv: "Prodaja" }, },
{ name: "katarina",  dep: { sifra: "CAD", naziv: "Cad odjel" }, },
{ name: "glibo",  dep: { sifra: "GRF", naziv: "Grafički odjel" }, },
{ name: "anel",  dep: { sifra: "PRO", naziv: "Proizvodnja" }, },
{ name: "nikolina",  dep: { sifra: "SAL", naziv: "Prodaja" }, },
{ name: "alen",  dep: { sifra: "CAD", naziv: "Cad odjel" }, },
{ name: "alex",  dep: { sifra: "SKL", naziv: "Skladište" }, },
{ name: "mato",  dep: { sifra: "PRO", naziv: "Proizvodnja" }, },
{ name: "martina",  dep: { sifra: "NAB", naziv: "Nabava" }, },
{ name: "zoran",  dep: { sifra: "UPR", naziv: "Uprava" }, },
{ name: "bruno",  dep: { sifra: "PRO", naziv: "Proizvodnja" }, },
{ name: "agata",  dep: { sifra: "SAL", naziv: "Prodaja" }, },
{ name: "suvak",  dep: { sifra: "SAL", naziv: "Prodaja" }, },
{ name: "marijan",  dep: { sifra: "GRF", naziv: "Grafički odjel" }, },
{ name: "baltazar",  dep: { sifra: "SAL", naziv: "Prodaja" }, },  
  
];
*/


global.update_data = [ 
  
  { "card_num": "0004265419 ", "full_name": "Toni Kutlić", "user_number": 10 },
 { "card_num": "0001709827", "full_name": "Bruno Žgela", "user_number": 67 },
 { "card_num": "0001648421", "full_name": "Anel Hafizović", "user_number": 30 },
 { "card_num": "0001655863", "full_name": "Mato Piškovic", "user_number": 49 },
 { "card_num": "0007781371", "full_name": "Ankica Dobrenić", "user_number": 19 },
 { "card_num": "0004342737", "full_name": "Ruža Dugošija", "user_number": 203 },
 { "card_num": "0007802737", "full_name": "Ivan Karaga", "user_number": 35 },
 { "card_num": "0007790705", "full_name": "Maja Brajković", "user_number": 13 },
 { "card_num": "0003943565", "full_name": "Vedran Kuzmec", "user_number": 306 },
 { "card_num": "0004279416", "full_name": "Mirna Šoštarić", "user_number": 96 },
 { "card_num": "0007774199", "full_name": "Mihael Kuzmec", "user_number": 38 },
 { "card_num": "0004328171", "full_name": "Lucija Ivanić", "user_number": 304 },
 { "card_num": "0004315285", "full_name": "Antonija Gršetić", "user_number": 201 },
 { "card_num": "0004311968", "full_name": "Kristina Vugrinec", "user_number": 65 },
 { "card_num": "0004320325", "full_name": "Alma Radić", "user_number": 51 },
 { "card_num": "0004264312", "full_name": "Franjo Hrvojić", "user_number": 401 },
 { "card_num": "0004325021", "full_name": "Kristina Jelekovac", "user_number": 83 },
 { "card_num": "0004263332", "full_name": "Martina Kuzmić", "user_number": 70 },
 { "card_num": "0003941301", "full_name": "Matija Dobrenović", "user_number": 303 },
 { "card_num": "0007745418", "full_name": "Meliha Đogić Mrkonjić", "user_number": 22 },
 { "card_num": "0007804082", "full_name": "Ljuba Vujević", "user_number": 64 },
 { "card_num": "0001676948", "full_name": "Nevenka Hrvojić", "user_number": 402 },
 { "card_num": "0004282798", "full_name": "Ivan Zgurić", "user_number": 403 },
 { "card_num": "0007743640", "full_name": "Kristijan Gašparić", "user_number": 25 },
 { "card_num": "0007780147", "full_name": "Martina Lukinić", "user_number": 40 },
 { "card_num": "0001662063", "full_name": "Dejan Gašparović", "user_number": 26 },
 { "card_num": "0007751337", "full_name": "Milena Pišković", "user_number": 50 },
 { "card_num": "0001644354", "full_name": "Stjepan Vuljanko", "user_number": 66 },
 { "card_num": "0007778332", "full_name": "Danijela Džolić", "user_number": 21 },
 { "card_num": "0004330017", "full_name": "Jana Karla Šiprak", "user_number": 321 },
 { "card_num": "0001668012", "full_name": "Petar Brebić", "user_number": 15 },
 { "card_num": "0001635646", "full_name": "Štefica Radić", "user_number": 52 },
 { "card_num": "0001656964", "full_name": "Paula Đuran", "user_number": 85 },
 { "card_num": "0004325672", "full_name": "Sanja Šiljić", "user_number": 82 },
 { "card_num": "0001668607", "full_name": "Snježana Šurmanović", "user_number": 60 },
 { "card_num": "0004350101", "full_name": "Viktorija Blažeković", "user_number": 99 },
 { "card_num": "0004327422", "full_name": "Renato Habijanec", "user_number": 29 } 
                      
];



global.update_multiple_users = function(update_data, data_index) {

  

  User.findOneAndUpdate(
    { user_number: global.update_data[data_index].user_number },
    { card_num: global.update_data[data_index].card_num },
    { new: true, upsert: true }, function(err, user) {

      if (err) {
        console.log(`Error response prilikom update usera ${ global.update_data[data_index].name } `, err);
      };
      
      var user_copy = global.mng_to_js(user);
      
      console.log(`user ` + user.full_name + ` je updated. Ukupno: ` + (data_index+1) + `/` + global.update_data.length );

      if ( (data_index+1) < global.update_data.length ) {
        global.update_multiple_users( global.update_data, data_index+1 );
      } else {
        console.log(`NAPRAVIO UPDATE NA SVIM USERIMA !!!!!`);            
      };      
      
    });
 

}; // end of update_multiple_users



function spremi_usera(user_num, user_object, original_pass, req, res, new_or_edit) {
  
  
      var cit_pass_timeout = 720; // in minutes
  
  
      scramblePass(original_pass).hash(function(error, new_hash) {
        
        if (error) {
          res.send({ success: false, msg: 'Something went wrong with pass hash !!!' });
          return;
        };
        
        // Store hash (incl. algorithm, iterations, and salt) 
        user_object.hash = new_hash;
        user_object.user_number = user_num;
        
        
        var transporter = nodemailer.createTransport(global.vel_mail_setup);
        
        
        /*
        
        var transporter = nodemailer.createTransport({
          service: 'Gmail',
          auth: {
            user: 'vrata.dizajn.crm@gmail.com', // Your email id
            pass: 'vrataCRM123!321' // Your password
          }          
        });
        
        */

        var mail_subject = 'Dobrodošli u Velprom App';
        
        var welcome_email_id = Date.now() + '-' + ( Date.now() + 35154354354 ) + '-' + ( Date.now() + 6846454354 );
        //req.headers.origin  OR  'http://192.168.178.30:8080'
        var link = `${global.cit_domain}/confirm_email/` + welcome_email_id ;
        var email_user_name = user_object.name;
        
        var html = email_body.generate(link, email_user_name).html;
        
        // setup email data
        var mailOptions = {
            from: `"Velprom App" <${global.vel_email_address}>`, // sender address
            to: user_object.email, // list of receivers
            subject: mail_subject, // Subject line
            html:  html // html body
        };

          // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
          
          if (error) {
            
            res.json({ 
              success: false, 
              msg: "User nije spremljen!<br>Problem u slanju maila za konfirmaciju !!!",
              error: error
            });
            
          };
          
          // add new email id to user object
          user_object.welcome_email_id = welcome_email_id;
          
          var new_user = new User(user_object);
          // save to DB
          new_user.save(function(err, user) {
            
            if (err) {
              res.send(err);
            };
            
            // create a token for new user !!!!
            
            var user_copy = global.mng_to_js(user);
            
            var sign_obj = {
              _id: user_copy._id,
              hash: user_copy.hash,
            };
           
            
            var token = jwt.sign(sign_obj, 'super_tajna', {
              expiresIn : 60 * cit_pass_timeout // expires in 30 min
            });

            // return user info including token as JSON
            
            // using now +  (expiresIn miliseconds) minus 10 sec just to be sure I'm in proper interval

            /* ----------------------- HERE IS RESPONSE FOR USER SAVE !!!!!  ----------------------- */

            
            user_copy.user_id = user._id;
            user_copy.success = true;
            user_copy.msg = 'Enjoy your token!';
            user_copy.token = token;
            user_copy.token_expiration = Date.now() + (1000 * 60 * cit_pass_timeout) - 10000;

            
            res.json(user_copy);

            
          }); // end of user save  
            
        }); // end of send welcome email
                
      }); // end of scramble pass
        
  
  
}; // kraj moje funkcije spremi usera


exports.create_a_user = function(req, res) {

  var user_object = req.body;
  
  
  /*
  const found_email = allow_emails.find(usr => usr.email == user_object.email);
  
  
  if ( !found_email ) {
    
    res.json({ 
      success: false, 
      msg: "Email nije na popisu dopuštenih email adresa !!!",
      user_number: null,
    });
    
    return;
  };
  
  user_object.title = found_email.title;
  
  */
  
  // COPY READABLE PASS TO VARIABLE
  var original_pass = String(user_object.password);
      
  // DELETE READABLE PASS !!!!!!!!!! -- just to be safe :-)
  delete user_object.password;
  var name_taken_switch = false;
  var email_taken_switch = false;
  
  User.findOne(
    { name: req.body.name }, 
    function(err, user) {
      
      if (err) {
        res.json({ success: false, msg: "Dogodila se greška u traženju usera!!!", user_number: null,  });
        return;
      };
      
      
      if (user) {
        
        // -------------------------- AKO JE NAŠAO ISTO IME / NADIMAK ----------------------------
        name_taken_switch = true;
        res.json({ success: false, msg: "Već postoji korisnik s ovim nadimkom !!!", user_number: null, name_taken: true });
        return;
        
      } else {
        
    
      User.findOne(
        { email: req.body.email }, 
        function(err, user) {
          if (err) throw err;
          
          if (user) {

            // -------------------------- AKO JE NAŠAO ISTI EMAIL  ----------------------------
            
            email_taken_switch = true;

            res.json({ 
                success: false, 
                msg: "Već postoji korisnik s ovim emailom !!!",
                user_number: null,
                email_taken: true
              });
            
            return;
            

          } else {
            
            // -------------------------- AKO NIJE ISTO IME NITI JE ISTI EMAIL ----------------------------

            // user counter kolekcija ima samo jedan dokument i taj dokument ima samo jedan propery, a to je count !!!!!
            // inc znači increment  - u biti povećavam zadnji upisani counter za +1 !!!!
            // $ne: null je bezveze query koji će uvijek vratiti true jer count nikada nije null !!!

              Counter.findOneAndUpdate(
                { users: { $ne: null } },
                { $inc: { users: 1 } },
                { "new": true }
              )
              .exec(function(err, counter) {
                if (err) {
                    res.json({ 
                      success: false, 
                      msg: "User nije spremljen! Counter problem.",
                      user_number: null
                    });
                  
                  return;
                };  
                var new_user_number = counter.users;
                var new_or_edit = 'new';

                spremi_usera(new_user_number, user_object, original_pass, req, res, new_or_edit);

                }); // kraj user counter ----> povećaj counter i spremi
            
          }; // kraj elese ako nije našao usera po email

        }); // end of User.findOne po EMAIL

      }; // end of else --- ako nije našao postojećeg usera po name
      
    }); // end of User.findOne po NAME
  
    
}; // end of create_a_user function


exports.upload_avatar = function(req, res) { 

  console.log(req);
  
  var avatar_upload = multer({ storage: avatar_storage }).single('avatar_image');

  avatar_upload(req, res, function (err) {
    
    if (err instanceof multer.MulterError) {
       
      res.json({
        success: false,
        msg: "Došlo je do greške kod spremanja avatara - pogledaj err property !!!",
        filename: null,
        err: err
      });
      
      return;
      
    };
    
    
    // provjeri jel ima file i ako ima jel ima file name !!!
    
    if ( !req.file || !req.file.filename ) {
      
      res.json({
        success: false,
        msg: "Došlo je do greške kod spremanja ikone na server.<br>Probajte ponovo!",
        filename: null
      });

    }
    else {
      
      if ( req.body.user_number ) {
        
        User.findOneAndUpdate(
          { user_number: Number(req.body.user_number) },
          { $set: { avatar: `/img/avatars/${req.file.filename}` }},
          {new: true}, 
          function(err, user) {
            
            if (err) {
              res.json({
                success: false,
                msg: "Došlo je do greške kod update-a avatara u bazi usera<br>Probajte ponovo!",
                filename: null
              });
              return;
            };
            
            res.json({
              success: true,
              msg: "Avatar je ažuriran i spremljen kod usera!",
              filename: req.file.filename
            });
  
          }); // kraj update avatar url kod usera !!!
        
      } 
      else {
        
        res.json({
          success: true,
          msg: "Avatar je spremljen !",
          filename: req.file.filename
        });
        
      }; // kraj jel ima ili nema user number
  
    }; // kraj jel ima ili nema file propertija u req
    
  });
  

  
}; // kraj upload avatar


exports.confirm_user_email = function(req, res) { 
  
  User.findOneAndUpdate(
    { welcome_email_id: req.params.welcome_email_id },
    { $set: { email_confirmed: true }},
    {new: true}, 
    function(err, user) {
      if (err) res.send(err);
      
      // res.json({ msg: "Email confirmed !!!", customer_id: customer._id });
      
      // this is just exapmle of sending raw html
      res.set('content-type','text/html');
      
      
var link_to_we_park_admin = 
`<a style="  background-color: #06253f;
border-radius: 8px;
height: 60px;
width: 200px;
font-size: 14px;
margin: 20px auto 0;
text-align: center;
display: block;
min-width: 200px;
color: #fff;
text-decoration: none;
line-height: 60px;"

target="_blank"
href="${global.cit_domain}">

ULAZAK U VELPROM APP

</a>`;
      
      
      var mail_confirmation_page = 
`
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
  <title>Velprom Email Confirmation</title>
  <meta name="description" content="Velprom Email Confirmation">
  <meta name="author" content="Velprom Ltd">

</head>
<body style="display: flex; 
             align-items: center; 
             justify-content: center; 
             flex-direction: column; 
             height: 100vh; 
             font-family: Arial, Helvetica, sans-serif;">




        <img style="
          width: 200px;
          height: auto;
          max-width: 200px;
          border-radius: 0;
          margin-top: 100px;
          display: block;
          float: none;
          clear: both;
          margin: 20px auto 0;"
          alt="Velprom Logo" 
          src="${global.cit_domain}/img/velprom_logo_email.png" />

  
  
  <h4 style="display: block; font-family: Arial, Helvetica, sans-serif; font-size: 30px;  margin: 15px; color: #06253f;">
  Email potvrđen!
  </h4>
  <h6 style="display: block; font-family: Arial, Helvetica, sans-serif; font-size: 15px; margin: 0px; color: #06253f;">
  Uživajte u Velprom aplikaciji :)
  </h6>

${ link_to_we_park_admin }

</body>
</html>
`;      
      res.send(mail_confirmation_page);  
      
    });
};


exports.get_sve_user_brojeve = function(req, res) { 
  
   User.find({})
    .select({
      "user_number": 1,
      "full_name": 1
    }) 
    .exec(function(err, useri) {
      if (err) res.send(err);
      res.json(useri);
    });
};


function run_user_edit(pronadjeni_user, req, res) {
  
  
  var cit_pass_timeout = 720; // ovo je u sekundama  ----> 12 sati
  
  // ------------------------------ stavio sam da uvijek bude TRUE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  // ------------------------------ stavio sam da uvijek bude TRUE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  // ------------------------------ stavio sam da uvijek bude TRUE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  if (
    
    (pronadjeni_user.roles && pronadjeni_user.roles.we_app_user == true)
    || 
    true
    
  ) {
    cit_pass_timeout = 720 * 1000; // to je  točno 500 dana
  };

  // -------------- OBRIŠI SVE PROPERTIJE KOJI IMAJU VEZS SA TOKENOM I AUTENTIKACIJOM
  
  var user_id = req.body._id;
  
  // delete req.body._id;
  delete req.body.user_id;
  delete req.body.success;
  delete req.body.msg;
  delete req.body.token;
  delete req.body.token_expiration;

  // obrisi i roles ----> tako da slučajno ne prebrišem roles koje su u menjuvremenu spremljene u bazu !!!!
  /// .. ali nisu povučene lokalno u cit_user !!!!
  // delete req.body.roles;
  
  User.findOneAndUpdate(
    { _id: user_id }, 
    req.body,
    { new: true }, 
    function(err, updated_user) {

      if (err) {
        res.send({ success: false, msg: 'Dogodila se greška u spremanju usera !', error: err });
        return;
      };

      if ( !updated_user ) {
        res.send({ success: false, msg: 'Dogodila se greška u spremanju usera !', error: 'updated_user je null' });
        return;
      };



    
      var clean_user = global.mng_to_js(updated_user);

      
      // resetiraj help times jer ću ionako doboti to iz IB usera
      clean_user.help_times = null; // brišem help times samo da bude manje podataka za prijenos !!!
      
      
      var IB_url = 'https://help.velprom.hr/update_help_user'

      if ( clean_user.deleted == true ) IB_url = 'https://help.velprom.hr/delete_help_user'
      
      axios.post(
        IB_url, 
        clean_user
      )
      .then(function (response) {

        var data = response.data;

        console.log('response data od INTERNE BROŠURE za update / delete :')
        console.log(data);

        if ( response.status == 200 && data.success == true ) {


          // prekopiraj help times sa usera kojeg sam dobio sa INTERNE BROŠURE
          // ako je delete_help_user onda mi neće vratiti usera nego samo success true !!!! 
          clean_user.help_times = data.user ?  data.user.help_times : [];

          res.setHeader('Access-Control-Allow-Origin', '*');
          // Request methods you wish to allow
          res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
          // Request headers you wish to allow
          res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

          
          if ( clean_user.deleted !== true ) {

          var sign_obj = {
            _id: clean_user._id,
            hash: clean_user.hash
          };


          var token = jwt.sign(sign_obj, 'super_tajna', {
            expiresIn : 60 * cit_pass_timeout // expires in 720 min
          });


          clean_user.user_id = clean_user._id;
          clean_user.success = true;
          clean_user.msg = 'Enjoy your token!';
          clean_user.token = token;
          clean_user.token_expiration = Date.now() + (1000 * 60 * cit_pass_timeout) - 10000;


          };
            
          res.json({ success: true, user: clean_user });


        } else {
          // ako nije uspio dobiti IB usere !!!!!!
          res.json({ success: false, msg: 'update_help_user ERROR', error: response });
        };

      })
      .catch(function (error) {

        // AXIOS REQ ERROR !!!!
        console.log(error);
        res.json({ success: false, msg: 'update_help_user AXIOS ERROR', error: error });

      });


      
  }); // kraj resulta za mongo update
  
  
};
// kraj run user edit


function find_user_and_update(req, res) { 
  
// find the user
  User.findOne({ _id: req.body._id }, function(err, user) {
    
    if (err) {
      res.json({ success: false, msg: 'Ažuriranje korisnika nije uspjelo.<br>Dogodila se greška prilikom pretrage usera!' });
      return;
    };
    
    if (!user) {
      // if no user
      res.json({ success: false, msg: 'Ažuriranje korisnika nije uspjelo.<br>Sustav nije pronašao korisnika u bazi!' });
    } 
    else {
      
      var clean_user = global.mng_to_js(user);
      // ----------------------------- ovo radi samo ako je super admin ili ako editira samog sebe !!!!
      run_user_edit(clean_user, req, res);
      
    };
    
  }); // end of User.findOne
  
}; // kraj func find user and update !!!


exports.update_user = function(req, res) {

  var update_user_num = req.body.user_number;
  
  if ( !update_user_num ) {
    
    res.json({ 
          success: false, 
          msg: "User nije ažuriran jer nedostaje BROJ korisnika!",
          user_number: update_user_num
        });    
    
    return;
    
  };
  
  var token = req.headers['x-auth-token'] || null;
  
  if ( token ) {
      
    global.check_user_auth(token).then(
    function( result ) {
      
      if ( result.success == true ) {
        // ako je super admin ili ako sprema sebe samog !!!
        if (  result.super_admin == true || update_user_num == result.user.user_number ) {
          
          find_user_and_update(req, res);
          
          return;
          
        } else {
          // ako nije super admin i nije vlasnik ovog parkinga
          res.json({ success: false, msg: 'Nemate ovlasti za ovu radnju!' });  
          
        };
        
      } else {
        
        res.json({ success: false, msg: 'Došlo je do greške prilikom editiranja korisnika!' }); 
        return;
        
      };

    })
    .catch(function(err) {
      
      // ako se dogodi bilo koja greška kod verifikacije tokena ili pronalaska usera
      res.json({ success: false, msg: 'Došlo je do greške prilikom editiranja korisnika!' }); 
      
    });

  } 
  else {
    // vrati da nema ovlasti ako je request bez tokena !!!!
    res.json({ success: false, msg: 'Nemate ovlasti za ovu radnju!' });    
    
  };
  
};
global.update_user = exports.update_user;
  
exports.delete_user = function(req, res) {
  
  var user_id = req.body._id;
  
  User.deleteOne(
    { _id: user_id  },
    function (err) {
    
    if (err) {
      res.json({ success: false, msg: 'Došlo je do greške brisanja korisnika!', err: err });
    } else {
      res.json({ success: true, msg: 'Korisnik je obrisan!',});
    };
    
  });
  
  
};  


exports.user_pass_reset = function(req, res) { 

  var user_email = req.body.user_email;
  var user_new_pass = req.body.new_pass;
  
  // find the user
  User.findOne(
    { email: user_email },
    function(err, user) {
    
    if (err) {
      res.json({ success: false, msg: 'Došlo je do greške kod pretrage po mail adresi!', err: err });
      return;
    };
    
    if (!user) {
      // if no user
      res.json({ success: false, msg: 'Nema korisnika s ovim mailom u bazi!<br>Provjerite jeste li dobro upisali adresu!' });
    } 
    else if (user) {
      
    var clean_user = global.mng_to_js(user);
    
    var new_pass_email_id = clean_user.user_number + '-' + Date.now() + '-' + ( Date.now() + 7234567891234 );

    User.findOneAndUpdate(
      { _id: clean_user._id }, 
      { new_pass_request: user_new_pass, new_pass_email_id: new_pass_email_id, new_pass_email_confirmed: false },
      { new: true }, function(err, new_user_db) {

      if (err) {
        res.json({ success: false, msg: 'Greška kod upisa zahtjeva za novu lozinku!', err: err });
        return;
      };
   

      var transporter = nodemailer.createTransport(global.vel_mail_setup);
        
      var html = confirm_pass.conf_html(new_pass_email_id, clean_user);
      // setup email data
      var mailOptions = {
          from: `"Velprom App" <${global.vel_email_address}>`, // sender address
          to: clean_user.email, // list of receivers
          subject: 'Velprom promjena lozinke', // Subject line
          html:  html // html body
      };

      // send mail with defined transport object
      transporter.sendMail(mailOptions, (error, info) => {

        if (error) {
          res.json({ success: false, msg: 'Greška prilikom slanja emaila za potvrdu resetiranja lozinke!', err: error });
        } else {
          res.json({ success: true, msg: 'Otvorite Vaš mail i kliknite na tipku POTVRDITE. Tako ćete potvrditi vlasništvo nad  email adresom.<br>Nakon potvrde email adrese možete koristiti vašu novu lozinku za prijavu u Velprom App' });
        };


      }); // end of send  email


    });    // kraj update usera s novom šifrom za email confim  


      
    }; // end of user is OK
    
  }); // end of User.findOne
  


};


function create_reset_pass_response_html(is_ok) {
  
  
  var big_text = 'Potvrda promjene lozinke je uspješna!';
  var small_text = 'Sada se možete ulogirati u Velprom aplikaciju s novom lozinkom.';
    
  if ( is_ok == false ) {
    big_text = '<span style="color: #cb0606;">Došlo je do greške u potvrdi email adrese!</span>';
    small_text = 'Moguće je da ste već kliknuli na gumb POTVRDI i nova lozinka je već aktivna. Ako se ne možete ulogirati s novom lozinkom, probajte ponovo kliknuti na gumb POTVRDI u emailu. Ukoliko se greška ponovi molimo nazovite:<br> 00385 92 3133 015';
  };
  
  var pass_email_confirm_response = 
`
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
  <title>Velprom Email Confirmation</title>
  <meta name="description" content="Velprom Email Confirmation">
  <meta name="author" content="Velprom Ltd">

</head>


<body style="display: flex; 
             align-items: center; 
             justify-content: center; 
             flex-direction: column; 
             height: 100vh; 
             font-family: Arial, Helvetica, sans-serif;">
 

        <img style="
          width: 200px;
          height: auto;
          max-width: 200px;
          border-radius: 0;
          margin-top: 100px;
          display: block;
          float: none;
          clear: both;
          margin: 20px auto 0;"
          alt="Velprom Logo" 
          src="${global.cit_domain}/img/velprom_logo_email.png" />
    
  
<h4 style="display: block; 
font-family: Arial, Helvetica, sans-serif;
font-size: 30px;
text-align: center;
margin: 15px; 
color: #6b8fb5;">
${big_text}
</h4>

<h6 style="display: block;
font-family: Arial, Helvetica, sans-serif;
font-size: 15px;
text-align: center;
margin: 0px; 
padding: 20px;
color: #103152;
max-width: 300px;">
${small_text}
</h6>

</body>


</html>
`; 
      
return pass_email_confirm_response;
  
};


exports.pass_reset_confirm = function(req, res) { 
  
  User.findOne(
    { new_pass_email_id: req.params.reset_email_id },
    function(err, user) {
      
      
      if ( !user || err ) {
        var response_html = create_reset_pass_response_html(false);
        // this is just exapmle of sending raw html
        res.set('content-type','text/html');
        res.send(response_html);
        return;
      };
      
      if (user) {
        
        var clean_user = global.mng_to_js(user);
        
        scramblePass( clean_user.new_pass_request ).hash(function(error, new_hash) {
          
          if (error) {
            var response_html = create_reset_pass_response_html(false);
            // this is just exapmle of sending raw html
            res.set('content-type','text/html');
            res.send(response_html);
            return;
          };
          
          User.findOneAndUpdate(
            { _id: clean_user._id },
            { hash: new_hash, new_pass_request: null, new_pass_email_confirmed: true },
            {new: true}, 
            function(err, user) {
              
              var response_html = create_reset_pass_response_html(true);
              // this is just exapmle of sending raw html
              res.set('content-type','text/html');
              res.send(response_html);
              
            }); // kraj update user
          
        }); // kraj scramble pass

      };
      
    }); // kraj find user
};


exports.pass_for_token = function(req, res) {
  
  var pass = req.body.pass;
  var user = req.body.user;
  
  // find the user
  User.findOne(
    { name: user }, 
    function(err, user) {
    
    if (err) {
      res.json({ success: false, msg: 'Došlo je do greške prilikom autentikacije!', err: err });
      return;
    };
    
    if (!user) {
      // if no user
      res.json({ success: false, msg: 'Korisnik nije pronađen!' });
      return;
    };

      // if user is found verify a hash 
      scramblePass(pass).verifyAgainst(user.hash, function(error, verified) {
        
        if (error) {
          res.json({ success: false, msg: 'Došlo je do greške prilikom provjere lozinke!', err: error });
          return;
        };
        
        if ( !verified ) {
          // if pass is wrong !!!!!!
          res.json({ success: false, msg: 'Lozinka nije ispravna!' });
        } else {
          
          res.json({ success: true, msg: 'Lozinka je ispravna!' });
          
        }; // end of ELSE !verified
        
      }); // end of scramblePass
      
    
  }); // end of User.findOne
  
}; // end of pass for token


exports.set_user_lang = function(req, res) {

  
  var user_number = req.body.user_number;
  var lang = req.body.lang;

  UserLang.findOneAndUpdate( 
    { user_number: user_number },
    { lang: lang },
    { upsert: true, safe: true, new: true },
    function(err, saved_user_lang) {

    if (err) {
      console.log('ERROR !!!!!!! U SAVE USER LANG FOR USER NUMBER: ' + user_number);
      res.json({success: false, err: err })
      console.log(err);

    } else {
      global.all_user_langs[user_number] = lang;
      res.json({success: true, lang: saved_user_lang.lang })
      
    };
  }); // end of upsert user lang
  
  
  
};


exports.r1_racun_user_podaci = function(req, res) {
  
  
  var user_num = req.body.user_num;
  
  var naziv_firme = req.body.naziv_firme;
  var oib = req.body.oib;
  var adresa = req.body.adresa_firme || '';
  var mjesto = req.body.pp_i_mjesto_firme ? ( ', ' + req.body.pp_i_mjesto_firme ) : '';
  
  var r1 = req.body.r1;
  
  
  var update_object = {};
  
  if ( oib ) update_object.oib = String(oib);
  if ( naziv_firme ) update_object.business_name = naziv_firme;
  if ( adresa ) update_object.business_address = adresa + mjesto;
  
  if ( r1 ) update_object.r1 = true;
  if ( !r1 ) update_object.r1 = false;
  


  User.findOneAndUpdate(
      { user_number: user_num },
      update_object,
      {new: true}, 
      function(err, user) {
        if (err) {
          res.send({ success: false, msg: 'popup_business_podaci_error', error: err });
        } else {
          res.send({ success: true, msg: 'Business podaci usera SPREMLJENI !!!!!' });
        };
      }); 
  
};


exports.get_user_business_data = function(req, res) { 
  
  User.findOne(
    { user_number: Number(req.params.user_num) },
    function(err, user) {
      
      if ( !user || err ) {
        res.send({ success: false, error: (err || null) } );
        return;
      };
      
      if (user) {
    
        var clean_user = global.mng_to_js(user);
        
        res.send({
          success: true,
          oib: clean_user.oib,
          business_name: clean_user.business_name,
          business_address: clean_user.business_address          
        });
        
      }; 
    }); // kraj find user
};


exports.update_user_from_db = function(req, res) {
  
  var user_number = req.params.user_number;
  
  var token = req.body.token || req.query.token || req.headers['x-auth-token'];

      if (token) {
        // verifies secret and checks exp
        jwt.verify(token, 'super_tajna', function(err, decoded) {      
            if (err) {
              return res.json({ success: false, msg: 'Failed to authenticate token.' });    
            } else {


            // find the user
              User.findOne({ _id: decoded._id }, function(err, user) {

                if (err) {
                  res.json({ success: false, msg: 'FIND korisnika nije uspjelo.<br>Dogodila se greška prilikom pretrage usera!' });
                  return;
                };

                if (!user) {
                  // if no user
                  res.json({ success: false, msg: 'FIND korisnika nije uspjelo.<br>Sustav nije pronašao korisnika u bazi!' });
                } 
                else {
                  var clean_user = global.mng_to_js(user);
                  res.json({ success: true, user: clean_user }); 
                };

              }); // end of User findOne              
              
            }; // kraj else for err od tokena
          
          }); // kraj verify token

      } else {
        
        res.json({ success: false, msg: 'FIND korisnika nije uspjelo. Neuspjela autorizacija !!!!' });
      }; // kraj ako postoji token
  
  
};


exports.search_kal_users = function(req, res) {
  
  global.cit_search_remote(req, res, User)
  .then(function(result) {
    res.json(result) 
  })
  .catch(function(error) {
    res.json(error) 
  });
  
};





exports.get_all_user_tests = function(req, res) {
  
  
  
};




exports.get_all_user_tests = async function(req, res) {

  
  var user_id = req.body._id;
  

  
          // axios.get('https://api.hnb.hr/tecajn/v2')
        axios.post(
          'https://help.velprom.hr/get_all_user_tests', 
          {user_id: user_id }
        )
        .then(function (response) {

          var data = response.data;

          console.log('response data od INTERNE BROŠURE za get_all_user_tests:')
          console.log(data);

          if ( response.status == 200 && data.success == true ) {

            res.setHeader('Access-Control-Allow-Origin', '*');
            // Request methods you wish to allow
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
            // Request headers you wish to allow
            res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');


            res.json({ success: true, tests: data.tests });

          } else {
            // ako nije uspio dobiti IB usere !!!!!!
            res.json({ success: false, msg: 'get_all_user_tests ERROR', error: response });
          };

        })
        .catch(function (error) {

          // AXIOS REQ ERROR !!!!
          console.log(error);
          res.json({ success: false, msg: 'get_all_user_tests AXIOS ERROR', error: error });

        });



  
}; // kraj save_user_as_admin





