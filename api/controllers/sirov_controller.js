var mongoose = require('mongoose');



var fs = require('fs');
var path = require("path");
var { spawn } = require("child_process");
var { exec } = require("child_process");

// mongoose.set('useFindAndModify', false);

var User = mongoose.model('User');
var Counter = mongoose.model('Counter');
var Grupa = mongoose.model('Grupa');
var Sirov = mongoose.model('Sirov');
var AlatShelf = mongoose.model('AlatShelf');

var PaletSifra = mongoose.model('PaletSifra');

var Partner = mongoose.model('Partner');



var nodemailer = require('nodemailer');
var multer = require('multer')

var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);


// var id = mongoose.Types.ObjectId('4edd40c86762e0fb12000003');
// var _id = mongoose.mongo.BSONPure.ObjectID.fromHexString("4eb6e7e7e9b7f4194e000001");

var find_controller = require('./find_controller.js');

// var sirov_for_db = require('./new_sirovs.js')

// var gotovi_alati = require('./gotovi_alati.js');


var novi_repro = require('./novi_repro.js');



exports.find_sirov = function(req, res) {

  global.cit_search_remote(req, res, Sirov)
  .then(function(result) {
    res.json(result) 
  })
  .catch(function(error) {
    res.json(error) 
  });
  
};


exports.find_sirov_location = function(req, res) {

  global.cit_search_remote(req, res, Sirov)
  .then(function(result) {
    res.json(result) 
  })
  .catch(function(error) {
    res.json(error) 
  });
  
};


exports.find_sirov_records = function(req, res) {

  global.cit_search_remote(req, res, Sirov)
  .then(function(result) {
    res.json(result) 
  })
  .catch(function(error) {
    res.json(error) 
  });
  
};







exports.upload_sirov_docs = function(req, res) {
  
  global.upload_docs(req, res);
  
};


exports.find_grupa = function(req, res) {

  global.cit_search_remote(req, res, Grupa)
  .then(function(result) {
    res.json(result) 
  })
  .catch(function(error) {
    res.json(error) 
  });
  
};


exports.edit_grupa_name = function(req, res) {
  
  var new_name = req.body.new_name;
  var id = req.body.id;

  Grupa.findOneAndUpdate(
    { _id: id },
    { $set: { name: new_name } },
    { new: true }, function(err, grupa) {

      
      if (err) {
        console.log(`Error response prilikom promjene imena grupe`, err);
        res.json({success: false, error: err, msg: `Greška prilikom promjene imena grupe !!!` })
        return;
      };
      
      var clean_grupa = global.mng_to_js(grupa);
      
      res.json({success: true, grupa: clean_grupa });
      
      
      // grupa flat je samo string od cijelog objekta grupe 
      // i u sirovinama imam samo id od grupe kao referencu !!!! ----> i naravno taj grupa flat 
      Sirov.updateMany(
        { "grupa": id },
        { grupa_flat: JSON.stringify(clean_grupa) },
        function(err, updated_sirovs) {
        if (err) { 
         console.log("Greška kod update grupe na svakoj sirovini", err);
        } else { 
          console.log("Promjenio sam grupa_flat na svakoj sirovini");
        };
      });      
      
    });
  
};



exports.remove_from_grupa = function(req, res) {
  
  var sirov_id = req.body.sirov_id;
  var id = req.body.id;

  Grupa.findOne(
    { _id: id },
    function(err, grupa) {

      
      if (err) {
        console.log(`Error response prilikom promjene imena grupe`, err);
        res.json({success: false, error: err, msg: `Greška prilikom promjene imena grupe !!!` })
        return;
      };
      
      var clean_grupa = global.mng_to_js(grupa);

      clean_grupa.sirovine = global.delete_item(clean_grupa.sirovine, sirov_id, `_id` );
      
      
      Grupa.findOneAndUpdate(
        { "_id": id },
        clean_grupa,
        function(err, updated_grupa) {
        if (err) { 
         console.log("Greška kod update grupe nakon izbacivanja sirovine", err);
        } else { 
          
          Sirov.updateMany(
            { "grupa": id },
            { grupa_flat: JSON.stringify(clean_grupa) },
            function(err, updated_sirovs) {
            if (err) { 
             console.log("Greška kod update grupe na svakoj sirovini nakon brisanja jedne od sirovina", err);
            } else { 
              console.log("Uspješno sam izbacio sirovinu iz grupe !!!!!");
              res.json({success: true, grupa: clean_grupa });
            };
              
          }); // napravio update svih sirovina
          
        };
          
      }); // napravio update grupe
      
      
      
      
            
      
    });
  
};





exports.new_grupa = async function(req, res) {

  var name = req.body.name;
  var sirovina_id = req.body.sirovina_id;
  var sirovina_naziv = req.body.sirovina_naziv;
  var old_grupa_id = req.body.old_grupa_id;
  
  var sirovina_sifra = req.body.sirovina_sifra;
  var sir_dobav = req.body.sirovina_dobav_naziv;

  var sirov_obj = { _id : sirovina_id, name : sirovina_naziv, sir_sifra: sirovina_sifra, sir_dobav: sir_dobav };
  
  
  const counter = await global.new_doc_counter(`grupa`);

  var new_grupa_obj = {
      sifra: "GRUPA" + counter.real_number,
      name: name,
      sirovine: [ sirov_obj ],
      sirovine_flat: JSON.stringify( [ sirov_obj ] )
  };
  
  
  var new_grupa = new Grupa(new_grupa_obj);
  // save to DB
  new_grupa.save(function(err, grupa) {

    if (err) {
      res.json({success: false, error: err });
      return;
    };
    
    var clean_grupa = global.mng_to_js(grupa);
    
    res.json({success: true, grupa: clean_grupa });
    
    

    if ( old_grupa_id ) {

      // obriši ovu sirovinu u staroj grupaciji
      Grupa.findOneAndUpdate(
        { _id: old_grupa_id },
        { $pull: { sirovine: { _id: sirovina_id } } },
        { new: true }, function(err, old_grupa) {

          if (err) {
            console.log(`Error prilikom brisanja sirovine u STAROJ grupi`, err);
            return;
          }; 
          
          console.log(`Obrisao sam sirovinu iz STARE grupe: ` + mng_to_js(grupa)._id );
          var clean_old_grupa = mng_to_js(old_grupa);
        

          /*
          --------------------------------------------------------------------------------
          RADIM SAO UPDATE GRUPA_FLAT jer to naravno nije povezano sa populate !!!!!
          --------------------------------------------------------------------------------
          */
          Sirov.updateMany( 
            { "grupa": clean_old_grupa._id },
            { grupa_flat: JSON.stringify(clean_old_grupa) },
            function(err, updated_sirovs) {
            if (err) { 
             console.log("Greška kod update grupa_flat na svakoj sirovini KOJE IMAJU JOŠ IMAJU STARU GRUPU", err);
            } else { 
              console.log("promjenio sam grupa_flat na svakoj sirovini");
            };
          });
         

        }); // kraj  brisanja kompanije unutar grupacije od partnera

    };
    

  }); // kraj save new grupa
  

  

};

exports.update_grupa = function(req, res) {
  
  var name = req.body.name;
  var id = req.body._id;
  var sirovine = req.body.sirovine;
  var sirovine_flat = req.body.sirovine_flat;
  
  
  var old_grupa_id =  req.body.old_grupa_id;
  var sirovina_id = req.body.sirovina_id;
  
  // obriši ova dva propertija jer mi ne trabaju kod save grupa
  delete req.body.old_grupa_id;
  delete req.body.sirovina_id;
  

  // UPDATE GRUPACIJA
  Grupa.findOneAndUpdate(
    { _id: id },
    
    /* doslovno radim update svih properija u grupaciji */
    req.body,
    
    
    { new: true }, function(err, grupa) {
      
      var clean_grupa = null;
      
      if (err) {
        console.log(`Error response prilikom promjene UPDATE grupacije`, err);
        res.json({success: false, error: err, msg: `Greška prilikom UPDATE grupacije !!!` })
      } else {
        clean_grupa = global.mng_to_js(grupa);
        res.json({success: true, grupa: clean_grupa })
      };


      Sirov.updateMany( 
        { "grupa": clean_grupa._id },
        { grupa_flat: JSON.stringify(clean_grupa) },
        function(err, updated_sirovs) {
        if (err) { 
         console.log("Greška kod update grupa_flat na svakoj sirovini", err);
        } else { 
          console.log("promjenio sam grupa_flat na svakoj sirovini");
        };
      });       
      
      
      if ( old_grupa_id ) {
        
        // obriši ovu sirovinu u staroj grupi
        Grupa.findOneAndUpdate(
          { _id: old_grupa_id },
          { $pull: { sirovine: { _id: sirovina_id } } },
          { new: true }, function(err, old_grupa) {
            
            if (err) {
              console.log(`Error prilikom brisanja sirovine u STAROJ grupi`, err);
              return;
            }; 
            console.log(`Obrisao sam sirovinu iz STARE grupe: ` + mng_to_js(grupa)._id );
            
            var clean_old_grupa = mng_to_js(old_grupa);
                        
            // napravi update od string tj FLAT verzije grupe u svakoj sirovini koja je član te grupe
            Sirov.updateMany(
              { "grupa": clean_old_grupa._id },
              { grupa_flat: JSON.stringify(clean_old_grupa) }, 
              function(err, updated_sirovs) {
              if (err) { 
               console.log("Greška kod update grupa_flat na svakoj sirovini KOJE IMAJU JOŠ IMAJU STARU GRUPU", err);
              } else { 
                console.log("promjenio sam grupa_flat na svakoj sirovini");
              };
            }); 
                        
          }); // kraj  brisanja kompanije unutar grupacije od partnera
        
      }; // kraj ako postoji stari id grupe
      

    }); // kraj od UPDATE od trenutne grupe
  
};


function update_sirov_grupa(clean_sirov) {

  if ( clean_sirov.grupa ) {

    Grupa.findOne(
      { _id: clean_sirov.grupa },
      function(err, grupa) {

        if (err) {
          console.log(`Error prilikom pretrage grupe nakon update sirovine`, err);
          console.log({success: false, error: err, msg: `Error prilikom pretrage grupe nakon update sirovine !!!` });
          return;
        };

        var clean_grupa = global.mng_to_js(grupa);

        $.each( clean_grupa.sirovine, function( s_ind, grupa_sir )  {
          if ( grupa_sir._id == clean_sirov._id ) clean_grupa.sirovine[s_ind].name = clean_sirov.full_naziv;
        });

        clean_grupa.sirovine_flat = JSON.stringify(clean_grupa.sirovine);

        Grupa.findOneAndUpdate(
          { "_id": clean_sirov.grupa },
          clean_grupa,
          function(err, updated_grupa) {
          if (err) { 
            console.log("Greška kod update grupe nakon update sirovine", err);
          } else { 
            console.log("Napravio upadate imena sirovine unutar grupa nakon update sirovine");
          };

        });

      }); // find tu grupu

  }; // ako sirovina ima grupu
  
};


exports.save_sirov = function(req, res) {

  var sirov_obj = req.body;
  
  sirov_obj.grupa_flat = sirov_obj.grupa ? JSON.stringify(sirov_obj.grupa) : null;
  
  // spremi samo id od grupe jer to radim POPULATE pa mi ne treba cijeli objekt
  sirov_obj.grupa = sirov_obj.grupa ? sirov_obj.grupa._id : null; 
  
  
  if ( sirov_obj._id ) {
    
    Sirov.findOne({ _id: sirov_obj._id }, function(err, sirov) {

      if (err) {
        res.json({ success: false, msg: 'Greška kod FIND ONE Sirov prilikom save Sirov!', err: err });
        return;
      };

      if (!sirov) {
        // if no user
        res.json({ success: false, msg: 'Ažuriranje Robe nije uspjelo.<br>Sustav nije pronašao robu u bazi!' });
      } 
      else {

        
        var DB_clean_sirov = global.mng_to_js(sirov);
        
        
        // ------------------------------------------------IZDVAJAM STATUSE RECORDE I LOCATIONS --------------------------------
        // ------------------------------------------------ u odvojene kopije arraja
        var DB_statuses = DB_clean_sirov.statuses || [];
        var DB_records = DB_clean_sirov.records || [];
        var DB_locations = DB_clean_sirov.locations || [];
        
        DB_statuses = global.cit_deep(DB_statuses);
        DB_records = global.cit_deep(DB_records);
        DB_locations = global.cit_deep(DB_locations);
        
        // prepiši nove podatke
        var clean_sirov = {...DB_clean_sirov, ...sirov_obj };
        
        
        // ------------------------------ UBACUJEM NOVE RECORDSE AKO POSTOJE U REQUESTU U KOPIJU KOJI SAM MALO PRIJE NAPRAVIO 
        if ( sirov_obj.records && sirov_obj.records.length > 0 ) {
          if ( !DB_records ) DB_records = [];
          $.each( sirov_obj.records, function(r_ind, req_rec) {
            if ( req_rec ) DB_records = global.upsert_item( DB_records, req_rec, `sifra`); 
          });
        };
        
        clean_sirov.records = DB_records;
        // --------END---------------------- UBACUJEM NOVE RECORDSE AKO POSTOJE U REQUESTU
        
        
        
        
        // ------------------------------ UBACUJEM NOVE STATUSES AKO POSTOJE U REQUESTU U KOPIJU KOJI SAM MALO PRIJE NAPRAVIO 
        if ( !DB_statuses ) DB_statuses = [];
        
        if ( sirov_obj.statuses && sirov_obj.statuses.length > 0 ) {
          $.each( sirov_obj.statuses, function(s_ind, local_status) {
            if ( local_status ) DB_statuses = global.upsert_item( DB_statuses, local_status, `_id`); 
          });
        };
        
        clean_sirov.statuses = DB_statuses;
        // -------------END----------------- UBACUJEM NOVE STATUSES AKO POSTOJE U REQUESTU
        
        
        
        // ------------------------------ UBACUJEM NOVE LOCATIONS AKO POSTOJE U REQUESTU U KOPIJU KOJI SAM MALO PRIJE NAPRAVIO 
        if ( !DB_locations ) DB_locations = [];
        
        if ( sirov_obj.locations && sirov_obj.locations.length > 0 ) {
          $.each( sirov_obj.locations, function(loc_ind, local_location) {
            if ( local_location ) DB_locations = global.upsert_item( DB_locations, local_location, `sifra`); 
          });
        };
        
        
        if ( 
          sirov_obj.delete_locations 
          && 
          sirov_obj.delete_locations.length > 0
          &&
          DB_locations.length > 0
          &&
          sirov_obj.locations
          &&
          sirov_obj.locations.length > 0
        
        ) {
            
            $.each( sirov_obj.delete_locations, function( del_ind, del_loc ) {
              var loc;
              for ( loc = DB_locations.length-1; loc>=0; loc-- ) {
                // pronađem u bazi lokaciju koju trebam obrisati
                if ( DB_locations[loc].sifra == del_loc.sifra ) {
                  DB_locations.splice(loc, 1);
                };
                
              };
            });
             
        };
        
        clean_sirov.locations = DB_locations;
        // --------------END---------------- UBACUJEM NOVE LOCATIONS AKO POSTOJE U REQUESTU
        
        
        
        Sirov.findOneAndUpdate(
        { _id: clean_sirov._id },
        clean_sirov,
        { new: true }, function(err, sirov) {

          if (err) {
            console.log(`Error response prilikom UPDATE ROBE`, err);
            res.json({success: false, error: err, msg: `Greška prilikom UPDATE ROBE !!!`, err: err })
            return;
          };

          var clean_sirov = global.mng_to_js(sirov);
          res.json({success: true, sirov: clean_sirov });
          
          update_sirov_grupa(clean_sirov);
          

        }); 
        
        
      }; // kraj ifa ako je našao sirovine po idiju

    }); // end of Sirov.findOne
    
    

    // kraj ako je UPDATE
    
  } 
  else {
    
    /* bezveze uzimam sirovs koji nije null a nikad nije null :P -----> to je dummy query samo da mogu napraviti inc */
    Counter.findOneAndUpdate({ sirovs: { $ne: null } }, { $inc: { sirovs: 1 } }, { "new": true })
    .exec(function(err, counter) {
      if (err) {
          res.json({ 
            success: false, 
            msg: "Sirovina nije spremljena! Counter problem.",
            user_number: null
          });
        return;
      };  

      
      var clean_count = mng_to_js(counter);
      
      var new_sirov_sifra = "SIR"+clean_count.sirovs + ".";

      // ovo je specijalna sifra za alate !!!
      if ( sirov_obj.klasa_sirovine && sirov_obj.klasa_sirovine.sifra == `KS5` ) {
        new_sirov_sifra = "ALA"+clean_count.sirovs + ".";
      };
      
      sirov_obj.sirovina_sifra = new_sirov_sifra;
      
      
      
      // --------------------------------------------------------------------------
      var new_sirov = new Sirov(sirov_obj);
      // save to DB
      new_sirov.save(function(err, sirov) {
        if (err) {
          console.log(`Error response prilikom SAVE sirovina`, err);
          res.json({success: false, error: err });
          return;
        };
        var clean_sirov = global.mng_to_js(sirov);
        res.json({success: true, sirov: clean_sirov });

      }); // kraj save new sirov
      
    // --------------------------------------------------------------------------  

    }); // kraj countera
    
    
  }; // kraj ako je SAVE 
    

};
// KRAJ save sirov !!!!


global.create_multiple_sirovs = function(sir_index, sir_object) {

    var sirovina_sifra_num = sir_index + 5010;
  
    sir_object.sirovina_sifra = "SIR" + sirovina_sifra_num;
  
  
    $.each( sir_object, function( key, value ) {
      
      // ako je objekt ili array onda ga flataj
      if ( $.isPlainObject(value) || $.isArray(value) ) {
        sir_object[key+'_flat'] = JSON.stringify(value);
      };
    });
  
  
    var new_sir = new Sirov(sir_object);
    // save to DB
    new_sir.save(function(err, sirovina) {

      if (err) {
        console.error(err);
      };

      // create a token for new user !!!!

      var sir_copy = global.mng_to_js(sirovina);

      console.log(`SIROVINA ` + sir_copy.naziv + ` je spremljena. Ukupno: ` + (sir_index+1) + `/` + global.novi_repro.length  + " SIROVINA ŠIFRA JE: " + sir_copy.sirovina_sifra );


      if ( (sir_index+1) < global.novi_repro.length ) {
        global.create_multiple_sirovs( sir_index+1, global.novi_repro[sir_index+1] );
      } else {
        console.log(`SPREMIO SVE BULK REPRO !!!!!`); 
      };

    }); // end of user save  



}; // end of create_multiple_sirovs


function save_records( arg_records, record_index, req, res ) {
  
  Sirov.findOneAndUpdate(
    { _id: arg_records[record_index].sirov_id },
    { $addToSet: { records: arg_records[record_index] } },
    {new: true},
    function(err, sirov) {

      if (err) {
        console.log(`Error in mongodb prilikom UPDATE RECORDS u Sirovini ID: ${arg_records[record_index].sirov_id}`, err);
        res.json({success: false, error: err, msg: `Error in mongodb prilikom UPDATE RECORDS u Sirovini ID: ${arg_records[record_index].sirov_id}` })
        return;
      };

      console.log( `Spremljen RECORD za sirovinu ID: ` + arg_records[record_index].sirov_id  );
      
      if ( record_index < arg_records.length - 1 ) {
        save_records( arg_records, record_index + 1, req, res );
      }
      else {
        
        res.json( { success: true, record_count: arg_records.length });
        
      };
       
    });
  
  
};
exports.save_sirov_records = function(req, res) {

  var records = req.body;
  save_records( records, 0, req, res );
  
};


function save_storno_records( arg_sirovs, sir_index, req, res, arg_records ) {
 
  // prvo pronadji o kojem se itemu radi iz requesta:
  /*
  [
    { sifra: record.sifra, sirov_id: record.sirov_id },
    { sifra: record.sifra, sirov_id: record.sirov_id },
    itd
  ]
  
  */
  var find_record = global.find_item(arg_records, arg_sirovs[sir_index]._id, `sirov_id` );
  
  // uzmi samo sifru
  var find_record_sifra = find_record ? find_record.sifra : null;
  
  // onda pronadji točan record u trenutnoj sirovini ( koji sam upravo napravi UPDATE TJ PRETVORIO SAM GA U STORNO )
  var stornirani_record = global.find_item(arg_sirovs[sir_index].records, find_record_sifra, `sifra` );
  
  Sirov.findOneAndUpdate(
    { _id: arg_sirovs[sir_index]._id, "records.sifra": find_record_sifra },
    { $set: { "records.$": stornirani_record } },
    {new: true},
    function(err, sirov) {

      if (err) {
        console.log(`Error in mongodb prilikom UPDATE RECORDS u Sirovini ID: ${arg_sirovs[sir_index]._id}`, err);
        res.json({success: false, error: err, msg: `Error in mongodb prilikom UPDATE RECORDS u Sirovini ID: ${arg_sirovs[sir_index]._id}` });
        return;
      };

      console.log( `Spremljen RECORD za sirovinu ID: ` + arg_sirovs[sir_index]._id  );
      
      if ( sir_index < arg_sirovs.length - 1 ) {
        save_storno_records( arg_sirovs, sir_index + 1, req, res, arg_records );
      }
      else {
        res.json( { success: true, record_count: arg_sirovs.length });
      };
       
    });
};



exports.storno_sirov_records = function(req, res) {

  var arg_records = req.body;
  
  /*
  PRIMJER KAKO IZGLEDA JEDAN RECORD:
  { sifra: record.sifra, sirov_id: record.sirov_id }
  
  */

  var sirov_ids = [];
  $.each(arg_records, function(r_ind, record) {
    sirov_ids.push(record.sirov_id);
  });
  
  
  Sirov.find( { _id: { $in: sirov_ids } })
  .exec(function(err, sirovs) {

    if (err) {

      console.log(err);
      console.log(`Error in mongodb prilikom PRETRAGE SIROVINA KOJE TREBA STORNIRATI`, err);
      res.json({success: false, error: err, msg: `Error in mongodb prilikom PRETRAGE SIROVINA KOJE TREBA STORNIRATI` });

    } else {

      var clean_sirovs_arr = global.mng_to_js_array(sirovs);
      
      $.each( arg_records, function(r_ind, record) {
        
        $.each( clean_sirovs_arr, function(s_ind, clean_sir) {
          
          
          // prvo nadji točnu sirovinu od pripadajućeg recorda
          if ( record.sirov_id == clean_sir._id ) {
            
            
            $.each(clean_sir.records, function(rec_ind, sirov_rec) {
            
              // pronadji u toj sirovini točno taj record 
              if ( sirov_rec.sifra == record.sifra ) {
                
                var all_reserv_types = [ `pk_kolicina`, `nk_kolicina`, `rn_kolicina`, `order_kolicina` ]; 

                // loop po svim tipovima reservacija 
                // i ako pronađe da je jedna od njih različita od null
                $.each( all_reserv_types, function(k_ind, reserv_type) {
                  
                  if ( clean_sirovs_arr[s_ind].records[rec_ind][reserv_type] ) {
                    
                    // UVIJE JE  POPUNJENA SAMO JEDNA VRSTA ZAPISANA 
                    // SVE OSTALE SU NULL !!!!
                    // negativna vrijednost od trenutne količine
                    clean_sirovs_arr[s_ind].records[rec_ind].storno_kolicina = -1 * clean_sirovs_arr[s_ind].records[rec_ind][reserv_type]; 
                    clean_sirovs_arr[s_ind].records[rec_ind].storno_time = Date.now();
                    
                  }; // ako nije null onda za taj tip recorda napravi storno !!! da bude ista  kolicina ali minus
                  
                }); // pronadji tip recorda koji nije null 
                
              }; // jesam li nasao pravi record objekt po sifri iz recorda u req
              
            }); // loop po sivm recordima u sirovini
            
          }; // nasao sirovinu za curr record u loopu
          
        }); // loop po svim sirovinama koje sam dobio od idijeva iz recorda
        
      }); // loop po svim recordima iz req.body
      
      save_storno_records( clean_sirovs_arr, 0, req, res, arg_records );

    };


  });
  
  
  
};



function insert_new_record(arg_record, sirov_id) {
  
  
  return new Promise( function(resolve, reject) {
  
    Sirov.findOneAndUpdate(
      { _id: sirov_id }, 
      { $addToSet: { records: arg_record } },
      { new: true }, function(update_err, updated_sirov) {

      if (update_err) {
        resolve({ success: false, msg: 'Greška kod UPDATE novog zapisa u sirovini ' + clean_sirov.sirovina_sifra, err: update_err });
        return;
      };

      var clean_updated_sirov = global.mng_to_js(updated_sirov);

     resolve({ success: true, sirov: true, result: arg_record });  

    });    // kraj update records in sirov

  }); // kraj promisa
  
};



exports.upsert_sirov_record = function(req, res) {

  var sirov_record = req.body;
  
  var sirov_id = sirov_record.sirov_id;
  
  // prepisujem sirov_id u sir_id jer taj property očekujem u funkciji update records() 
  // ----> tu func koristim na više mjesta 
  // sirov_id i sir_id je zaprvo ISTO !!!!!!!!!1
  
  
  sirov_record.sir_id = sirov_id;
  
  
  
  // delete sirov_record.sirov_id;
  
  Sirov.findOne(
  { _id: sirov_id },
  async function(err, sirov) {

    if (err) {
      
      res.json({ 
        success: false,
        msg: ('U bazi nije pronađena Roba s idjem : ' + sirov_id ),
      });
      
      return;
    };
    
    
    var clean_sirov = global.mng_to_js(sirov);
    
    if ( !clean_sirov.records ) clean_sirov.records = [];
    clean_sirov.records = global.upsert_item( clean_sirov.records, sirov_record, `sifra` );
    
    
    
    // UBACUJEM NOVI RECORD KOJI SAM DOBIO U req body
    var new_record = await insert_new_record( sirov_record, sirov_id );
    
    var sklad_kolicina_update = await update_sklad_kolicina( sirov_record );  
    
    
    // SVAKI NOVI RECORD KOJI DODJE AKO IMA PREVIOUS ONDA TAJ RECORD MORAM STORNIRATI !!
    // SVAKI NOVI RECORD KOJI DODJE AKO IMA PREVIOUS ONDA TAJ RECORD MORAM STORNIRATI !!
    
    var updated_prev_records_arr = [];
    
    
    var curr_record = sirov_record;
    
    $.each( clean_sirov.records, function(r_ind, rec) {
     
      // var curr_record = rec;
      var search_for = ``;
      var kolicina_minus = ``;
      
      if ( curr_record.type == "order_dobav" ) search_for = `offer_dobav`; 
      if ( curr_record.type == "in_record" ) search_for = `order_dobav`; 
      
      if ( curr_record.type == "order_dobav" ) kolicina_minus = `order_kolicina`;
      if ( curr_record.type == "in_record" ) kolicina_minus = `order_kolicina`;
      
      
      if ( curr_record.type == "order_reserv" ) search_for = `offer_reserv`;
      if ( curr_record.type == "radni_nalog" ) search_for = `order_reserv`;
      
      if ( curr_record.type == "order_reserv" ) kolicina_minus = `pk_kolicina`;
      if ( curr_record.type == "radni_nalog" ) kolicina_minus = `nk_kolicina`;
      
      
      $.each( clean_sirov.records, function(old_ind, old_rec) {
        
        if ( 
          
          curr_record.hasOwnProperty('doc_parent') // ovo vrijedi samo za order dobav i offer dobav i in_record i in_pro_record
          &&
          old_rec.type == search_for 
          &&
          !old_rec.storno_time 
          &&
          old_rec.doc_sifra == curr_record.doc_parent.doc_sifra
          
        ) {
          
          // storniraj prethodni record
          clean_sirov.records[old_ind].storno_kolicina = -1 * (clean_sirov.records[old_ind][kolicina_minus] || 1);
          // dodaje storno vrijeme u prethodnom recordu
          clean_sirov.records[old_ind].storno_time = Date.now();
          
          updated_prev_records_arr.push(clean_sirov.records[old_ind]);
          
          
          clean_sirov.records[r_ind].record_parent = old_rec.record_parent || old_rec;
          updated_prev_records_arr.push(clean_sirov.records[r_ind]);
          
        };
        
        
        
        if ( 
          curr_record.hasOwnProperty('record_kalk') // ovo vrijedi samo za reserv order, reserv offer i radni nalog
          
          &&
          old_rec.type == search_for 
          &&
          !old_rec.storno_time 
          &&
          old_rec.record_kalk && curr_record.record_kalk
          &&
          old_rec.record_kalk.kalk_sifra == curr_record.record_kalk.kalk_sifra
          
        ) {
          
          // storniraj prethodni record
          clean_sirov.records[old_ind].storno_kolicina = -1 * clean_sirov.records[old_ind][kolicina_minus];
          // dodaje storno vrijeme na prethodni record
          clean_sirov.records[old_ind].storno_time = Date.now();
          updated_prev_records_arr.push(clean_sirov.records[old_ind]);
          
          // dodjeli current recordu parent
          clean_sirov.records[r_ind].record_parent = old_rec.record_parent || old_rec;
          updated_prev_records_arr.push(clean_sirov.records[r_ind]);
          
        };
        
        
        
      }); // loop po recordima ALI TRAŽIM stare tj postojeće recorde
      
      
    }); // loop po svim recordima jer provjeravam sve recorde da napravim potreban update (storno, i slično)

    
    if ( updated_prev_records_arr.length > 0 ) {
      update_records(updated_prev_records_arr, 0, req, res);  
    } else {
      console.log("Ne postoji niti jedan STARI record ZA KOJI TREBAM NAPRAVITI UPDATE !!!", err);
      res.json({ success: true, msg: 'Ne postoji niti jedan STARI record ZA KOJI TREBAM NAPRAVITI UPDATE !!! ' });
    };
    
    


  }); // kraj find one sirov
  
};



function get_last_hist_price(find_sir) {
  
  var last_price = find_sir.cijena;
  var last_alt_price = find_sir.alt_cijena;
  
  if ( find_sir.price_hist && $.isArray(find_sir.price_hist) && find_sir.price_hist.length > 0 ) { 

    var criteria = [ 'from_date', 'time' ]; 
    find_sir.price_hist = global.multisort( find_sir.price_hist, criteria );


    // item od najstarijeg datuma u prošlosti ( min datum ) do zadnjeg datuma ( max ) u budućnosti 
    $.each( find_sir.price_hist, function(h_ind, h_price) {

      if ( h_price.from_date <= curr_date ) {

        last_price = h_price.cijena;
        last_alt_price = h_price.alt_cijena;

      };


    });

  }; // kraj ako postoji price hist


  return {
    cijena: last_price,
    alt_cijena: last_alt_price
  }
  
  
};


function update_sklad_kolicina( sirov_record ) {
  
  return new Promise( function(resolve, reject) {

    if ( 
      sirov_record.type == `in_record`
      ||
      sirov_record.type == `in_pro_record`
    )
    {
          
      Sirov.findOne( { _id: sirov_record.sir_id })
      .exec(function(find_sir_err, find_sir) {

        if (find_sir_err) {

            resolve({ 
              success: false, 
              msg: `Došlo je do greške u pretrazi sirovine kako bi ažurirao količinu u skladištu PO PRIMCI za robu ${find_sir.sirovina_sifra}`,
            });

          resolve(null);
          return;
          

        } 
        else { 
          
          
          var curr_date = Date.now();
          
          var last_price = get_last_hist_price(find_sir).cijena;
          var last_alt_price = get_last_hist_price(find_sir).alt_cijena;

          
          var new_price = 0;
          
          // record uvijek ima zamo jednu sirovinu i jednu količinu u arraju
          var primka_kolicina = sirov_record.doc_kolicine[0];
          
          // DODAJ JOŠ -----> udio doc dostava ----> to je vrijednost koja je automatski prepisana iz ND tj doc parenta od ove primke
          var primka_unit_price = (primka_kolicina.price || 0) + ( primka_kolicina.doc_dostava || 0 ); 
          var primka_sum_price =  primka_unit_price * (primka_kolicina.count || 0);
          
          
          var curr_sum_price = (find_sir.sklad_kolicina || 0) * last_price;

          var new_price = (primka_sum_price + curr_sum_price) / (  (primka_kolicina.count || 0) + (find_sir.sklad_kolicina || 0)   );
          
          
          find_sir.price_hist.push({

            spec: {
              docs: [
                  {
                  "sifra": "doc" + global.cit_rand(),
                  "time": curr_date,
                  "link": sirov_record.doc_link,
                  }
                ],
              "sifra": "specs" + global.cit_rand(),
              "komentar": sirov_record.doc_sifra,
              "time": curr_date
            },
            
            "sifra" : "pricehist" + global.cit_rand(),

            "time" : curr_date,
            from_date: sirov_record.record_est_time || null,
            "alt_cijena" : new_price / (find_sir.normativ_omjer || 1),
            "cijena" : new_price
            
          
          });
          


          Sirov.findOneAndUpdate( 
            { _id: sirov_record.sir_id },
            { 
              sklad_kolicina: (find_sir.sklad_kolicina + sirov_record.sklad_kolicina), // povećaj sklad količinu za količinu u recordu
              price_hist: find_sir.price_hist,
            }, 
            { "new": true }
          )
          .exec(function(sir_err, updated_sir) {

            if (sir_err) {

                resolve({ 
                  success: false, 
                  msg: `Došlo je do greške u povećanju količine na skladištu PO PRIMCI za robu ${updated_sir.sirovina_sifra}`,
                });

              resolve(null);
              return;

            } 
            else { 
              // NAPRAVIO USPJEŠAN UPDATE
              resolve(true);
            }

          }); // kraj update sirov sklad količina ako je primka

          
        }; // kraj ako je pronašao sirovinu koju treba update

      }); // kraj FIND sirov

        
    } 
    else {
      // ako nije niti in_record niti in_pro_record !!!!!
      // ONDA VRATI TRUE KAO DA JE SVE OK tako da nastavim dalje
    
      resolve(true);
    };
  
  }); // kraj promisa
  
};


function update_records(updated_records, sir_index, req, res) {


  Sirov.findOneAndUpdate(
    { _id: updated_records[sir_index].sir_id, "records.sifra": updated_records[sir_index].sifra }, 
    { $set: { "records.$": updated_records[sir_index] } }, // ovo je positional operator !!!!!
    { new: true }, 
    async function(update_err, updated_sirov) {

    if (update_err) {
      console.error({ success: false, msg: 'Greška kod UPDATE novih doc sifra i doc link i PDF NAME ' + updated_sirov.sirovina_sifra, err: update_err });
    } else {
      console.log(`UPDATE RECORDA OD SIROVINE ` + updated_sirov.sirovina_sifra + `. Ukupno: ` + (sir_index+1) + `/` + updated_records.length );
    };
      
      

    if ( (sir_index+1) < updated_records.length ) {
      
      update_records( updated_records, sir_index+1, req, res );
      
    } 
    else {
      
      console.log("Napravio update na svakom recordu sa novim doc sifra i pdf name");
      // ako je sve osim primke dobavljača ili primke iz proizvodnje
      res.json({ success: true, updated_sirovs: sir_index+1 });
      
    }; // ako sam napravio update na svim recordima tj sirovinama

  });    // kraj update records in sirov


};


var map_name_to_type =  {

  offer_dobav: `RFQ`,

  ulazna_ponuda: `PONUDA DOBAV.`,
  ulazna_ponuda_ok: `ODOBRENJE PD`,

  order_dobav: `NARUDŽBA DOB.`,
  in_record: `PRIMKA`,
  in_pro_record: `PR.PROIZ.`,
  offer_reserv: `REZERV. PO PK`,
  order_reserv: `REZERV. PO NK`,
  radni_nalog: `RADNI NALOG`,
  ms_record: `MEĐUSKLAD.`,
  utrosak: `UTROŠAK`,
  skart: `ŠKART`,
  pro_record: `PROIZVEDENO`,
  out_record: `OTPREMNICA`,

};

  var curr_time = Date.now();
  


function save_partner_doc(req, res) {
  
  
  
  var curr_time = Date.now();
  
  var doc_sifra = req.body.doc_sifra;
  var doc_type = req.body.doc_type;
  var proj_sifra = req.body.proj_sifra;
  
  var pdf_name = req.body.pdf_name;
  var pdf_time = req.body.pdf_time;
  
  var data_for_doc = req.body.data_for_doc;
  
  
  
  var doc_link = 
`
<a href="/docs/${global.time_path(pdf_time)}/${pdf_name}" target="_blank" onClick="event.stopPropagation();" > 
  ${ map_name_to_type[doc_type] }: ${ doc_sifra }
</a>
`; 
  
  data_for_doc.doc_sifra = doc_sifra;
  data_for_doc.pdf_name = pdf_name;
  data_for_doc.pdf_time = pdf_time;
  data_for_doc.doc_link = doc_link;
  
  
  return new Promise( function(resolve, reject) {
    

    /*
    ------------------------------------------------------------------------------------------------
    trebam zapisati doc u samog partnera 
    to može biti PK ili NK 
    ------------------------------------------------------------------------------------------------
    */
    

    Partner.findOne(
      { _id: data_for_doc.partner_id },
      function(err, partner) {

        if (err) {

          resolve({ 
            success: false,
            msg: ('U bazi nije pronađen partner i ne mogu napraviti update racorda od partnera za ovu ponudu ili NK : ' + data_for_doc.partner_id ),
          });
          return;
        };

        var clean_partner = global.mng_to_js(partner);

        if ( !clean_partner.docs ) clean_partner.docs = [];


        
        clean_partner.docs.push(data_for_doc);
        


        Partner.findOneAndUpdate(
          { _id: data_for_doc.partner_id }, 
          { $set: { docs: clean_partner.docs } },
          { new: true }, function(update_err, updated_partner) {

          if (update_err) {
            res.json({ success: false, msg: 'Greška kod UPDATE novog docs  record u PARTNERU ' + updated_partner.partner_sifra, error: update_err });
            return;
          };

          var clean_updated_partner = global.mng_to_js(updated_partner);

          resolve({ success: true, partner: clean_updated_partner });  

        });    // kraj update docs in partner


      }); // kraj find one sirov
    
    
  }); // kraj promisa
  
  
};



exports.update_doc_sifra_in_sirov_records = async function(req, res) {

  
  
  /*
  data_for_doc: data,
  doc_sifra: data.doc_sifra,
  doc_type: data.doc_type,
  proj_sifra: data.proj_sifra,
  pdf_name: arg_pdf.ime_filea,
  pdf_time: arg_pdf.time,
  
  */
  
  
  var curr_time = Date.now();
  
  var doc_sifra = req.body.doc_sifra;
  var doc_type = req.body.doc_type;
  var proj_sifra = req.body.proj_sifra;
  var pdf_name = req.body.pdf_name;
  var pdf_time = req.body.pdf_time;
  
  
  var data_for_doc = req.body.data_for_doc || null ;
  
  var updated_records = [];
  
  
  var doc_link = 
`
<a href="/docs/${global.time_path(pdf_time)}/${pdf_name}" target="_blank" onClick="event.stopPropagation();" > 
  ${ map_name_to_type[doc_type] }: ${ doc_sifra }
</a>
`;  
  
  
  // SAVE NOVI DOC U PARENTU !!!!!
  var partner_docs = await save_partner_doc( req, res );

  
  /*
  ------------------------------------------------------------
    if (
    data_for_doc 
      &&
    (data_for_doc.doc_type == `offer_reserv` || data_for_doc.doc_type == `order_reserv`)
      
    ) {
      // NEMOJ IĆI DALJE JER 
      
      
      
    };
  ------------------------------------------------------------
  */
    
    

  // "records.storno_kolicina": null, /* ne smje biti storniran record */
  // "records.record_est_time": { $gt: curr_time }, /* mora biti est time u budućnosti */
        
  
  Sirov.find( 
    { 
      "records.record_kalk.proj_sifra": proj_sifra, // daj mi bilo koju sirovinu koja ima istu sifru projekta i ima isti record type
      "records.type": doc_type,
      "records.storno_time": null // da nije stornirani record
    },
    function(err, sirovs) {
      
    if (err) { 
      console.log("Greška kod FIND sirovine za upis novih doc sifra i doc link !!!", err);
      res.json({ success: false, msg: 'Greška kod FIND sirovine za upis novih doc sifra i doc link !!! ', error: err });
      
    } else { 
      
      var clean_sirovs = global.mng_to_js_array(sirovs);
  
      var record_count = 0;
    
      if ( clean_sirovs && clean_sirovs.length > 0 ) {
        
        $.each( clean_sirovs, function(s_ind, clean_sir) {
          
          
          if ( clean_sir.records && clean_sir.records.length > 0 ) {
          
            
            // global.multisort( clean_sir.records, [ '!time' ] );
            
            $.each( clean_sir.records, function(r_ind, record) {

              if ( 

                record.record_kalk 
                && 
                record.record_kalk.proj_sifra == proj_sifra  
                &&
                record.type == doc_type                      
                &&
                record.storno_time == null                   
                /*record.record_est_time > curr_time */
              ) {
                record_count += 1;
                clean_sirovs[s_ind].records[r_ind].doc_sifra =  doc_sifra;
                clean_sirovs[s_ind].records[r_ind].doc_link =  doc_link;
                clean_sirovs[s_ind].records[r_ind].pdf_name =  pdf_name;
                clean_sirovs[s_ind].records[r_ind].pdf_time =  pdf_time;
                clean_sirovs[s_ind].records[r_ind].sir_id =  clean_sir._id || clean_sir.sirov_id;
                
                updated_records.push(record);

              }; // ako je nasao record sa project sifrom i doc type i ako je estimated time u budućnosti

            }); // loop po recordima od sirovine
          
          };
          
          
        }); // loop po sirovinama
        
      }; // ako je nasao neke sirovine
      
      
      
      if ( record_count > 0 ) {
        update_records(updated_records, 0, req, res);  
      } else {
        console.log("Ne postoji niti jedan record rezervacije za ovaj dokument !!!", err);
        res.json({ success: true, msg: 'Ne postoji niti jedan record rezervacije za ovaj dokument!!! ' });
      };
 
      
    }; // ako je našao sirov
      
  });
  
  
};





exports.offer_reserv_records = function(req, res) {

  var sirov_records = req.body;
  
  
  
  $.each( sirov_records, function( r_ind, record ) {
    
  });
  
  var sirov_id = sirov_record.sirov_id;
  delete sirov_record.sirov_id;
  
  Sirov.findOne(
  { _id: sirov_id },
  function(err, sirov) {

    if (err) {
      
      res.json({ 
        success: false,
        msg: ('U bazi nije pronađena Roba s idjem : ' + sirov_id ),
      });
      
      return;
    };
    
    
    var clean_sirov = global.mng_to_js(sirov);
    
    if ( !clean_sirov.records ) clean_sirov.records = [];
    clean_sirov.records = global.upsert_item(clean_sirov.records, sirov_record, `sifra` );
    
    Sirov.findOneAndUpdate(
      { _id: sirov_id }, 
      { $set: { records: clean_sirov.records } },
      { new: true }, function(update_err, updated_sirov) {

      if (update_err) {
        res.json({ success: false, msg: 'Greška kod UPDATE novog zapisa u sirovini ' + updated_sirov.sirovina_sifra, err: update_err });
        return;
      };
    
      var clean_updated_sirov = global.mng_to_js(updated_sirov);
        
      res.json({ success: true, sirov: clean_updated_sirov });  
        
    });    // kraj update records in sirov
    

  }); // kraj find one sirov
  
};


exports.get_last_sirov_records = function(req, res) {

  var sirov_records = req.body;
  
  
  
  return;
  
  
  
  var sirov_records_ids = [];
  
  $.each( sirov_records, function( r_ind, record ) {
    
    
    
  });
  
  
  var sirov_id = sirov_record.sirov_id;
  delete sirov_record.sirov_id;
  
  Sirov.findOne(
  { _id: sirov_id },
  function(err, sirov) {

    if (err) {
      
      res.json({ 
        success: false,
        msg: ('U bazi nije pronađena Roba s idjem : ' + sirov_id ),
      });
      
      return;
    };
    
    
    var clean_sirov = global.mng_to_js(sirov);
    
    if ( !clean_sirov.records ) clean_sirov.records = [];
    clean_sirov.records = global.upsert_item( clean_sirov.records, sirov_record, `sifra` );
    
    Sirov.findOneAndUpdate(
      { _id: sirov_id }, 
      { $set: { records: clean_sirov.records } },
      { new: true }, function(update_err, updated_sirov) {

      if (update_err) {
        res.json({ success: false, msg: 'Greška kod UPDATE novog zapisa u sirovini ' + updated_sirov.sirovina_sifra, err: update_err });
        return;
      };
    
      var clean_updated_sirov = global.mng_to_js(updated_sirov);
        
      res.json({ success: true, sirov: clean_updated_sirov });  
        
    });    // kraj update records in sirov
    

  }); // kraj find one sirov
  
};


exports.get_alat_shelfs = function(req, res) {
  

  AlatShelf.findOne(
    { _id: "626da455f19beaade5b7cb34" }, // uvijek je isti _id
    function(err, alat_location) {
      
    
      if (err) {
        console.log(`Error response prilikom GET ALAT SHELFS`, err);
        res.json({success: false, error: err, msg: `Greška prilikom GET ALAT SHELFS !!!` })
        return;
      };

      var clean_alat_location = global.mng_to_js(alat_location);

      res.json({success: true, alat_locations: clean_alat_location });

  }); // end of find alat shelf document
  
};


exports.update_alat_shelfs = function(req, res) {

  var new_alat_location = req.body;
  
  
  AlatShelf.findOne(
    { _id: "626da455f19beaade5b7cb34" }, // uvijek je isti _id
    function(err, alat_location) {
      
    
      if (err) {
        console.log(`Error response prilikom GET ALAT SHELFS`, err);
        res.json({success: false, error: err, msg: `Greška prilikom GET ALAT SHELFS !!!` })
        return;
      };

      var clean_alat_location = global.mng_to_js(alat_location);

      var curr_shelf = null;
      var ne_stane = null;
      
      // PRVOI PROVJERI JEL ALAT NA ISTOM MJESTU !!!
      var same_shelf = false;
      $.each( clean_alat_location.shelfs, function(shelf_ind, shelf ) {
        
        if ( shelf.sifra == new_alat_location.shelf ) {
          if ( !shelf.alati ) clean_alat_location.shelfs[shelf_ind].alati = [];
          
          $.each( shelf.alati, function(alat_ind, alat ) {
            
            if ( alat._id == new_alat_location.alat_id ) {
              // ako već postoji ovaj alat unutar ove police onda samo napravi update šifre i OLD šifre za svaki slučaj !!!!
              clean_alat_location.shelfs[shelf_ind].alati[alat_ind].sifra = new_alat_location.alat_sifra;
              clean_alat_location.shelfs[shelf_ind].alati[alat_ind].old_sifra = new_alat_location.alat_old_sifra;
              same_shelf = true;
            };
            
          });
        };
        
      });
      
      
      if ( same_shelf == false ) {
        
        // AKO NIJE NA ISTOM MJESTU ----> PROVJERi JEL U NEKOJ DRUGOJ POLICI !!!!
        $.each( clean_alat_location.shelfs, function(shelf_ind, shelf ) {
          
          if ( !shelf.alati ) clean_alat_location.shelfs[shelf_ind].alati = [];
          
          $.each( shelf.alati, function(alat_ind, alat ) {
            
            if ( alat._id == new_alat_location.alat_id ) {
              
              curr_shelf = shelf.sifra;
              
              if ( curr_shelf !== new_alat_location.shelf ) {
                // AKO TRENUTNA POLICA NIJE ISTA KAO OVA IS REQUESTA ONDA SMANJI COUNT I POVEĆAJ FREE
                clean_alat_location.shelfs[shelf_ind].count -= 1;
                clean_alat_location.shelfs[shelf_ind].free += 1;
                // I ZATIM OBRIŠI TAJ ALAT SA TE POLICE !!
                clean_alat_location.shelfs[shelf_ind].alati =  global.delete_item( clean_alat_location.shelfs[shelf_ind].alati, new_alat_location.alat_id, `_id` );
                
              };        
              
            };
            
          });

        });
        
        
        
        $.each( clean_alat_location.shelfs, function(shelf_ind, shelf ) {

          // prondađi policu koja je u requestu
          if ( shelf.sifra == new_alat_location.shelf ) {
            if ( !shelf.alati ) clean_alat_location.shelfs[shelf_ind].alati = [];
            // ako sam prešao maximum !!!
            if ( shelf.max < shelf.count + 1 ) {
              ne_stane = true;
            }
            else {
              
              clean_alat_location.shelfs[shelf_ind].count += 1;
              clean_alat_location.shelfs[shelf_ind].free -= 1;
              
              // push ovaj alat u ovu policu
              clean_alat_location.shelfs[shelf_ind].alati.push({
                
                _id: new_alat_location.alat_id,
                sifra: new_alat_location.alat_sifra || null,
                old_sifra: new_alat_location.alat_old_sifra || null,
                
                time: new_alat_location.time,
                user_id: new_alat_location.user_id,
                user_number: new_alat_location.user_number,
                full_name: new_alat_location.full_name,
                email: new_alat_location.email,
                
              });
              
            }; // ako nije prešao max
            
          }; // našao je policu iz req

        }); // loop po policama
        
        
      }; // ako polica nije na istom mjestu
      

      clean_alat_location.user_edited = { 
        _id: new_alat_location._user_id,
        user_number: new_alat_location.user_number,
        full_name: new_alat_location.full_name,
        email: new_alat_location.email
      };
      
      clean_alat_location.edited = Date.now();

      AlatShelf.findOneAndUpdate(
      { _id: "626da455f19beaade5b7cb34" },
      clean_alat_location,
      { new: true }, function(err, saved_alat_location) {

        if (err) {
          console.log(`Error response prilikom UPDATE lokacije alata`, err);
          res.json({success: false, error: err, msg: `Greška prilikom UPDATE lokacije alata !!!` })
          return;
        };

        var clean_alat_location = global.mng_to_js(saved_alat_location);
        res.json({success: true, alat_locations: clean_alat_location });

      }); 

  }); // end of find alat shelf document
  
  
  

};


global.get_sirovs_count = async function() {

  var count = await global.get_doc_count(Sirov, {});
  console.log(`----------------`, count);
  
};



exports.get_new_palet_sifre = function(req, res) {

  
  var sifre_count = req.body.sifre_count || 1;
  
  PaletSifra.findOneAndUpdate( 
    { _id: { $ne: null } },
    { $inc: { counter: sifre_count } },
    { "new": true }
  )
  .exec(function(update_err, updated_doc) {
    
    if (update_err) {
        res.json({ 
          success: false, 
          msg: "Sifra pakiranja / palete nije spremljena! Counter problem.",
        });
      return;
    };  
    var doc = global.mng_to_js(updated_doc);
    
    var new_sifre_arr = [];
    var i
    for( i = doc.counter; i > doc.counter-sifre_count; i-- ) { // idem unazad jer je sada counter već updated pa ne znam koji je bio početni counter - zato idem unazad
      new_sifre_arr.push( doc.sifre[ i ] );
    };
    
    res.json({ success: true, sifre: new_sifre_arr });

  }); // kraj updateOne    
  
  
};



/*
global.run_update_sirovs = function(clean_sirovs_arr, data_index) {

  var kvaliteta_mix = null;
  var kontra_tok_x_tok = null;
  var val_x_kontraval = null;

  if ( 
    clean_sirovs_arr[data_index].kvaliteta_1 
    && 
    clean_sirovs_arr[data_index].kvaliteta_2
  ) {

    kvaliteta_mix = clean_sirovs_arr[data_index].kvaliteta_1 + "/" + clean_sirovs_arr[data_index].kvaliteta_2;

  };
  
  if ( 
      clean_sirovs_arr[data_index].kontra_tok !== null
      && 
      clean_sirovs_arr[data_index].tok !== null
    ) {

      kontra_tok_x_tok = "P" + clean_sirovs_arr[data_index].kontra_tok + "X" + clean_sirovs_arr[data_index].tok;
    };
    
    
    if ( 
      clean_sirovs_arr[data_index].po_valu !== null
      && 
      clean_sirovs_arr[data_index].po_kontravalu !== null
    ) {

      val_x_kontraval = clean_sirovs_arr[data_index].po_valu + "X" + clean_sirovs_arr[data_index].po_kontravalu;
    };
  
  
  
  
  
  

  Sirov.findOneAndUpdate(
    { _id: clean_sirovs_arr[data_index]._id },
    { kvaliteta_mix: kvaliteta_mix, kontra_tok_x_tok: kontra_tok_x_tok, val_x_kontraval: val_x_kontraval },
    { new: true, upsert: true }, function(err, new_sirov) {

      if (err) {
        console.log(`Error kod upisivanja kvaliteta_mix za  ${ clean_sirovs_arr[data_index].full_naziv } `, err);
      };

      console.log(`Sirovina ` + clean_sirovs_arr[data_index].full_naziv + ` je updated. Ukupno: ` + (data_index+1) + `/` + clean_sirovs_arr.length );

      if ( (data_index+1) < clean_sirovs_arr.length ) {
        global.run_update_sirovs( clean_sirovs_arr, data_index+1 );
      } else {
        console.log(`NAPRAVIO UPDATE NA SVIM SIROVINAMA !!!!!`);            
      };      

    });

  
  
};
*/

/*

global.run_update_sirovs = function(clean_sirovs_arr, data_index) {


  var new_sifra = clean_sirovs_arr[data_index].sirovina_sifra.replace( ".", "" );

  Sirov.findOneAndUpdate(
    { _id: clean_sirovs_arr[data_index]._id },
    { sirovina_sifra: new_sifra },
    { new: true, upsert: true }, function(err, new_sirov) {

      if (err) {
        console.log(`Error kod upisivanja sifre s točkom za  ${ clean_sirovs_arr[data_index].full_naziv } `, err);
      };

      console.log(`Sirovina ` + clean_sirovs_arr[data_index].full_naziv + ` je updated. Ukupno: ` + (data_index+1) + `/` + clean_sirovs_arr.length );

      if ( (data_index+1) < clean_sirovs_arr.length ) {
        global.run_update_sirovs( clean_sirovs_arr, data_index+1 );
      } else {
        console.log(`NAPRAVIO UPDATE NA SVIM SIROVINAMA !!!!!`);            
      };      

    });

  
  
};

*/

/*


global.run_update_sirovs = function(clean_sirovs_arr, data_index) {

  var sklad_kolicina = ( clean_sirovs_arr[data_index].dostupna_kolicina || 0 ) + ( clean_sirovs_arr[data_index].kolicina || 0 );
  
  var update_data = {
    
    pk_kolicina: 0,
    nk_kolicina: 0,
    order_kolicina: 0,
    sklad_kolicina: sklad_kolicina || 0,
    
    
    kolicina: undefined, // ovak brišem property 
    rezervirana_kolicina: undefined, // ovako brišem property 
    dostupna_kolicina: undefined, // ovako brišem property 
    
  };

  
  if ( clean_sirovs_arr[data_index].sirovina_sifra.indexOf(".") == -1 ) clean_sirovs_arr[data_index].sirovina_sifra += ".";
  
  
  
  
  Sirov.findOneAndUpdate(
    { _id: clean_sirovs_arr[data_index]._id },
    { sirovina_sifra: clean_sirovs_arr[data_index].sirovina_sifra },
    { new: true, upsert: true }, function(err, new_sirov) {

      if (err) {
        console.log(`Error kod upisivanja novih props unutar sirovine  ${ clean_sirovs_arr[data_index].full_naziv } `, err);
      };

      console.log(`Sirovina ` + clean_sirovs_arr[data_index].full_naziv + ` je updated. Ukupno: ` + (data_index+1) + `/` + clean_sirovs_arr.length );

      if ( (data_index+1) < clean_sirovs_arr.length ) {
        global.run_update_sirovs( clean_sirovs_arr, data_index+1 );
      } else {
        console.log(`NAPRAVIO UPDATE NA SVIM SIROVINAMA !!!!!`);            
      };      

    });

  
  
};

*/


// var sirov_filovi_array = require(`./sir_filovi_arr.js`);


function remove_existing_docs(specs, filovi_za_ovaj_alat) {


  if ( specs && specs.length > 0 && filovi_za_ovaj_alat.length > 0 ) {

    $.each(specs, function (spec_ind, spec) {

      if (spec.docs && spec.docs.length > 0) {

        $.each(spec.docs, async function (doc_ind, doc) {

          var link_temp_elem = $(doc.link);
          var file_name = link_temp_elem.text().trim();


          for (i = filovi_za_ovaj_alat.length - 1; i >= 0; i--) {
            
            // ako je isto ime u arrayu za movanje i u postojećem linku
            // onda to znači da već imam taj file u bazi !!!!! ------> i zato gra obrišem iz arraja za ovaj alat(filovi koje trebam ubaciti)
            if (filovi_za_ovaj_alat[i] == file_name) filovi_za_ovaj_alat.splice(i, 1);
          };

        });
      };
    });
    
    
  };

  
  return filovi_za_ovaj_alat;

};


global.move_paths_array = [];
global.run_update_sirovs = function(clean_sirovs_arr, d_index) {

  
  $.each(clean_sirovs_arr, function(data_index, sir) { 
    
    
    var mjesec_bezveze = 0;
    
    if ( data_index < 1000 ) mjesec_bezveze = 0;
    if ( data_index >= 1000 && data_index < 2000 ) mjesec_bezveze = 1;
    if ( data_index >= 2000 && data_index < 3000 ) mjesec_bezveze = 2;
    if ( data_index >= 3000 && data_index < 4000 ) mjesec_bezveze = 3;
    if ( data_index >= 4000 && data_index < 5000 ) mjesec_bezveze = 4;
    if ( data_index >= 5000 && data_index < 6000 ) mjesec_bezveze = 5;
    if ( data_index >= 6000 && data_index < 7000 ) mjesec_bezveze = 6;
    if ( data_index >= 7000 && data_index < 8000 ) mjesec_bezveze = 7;
    if ( data_index >= 8000 ) mjesec_bezveze = 8;
    
    
    var dummy_time = Number( new Date(2022, mjesec_bezveze, 2, 1) );
    
    var yyyy_month_path = global.time_path(dummy_time);
    
    
    var specs = clean_sirovs_arr[data_index].specs;
    
    if ( !specs ) specs = [];
    
    var stari_naziv = clean_sirovs_arr[data_index].stari_naziv;
    
    var filovi_za_ovaj_alat = [];
    
    // prvo napuni arrasa filovima za ubacivanje u ovaj alat
    $.each(global.filovi_arr, function(f_ind, file_obj) {
      if ( file_obj.sifra == stari_naziv.trim() ) filovi_za_ovaj_alat.push(file_obj.name); // ubacujem samo stringove imena filova
    });
    
    
    // zatim removaj sve filove koji već postoje u tom alatu
    filovi_za_ovaj_alat = remove_existing_docs(specs, filovi_za_ovaj_alat);
    

    /*
    if ( specs && $.isArray(specs) && specs.length > 0 ) {

      $.each(specs, function(spec_ind, spec) {

        if ( spec.docs && $.isArray(spec.docs) && spec.docs.length > 0  ) {

          $.each(spec.docs, async function(doc_ind, doc) {

            if ( doc.link ) {

              var link_temp_elem = $(doc.link);
              var file_name = link_temp_elem.text().trim();


              specs[spec_ind].docs[doc_ind].time = dummy_time;

              var vec_ima_time_path = false;
              if ( doc.link.indexOf(`/2022/`) > -1 ) vec_ima_time_path = true;

              var new_link = doc.link;

              if ( vec_ima_time_path == false ) {
                new_link = doc.link.replace(`href="/docs/`, `href="/docs/${yyyy_month_path}/`);
                specs[spec_ind].docs[doc_ind].link = new_link;

                var doc_path = global.appRoot + `/public/docs/${file_name}`;
                var new_path = global.appRoot + `/public/docs/${yyyy_month_path}/${file_name}`;
                global.move_paths_array.push({ doc_path: doc_path, new_path: new_path, file_name: file_name });
              };



            }; // jel ima doc link i nema move error

          }); // lopp po docs

        }; // jel ima docs i jel nema move error


      }); // loop po specs

    }; // kraj jel uopće ima specs
    */

    
    // ako ima još neke filove za spremiti u bazu
    if ( filovi_za_ovaj_alat.length > 0 ) {

      var new_docs = [];
      $.each( filovi_za_ovaj_alat, function(f_ind, f_name) {

        new_docs.push({
            "sifra" : "doc" + global.cit_rand(),
            "time" : dummy_time,
            "link" : `<a href="/docs/${yyyy_month_path}/${f_name}" target="_blank" onClick="event.stopPropagation();" >${f_name}</a>`
        });

        var doc_path = global.appRoot + `/public/docs/${f_name}`;
        var new_path = global.appRoot + `/public/docs/${yyyy_month_path}/${f_name}`;
        global.move_paths_array.push({ doc_path: doc_path, new_path: new_path, file_name: f_name });


      });

      specs.push({
          "docs" : new_docs,
          "sifra" : "spec" + global.cit_rand(),
          "komentar" : "DODATNE DATOTEKE",
          "time" : new_docs
      });


      clean_sirovs_arr[data_index].specs = specs;

    }; // kraj da li je ostalo što za ubaciti u bazu

  }); // loop po svim sirovinama  
    
  
  console.log(global.move_paths_array);
  
  // global.multiple_mv(global.move_paths_array, 0);
  

  function update_sirov_DB(clean_sirovs_arr, sirov_ind) {
    
    Sirov.findOneAndUpdate(
      { _id: clean_sirovs_arr[sirov_ind]._id },
      { specs: clean_sirovs_arr[sirov_ind].specs },
      { new: true, upsert: true }, function(err, new_sirov) {

        if (err) {
          console.log(`Error kod UPDATE SPECS unutar sirovine  ${ clean_sirovs_arr[sirov_ind].full_naziv } `, err);
        };

        console.log(`Sirovina ` + clean_sirovs_arr[sirov_ind].full_naziv + ` je updated. Ukupno: ` + (sirov_ind+1) + `/` + clean_sirovs_arr.length );

        if ( (sirov_ind+1) < clean_sirovs_arr.length ) {
          update_sirov_DB( clean_sirovs_arr, sirov_ind+1 );
        } else {
          console.log(`NAPRAVIO UPDATE NA SVIM SIROVINAMA  ------> U BAZI !!!!!`);          
          global.multiple_mv(global.move_paths_array, 0);
        };      

      });

  };
  
  update_sirov_DB(clean_sirovs_arr, 0);
  
  
  
}; // kraj run update sirovs


global.update_multiple_sirovs = function(update_data, data_index) {
  
  
  
  global.move_paths_array = [];
  global.mv_errors = [];

  /*
  
    .select({
      "_id": 1,
      "kvaliteta_1": 1,
      "kvaliteta_2": 1,
      full_naziv: 1
    }) 
  
  
  */

  Sirov.find({ "klasa_sirovine.sifra": "KS5" }) // ovo su samo alati
  .exec(function(err, sirovs) {

    if (err) {
      console.log(err);
    } else {
      var clean_sirovs_arr = global.mng_to_js_array(sirovs);
      global.run_update_sirovs( clean_sirovs_arr, data_index);
    };

  });

}; // end of update_multiple_sirovs




function move_file_exec(arg_command, file_name) {
  
  return new Promise( function(resolve, reject) {
   
      exec( arg_command, function(error, stdout, stderr) {

        if (error) {
          console.log(`MOVE FILE COMMAND -----> error: ${error.message}`);
          console.error({
              success: false,
              ime_filea: file_name,
              msg: error.message,
            });
          resolve(null);
          return;
        };

        if (stderr) {
            console.log(`MOVE FILE COMMAND -----> stderr: ${stderr}`);
            console.error({
              success: false,
              ime_filea: file_name,
              msg: stderr
            });
            resolve(null);
            return;    
        };

        console.log(`MOVE FILE COMMAND -----> stdout: ${stdout}`);

        // resolve(compressed_name + `.jpg`);
        
        resolve( true );
        

      }); // kraj EXEC
    
    
    
    
    
  }); // kraj promisa
  
};


global.mv_errors = [];
global.multiple_mv = function(mv_array, index) {


  var move_file_command =  `mv "${mv_array[index].doc_path}" "${mv_array[index].new_path}"`;

  move_file_exec(move_file_command, mv_array[index].file_name)
  .then(
  function(is_moved) {
    
    if( is_moved === null ) {
      global.mv_errors.push(mv_array[index]);
    };
    
    if ( (index+1) < mv_array.length ) {
      global.multiple_mv( mv_array, index+1 ); // movaj sljedeći file !!!!!
    } else {
      console.log(` ------------------------- NAPRAVIO MV SVIH FILOVA !!!!!`);  
      console.log(` OVO SU GREŠKE KOD MV !!!!`);  
      console.log(global.mv_errors);
    };
    
    
  });
  
  
};


global.check_miss_files = function() {
  
  Sirov.find({ "klasa_sirovine.sifra": "KS5" }) // ovo su samo alati
  .exec(function(err, sirovs) {

    if (err) {
      console.log(err);
      
    } else {
      
      var clean_sirovs_arr = global.mng_to_js_array(sirovs);
      
      $.each(clean_sirovs_arr, function(sir_ind, sir) {
        
        
        if ( sir.specs && sir.specs.length > 0 ) {

          $.each( sir.specs, function (spec_ind, spec) {

            if (spec.docs && spec.docs.length > 0) {

              $.each(spec.docs, async function (doc_ind, doc) {

                var link_temp_elem = $(doc.link);
                var file_name = link_temp_elem.text().trim();
                var path = link_temp_elem.attr(`href`);
                

                try {
                  
                  if ( fs.existsSync( global.appRoot + `/public` + path) ) {
                    
                    global.missing_files.push({
                      _id: sir._id,
                      sifra: sir.sirovina_sifra,
                      path: (global.appRoot + `/public` + path),
                      file_name: file_name, 
                      missing: false 
                    });
                    
                  } else {
                     
                    global.missing_files.push({ 
                      _id: sir._id,
                      sifra: sir.sirovina_sifra,
                      path: (global.appRoot + `/public` + path),
                      file_name: file_name,
                      missing: true,
                      err: "NO FILE" 
                    });
                       
                     
                  }
                } catch(err) {
                  
                  global.missing_files.push({
                    _id: sir._id,
                    sifra: sir.sirovina_sifra, 
                    path: (global.appRoot + `/public` + path),
                    file_name: file_name,
                    missing: true,
                    err: err 
                  });
                  
                };


              }); // kraj loopa po docsima u spec
              
            }; // jel postoje docs
            
          }); // loop po specsima

        }; // jel postoje specs
        
      }); // loop po clean sirovinama
      
      
    }; // kraj ako nije error od find mongoose

  }); // kraj exec 
  
};


var replaced_count = 0;

global.replace_miss_files = function() {
  
  
  var sir_ids = [];
  
  $.each(replace_files_arr, function(r_ind, rep_file) {
    sir_ids = global.insert_uniq(sir_ids, rep_file._id );
  });
  
  // { _id: { $in: sir_ids } }
  
  Sirov.find({ _id: { $in: sir_ids } }) // ovo su samo idjevi koje sam izvukao iz replace files arr
  .exec(function(err, sirovs) {

    if (err) {
      console.log(err);
      
    } else {
      
      var clean_sirovs_arr = global.mng_to_js_array(sirovs);
      
      $.each(clean_sirovs_arr, function(sir_ind, sir) {
        
        
        if ( sir.specs && sir.specs.length > 0 ) {

          $.each( sir.specs, function (spec_ind, spec) {

            if (spec.docs && spec.docs.length > 0) {

              $.each(spec.docs, async function (doc_ind, doc) {

                var link_temp_elem = $(doc.link);
                var file_name = link_temp_elem.text().trim();
                var path = link_temp_elem.attr(`href`);
                
                
                $.each(replace_files_arr, function(r_ind, rep_file) {
                  
                  if ( rep_file.path == (global.appRoot+`/public` + path) ) {
                    var relative_replace_path = rep_file.replace_path.replace( global.appRoot+`/public`, ``);
                    
                    clean_sirovs_arr[sir_ind].specs[spec_ind].docs[doc_ind].link = 
                      `<a href="${relative_replace_path}" target="_blank" onClick="event.stopPropagation();" >${file_name}</a>`;
                    
                    replaced_count += 1;
                    replace_files_arr[r_ind].replaced = true;
                    
                    
                  };
                });

              }); // kraj loopa po docsima u spec
              
            }; // jel postoje docs
            
          }); // loop po specsima

        }; // jel postoje specs
        
      }); // loop po clean sirovinama
      
      
      console.log(" ---------------------------------------- koliko treba replaceati: " + replace_files_arr.length );
      console.log(" ---------------------------------------- koliko je replaced: " + replaced_count );
      
      
     function update_sirov_DB(clean_sirovs_arr, sirov_ind) {

        Sirov.findOneAndUpdate(
          { _id: clean_sirovs_arr[sirov_ind]._id },
          { specs: clean_sirovs_arr[sirov_ind].specs },
          { new: true, upsert: true }, function(err, new_sirov) {

            if (err) {
              console.log(`Error kod UPDATE SPECS unutar sirovine  ${ clean_sirovs_arr[sirov_ind].full_naziv } `, err);
            };

            console.log(`Sirovina ` + clean_sirovs_arr[sirov_ind].full_naziv + ` je updated. Ukupno: ` + (sirov_ind+1) + `/` + clean_sirovs_arr.length );

            if ( (sirov_ind+1) < clean_sirovs_arr.length ) {
              update_sirov_DB( clean_sirovs_arr, sirov_ind+1 );
            } else {
              console.log(`NAPRAVIO UPDATE NA SVIM SIROVINAMA  ------> U BAZI !!!!! ` );          
              
            };      

          });

      };
      
      update_sirov_DB(clean_sirovs_arr, 0 );
      
      
      
    }; // kraj ako nije error od find mongoose

  }); // kraj exec 
  
};



global.svi_prirezi_alata = function() {
  
  var pipe_alat = ``;
  
  Sirov.find({ "klasa_sirovine.sifra": "KS5" }) // ovo su samo alati
  .exec(function(err, sirovs) {

    if (err) {
      console.log(err);
    } else {
      var clean_sirovs_arr = global.mng_to_js_array(sirovs);
      
      $.each(clean_sirovs_arr, function(alat_ind, alat) {
        
        pipe_alat += alat.sirovina_sifra + "|" + alat.stari_naziv + "|" + alat.po_valu + "|" + alat.po_kontravalu + "<br>"; 
        
        
      });
      
  
      console.log(pipe_alat);
      
    };

  });  
  
  
};









