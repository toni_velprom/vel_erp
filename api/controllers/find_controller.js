var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var nodemailer = require('nodemailer');
var fs = require('fs')
var path = require("path");

var User = mongoose.model('User');

var jwt = require('jsonwebtoken');

var Project = mongoose.model('Project');
var Product = mongoose.model('Product');
var Status = mongoose.model('Status');

var Sirov = mongoose.model('Sirov');

var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);



global.escape_spec_chars = function(arg_string) {
  
  var spec_chars = [ `.`, `^`, `$`, `*`, `+`, `-`, `?`, `(`, `)`, `[`, `]`, `{`, `}`, `\\`, `|`, `—`, `/` ];
  
  var trazi_string_arr = arg_string.split("");
  
  var new_trazi_string = ``;
  
  $.each( trazi_string_arr, function( s_ind, char) {
    var new_char = char;
    if ( spec_chars.indexOf(char) > -1 ) new_char = `\\` + char;
    
    new_trazi_string += new_char;
  });
  
  // var spec_regex = [ /\./ig, /\^/ig, /\$/ig, /\*/ig, /\+/ig, /\-/ig, /\?/ig, /\(/ig, /\)/ig, /\[/ig, /\]/ig, /\{/ig, /\}/ig, /\//ig, /\|/ig, /\—/ig, ];
  /*
  $.each( spec_regex, function( r_ind, reg) {
    if ( spec_chars[r_ind] !== `\\` ) {
      trazi_string = arg_string.replace(reg, "\\" + spec_chars[r_ind] );
    } else {
      trazi_string = arg_string.replace(reg, "\\\\" );
    };
  });
  */
  
  return new_trazi_string
  
};


global.cit_search_remote = function(req, res, DB_model) {
  
  return new Promise( function( resolve, reject ) {   


    var token = req.headers['x-auth-token'] || null;

    // iznimka  je kada radim request za park places ---_> user ne mora biti ulogiran
    if ( token ) {

      global.check_user_auth(token)
      .then(function( result ) {
        // iznimka  je kada radim request za park places ---_> user ne mora biti ulogiran
        if ( result.success == true  ) {
          // ovo je glavna funkcija za find
          resolve( global.run_cit_find( req, res, result.super_admin, result.user, DB_model ) );
        } else {
          resolve([]);
          return;
        };

      })
      .catch(function(err) {
        resolve([]);
      });

    } else {
      // vrati prazan array ako nema token
      resolve([]);

    };

  }); // kraj promisa

}; // end of cit_search_remote 


function get_non_finish_orders(statuses) {
  
  return new Promise( async function(resolve, reject) {

    if ( statuses && statuses.length > 0 ) {
      
      
      statuses = global.mng_to_js_array(statuses);
      
      var just_order_statuses = [];
      $.each(statuses, function(s_ind, status) {
        // ovo je ZAHTJEV NABAVI ZA SIROVINU
        if ( status.status_tip.sifra == "IS1634121913167369" ) {
          just_order_statuses.push(status);
        };
      });
      
      
      var all_children_query = { elem_parent: { $in: [] } };
      $.each(statuses, function(s_ind, status) {
        all_children_query.elem_parent.$in.push( status.sifra );
      });
      
      
      const all_children_all_statuses = await Status.find(all_children_query).exec();
      
      var all_ch_all_sta_clean = global.mng_to_js_array(all_children_all_statuses);
      

      var just_non_finish_orders = [];

      $.each( just_order_statuses, function(o_ind, order) {

        
        var branch_done = false
        
        $.each( order.status_tip.grana_finish, function(f_index, finish) {
          $.each( all_ch_all_sta_clean, function(c_index, child_status) {
            // ako BILO KOJI CHILD STATUS IMA STATUS TIP KOJI JE  JEDAN OD FINISH OD PARENTA 
            // -----------> onda je grana gotova !!!!
            if (
              child_status.elem_parent == order.sifra // mora biti djete baš od tog ordera
              &&
              child_status.status_tip.sifra == finish.sifra // mora biti jedan od finish statusa
            ) {
              branch_done = true;
            };

          }); // lopp po svakom djetetu od svakog ordera
        }); // loop po svim finish objektima u jednom orderu 
        
        if ( branch_done == false ) just_non_finish_orders.push( order );
        

      }); // loop po svim orderima

      resolve(just_non_finish_orders);
      
    } 
    else {

      resolve(statuses);

    };

  }); // kraj promisa

}; 


function get_entire_branch(parent_sifra) {
  
  return new Promise( async function(resolve, reject) {
    
    var branch_query = {
      $or: [ { sifra: parent_sifra }, { elem_parent: parent_sifra } ] 
    };
    
    try {
      var all_statuses = await Status.find(branch_query).populate([ 'kalk' ]).exec();
      
      var clean_statuses = [];
      if ( all_statuses.length > 0 ) clean_statuses = global.mng_to_js_array(all_statuses);
      resolve( { success: true,  statuses: clean_statuses });
        
      
    }
    catch (err) {
      console.log(err)
      resolve( {success: false,  msg: `Došlo je do greške na serveru prilikom dobivanja svih statusa unutar proizvodnog naloga!!!`, error: err });
    };
    
  });
  
};


function cit_forecast( sirovina, sirov_on_date ) {


  // startna kolicina
  var forecast_kolicina = sirovina.sklad_kolicina;
  var curr_time = Date.now();



  $.each(sirovina.records, function( r_ind, record ) {

    if ( 
      record.record_est_time >= curr_time // novije je od danasnjeg datuma
      &&
      /*
      record.record_est_time <= sirov_on_date // mora biti starije tj manji datum od roka za def tj procjene početka proizvodnje
      &&
      za sada neću gledati rok za definiciju nego ću gledati sve do zadnjeg recorda
      zato što se lako može dogoditi da na primjer jedan radnik reservira robu za 12 mjesec
      to ne znači da ako radnik poslje njega reservira robu za 11 mjesec da može IGNORIRATI OVU RESERVACIJU U 12 mjesecu !!!!!
      */
      
      record.record_est_time <= curr_time + (1000*60*60*24*90) // recorde koji su dalji od 3 mjeseca u budoćnosti NE GLEDAJ
      && 
      !record.storno_time // nije storno !!!
      
      
      // za sada ću ipak uzimati u obzir i rezervacije po ponudi kupca !!!!!!
      // && 
      // !record.pk_kolicina // nije rezervirano po ponudi

    ) {

      // može biti po NK ili po RN
      var minus_kolicina = record.pk_kolicina || record.nk_kolicina || record.rn_kolicina || 0;

      forecast_kolicina = forecast_kolicina - minus_kolicina; 
      // + (record.order_kolicina || 0); -----> NEĆU VIŠE DODAVATI KOLIČINU KOJA DOLAZI PO NARUDŽBI OD DOBAVLJAČA U FORCAST (kao da MRAV uopće ne zna da dolazi roba )
      // na taj način ću zadovoljit Radmilin zahtjev da uopće ne uzimam u obzir dolazak robe u budućnosti
      // što se MRAV-a tiče roba se samo magično pojavi na skladištu :)

    };

  });

  return forecast_kolicina;

};



function filter_statuses(
items,
 
tip_1,
machine_1,
radna_1,
 
tip_2,
machine_2,
radna_2
) {
  
  
  var original_items = global.cit_deep( items );
  

  if ( tip_1 && !machine_1 && !radna_1 ) {


    var filtered_items = [];

    $.each(items, function (item_ind, item) {


      if ( item.kalk && item.kalk.post_kalk && item.kalk.post_kalk[0] && item.kalk.post_kalk[0].procesi ) {

        $.each(item.kalk.post_kalk[0].procesi, function (proc_ind, proc) {
          if ( 
            proc.row_sifra == item.proces_id &&
            proc.action &&
            proc.action.action_radna_stn &&
            proc.action.action_radna_stn.type == tip_1 ) {

            // BITNO !!!!
            // upisujem proces index tako da ako je user izabrao filtere za sljedeći proces da mogu znati koji je proces iza ovog procesa
            item.search_proces_index = proc_ind;
            filtered_items.push(item);

          };

        });

      };



    });

   
    if ( filtered_items.length > 0 ) items = filtered_items;

  };


  if ( machine_1 && !radna_1 ) {

    var filtered_items = [];

    $.each(items, function (item_ind, item) {


      if ( item.kalk && item.kalk.post_kalk && item.kalk.post_kalk[0] && item.kalk.post_kalk[0].procesi ) {

        $.each(item.kalk.post_kalk[0].procesi, function (proc_ind, proc) {
          if ( 
            proc.row_sifra == item.proces_id &&
            proc.action &&
            proc.action.action_radna_stn &&
            proc.action.action_radna_stn.machine_sifra == machine_1 ) {

            // BITNO !!!!
            // upisujem proces index tako da ako je user izabrao filtere za sljedeći proces da mogu znati koji je proces iza ovog procesa
            item.search_proces_index = proc_ind;
            filtered_items.push(item);


          };

        });

      };



    });
    
    if ( filtered_items.length > 0 ) items = filtered_items;

  }; // kraj ako je izabran stroj


  if ( radna_1 ) {

    var filtered_items = [];


    $.each(items, function (item_ind, item) {

      if ( item.kalk && item.kalk.post_kalk && item.kalk.post_kalk[0] && item.kalk.post_kalk[0].procesi ) {

        $.each(item.kalk.post_kalk[0].procesi, function (proc_ind, proc) {
          if ( 
            proc.row_sifra == item.proces_id &&
            proc.action &&
            proc.action.action_radna_stn &&
            proc.action.action_radna_stn.sifra == radna_1 ) {

            // BITNO !!!!
            // upisujem proces index tako da ako je user izabrao filtere za sljedeći proces da mogu znati koji je proces iza ovog procesa
            item.search_proces_index = proc_ind;
            filtered_items.push(item);



          };

        });

      };


    });


    if ( filtered_items.length > 0 ) items = filtered_items;


  }; // kraj ako je izabrana radna stanica 


  // ------------------------------------ FILTER 2 TJ ZA SLJEDEĆI PROCES ------------------------------------
  // ------------------------------------ FILTER 2 TJ ZA SLJEDEĆI PROCES ------------------------------------
  // ------------------------------------ FILTER 2 TJ ZA SLJEDEĆI PROCES ------------------------------------

  
  
  $.each( items, function( f_ind, f_item ) {
  

    if ( tip_2 && !machine_2 && !radna_2 ) {


        var filtered_items = [];

        $.each(original_items, function (orig_ind, original) {


          if ( 
            original.kalk && original.kalk.post_kalk && original.kalk.post_kalk[0] && original.kalk.post_kalk[0].procesi 
            &&
            original.kalk._id == f_item.kalk._id
          ) {

            $.each(original.kalk.post_kalk[0].procesi, function (proc_ind, proc) {
              if ( 
                proc_ind == f_item.search_proces_index + 1
                &&
                proc.row_sifra == original.proces_id &&
                proc.action &&
                proc.action.action_radna_stn &&
                proc.action.action_radna_stn.type == tip_2
  
              ) {


                filtered_items.push(original);

              };

            });

          };



        });

      
        // dodaj filtered items na postojeće iteme
        if ( filtered_items.length > 0 ) items = [ ...items, ...filtered_items ];


    }; // kraj ako je izabran proces tip 2

    if ( machine_2 && !radna_2 ) {



      var filtered_items = [];


      $.each(original_items, function (orig_ind, original) {


        if ( 
          original.kalk && original.kalk.post_kalk && original.kalk.post_kalk[0] && original.kalk.post_kalk[0].procesi 
          &&
          original.kalk._id == f_item.kalk._id

        ) {

          $.each(original.kalk.post_kalk[0].procesi, function (proc_ind, proc) {
            if ( 
              proc_ind == f_item.search_proces_index + 1
              &&
              proc.row_sifra == original.proces_id &&
              proc.action &&
              proc.action.action_radna_stn &&
              proc.action.action_radna_stn.machine_sifra == machine_2
          
            ) {


              filtered_items.push(original);


            };

          });

        };



      });


      // dodaj filtered items na postojeće iteme
      if ( filtered_items.length > 0 ) items = [ ...items, ...filtered_items ];

 

    }; // kraj ako je izabran stroj
    
    if ( radna_2 ) {


      var filtered_items = [];

      $.each(original_items, function (orig_ind, original) {

        if ( 
          original.kalk && original.kalk.post_kalk && original.kalk.post_kalk[0] && original.kalk.post_kalk[0].procesi 
          &&
          original.kalk._id == f_item.kalk._id

        ) {

          $.each(original.kalk.post_kalk[0].procesi, function (proc_ind, proc) {
            if ( 
              proc_ind == f_item.search_proces_index + 1
              &&
              proc.row_sifra == original.proces_id &&
              proc.action &&
              proc.action.action_radna_stn &&
              proc.action.action_radna_stn.sifra == radna_2
              

            ) {


              filtered_items.push(original);


            };

          });

        };



      });


      // dodaj filtered items na postojeće iteme
      if ( filtered_items.length > 0 ) items = [ ...items, ...filtered_items ];




    }; // kraj ako je izabrana radna stanica 2

  });
  
  
  var uniq_items = [];
  
  $.each( items, function(item_ind, item) {
    
    var already_in_uniq = false;
    $.each( uniq_items, function(uniq_ind, uniq) {
      if ( item.sifra == uniq.sifra || item.elem_parent !== null ) already_in_uniq = true;
    });  

    if ( already_in_uniq == false ) uniq_items.push( item );
    
  });

  return uniq_items;

};




global.run_cit_find = function( req, res, super_admin, arg_user, DB_model ) {
  
  return new Promise( function( resolve, reject ) { 

    
    if ( DB_model == null ) {
      resolve({ success: false, msg: 'No such end point for searching.' });
      return;
    };

    /*
    DB_model.estimatedDocumentCount({}, function (err, count) {
      console.log( 'OVO JE BROJ DOKUMENATA U ' + req.originalUrl + ' kolekciji  : '  +  count );

    });
    */


    var sortiraj_po = req.body.sort || null;
    
    var get_count = req.body.get_count || null;
    
    
    if ( sortiraj_po ) {
      
      /* BITNO !!!!  BITNO !!!!   BITNO !!!!   BITNO !!!!   BITNO !!!! */
      // sort koji dođe u requestu je array zato što mi je bitan redosljed sortiranja kada je više polja
      // moram ga pretvoriti u objekt kako bi ga mogao koristiti u mongoose queryju

      var new_sort_obj = {};
      $.each( sortiraj_po, function(sort_ind, sort_obj) {
        // svaki sort obj ima samo jedan prop i value -1 ili 1 !!!1
        $.each( sort_obj, function(sort_key, sort_direction) {
          new_sort_obj[sort_key] = sort_direction;
        });    

      });

      // sada overwrite sortiraj_po da bude novi objekt
      sortiraj_po = new_sort_obj;
    };

    var list_limit = req.body.limit;

    // if ( req.originalUrl == '/search_actions' ) list_limit = 20000;

    var find_query = {};

    var trazi_string = req.body.trazi_string || null;
    // samo ako trazi string NIJE NULL
    if ( trazi_string ) trazi_string = global.escape_spec_chars(trazi_string);
    
    // trazi_string = trazi_string.replace(/\./ig, "\\." );
    
    
    var trazi_polja = [];
    var trazi_array = [];
    var filter_funkcija = req.body.filter || null;
    var filtrirano = null;


    var kreirao_query = false;

    // NEMA querija ( prazan objekt )
    // NEMA search stringa  
    // ----> onda daj SVE tj nema querija !!!!
    if ( 
      
      ( !req.body.query || Object.keys(req.body.query).length == 0 )
      &&
      !trazi_string 
    ) {
      find_query = {};
      kreirao_query = true;
    }; 
    


    // TODO ----> OVO NE KORISTIM ALI ĆE MI KASNIJE TREBATI !!!!!!
    // TODO ----> OVO NE KORISTIM ALI ĆE MI KASNIJE TREBATI !!!!!!
    if ( req.originalUrl == '/search_regs' ) {
      
      req.body.query = {
        $or: [
          { "car_plates.reg": trazi_string.replace(/(?!ć|Ć|č|Č|š|Š|đ|Đ|ž|Ž)\W/g, '').replace(/\_/g, '').toUpperCase() },
          { email: { "$regex": trazi_string, "$options": "i" } }
        ]
      };
    };
    // TODO ----> OVO NE KORISTIM ALI ĆE MI KASNIJE TREBATI !!!!!!
    // TODO ----> OVO NE KORISTIM ALI ĆE MI KASNIJE TREBATI !!!!!!


    // ali ako postoji NEŠTO u query
    // i ako nema traži stringa !!!!!
    // onda je traži po tom queriju
    
    
    
    var sirov_on_date = null;
    if ( req.body.query && req.body.query.hasOwnProperty('on_date') ) {
      // zapiši ovaj datum u varijablu
      sirov_on_date = req.body.query.on_date;
      // obriši ovaj prop tako da ne utječe na pretragu !!!
      delete req.body.query.on_date;
    };
    
    
    
    // -----------------------------------  OVO JE za request za /find_docs -----------------------------------
    
    var get_doc_type = null;
    if ( req.body.query && req.body.query.hasOwnProperty('get_doc_type') ) {
      // zapiši ovaj prop u varijablu
      get_doc_type = req.body.query.get_doc_type;
      // obriši ovaj prop tako da ne utječe na pretragu !!!
      delete req.body.query.get_doc_type;
    };
    
    // -----------------------------------  OVO JE za request za /find_docs -----------------------------------
    var get_doc_sirov_id = null;
    if ( req.body.query && req.body.query.hasOwnProperty('get_doc_sirov_id') ) {
      // zapiši ovaj prop u varijablu
      get_doc_sirov_id = req.body.query.get_doc_sirov_id;
      // obriši ovaj prop tako da ne utječe na pretragu !!!
      delete req.body.query.get_doc_sirov_id;
    };
    
    
    // -----------------------------------  OVO JE za request za /find_sirov_records -----------------------------------
    var find_record_type = null;
    if ( req.body.query && req.body.query.hasOwnProperty('find_record_type') ) {
      // zapiši ovaj prop u varijablu
      find_record_type = req.body.query.find_record_type;
      // obriši ovaj prop tako da ne utječe na pretragu !!!
      delete req.body.query.find_record_type;
    };
    
    
    
    
    
    var filter_proces_tip_1 = null;
    var filter_machine_1 = null;
    var filter_radne_stanice_1 = null;
    
    var filter_proces_tip_2 = null;
    var filter_machine_2 = null;
    var filter_radne_stanice_2 = null;
    

    if (  req.body.query.filter_proces_tip_1 ) { filter_proces_tip_1 = req.body.query.filter_proces_tip_1; delete req.body.query.filter_proces_tip_1; };
    if (  req.body.query.filter_machine_1 ) { filter_machine_1 = req.body.query.filter_machine_1; delete req.body.query.filter_machine_1; };
    if (  req.body.query.filter_radne_stanice_1 ) { filter_radne_stanice_1 = req.body.query.filter_radne_stanice_1; delete req.body.query.filter_radne_stanice_1; };

    if (  req.body.query.filter_proces_tip_2 ) { filter_proces_tip_2 = req.body.query.filter_proces_tip_2; delete req.body.query.filter_proces_tip_2; };
    if (  req.body.query.filter_machine_2 ) { filter_machine_2 = req.body.query.filter_machine_2; delete req.body.query.filter_machine_2; };
    if (  req.body.query.filter_radne_stanice_2 ) { filter_radne_stanice_2 = req.body.query.filter_radne_stanice_2; delete req.body.query.filter_radne_stanice_2; };
    
    
    // NE POSTOJI string za search
    // ali postoji query
    if ( 
      kreirao_query == false                 && 
      req.body.query                         && 
      Object.keys(req.body.query).length > 0 &&
      !trazi_string
    ) {
      
      find_query = req.body.query;
      kreirao_query = true;
    };

    
    
    // postoji string za search
    if (
      kreirao_query == false
      &&
      trazi_string !== null 
    ) {

      trazi_array = [];

      
      // ----------------------------  FIND IN ----------------------------
      if ( req.body.find_in && req.body.find_in.length > 0 ) {
        

        
                
        // ------------------------------------ OVERRIDE FIND_IN AKO JE FIND DOCS ------------------------------------
        if ( 
          req.originalUrl == '/find_docs' 
          ||
          req.originalUrl == '/find_locations' 
          ||
          req.originalUrl == '/find_sirov_location' 
          ||
          req.originalUrl == '/find_sirov_records' 
          
        ) {
          
          FIND_IN = req.body.find_in;
          
        }
        else {
          
          // ------------------------------------ REGULARNI FIND_IN REG EX ------------------------------------
          // ------------------------------------ REGULARNI FIND_IN REG EX ------------------------------------
          
          
          
          
          
          var FIND_IN = [];
          
          
          
          // MORAM PRVO PROVJERITI DA LI UOPĆE POSTOJI TAJ PROP U SCHEMI OD DB MODELA
          // MORAM PRVO PROVJERITI DA LI UOPĆE POSTOJI TAJ PROP U SCHEMI OD DB MODELA
          // MORAM PRVO PROVJERITI DA LI UOPĆE POSTOJI TAJ PROP U SCHEMI OD DB MODELA
          
          $.each( req.body.find_in, function(fin_ind, fin) {
            $.each( DB_model.schema.paths, function(search_key, schema_object) {

              if ( fin == search_key ) {
                FIND_IN.push(fin);
              };

            });
          });

          if ( req.body.find_in.length > FIND_IN.length ) {

            resolve({

              success: false, 
              msg: `Jedno ili više polja definiranih u FIND_IN ne postoji u SCHEMI DOKUMENTA U BAZI`, 
              error: { 
                find_in: req.body.find_in,
                schema: FIND_IN,
              },

            });

            return;

          };

          // --------------------END---------------- REGULARNI FIND_IN REG EX ------------------------------------
          // --------------------END---------------- REGULARNI FIND_IN REG EX ------------------------------------
        };

        
        // pretvori FIND ARRAY U query za pretragu 
        FIND_IN.forEach(function(polje, index) {
          var search_objekt = { [polje] : { "$regex": trazi_string, "$options": "i", $exists: true } };
          // search_objekt[polje] = { "$regex": trazi_string, "$options": "i" };
          trazi_array.push(search_objekt);
        });
        
      }
      else {
        
        // ---------------------------- NEMA FIND IN  TRAŽI U SVIM POLJIMA KOJA SU STRING ----------------------------
        var all_model_keys = Object.keys( DB_model.schema.paths );
        // izvuci iz modela samo string propertije 
        all_model_keys.forEach(function(key, key_index) {
          if ( DB_model.schema.paths[ key ].instance == 'String' ) trazi_polja.push(key);
        });

        trazi_polja.forEach(function(polje, index) {
          var search_objekt = {};

          search_objekt[polje] = { "$regex": trazi_string, "$options": "i" };
          trazi_array.push(search_objekt);
        });
        
      };

      
      find_query = { $or: trazi_array };
      
      // ako je definiran dodatni query onda ga spoji sa find id sa AND operatorom
      // dakle mora pronaći sve što je u queriju i također mora pronaći barem nešto iz find in
      if ( req.body.query && Object.keys(req.body.query).length > 0 ) {
        find_query = { $and: [ { $or: trazi_array }, req.body.query ] };
      };
      
      kreirao_query = true;

    };


    // ako nije ništa od gore navedenog onda jednostavno uzmi sve bez ikakvog filtera !!!!!!
    if ( kreirao_query == false ) {
      find_query = {};
    };


    var model_find =  DB_model.find(find_query);
    // model populate je isti kao i model find
    var model_populate = model_find;

    // ali ako se radi o actions onda populate nije isti već dodajem _user u svaki doc
    if ( req.originalUrl == '/search_actions' ) model_find = model_find.populate('_user');

    var items_skip = req.body.page ? ((req.body.page - 1) * list_limit) : 0;

    if ( req.body.return && Object.keys(req.body.return).length > 0 ) model_find = model_find.select( req.body.return );
    
    
 
    // populate objects if any of these queries 
    if ( req.originalUrl == '/find_partner' ) {
      
      model_find = model_find
      .populate('grupacija')
      .populate('statuses');
      
      
    };
    
    
    if ( req.originalUrl == '/find_proj' ) {
      // model_find = model_find.populate('kupac');
      model_find = model_find
      .populate({
        path: 'kupac',
        populate: { path: 'grupacija' }
      })
      .populate({
        path: 'items',
        populate: [ 'statuses', 'kalk', 'mont_ploce', 'variant_of', 'original', 'alati', 'partner' ],
      })
      
    };
    
    
    if ( req.originalUrl == '/find_product' ) {
      // model_find = model_find.populate('kupac');
      model_find = model_find
      .populate(['statuses', 'kalk', 'mont_ploce', 'variant_of', 'original', 'alati', 'partner'  ])
    };
    
    
    if (
      req.originalUrl == '/find_status'
      ||
      req.originalUrl == '/find_status_for_production'
      ||
      req.originalUrl == '/filter_statuses'
    
    ) {
      // model_find = model_find.populate('kupac');
      model_find = model_find
      .populate([ 'kalk' ])
    };
    
    
    if ( req.originalUrl == '/find_sirov' ) {
      
      
      // model_find = model_find.populate('kupac');
      model_find = model_find
      .populate({
        path: 'dobavljac',
        populate: { path: 'grupacija' }
      })
      .populate('grupa')
      .populate('statuses')
      .populate('original')
      .populate('alat_owner')
      .populate('list_kupaca')
      
    };    
    
    
    
    
    // TODO OBRISATI KASNIJE
    /*

    if ( req.originalUrl == '/search_actions' ) {
      list_limit = 100000;
      items_skip = 0;
    };

    */
    
    
    



    model_find
      .limit(list_limit)
      .skip( items_skip )
      .sort( (sortiraj_po || { _id: -1 }) )
      .exec( async function(err, docs) {

      
      if (err) {
        resolve({ success: false, msg: 'Došlo je do greške kod pretrage u bazi !!!', error: err });
        return;
      };
      
      var clean_docs = (docs.length > 0) ? global.mng_to_js_array(docs) : []

      
      if ( req.originalUrl == '/find_status_in_sirov' ) {
        
        try {
          docs = await get_non_finish_orders(clean_docs);
          
          clean_docs = (docs.length > 0) ? global.mng_to_js_array(docs) : []
          
        } catch (err) {
          console.log(err);
          resolve({ success: false, msg: 'Došlo je do greške kod pretrage get_non_finish_orders !!!', error: err });
        };
        
      };
      
      
      /*
      
      // ------------------------------ OVO JE NEPOTREBNO JER VEĆ U QUERIJU DOBIJE CIJELI BRANCH !!!!
      // ------------------------------ OVO JE NEPOTREBNO JER VEĆ U QUERIJU DOBIJE CIJELI BRANCH !!!!
      // ------------------------------ OVO JE NEPOTREBNO JER VEĆ U QUERIJU DOBIJE CIJELI BRANCH !!!!
      // ------------------------------ OVO JE NEPOTREBNO JER VEĆ U QUERIJU DOBIJE CIJELI BRANCH !!!!
      
      
      if ( req.originalUrl == '/find_status_for_production' ) {
        
        
        var parent_sifra = null;
        
        // loop po svim statusima i pronadji koji od njih je parent
        // $.each( clean_docs, function(cl_ind, clean) {
        //  if ( clean.elem_parent !== null ) parent_sifra = clean.elem_parent;
        // });
        
        // ako nije nasao parent sifru ONDA ZNAČI DA SE RADI SAMO O JEDNOM STATUSU KOJI JE VEĆ PARENT
        // if ( parent_sifra == null ) {
          
          
          $.each( clean_docs, function(cl_ind, clean) {
            if ( clean.elem_parent == null ) parent_sifra = clean.sifra;
          });
          
          
        // };
        
        
        // 
        if ( parent_sifra !== null ) {

          try {
            var branch = await get_entire_branch( parent_sifra );
            if ( branch.success == true ) {
              clean_docs = branch.statuses ? branch.statuses : [];
            } else {
              resolve({ success: false, msg: 'U bazi nije pronađena grana s ovim parent-om za proizvodni nalog !!!!', error: "" });
            };
          } catch (err) {
            console.log(err);
            resolve({ success: false, msg: 'Greška na serveru kod dobivanja cijele grane (svi statusi) proizvodnog naloga', error: err });
            return;
          };

          
        }
        else {
          
          resolve({ success: false, msg: 'Nije našao parent status za radni nalog !!!', error: `` });
          return;
        }
        
      }; // kraj jel query za production
      
      
      
      // ------------------------------ OVO JE NEPOTREBNO JER VEĆ U QUERIJU DOBIJE CIJELI BRANCH !!!!
      // ------------------------------ OVO JE NEPOTREBNO JER VEĆ U QUERIJU DOBIJE CIJELI BRANCH !!!!
      // ------------------------------ OVO JE NEPOTREBNO JER VEĆ U QUERIJU DOBIJE CIJELI BRANCH !!!!
      
      */
      

      if ( 
        clean_docs && clean_docs.length > 0 
        &&
        req.originalUrl == '/find_sirov'
      
      ) {

        $.each(clean_docs, function(f_ind, f_sirov) {
          var forcast_kolicina = sirov_on_date ? cit_forecast(f_sirov, sirov_on_date ) : null;
          clean_docs[f_ind].forcast_kolicina = forcast_kolicina;
        }); 

      };

      
      // --------------------- START AKO JE FIND GRUPA --------------------
      
      if ( 
        clean_docs && clean_docs.length > 0 
        &&
        req.originalUrl == '/find_grupa' ) {
        
        var grupa_sirovs_ids = [];
        
        $.each( clean_docs, function(gs_ind, grupa_doc) {
          $.each( grupa_doc.sirovine, function(sir_index, sirovina) {
            grupa_sirovs_ids.push(sirovina._id);
          });
        });
        
        var sve_sirovine = await Sirov.find( { _id: { $in: grupa_sirovs_ids } } ).exec();
        sve_sirovine = global.mng_to_js_array(sve_sirovine);
        
        // NAPRAVI UPDATE IMENA SVAKE SIROVINE UNUTAR GRUPE !!!
        
        $.each( clean_docs, function(gs_ind, grupa_doc) {
          $.each( grupa_doc.sirovine, function(sir_index, sirovina) {
            
            $.each( sve_sirovine, function(db_sir_index, DB_sirovina) {
              if ( DB_sirovina._id == sirovina._id ) clean_docs[gs_ind].sirovine[sir_index].name = DB_sirovina.full_naziv;
            });
            
          });
        });
        
      }; 
      
      
      
      /*
      
      if ( req.originalUrl.indexOf('/find_status_for_page/') > -1 ) {
        var status_page = req.params.status_page;
        if ( status_page == `cit_task_from_me` ) {
          
        };
      };

      */
      

      if ( typeof filter_funkcija == "string") {
        
        var run_filter = new Function('items', 'jQuery', filter_funkcija);
        clean_docs = run_filter( clean_docs, $ );
        
      };
      
      if ( $.isArray(filter_funkcija) ) {
        
        $.each( filter_funkcija, function(f_ind, filter_string) {
          var run_filter = new Function('items', 'jQuery', filter_string);
          clean_docs = run_filter( clean_docs, $ );
        });
        
      };


      // ----------------------------------- SAMO ZADNJIH 100 RECORDA STATUSA I DOCSA -----------------------------------
      if ( 
        clean_docs && clean_docs.length > 0 
        &&
        req.originalUrl == '/find_sirov'  
      ) {
        
        var last_100_records = [];
        
        $.each( clean_docs, function(doc_ind, doc) {
          
          if ( doc.records && doc.records.length > 0 ) {
            
            // sortiratj tako da najnoviji budu prvi !!!
            global.multisort( clean_docs[doc_ind].records, [ '!time' ] );
            
            // ODREŽI TAKO DA BUDE SAMO PRVIH 100 RECORDA !!!!
            var last_100_records = clean_docs[doc_ind].records.slice(0, 100);
            
            clean_docs[doc_ind].records_count = doc.records.length;
            clean_docs[doc_ind].records = last_100_records;
            
            
          }; // ako ima u sebi statuse
          
        }); // kraj loop po svim clean docs
        
      }; // ako je find sirov ili find partner i ako postoje clean docs
      
      
      if ( 
        clean_docs && clean_docs.length > 0 
        &&
        ( req.originalUrl == '/find_sirov' || req.originalUrl == '/find_partner' )  
      ) {
        
        var last_100_statuses = [];
        
        $.each( clean_docs, function(doc_ind, doc) {
          
          if ( doc.statuses && doc.statuses.length > 0 ) {
            
            // sortiratj tako da najnoviji budu prvi !!!
            global.multisort( clean_docs[doc_ind].statuses, [ '!time' ] );
            
            // ODREŽI TAKO DA BUDE SAMO PRVIH 100 STATUSA !!!!
            var last_100_statuses = clean_docs[doc_ind].statuses.slice(0, 100);
            
            clean_docs[doc_ind].statuses_count = doc.statuses.length;
            clean_docs[doc_ind].statuses = last_100_statuses;
            
            
          }; // ako ima u sebi statuse
          
        }); // kraj loop po svim clean docs
        
        
      }; // ako je find sirov ili find partner i ako postoje clean docs
      
      
      
      if ( 
        clean_docs && clean_docs.length > 0 
        &&
        req.originalUrl == '/find_partner'
      ) {
        
        var last_100_docs = [];
        
        $.each( clean_docs, function(doc_ind, doc) {
          
          if ( doc.docs && doc.docs.length > 0 ) {
            
            // sortiratj tako da najnoviji budu prvi !!!
            global.multisort( clean_docs[doc_ind].docs, [ '!time' ] );
            
            // ODREŽI TAKO DA BUDE SAMO PRVIH 100 STATUSA !!!!
            var last_100_docs = clean_docs[doc_ind].docs.slice(0, 100);
            
            clean_docs[doc_ind].docs_count = doc.docs.length;
            clean_docs[doc_ind].docs = last_100_docs;
            
            
          }; // ako ima u sebi docs
          
        }); // kraj loop po svim clean mongo dokumentima
        
        
      }; // ako je find sirov ili find partner i ako postoje mongo dokumenti
      
      
      
      if ( 
        clean_docs && clean_docs.length > 0 
        &&
        req.originalUrl == '/find_docs'
      ) {
        
        var last_100_docs = [];
        var reduced_last_100_docs = [];
        
        $.each( clean_docs, function(doc_ind, doc) {
          
          if ( doc.docs && doc.docs.length > 0 ) {

            
            // sortiratj tako da najnoviji budu prvi !!!
            global.multisort( clean_docs[doc_ind].docs, [ '!rok' ] );
            
            
          }; // ako ima u sebi docs
          
        }); // kraj loop po svim clean mongo dokumentima
        

        if ( clean_docs && clean_docs.length > 0 ) {

          $.each( clean_docs, function(r_ind, doc) {
            
            
            if ( doc.doc_type == get_doc_type && doc.doc_sirovs ) {
              
              
              $.each( doc.doc_sirovs, function(s_ind, sir) {
                
                if ( 
                  
                  sir._id == get_doc_sirov_id 
                  && 
                  sir.doc_kolicine 
                  && 
                  sir.doc_kolicine.length > 0
                  
                ) {
                  
                  
                  /*
                  -------- ZA SADA OVO NE KORISTIM ----------
                  reduced_last_100_docs.push({
                
                    sifra: doc.sifra,

                    doc_type: doc.doc_type,
                    doc_sifra: doc.doc_sifra,
                    time: doc.time,
                    doc_link: doc.doc_link,
                    doc_sirovs: doc.doc_sirovs,
                    rok: doc.rok,
                    count: sir.doc_kolicine[0].count || 0,

                  });
                  */
                  
                  
                  var doc_link =
`
<a href="/docs/${time_path(doc.pdf_time)}/${doc.pdf_name}" target="_blank" onClick="event.stopPropagation();" > 
  ${ doc.doc_sifra }
</a>
`; 
                  
                  var refer_doc_object = {
                    sifra: doc.sifra,
                    refer_doc: doc.doc_sifra,
                    time: doc.rok,
                    primka_count: sir.doc_kolicine[0].count || 0,
                    link: doc_link,
                  };

                  last_100_docs = global.insert_uniq(last_100_docs, refer_doc_object, `refer_doc`);

                  
                }; // ako je sirovina _id koju tražim i ako u sebi ima doc kolicine barem jednu količinu ( za primku bi trebala biti samo jedan količina !!!)
                
                
              }); // kraj loopa po svim sirovs u doc
              

            }; // jel doc type koji trebam
            
          }); // kraj loopa po svim dokumentima koje sam našao u mongo db
          

        }; // kraj ako je našao nešto u docs od partnera
        

        // ODREŽI TAKO DA BUDE SAMO PRVIH 100 STATUSA !!!!
        reduced_last_100_docs = last_100_docs.slice(0, 100);

        // vrati samo array docsa -----> ne treba mi partner tj. dobavljač !!!!  
        clean_docs = reduced_last_100_docs;
        
        
      }; // ako je find_primke tj find docs
      
      
      
      if ( 
        clean_docs && clean_docs.length > 0 
        &&
        req.originalUrl == '/find_sirov_records'
      ) {
        
        var reduced_last_100_records = [];
        
        $.each( clean_docs, function(doc_ind, doc) {
          
          if ( doc.records && doc.records.length > 0 ) {
            // sortiratj tako da najnoviji budu prvi !!!
            global.multisort( clean_docs[doc_ind].records, [ '!record_est_time' ] );
            
            
            $.each( clean_docs[doc_ind].records, function(r_ind, record) {

              if ( 
                record.type == find_record_type  // na primjer ovo može biti "radni_nalog"
                &&
                reduced_last_100_records.length < 100 
              ) {

                  
  function gen_record_kalk_partner_link(record_obj, record_kalk) {
      
    var kalk_link = 
`
<a href="#project/${record_kalk.proj_sifra || null}/item/${record_kalk.item_sifra || null}/variant/${record_kalk.variant || null}/kalk/${record_kalk.kalk_sifra || null}/proces/null" 
  style="text-align: left;"
  target="_blank"
  onClick="event.stopPropagation();"> 
  ${ record_obj.doc_sifra } 
</a>
`;
    return kalk_link;
    
  };
 

                
                var refer_doc_object = { 
                  sifra: record.sifra,
                  refer_doc: record.doc_sifra,
                  time: record.record_est_time,
                  link: (record.record_kalk ? gen_record_kalk_partner_link(record, record.record_kalk) : "" ),
                };
                
                reduced_last_100_records = global.insert_uniq(reduced_last_100_records, refer_doc_object, `refer_doc`);
                

              };

            }); // kraj loopa po svim recordsima od ove sirovine
            
          }; // ako ima u sebi records
          
        }); // kraj loop po svim clean mongo dokumentima
        

        // vrati samo array docsa -----> ne treba mi partner tj. dobavljač !!!!  
        clean_docs = reduced_last_100_records;
        
        
      }; // KRAJ  ako je find_sirov_records 

      
      
      if ( 
        clean_docs && clean_docs.length > 0 
        &&
        req.originalUrl == '/find_locations'
      ) {
        
        var last_100_locations = [];
        var reduced_last_100_locations = [];
        
        $.each( clean_docs, function(loc_ind, doc) {
          
          if ( doc.locations && doc.locations.length > 0 ) {

            
            // sortiratj tako da najnoviji budu prvi !!!
            global.multisort( clean_docs[loc_ind].docs, [ '!time' ] );
            
            // ODREŽI TAKO DA BUDE SAMO PRVIH 100 STATUSA !!!!
            last_100_locations = clean_docs[loc_ind].docs.slice(0, 100);
            
            
          }; // ako ima u sebi locations
          
        }); // kraj loop po svim clean mongo dokumentima
        

        if ( last_100_locations && last_100_locations.length > 0 ) {

          $.each( last_100_locations, function(r_ind, location) {
            
            
          
            
            
            if ( location.doc_type == get_doc_type ) {
              
              
              var primka_count = 0;
              
              $.each( doc.doc_sirovs, function(s_ind, sir) {
                
                if ( 
                  
                  sir._id == get_doc_sirov_id 
                  && 
                  sir.doc_kolicine 
                  && 
                  sir.doc_kolicine.length > 0
                  
                ) {
                  
                  primka_count = sir.doc_kolicine[0].primka_count || 0;
                  
                  
                  reduced_last_100_docs.push({
                
                    sifra: doc.sifra,

                    doc_type: doc.doc_type,
                    doc_sifra: doc.doc_sifra,
                    time: doc.time,
                    doc_link: doc.doc_link,
                    doc_sirovs: doc.doc_sirovs,
                    rok: doc.rok,
                    primka_count: primka_count,

                  });

                  
                  
                }; // ako je sirovina _id koju tražim i ako u sebi ima doc kolicine barem jednu količinu ( za primku bi trebala biti samo jedan količina !!!)
                
                
              });
              

            };
            
          });
          

        }; // kraj ako je našao nešto u docs od partnera
        

        // vrati samo array docsa -----> ne treba mi partner tj. dobavljač !!!!  
        clean_docs = reduced_last_100_docs;
        
        
      }; // ako je find_locations 
            
      
      
      
      if ( 
        clean_docs && clean_docs.length > 0 
        &&
        req.originalUrl == '/find_sirov_location'
      ) {
        
      
        var mached_locations = [];
        
        $.each( clean_docs, function(r_ind, doc) {
          
          
          $.each( doc.locations, function(loc_ind, loc) {
            
            if ( loc.palet_sifre.indexOf( trazi_string.toUpperCase() ) > -1 ) {
              mached_locations = global.upsert_item( mached_locations, loc, `sifra`); 
            };
           
            
            var regex_primka = new RegExp(trazi_string, 'gm');
            
            if ( 
              loc.primka_record 
              &&
              loc.primka_record.doc_sifra 
              &&
              regex_primka.test(loc.primka_record.doc_sifra)
              
              
              /*
              loc.primka_record.doc_sifra.match(trazi_primku).length > 0
              loc.primka_record.doc_sifra.indexOf( trazi_string.toUpperCase() ) > -1
              */
              
            ) {
              
              
              
              mached_locations = global.upsert_item( mached_locations, loc, `sifra`); 
              
            };
            
            
            
            if ( 
              loc.primka_pro_record 
              &&
              loc.primka_pro_record.doc_sifra 
              &&
              regex_primka.test(loc.primka_pro_record.doc_sifra)
              
              
              /*
              loc.primka_pro_record.doc_sifra.match(trazi_primku).length > 0
              loc.primka_pro_record.doc_sifra.indexOf( trazi_string.toUpperCase() ) > -1
              */
              
            ) {
              
              
              
              mached_locations = global.upsert_item( mached_locations, loc, `sifra`); 
              
            };            
            
            
          
          });
          
          
        });
        
        
        
        
        


        // vrati samo array lokacija (a ne sirovine )
        clean_docs = mached_locations;
        
        
      }; // ako je find sirov location 
      
      

      
      if ( 
        clean_docs && clean_docs.length > 0 
        &&
        req.originalUrl == '/filter_statuses'
        &&
        /* ako postoji bilo koji od ovih dolje filtera */
        (
          filter_proces_tip_1
          ||
          filter_machine_1
          ||
          filter_radne_stanice_1
          ||
          filter_proces_tip_2
          ||
          filter_machine_2
          ||  
          filter_radne_stanice_2

        )
        
        ) {
        
      
        clean_docs = filter_statuses(
          clean_docs,
          
          filter_proces_tip_1,
          filter_machine_1,
          filter_radne_stanice_1,
    
          filter_proces_tip_2,
          filter_machine_2,
          filter_radne_stanice_2
        );
        
        
      };
      
      
      
      
      if ( get_count && clean_docs && clean_docs.length > 0 ) {

        // ----------------------------------- AKO SAM TRAŽIO COUNT U REQUESTU -----------------------------------
        // -------------START----------------- AKO SAM TRAŽIO COUNT U REQUESTU -----------------------------------
        // ----------------------------------- AKO SAM TRAŽIO COUNT U REQUESTU -----------------------------------
        
        var find_query_string = JSON.stringify(find_query);
        
        if ( !global.query_cache ) global.query_cache = [];
        
        var curr_time = Date.now();
        var cache_length = global.query_cache.length;
        
        if ( cache_length > 0 ) {
          
          var f;
          
          for( f=cache_length-1; f>=0; f-- ) {
            // ako je time od cache zapisa starije od 1 sat vremena onda ga obriši
            if (  curr_time - global.query_cache[f].time > 1000*60*60  ) global.query_cache.splice(f,1);
          };
          
        };
        
        // probaj pronaći jel već imam taj isti query u cache
        var cache_query = global.find_item( global.query_cache, find_query_string, `query` );
        
        if ( cache_query ) {
          // ako imam ovaj cahce onda vrati length 
          resolve( { docs: clean_docs,  count: cache_query.len || 0 } );
          
        } else {
         
          // napravi database query bez limita
          var no_limit_docs = await DB_model.find(find_query).exec();
          // ako nema ovog cacha onda ga ubaci
          global.query_cache.push({
            query: find_query_string,
            time: curr_time,
            len: no_limit_docs.length || 0
          });
          
          resolve( { docs: clean_docs,  count: (no_limit_docs ? no_limit_docs.length : 0) } );
          
        };
        
        
      } 
      else {
        
        // --------------------------------------------- NIJE TRAŽIO GET COUNT I ZATO NE MORAM KREIRATI CACHE
        // --------------------------------------------- NIJE TRAŽIO GET COUNT I ZATO NE MORAM KREIRATI CACHE
        // --------------------------------------------- NIJE TRAŽIO GET COUNT I ZATO NE MORAM KREIRATI CACHE
        
        resolve(clean_docs);
      
      };
  
      

    }); // kraj exec od GLAVNOG querija !!!
    
    
    // zaustavi danji rad ako je request za sve zapise
    // if (trazi_polja == 'all') return;
    /*
    DOBIJEM NEŠTO SLIČNO OVOME:
    -----------------------------------------------------------
    [
      { "en.name": { "$regex": trazi_string, "$options": "i" }},
      { "de.name": { "$regex": trazi_string, "$options": "i" }},
      { "hr.name": { "$regex": trazi_string, "$options": "i" }},
      { "sr.name": { "$regex": trazi_string, "$options": "i" }},
    ] 
    -----------------------------------------------------------
    */

    /*
    UBACITI OVO PRIJE .exec AKO ŽELIM SELEKTIRATI SAMO NEKA POLJA:
    -----------------------------------------------------------
    .select(select_objekt)

    dobijem nešto slično ovome
    .select({
      _id: 1,
      status: 1,
      gametype_id: 1,
      coverImg: 1,
      "config.status.lastChangedAt": 1,
      "hr.name": 1, 
      "en.name": 1, 
      "de.name": 1, 
      "sr.name": 1
    })
    -----------------------------------------------------------
    */  
  
  }); // kraj promisa
  
};
