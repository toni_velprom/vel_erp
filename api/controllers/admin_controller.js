var mongoose = require('mongoose');

// mongoose.set('useFindAndModify', false);

var User = mongoose.model('User');
var Counter = mongoose.model('Counter');
var Grupa = mongoose.model('Grupa');

var Project = mongoose.model('Project');
var Product = mongoose.model('Product');
var Status = mongoose.model('Status');


var AdminConf = mongoose.model('AdminConf');


var nodemailer = require('nodemailer');
var multer = require('multer')

var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);


exports.get_admin_conf = function(req, res) {
  

  // find the user
  AdminConf.findOne(
    { _id: "616013226c72300eabd131b0" }, // uvijek je isti _id
    function(err, admin_conf) {
    
    if (err) {
      console.log(`Error response prilikom GET admin konfiguracije`, err);
      res.json({success: false, error: err, msg: `Greška prilikom GET admin konfiguracije !!!` })
      return;
    };
      
    var clean_admin_conf = global.mng_to_js(admin_conf);
    var token = req.body.token || req.query.token || req.headers['x-auth-token'];
      
      if (token) {
      
        global.check_user_auth(token)
        .then(function(response) {

          if ( response.success == true ) {
            // ako nije admin daj useru samo statuse !!!!
            if ( !response.super_admin ) clean_admin_conf = global.cit_deep( { status_conf: clean_admin_conf.status_conf } );
            
          } 
          else {
            // ako response nije true opet user daj samo statuse 
            if ( !response.super_admin ) clean_admin_conf = global.cit_deep( { status_conf: clean_admin_conf.status_conf } );
          };

        })
        .catch(function(err) {
          
          // AKO JE GREŠKA PRILIKOM VERIFIKACIJE
          res.json({ 
            success: false, 
            msg: 'Došlo je do greške prilikom verifikacije korisnika.<br>ERROR: 001' 
          });
          return;

        });

    } else {
      
      // AKO NEMA TOKEN !!!
      res.json({ 
        success: false, 
        msg: 'Došlo je do greške prilikom verifikacije korisnika.<br>ERROR: 001' 
      });
      return; 
    };

    res.json({success: true, admin_conf: clean_admin_conf });
    
  }); // end of admin conf findOne
  
};


exports.save_admin_conf = function(req, res) {

  var admin_conf = req.body;
  
  AdminConf.findOneAndUpdate(
    { _id: "616013226c72300eabd131b0" },
    admin_conf,
    { new: true }, function(err, admin_conf) {

      if (err) {
        console.log(`Error response prilikom editiranja ADMIN konfiguracije`, err);
        res.json({success: false, error: err, msg: `Greška prilikom editiranja ADMIN konfiguracije !!!` })
        return;
      };
      
      var clean_admin_conf = global.mng_to_js(admin_conf);
      res.json({success: true, admin_conf: clean_admin_conf });
      
    });   

};

