var mongoose = require('mongoose');

// mongoose.set('useFindAndModify', false);

var User = mongoose.model('User');
var Counter = mongoose.model('Counter');
var Grupa = mongoose.model('Grupa');

var Project = mongoose.model('Project');

var Product = mongoose.model('Product');
var Sirov = mongoose.model('Sirov');
var Partner = mongoose.model('Partner');

var Status = mongoose.model('Status');

var nodemailer = require('nodemailer');
var multer = require('multer')

var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);


// var id = mongoose.Types.ObjectId('4edd40c86762e0fb12000003');
// var _id = mongoose.mongo.BSONPure.ObjectID.fromHexString("4eb6e7e7e9b7f4194e000001");

var find_controller = require('./find_controller.js');


function check_status_place(status_list, curr_status ) {

  // STATUS NE MOŽE BITI POČETAK GRANE AKO JE VEĆ UNUTER NEKE GRANE !!!!
  var is_next_status = false;
  // STATUS NE MOŽE BITI POČETAK GRANE AKO JE VEĆ FINISH NEKE GRANE !!!!
  var is_finish_status = false;

  var has_children = false;

  // loopaj sve statuse i pogledaj jel se trenutni status nalazi unutar next statusa ili finish statusa
  $.each(status_list , function(check_index, check_status ) {

    if ( check_status.next_statuses && check_status.next_statuses.length > 0 ) {
      $.each(check_status.next_statuses , function(next_index, next_status ) {
        if ( next_status.sifra == curr_status.sifra ) {
          is_next_status = true;
        };
      });
    };

    if ( check_status.grana_finish && check_status.grana_finish.length > 0 ) {
      $.each(check_status.grana_finish , function(grana_index, grana_finish_object ) {
        if ( grana_finish_object.sifra == curr_status.sifra ) {
          is_finish_status = true;
        };
      });
    };

  });

  if ( curr_status.next_statuses && curr_status.next_statuses.length > 0 ) has_children = true;
  if ( curr_status.grana_finish && curr_status.grana_finish.length > 0 ) has_children = true;

  return {
    next: is_next_status,
    finish: is_finish_status,
    has_children: has_children,
  }

};

function get_status_parent(data, curr_status) {
  
  var parent_status = null;
  
  $.each(data.statuses, function( s_ind, status ) {
    if ( status.sifra == curr_status.elem_parent ) parent_status = status;
  });
  
  return parent_status;
  
};

function get_status_children(data, arg_parent) {
  
  var children = [];
  $.each(data.statuses, function( s_ind, status ) {
    if ( status.elem_parent == arg_parent.sifra ) children.push(status);
  });
  return children;
  
};

function is_branch_done(arg_parent, arg_children) {
  
  var branch_done = false;
  
  if ( 
    arg_parent.status_tip &&
    arg_parent.status_tip.grana_finish &&
    arg_parent.status_tip.grana_finish.length > 0 
  ) {
    
    $.each( arg_parent.status_tip.grana_finish, function(f_index, finish) {

      $.each( arg_children, function(c_index, child_status) {
        // ako BILO KOJI CHILD STATUS IMA STATUS TIP KOJI JE  JEDAN OD FINISH OD PARENTA 
        // -----------> onda je grana gotova !!!!
        if ( child_status.status_tip.sifra == finish.sifra ) {
          branch_done = true;
        };
        
      }); // loop po svim children
      
    }); // loop po svim finish tipovima
    
  };

  return branch_done;
  
};


exports.find_status = function(req, res) {

  global.cit_search_remote(req, res, Status)
  .then(function(result) {
    res.json(result) 
  })
  .catch(function(error) {
    res.json(error) 
  });
  
};


// JAKO BITNO !!!!!!
// JAKO BITNO !!!!!!
// ovdje ne moram provjeravati jeli status ima branch done ili ne zato što user niti ne može izabrati statuse koji su zatvoreni !!!
exports.already_has_this_status_tip_in_branch = function(req, res) {
  
  var status_sifre = req.body.status_sifre;
  var compare_status_tip_sifra = req.body.compare_status_tip_sifra;
  
  // prvo nadji sve statuse
  Status.find(
    { time: { $gt: (Date.now()-1000*60*60*24*180) } }, // zadnja 6 mjeseca
    function(err, statuses) {

      if (err) {
        console.log(`Error response prilikom pretrage statusa sa querijem { time: { $gt: (Date.now()-1000*60*60*24*90) } } tj statusi zadnja 3 mjeseca` );
        console.log(err);
        res.json( { success: false, msg: `Greška prilikom pretrage statusa u zadnja 3 mjeseca`, error: err } ) 
        return;
      };
    
      var all_statuses_clean = global.mng_to_js_array(statuses);
      // prvo od svih statusa u zadnjih 6 mjesec moram pronaći statuse koji meni trebaju
      var selected_statuses = []; 
      $.each( all_statuses_clean, function(s_ind, stat) {
        if ( status_sifre.indexOf(stat.sifra) > -1 ) selected_statuses.push(stat);
      });
      
      var parents = []; 
      // onda trebam u statusima koji meni trebaju pronaći DJECU 
      $.each( selected_statuses, function(s_ind, stat) {
        // ako ima parent
        if ( stat.elem_parent ) {
          // CHILD status prepoznajem  po tome što ima elem_parent sifru
          $.each( all_statuses_clean, function(db_ind, db_stat) {
            // pronadji parent status od toh DJETETA u statusima od zadnjih 6 mjeseci
            if ( db_stat.sifra == stat.elem_parent ) parents.push(stat);
          });
        };
      });
      
      // UGURAJ U SVAKI PARENT NJEGOVU DJECU
      // UGURAJ U SVAKI PARENT NJEGOVU DJECU
      // UGURAJ U SVAKI PARENT NJEGOVU DJECU
      
      $.each( parents, function(p_ind, parent) {
        
        $.each( all_statuses_clean, function(db_ind, db_stat) {
          // ako je djete onda ima elem_parent property kao parent sifra
          if ( db_stat.elem_parent && db_stat.elem_parent == parent.sifra ) {
            // ako nema propertija status children onda napravi da bude inicijalno prazan array
            if ( !parents[p_ind].status_children ) parents[p_ind].status_children = [];
            parents[p_ind].status_children.push(db_stat);
          };
          
        });
        
      });

      
      // sve samo ovo napravio da dobijem array parent statusa
      // i u svakom parentu unutar propertija status_children imam svu djecu od tog parenta
      var remove_this_status_sifre = [];
      $.each( parents, function(p_ind, parent) {
        
        // samo provjeravam jel ovaj parent ima djecu UOPĆE
        if ( parent.status_children && parents.status_children.length > 0 ) {
          
          $.each( parent.status_children, function(c_ind, child) {
            // ako bilo koje od djece već ima da status tip sa šifrom koju sam posalo u req
            // to znači da ta grana statusa već ima POTREBAN status 
            if ( child.status_tip && child.status_tip.sifra == compare_status_tip_sifra ) remove_this_status_sifre.push(child.sifra);
          });
          
        };
        
      });
      
      
      var result_sifre = []
      $.each( status_sifre, function(s_ind, sifra) {
        // sada u responsu vrati nazad samo one šifre koje nemaju potrebne ODGOVORE u svojim granama
        if ( remove_this_status_sifre.indexOf(sifra) == -1 ) result_sifre.push(sifra);
      });
      
      res.json({ success: true, result: result_sifre });
      
      
    }); // kraj find statuse u zadnja 3 mjeseca
  
};


function reply_for_update(clean_status) {
  
  return new Promise( function(resolve, reject) {
    
    
    // ako ima reply for onda moram napraviti update na statusu na kojeg je user odgovorio
    if ( clean_status.reply_for ) {

      Status.findOneAndUpdate(
      { sifra: clean_status.reply_for }, 
      { $set: { responded: true, end: Date.now() } },
      { new: true }, function(err, status) {
        if (err) {
          reject({ success: false, error: err, msg: `Greška prilikom UPDATE REPLY FOR STATUSA !!!` });
          return;
        };
        // napokon response nakon update statusa na kojeg sam odgovorio
        resolve({success: true, status: clean_status });

      }); // kraj update replay for

    } 
    else {
      // ako nema reply_for for onda samo vrati response
      resolve({success: true, status: clean_status });

    }; // kraj else tj ako nema reply for prop
    
  }); // kraj promisa
  
};


function work_done_update(clean_status) {
  
  return new Promise( function(resolve, reject) {
    
    
    // ako ima work_done_for UNUTAR STATUS_TIP  ----> onda moram napraviti update na statusu 
    if ( clean_status.done_for_status ) {

      Status.findOneAndUpdate(
      { sifra: clean_status.done_for_status },
      { $set: { work_finished: true, end: Date.now() } },
      { new: true }, function(err, status) {
        if (err) {
          reject({ success: false, error: err, msg: `Greška prilikom UPDATE WORK DONE FOR STATUS !!!` });
          return;
        };
        // napokon response nakon update statusa na kojeg sam odgovorio
        resolve({success: true, status: clean_status });

      }); // kraj update replay for

    } 
    else {
      // ako nema reply_for for onda samo vrati response
      resolve( { success: true, status: clean_status } );

    }; // kraj else tj ako nema reply for prop


    
    
    
  }); // kraj promisa
  
};


function add_status_to_product(clean_status) {
  
  return new Promise( function(resolve, reject) {  
  
    
    var product_id = clean_status.product_id;
    
    if ( product_id ) {
  
      Product.findOneAndUpdate(
        { _id: clean_status.product_id },
        { $addToSet: { statuses: clean_status._id } },
        {new: true}, 
        function(prod_err, updated_product) {

          if (prod_err) {

            console.log(`Error prilikom ADD TO SET status _id into product statuses`, prod_err);
            reject({
              success: false,
              error: prod_err,
              msg: `Error prilikom ADD TO SET status _id into product statuses` 
            });

          } else {

            resolve(clean_status);

          }; // kraj ako je greska prilikom injektiranja _idja od statusa u odgovarajući product u bazi

      });    
  
    }
    else {
      
      resolve(clean_status);
    };
    
    
  }); // kraj promisa
  
  
};


function add_status_to_model( clean_status, Model_name, Model_id ) {
  
  return new Promise( function(resolve, reject) {  
    
    // AKO POSTOJI CLEAN STATUS  - MODEL NAME - MODEL ID
    if ( clean_status && Model_name && Model_id ) {

      mongoose.model( Model_name ).findOneAndUpdate(
        { _id: Model_id },
        { $addToSet: { statuses: clean_status._id } },
        {new: true}, 
        function(model_err, updated_doc) {

          if (model_err) {

            console.log(`Error prilikom ADD TO SET status _id into MODEL statuses`, model_err);
            
            reject({
              success: false,
              error: model_err,
              msg: `Error prilikom ADD TO SET status _id into MODEL statuses` 
            });

          } else {

            resolve(clean_status);

          }; // kraj ako je greska prilikom injektiranja _idja od statusa u odgovarajući product u bazi

      });    

    } 
    else {
      // ako nemam id i model name i model id ONDA SAMO VRATI TRUE  ------> tako da se lanac nastavi
      // ako nemam id i model name i model id ONDA SAMO VRATI TRUE 
      resolve(true);
      
    };
      
      
  }); // kraj promisa
  
  
};


exports.get_complete_branch = function(req, res) {
  
  var status_obj = req.body;
  var has_parent_status = status_obj.elem_parent_object ? status_obj.elem_parent_object : null;
  var parent_status_sifra = status_obj.elem_parent ? status_obj.elem_parent : null;
    
  var status_query = {};
  
  if ( has_parent_status ) {
    // ZNAČI OVAJ STATUS JE DIJETE
    status_query = { 
      $or: [ 
        { elem_parent: parent_status_sifra}, 
        { elem_parent: null, sifra: parent_status_sifra } 
      ] 
    }; // sva djeca sa elem parent PLUS sam elem parent 
  }
  else {
    // ZNAČI OVAJ STATUS JE PARENT
    status_query = { 
      $or: [ 
        { elem_parent: status_obj.sifra }, 
        { elem_parent: null, sifra: status_obj.sifra } 
      ] 
    };
    
  };
  

  Status.find(
    status_query,
    function(branch_error, branch_statuses) {

      if (branch_error) {

        console.log(`Error prilikom pretrage statusa u grani unutar funkcije get_complete_branch`, branch_error);

        reject({
          success: false,
          error: branch_error,
          msg: `Error prilikom pretrage statusa u grani unutar funkcije get_complete_branch !!!` 
        });

      } else {

        var clean_branch = global.mng_to_js_array(branch_statuses);
        res.json({ success: true, branch: clean_branch });

      }; // kraj jel nasao branch statuses

  });    
  
};





function get_branch_done(status_obj) {
  
  return new Promise( function(resolve, reject) {  
  
    var parent_status = status_obj.elem_parent_object ? status_obj.elem_parent_object : null;
    var parent_status_sifra = status_obj.elem_parent ? status_obj.elem_parent : null;
    
    // samo ako je ovaj status djete  
    // ako nije dijete onda je to prvi status u grani i naravno da nije branch done !!!
    
    if ( parent_status ) {
      
      // nađi svu DJECU od parent statusa !!!!
      // tj sve statuse koji imaju elem parent  == parent sifra
      Status.find(
        { elem_parent: parent_status_sifra },
        function(children_error, children_statuses) {

          if (children_error) {

            console.log(`Error prilikom pretrage CHILDREN statusa u grani`, children_error);
            
            reject({
              success: false,
              error: children_error,
              msg: `Error prilikom pretrage CHILDREN statusa u grani` 
            });

          } else {
            
            var clean_children = global.mng_to_js_array(children_statuses);
            
            // moram dodati i ovaj trenutni status kojeg spremam jer je i on djete
            // moram dodati i ovaj trenutni status kojeg spremam jer je i on djete
            // moram dodati i ovaj trenutni status kojeg spremam jer je i on djete
            clean_children.push(status_obj);
            
            
            
            var branch_sifre = [];
            
            
            $.each( clean_children, function( c_ind, child) {
              branch_sifre.push(child.sifra);
            });
            
            
            // ubaci i parent u array tako da i na parentu imam propery da je grana gotova
            // ubaci i parent u array tako da i na parentu imam propery da je grana gotova
            // ubaci i parent u array tako da i na parentu imam propery da je grana gotova
            
            branch_sifre.push( parent_status.sifra );
            
            var branch_is_done = is_branch_done(parent_status, clean_children);
            
            if ( branch_is_done ) {

              Status.updateMany( 
                { "sifra": { $in: branch_sifre } },
                { branch_done: true  },
                { new: true },
                function(branch_err, branch_statuses) {
                  
                if (branch_err) { 
                  
                  console.log("Greška kod update statusa sa branch done == true", branch_err);
                  
                  reject({
                    success: false,
                    error: branch_err,
                    msg: `Greška kod update statusa sa branch done == true` 
                  });
                  
                } else { 
                  
                  // također moram staviti da je grana gotova i u ovaj
                  status_obj.branch_done = true;
                  resolve( status_obj );
                  
                };
                  
              });  // kraj update many svih statusa 
              

            } else {
              
              status_obj.branch_done = false;
              // ako grana nije gotova samo pošalji objekt dalje
              resolve(status_obj);  
              
            };
            
          }; // kraj jel nasao children sattuses

      });    

    } 
    else {
      status_obj.branch_done = false;
      // ako status objekt nema parenta samo pošalji dalje !!!!
      resolve(status_obj);
      
    };
      
      
  }); // kraj promisa
  
    
    
  
}; // kraj get_branch done


function save_new_status(status_obj, res) {
  
  var Model_name = status_obj.Model_name || null;
  var Model_id = status_obj.Model_id || null;
  
  get_branch_done(status_obj)
  .then(
  function( branch_status_obj ) {

    // --------------------------------------------------------------------------
    var new_status = new Status(branch_status_obj);
    // save to DB
    new_status.save(function(err, status) {

      if (err) {
        console.log(`Error response prilikom SAVE STATUSA`, err);
        res.json({success: false, error: err });
        return;
      };

      var clean_status = global.mng_to_js(status);
      clean_status.status_id = clean_status._id;

      reply_for_update(clean_status)
      .then(
      function(response) {
        return work_done_update(clean_status);
      })
      .then(
      function(work_response) {
        return add_status_to_product(clean_status);
      })
      .then(
      function(product_response) {
        return add_status_to_model(clean_status, Model_name, Model_id);
      })
      .then(
      function(model_response) {
        res.json( {success: true, status: clean_status });
      })
      .catch(
      function(err){
        console.log(`Error response prilikom UPDATE REPLY FOR STATUSA`, err);
        res.json({ success: false, msg: `Greška pirlikom spremanja statusa`, err: err })
        return;
      });


    }); // kraj save new status
    // --------------------------------------------------------------------------  

  })
  .catch(
  function(err) {
    console.log(`Error response prilikom provjere jel grana gotova`, err);
    res.json({ success: false, msg: `Error response prilikom provjere jel grana gotova`, err: err })
    return;
  });
  
};


exports.save_status = function(req, res) {

  var status_obj = req.body;
  
  // obriši statuse jer ću id uzimati ručno !!!!!
  // status_obj.statuses = [];
  
  if ( status_obj._id || status_obj.status_id ) {
  
    Status.findOneAndUpdate(
      { _id: status_obj._id || status_obj.status_id },
      status_obj,
      { new: true }, function(err, status) {

        if (err) {
          console.log(`Error response prilikom UPDATE STATUSA`, err);
          res.json({success: false, error: err, msg: `Greška prilikom UPDATE STATUSA !!!` })
          return;
        };
        
        var clean_status = global.mng_to_js(status);
        clean_status.status_id = clean_status._id;
        
        res.json({success: true, status: clean_status });

      });
      // kraj ako je UPDATE
      
  } else {
    
    save_new_status(status_obj, res);

  }; // kraj od else ( NEMA _id ) 
    

};


function status_find_count(status_query) {
  
  return new Promise( function(resolve, reject) {
    
    Status.find(
    status_query,
    function(err, statuses) {

      if (err) {
        console.log(`Error response prilikom pretrage statusa sa querijem`, status_query );
        console.log(err);
        reject(err);
        return;
      };

      resolve({success: true, count: statuses.length });

    });
  
    
  }); // kraj promisa
  
};


exports.get_statuses_count = function(req, res) {
  
  
  var cit_my_tasks_query = {
    
    $or: [
      {
        "to.user_number": req.body.user_number,
        responded: null, 
        seen: null,
        "status_tip.is_work" : { $ne: true }
      }
    ]
    
  };
  
  /*
  if ( req.body.dep == "PRO" || req.body.dep == "PRO2" ) {
  
    cit_my_tasks_query.$or.push({
      $or: [ { "dep_to.sifra": "PRO" }, { "dep_to.sifra": "PRO2" } ],
      branch_done: false,
      "to.user_number": req.body.user_number,
    });
    
  };
  */
  
  // cit_my_tasks_query
  if ( req.body.dep == "PRO" || req.body.dep == "PRO2" ) {
    
    
    cit_my_tasks_query = {
    
    $or: [
        {
          "to.user_number": req.body.user_number,
          branch_done: false, 
        }
      ]
      
    };
    
    
    
  };
  
  var cit_task_from_me_query = {

    $or: [

      /* ako NIJE radni status onda trebam OD MENE */
      {
      "from.user_number": req.body.user_number,
      responded: null, 
      seen: null,
      "status_tip.is_work" : { $ne: true }
      },

      /* ako JESTE radni status onda trebam PREMA MENI -- to je samo indikator da netko radi na mom zahtjevu */
      {
      "to.user_number": req.body.user_number,
      responded: null, 
      seen: null,
      $or: [ { work_finished: null }, { work_finished: { $exists: false } } ],
      "status_tip.is_work" : true
      },  
    ]

  };
  
  var cit_working_query = {
    
    "from.user_number": req.body.user_number,
    work_finished: null,
    "status_tip.is_work" : true
    
  };

  var cit_my_tasks = null;
  var cit_task_from_me = null;
  var cit_working = null;
  
  status_find_count(cit_my_tasks_query)
  .then(function(cit_my_tasks_result) {
    
    cit_my_tasks = cit_my_tasks_result.count;
    return status_find_count(cit_task_from_me_query);
    
  })
  .then(function(cit_task_from_me_result) {
    
    cit_task_from_me = cit_task_from_me_result.count;
    return status_find_count(cit_working_query);
  })
  .then(function(cit_working_result) {
    
    cit_working = cit_working_result.count;
    
    res.json({
      success: true,
      cit_my_tasks: cit_my_tasks,
      cit_task_from_me: cit_task_from_me,
      cit_working: cit_working
    });
    
  })
  .catch(function(error) {
    
    res.json({ success: false, error: error, msg: `Error response prilikom pretrage statusa !!!` });
    
  });
  
  
};


exports.proces_rn_update_statuses = function(req, res) {
  
  /*
  OVAKO IZGLEDA REQUEST:
  ---------------------------------------------
  
  {
        
    rn_type: arg_rn_type, 
    kalk_id: kalk_id,
    proces_id: proces_sifra,
    user: {

      time: Date.now(),
      _id: window.cit_user._id,
      user_number: window.cit_user.user_number,
      full_name: window.cit_user.full_name,
      email: window.cit_user.email
    }
  }
  ---------------------------------------------
  
  
  */

  Status.updateMany(
  { kalk: req.body.kalk_id, proces_id: req.body.proces_id }, 
  { $set: { [req.body.rn_type]: req.body.user } },
  function(err, statuses) {
    if (err) {
      res.json({ success: false, error: err, msg: `Greška prilikom update radnog procesa  u statusu !!!` });
      return;
    };
    
    var clean_statuses = global.mng_to_js_array(statuses);
    
    // napokon response nakon update statusa na kojeg sam odgovorio
    res.json({success: true, statuses: clean_statuses });

  }); // kraj update replay for
  
};


