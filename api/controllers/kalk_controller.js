var mongoose = require('mongoose');

// mongoose.set('useFindAndModify', false);

var User = mongoose.model('User');
var Counter = mongoose.model('Counter');
var Grupa = mongoose.model('Grupa');

var Project = mongoose.model('Project');
var Product = mongoose.model('Product');
var Status = mongoose.model('Status');
var Kalk = mongoose.model('Kalk');

var nodemailer = require('nodemailer');
var multer = require('multer')

var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);


// var id = mongoose.Types.ObjectId('4edd40c86762e0fb12000003');
// var _id = mongoose.mongo.BSONPure.ObjectID.fromHexString("4eb6e7e7e9b7f4194e000001");

var find_controller = require('./find_controller.js');


exports.find_kalk = function(req, res) {

  global.cit_search_remote(req, res, Kalk)
  .then(function(result) {
    res.json(result) 
  })
  .catch(function(error) {
    res.json(error) 
  });
  
};


function save_new_kalk(kalk_obj, res) {
  
  // --------------------------------------------------------------------------
  var new_kalk = new Kalk(kalk_obj);
  // save to DB
  new_kalk.save(function(err, kalk) {
    
    if (err) {
      console.log(`Error response prilikom SAVE NOVE KALKLULACIJE`, err);
      res.json({success: false, error: err });
      return;
    };
    
    var clean_kalk = global.mng_to_js(kalk);
    
    // DODAJ OVAJ PRODUCT U ITEMS ARRAY U PROJEKTU
    Product.findOneAndUpdate(
      { _id: clean_kalk.product_id },
      { $set: { kalk: clean_kalk._id } },
      { new: true }, function(prod_err, product) {
        
        if (prod_err) {
          console.log(`Error prilikom SET kalk into product document`, err);
          res.json({success: false, error: prod_err });
        } else {
          res.json({success: true, kalk: clean_kalk });
        };
        
      });
    
  }); // kraj save new proj
  // --------------------------------------------------------------------------  
  
};


exports.save_kalk = function(req, res) {

  var kalk_obj = req.body;
      
  var kalk_type = kalk_obj.type;
  
  var set_query = null;
  
  if ( kalk_type == `offer` ) set_query = { $set: { offer_kalk: kalk_obj.offer_kalk, meta_offer_kalk: kalk_obj.meta_offer_kalk } };
  if ( kalk_type == `pro` ) set_query = { $set: { pro_kalk: kalk_obj.pro_kalk, meta_pro_kalk: kalk_obj.meta_pro_kalk } };
  if ( kalk_type == `post` ) set_query = { $set: { post_kalk: kalk_obj.post_kalk, meta_post_kalk: kalk_obj.meta_post_kalk } };
  if ( kalk_type == `all` ) set_query = kalk_obj; // spremi cjeli kalk !!!
  
      
  // AKO PRODUCT IMA ID ONDA JE OVO ZAPRAVO UPDATE !!!
  if ( kalk_obj._id ) {
  
    Kalk.findOneAndUpdate(
      { _id: kalk_obj._id },
      set_query,
      { new: true }, function(err, kalk) {

        if (err) {
          console.log(`Error response prilikom UPDATE KALKULACIJE`, err);
          res.json({success: false, error: err, msg: `Greška prilikom UPDATE KALKULACIJE !!!` })
          return;
        };
        
        var clean_kalk = global.mng_to_js(kalk);
        res.json({success: true, kalk: clean_kalk });

      });

    // kraj ako je UPDATE
    
  } 
  else {
    

    save_new_kalk(kalk_obj, res);
    

  }; // kraj od else ( NEMA _id ) 
    

};

  
function run_update_kalk( clean_kalk, proces_sifra, key, key_sifra, val ) {

  
  $.each( clean_kalk.post_kalk[0].procesi, function(p_ind, proces) {


    if ( proces.row_sifra == proces_sifra ) {

      if ( $.isArray( proces[key] ) ) {

        $.each( proces[key], function(prop_ind, prop) {
          if ( prop.sifra == key_sifra ) {
            clean_kalk.post_kalk[0].procesi[p_ind][key][prop_ind] = val;
          };

        });

      }; // kraj ako je proces[key] array


      // ako je ovo objekt onda je key_sifra zapravo pod propery

      if ( $.isPlainObject( proces[key] ) ) { 

        $.each( proces[key], function(prop_key, prop) {

          if ( prop_key == key_sifra ) {
            
            clean_kalk.post_kalk[0].procesi[p_ind][key][prop_key] = val;
            
          };

        });

      };

    }; // kraj ako je nasao proces 

  }); // loop po post procesima !!!
  
  
  // -------------------------------- PRODON SIROV ID UPDATE ------------------------------------------------
  // -------------------------------- PRODON SIROV ID UPDATE ------------------------------------------------
  
  // traži po svim procesima
  $.each( clean_kalk.post_kalk[0].procesi, function(proc_ind, find_proces) {


    $.each( find_proces.izlazi, function(izlaz_ind, find_izlaz) {

      var curr_izlaz = find_izlaz;

      // ako izlaz ima id koji je zapisan u bazu
      if ( curr_izlaz.prodon_sirov_id ) {
        
        // ponovo loop po svim procesima
        $.each( clean_kalk.post_kalk[0].procesi, function(proc_ind_2, find_proces_2) {

          // -------------------------------------------po svim ulazima -------------------------------------
          $.each( find_proces_2.ulazi, function(ulaz_ind_2, find_ulaz_2) {

            // ako u procesu imam ULAZ koji je isti kao current izlaz  
            if ( find_ulaz_2.izlaz_sifra == curr_izlaz.sifra ) {

              clean_kalk.post_kalk[0].procesi[proc_ind_2].ulazi[ulaz_ind_2].prodon_sirov_id = curr_izlaz.prodon_sirov_id;

            };

          }); // loop po svim ulazima u procesu
          // --------------------------------------------po svim ulazima ------------------------------------

        });  // ponovo loop po svim procesima
        
      };

    }); // loop po svim izlazima  

  }); // loop po svim procesima
  // -------------------------------- PRODON SIROV ID UPDATE ------------------------------------------------
  // -------------------------------- PRODON SIROV ID UPDATE ------------------------------------------------
  
  
  return clean_kalk;
  
};

exports.update_post_kalk = function(req, res) {

  var kalk_id = req.body.kalk_id;
  var proces_sifra = req.body.proces_sifra;
  
  var key = req.body.key;
  var key_sifra = req.body.key_sifra;
  
  var val = req.body.val;
  
  

  Kalk.findOne(
  { _id: kalk_id },
  function(err, kalk) {

    if (err) {
      console.log(`Error response prilikom PRETRAGE KALKULACIJA ZA UPDATE POST KALKULACIJE`, err);
      res.json({success: false, error: err, msg: `Greška prilikom PRETRAGE KALKULACIJA ZA UPDATE POST KALKULACIJE !!!` })
      return;
    };

    var clean_kalk = global.mng_to_js(kalk);


    var updated_kalk = run_update_kalk(clean_kalk, proces_sifra, key, key_sifra, val );

    Kalk.findOneAndUpdate(
    { _id: kalk_id },
    updated_kalk,
    { new: true }, function(err, kalk) {

      if (err) {
        console.log(`Error response prilikom UPDATE POST KALKULACIJE`, err);
        res.json({success: false, error: err, msg: `Greška prilikom UPDATE POST KALKULACIJE !!!` })
        return;
      };

      var clean_kalk = global.mng_to_js(kalk);
      
      res.json({success: true, kalk: clean_kalk });

    }); // kraj update kalkulacije 

  }); // kraj pretrage kalkulacija find one

};

  


exports.update_kalk_product_name = function(req, res) {
  
  var product_id = req.body.product_id;
  var full_product_name = req.body.full_product_name;
  
  Kalk.findOneAndUpdate(
    { product_id: product_id },
    { $set: {  full_product_name: full_product_name } },
    { new: true }, function(err, kalk) {

      if (err) {
        console.log(`Error response prilikom UPDATE KALK PRODUCT NAME`, err);
        res.json({success: false, error: err, msg: `Error response prilikom UPDATE KALK PRODUCT NAME !!!` })
        return;
      };

      var clean_kalk = global.mng_to_js(kalk);
      
      res.json({success: true, full_product_name: full_product_name });

    }); // kraj update kalkulacije 
  
};
