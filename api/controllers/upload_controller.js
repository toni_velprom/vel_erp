var mongoose = require('mongoose');

// mongoose.set('useFindAndModify', false);

var User = mongoose.model('User');
var Counter = mongoose.model('Counter');
var Product = mongoose.model('Product');

var nodemailer = require('nodemailer');

var multer = require('multer');

var fs = require('fs');
var path = require("path");
var { spawn } = require("child_process");
var { exec } = require("child_process");
var server_settings = require('../../settings');


var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);

var find_controller = require('./find_controller.js');



global.files_to_delete = [];


function delete_temp_file(temp_path) {
  
      fs.unlink( temp_path , (err) => {
      if (err) {
        throw err;
        return;
      };
      console.log(`Deleted TEMP file ----->  ` + temp_path);
    });
  
};


clearInterval(delete_temp_files_interval);

var delete_temp_files_interval = setInterval( function() {
  
  
  if ( files_to_delete.length > 0 ) {
    
    var curr_time = Date.now();
    var arr_length = files_to_delete.length;
    var i;
    for( i = arr_length - 1 ; i >= 0 ; i--) {
      
      // ako je prošlo više od 5 min onda obriši temp file
      if ( curr_time - files_to_delete[i].time > 1000 * 60 * 5 ) {
        delete_temp_file(files_to_delete[i].path);
      };
      
    }; // kraj loop po arraju od zadnjeg do prvog
    
  }; // kraj ako ima nešto u arr za delete
  
  
}, 1000 * 60 * 5 ); // loop svakih 5 minuta



var docs_storage = multer.diskStorage({
  
  destination: function (req, file, cb) {
    
    var yyyy = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    if (month < 10) month = `0` + month;
    
    cb(null, global.appRoot + `/public/docs/${yyyy}/${month}` )
  },
  filename: function (req, file, cb) {
    // ako postoji partner sifra onda ju stavi na početak imena
    var partner_sifra = (req.body && req.body.partner_sifra) ? (req.body.partner_sifra+"-") : "";
    // cb(null, file.originalname + '-' + Date.now() + `.png`)
    cb( null,  partner_sifra + Date.now() + "-" + global.filename_safe(file.originalname) );
  }
});



var axios = require('axios').default;
axios.defaults.adapter = require('axios/lib/adapters/http');



function run_compress_exec(file_obj, file_index, req, res) {

  return new Promise( function(resolve, reject) {
    
    if ( file_obj.mimetype.indexOf(`image`) > -1 && file_obj.size > 350000 ) {
    
      
      var yyyy = new Date().getFullYear();
      var month = new Date().getMonth() + 1;
      if (month < 10) month = `0` + month;

      var doc_path = global.appRoot + `/public/docs/${yyyy}/${month}/`;
      // var compressed_name = `compressed_` + file_obj.filename.substring(0, file_obj.filename.lastIndexOf(".") );
      
      var compressed_name = `compressed_` + file_obj.filename;
      
      // var ext = file_obj.filename.substring( file_obj.filename.lastIndexOf(".")+1 );
      // var compress_command = `convert ${doc_path + file_obj.filename} -define jpeg:extent=350kb ${doc_path + compressed_name}.jpg`;
      
      var compress_command = `convert ${doc_path + file_obj.filename} -resize 2000x2000 ${doc_path + compressed_name}`;

      exec( compress_command, function(error, stdout, stderr) {

        if (error) {
          delete_temp_file(`${doc_path + file_obj.filename}`);
          console.log(`compress_command -----> error: ${error.message}`);

          res.json({
              success: false,
              ime_filea: file_obj.filename,
              msg: error.message,
            });


          resolve(null);

          return;
        };

        if (stderr) {

          if ( stderr.indexOf(`Loading`) == -1 ) {

            console.log(`compress_command -----> stderr: ${stderr}`);
            delete_temp_file(`${doc_path + file_obj.filename}`);

            res.json({
              success: false,
              ime_filea: file_obj.filename,
              msg: stderr
            });

            resolve(null);

            return;
          };

        };

        console.log(`compress_command -----> stdout: ${stdout}`);

        
        global.files_to_delete.push({ path: `${doc_path + file_obj.filename}`, time: Date.now() });
        
        // delete originalni file nakon 2 sekunde tako da izbjegnem save i  delete u isto vrijeme
        // setTimeout( function() { delete_temp_file(`${doc_path + file_obj.filename}`); }, 2000);

        // resolve(compressed_name + `.jpg`);
        
        resolve( compressed_name );
        

      }); // kraj EXEC
    
    }
    else {
      
      resolve( file_obj.filename );
      
    }
    
  }); // kraj promisa  
    
};

function compress_files(files_arr, req, res) {
  
  
  var curr_time = Date.now();
 
  /*
  OVAKO IZGLEDA FILE OBJECT
  ------------------------------------------------------------------------------------------------
  destination: "/Users/tonikutlic/TONI/2021/VELPROM/VEL_ERP/public/docs"
  encoding: "7bit"
  fieldname: "cit_docs"
  filename: "1656419555183-velprom_alt_2.png"
  mimetype: "image/png"
  originalname: "VELPROM ALT 2.png"
  path: "/Users/tonikutlic/TONI/2021/VELPROM/VEL_ERP/public/docs/1656419555183-velprom_alt_2.png"
  size: 8 392 131
  ------------------------------------------------------------------------------------------------
  */
  
  
  async function run_compress(file_obj, file_index, req, res) {
    
    var compressed_filename = await run_compress_exec(file_obj, file_index, req, res);
      
    if ( compressed_filename ) files_arr[file_index].filename = compressed_filename;
    
    // prekini rekurziju ako je BILO KOJI OD COMPRESS REZULTATA FAILED
    // prekini rekurziju ako je BILO KOJI OD COMPRESS REZULTATA FAILED
    // prekini rekurziju ako je BILO KOJI OD COMPRESS REZULTATA FAILED
    
    if ( compressed_filename === null ) return;
    
    
    if ( file_index < files_arr.length-1 ) {
      run_compress( files_arr[file_index+1], file_index+1, req, res );
    }
    else {
      // ----------------- KRAJ  - SVE SLIKE SU GOTOVOVE
      res.json({
        success: true,
        msg: "Dokumenti su spremljeni !",
        files: files_arr,
        time: curr_time
      });

    };

    
  };
  
  
  run_compress(files_arr[0], 0, req, res);
  
};



global.upload_docs = function(req, res) { 

  
  
  var curr_time = Date.now();
  
  console.log(req);
  
  var docs_upload = multer({ storage: docs_storage }).array('cit_docs', 999);

  docs_upload(req, res, function (err) {
    
    if (err instanceof multer.MulterError) {
       
      res.json({
        success: false,
        msg: "Došlo je do greške kod spremanja dokumenata.<br>ERROR: 005",
        filename: null,
        err: err
      });
      
      return;
      
    };
    
    if ( !req.files ) {
      
      res.json({
        success: false,
        msg: "Došlo je do priliko prijenosa dokumenata na server.<br>Probajte ponovo!<br>ERROR: 006",
        filename: null
      })  

    };
      
    if ( req.body.user_number ) {
        
      /*  
      User.findOneAndUpdate(
          { user_number: Number(req.body.user_number) },
          { $set: { avatar: `/img/avatars/${req.file.filename}` }},
          {new: true}, 
          function(err, user) {
            
            if (err) {
              res.json({
                success: false,
                msg: "Došlo je do greške kod update-a avatara u bazi usera<br>Probajte ponovo!",
                filename: null
              });
              return;
            };
            
            res.json({
              success: true,
              msg: "Avatar je ažuriran i spremljen kod usera!",
              filename: req.file.filename
            });
  
          });    
        
        */
        
    } 
    else {
    
      // AKO NE TREBA KOMPRESIJA 
      // AKO NE TREBA KOMPRESIJA 
      if ( !req.body.for_gallery ) {
        
        res.json({
          success: true,
          msg: "Dokumenti su spremljeni !",
          files: req.files,
          time: curr_time
        });
      }
      else {
        // KOMPRESIJA
        compress_files(req.files, req, res);
        
      };
        

    }; // kraj jel ima ili nema user number
  
  });
  
};
// kraj upload docs



global.new_doc_counter = function(counter_name, month_reset) {
  
  return new Promise( function (resolve, reject) {
    
    var query = {};
    query[counter_name] = { $ne: null };
    
    // var inc = { $inc: { [counter_name]: 1 } };
    
        
    Counter.findOne( query )
    .exec(function(read_err, read_counter) {
      
      
      if (read_err) {
        
          reject({ 
            success: false, 
            msg: "Dokument nije spremljen! Counter problem.",
          });
        return;
      };  
      
      
      var clean_count = global.mng_to_js(read_counter);
      
      var date = new Date();
      
      var year = String( date.getFullYear() );
      year = year.slice(-2); // samo zadnje dvije znamenke u god
      var current_month = date.getMonth(); 
      
      var counter_number = clean_count[counter_name] + 1; // plus jedan UVIJEK 
      var counter_month = clean_count.month;
      
      
      
      // ako je month reset FALSE onda sve ovo preskoči !!!!
      if (  month_reset !== false ) {
        // ako nije isti mjesec onda RESET COUNTER
        if ( current_month !== counter_month ) counter_number = 1; // ----> osim kada pređe u sljedeći mjesec :)
      };
      
        
      Counter.findOneAndUpdate(
        query,
        { $set:{ month: current_month, [counter_name]: counter_number } }, 
        { "new": true }
        )
        .exec(function(err, counter) {
        
          if (err) {

              reject({ 
                success: false, 
                msg: "Dokument nije spremljen! Counter problem.",
              });

            return;
          };  

          var clean_count = global.mng_to_js(counter);

          resolve({
            
            success: true,
            year: year.padStart(2, "0"),
            month: String(current_month+1).padStart(2, "0"),
            number: counter_number, // String(counter_number).padStart(4, "0"),
            real_number: counter_number,
            
          });

        }); // kraj countera    
  
    }); // kraj findcountera    
    
  }); // kraj promisa
  
};


exports.get_new_counter = function(req, res) {
  
  var month_reset = null
  
  if ( typeof req.body.month_reset !== `undefined` ) month_reset = req.body.month_reset;

  global.new_doc_counter(req.body.doc_type, month_reset )
  .then(function(result) {
    res.json(result) 
  })
  .catch(function(error) {
    res.json(error) 
  });
  
};



global.save_pdf = function(req, res) {
  
  var html_sadrzaj = req.body.html;
  var br_user_generirao = req.body.user.user_number;
  var full_name_user_generirao = req.body.user.full_name;
  var orient = req.body.orient;
  
  full_name_user_generirao = global.string_safe(full_name_user_generirao);
  
  var doc_type = req.body.doc_type;
  
  // var ime_filea = getMeADateAndTime().datum + '_' + getMeADateAndTime().vrijeme + "-" + 'generirana-ponuda.pdf';  
  var time_stamp = Date.now();
  
  var ime_filea = time_stamp + "_" + br_user_generirao + "-" + full_name_user_generirao + "-" + doc_type;  

  var yyyy = new Date().getFullYear();
  var month = new Date().getMonth() + 1;
  if (month < 10) month = `0` + month;
  
  var doc_path = global.appRoot + `/public/docs/${yyyy}/${month}/`;
  
  fs.writeFileSync( `${doc_path + ime_filea}.html`, html_sadrzaj );
  
  var make_pdf_command = 
      `wkhtmltopdf --enable-local-file-access --disable-smart-shrinking --footer-center [page]/[topage] -O ${orient} ${doc_path + ime_filea}.html ${doc_path + ime_filea}.pdf`

  
  exec( make_pdf_command, function(error, stdout, stderr) {
    
    if (error) {
      delete_temp_file(`${doc_path + ime_filea}.html`);
      console.log(`make_pdf_command -----> error: ${error.message}`);
      
      res.json({
          success: false,
          ime_filea: ime_filea,
          msg: error.message,
        });
      
      return;
    };
    
    if (stderr) {
      
      if ( stderr.indexOf(`Loading`) == -1 ) {
        
        console.log(`make_pdf_command -----> stderr: ${stderr}`);
        delete_temp_file(`${doc_path + ime_filea}.html`);
        
        res.json({
          success: false,
          ime_filea: ime_filea + `.pdf`,
          msg: stderr
        });
        
        return;
      };
      
    };

    console.log(`make_pdf_command -----> stdout: ${stdout}`);
  
    delete_temp_file(`${doc_path + ime_filea}.html`);
    
    res.json({
      success: true,
      ime_filea: ime_filea + `.pdf`,
      time: time_stamp
    });
    


  });

  
};
exports.save_pdf = global.save_pdf;


exports.hnb_tecaj = function(req, res) {
  
  // axios.get('https://api.hnb.hr/tecajn/v2')
  axios.get('https://api.hnb.hr/tecajn-eur/v3')
  .then(function (response) {
    
    var data = response.data;
    
    console.log('response data od hnb tečaj:')
    console.log(data);
    
    if (response.status == 200 ) {
      
      res.setHeader('Access-Control-Allow-Origin', '*');
      // Request methods you wish to allow
      res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
      // Request headers you wish to allow
      res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
      
      res.json({ success: true, result: data });

    } else {
      // ako nije uspješna verifikacija captche !!!!!!
      res.json({ success: false, msg: 'HNB ERROR', error: response });
    };
    
  })
  .catch(function (error) {
    
    // AXIOS REQ ERROR !!!!
    console.log(error);
    res.json({ success: false, msg: 'AXIOS REQ ERROR', error: error });
    
  });
  
};


exports.get_new_sub_nacrt = function(req, res) {
  
  Product.find( { "nacrt_specs.template_num": req.body.template_num, "nacrt_specs.tip.sifra": req.body.tip })
  .exec(function(prod_err, products) {

    if ( prod_err ) {

        reject({ 
          success: false, 
          msg: "Greška kod pronalaska sljedećeg broja za SUB nacrt.",
        });

      return;

    };  

    
    var sub_max = 0;
    
    if ( products.length > 0 ) {
      
      var clean_prods = global.mng_to_js_array(products);
      
      $.each(clean_prods, function(p_ind, prod) {
        $.each(prod.nacrt_specs, function(n_ind, nacrt) {
          
          if ( nacrt.template_num == req.body.template_num && nacrt.tip.sifra == req.body.tip ) {
            if ( nacrt.sub > sub_max ) sub_max = nacrt.sub;
          };
          
        });
      });
      
    };
    
    res.json({ success: true, sub: sub_max+1 })

  }); // kraj find product
  
};


async function send_mail_from_user(req, res) {
  
  var { arg_body, arg_subject, to_arr, cc_arr, bcc_arr, att_files, arg_from } = req.body;
  
  var result = await global.cit_send_mail( arg_body, arg_subject, to_arr, cc_arr, bcc_arr, att_files, arg_from );

  res.json(result);
  
};
exports.send_mail_from_user = send_mail_from_user;

