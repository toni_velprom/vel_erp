// process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

var server_settings = require('./settings');


var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);


var CountersModel = require('./api/models/counters_model');
var PaletSifra = require('./api/models/palet_sifra_model');
var UserModel = require('./api/models/user_model');


var ProjectModel = require('./api/models/project_model');
var ProductModel = require('./api/models/product_model');
var StatusModel = require('./api/models/status_model');

var PartnerModel = require('./api/models/partner_model');
var SirovModel = require('./api/models/sirov_model');

var ProductModel = require('./api/models/product_model');

var PlateModel = require('./api/models/plate_model');

var GrupacijaModel = require('./api/models/grupacija_model');
var GrupaModel = require('./api/models/grupa_model');
var AdminModel = require('./api/models/admin_model');

var KalkModel = require('./api/models/kalk_model');
var SkladModel = require('./api/models/sklad_model');

var AlatShelfsModel = require('./api/models/alat_shelfs_model');


var KvizModel = require('./api/models/kviz_model');
var TestTemplateModel = require('./api/models/test_template_model');

var auth_rules = require('./auth_rules');

var cit_email = require('./cit_email');

var path = require('path');
global.appRoot = path.resolve(__dirname);

global.cit_domain = server_settings.domain;
// global.cit_domain = `http://localhost:8080`;

var compile_files = require('./compile_files');

var express = require('express');
var cors = require('cors');
var app = express();
var https = require('https');
var server = require('http').Server(app);

var fs = require('fs');

var privateKey = null;
var certificate = null;
var ca = null;
var https_server = null;

global.cit_io = null;

// var rampa_controller = require('./api/controllers/rampa_controller.js');


if ( server_settings.ip_address !== '0.0.0.0' ) {
  
  
/*
  privateKey = fs.readFileSync('privkey1.pem', 'utf8');
  certificate = fs.readFileSync('cert1.pem', 'utf8');
  ca = fs.readFileSync('chain1.pem', 'utf8');

  https_server = https.createServer({
      key: privateKey,
      cert: certificate,
      ca: ca
    }, app);

  global.cit_io = require('socket.io')(https_server);
*/
  
  
  global.cit_io = require('socket.io')(server);
  
  
} else {
  
  global.cit_io = require('socket.io')(server);
  
};




var imap_controller = require('./api/controllers/imap_controller');


var socket_controller = require('./api/controllers/socket_controller');
var socket_routes = require('./api/routes/socket_routes');
socket_routes(app, socket_controller);


global.dev_or_prod = 'prod';


/*
global.vel_email_address = 'toni@circulum.it';
global.vel_mail_setup = {
  host: 'mail.circulum.it',
  port: 587,
  secure: false, // use TLS,
  requireTLS: true,
  auth: {
    user: global.vel_email_address,
    pass: 'ToNi3007'
  },
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false
  }
};
*/



global.velprom_footer =
  
`
<br style="display: block; clear: both;">

<b style="font-size: 16px; font-weight: 700;">Velprom d.o.o.</b><br>
<b>OFFICE:</b>&nbsp;Poštanska 7, 10410 Velika Gorica<br>
<b>OIB:</b>&nbsp;33428207264, <b>MB:</b>&nbsp;3528090<br>
<b>TEL:</b>&nbsp;+385 1 6215 541<br>
<b>FAX:</b>&nbsp;+385 1 2099 483<br>
<b>MAIL:</b>&nbsp;info@velprom.hr<br>
<b>WEB:</b>&nbsp;www.velprom.hr<br>
<br>
<div style="font-size: 10px; border-top: 1px solid #e8e6e6; padding-top: 20px; max-width: 350px; margin: 0 auto;">
  Tvtka je registrirana na Trgovačkom sudu u Zagrebu sa sjedištem: Dalmatinska 27, Gradići; 10410 Velika Gorica, Hrvatska&nbsp;
  IBAN : HR7225000091101346755 Addiko Bank d.d., Iznos temeljnog kapitala: 20.000,00
</div>
<br style="display: block; clear: both;">

`;


// these methods all returned promise
// checkAuth will auto invoke and it will check smtp auth

  

if ( global.dev_or_prod == 'dev' ) {
  
} 
else if ( global.dev_or_prod == 'prod' ) {
  
};



var port = process.env.PORT || server_settings.port;

var mongoose = require('mongoose');
// mongoose.set('useFindAndModify', false);
mongoose.Promise = global.Promise;

var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var scramblePass = require('password-hash-and-salt');

// configure app to use bodyParser()
// this will let us get the data from a POST

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));


/*  
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
*/

app.set('json spaces', 2);
app.use(cors());


/*
--------------------------------------------
CONNECT TO MONGO DB
--------------------------------------------
*/

global.conn_to_db = function() {

  var conn = mongoose.connect(
    
    server_settings.mongodb_connect,
    
    { useNewUrlParser: true, useUnifiedTopology: true }
  )
  .then(
    (conn) => {
      console.log("Conected to Velprom DB");
    },
    err => {
      console.log("NOT Conected to Velprom DB. This is ERROR:");
      console.log(err);
    }
  );

};

global.conn_to_db();

/*

--------------------------------------------
CONNECT TO MONGO DB
--------------------------------------------
*/



/* ------- START ------------------- THIS IS GATEKEEPER !!!!! --------------------------*/


// ATTACH GATE KEEPER TO ALL ROUTES !!!!!!!!!!!
app.route('/*')
    .get(auth_rules.gatekeeper)
    .post(auth_rules.gatekeeper)
    .put(auth_rules.gatekeeper);

/* ------- END -------------------  GATEKEEPER --------------------------*/



/* -----------------START-------------------------  GLOBALS   ----------------------------------------------------- */


global.cit_round = function (broj, brojZnamenki) {

  var broj = parseFloat(broj);

  if ( typeof brojZnamenki == 'undefined' ) {
   brojZnamenki = 0;
  };
  var multiplicator = Math.pow(10, brojZnamenki);
  broj = parseFloat((broj * multiplicator).toFixed(11));
  var test =(Math.round(broj) / multiplicator);
  // + znak ispred je samo js trik da automatski pretvori string u broj
  return +(test.toFixed(brojZnamenki));
  
};

global.cit_rand = function() {
  var rand_id = String( Math.random()+Date.now() ).replace(`.`, ``);
  return rand_id;
};


global.cit_deep = function(data) {
  data = JSON.stringify(data);
  data = JSON.parse(data);
  return data;
};

global.mng_to_js_array = function (mongoose_array) {
  
  var clean_arr = []; 
  $.each( mongoose_array, function(mng_index, mongoose_object) {
    
    var js_object = mongoose_object;
    
    try{
      js_object = mongoose_object.toJSON();  
    }
    catch {
      console.log(`mongoose objekt je već vjerovatno pretvoren u javascript objekt`);
    };
    
    js_object = JSON.stringify(js_object);
    js_object = JSON.parse(js_object);
    clean_arr.push(js_object);
  });  
  return clean_arr;
};

global.mng_to_js = function (mongoose_object) {
  
  var js_object = mongoose_object;
    
  try{
    js_object = mongoose_object.toJSON();  
  }
  catch {
    console.log(`mongoose objekt je već vjerovatno pretvoren u javascript objekt`);
  };
  
  js_object = JSON.stringify(js_object);
  js_object = JSON.parse(js_object);
  
  return js_object;
  
};

global.filename_safe = function(str) {
  
  var just_name = str.substring(0, str.lastIndexOf(".") );
  var ext = str.substring( str.lastIndexOf(".")+1 );

  just_name = just_name.replace(/\-/gi, "_");  
    
  just_name = just_name.replace(/š/gi, "s");
  just_name = just_name.replace(/đ/gi, "dj");
  just_name = just_name.replace(/ć/gi, "c");
  just_name = just_name.replace(/ž/gi, "z");
  just_name = just_name.replace(/[^a-z0-9_\-]/gi, '_').toLowerCase();
  
  return (just_name + "." + ext);
  
};

global.string_safe = function(str) {

  var safe = str.replace(/\-/gi, "_");  
    
  safe = safe.replace(/š/gi, "s");
  safe = safe.replace(/đ/gi, "dj");
  safe = safe.replace(/ć/gi, "c");
  safe = safe.replace(/ž/gi, "z");
  safe = safe.replace(/[^a-z0-9_\-]/gi, '_').toLowerCase();
  
  return safe;
  
};

global.concat_full_adresa = function(data, arg_vrsta_adrese) {

  var full_adresa = "";
  var found_sjediste = false;

  if (data.adrese && data.adrese.length > 0) {
    
    
    if ( arg_vrsta_adrese ) {

      $.each(data.adrese, function (index, adresa_obj) {
      
        if ( adresa_obj.vrsta_adrese && adresa_obj.vrsta_adrese.sifra == arg_vrsta_adrese ) {

          var adresa = adresa_obj.adresa;
          var kvart = adresa_obj.kvart ? (", " + adresa_obj.kvart) : "";
          var naselje = adresa_obj.naselje ? (", " + adresa_obj.naselje) : "";
          var posta = adresa_obj.posta ? (", " + adresa_obj.posta) : "";
          var mjesto = adresa_obj.mjesto ? (", " + adresa_obj.mjesto) : "";
          var drzava = adresa_obj.drzava ? (", " + adresa_obj.drzava.naziv) : "";

          full_adresa = adresa + kvart + naselje + posta + mjesto + drzava;

        }; // kraj ako je sjedište

      });

      
    } 
    else {
      
      // ako nema uvijeta koja vrsta adrese treba biti onda traži bilo koju adresu
      $.each(data.adrese, function (index, adresa_obj) {

        var adresa = adresa_obj.adresa;
        var kvart = adresa_obj.kvart ? (", " + adresa_obj.kvart) : "";
        var naselje = adresa_obj.naselje ? (", " + adresa_obj.naselje) : "";
        var posta = adresa_obj.posta ? (", " + adresa_obj.posta) : "";
        var mjesto = adresa_obj.mjesto ? (", " + adresa_obj.mjesto) : "";
        var drzava = adresa_obj.drzava ? (", " + adresa_obj.drzava.naziv) : "";

        full_adresa = adresa + kvart + naselje + posta + mjesto + drzava;

      });
      
    };
      
  }; // kraj ako postoje adrese u partneru


  return full_adresa;

};

global.concat_naziv_sirovine = function(data) {

  var full_naziv = 
      (data.klasa_sirovine ? data.klasa_sirovine.naziv+" " : "") +
      (data.tip_sirovine ? data.tip_sirovine.naziv+" " : "") +
      (data.vrsta_materijala ? data.vrsta_materijala.naziv+" " : "") +
      (data.naziv ? data.naziv + " " : "") +
      (data.kvaliteta_1 ? data.kvaliteta_1 + "/" : "") +
      (data.kvaliteta_2 ? data.kvaliteta_2 + " " : "") +
      (data.po_valu ? data.po_valu + "X" : "") +
      (data.po_kontravalu ? data.po_kontravalu : "");

  return full_naziv;

};

global.cit_dt = function(number, cit_format) {
   
  var today = null;

  // ako nije undefined ili null onda napravi datum od broja 
  if ( number || number == 0 ) {
    today = new Date( number );  
  } else {
    today = new Date();  
  };

  var sek = today.getSeconds();
  var min = today.getMinutes();
  var hr = today.getHours();
  var dd = today.getDate();
  var mm = today.getMonth() + 1; // January is 0!
  var yyyy = today.getFullYear();

  // ako je samo jedna znamenka
  if (dd < 10) {
    dd = '0' + dd;
  };
  // ako je samo jedna znamenka
  if (mm < 10) {
    mm = '0' + mm;
  };

  // ako je samo jedna znamenka
  if (sek < 10) {
    sek = '0' + sek;
  };


  // ako je samo jedna znamenka
  if (min < 10) {
    min = '0' + min;
  };

  // ako je samo jedna znamenka
  if (hr < 10) {
    hr = '0' + hr;
  };

  if ( !cit_format || cit_format == 'd.m.y' ) {
    today =  dd + '.' + mm + '.' + yyyy;
  };


  // ako nije neveden format ili ako je sa crticama
  if ( cit_format == 'y_m_d' ) {
    today = yyyy + '_' + mm + '_' + dd;
  };  

  // ako nije neveden format ili ako je sa crticama
  if ( cit_format == 'y-m-d' ) {
    today = yyyy + '-' + mm + '-' + dd;
  };


  time = hr + ':' + min + ':' + sek;


  return {
    date: today,
    time: time
  }

};

// ------------------------------------------------ MULTISORT  
(function (root, factory) {
  // From: https://github.com/umdjs/umd/blob/d31bb6ee7098715e019f52bdfe27b3e4bfd2b97e/templates/returnExports.js
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define([], factory);
  } else if (typeof module === 'object' && module.exports) {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = factory();
    global.multisort = factory();
    
  } else {
    // Browser globals (root is window)
    root.multisort = factory();
    global.multisort = factory();
    
  }
}(this, 
  function () {

  function mapArray(items, cb) {
    if (!Array.isArray(items)) {
      throw new Error('Not supported');
    }
    var mappedItems = [];
    for (var i = 0; i < items.length; i++) {
      var mapped = cb(items[i]);
      mappedItems.push(mapped);
    }
    return mappedItems;
  }

  // For each item in the input array, transform it into an object with two keys.
  // 'item' stores the item, and 'values' is an array with the results of each
  // evaluator applied to the item.
  var evaluateItems = function(input, evaluators) {
    for (var i = 0, len = input.length; i < len; i++) {
      var item = input[i];
      input[i] = {
        item: item,
        values: mapArray(evaluators, function(evaluator) {return evaluator.func(item)})
      }
    }
  }

  // The opposite of the above function.
  // Reverts the input array back to its original format.
  var revertItems = function(input) {
    for (var i = 0, len = input.length; i < len; i++) {
      input[i] = input[i].item;
    }
  }

  // Users can pass three types of criteria - functions, strings, and numbers.
  var makeEvaluator = function(input) {
    if (isFunction(input)) {
      return makeFunctionalEvaluator(input);
    } else if (isString(input)) {
      return makeStringEvaluator(input);
    } else if (isNumber(input)) {
      return makeNumericalEvaluator(input);
    }
    throw "Improper input for comparator!"
  };

  // Functional evaluators don't need any transformation, and can't have inverted order.
  var makeFunctionalEvaluator = function(input) {
    return {
      func: input,
      invert: false
    }
  }

  // Numerical evaluators sort the input directly if the criterion is non-negative,
  // and in inverted order if the criterion is negative.
  var makeNumericalEvaluator = function(input) {
    return {
      func: function(item) {return item},
      invert: (input < 0)
    }
  };

  var makeStringEvaluator = function(input) {
    // Invert the sort if initial character is ! or ~.
    var invert;
    if (input[0] === "!" || input[0] === "~") {
      input = input.slice(1);
      invert = true;
    }

    // Allow an initial dot: ".prop.subprop" as well as "prop.subprop"
    if (input[0] === ".") {
      input = input.slice(1);
    }

    if (input[input.length - 1] === "?") {
      input = input.slice(0, -1);
      return {
        func: function(item) {return nestedProperty(item, input) != null},
        invert: invert
      }
    } else {
      return {
        func: function(item) {return nestedProperty(item, input)},
        invert: invert
      }
    }
  };

  var nestedProperty = function(obj, path) {
    if (path === "") return obj
    path = path.split(".")
    var current = obj;
    while (path.length) {
      var nextKey = path.shift()

      if (/[^\(\r\n]*\([^\(\r\n]*\)$/.test(nextKey)) {
        var indexOfOpenParenthesis = nextKey.indexOf("(");
        var args = JSON.parse("[" + nextKey.slice(indexOfOpenParenthesis+1, -1) + "]");
        nextKey = nextKey.slice(0, indexOfOpenParenthesis);
      }

      // If key is a function...
      if (args) {
        current = current[nextKey].apply(current, args);
      } else {
        current = current[nextKey];
      }

      // Stop going through the path if we reach a null or undefined
      if (current == null) return null;
    }
    return current;
  };


  var isFunction = function(input) {
    return typeof input === "function";
  };

  var isNumber = function(input) {
    return typeof input === "number";
  };

  var isString = function(input) {
    return typeof input === 'string';
  };

  return function(toSort, sortings) {
    // Allow partial application.
    if (arguments[1] == null) {
      var partialApplication = true;
      sortings = toSort;
    }

    if (!Array.isArray(sortings)) {
      sortings = [sortings]
    }

    // Turn each sorting into a function that evalutes an item.
    var evaluators = mapArray(sortings, makeEvaluator);

    if (partialApplication) {
      var sortFunction = function(toSort) {
        return module.exports(toSort, sortings)
      };
      // To allow this to plug in to other sorting mechanisms.
      sortFunction.comparator = function(a, b) {
        for (var i = 0; i < evaluators.length; i++) {
          var evaluator = evaluators[i];
          var invert = evaluator.invert;
          var aValue = evaluator.func(a);
          var bValue = evaluator.func(b);

          if (aValue > bValue) {
            return invert ? -1 : 1;
          } else if (bValue > aValue) {
            return invert ? 1 : -1;
          }
        }
        return 0;
      }
      return sortFunction;
    }

    // For each item, decorate it with the results of each evaluator.
    evaluateItems(toSort, evaluators);

    // Sort by the decorated results.
    toSort.sort(function(a, b) {
      var aValues = a.values;
      var bValues = b.values;
      for (var i = 0, len = evaluators.length; i < len; i++) {
        var invert = evaluators[i].invert;
        var aValue = aValues[i];
        var bValue = bValues[i];

        if (aValue > bValue) {
          return invert ? -1 : 1;
        } else if (bValue > aValue) {
          return invert ? 1 : -1;
        }
      }
      return 0;
    });

    // Undecorate each item to return cleanly.
    revertItems(toSort);
    return toSort

  };

}));


global.find_item = function(array, value, prop) {
  
  var obj = null;
  
  $.each( array, function(index, element) {
    
    if ( $.isPlainObject(element) ) {
      // ----------------------------------------------------------------
      // loopaj sve dok je obj null
      // ali kad nađe prvi element onda STANI !!!! 
      if ( obj == null && element[prop] == value ) {
        obj = element;
      };
      // ----------------------------------------------------------------
    } else if ( typeof element == "number" || typeof element == "string" ) {
      
      if ( obj == null && element == value ) obj = element;
      // ----------------------------------------------------------------
    };
      
  });
  
  return obj;
  
};

global.find_all_items = function(array, value, prop) {
  
  let result_arr = [];
  
  $.each( array, function(index, element) {
    // loopaj sve dok je ind null
    // ali kad nađe prvi element onda STANI !!!! 
    if ( element[prop] == value ) {
      result_arr.push(element);
    };
  });

  if ( result_arr.length == 0 ) result_arr = null;
  
  return result_arr;
};

global.find_index = function(array, value, prop ) {
  
  let ind = null;
  
  $.each( array, function(index, element) {
    // loopaj sve dok je ind null
    // ali kad nađe ind onda STANI !!!! 
    if ( ind == null && element[prop] == value ) {
      ind = index;
    };
  });
  return ind;
};

  
  
  

global.upsert_item =  function(array, new_item, prop ) {
  
  var array_copy = global.cit_deep(array);
  
  let found_index = null;
  
  $.each( array_copy, function(index, element) {
    
    
    if ( $.isPlainObject(element) ) {
      if ( element[prop] == new_item[prop] ) {
        found_index = index;
      };
    } else if ( typeof element == "number" || typeof element == "string" ) {
      if ( element == new_item ) {
        found_index = index;
      }; 
    };
    
  });

  if ( found_index !== null ) {
    array_copy[found_index] = new_item;
  } else {
    if ( !array_copy || $.isArray(array_copy) == false ) array_copy = [];
    array_copy.push(new_item);
  };
  
  return array_copy;
  
};

global.delete_item = function(array, value, prop ) {
  
  var array_copy = global.cit_deep(array);
  

  if ( $.isArray(array_copy) == false ) {
    console.error(array, "nije ARRAY !!!!!");
    return array_copy;
  };
  
  
  if ( $.isArray(array_copy) == true && array_copy.length == 0 ) {
    console.error(array, "ARRAY JE PRAZAN !!!!!");
    return array_copy;
  };
  
  
  
  var index;
  for( index=array_copy.length-1; index >= 0; index-- ) {
    
    var found_index = null;
    var element = array_copy[index];
    
    if ( $.isPlainObject(element) || $.isArray(element) ) {
    
      if ( element[prop] == value ) {
        found_index = index;
      };
      
    } 
    else if ( typeof element == "number" || typeof element == "string" ) {
      
      if ( element == value ) {
        found_index = index;
      };
      
    };
    
    
    if ( found_index !== null ) {
      array_copy.splice(found_index, 1);
    };
    
  }; // kraj for loopa UNAZAD

  
  return array_copy;
};

// ovo npr koristim za nacrt sifre tako da mi ne ponavlja nacrte sa istim tempalte-om NEGO SAMO UNIQUE
// ovo npr koristim za nacrt sifre tako da mi ne ponavlja nacrte sa istim tempalte-om NEGO SAMO UNIQUE
global.insert_uniq = function (array, new_item, prop ) {
  
  var array_copy = global.cit_deep(array);
  
  let found_index = null;
  $.each( array_copy, function(index, element) {

    
    if ( $.isPlainObject(element) ) {
      if ( element[prop] == new_item[prop] ) {
        found_index = index;
      };
    } else if ( typeof element == "number" || typeof element == "string" ) {
      
      if ( element == new_item ) {
        found_index = index;
      };
      
    }

  });
  

  // samo ako nije našao objekt s istim propertijem
  if ( found_index == null ) {
    if ( !array_copy || $.isArray(array_copy) == false ) array_copy = [];
    array_copy.push(new_item);
  };
  
  return array_copy;
  
};

global.only_uniq = function (value, index, self) {
  return self.indexOf(value) === index;
};

// usage example:
var a = ['a', 1, 'a', 2, '1'];
var uni = a.filter(global.only_uniq);

// console.log(unique); // ['a', 1, 2, '1']




global.cit_ms_at_zero = function (time) {
  
  var date = new Date(time);
  
  var date_units = get_date_units(date);
  var date_at_zero = new Date(date_units.yyyy, date_units.mnt, date_units.day, 0, 0, 0 );
  return Number(date);
  
  
};




global.time_path = function(arg_time) {
  
  var path = ``;
  
  if( !arg_time ) {
    popup_warn(`Nedostaje vrijeme kreiranja dokumenta !!!!`);
    return;
  };
  
  var yyyy = new Date(arg_time).getFullYear();
  var month = new Date(arg_time).getMonth() + 1;
  if (month < 10) month = `0` + month;
  
  path = `${yyyy}/${month}`;
  
  return path;
  
};


global.get_doc_count = async function( DB_model, arg_query ) {
  try {
    return DB_model.countDocuments( arg_query ).exec();
  }
  catch ( err) {
    console.log(err);
    return ({ success: false, error: err });
  };
  
};










/* -----------------END-------------------------  GLOBALS   ----------------------------------------------------- */


var nl_controller = require('./api/controllers/news_letter_controller');


var upload_controller = require('./api/controllers/upload_controller');


var user_routes = require('./api/routes/user_routes');
user_routes(app);

var partner_routes = require('./api/routes/partner_routes');
partner_routes(app);


var sirov_routes = require('./api/routes/sirov_routes');
sirov_routes(app);


var proj_routes = require('./api/routes/project_routes');
proj_routes(app);


var product_routes = require('./api/routes/product_routes');
product_routes(app);


var status_routes = require('./api/routes/status_routes');
status_routes(app);

var admin_routes = require('./api/routes/admin_routes');
admin_routes(app);

var upload_routes = require('./api/routes/upload_routes');
upload_routes(app);

var kalk_routes = require('./api/routes/kalk_routes');
kalk_routes(app);

var sklad_routes = require('./api/routes/sklad_routes');
sklad_routes(app);

var plate_routes = require('./api/routes/plate_routes');
plate_routes(app);


app.use('/', express.static(path.join(__dirname, '/public'), {dotfiles:'allow'} ) );


app.use('*', function(req, res, next) {
  var contype = req.headers['content-type'];
  if ( contype && contype.indexOf('application/json') > -1 ) {
    // return res.send({error: 'No such end point to return json: ' + req.originalUrl  });
    return res.send({ msg: 'ENDPOINT NOT ACTIVE: ' + req.originalUrl });
  };
  next();
});


app.get('*', function (req, res) {
  // res.set('content-type','text/html');
  res.sendFile(path.resolve(__dirname+'/public/', 'index.html'));
});

// app.use('*', express.static(path.join(__dirname, '/public/404'), {dotfiles:'allow'} ) );


if ( server_settings.ip_address !== '0.0.0.0' ) {
  https_server.listen(port, server_settings.ip_address);
} else {
  server.listen(port, server_settings.ip_address);
};

console.log('VELPROM started at ' + server_settings.ip_address + ': ' + port);
