var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var nodemailer = require('nodemailer');

var fs = require('fs')

var path = require("path");

var xl = require('excel4node');


var ParkPlace = mongoose.model('ParkPlace');
var ParkAction = mongoose.model('ParkAction');

var User = mongoose.model('User');

var jwt = require('jsonwebtoken');


var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);



exports.cit_search_remote = function(req, res) {
  
  
  var token = req.headers['x-auth-token'] || null;
  
  
  // iznimka  je kada radim request za park places ---_> user ne mora biti ulogiran
  if ( token ) {
      
    check_user_auth(token).then(
      
    function( result ) {
      
      
      // iznimka  je kada radim request za park places ---_> user ne mora biti ulogiran
      if ( result.success == true  ) {
        
        // ovo je glavna funkcija za find
        run_find( req, res, result.super_admin, result.user );
        return;
        
        
      } else {
        res.json([]);
        return;
        
      };

    })
    .catch(function(err) {
      res.json([]); 
    });

  } else {
    // vrati prazan array ako nema token
    res.json([]);    
    
  };
  
}; // end of cit_search_remote 


function run_find( req, res, super_admin, arg_user ) {
  
  var DB_model = null;  
  
  if ( req.originalUrl == '/search_park_places') DB_model = ParkPlace;
  
  if ( req.originalUrl == '/search_pp_owners') DB_model = User;
  
  
  if ( req.originalUrl == '/stats_search_customers') DB_model = User;
  if ( req.originalUrl == '/search_customers') DB_model = User;
  
  if ( req.originalUrl == '/search_actions' ) DB_model = ParkAction;
  
  if ( req.originalUrl == '/search_regs' ) DB_model = User;
  
  
  if ( DB_model == null ) {
    res.send({ success: false, msg: 'No such end point for searching.' });
    return;
  };
  
  
  /*
  DB_model.estimatedDocumentCount({}, function (err, count) {
    console.log( 'OVO JE BROJ DOKUMENATA U ' + req.originalUrl + ' kolekciji  : '  +  count );
  
  });
  */
  
  
  var sortiraj_po = req.body.sort || null;
  
  
  
  var list_limit = req.body.limit;

  // if ( req.originalUrl == '/search_actions' ) list_limit = 20000;

  // populate objects if any of these queries 
  // if ( req.originalUrl == '/search_pp_owners' ) model_populate = model_find.populate('park_places_owner');
  // if ( req.originalUrl == '/search_park_places' ) model_populate = model_find.populate('pp_owner');


  var find_query = {};
  
  var trazi_string = req.body.trazi_string || null;
  
  
  var trazi_polja = [];
  var trazi_array = [];
  var filter_funkcije = req.body.filer || null;
  var filtrirano = null;
  
  var start = null;
  var end = null;
  
  var kreirao_query = false;
  
  // ako je query prazan objekt ( ali ipak postoji ) i ako je search string null ili prazan  ----> onda traži SVE  !!!!
  if ( req.body.query && Object.keys(req.body.query).length == 0 && !trazi_string ) {
    find_query = {};
    kreirao_query = true;
  
  }; 
  
  
  if ( req.originalUrl == '/search_regs' ) {
    req.body.query = {
      $or: [
        { "car_plates.reg": trazi_string.replace(/(?!ć|Ć|č|Č|š|Š|đ|Đ|ž|Ž)\W/g, '').replace(/\_/g, '').toUpperCase() },
        { email: { "$regex": trazi_string, "$options": "i" } }
      ]
    };
  };
  
  
  // ali ako postoji query onda je to prioritet tj traži sve po tom queriju
  if ( kreirao_query == false && req.body.query && Object.keys(req.body.query).length > 0 ) {
    
    find_query = req.body.query;
    
    if ( req.originalUrl == '/search_actions' ) {

      start = find_query.start;
      end = find_query.end;

      delete find_query.start;
      delete find_query.end;
      
    };
    
    kreirao_query = true;
    
  };
  
  // ako je query prazan, 
  // ali search string je neki tekst 
  // -----> izaberi sve string propertije i traži string u njima s regexom
  if ( 
    kreirao_query == false                  &&
    req.body.query                          &&
    Object.keys(req.body.query).length == 0 &&
    trazi_string !== null
  ) {
    
    // ONDA TRAŽI PO SVIM STRING PROPERTIJIMA !!!!!!
    var all_model_keys = Object.keys( DB_model.schema.paths );
    
    // izvuci iz modela samo string propertije 
    all_model_keys.forEach(function(key, key_index) {
      if ( DB_model.schema.paths[ key ].instance == 'String' ) trazi_polja.push(key);
    });
    
    trazi_polja.forEach(function(polje, index) {
      var search_objekt = {};
      search_objekt[polje] = { "$regex": trazi_string, "$options": "i" };
      trazi_array.push(search_objekt);
    });
    
    // if ( req.originalUrl == '/search_pp_owners' ) trazi_array.push({"roles.owner": true});

    find_query = { $or: trazi_array };
    
    kreirao_query = true;
    
  };
    
  // ako je postoji array polja find_in
  // i postoji search string 
  // onda traži regex samo u tim properijima
  if ( 
    kreirao_query == false      &&
    req.body.find_in            &&
    req.body.find_in.length > 0 &&
    trazi_string !== null 
  ) {

    trazi_array = [];
    
    req.body.find_in.forEach(function(polje, index) {
      
      var search_objekt = {};
      
      search_objekt[polje] = { "$regex": trazi_string, "$options": "i" };
      trazi_array.push(search_objekt);
      
    });
    
    // if ( req.originalUrl == '/search_pp_owners' ) trazi_array.push({"roles.owner": true});

    find_query = { $or: trazi_array };
    kreirao_query = true;
    
  };
  
  
  // ako nije ništa od gore navedenog onda jednostavno traži sve !!!!!!
  if ( kreirao_query == false ) {
    find_query = {};
  };
  

  // VAŽNO AKO NIJE ADMIN MORAM DODATI U QUERY SAMO PARKING PLACES KOJIMA JE OWNER
  // VAŽNO AKO NIJE ADMIN MORAM DODATI U QUERY SAMO PARKING PLACES KOJIMA JE OWNER
  // VAŽNO AKO NIJE ADMIN MORAM DODATI U QUERY SAMO PARKING PLACES KOJIMA JE OWNER
  
  if ( super_admin == false && req.originalUrl == '/search_actions' )  {
    
    var pp_owner_book_query = true;
    var pp_owner_park_query = true;
    var jel_trazi_svoje_parkinge = true;  
    
    // ako je ovaj user owner od bilo kojeg  parkirališta !!!
    if ( 
      arg_user.roles &&
      arg_user.roles.pp_owner &&
      $.isArray(arg_user.roles.pp_owner) &&
      arg_user.roles.pp_owner.length > 0
    ) {
      
        // ako query ima or u sebi onda je i park i book
        if ( find_query['$or'] ) {
          
          // ako postoji pp_sifra u prvom queriju
          if ( find_query['$or'][0].pp_sifra ) {
            // ali nije niti jedna od njegovih
            if ( $.inArray( find_query['$or'][0].pp_sifra, arg_user.roles.pp_owner ) == -1 ) pp_owner_book_query = false; 
          } else {
            // ako nema pp sifra u queriju
            pp_owner_book_query = null;
          };
          
          // ako postoji pp_sifra u drugom queriju
          if ( find_query['$or'][1].pp_sifra ) {
            // ali nije niti jedna od njegovih
            if ( $.inArray( find_query['$or'][1].pp_sifra, arg_user.roles.pp_owner ) == -1 ) pp_owner_park_query = false; 
          } else {
            // ako nema pp sifra u queriju
            pp_owner_park_query = null;
          };

          // ako bilo koji od querija ima pp_sifra koja nije u njegovom vlasništvu !!!
          if ( pp_owner_book_query == false || pp_owner_park_query == false  ) jel_trazi_svoje_parkinge = false;
          
          if ( pp_owner_book_query == null || pp_owner_park_query == null  ) jel_trazi_svoje_parkinge = null;
          

        } else {
          
          // ako NEMA find_query['$or'] u queriju ali postoji pp_sifra 
          if ( 
            find_query.pp_sifra &&
            $.inArray( find_query.pp_sifra, arg_user.roles.pp_owner ) == -1 
          ) {
            jel_trazi_svoje_parkinge = false;
          };
           
          // ako nema $or u queriju i ne postoji pp sifra !!!
          if ( !find_query.pp_sifra ) jel_trazi_svoje_parkinge = null;
          
        };


        if ( jel_trazi_svoje_parkinge == false ) {
          res.json({ success: false, msg: 'popup_nemate_ovlasti' });
          return;
        };

      // ako je ova varijabla null to znači ne traži niti jedan specifični parking
      // već je to polje ostavio prazno !!!
        if ( jel_trazi_svoje_parkinge == null ) {

          var svi_userovi_pp_array = [];

          // VAŽNO AKO NIJE ADMIN MORAM DODATI U QUERY SAMO PARKING PLACES KOJIMA JE OWNER
          // VAŽNO AKO NIJE ADMIN MORAM DODATI U QUERY SAMO PARKING PLACES KOJIMA JE OWNER
          // VAŽNO AKO NIJE ADMIN MORAM DODATI U QUERY SAMO PARKING PLACES KOJIMA JE OWNER
          $.each( arg_user.roles.pp_owner,  function(pp_index, pp_owner_sifra) {
            var pp_query = { pp_sifra: pp_owner_sifra };
            svi_userovi_pp_array.push(pp_query);
          });

          // dodaj queriju and i stavi u drugi item sve pp_sifre od ownera
          find_query = { 
            $and: [ 
              find_query,
              { $or: svi_userovi_pp_array }
            ]
          };

        }; // kraj ako u queriju nema pp sifri ali je user vlasnik jednog ili više parkinga !!!!

      // kraj ifa ako je owner tj  ima nešto u pp_powner arraju
      } else {
        
        // ako nije owner ničega
        res.json({ success: false, msg: 'popup_nemate_ovlasti' });
        return;
        
      };
    
    
  }; // kraj ako nije admin i ako je search actions !!!
  
  
  
  var model_find =  DB_model.find(find_query);
  // model populate je isti kao i model find
  var model_populate = model_find;
  // ali ako se radi o actions onda populate nije isti već dodajem _user u svaki doc
  if ( req.originalUrl == '/search_actions' ) model_populate = model_find.populate('_user');
  
  var items_skip = req.body.page ? ((req.body.page - 1) * list_limit) : 0;
  
  // TODO OBRISATI KASNIJE
  /*
  
  if ( req.originalUrl == '/search_actions' ) {
    list_limit = 100000;
    items_skip = 0;
  };
  
  */
  
  
  model_populate
    .limit(list_limit)
    .skip( items_skip )
    .sort( (sortiraj_po || { _id: -1 }) )
    .select( req.body.return )
    .exec(function(err, docs) {

    
    
    if (err) {
      res.send({ success: false, msg: 'Došlo je do greške kod pretrage akcija u bazi !!!', error: err });
      return;
    };
    
    var clean_docs = [];
    
    if ( docs.length > 0 ) {
      $.each(docs, function(doc_index, doc ) {
        var clean_doc = global.mng_to_js(doc);
        
        
        
        // ------------------- OVDJE MODIFICIRAM RESULT KAKO MI PAŠE OVISNO O REQ -------------------
        // --------START------ OVDJE MODIFICIRAM RESULT KAKO MI PAŠE OVISNO O REQ -------------------
        // ------------------- OVDJE MODIFICIRAM RESULT KAKO MI PAŠE OVISNO O REQ -------------------
        
        if ( req.originalUrl == '/search_actions' ) {
          
          if ( clean_doc._user ) {
            // ako postoji _user koji nastaje od populate action_schema.virtual('_user' ....
            // onda upiši email u varijablu user email
            clean_doc.user_email = clean_doc._user ? clean_doc._user.email : clean_doc.user_number;
            delete clean_doc._user;
          } else {
            // ako nema user email-a onda samo uzmi user number
            clean_doc.user_email = String( clean_doc.user_number );
          };
        
        };
        
        
        if ( req.originalUrl == '/search_regs' ) {

          
        };
        
        // ------------------- OVDJE MODIFICIRAM RESULT KAKO MI PAŠE OVISNO O REQ -------------------
        // --------END-------- OVDJE MODIFICIRAM RESULT KAKO MI PAŠE OVISNO O REQ -------------------
        // ------------------- OVDJE MODIFICIRAM RESULT KAKO MI PAŠE OVISNO O REQ -------------------
        
        
        clean_docs.push(clean_doc);
      });
      
      // ako se radi o action queriju za statistiku onda odreži sve book i park akcije da stanu u interval od - do tj  start - end
      if ( req.originalUrl == '/search_actions' ) clean_docs = crop_actions_to_interval(clean_docs, start, end);
      
    };
    
    
    
    filtrirano = run_filter( clean_docs, filter_funkcije);

    
    
    // ako je pretraga plark places injektiraj current price
    if ( req.originalUrl == '/search_park_places' ) {
      
      filtrirano.forEach(function(pp_obj, pp_index) {
        filtrirano[pp_index].pp_current_price = park_actions_controller.find_current_price(pp_obj);
      });
    };
    
    
    if ( 
      super_admin == false 
      && 
      ( req.originalUrl == '/search_customers' || req.originalUrl == '/stats_search_customers' )
      ) {
    
      // ako je ovo owner on nekog parkirališta !!!!
      if ( 
        arg_user.roles &&
        arg_user.roles.pp_owner &&
        $.isArray(arg_user.roles.pp_owner) &&
        arg_user.roles.pp_owner.length > 0
      ) {

        var samo_useri_s_njegovih_pp = [];
        
        
        filtrirano.forEach(function(user_obj, user_index) {
          
          var user_posjetio_ownerov_pp = false;
          // provjeri jel user posjedio jedan od ownerovih parkova !!!!
          $.each( user_obj.visited_pp, function(vis_index, vis_pp) {
            if ( $.inArray(vis_pp, arg_user.roles.pp_owner) > -1 ) {
              user_posjetio_ownerov_pp = true;
            };
          });
          
          // ako je onda ubaci ovog usera u novi array samo sa posjetiteljima ownerovih parkova
          if ( user_posjetio_ownerov_pp == true ) samo_useri_s_njegovih_pp.push(user_obj);
          
        });
        
        filtrirano = samo_useri_s_njegovih_pp;
      } 
      else {
        
        // ako nije owner od niti jednog parkirališta i nije super admin
        
        var samo_current_user = null;
        filtrirano.forEach(function(user_obj, user_index) {
          if ( user_obj._id == arg_user._id ) {
            samo_current_user = user_obj;
          };
        });

        if ( samo_current_user !== null ) {
          // ako je našao current usera onda ga ubaci u array
          filtrirano = [samo_current_user];
        } else {
          // ako nije našao usera onda vrati prazan array
          filtrirano = [];
        };
        
      }; // kraj else ako nije owner parkirališta
    
  };

  

    if ( req.originalUrl == '/search_actions' ) {

      
      
      var clean_agg_actions = [];
      var uniq_users = [];
      
      var count = 0;
      var distinct = 0;
      var time_book = 0;
      var time_park = 0;
      var novac_book = 0;
      var novac_park = 0;
      

      
      // keširam aggregaciu akcija !!!!!!!!!!!
      // ako je string od querija isti kao i string od prošlog querija
      // onda vrati keširani resultat !!!!!
      if ( JSON.stringify(find_query) == global.cache_find_query ) {
        
        // 
        var cache_object = JSON.parse(global.cache_agg_result);
        // moram ubaciti nove rows jer je možda user kliknuo na novu stranicu
        // ovak ostaju svi podaci od sume i agregacije ali se mijenja array u rows ( to su sve akcije na jednom page-u)
        // na taj način izbjegavam pravljenje sume i agregacije svih podataka
        // samo predajem rezultate od trenutnog page-a
        
        cache_object.rows = filtrirano;
        
        res.json( cache_object );
        return;
        
      };
      
      global.cache_find_query = JSON.stringify(find_query);
          
      // VAŽNO !!! Ponovo zovem bazu da mi vrati sve actionse po queriju
      DB_model.find( find_query )
      .populate('_user') /* pogledati u park_actions_model.js za objašnjenje kako napraviti join action i user */
      .exec(function(err, agg_actions) {
      
        if ( err ) {
          res.send({ 
            success: false,
            msg: 'Došlo je do greške kod prikupljanja sume podataka podataka za actions pretragu !!!', 
            error: err
          });
          return;
        };

        if ( agg_actions.length > 0 ) {
          
          $.each(agg_actions, function( agg_index, agg_act) {
            var cl_action = global.mng_to_js(agg_act);
            
            // 
            cl_action.user_email = cl_action._user ? cl_action._user.email : cl_action.user_number;
            
            if ( $.inArray( cl_action.user_email, uniq_users ) == -1 ) {
              uniq_users.push(cl_action.user_email);
            };
            
            if ( cl_action._user ) delete cl_action._user;
            
            clean_agg_actions.push(cl_action);
          });
          
          
          clean_agg_actions = crop_actions_to_interval(clean_agg_actions, start, end);
          
          count = clean_agg_actions.length;
          distinct = uniq_users.length;
          
          var time_now = Date.now();
          
          
          // SUMA SVIH PAID BOOKING I PARKINGA 
          $.each(clean_agg_actions, function( cl_ind, cl_act) {
            
            if ( cl_act.book_duration_c ) {
              time_book += cl_act.book_duration_c;
              var book_paid = ( cl_act.book_duration_c/( 1000*60*60 ) ) * cl_act.current_price;
              novac_book += book_paid;
            };
            
            if ( cl_act.duration_c ) {
              time_park += cl_act.duration_c;
              var park_paid = ( cl_act.duration_c/( 1000*60*60 ) ) * cl_act.current_price;
              novac_park += park_paid;
            };
            
          });
          
        }; // kraj jel postoje actions

        
        // ---------- VAŽNO ----------
        // pretvaram već postojeći array filtrirano u objekt
        // a postojeći array stavljam da bude unutar tog novog objekta
        // to je samo zato što vraćam varijablu
        
        var agg_result = {
          success: true,
          rows: filtrirano,

          count: count,
          distinct: distinct,
          time_book: time_book,
          time_park: time_park,
          novac_book: novac_book,
          novac_park: novac_park,
          timestamp: Date.now(),
        };
        
        global.cache_agg_result = JSON.stringify(agg_result);
        
        global.search_actions_result_array_cache.push({actions: clean_agg_actions, timestamp: agg_result.timestamp } );
        
        // ako array od cache-a ima više od 10 itema onda izbaci prvi tj. najstariji
        if ( global.search_actions_result_array_cache.length > 10 ) {
          global.search_actions_result_array_cache.shift();
        };
        
        res.json(agg_result);

        
      }); // kraj finda svih action za kreiranje agregata tj sume itd
      
      // kraj ako je pretraga po actions
    } else { 
      
      
      res.json(filtrirano);
      
      
    };

  });

  
  
  // zaustavi danji rad ako je request za sve zapise
  // if (trazi_polja == 'all') return;
  /*
  DOBIJEM NEŠTO SLIČNO OVOME:
  -----------------------------------------------------------
  [
    { "en.name": { "$regex": trazi_string, "$options": "i" }},
    { "de.name": { "$regex": trazi_string, "$options": "i" }},
    { "hr.name": { "$regex": trazi_string, "$options": "i" }},
    { "sr.name": { "$regex": trazi_string, "$options": "i" }},
  ] 
  -----------------------------------------------------------
  */
  
  /*
  UBACITI OVO PRIJE .exec AKO ŽELIM SELEKTIRATI SAMO NEKA POLJA:
  -----------------------------------------------------------
  .select(select_objekt)

  dobijem nešto slično ovome
  .select({
    _id: 1,
    status: 1,
    gametype_id: 1,
    coverImg: 1,
    "config.status.lastChangedAt": 1,
    "hr.name": 1, 
    "en.name": 1, 
    "de.name": 1, 
    "sr.name": 1
  })
  -----------------------------------------------------------
  */  
  
};
