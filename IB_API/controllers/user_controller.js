
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);


const fs = require('fs');
const path = require('path');

var jwt = require('jsonwebtoken');
var scramblePass = require('password-hash-and-salt');
var nodemailer = require('nodemailer');

global.all_users = null;


global.vel_email_address = 'app@velprom.hr';

global.vel_mail_setup = {
  host: "mail.velprom.hr",
  port: 587,
  secure: false, // use TLS,
  requireTLS: true,
  auth: {
    user: global.vel_email_address,
    pass: 'eukaliptus69'
  },
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false
  }
};




exports.test_toni = function(req, res) {
  
  
var transporter = nodemailer.createTransport(global.vel_mail_setup);
        
        
        /*
        
        var transporter = nodemailer.createTransport({
          service: 'Gmail',
          auth: {
            user: 'vrata.dizajn.crm@gmail.com', // Your email id
            pass: 'vrataCRM123!321' // Your password
          }          
        });
        
        */

      
      
        
       
        
        // setup email data
        var mailOptions = {
            from: `"Velprom App" <${global.vel_email_address}>`, // sender address
            to: `tkutlic@gmail.com`, // list of receivers
            subject: "test toni", // Subject line
            html:  "<h1>OVO JE MAIL HTML H1</h1>" // html body
        };

          // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
          
          if (error) {
            res.json({ success: false, msg: 'MAIL NE RADI', err: error });
          } else {
            res.json({ success: true, msg: 'MAIL RADI' });
          };
          
        });
  
  
  
};


fs.readFile(path.join(__dirname, 'useri.json'), (err, data) => {

  if (err) {
    console.error({ success: false, msg: 'Došlo je do greške kod otvranja users file-a' });
    return;

  } else {

    global.all_users = JSON.parse(data);
    console.log(`USPJEŠNO OTVORIO USERS FILE !!!`);

  }; // kraj read file postoji  tj nema errora !!!!

}); // kraj fs read



fs.readFile(path.join(__dirname, 'user_tests.json'), (err, data) => {

  if (err) {
    console.error({ success: false, msg: 'Došlo je do greške kod otvranja TESTS file-a' });
    return;

  } else {

    global.all_user_tests = JSON.parse(data);
    console.log(`USPJEŠNO OTVORIO TESTS FILE !!!`);

  }; // kraj read file postoji  tj nema errora !!!!

}); // kraj fs read





fs.readFile(path.join(__dirname, 'all_pits.json'), (err, data) => {

  if (err) {
    console.error({ success: false, msg: 'Došlo je do greške kod otvranja SVIH PITANJA ZA TESTOVE' });
    return;

  } else {

    global.all_pits = JSON.parse(data);
    console.log(`USPJEŠNO OTVORIO FILE SA SVIM PITANJIMA !!!`);

  }; // kraj read file postoji  tj nema errora !!!!

}); // kraj fs read





fs.readFile(path.join(__dirname, 'tests_temps.json'), (err, data) => {

  if (err) {
    console.error({ success: false, msg: 'Došlo je do greške kod otvranja SVIH DEFINICIJA TESTOVA' });
    return;

  } else {

    global.all_test_temps = JSON.parse(data);
    console.log(`USPJEŠNO OTVORIO FILE SA SVIM TEST TEMPLATES !!!`);

  }; // kraj read file postoji  tj nema errora !!!!

}); // kraj fs read









exports.authenticate_a_user = function(req, res) {
  
  var cit_pass_timeout = 720;
  
  var pass = req.body.password;

        
  let found_user = global.all_users.find(user => {
    return (req.body.name == user.name);
  });

  
  

  if ( !found_user ) {

    res.json({ success: false, msg: 'Potvrda korisnika nije uspjela.<br>Nema tog korisnika u bazi :(' });

  } else {

    // if user is found verify a hash 
    scramblePass(pass).verifyAgainst(found_user.hash, function(error, verified) {
      if (error) {

        res.json({ success: false, msg: 'Došlo je do greške kod pretrage korisnika.<br>Nazovite 092 3133 015' });

        throw new Error('Something went wrong with hash checking!');
        return;
      }

      if(!verified) {
        // if pass is wrong !!!!!!
        res.json({ success: false, msg: 'Potvrda korisnika nije uspjela.<br>Kriva lozinka!' });
      } else {



        var sign_obj = {
          _id: found_user._id,
          hash: found_user.hash
        };


        var token = jwt.sign(sign_obj, 'super_tajna', {
          expiresIn : 60 * cit_pass_timeout // expires in 720 min
        });


        found_user.user_id = found_user._id;
        found_user.success = true;
        found_user.msg = 'Enjoy your token!';
        found_user.token = token;
        found_user.token_expiration = Date.now() + (1000 * 60 * cit_pass_timeout) - 10000;

        res.json(found_user); 

      }; // end of ELSE !verified

    }); // end of scramblePass


  }; // jel nasao usera



  
}; // end of authenticate_a_user



function record_help_time(user_index) {
  

  var time_now = Date.now();
  
  global.all_users[user_index].last_login = time_now;


  // ako uopće ne postoji help times
  if ( !global.all_users[user_index].help_times ) {

    global.all_users[user_index].help_times = [];
    global.all_users[user_index].help_times.push({in: time_now, out: time_now });

  } else {

    // ako postoji help times
    var last_help_time = 0;
    var last_help_index = null;

    $.each(global.all_users[user_index].help_times, function(help_ind, help) {
      // ako je last time manji ili jednak out vremenu u ovom objektu
      if ( last_help_time <= help.out ) {
        // onda je to novi last time
        last_help_time = help.out;
        last_help_index = help_ind;
      }
    });

    // ako je zadnje vrijeme manje od 5 min od NOW
    if (time_now - last_help_time < 5*60*1000 ) {
      // onda upiši trenutno vrijeme u taj hel pobjekt
      global.all_users[user_index].help_times[last_help_index].out = time_now;

    } else {
      // ako je prošlo više od 5 min od zanjeg help out onda napravi novi help objekt
      global.all_users[user_index].help_times.push({in: time_now, out: time_now });
    };

    // kraj else postoji help_times
  };
  
  
};


exports.im_online = function(req, res) {

  var old_token = req.body.token;
  // verifies secret and checks exp
  jwt.verify(old_token, 'super_tajna', function(err, decoded_user) { 
    
      if (err) {
        
        res.json({ success: false, msg: 'Token korisnika nije potvrđen!' });    
        
        return;
        
      } else {
        
        // if everything is OK - make a new token
        // decoded is extrapolated user object from token itself !!!!
        // token always has the scrambled user data inside !!!!!
  
      var time_now = Date.now();  
        

      let found_user = null;
      let user_index = null;
     

      $.each(global.all_users, function(index, user) {
        if (user._id == decoded_user._id) {
          user_index = index;
          found_user = user;
        };
      });

      if ( user_index == null ) {

        res.json({ success: false, msg: 'Potvrda korisnika nije uspjela.<br>Nema tog korisnika u bazi :(' });

      } else {

        record_help_time(user_index);
        res.json({ success: true, msg: 'IM ONLINE SUCCESS!' });

      };  // kraj ako je našao usera   



    }; // kraj da je token dobar !!
    
    
  }); // kraj provjere tokena !!!
  
  
};


function write_users_file() {

  let data = JSON.stringify(global.all_users, null, 2);

  fs.writeFile(path.join(__dirname, 'useri.json'), data, (err) => {

    if (err) {
      console.error({ success: false, msg: 'Greška prilikom upisivanja all users u file!' });
      throw err;
    } else {
      console.log(`WRITE USERS FILE OK`);
    };

  }); // kraj write file  

};

global.write_users_file = write_users_file;


setInterval(function() {
  
  write_users_file();
  
}, 30*1000);


exports.prolong_token = function (req, res) {

  var cit_pass_timeout = 720; // in minutes

  var old_token = req.body.token;
  // verifies secret and checks exp
  jwt.verify(old_token, 'super_tajna', function (err, decoded_user) {

    if (err) {

      res.json({
        success: false,
        msg: 'Token korisnika nije potvrđen!'
      });

      return;

    } else {

      // if everything is OK - make a new token
      // decoded is extrapolated user object from token itself !!!!
      // token always has the scrambled user data inside !!!!!


          let found_user = null;
          let user_index = null;

          $.each(global.all_users, function (index, user) {
            if (user._id == decoded_user._id) {
              user_index = index;
              found_user = user;
            };
          });

          if (user_index == null) {

            res.json({
              success: false,
              msg: 'Potvrda korisnika nije uspjela.<br>Nema tog korisnika u bazi :('
            });

          } else {

            var sign_obj = {
              _id: decoded_user._id,
              hash: decoded_user.hash
            };

            var new_token = jwt.sign(sign_obj, 'super_tajna', {
              expiresIn: 60 * cit_pass_timeout // expires in 30 min
            });
            res.json({
              token: new_token,
              token_expiration: Date.now() + (1000 * 60 * cit_pass_timeout) - 10000, // minus 10 sec just to be sure
            });

          }; // kraj ako je našao usera   


    }; // kraj da je token dobar !!


  }); // kraj provjere tokana !!!


};


exports.logout_user = function(req, res) {
  var cit_pass_timeout = 720; // in minutes

  var old_token = req.body.token;
  // verifies secret and checks exp
  jwt.verify(old_token, 'super_tajna', function (err, decoded_user) {

    if (err) {

      res.json({
        success: false,
        msg: 'Token korisnika nije potvrđen!'
      });

      return;

    } else {

          let found_user = null;
          let user_index = null;

          $.each(global.all_users, function (index, user) {
            if (user._id == decoded_user._id) {
              user_index = index;
              found_user = user;
            };
          });

          if (user_index == null) {

            res.json({
              success: false,
              msg: 'Potvrda korisnika nije uspjela.<br>Nema tog korisnika u bazi :('
            });

          } else {
            
            record_help_time(user_index);

            res.json({
              success: true,
              msg: `LOGOUT SUCCSESS!`
            });

          }; // kraj ako je našao usera   


    }; // kraj da je token dobar !!


  }); // kraj provjere tokana !!!


};


global.cit_deep = function(data) {
  data = JSON.stringify(data);
  data = JSON.parse(data);
  return data;
};



exports.get_IB_users = function(req, res) {
  
  
  if ( !global.all_users ) {
    res.json({ success: false, msg: `Nisam našao IB usere na serveru !!!`, users: null });
    return;
  };
 
  
  
  if ( req.body.user_ids ) {
  
    var filtered_users = [];
    
    $.each( global.all_users, function(u_ind, user) {
      if ( req.body.user_ids.indexOf( user._id ) > -1 ) filtered_users.push(user);
    });
    

    $.each( filtered_users, function(user_ind, filtered_user) {
      var user_tests_result = run_get_user_tests(filtered_user._id);
      filtered_users[user_ind].tests = user_tests_result || [];
    });

    res.json({ success: true, users: filtered_users });
  
  } else {


    $.each( global.all_users, function(user_ind, g_user) {
      var user_tests_result = run_get_user_tests(g_user._id);
      global.all_users[user_ind].tests = user_tests_result || [];
    });


    res.json({ success: true, users: global.all_users });
  };
  
  
};

exports.make_new_help_user = function(req, res) {
  
  
  var new_user = req.body;

  var name_exists = false;
  var email_exists = false;
  
  $.each(global.all_users, function(u_ind, user) {
    if (user.name == new_user.name ) name_exists = true;
    if (user.email == new_user.email ) email_exists = true;
  });

  
  if ( name_exists || email_exists ) {
    
    res.json({ success: false, msg: 'MAIL ILI NADIMAK VEĆ POSTOJE' });
    return;
  };
  
  
  global.all_users.push(new_user);

  res.json({ success: true });
  
  
};

exports.update_help_user = function(req, res) {
  
  
  var update_user = req.body;
  
  var found_user_index = null;
  
  
  $.each(global.all_users, function(u_ind, user) {
    if (user.user_number == update_user.user_number ) found_user_index = u_ind;
  });

  
  if ( found_user_index == null ) {
    res.json({ success: false, msg: 'NISAM NAŠAO TOG USERA NA IB SERVERU !' });
    return;
  };
  
  
  // kopiraj help times ako postoje -----> ako ne postoje onde nek bude prazan array
  var help_times = global.all_users[found_user_index].help_times || [];
  
  // uzmi postojeći help times i dodaj ga updatanom useru
  update_user.help_times = global.cit_deep(help_times);
  
  // zatim ubaci updateanog usera na mjesto starog usera
  global.all_users[found_user_index] = update_user;

  res.json({ success: true, user: update_user });
  
  
};

exports.delete_help_user = function(req, res) {
  
  
  var delete_user = req.body;
  
  var found_user_index = null;
  
  
  $.each(global.all_users, function(u_ind, user) {
    if (user.user_number == delete_user.user_number ) found_user_index = u_ind;
  });

  
  if ( found_user_index == null ) {
    res.json({ success: false, msg: 'NISAM NAŠAO TOG USERA NA IB SERVERU !' });
    return;
  };
  
  global.all_users.splice(found_user_index, 1);

  res.json({ success: true });
  
  
};


// --------------------------- USER TEST TEMPLATES (sve klase ili templates testova !!!! ) ---------------------------


exports.get_all_test_temps = function(req, res) {
  
  
  if ( !global.all_test_temps ) {
    res.json({ success: false, msg: `Nisam našao niti jedan TEST template !!!`, temps: null });
    return;
  };
 
  res.json({ success: true, tests: global.all_test_temps });
  
  
};

exports.new_test_temp = async function(req, res) {
  
  var new_temp = req.body;

  if ( !global.all_test_temps ) {
    res.json({ success: false, msg: `Nisam našao niti jedan TEST template !!!`, temps: null });
    return;
  };

  global.all_test_temps.push(new_temp);


  var write_temps = await write_temps_file();

  if ( write_temps.success == true ) {
    res.json({ success: true });
  } else {
    res.json({ success: false, err: write_temps.err });
  };

  
};

exports.update_test_temp = async function(req, res) {
  

  if ( !global.all_test_temps ) {
    res.json({ success: false, msg: `Nisam našao niti jedan TEST template !!!`, temps: null });
    return;
  };

  var update_temp = req.body;
  
  $.each( global.all_test_temps, function(u_ind, temp) {
    
    if ( temp.template_sifra == update_temp.template_sifra ) {

      global.all_test_temps[u_ind] = update_temp;

      // napravi update na svim instancama ovog template
      run_update_tests(update_temp);

      
    };
    
  });


  var write_temps = await write_temps_file();

  if ( write_temps.success == true ) {
    res.json({ success: true, temps: global.all_test_temps });
  } else {
    res.json({ success: false, err: write_temps.err });
  };

  
  
};

exports.delete_test_temp = async function(req, res) {
  
  
  if ( !global.all_test_temps ) {
    res.json({ success: false, msg: `Nisam našao niti jedan TEST template !!!`, temps: null });
    return;
  };


  var delete_this_tempate_sifra = req.body.template_sifra;
  var found_temp = null;
  var t;
  for ( t=global.all_test_temps.length - 1; t >= 0 ; t-- ) {

    if ( global.all_test_temps[t].template_sifra == delete_this_tempate_sifra) {
      global.all_test_temps.splice(t, 1);
      found_temp = true;
    };

  };
  
  if ( found_temp == null ) {
    res.json({ success: false, msg: 'NISAM NAŠAO TAJ TEMPLATE NA IB SERVERU !' });
    return;
  };



  //  ----------------------------------------------------------------------------------------------------------------
  // Nisam siguran jel trebam brisati sve instance od testova za svakog usera !!!!
  //  ----------------------------------------------------------------------------------------------------------------

  var i;
  var found_test = null;
  for ( i=global.all_user_tests.length - 1; i >= 0 ; i-- ) {
  
    var test = global.all_user_tests[i];
  
    if (
      test.template_sifra == delete_this_tempate_sifra
      &&
      !test.started // ne smije biti započeti test !!!!

       ) {
      global.all_user_tests.splice(i, 1);
      found_test = true;
    };
    
  };
  
  if ( found_test == null ) {
    res.json({ success: false, msg: 'NISAM NAŠAO NITI JEDAN TAKAV TEST NA IB SERVERU !' });
    return;
  };



  var write_temps = await write_temps_file();

  if ( write_temps.success == true ) {
    res.json({ success: true, temps: global.all_test_temps });
  } else {
    res.json({ success: false, err: write_temps.err });
  };



  res.json({ success: true });
  
  
};





function write_temps_file() {

  return new Promise( function(resolve, reject) {

    let data = JSON.stringify(global.all_test_temps, null, 2);

    fs.writeFile(path.join(__dirname, 'tests_temps.json'), data, (err) => {

      if (err) {
        console.error({ success: false, msg: 'Greška prilikom upisivanja ALL TEST TEMPS  u file!' });
        resolve({success: false, err: err });
      } else {
        resolve({success: true });
        console.log(`$$$$$$$$$$$$$$$$$$$$$$$$$$ RITE ALL TEST TEMPS FILE OK $$$$$$$$$$$$$$$$$$$$$$$$$$ `);
      };

    }); // kraj write file 
    
  }); // kraj promisa

};







// --------------------------- USER TEST FILES (sve instance od test templates raspoređene po userima !!!! ) ---------------------------


function eval_test( arg_test ) {

  arg_test.count_pit = arg_test.pits.length;
  arg_test.count_correct = 0;
          
  //  --------------------------------------------- broji samo ako je započet test 
  if ( arg_test.started ) {
    
    var correct_array = [];
    
    function is_correct( arg_pit ) {
      
      // arg pit je trenutno pitanje koje sam izvukao is user_test objekta ( tj instance pitanja kod usera )
      
      // prvo moram vidjeti koji je array točnih odgovora !!!
      // nađi pitanja koje imaju isti pit property !!!
      
      var curr_pits = find_all_items( data.all_pits, arg_pit.pit, `pit` ); 
      
      $.each( curr_pits, function(cp_index,  cp ) {
        if ( cp.oznaka.toUpperCase() == `T` ) {
          correct_array.push(cp.odg);
        };
      });
      
      var is_correct = false;
      var count = 0;
      
      if ( arg_pit.odg && arg_pit.odg.length > 0 ) {
        // ------------------------------------------------------------------------------------------

        // loop po svim odgovorima u trenutnom useru i trenutno testu ---> to je obični array sa brojevima npr [ 1, 3 ]
        // to uspoređujem sa arrajom koji je isti oblik  ----> npr [ 2, 3 ]
        
        $.each( arg_pit.odg, function( odg_ind, answer ) {
          // ako je ovja broj odgovora unutar correct arraja
          if ( correct_array.indexOf(answer) > -1 ) {
            count += 1;
          };

        });

        // jedino kada su arrays isti onda će count biti isti kao length od correct arraja
        if ( count == correct_array.length ) {
          is_correct = true;
        };

        // ------------------------------------------------------------------------------------------
      }; // samo ako postoji neki odgovor !!!
      
      
      return is_correct;
      
    };

    
    $.each( arg_test.pits, function(p_ind, pit) {
      if ( is_correct(pit) ) arg_test.count_correct += 1;
    });
    
    
  }; 
  // ----------------------END----------------------- broji samo ako je započet test 

  return arg_test;

};



function run_get_user_tests( user_id ) {

   
  if ( user_id ) {
  
    // ako imam user id onda filtriram samo njegove testove !!!!!!!!!!!!!!!!!!
    var filtered_tests = [];


    $.each( global.all_user_tests, function(u_ind, test) {
      if ( test.user && user_id == test.user._id ) filtered_tests.push(test);
    });
    
    $.each(filtered_tests, function(t_ind, test) {
      filtered_tests[t_ind] = eval_test(test);
    });

    return { success: true, tests: filtered_tests, pits: global.all_pits || null }
  
  } else {
    
    // ako nemam user id onda uzimam sve testove !!!!!!!!!!!!
    $.each(global.all_user_tests, function(t_ind, test) {
      global.all_user_tests[t_ind] = eval_test(test);
    });

    return { success: true, tests: global.all_user_tests, pits: global.all_pits || null };
    
  };
   


};


exports.get_all_user_tests = function(req, res) {
  
  
  var user_id = req.body.user_id || null;
  
  if ( !global.all_user_tests ) {
    res.json({ success: false, msg: `Nisam našao IB testove od usera na serveru !!!`, tests: null });
    return;
  };
 
  var user_tests_result = run_get_user_tests(user_id);


  res.json( user_tests_result );

  
};

exports.new_user_tests = function(req, res) {
  
  var new_tests_array = req.body;

    
  if ( !global.all_user_tests ) {
    res.json({ success: false, msg: `Nisam našao IB testove od usera na serveru !!!`, tests: null });
    return;
  };

  
  global.all_user_tests = [
    
    ...global.all_user_tests,
    ...new_tests_array
    
  ];

  res.json({ success: true });
  
  
};




function run_update_tests(update_test) {

  $.each( global.all_user_tests, function(u_ind, test) {
    
    if ( 

      test.template_sifra == update_test.template_sifra
      &&
      !test.started // ne smije biti započeti test !!!!
      
      ) {

      // napravi update svega ------> pazi da u update test objektu nemam sifru, user, 
      global.all_user_tests[u_ind] = { ...global.all_user_tests[u_ind],  ...update_test };
      
    };
    
  });

  // ipak neću eksplicitno spremati user tests file zato što čekam da se spremi svakih 5 minuta
  // global.write_tests_file();


};

exports.update_user_tests = function(req, res) {
  
  
  if ( !global.all_user_tests ) {
    res.json({ success: false, msg: `Nisam našao IB testove od usera na serveru !!!`, tests: null });
    return;
  };

  var update_test = req.body;
  
  run_update_tests(update_test);


  res.json({ success: true, tests: global.all_user_tests });
  
  
};

exports.delete_user_tests = function(req, res) {
  
  
  if ( !global.all_user_tests ) {
    res.json({ success: false, msg: `Nisam našao IB testove od usera na serveru !!!`, tests: null });
    return;
  };


  var delete_this_tempate_sifra = req.body.template_sifra;
  
  
  var i;
  var found_test = null;
  for ( i=global.all_user_tests.length - 1; i >= 0 ; i-- ) {
  
    var test = global.all_user_tests[i];
  
    if (test.template_sifra == delete_this_tempate_sifra ) {
      global.all_user_tests.splice(i, 1);
      found_test = true;
    };
    
  };
  
  if ( found_test == null ) {
    res.json({ success: false, msg: 'NISAM NAŠAO NITI JEDAN TAKAV TEST NA IB SERVERU !' });
    return;
  };

  res.json({ success: true });
  
  
};


function write_tests_file() {

  let data = JSON.stringify(global.all_user_tests, null, 2);

  fs.writeFile(path.join(__dirname, 'user_tests.json'), data, (err) => {

    if (err) {
      console.error({ success: false, msg: 'Greška prilikom upisivanja all user TESTS  u file!' });
      throw err;
    } else {
      console.log(`------------------------ WRITE TESTS FILE OK`);
    };

  }); // kraj write file  

};
global.write_tests_file = write_tests_file;


// ovo vrtim u intervalu zato što u svakom trenutku netko može pisati neki test i stalno moram raditi update !!!!
setInterval(function() {
  
  write_tests_file();
  
}, 60*1000 * 5 ); // svakih 5 min




// --------------------------- SVA PITANJA ( tj. sva moguća pittanja koja su na raspolaganju ) ---------------------------


exports.get_all_pits = async function(req, res) {
  
  if ( !global.all_pits ) {
    res.json({ success: false, msg: `Nisam našao PITANJA ZA TESTOVE na IB serveru !!!`, pits: null });
    return;
  };

   res.json({ success: true, pits: global.all_pits });
  
};

exports.new_pits = async function(req, res) {
  
  var new_pits = req.body; /// ovo je isto array a ne objekt
    
  if ( !global.all_pits ) {
    res.json({ success: false, msg: `Nisam našao PITANJA ZA TESTOVE na IB serveru !!!`, pits: null });
    return;
  };

  global.all_pits = [ ...global.all_pits , ...new_pits ];

  var write_pits = await write_pits_file();

  if ( write_pits.success == true ) {
    res.json({ success: true });
  } else {
    res.json({ success: false, err: write_pits.err });
  };

  
};

exports.update_pits = async function(req, res) {
  
  
  if ( !global.all_pits ) {
    res.json({ success: false, msg: `Nisam našao PITANJA ZA TESTOVE na IB serveru !!!`, pits: null });
    return;
  };

  var update_pits_array = req.body; // pazi ovo je array a NE OBJEKT !!!!!!!!!!!!!!!!!!!!!
  
  var i;
  var found_pit = null;

  // -------- prvo obriši sve objekte 
  // -------- prvo obriši sve objekte 
  // -------- prvo obriši sve objekte 
  for ( i=global.all_pits.length - 1; i >= 0 ; i-- ) {
    // ako bilo koje pitanje u all pits ima isti pit kao prvi item u arrayu koje sam poslao
    if (global.all_pits[i].pit == update_pits_array[0].pit ) {
      global.all_pits.splice(i, 1);
      found_pit = true;
    };
  };
  
  if ( found_pit == null ) {
    res.json({ success: false, msg: 'NISAM NAŠAO NITI JEDNO PITANJE S TIM ID-jem NA IB SERVERU !' });
    return;
  };

  // zatim ubaci nove objekte !!!!
  global.all_pits = [ ...global.all_pits, ...update_pits_array ];


  var write_pits = await write_pits_file();

  if ( write_pits.success == true ) {
    res.json({ success: true });
  } else {
    res.json({ success: false, err: write_pits.err });
  };
  
  
};

exports.delete_pits = async function(req, res) {
  
  
  if ( !global.all_pits ) {
    res.json({ success: false, msg: `Nisam našao PITANJA ZA TESTOVE na IB serveru !!!`, tests: null });
    return;
  };

  var delete_this_pit = req.body.pit;
  
  var i;
  var found_pit = null;

  for ( i=global.all_pits.length - 1; i >= 0 ; i-- ) {
    
    if (global.all_pits[i].pit == delete_this_pit ) {
      global.all_pits.splice(i, 1);
      found_pit = true;
    };
  };
  
  if ( found_pit == null ) {
    res.json({ success: false, msg: 'NISAM NAŠAO NITI JEDNO PITANJE S TIM ID-jem NA IB SERVERU !' });
    return;
  };


  var write_pits = await write_pits_file();

  if ( write_pits.success == true ) {
    res.json({ success: true });
  } else {
    res.json({ success: false, err: write_pits.err });
  };

  
};


function write_pits_file() {

  return new Promise( function(resolve, reject) {

    let data = JSON.stringify(global.all_pits, null, 2);

    fs.writeFile(path.join(__dirname, 'all_pits.json'), data, (err) => {

      if (err) {
        console.error({ success: false, msg: 'Greška prilikom upisivanja ALL PITS  u file!' });
        resolve({success: false, err: err });
      } else {
        resolve({success: true });
        console.log(`WRITE ALL PITS FILE OK ///////////////////////////////`);
      };

    }); // kraj write file 
    
  }); // kraj promisa

};


