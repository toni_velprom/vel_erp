
exports.generate = function (link, user_name) {
  
var glavni_naslov = 'Dobro došli u Velprom App!';


  var html = `
<div style="width: 100%; height: auto; text-align: center; font-family: Arial, Helvetica, sans-serif">
<div style="font-size: 25px; margin-top: 20px; margin-bottom: 20px; color: #06253f; font-weight: 400;">Bok ${user_name}!</div>
<div style="font-size: 25px; margin-top: 20px; margin-bottom: 20px; color: #06253f; font-weight: 400;">${glavni_naslov}</div>
  
  <img style="width: 100%;
              height: auto;
              max-width: 500px;
              border-radius: 10px"
              alt="Velprom App - welcome image" 
              src="${global.cit_domain}/img/email_image.jpg">
  
  <div style="font-size: 25px; margin-top: 20px; color: #06253f; font-weight: 400;">Još malo i gotovi ste :)</div>  
  <div style="font-size: 16px; margin-top: 20px; color: #06253f;">Molimo vas potvrdite registraciju klikom na tipku:</div>   

  <a style="  background-color: #06253f;
              border-radius: 8px;
              height: 60px;
              width: 200px;
              font-size: 14px;
              margin: 20px auto 0;
              text-align: center;
              display: block;
              min-width: 200px;
              color: #fff;
              text-decoration: none;
              line-height: 60px;"

     target="_blank"
     href="${link}">
    POTVRDITE
  </a>   
  
  </div>
`

  return {
    html: html
  }

};

exports.exp_booking_mail = function ( info_data ) {
  

var glavni_naslov = 'Dobro došli u Velprom App!';


  var html = `
<div style="width: 100%; height: auto; text-align: center; font-family: Arial, Helvetica, sans-serif">
<div style="font-size: 20px; margin-top: 20px; margin-bottom: 20px; color: #06253f; font-weight: 400;">Bok ${user_name}!</div>
<div style="font-size: 20px; margin-top: 20px; margin-bottom: 20px; color: #06253f; font-weight: 400;">${glavni_naslov}</div>
  
  <img style="width: 100%;
              height: auto;
              max-width: 500px;
              border-radius: 10px"
              alt="Velprom App - welcome image" 
              src="${global.cit_domain}/img/email_image.jpg">
  
  <div style="font-size: 25px; margin-top: 20px; color: #06253f; font-weight: 400;">Još malo i gotovi ste :)</div>  
  <div style="font-size: 16px; margin-top: 20px; color: #06253f;">Molimo vas potvrdite registraciju klikom na tipku:</div>   

  <a style="  background-color: #06253f;
              border-radius: 8px;
              height: 60px;
              width: 200px;
              font-size: 14px;
              margin: 20px auto 0;
              text-align: center;
              display: block;
              min-width: 200px;
              color: #fff;
              text-decoration: none;
              line-height: 60px;"
     target="_blank"
     href="${link}">
    POTVRDITE
  </a>   
  
  </div>
`;
  
  return html;
  
};