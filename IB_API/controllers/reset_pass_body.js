
exports.conf_html = function ( link_za_reset_pass, user ) {

 
  var html = `

<div style="height: auto;
            text-align: center;
            font-family: Arial, Helvetica, sans-serif;
            background-color: #f3fcff;
            padding: 20px 0 0;">

    <div style="height: auto;
                text-align: center;
                font-family: Arial, Helvetica, sans-serif;
                padding: 3px;">

 
      <div style="  font-size: 16px;
                    margin-top: 20px;
                    margin-bottom: 20px;
                    color: #06253f;
                    font-weight: 400;
                    line-height: 22px;
                    letter-spacing: normal;">
        
        Resetiranje Velprom lozinke <br>
      </div>

      <div style="    
        font-size: 13px;
        margin-top: 40px;
        margin-bottom: 20px;
        color: #06253f;
        font-weight: 400;
        text-align: left;
        line-height: 26px;
        max-width: 400px;
        clear: both;
        width: auto;
        margin: 0 auto;
        padding: 10px;
        height: auto;
        background-color: #fff;
        border-radius: 10px;
        border: 1px solid #e8e6e6;
        letter-spacing: 0.04rem;
        text-align: center;">

<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div> 

Vaše korisničko ime u Velprom aplikaciji je:
<br>
<br>
<div style="font-size: 20px;">${user.name}</div>
<br>
<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div>
<div style="font-size: 12px">Pritiskom na tipku ispod potvrđujete vlasništvo nad ovom mail adresom. Nakon potvrde email-a možete koristiti vašu novu lozinku za login u Velprom App</div>

<div style="border-top: 1px solid #e8e6e6; margin: 5px 0; line-height: 1px; font-size: 1px; clear: both"></div>

        
  <a style="  background-color: #06253f;
              border-radius: 8px;
              height: 60px;
              width: 200px;
              font-size: 14px;
              margin: 20px auto 0;
              text-align: center;
              display: block;
              min-width: 200px;
              color: #fff;
              text-decoration: none;
              line-height: 60px;"
     
     target="_blank"
     
     href="${ global.cit_domain + '/pass_reset_confirm/' + link_za_reset_pass}">
    
    POTVRDITE
  </a>

<br style="display: block; clear: both;">
</div>

      <br style="display: block; clear: both;">
      
    </div>


    <div style="height: auto;
                text-align: center;
                font-family: Arial, Helvetica, sans-serif;
                background-color: #f3fcff;
                padding: 0;">

      
      
        <img style="
          width: 200px;
          height: auto;
          max-width: 200px;
          border-radius: 0;
          margin-top: 100px;
          display: block;
          float: none;
          clear: both;
          margin: 20px auto;"
          alt="Velprom Logo" 
          src="${global.cit_domain}/img/velprom_logo_email.png" />



      <div style="
        font-size: 11px;
        margin-top: 0;
        color: #727272;
        font-weight: 400;
        float: left;
        width: 100%;
        line-height: 18px;
        border-top: 1px solid #eaeaea;
        padding-top: 10px;
        background-color: #fff;
        letter-spacing: 0.06rem;">

        
        
          ${global.velprom_footer}
        
      </div>        
      
      <br style="display: block; clear: both;">
    </div>  

    
</div>   
  
`;
  
  return html;
  
};