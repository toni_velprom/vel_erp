'use strict';

var cors = require('cors');

module.exports = function(app) {
  
  
  app.use(cors());
  
  var userQueries = require('../controllers/user_controller');
  // var findQueries = require('../controllers/find_controller');

  app.route('/authenticate')
    .post(userQueries.authenticate_a_user);
  
  app.route('/prolong')
    .post(userQueries.prolong_token);  
  
    
  app.route('/logout_user')
    .post(userQueries.logout_user);  
      
  app.route('/im_online')
    .post(userQueries.im_online);  
  
  
  app.route('/test_toni')
    .get(userQueries.test_toni); 
  
  
  
  app.route('/make_new_help_user')
    .post(userQueries.make_new_help_user); 
  
  
  app.route('/delete_help_user')
    .post(userQueries.delete_help_user);
  
  
  app.route('/update_help_user')
    .post(userQueries.update_help_user);
    
  
  app.route('/get_IB_users')
    .post(userQueries.get_IB_users);
    
  

    // ----------------- TESTOVI  -----------------

  app.route('/get_all_user_tests')
    .post(userQueries.get_all_user_tests);
    
  
  app.route('/new_user_tests')
    .post(userQueries.new_user_tests);
    
    
  app.route('/update_user_tests')
    .post(userQueries.update_user_tests);
    
  
  app.route('/delete_user_tests')
    .post(userQueries.delete_user_tests);

  
  
  // ------------------- SVA PITANJA ---------------------


  app.route('/get_all_pits')
    .post(userQueries.get_all_pits);

  app.route('/new_pits')
    .post(userQueries.new_pits);

  app.route('/update_pits')
    .post(userQueries.update_pits);
  

  app.route('/delete_pits')
    .post(userQueries.delete_pits);
  

  // ------------------- SVI TEMPLATI TESTOVA ---------------------

  app.route('/get_all_test_temps')
    .post(userQueries.get_all_test_temps);

  app.route('/new_test_temp')
    .post(userQueries.new_test_temp);

  app.route('/update_test_temp')
    .post(userQueries.update_test_temp);

  app.route('/delete_test_temp')
    .post(userQueries.delete_test_temp);


  
};
