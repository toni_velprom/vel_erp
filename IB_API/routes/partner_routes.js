'use strict';

var cors = require('cors');

var partner_controller = require('../controllers/partner_controller');


module.exports = function(app) {
  
  
  app.use(cors());
  
  app.route('/save_partner')
    .post(partner_controller.save_partner);

};
