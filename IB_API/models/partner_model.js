'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

// var ParkPlaceOwner = mongoose.model('ParkPlaceOwner');

var partner_obj = { 
  

  user_saved: { type: Schema.Types.Mixed, required: false },
  user_edited: { type: Schema.Types.Mixed, required: false },
  
  saved: { type: Number, required: false },
  edited: { type: Number, required: false },
  last_login: { type: Number, required: false },
  
  
  user_address: { type: String, required: false },
  user_tel: { type: String, required: false },
  
  business_name: { type: String, required: false },
  business_address: { type: String, required: false },
  business_email: { type: String, required: false },
  business_tel: { type: String, required: false },
  
  business_contact_name: { type: String, required: false },
  
  oib: { type: String, required: false },
  iban: { type: String, required: false },
  
  pdv: { type: Boolean, required: false },
  obrtnik: { type: Boolean, required: false },
  
  gdpr: { type: Boolean, required: false },
  
  /* START ---------- OVO SU SVE TIME STAMPOVI */
  gdpr_saved: { type: Number, required: false },
  gdpr_accassed: { type: Number, required: false },
  user_deleted: { type: Number, required: false },
  /* END ---------- OVO SU SVE TIME STAMPOMVI */
  
  
  name: { type: String, required: true },
  user_number: { type: Number, required: false },
  
  full_name: { type: String, required: false },
  
  
}; 

var partner_schema = new Schema( 
  partner_obj,  
  { collection : 'partners'});  
 
module.exports = mongoose.model('Partner', partner_schema);
