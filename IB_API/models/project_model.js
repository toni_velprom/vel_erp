'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

// var ParkPlaceOwner = mongoose.model('ParkPlaceOwner');

var project_obj = { 
  project_id: { type: Number, required: false },
  project_name: { type: String, required: false },
  project_referent: { type: String, required: false },
  project_status: { type: Schema.Types.Mixed, required: false },
  project_komentar: { type: String, required: false },
  project_interni_komentar: { type: String, required: false },
  project_rok_za_def: { type: Number, required: false },
  project_potrebno_dana: { type: Number, required: false },
  project_rok_isporuke: { type: Number, required: false },
  project_items: [{ type: Schema.Types.ObjectId, ref: 'Item', required: false }],
}; 

var project_schema = new Schema( 
  project_obj,  
  { collection : 'projects'});  
 
module.exports = mongoose.model('Project', project_schema);
