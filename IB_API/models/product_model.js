'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

// var ParkPlaceOwner = mongoose.model('ParkPlaceOwner');

var product_obj = { 
  
  prod_id: { type: Number, required: false },
  prod_name: { type: String, required: false },
  
  prod_klasa: { type: Schema.Types.Mixed, required: false },
  prod_tip: { type: Schema.Types.Mixed, required: false },
  
  prod_trenutno_odgovoran: { type: String, required: false },
  
  prod_status: { type: Schema.Types.Mixed, required: false },
  
  prod_interni_komentar: { type: String, required: false },
  prod_komentar: { type: String, required: false },
  
  prod_rok_za_definiranje_upita: { type: Number, required: false },
  prod_dani_realizacije: { type: Number, required: false },
  prod_rok_isporuke: { type: Number, required: false },
  
}; 

var product_schema = new Schema( 
  product_obj,  
  { collection : 'products'});  
 
module.exports = mongoose.model('Product', product_schema);
