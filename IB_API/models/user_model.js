'use strict'; 
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 


var User_Object = { 
  
  user_saved: { type: Schema.Types.Mixed, required: false },
  user_edited: { type: Schema.Types.Mixed, required: false },
  
  saved: { type: Number, required: false },
  edited: { type: Number, required: false },
  
  last_login: { type: Number, required: false },
  help_times: { type: Schema.Types.Mixed, required: false },
  
  
  user_address: { type: String, required: false },
  user_tel: { type: String, required: false },
  
  name: { type: String, required: true },
  user_number: { type: Number, required: false },
  avatar: { type: String, required: false },
  title: { type: String, required: false },
  
  full_name: { type: String, required: false },
  
  hash: { type: String, required: true },
  email: { type: String, required: true },
  email_confirmed: { type: Boolean, required: false },
  welcome_email_id: { type: String, required: true },
  roles: { type: Schema.Types.Mixed, required: false },
  
  new_pass_request: { type: String, required: false },
  new_pass_email_id: { type: String, required: false },
  new_pass_email_confirmed: { type: Boolean, required: false },
  
}; 



var UserSchema = new Schema( 
  User_Object,  
  { collection : 'users'});  
 
module.exports = mongoose.model('User', UserSchema);
