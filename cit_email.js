process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

var mongoose = require('mongoose');
// mongoose.set('useFindAndModify', false);

var User = mongoose.model('User');
var jwt = require('jsonwebtoken');

var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);

var nodemailer = require('nodemailer');


global.vel_email_address = 'app@velprom.hr';

global.vel_mail_setup = {
  host: "mail.velprom.hr",
  port: 587,
  secure: false, // use TLS,
  requireTLS: true,
  auth: {
    user: global.vel_email_address,
    pass: 'eukaliptus69'
  },
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false
  }
};


function get_user_mail(user_mail) {
  
  var mail_pass = [
    { "toni.kutlic@velprom.hr": 'eukaliptus69' },
  ];
  
  var mail_setup = {
    
    host: "mail.velprom.hr",
    port: 587,
    secure: false, // use TLS,
    requireTLS: true,
    auth: {
      user: mails[user_mail],
      pass: mail_pass[user_mail]
    },
    tls: {
      // do not fail on invalid certs
      rejectUnauthorized: false
    }
  };
  
  return mail_setup;
  
};




global.cit_send_mail = function ( arg_body, arg_subject, to_arr, cc_arr, bcc_arr, att_files, arg_from ) {
  
  var transporter = nodemailer.createTransport(global.vel_mail_setup);
  
  if ( arg_from ) {
    transporter = nodemailer.createTransport( get_user_mail(arg_from.mail) );
  };
  
  return new Promise( function(resolve, reject) {

    var attachments = null;

    if ( att_files && $.isArray(att_files) && att_files.length > 0  ) {

      attachments = [];

      $.each( att_files, function(f_ind, filename) {

        attachments.push({
          filename: filename,
          path: global.appRoot + '/public/docs/' + filename, 
        });

      });

    };

    // setup email data
    var mailOptions = {
      from: `"Velprom App" <${global.vel_email_address}>`, // sender address
      to: to_arr, // list of receivers
      cc: cc_arr || null,
      bcc: bcc_arr || null,
      subject: arg_subject, // Subject line
      html:  arg_body, // html body
      attachments: attachments,
    };
    
    
    if ( arg_from ) {
      
      mailOptions = {
        from: `"${arg_from.full_name}" <${arg_from.email}>`, // sender address
        to: to_arr, // list of receivers
        cc: cc_arr || null,
        bcc: bcc_arr || null,
        subject: arg_subject, // Subject line
        html:  arg_body, // html body
        attachments: attachments,
      };
      
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (mail_err, info ) => {

      if (mail_err) {
        
        console.error(`GREŠKA KOD SLANJA EMAIL-a`);
        console.error(mail_err);
        resolve({ success: false, msg: `GREŠKA KOD SLANJA EMAIL-a`, error: mail_err });

      } else {

        console.log(`POSLAN EMAIL`);
        resolve({ success: true, info: info });
        
      };

    }); // end of send  email


  }); // kraj promisa

  
  
  
};


