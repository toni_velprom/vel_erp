async function offer_kupac() {

    
    var this_comp_id = $(this).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;
    
    var product_id = $(this).closest('.product_comp')[0].id;
    var product_data =  this_module.product_module.cit_data[product_id];
    
    var project_id = $(this).closest('.project_comp')[0].id;
    var project_data =  this_module.project_module.cit_data[project_id];

    
    var sjediste = concat_full_adresa( project_data.kupac, `VAD1`); // VAD1 je sjedište

    if ( !sjediste ) {
      popup_warn(`Kupac nema adresu sjedišta!!!<br>Potrebno otvoriti karticu kupca i napraviti sjedište.`);
      return;
    };

    $.each( project_data.kupac.kontakti, function (k_ind, kontakt) {
      if ( kontakt.adresa_kontakta?.vrsta_adrese?.sifra == `VAD1` ) {
        data.partner_adresa = kontakt;
        data.partner_adresa.adrese = [ data.partner_adresa.adresa_kontakta ]
        data.partner_adresa.full_adresa = concat_full_adresa( data.partner_adresa );
      };
    });  
    
    
    if ( !product_data.doc_adresa ) {
      popup_warn(`Niste izabrali adresu primatelja za ovaj dokument!!!`);
      return;
    };
    
    
    
    if ( !product_data.dostav_adresa ) {
      popup_warn(`Niste izabrali adresu dostave za ovaj dokument!!!`);
      return;
    };
    
    
    refresh_simple_cal( $('#'+data.id+`_doc_rok_time`) );

    
    if ( !data.doc_lang ) {
      data.doc_lang = { sifra: `hr`, naziv: `HRVATSKI` };
      
      //popup_warn(`Izaberite jezik dokumenta!`);
      // return;
    };
    
    
    if ( !project_data.kupac ) {
      popup_warn(`Potrebno izabrati kupca u projektu!`);
      return;
    };
    
    
    if ( !data.doc_rok_time ) {
      popup_warn(`Potrebno izabrati rok dokumenta!`);
      return;
    };
    
    
    if ( 
      product_data?.rok_za_def // samo ako postoji rok za def u podacima proizvoda
      &&
      data.doc_rok_time > product_data.rok_za_def // samo ako je odabrani datum veći od roka za definiciju
    ) {
      popup_warn(`Rok za definiciju proizvoda je:<br><br>${ cit_dt( product_data.rok_za_def).date } !!!<br><br>Ne možeš izabrati datum iza tog datuma :P`);
      $(`#${data.id}_doc_rok_time`).val(``);
      data.doc_rok_time = null;
      return;
    };
    
    
    var preview_mod = await get_cit_module('/modules/previews/cit_previews.js', `load_css`);
    
    console.log(`PODACI OD KUPCA ----------------- !!!!! `);
    
    var db_partner = null;
    var kupac_id = project_data.kupac?._id ? project_data.kupac._id : null;
    
    if( !kupac_id ) {
      popup_warn(`Nedostaje info o kupcu unutar projekta !!!`);
      return;
    };
    
    if ( kupac_id ) {
      db_partner = await get_partner( kupac_id );
    };

    if ( !db_partner ) {
      popup_error(`Greška prilikom pretrage partnera u bazi !!!!`);
      return;
    };
    
    var items = [];
    
    $.each( data.offer_kalk, function(k_ind, kalk) {
      
      if ( kalk.show_in_offer ) {
        
        var price_start = kalk.kalk_final_unit_price;
        var price_final = null;
        
        // postoji popust u kunama onda izračunaj popust u %
        if ( kalk.kalk_discount_money ) {
          kalk.kalk_discount = 100 * kalk.kalk_discount_money / (price_start * kalk.kalk_naklada);
          format_number_input( kalk.kalk_discount, $(`#`+kalk.kalk_sifra+`_kalk_discount`)[0], 2);
        };
        // ako mi fali popust u kn onda ga postavi na nula
        if ( !kalk.kalk_discount_money ) kalk.kalk_discount_money = 0;
        // ako marža u kunama onda izračunaj maržu u %
        if ( kalk.kalk_markup_money ) {
          kalk.kalk_markup = 100 * kalk.kalk_markup_money / (price_start * kalk.kalk_naklada);
          format_number_input( kalk.kalk_markup, $(`#`+kalk.kalk_sifra+`_kalk_markup`)[0], 2);
        };
        // ako mi fali marža u parama on je postavi na nula
        if ( !kalk.kalk_markup_money ) kalk.kalk_markup_money = 0;
        
        

        // NEMA NI MARŽU NI POPUST
        if ( !kalk.kalk_markup && !kalk.kalk_discount ) price_final = price_start;
        
        // ima maržu nema popust
        if ( kalk.kalk_markup && !kalk.kalk_discount ) price_final = price_start * (100 + kalk.kalk_markup )/100;
        // ima popust nema maržu
        if ( !kalk.kalk_markup && kalk.kalk_discount ) price_final = price_start * (100 - kalk.kalk_discount )/100;

        // ima i maržu i popust
        if ( kalk.kalk_markup && kalk.kalk_discount ) {

          // startna cijena je sada s popustom
          price_start = kalk.kalk_final_unit_price * (100 + kalk.kalk_markup )/100;
          price_final = price_start * (100 - kalk.kalk_discount )/100;
        };

        items.push({

          naziv: product_data.full_product_name || "", 
          jedinica: "kom",

          unit_price: price_start,
          discount: kalk.kalk_discount,
          dis_unit_price: price_final,

          count: kalk.kalk_naklada,
          dis_price: price_final * kalk.kalk_naklada,
          decimals: 3,

        });
        
      };
      
    });
    
    
    // pronadji kalkulacjiju koju sam označio da treba biti rezervirana !!!
    // pronadji kalkulacjiju koju sam označio da treba biti rezervirana !!!
    // pronadji kalkulacjiju koju sam označio da treba biti rezervirana !!!
    
    var selected_kalk = null;
    
    $.each( data.offer_kalk, function(k_ind, kalk) {
      
      if ( kalk.kalk_reserv == true ) {
        
        selected_kalk = cit_deep(kalk);
        selected_kalk.full_record_name = selected_kalk.kalk_naklada + " kom -- " + data.full_product_name;
        selected_kalk.proj_sifra = data.proj_sifra;
        selected_kalk.item_sifra = data.item_sifra;
        selected_kalk.variant = data.variant;
        selected_kalk.product_id = product_data._id;

        
      };
      
    });
    
    
    if ( !selected_kalk ) {
      popup_warn(`Prije generiranja ponude morate rezervirati robu za jednu od kalkulacija !!!`);
      return;
    };
    
    
    
    var sirov_ulazi = [];
    
    $.each( selected_kalk.procesi, function( p_ind, proces ) {
      
      $.each( proces.ulazi, function( u_ind, ulaz ) {
        
        if (ulaz._id) {
          
          var sirov_obj = {
            _id: ulaz._id,
            kalk_full_naziv: ulaz.kalk_full_naziv,
            kalk_kom_in_sirovina: ulaz.kalk_kom_in_sirovina,
            kalk_sum_sirovina: ulaz.kalk_sum_sirovina,
            kalk_skart: null,
          };
          sirov_ulazi.push(sirov_obj);
          
        };
        
      });
      
    });
    
    
    var data_for_doc = {

      for_module: `kalk`,
      
      proj_sifra : product_data.proj_sifra || null,
      item_sifra : product_data.item_sifra || null,
      variant : product_data.variant || null,
      status: null,
      
      
      sirov_id: null,
      full_record_name: product_data.full_product_name,
      
      
      doc_valuta: ( data.doc_valuta?.valuta || "HRK" ), // default je HRK
      doc_valuta_manual: (data.doc_valuta_manual || null ), // manual tečaj
      referent: window.cit_user.full_name,
      place: `Velika Gorica`,

      doc_type: `offer_reserv`,
      doc_num: `---`,

      oib: (db_partner.oib || db_partner.vat_num || ""),
      
      partner_name: db_partner.naziv,
      partner_adresa: data.partner_adresa.full_adresa,
      doc_adresa: product_data.doc_adresa.full_adresa,
      dostav_adresa: product_data.dostav_adresa.full_adresa,
      partner_id: db_partner._id,
      
      
      /*
      partner_place: `67122 ALTRIP`,
      partner_country: `Germany`,
      */
      
      rok: data.doc_rok_time || product_data.rok_za_def,
      dobav_ponuda_sifra: ``,
      
      items: items,
      sirov_ulazi: sirov_ulazi,
      
      
      kalk: selected_kalk,
      
    };
    
    // return;
    
    preview_mod.generate_cit_doc( data_for_doc, data.doc_lang.sifra );

  };
this_module.offer_kupac = offer_kupac;
  

async function order_kupac() {


var this_comp_id = $(this).closest('.cit_comp')[0].id;
var data = this_module.cit_data[this_comp_id];
var rand_id = this_module.cit_data[this_comp_id].rand_id;

var product_id = $(this).closest('.product_comp')[0].id;
var product_data =  this_module.product_module.cit_data[product_id];

var project_id = $(this).closest('.project_comp')[0].id;
var project_data =  this_module.project_module.cit_data[project_id];

var sjediste = concat_full_adresa( project_data.kupac, `VAD1`); // VAD1 je sjedište

if ( !sjediste ) {
  popup_warn(`Kupac nema adresu sjedišta!!!<br>Potrebno otvoriti karticu kupca i napraviti sjedište.`);
  return;
};

$.each( project_data.kupac.kontakti, function (k_ind, kontakt) {
  if ( kontakt.adresa_kontakta?.vrsta_adrese?.sifra == `VAD1` ) {
    data.partner_adresa = kontakt;
    data.partner_adresa.adrese = [ data.partner_adresa.adresa_kontakta ]
    data.partner_adresa.full_adresa = concat_full_adresa( data.partner_adresa );
  };
}); 


if ( !product_data.doc_adresa ) {
  popup_warn(`Niste izabrali adresu primatelja za ovaj dokument!!!`);
  return;
};  


if ( !product_data.dostav_adresa ) {
  popup_warn(`Niste izabrali adresu dostave za ovaj dokument!!!`);
  return;
};  


refresh_simple_cal( $('#'+data.id+`_doc_rok_time`) );


if ( !data.doc_lang ) {

  // ako nije izabrao onda stavi da je 
  data.doc_lang =  { sifra: `hr`, naziv: `HRVATSKI` };

  //popup_warn(`Izaberite jezik dokumenta!`);
  // return;
};  



if ( !project_data.kupac ) {
  popup_warn(`Potrebno izabrati kupca u projektu!`);
  return;
};


if ( !data.doc_rok_time ) {
  popup_warn(`Potrebno izabrati rok dokumenta!`);
  return;
};


if ( 
  product_data?.rok_za_def // samo ako postoji rok za def u podacima proizvoda
  &&
  data.doc_rok_time > product_data.rok_za_def // samo ako je odabrani datum veći od roka za definiciju
) {
  popup_warn(`Rok za definiciju proizvoda je:<br><br>${ cit_dt( product_data.rok_za_def).date } !!!<br><br>Ne možeš izabrati datum iza tog datuma :P`);
  $(`#${data.id}_doc_rok_time`).val(``);
  data.doc_rok_time = null;
  return;
};


var preview_mod = await get_cit_module('/modules/previews/cit_previews.js', `load_css`);

console.log(`PODACI OD KUPCA ----------------- !!!!! `);

var db_partner = null;
var kupac_id = project_data.kupac?._id ? project_data.kupac._id : null;


if( !kupac_id ) {
  popup_warn(`Nedostaje info o kupcu unutar projekta !!!`);
  return;
};


if ( kupac_id ) {
  db_partner = await get_partner( kupac_id );
};

if ( !db_partner ) {
  popup_error(`Greška prilikom pretrage partnera u bazi !!!!`);
  return;
};


var items = [];


$.each( data.pro_kalk, function(k_ind, kalk) {


  var price_start = kalk.kalk_final_unit_price;
  var price_final = null;

  // postoji popust u kunama onda izračunaj popust u %
  if ( kalk.kalk_discount_money ) {
    kalk.kalk_discount = 100 * kalk.kalk_discount_money / (price_start * kalk.kalk_naklada);
    format_number_input( kalk.kalk_discount, $(`#`+kalk.kalk_sifra+`_kalk_discount`)[0], 2);
  };
  // ako mi fali popust u kn onda ga postavi na nula
  if ( !kalk.kalk_discount_money ) kalk.kalk_discount_money = 0;
  // ako marža u kunama onda izračunaj maržu u %
  if ( kalk.kalk_markup_money ) {
    kalk.kalk_markup = 100 * kalk.kalk_markup_money / (price_start * kalk.kalk_naklada);
    format_number_input( kalk.kalk_markup, $(`#`+kalk.kalk_sifra+`_kalk_markup`)[0], 2);
  };
  // ako mi fali marža u parama on je postavi na nula
  if ( !kalk.kalk_markup_money ) kalk.kalk_markup_money = 0;


  // NEMA NI MARŽU NI POPUST
  if ( !kalk.kalk_markup && !kalk.kalk_discount ) price_final = price_start;

  // ima maržu nema popust
  if ( kalk.kalk_markup && !kalk.kalk_discount ) price_final = price_start * (100 + kalk.kalk_markup )/100;
  // ima popust nema maržu
  if ( !kalk.kalk_markup && kalk.kalk_discount ) price_final = price_start * (100 - kalk.kalk_discount )/100;

  // ima i maržu i popust
  if ( kalk.kalk_markup && kalk.kalk_discount ) {

    // startna cijena je sada s popustom
    price_start = kalk.kalk_final_unit_price * (100 + kalk.kalk_markup )/100;
    price_final = price_start * (100 - kalk.kalk_discount )/100;
  };



  items.push({
    naziv: product_data.full_product_name || "", 
    jedinica: "kom",

    unit_price: price_start,
    discount: kalk.kalk_discount,
    dis_unit_price: price_final,

    count: kalk.kalk_naklada,
    dis_price: price_final * kalk.kalk_naklada,
    decimals: 3,

  });

}); // kraj each kalk in pro ----> zapravo je uvijek jedan jedini !!! :)




// pronadji kalkulacjiju koju sam označio da treba biti rezervirana !!!
// pronadji kalkulacjiju koju sam označio da treba biti rezervirana !!!
// pronadji kalkulacjiju koju sam označio da treba biti rezervirana !!!

var selected_kalk = null;

$.each( data.offer_kalk, function(k_ind, kalk) {

  if ( kalk.kalk_for_pro == true ) {

    selected_kalk = cit_deep(kalk);
    selected_kalk.full_record_name = selected_kalk.kalk_naklada + " kom -- " + data.full_product_name;
    selected_kalk.proj_sifra = data.proj_sifra;
    selected_kalk.item_sifra = data.item_sifra;
    selected_kalk.variant = data.variant;
    selected_kalk.product_id = product_data._id;

  };

});

if ( !selected_kalk ) {
  popup_warn(`Prije generiranja ponude morate rezervirati robu za NK ( za jednu od kalkulacija)!!!`);
  return;
};

var sirov_ulazi = [];

$.each( selected_kalk.procesi, function( p_ind, proces ) {

  $.each( proces.ulazi, function( u_ind, ulaz ) {

    if ( ulaz._id ) {

      var sirov_obj = {
        _id: ulaz._id,
        kalk_full_naziv: ulaz.kalk_full_naziv,
        kalk_kom_in_sirovina: ulaz.kalk_kom_in_sirovina,
        kalk_sum_sirovina: ulaz.kalk_sum_sirovina,
        kalk_skart: null,
      };
      sirov_ulazi.push(sirov_obj);

    }; // kraj ako ima _id

  });

});


var data_for_doc = {

  for_module: `kalk`,

  proj_sifra : product_data.proj_sifra || null,
  item_sifra : product_data.item_sifra || null,
  variant : product_data.variant || null,
  status: null,


  sirov_id: null,
  full_record_name: product_data.full_product_name,


  doc_valuta: ( data.doc_valuta?.valuta || "HRK" ), // default je HRK
  doc_valuta_manual: (data.doc_valuta_manual || null ), // manual tečaj
  referent: window.cit_user.full_name,
  place: `Velika Gorica`,

  doc_type: `order_reserv`,
  doc_num: `---`,

  oib: (db_partner.oib || db_partner.vat_num || ""),

  partner_name: db_partner.naziv,
  partner_adresa: data.partner_adresa.full_adresa,
  doc_adresa: product_data.doc_adresa.full_adresa,
  dostav_adresa: product_data.dostav_adresa.full_adresa,
  partner_id: db_partner._id,


  /*
  partner_place: `67122 ALTRIP`,
  partner_country: `Germany`,
  */

  rok: data.doc_rok_time || product_data.rok_za_def,
  dobav_ponuda_sifra: ``,

  items: items,
  sirov_ulazi: sirov_ulazi,


  kalk: selected_kalk,

};

// return;

preview_mod.generate_cit_doc( data_for_doc, data.doc_lang.sifra );

};
this_module.order_kupac = order_kupac;


async function reserv_status_and_record( data_for_doc, arg_pdf_name, arg_sirovine ) {


  var this_comp_id = $(`#`+ data_for_doc.kalk.kalk_sifra + `_kalkulacija` ).closest('.cit_comp.kalk_comp')[0].id;
  var data = this_module.cit_data[this_comp_id];
  var rand_id =  this_module.cit_data[this_comp_id].rand_id;


  var product_div = $(`#`+ data_for_doc.kalk.kalk_sifra + `_kalkulacija` ).closest('.product_comp');

  var product_id = product_div[0].id;
  var product_data =  this_module.product_module.cit_data[product_id];


  var status_module = await get_cit_module(`/modules/status/status_module.js`, `load_css`);
  var status_comp_id = product_div.find(`.status_comp`)[0].id;
  var status_data = status_module.cit_data[status_comp_id];

  var sirov_ids = []; 
  var db_sirovine = [];

  var parent_record = null;
  var status_offer_reserv = null;
  var reply_for = null;
  var elem_parent_object = null;

  var all_sirov_records = [];
  var arg_storno_data = null;

  var link = ``;
  var komentar = ``;

  var status_tip = null;

  if ( !arg_sirovine ) {

    $.each( data_for_doc.sirov_ulazi, function( s_ind, sirov ) {
      sirov_ids.push(sirov._id);
    });

    // ---------------------------- AKO JE SAMO ZA JEDAN PROCES DODATNO !!!!!! ----------------------------
    // ---------------------------- AKO JE SAMO ZA JEDAN PROCES DODATNO !!!!!! ----------------------------
    // ---------------------------- AKO JE SAMO ZA JEDAN PROCES DODATNO !!!!!! ----------------------------

    if ( data_for_doc.proces ) {


      // ---------------------------------------- RESETIRAJ SIROV ULAZE ( koji su do sada bili za sve procese )
      var sirov_ulazi = [];

      $.each( data_for_doc.kalk.procesi, function( p_ind, proces ) {


        // samoi traži proces koji je u data_for_doc ----> sve ostale procese ignoriraj 
        if ( proces.row_sifra == data_for_doc.proces.row_sifra ) {

          $.each( proces.ulazi, function( u_ind, ulaz ) {

            if ( ulaz._id ) {

              var sirov_obj = {
                _id: ulaz._id,
                kalk_full_naziv: ulaz.kalk_full_naziv,
                kalk_kom_in_sirovina: ulaz.kalk_kom_in_sirovina,
                kalk_sum_sirovina: ulaz.kalk_sum_sirovina,
                kalk_skart: null,
              };

              sirov_ulazi.push(sirov_obj);

            }; // kraj ako ima _id

          });

        };

      });


      // ------------------------------------------ OVERRIDE postojeće sirov ulaze koji su za sve procese
      // ------------------------------------------ OVERRIDE postojeće sirov ulaze koji su za sve procese
      // ------------------------------------------ OVERRIDE postojeće sirov ulaze koji su za sve procese

      // i upiši samo sirovine za ovaj jedan jedini proces !!!
      data_for_doc.sirov_ulazi = sirov_ulazi;

      // ------------------------------------------ resetiraj sirov ids ------------------------------------------
      // ------------------------------------------ resetiraj sirov ids ------------------------------------------
      // ------------------------------------------ resetiraj sirov ids ------------------------------------------
      sirov_ids = [];

      $.each( data_for_doc.sirov_ulazi, function( s_ind, sirov ) {
        sirov_ids.push(sirov._id);
      });

    };

    // ---------------------------- AKO JE SAMO ZA JEDAN PROCES DODATNO !!!!!! ----------------------------
    // ---------------------------- AKO JE SAMO ZA JEDAN PROCES DODATNO !!!!!! ----------------------------
    // ---------------------------- AKO JE SAMO ZA JEDAN PROCES DODATNO !!!!!! ----------------------------


    var props = {
      filter: null,
      url: '/find_sirov',
      query: { _id: { $in:  sirov_ids } },
      desc: `pretraga sirovina za offer ili order reserv unutar kalkulacija`,
    };

    var db_result = [];

    try {
      db_result = await search_database(null, props );
    } catch (err) {
      console.log(err);
      return false;
    };

    if ( db_result && $.isArray(db_result) && db_result.length > 0 ) {
      db_sirovine = db_result;
    };


  } 
  else {

    db_sirovine = arg_sirovine;

  };


  // ------------ START ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )
  // ------------ START ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )
  // ------------ START ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )

  $.each( db_sirovine, function( s_ind, sirov ) {


    $.each( data_for_doc.sirov_ulazi, function(ulaz_ind, ulaz) {

      // ubaci podatke od svih sirov ulaza od trenutne kalkulacije
      if ( ulaz._id == sirov._id ) {
        sirov.kalk_full_naziv = ulaz.kalk_full_naziv;
        sirov.kalk_kom_in_sirovina = ulaz.kalk_kom_in_sirovina;
        sirov.kalk_sum_sirovina = ulaz.kalk_sum_sirovina;
        sirov.kalk_skart = ulaz.kalk_skart;
      };

    });


    // ------------------ PRONALAZAK PARENT RECORDA ---------------------- 
    // ------------------ PRONALAZAK PARENT RECORDA ---------------------- 

    // traži parent record samo ako nije offer reserv
    if ( data_for_doc.doc_type !== `offer_reserv`) {

      // 
      if ( sirov.records ) {

        $.each(sirov.records, function(rec_ind, rec ) {

          if ( 
            rec.record_kalk.kalk_sifra == data_for_doc.kalk.kalk_sifra
          ) {

            // ako ovaj record već ima parenta onda uzmi tog parenta tako da imaju zajednički parent
            if ( rec.record_parent ) {
              parent_record = cit_deep(rec.record_parent);
            } 
            else {
              // ako ovaj record NEMA parent onda neka taj record postane parent !!!
              parent_record = cit_deep(rec);
            };

          };

        }); 

      };

    };

    // ------------------ PRONALAZAK PARENT RECORDA ---------------------- 
    // ------------------ PRONALAZAK PARENT RECORDA ---------------------- 


    // ------------------PRONALAZAK PARENT RECORDA ---------------------- SPECIJALNO ZA RADNI NALOG ----------------------------------------
    // ------------------PRONALAZAK PARENT RECORDA ---------------------- SPECIJALNO ZA RADNI NALOG ----------------------------------------

    if ( data_for_doc.doc_type == `radni_nalog`) {


      // pošto sifra kalkulacije za radni nalog nije ista kao šifra kalkulacije za proizvodnju
      // radni nalog se nalazi u  PRO klak dok je reservacija za proizvodnju u OFFER kalk !!!!!

      var kalk_offer_for_pro = null;
      $.each( data.offer_kalk, function(k_ind, kalk) {
        if ( kalk.kalk_for_pro == true ) kalk_offer_for_pro = kalk;
      });

      if ( sirov.records ) {

        $.each( sirov.records, function(rec_ind, rec ) {

          // inače uspoređujem kalk šifre ali pošto je radni nalog iz pro_kalk
          // a rezervacija po NK je iz offer kalk
          // te dvije kalkulacije imaju posve različite šifre
          // ZATO USPOREĐ
          if ( 
            rec.type == `order_reserv`
            &&
            rec.record_kalk.kalk_sifra == kalk_offer_for_pro.kalk_sifra

          ) {

            // ako ovaj record već ima parenta onda uzmi tog parenta tako da imaju zajednički parent
            if ( rec.record_parent ) {
              parent_record = cit_deep(rec.record_parent);
            } 
            else {
              // ako ovaj record NEMA parent onda neka taj record postane parent !!!
              parent_record = cit_deep(rec);

            };

          };

        }); 

      }; // ako postoje sirov records


    }; // kraj ako je ovo kriranje statusa za radni nalog

    // ------------------PRONALAZAK PARENT RECORDA ---------------------- SPECIJALNO ZA RADNI NALOG ----------------------------------------
    // ------------------PRONALAZAK PARENT RECORDA ---------------------- SPECIJALNO ZA RADNI NALOG ----------------------------------------



    var doc_link = 
`
<a href="/docs/${arg_pdf_name}" target="_blank" onClick="event.stopPropagation();" > 
  ${ this_module.sirov_module.map_name_to_type[data_for_doc.doc_type] }: ${ data_for_doc.doc_sifra }
</a>
`;  

    var count = sirov.kalk_sum_sirovina || 0;

    var record_est_time = product_data.rok_za_def;

    var new_record = {

      sirov_id: sirov._id,

      count: count,

      record_parent: parent_record,

      time: Date.now(),

      sifra: `record`+cit_rand(),

      doc_sifra: data_for_doc.doc_sifra,

      pdf_name: arg_pdf_name,

      doc_link: doc_link,

      type: data_for_doc.doc_type,

      record_statuses: null,

      record_kalk: {

        proj_sifra: data_for_doc.kalk.proj_sifra,
        item_sifra: data_for_doc.kalk.item_sifra,
        variant: data_for_doc.kalk.variant,
        kalk_sifra: data_for_doc.kalk.kalk_sifra, // samo uzmi sifru
        full_record_name: (data_for_doc.kalk.full_product_name || data_for_doc.kalk.full_record_name),

      },

      record_est_time: record_est_time, // procjena vremena kada će doći do promjene

      storno_time: null,
      storno_kolicina: null,

      pk_kolicina: data_for_doc.doc_type == `offer_reserv` ? count : null,
      nk_kolicina: data_for_doc.doc_type == `order_reserv` ? count : null,
      rn_kolicina: data_for_doc.doc_type == `radni_nalog` ? count : null,
      order_kolicina: null,
      sklad_kolicina: null,

      partner_name: data_for_doc.partner_name,
      partner_adresa: data_for_doc.partner_adresa,
      partner_id: data_for_doc.partner_id,


    };

    all_sirov_records.push(new_record);

  });
  // kraj loopa po svim sirovinama u kalkulaciji

  // ------------ END ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )
  // ------------ END ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )
  // ------------ END ------------------------ LOOP PO SIROVINAMA U KALKULACIJI ( uzeo ih iz baze )

  var save_result = await this_module.save_sirov_records(all_sirov_records);

  // može biti bilo koji element samo da je unutar post kalkulacije
  var post_kalk_first_elem = $(`#`+ data_for_doc.kalk.kalk_sifra + `_kalk_komentar`)[0];

  if ( data_for_doc.doc_type !== `radni_nalog`) {
    this_module.make_new_kalk_status(data_for_doc, arg_pdf_name, null, post_kalk_first_elem );
  } 
  else {
    this_module.make_radni_proces_statuses(data_for_doc, arg_pdf_name, null, null);
  };

};
this_module.reserv_status_and_record = reserv_status_and_record;



async function make_new_kalk_status( this_elem ) {


  if ( storno_records ) {
    this_elem = $(`#` + data_for_doc.kalk.kalk_sifra + `_kalk_komentar` )[0];
  };

  var this_comp_id = $(this_elem).closest('.cit_comp')[0].id;
  var data = this_module.cit_data[this_comp_id];
  var rand_id =  this_module.cit_data[this_comp_id].rand_id;


  var product_id = $(this_elem).closest('.product_comp')[0].id;
  var product_data =  this_module.product_module.cit_data[product_id];


  var status_module = await get_cit_module(`/modules/status/status_module.js`, `load_css`);

  // prvo nadjem parent product component pa onda pomoću njega nadjem status component
  var status_comp_id = $(this_elem).closest('.product_comp').find(`.status_comp`)[0].id;
  var status_data = status_module.cit_data[status_comp_id];


  var kalk_box = $(this_elem).closest('.kalk_box');

  var kalk_type = kalk_box.attr(`data-kalk_type`);
  var kalk_sifra = kalk_box.attr(`data-kalk_sifra`);

  var curr_kalk = this_module.kalk_search(data, kalk_type, kalk_sifra, null, null, null, null );


  var curr_reserv_sifra = curr_kalk.kalk_sifra;
  var curr_reserv_state = curr_kalk.kalk_reserv;


  var parent_status = null;
  var reply_for = null;
  var elem_parent_object = null;


  var new_status_sifra = `status`+cit_rand();
  var time_now = Date.now();

  if ( !window.cit_user ) {
    popup_warn(`Niste se ulogirali !!!`);
    return;
  };


  var status_tip = null;
  var komentar = null;
  var link = null;


  if ( !storno_records && data_for_doc.doc_type == `offer_reserv` ) {


    komentar = "REZERVIRANA ROBA PO PONUDI (automatski kreiran status)";


    link = `#project/${data_for_doc.kalk.proj_sifra || null}/item/${data_for_doc.kalk.item_sifra || null}/variant/${data_for_doc.kalk.variant || null}/kalk/${data_for_doc.kalk.kalk_sifra || null}/proces/null`;


    status_tip = {

      "naziv" : "REZERVACIJA ROBE PO PONUDI KUPCA",
      "def_deadline" : null,
      "cat" : {
          "sifra" : "preprod",
          "naziv" : "PRED-PROIZVODNJA"
      },
      "dep" : {
          "sifra" : "SAL",
          "naziv" : "Prodaja"
      },
      "sifra" : "IS16424230681369185",
      "is_work" : null,
      "active" : true,
      "next_statuses" : [],
      "grana_finish" : [ 
        {
            "naziv" : "REZERVACIJA ROBE PO NARUDŽBI KUPCA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16424231019669587",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 9200,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : 20
        }, 
        {
            "naziv" : "ODUSTAJMO OD OVE PONUDE I NARUDŽBE",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS1642423151409516",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 9300,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : 30
        }, 
        {
            "sifra" : "ISerror",
            "active" : true,
            "next_status_num" : 50,
            "naziv" : "GREŠKA UPISA",
            "next_statuses" : [],
            "grana_finish" : [],
            "is_work" : null,
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "next_statuses_string" : "",
            "grana_finish_string" : ""
        }
      ],
      "status_num" : 9100,
      "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
      "cat_naziv" : "PRED-PROIZVODNJA",
      "dep_naziv" : "Prodaja",
      "active_status" : true,
      "work_done_for" : null,
      "work_done_for_naziv" : "",
      "work_finished" : null,
      "button_edit" : true,
      "next_status_num" : null
    };

  };

  // ako je status REZERVACIJA ROBE PO NARUDŽBI KUPCA
  if ( !storno_records && data_for_doc.doc_type == `order_reserv` ) {


    komentar = "REZERVIRANA ROBA PO NK (automatski kreiran status)";

    link = `#project/${data_for_doc.kalk.proj_sifra || null}/item/${data_for_doc.kalk.item_sifra || null}/variant/${data_for_doc.kalk.variant || null}/kalk/${data_for_doc.kalk.kalk_sifra || null}/proces/null`;


    status_tip = {

      "naziv" : "REZERVACIJA ROBE PO NARUDŽBI KUPCA",
      "def_deadline" : null,
      "cat" : {
          "sifra" : "preprod",
          "naziv" : "PRED-PROIZVODNJA"
      },
      "dep" : {
          "sifra" : "SAL",
          "naziv" : "Prodaja"
      },
      "sifra" : "IS16424231019669587",
      "is_work" : null,
      "active" : true,
      "next_statuses" : [],
      "grana_finish" : [],
      "status_num" : 9200,
      "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
      "cat_naziv" : "PRED-PROIZVODNJA",
      "dep_naziv" : "Prodaja",
      "active_status" : true,
      "work_done_for" : null,
      "work_done_for_naziv" : "",
      "work_finished" : null,
      "button_edit" : true,
      "next_status_num" : 20
    };

  };


  if ( storno_records ) {

    komentar = `RESERV. PO PONUDI PRETVOREN U RESERV. PO NK (automatski kreiran status)`;

    // uzmi prvi storno record zato što su podaci od producta i kalkulacije za sve recorde POSVE JEDNAKI 
    // uzmi prvi storno record zato što su podaci od producta i kalkulacije za sve recorde POSVE JEDNAKI 
    link = `#project/${ storno_records[0].record_kalk.proj_sifra || null}/item/${ storno_records[0].record_kalk.item_sifra || null}/variant/${storno_records[0].record_kalk.variant || null }/kalk/${storno_records[0].record_kalk.kalk_sifra || null}/proces/null`;

    status_tip = {

      "naziv" : "ODUSTAJEMO OD OVE PONUDE I NARUDŽBE",
      "cat" : {
          "sifra" : "preprod",
          "naziv" : "PRED-PROIZVODNJA"
      },
      "dep" : {
          "sifra" : "SAL",
          "naziv" : "Prodaja"
      },
      "sifra" : "IS1642423151409516",
      "is_work" : null,
      "active" : true,
      "next_statuses" : [],
      "grana_finish" : [],
      "def_deadline" : null,
      "status_num" : 9300,
      "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
      "cat_naziv" : "PRED-PROIZVODNJA",
      "dep_naziv" : "Prodaja",
      "active_status" : true,
      "work_done_for" : null,
      "work_done_for_naziv" : "",
      "work_finished" : null,
      "button_edit" : true,
      "next_status_num" : 30

      };


  };


  if ( storno_records || data_for_doc.doc_type == `order_reserv` ) {

    $.each( product_data.statuses, function( s_ind, status ) {
      /* ovo je sifra za REZERVACIJA ROBE PO PONUDI KUPCA  ----> to je parent status */
      if ( status.status_tip.sifra ==  `IS16424230681369185` && status.branch_done !== true ) {
        parent_status = cit_deep(status);
        reply_for = status.sifra;
      };
    });

  };

  var status_docs = [
    {
      sifra: `doc`+cit_rand(),
      time: Date.now(),
      link: `<a href="/docs/${arg_pdf_name}" target="_blank" onClick="event.stopPropagation();" >${ arg_pdf_name }</a>`,
    }
  ];

  var reserv_status = {

    "full_product_name" : (data_for_doc.kalk.full_product_name || data_for_doc.kalk.full_record_name),
    "product_id" : data_for_doc.kalk.product_id,
    "link" : link,
    "proj_sifra" : data_for_doc.kalk.proj_sifra,
    "item_sifra" : data_for_doc.kalk.item_sifra,
    "variant" : data_for_doc.kalk.variant,
    "sifra" : new_status_sifra,
    "status" : new_status_sifra,
    "elem_parent" : parent_status?.sifra || null, // ako curr status već ima parent onda uzmi njega
    "elem_parent_object" : parent_status || null,
    "time" : time_now,
    "work_times" : null,
    "docs" : status_docs,
    "est_deadline_hours" : null,

    "est_deadline_date" : (product_data.rok_za_def || data_for_doc.rok || null),

    "start" : time_now,
    "end" : null,
    "old_status_tip" : null,
    "status_tip" : status_tip,
    "dep_from" : {
        "sifra" : window.cit_user.dep.sifra,
        "naziv" : window.cit_user.dep.naziv,
    },
    "from" : {
        "_id" : window.cit_user._id,
        "name" : window.cit_user.name,
        "user_number" : window.cit_user.user_number,
        "full_name" : window.cit_user.full_name,
        "email" : window.cit_user.email,
    },

    "dep_to" : {
        "sifra" : window.cit_user.dep.sifra,
        "naziv" : window.cit_user.dep.naziv,
    },
    "to" : {
        "_id" : window.cit_user._id,
        "name" : window.cit_user.name,
        "user_number" : window.cit_user.user_number,
        "full_name" : window.cit_user.full_name,
        "email" : window.cit_user.email,
    },
    "old_komentar" : parent_status?.komentar || null,
    "komentar" : komentar || null,
    "seen" : null,
    "reply_for" : reply_for || null,
    "responded" : null,
    "done_for_status" : null,
    "work_finished" : null,
    "sirov" : null,
    "order_sirov_count" : null,
    "record_statuses" : [],

    "rok_isporuke" : product_data.rok_isporuke || null,
    "potrebno_dana_proiz" : product_data.potrebno_dana_proiz || null,
    "potrebno_dana_isporuka" : product_data.rok_isporuke || null,
    "rok_proiz" : product_data.rok_proiz || null,
    "rok_za_def" : product_data.rok_za_def || null,
    "branch_done" :  parent_status ? true : false, // ako postoji parent za offer reserv onda je ovo kraj grane

  };

  // napravi flat objekte i arraye
  $.each( reserv_status, function( key, value ) {
    // ako je objekt ili array onda ga flataj
    if ( $.isPlainObject(value) || $.isArray(value) ) {
      reserv_status[key+'_flat'] = JSON.stringify(value);
    };
  });

  $.ajax({
    headers: {
      'X-Auth-Token': window.cit_user.token
    },
    type: "POST",
    cache: false,
    url: `/save_status`,
    data: JSON.stringify( reserv_status ),
    contentType: "application/json; charset=utf-8",
    dataType: "json"
  })
  .done( async function (result) {

    console.log(result);

    if ( result.success == true ) {

      cit_toast(`STATUS REZERVACIJE JE SPREMLJEN!`);
      cit_toast(`Poslan status prema ${ reserv_status.to.full_name}`, `background: #3f6ad8;`);
      cit_socket.emit('new_status', reserv_status );
      animate_badge( $(`#cit_task_from_me`), 1 );

      reserv_status._id = result.status._id;

      product_data.statuses = upsert_item( product_data.statuses, reserv_status, `sifra` );
      status_data.statuses = upsert_item( status_data.statuses, reserv_status, `sifra` );

      status_module.make_status_list(status_data);
      status_module.remove_header_arrows_and_blink(status_data);

      // --------------------------------
      var doc_type_human_name = "";


      if ( !storno_records ) {
        // ------------------------------------ SAMO AKO NIJE STORNO ------------------------------------

        if ( data_for_doc.doc_type == "offer_reserv" ) doc_type_human_name = " PONUDA KUPCU ";
        if ( data_for_doc.doc_type == "order_reserv" ) doc_type_human_name = " POTVRDA NARUDŽBE KUPCA ";

        var komentar = ( data_for_doc.doc_sifra ? data_for_doc.doc_sifra : "" )  + doc_type_human_name + " (Automatski generiran dokument)";

        var new_kalk_spec = {

          "docs": [
            {
              "sifra": "doc"+cit_rand(),
              "time": Date.now(),
              "link": `<a href="/docs/${arg_pdf_name}" target="_blank" onClick="event.stopPropagation();" >${ arg_pdf_name }</a>`
            }
          ],
          "sifra": "kalkspecs"+cit_rand(),
          "komentar": komentar,
          "time": Date.now(),
        };

        product_data.kalk_specs = upsert_item( product_data.kalk_specs, new_kalk_spec, `sifra`);

        this_module.product_module.make_kalk_specs_list(product_data);

        // ------------------------------------ SAMO AKO NIJE STORNO ------------------------------------
      };


      setTimeout( function() {  $(`#` + data.id + ` .save_offer_kalk_btn` ).trigger(`click`); }, 200);
      setTimeout( function() {  $(`#` + data.id + ` .save_pro_kalk_btn` ).trigger(`click`); }, 300);
      setTimeout( function() {  $(`#` + data.id + ` .save_post_kalk_btn` ).trigger(`click`); }, 400);
      setTimeout( function() {  $(`#`+ product_data.id + ` .save_product_btn` ).trigger(`click`); }, 500);


    } else {

      // ako result NIJE SUCCESS = true

      if ( result.msg ) console.error(result.msg);
      if ( !result.msg ) console.error(`Greška prilikom spremanja STATUSA!`);

    };

  })
  .fail(function (error) {
    console.error(`Došlo je do greške na serveru prilikom spremanja STATUSA!`);
    console.log(error);
  })
  .always(function() {
    toggle_global_progress_bar(false);
  });



};
this_module.make_new_kalk_status = make_new_kalk_status;



async function make_radni_nalog_statuses( data_for_doc, arg_pdf_name, storno_records, arg_sirovine ) {


  var product_id = $(`#`+ data_for_doc.kalk.kalk_sifra + `_kalkulacija` ).closest('.product_comp')[0].id;
  var product_data =  this_module.product_module.cit_data[product_id];


  var this_comp_id = $(data_for_doc.switch_elem).closest('.cit_comp')[0].id;
  var data = this_module.cit_data[this_comp_id];
  var rand_id =  this_module.cit_data[this_comp_id].rand_id;

  // var product_module = await get_cit_module(`/modules/products/product_module.js`, `load_css`);
  var product_id = $(data_for_doc.switch_elem).closest('.product_comp')[0].id;
  var product_data = this_module.product_module.cit_data[product_id];

  var full_product_name = this_module.product_module.generate_full_product_name(product_data);

  var kalk_box = $(data_for_doc.switch_elem).closest('.kalk_box');


  var kalk_type = `post_kalk`;

  // pošto je ovo jedan jedini kalk jer u post kalk nema više kalkova :)
  var post_kalk = data_for_doc.kalk;


  var status_module = await get_cit_module(`/modules/status/status_module.js`, `load_css`);

  // prvo nadjem parent product component pa onda pomoću njega nadjem status component
  var status_comp_id = $(data_for_doc.switch_elem).closest('.product_comp').find(`.status_comp`)[0].id;
  var status_data = status_module.cit_data[status_comp_id];


  var sirov_ids = []; 
  var db_sirovine = [];

  var parent_record = null;
  var status_offer_reserv = null;
  var reply_for = null;
  var elem_parent_object = null;

  var all_sirov_records = [];
  var arg_storno_data = null;

  var link = ``;
  var link_in_koment = ``;
  var komentar = ``;

  var status_tip = null;


  var time_now = Date.now();


  if ( !window.cit_user ) {
    popup_warn(`Niste se ulogirali !!!`);
    return;
  };


  var all_prodon_statusi = [];

  // -------------- START --------------------------------------- START LOOPA PO SVIM POST KALK PROCESIMA  
  // -------------- START --------------------------------------- START LOOPA PO SVIM POST KALK PROCESIMA  
  // -------------- START --------------------------------------- START LOOPA PO SVIM POST KALK PROCESIMA  

  $.each( post_kalk.procesi, function( p_ind, proces ) {



    function check_is_for_one_proces( this_proces ) {

      var is_ok = false;

      if ( !data_for_doc.proces )  {

        is_ok = true;

      } else {

        if ( this_proces.row_sifra == data_for_doc.proces.row_sifra ) is_ok = true;

      };

      return is_ok;  

    };


    // ------------------- PROVEJRA JEL ZA SVE PROCESE ILI SAMO ZA JEDAN NAKNADNI !!! -------------------
    // ------------------- PROVEJRA JEL ZA SVE PROCESE ILI SAMO ZA JEDAN NAKNADNI !!! -------------------
    // ------------------- PROVEJRA JEL ZA SVE PROCESE ILI SAMO ZA JEDAN NAKNADNI !!! -------------------

    if ( check_is_for_one_proces(proces) ) {


      var rn_extra = null;


      if ( proces.extra_data?.proces_radni_nalog ) {

        rn_extra = {
          time: Date.now(),
          _id: window.cit_user._id,
          user_number: window.cit_user.user_number,
          full_name: window.cit_user.full_name,
          email: window.cit_user.email
        };

      };


      var new_status_sifra = `status`+cit_rand();

      var link_to_post_kalk_proces = `#project/${data.proj_sifra || null}/item/${data.item_sifra || null}/variant/${data.variant || null}/kalk/${post_kalk.kalk_sifra || null}/proces/${proces.row_sifra}`;

      komentar = 
`RADNI PROCES BR. ${p_ind+1}: 
<br>
<a href="${link_to_post_kalk_proces}" 
    target="_blank"
    onClick="event.stopPropagation();"> 
    ${ full_product_name + "<br><br>" + proces.action.action_radna_stn.naziv }
</a>

`;

      link = `#production/${new_status_sifra}/proces/${proces.row_sifra}`;  

      status_tip = {

        "naziv" : "PROIZVODNI PROCES - RADNI NALOG",
        "cat" : {
            "sifra" : "prod",
            "naziv" : "PROIZVODNJA"
        },
        "dep" : {
            "sifra" : "PRO",
            "naziv" : "Proizvodnja/Work"
        },
        "sifra" : "IS16430378755748118",
        "is_work" : null,
        "active" : true,
        "next_statuses" : [ 
            {
                "naziv" : "PROIZVODNI PROCES - ZAPOČET",
                "cat" : {
                    "sifra" : "prod",
                    "naziv" : "PROIZVODNJA"
                },
                "dep" : {
                    "sifra" : "PRO",
                    "naziv" : "Proizvodnja/Work"
                },
                "sifra" : "IS16430378920956416",
                "is_work" : null,
                "active" : true,
                "next_statuses" : [],
                "grana_finish" : [],
                "def_deadline" : null,
                "status_num" : 9800,
                "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                "cat_naziv" : "PROIZVODNJA",
                "dep_naziv" : "Proizvodnja/Work",
                "active_status" : true,
                "work_done_for" : null,
                "work_done_for_naziv" : "",
                "work_finished" : null,
                "button_edit" : true,
                "next_status_num" : 20
            }, 
            {
                "naziv" : "PROBLEM OSOBLJA",
                "status_num" : 8300,
                "def_deadline" : null,
                "cat" : {
                    "sifra" : "prod",
                    "naziv" : "PROIZVODNJA"
                },
                "dep" : {
                    "sifra" : "PRO",
                    "naziv" : "Proizvodnja/Work"
                },
                "sifra" : "IS16418047377375376",
                "is_work" : null,
                "active" : true,
                "next_statuses" : [],
                "grana_finish" : [],
                "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                "cat_naziv" : "PROIZVODNJA",
                "dep_naziv" : "Proizvodnja/Work",
                "active_status" : true,
                "work_done_for" : null,
                "work_done_for_naziv" : "",
                "work_finished" : null,
                "button_edit" : true,
                "next_status_num" : 40
            }, 
            {
                "work_done_for" : "IS16418047846511135",
                "naziv" : "PROBLEM OSOBLJA JE RJEŠEN",
                "def_deadline" : null,
                "cat" : {
                    "sifra" : "prod",
                    "naziv" : "PROIZVODNJA"
                },
                "dep" : {
                    "sifra" : "PRO",
                    "naziv" : "Proizvodnja/Work"
                },
                "sifra" : "IS16418048176687437",
                "is_work" : null,
                "active" : true,
                "next_statuses" : [],
                "grana_finish" : [],
                "status_num" : 8500,
                "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                "cat_naziv" : "PROIZVODNJA",
                "dep_naziv" : "Proizvodnja/Work",
                "active_status" : true,
                "work_done_for_naziv" : "PROBLEM OSOBLJA SE RJEŠAVA",
                "work_finished" : null,
                "button_edit" : true,
                "next_status_num" : 60
            }, 
            {
                "status_num" : 8000,
                "naziv" : "PROBLEM STROJ",
                "def_deadline" : null,
                "cat" : {
                    "sifra" : "prod",
                    "naziv" : "PROIZVODNJA"
                },
                "dep" : {
                    "sifra" : "PRO",
                    "naziv" : "Proizvodnja/Work"
                },
                "sifra" : "IS1641804595136842",
                "is_work" : null,
                "active" : true,
                "next_statuses" : [],
                "grana_finish" : [],
                "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                "cat_naziv" : "PROIZVODNJA",
                "dep_naziv" : "Proizvodnja/Work",
                "active_status" : true,
                "work_done_for" : null,
                "work_done_for_naziv" : "",
                "work_finished" : null,
                "button_edit" : true,
                "next_status_num" : 80
            }, 
            {
                "naziv" : "SERVIS STROJA U TOKU",
                "def_deadline" : null,
                "is_work" : true,
                "cat" : {
                    "sifra" : "prod",
                    "naziv" : "PROIZVODNJA"
                },
                "dep" : {
                    "sifra" : "PRO",
                    "naziv" : "Proizvodnja/Work"
                },
                "sifra" : "IS16418046310852476",
                "active" : true,
                "next_statuses" : [],
                "grana_finish" : [],
                "status_num" : 8100,
                "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                "cat_naziv" : "PROIZVODNJA",
                "dep_naziv" : "Proizvodnja/Work",
                "active_status" : true,
                "work_done_for" : null,
                "work_done_for_naziv" : "",
                "work_finished" : null,
                "button_edit" : true,
                "next_status_num" : 100
            }, 
            {
                "naziv" : "SERVIS STROJA GOTOV",
                "work_done_for" : "IS16418046310852476",
                "is_work" : false,
                "def_deadline" : null,
                "cat" : {
                    "sifra" : "prod",
                    "naziv" : "PROIZVODNJA"
                },
                "dep" : {
                    "sifra" : "PRO",
                    "naziv" : "Proizvodnja/Work"
                },
                "sifra" : "IS1641804676056579",
                "active" : true,
                "next_statuses" : [],
                "grana_finish" : [],
                "status_num" : 8200,
                "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                "cat_naziv" : "PROIZVODNJA",
                "dep_naziv" : "Proizvodnja/Work",
                "active_status" : true,
                "work_done_for_naziv" : "SERVIS STROJA U TOKU",
                "work_finished" : null,
                "button_edit" : true,
                "next_status_num" : 120
            }, 
            {
                "naziv" : "PROBLEM OSOBLJA SE RJEŠAVA",
                "def_deadline" : null,
                "is_work" : true,
                "cat" : {
                    "sifra" : "prod",
                    "naziv" : "PROIZVODNJA"
                },
                "dep" : {
                    "sifra" : "PRO",
                    "naziv" : "Proizvodnja/Work"
                },
                "sifra" : "IS16418047846511135",
                "active" : true,
                "next_statuses" : [],
                "grana_finish" : [],
                "status_num" : 8400,
                "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                "cat_naziv" : "PROIZVODNJA",
                "dep_naziv" : "Proizvodnja/Work",
                "active_status" : true,
                "work_done_for" : null,
                "work_done_for_naziv" : "",
                "work_finished" : null,
                "button_edit" : true,
                "next_status_num" : 140
            }, 
            {
                "naziv" : "GREŠKA U NALOGU",
                "def_deadline" : null,
                "cat" : {
                    "sifra" : "prod",
                    "naziv" : "PROIZVODNJA"
                },
                "dep" : {
                    "sifra" : "PRO",
                    "naziv" : "Proizvodnja/Work"
                },
                "sifra" : "IS1641804940312613",
                "is_work" : null,
                "active" : true,
                "next_statuses" : [],
                "grana_finish" : [],
                "status_num" : 8600,
                "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                "cat_naziv" : "PROIZVODNJA",
                "dep_naziv" : "Proizvodnja/Work",
                "active_status" : true,
                "work_done_for" : null,
                "work_done_for_naziv" : "",
                "work_finished" : null,
                "button_edit" : true,
                "next_status_num" : 160
            }, 
            {
                "naziv" : "GREŠKA U NALOGU SE ISPRAVLJA",
                "def_deadline" : null,
                "cat" : {
                    "sifra" : "prod",
                    "naziv" : "PROIZVODNJA"
                },
                "is_work" : true,
                "dep" : {
                    "sifra" : "PRO",
                    "naziv" : "Proizvodnja/Work"
                },
                "sifra" : "IS16418050178509492",
                "active" : true,
                "next_statuses" : [],
                "grana_finish" : [],
                "status_num" : 8700,
                "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                "cat_naziv" : "PROIZVODNJA",
                "dep_naziv" : "Proizvodnja/Work",
                "active_status" : true,
                "work_done_for" : null,
                "work_done_for_naziv" : "",
                "work_finished" : null,
                "button_edit" : true,
                "next_status_num" : 180
            }, 
            {
                "naziv" : "GREŠKA U NALOGU ISPRAVLJENA",
                "work_done_for" : "IS16418050178509492",
                "def_deadline" : null,
                "cat" : {
                    "sifra" : "prod",
                    "naziv" : "PROIZVODNJA"
                },
                "dep" : {
                    "sifra" : "PRO",
                    "naziv" : "Proizvodnja/Work"
                },
                "sifra" : "IS16418050525633677",
                "is_work" : null,
                "active" : true,
                "next_statuses" : [],
                "grana_finish" : [],
                "status_num" : 8800,
                "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                "cat_naziv" : "PROIZVODNJA",
                "dep_naziv" : "Proizvodnja/Work",
                "active_status" : true,
                "work_done_for_naziv" : "GREŠKA U NALOGU SE ISPRAVLJA",
                "work_finished" : null,
                "button_edit" : true,
                "next_status_num" : 200
            }
        ],
        "grana_finish" : [ 
            {
                "naziv" : " PROIZVODNI PROCES - GOTOV",
                "cat" : {
                    "sifra" : "prod",
                    "naziv" : "PROIZVODNJA"
                },
                "dep" : {
                    "sifra" : "PRO",
                    "naziv" : "Proizvodnja/Work"
                },
                "sifra" : "IS1643038121299174",
                "is_work" : null,
                "active" : true,
                "next_statuses" : [],
                "grana_finish" : [],
                "def_deadline" : null,
                "status_num" : 9900,
                "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                "cat_naziv" : "PROIZVODNJA",
                "dep_naziv" : "Proizvodnja/Work",
                "active_status" : true,
                "work_done_for" : null,
                "work_done_for_naziv" : "",
                "work_finished" : null,
                "button_edit" : true,
                "next_status_num" : 20
            }, 
            {
                "sifra" : "ISerror",
                "active" : true,
                "next_status_num" : 50,
                "naziv" : "GREŠKA UPISA",
                "next_statuses" : [],
                "grana_finish" : [],
                "is_work" : null,
                "def_deadline" : null,
                "cat" : {
                    "sifra" : "preprod",
                    "naziv" : "PRED-PROIZVODNJA"
                },
                "next_statuses_string" : "",
                "grana_finish_string" : ""
            }, 
            {
                "naziv" : "ODUSTAJEMO OD OVE GRANE",
                "dep" : {
                    "sifra" : "GEN",
                    "naziv" : "Općenito"
                },
                "cat" : {
                    "sifra" : "preprod",
                    "naziv" : "PRED-PROIZVODNJA"
                },
                "sifra" : "IS16341239747891487",
                "is_work" : null,
                "active" : true,
                "next_statuses" : [],
                "grana_finish" : [],
                "def_deadline" : null,
                "status_num" : 1200,
                "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                "cat_naziv" : "PRED-PROIZVODNJA",
                "dep_naziv" : "Općenito",
                "active_status" : true,
                "button_edit" : true,
                "next_status_num" : 80,
                "work_finished" : null,
                "work_done_for" : null,
                "work_done_for_naziv" : ""
            }
        ],
        "def_deadline" : null,
        "status_num" : 9700,
        "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
        "cat_naziv" : "PROIZVODNJA",
        "dep_naziv" : "Proizvodnja/Work",
        "active_status" : true,
        "work_done_for" : null,
        "work_done_for_naziv" : "",
        "work_finished" : null,
        "button_edit" : true,
        "next_status_num" : null
    };


      var status_docs = [
        {
          sifra: `doc`+cit_rand(),
          time: Date.now(),
          link: `<a href="/docs/${arg_pdf_name}" target="_blank" onClick="event.stopPropagation();" >${ arg_pdf_name }</a>`,
        }
      ];

      var prodon_status = {

        "full_product_name" : full_product_name,

        "product_id" : data.product_id,
        "link" : link,
        "proj_sifra" : data.proj_sifra,
        "item_sifra" : data.item_sifra,
        "variant" : data.variant,

        "sifra" : new_status_sifra,
        "status" : new_status_sifra,
        "elem_parent" : null, // ako curr status već ima parent onda uzmi njega
        "elem_parent_object" : null,
        "time" : time_now,
        "work_times" : null,
        "docs" : status_docs,
        "est_deadline_hours" : null,

        "est_deadline_date" : (product_data.rok_proiz || data_for_doc.rok || null),

        "start" : time_now,
        "end" : null,
        "old_status_tip" : null,


        "status_tip" : status_tip,


        kalk: data._id || null,

        proces_id: proces.row_sifra,


        "dep_from" : {
            "sifra" : window.cit_user.dep.sifra,
            "naziv" : window.cit_user.dep.naziv,
        },
        "from" : {
            "_id" : window.cit_user._id,
            "name" : window.cit_user.name,
            "user_number" : window.cit_user.user_number,
            "full_name" : window.cit_user.full_name,
            "email" : window.cit_user.email,
        },

        "dep_to" : {
          sifra: "PRO", 
          naziv: "Proizvodnja/Work",
        },
        "to" : {
            "_id" : "61f8fc181039d39764a9eb54",
            "name" : "radnici",
            "user_number" : 301,
            "full_name" : "Radnici Proizvodnja",
            "email" : "production@velprom.hr",
        },
        "old_komentar" : null,
        "komentar" : komentar || null,
        "seen" : null,
        "reply_for" : null,
        "responded" : null,
        "done_for_status" : null,
        "work_finished" : null,
        "sirov" : null,
        "order_sirov_count" : null,
        "record_statuses" : [],

        "rok_isporuke" : product_data.rok_isporuke || null,
        "potrebno_dana_proiz" : product_data.potrebno_dana_proiz || null,
        "potrebno_dana_isporuka" : product_data.rok_isporuke || null,
        "rok_proiz" : product_data.rok_proiz || null,
        "rok_za_def" : product_data.rok_za_def || null,
        "branch_done" :  false, 


        rn_extra: rn_extra,


      };

      // napravi flat objekte i arraye
      $.each( prodon_status, function( key, value ) {
        // ako je objekt ili array onda ga flataj
        if ( $.isPlainObject(value) || $.isArray(value) ) {
          prodon_status[key+'_flat'] = JSON.stringify(value);
        };
      });    

      all_prodon_statusi.push(prodon_status);


    }; 
    // ------------------- PROVEJRA JEL ZA SVE PROCESE ILI SAMO ZA JEDAN NAKNADNI !!! -------------------
    // ------------------- PROVEJRA JEL ZA SVE PROCESE ILI SAMO ZA JEDAN NAKNADNI !!! -------------------
    // ------------------- PROVEJRA JEL ZA SVE PROCESE ILI SAMO ZA JEDAN NAKNADNI !!! -------------------

  }); // loop po svim procesima unutar post kalkulacije


  // ------------------------------------------------------ KRAJ LOOPA PO SVIM POST KALK PROCESIMA
  // ------------------------------------------------------ KRAJ LOOPA PO SVIM POST KALK PROCESIMA
  // ------------------------------------------------------ KRAJ LOOPA PO SVIM POST KALK PROCESIMA



  function save_all_prodon_statuses(all_prodon_statusi, p_index) {

      $.ajax({
        headers: {
          'X-Auth-Token': window.cit_user.token
        },
        type: "POST",
        cache: false,
        url: `/save_status`,
        data: JSON.stringify( all_prodon_statusi[p_index] ),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      })
      .done( async function (result) {

        console.log(result);

        if ( result.success == true ) {

          cit_toast(`STATUTS ZA RADNI PROCES JE SPREMLJEN!`);
          cit_toast(`Poslan status prema ${ all_prodon_statusi[p_index].to.full_name}`, `background: #232324;`);
          cit_socket.emit('new_status', all_prodon_statusi[p_index] );
          animate_badge( $(`#cit_task_from_me`), 1 );

          all_prodon_statusi[p_index]._id = result.status._id;

          product_data.statuses = upsert_item( product_data.statuses, all_prodon_statusi[p_index], `sifra` );
          status_data.statuses = upsert_item( status_data.statuses, all_prodon_statusi[p_index], `sifra` );


          // --------------------------------

          // na primjer ako array ima 5 itema od 0-4

          if ( p_index < all_prodon_statusi.length - 1 ) {
            // onda p index smije biti 3 ili manji
            save_all_prodon_statuses(all_prodon_statusi, p_index+1);

          } else {

            var doc_type_human_name = " RADNI_NALOG ";

            var komentar =  data_for_doc.doc_sifra + doc_type_human_name + " (Automatski generiran dokument)";

            var new_kalk_spec = {

              "docs": [
                {
                  "sifra": "doc"+cit_rand(),
                  "time": Date.now(),
                  "link": `<a href="/docs/${arg_pdf_name}" target="_blank" onClick="event.stopPropagation();" >${ arg_pdf_name }</a>`
                }
              ],
              "sifra": "kalkspecs"+cit_rand(),
              "komentar": komentar,
              "time": Date.now(),
            };

            product_data.kalk_specs = upsert_item( product_data.kalk_specs, new_kalk_spec, `sifra`);

            this_module.product_module.make_kalk_specs_list(product_data);
            status_module.make_status_list(status_data);
            status_module.remove_header_arrows_and_blink(status_data);


            setTimeout( function() {  $(`#` + data.id + ` .save_offer_kalk_btn` ).trigger(`click`); }, 200);
            setTimeout( function() {  $(`#` + data.id + ` .save_pro_kalk_btn` ).trigger(`click`); }, 300);
            setTimeout( function() {  $(`#` + data.id + ` .save_post_kalk_btn` ).trigger(`click`); }, 400);
            setTimeout( function() {  $(`#`+ product_data.id + ` .save_product_btn` ).trigger(`click`); }, 500);



          };


        } else {

          // ako result NIJE SUCCESS = true

          if ( result.msg ) console.error(result.msg);
          if ( !result.msg ) console.error(`Greška prilikom spremanja STATUSA!`);

        };

      })
      .fail(function (error) {
        console.error(`Došlo je do greške na serveru prilikom spremanja STATUSA!`);
        console.log(error);
      })
      .always(function() {
        toggle_global_progress_bar(false);
      });



  };


  if ( all_prodon_statusi.length > 0 ) save_all_prodon_statuses(all_prodon_statusi, 0);


};
this_module.make_radni_nalog_statuses = make_radni_nalog_statuses;



