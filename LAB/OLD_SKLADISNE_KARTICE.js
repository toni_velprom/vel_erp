          
    $('#new_skl_card_btn').off("click");
    $('#new_skl_card_btn').on("click", async function() {
      
      var this_button = this;
      
      if ( !window.current_location ) {
        popup_warn(`Niste označili niti jednu lokaciju!!!`);
        return;
      };

      
      var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      var popup_text =
`Ova lokacija već ima generirane skladišne kartice !!!<br>
Želite li prepisati stare kartice?<br>
<br>
Ako to napravite morate fizički promijeniti sve kartice na svakoj paleti ili pakiranju na ovoj lokaciji !!!
`;
      
      
      if ( window.current_location.palet_sifre?.length > 0 ) {
        
        function new_cards_yes() {
          this_module.create_or_print_skl_card(this_button, `new_cards`);
        };

        function new_cards_no() {
          show_popup_modal(false, popup_text, null );
        };

        var pop_mod = await get_cit_module('/preload_modules/site_popup_modal/site_popup_modal.js', null);
        var show_popup_modal = pop_mod.show_popup_modal;
        show_popup_modal(`warning`, popup_text, null, 'yes/no', new_cards_yes, new_cards_no, null);
        
        
      }
      else {
        
        this_module.create_or_print_skl_card(this_button, `new_cards`);
        
      };
      
 
      
    }); 
    
    $('#print_skl_card_btn').off("click");
    $('#print_skl_card_btn').on("click", function() {
      
      var this_button = this;
      
      if ( !window.current_location ) {
        popup_warn(`Niste označili niti jednu lokaciju!!!`);
        return;
      };

      var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
      var data = this_module.cit_data[this_comp_id];
      var rand_id =  this_module.cit_data[this_comp_id].rand_id;
      
      if ( !window.current_location.palet_sifre || window.current_location.palet_sifre?.length == 0 ) {
        popup_warn(`Trenutno nije kreirana niti jadna skladišna kartica!<br>Kliknite na gumb KREIRAJ SKL. KART. !!! `);
        return;
      }
      else {
        this_module.create_or_print_skl_card(this_button, `print_cards`); 
      };
      
      
    }); 
  


  async function create_or_print_skl_card( this_button, arg_action_type ) {


    var this_comp_id = $(this_button).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;


    
    if ( !window.current_location.sklad && !data.sklad ) {
      popup_warn(`Odaberite skladište!`);
      return;
    };

    if ( !window.current_location.sector && !data.sector ) {
      popup_warn(`Odaberite sektor!`);
      return;
    };


    if ( !window.current_location.level &&  !data.level ) {
      popup_warn(`Odaberite level!`);
      return;
    };
    
    // ako je alat tj klasa ----> KS5
    if ( !data.klasa_sirovine?.sifra == "KS5" && ( !window.current_location.alat && !data.alat_shelf ) ) {
      popup_warn(`Za alat obavezno upisati policu!`);
      return;
    };
    

    var preview_mod = await get_cit_module('/modules/previews/cit_previews.js', `load_css`);
    
    if ( arg_action_type == `new_cards` ) {
      
      var card_count = 0;
      
      // ali ako ima palet count onda uzmi taj broj
      if ( window.current_location?.palet_count ) card_count = window.current_location.palet_count;
      
      // ako ima pakete na paleti onda OVERRIDE pa uzmi taj broj
      if ( window.current_location?.box_count_on_palet ) {
        card_count = window.current_location.box_count_on_palet;
        // ako ima još i broj paleta onda pomnoži broj pakiranja na paleti s brojem paleta
        if ( window.current_location.palet_count ) card_count = window.current_location.box_count_on_palet * window.current_location.palet_count;
      };
      
      
      // ako ima box count ODVOJENO onda OPET OVERRIDE NA TAJ BROJ
      if ( window.current_location?.box_count ) card_count = window.current_location.box_count;
      

      var get_palet_sifre = await get_new_palet_sifre(card_count);
      
      
      if ( get_palet_sifre.success == true ) {
        
        data.palet_sifre = get_palet_sifre.sifre;
        // stavi nove sifre u current location i napravi UPSERT U LOCATIONS !!!
        window.current_location.palet_sifre = data.palet_sifre;
        
        data.locations = upsert_item(data.locations, window.current_location, `sifra`);
        
        setTimeout( function() {
          // prikaži nove šifre koje sam upravo dobio sa servera
          this_module.make_locations_list(data);
          $(`#save_sirov_btn`).trigger(`click`);
        }, 700 );
        
      };
      
      
    } 
    else if ( arg_action_type == `print_cards` ) {
      
      data.palet_sifre = window.current_location.palet_sifre;

      // ako se radi samo o printanju sklad akrtice
      if ( !data.palet_sifre || data.palet_sifre?.length == 0 ) {
        popup_warn(
`
Nema niti jedne šifre skladišne kartice !!!<br>
Potrebno je ili SPREMITI NOVU lokaciju i zatim kreirati nove skladišne kartice ili izabrati EDITIRANJE POSTOJEĆE lokacije !!!
`);
        return;
      };
    
    };
    

    var db_partner = null;
    var dobavljac_id = data.dobavljac?._id ? data.dobavljac._id : null;

    if ( dobavljac_id ) {
      db_partner = await this_module.get_dobavljac(dobavljac_id);
    };

    if ( !db_partner ) {
      popup_error(`Greška prilikom pretrage dobavljača u bazi !!!!`);
      return;
    };

    var data_for_doc = {
      
      sifra: "dfd"+cit_rand(),
      
      time: Date.now(),

      for_module: `sirov`,

      proj_sifra : null,
      item_sifra : null,
      variant : null,
      status: null,
      
      /*
      ---------------------------------------- OVO SU SVI PODACI ZA LOCATION OBJ ----------------------------------------
      sifra,
      palet_sifre,
          
      ms_record,
      ms_time,

      primka_record,
      primka_pro_record,

      count,

      neto_masa_paketa_kg,
      bruto_masa_paketa_kg,

      neto_masa_palete_kg,
      bruto_masa_palete_kg,

      choose_location_from,
      choose_primka,
      choose_palet_sifra,

      palet_count,

      box_count_on_palet,
      red_count_on_palet,
      box_count_in_red,

      palet_unit_count,
      paleta_x,
      paleta_y,
      paleta_z,

      box_count,
      box_unit_count,
      box_x,
      box_y,
      box_z,

      sklad,
      sector,
      level,
      shelf,
      
      ---------------------------------------- OVO SU SVI PODACI ZA LOCATION OBJ ----------------------------------------
      */
      
      primka_record: window.current_location?.primka_record || null,
      primka_pro_record: window.current_location?.primka_pro_record || null,
      
      
      jedinica: data.osnovna_jedinica?.jedinica || "",
      
      palet_sifre: data.palet_sifre,
      
      palet_unit_count: window.current_location.palet_unit_count || null,
      box_unit_count: window.current_location.box_unit_count || null,
      
      
      sklad: window.current_location.sklad || data.sklad,
      sector: window.current_location.sector || data.sector,
      level: window.current_location.level || data.level,
      alat_shelf: window.current_location.shelf || data.alat_shelf,

      sirov_id: data._id || null,
      full_record_name: (data.sirovina_sifra + "--" + window.concat_naziv_sirovine(data) + "--" + data.dobavljac?.naziv ),

      
      doc_valuta: ( data.doc_valuta?.valuta || "HRK" ), // default je HRK
      doc_valuta_manual: (data.doc_valuta_manual || null ), // manual tečaj
      referent: window.cit_user.full_name,
      place: `Velika Gorica`,

      doc_type: `sklad_kartica`,
      doc_num: "----",

      oib: (db_partner.oib || db_partner.vat_num || ""),
      partner_name: db_partner.naziv,
      partner_adresa: null,
      partner_id: db_partner._id,
      

      /*
      partner_place: `67122 ALTRIP`,
      partner_country: `Germany`,
      */

      rok: Date.now(),
      dobav_ponuda_sifra: null,

      items: null,

    };

    // return;

    preview_mod.generate_cit_doc( data_for_doc, data.doc_lang?.sifra || "hr" );

  };
  this_module.create_or_print_skl_card = create_or_print_skl_card;
  