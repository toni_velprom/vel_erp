
function is_start_proces(arg_proc) {

  var is_start = false;
  var real_sirov = 0;

  $.each(arg_proc.ulazi, function(au_ind, arg_ulaz) {

    if ( 
      // ako ima tip sirovine u ULAZU NE SMJE BITI POMOĆNI TIP ----> i mora imati _id i NE SMJE IMATI KALK ELEMENT
      ( !arg_ulaz.kalk_element && arg_ulaz._id && arg_ulaz.tip_sirovine && arg_ulaz.tip_sirovine.sifra !== `TSIR11` )
      ||
      ( !arg_ulaz.kalk_element && arg_ulaz.kalk_plate_id ) // također može biti montažna ploča

    ) {

      real_sirov += 1;

    };

  });


  if ( real_sirov == 1 ) {
    is_start = true;
  };

  return is_start;

};


function find_prev_proces_index( curr_kalk, izlaz_sifra_od_ulaza, arg_curr_proc_index ) {


  var prev_proces_index = null;

  var prev;

  for( prev = arg_curr_proc_index - 1 ; prev >= 0; prev-- ) {

    var prev_izlaz = find_item( curr_kalk.procesi[prev].izlazi, izlaz_sifra_od_ulaza, `sifra` );

    if ( prev_izlaz ) {

      prev_proces_index = prev;

    };


  }; // loop unazad po svim prijašnjim procesima


  return prev_proces_index;
  

};


function kalk_extra_reverse( curr_kalk, arg_proces_index, arg_skart_koef ) {

  
  var curr_proces = curr_kalk.procesi[arg_proces_index];
  var curr_skart_koef = curr_proces.proces_skart ? (1 - curr_proces.proces_skart) : 1; // prvo pogledaj skart za current proces

  // ako postoji argument skart koef onda ga pomnoži sa trenutnim koeficijentom !!!!
  // NA PRIMJER ako je skart u argumentu 0.97 i ako je trenutni skart 0.95 ----> onda trenutni skart postaje 0.95 * 0.97 = 0.9215
  
  if ( arg_skart_koef ) curr_skart_koef = curr_skart_koef * arg_skart_koef;
  

  var izlaz;
  // ako nije posve zadnji proces onda dodaj u izlaze sum (ne u extra polje)
  if ( arg_proces_index < curr_kalk.procesi.length -1 ) {

    $.each( curr_proces.izlazi, function(iz_ind, izlaz ) {

      if ( !izlaz.skart_koef ) {
        curr_kalk.procesi[arg_proces_index].izlazi[iz_ind].original_kom = izlaz.kalk_sum_sirovina;
        
        var potrebno_kom = Math.ceil( izlaz.kalk_sum_sirovina/arg_skart_koef );
        curr_kalk.procesi[arg_proces_index].izlazi[iz_ind].kalk_sum_sirovina = potrebno_kom;
        curr_kalk.procesi[arg_proces_index].izlazi[iz_ind].skart_koef = arg_skart_koef; // dodajem skart koef da znam da sam to izračunao
      };

    });

  };

  

  // DODAJ EXTRA KOMADA SAMO NA POČETNE PROCESE TJ ULAZE U POĆETNIM PROCESIMA !!!!!!!!!
  // DODAJ EXTRA KOMADA SAMO NA POČETNE PROCESE TJ ULAZE U POĆETNIM PROCESIMA !!!!!!!!!
  
  
  
  if ( is_start_proces(curr_proces) ) { 

    $.each( curr_proces.ulazi, function(ulaz_ind, ulaz ) {
      
      if ( !ulaz.skart_koef ) {
        var potrebno_kom = ulaz.kalk_sum_sirovina / curr_skart_koef;
        var extra_kom = potrebno_kom - ulaz.kalk_sum_sirovina;
        curr_kalk.procesi[arg_proces_index].ulazi[ulaz_ind].kalk_extra_kom = extra_kom; // DODAJ U EXTRA POLJE ( ne u sum sirovina)
        curr_kalk.procesi[arg_proces_index].ulazi[ulaz_ind].skart_koef = curr_skart_koef;
      };
      
    });
    
    // -------------------------------- I TO JE KRAJ OVE GRANE JER NEMA VIŠE PRIJE OVOG PROCESA  !!!


  } else { 

    
    // ako nije start proces onda dodaj količinu u sum sirovina (NE U EXTRA POLJE)
    $.each( curr_proces.ulazi, function(u_ind, ulaz) {

      if ( !ulaz.skart_koef ) {
        curr_kalk.procesi[arg_proces_index].ulazi[ulaz_ind].original_kom = ulaz.kalk_sum_sirovina;
        var potrebno_kom = ulaz.kalk_sum_sirovina / curr_skart_koef;
        curr_kalk.procesi[arg_proces_index].ulazi[ulaz_ind].kalk_sum_sirovina = potrebno_kom;
        curr_kalk.procesi[arg_proces_index].ulazi[ulaz_ind].skart_koef = curr_skart_koef;
      };
      
    });


    // ako nije start proces onda traži sve procese od svih ulaza
    // pogledaj jel ovaj ulaz negdje IZLAZ
    
    $.each( curr_proces.ulazi, function(u_ind, ulaz) {
      
      var prev_proc_index = find_prev_proces_index(curr_kalk, ulaz.izlaz_sifra, arg_proces_index);
      kalk_extra_reverse( curr_kalk, prev_proc_index, curr_skart_koef );
      
    });




  }; // kraj else ako ovo nije start proces 


  
/*
  

  if ( x_count == 62 ) {
    console.log(`x_count == 62`);
  };

  // NAPRAVI UPDATE NA PRVOM PROCESU I TO ĆE SE PROPAGIRATI NA SVE OSTALE PROCESE DO KRAJA
  this_module.update_kalk( curr_kalk, product_data, 0, changed_ulaz=true, null, null );

  var last_izlaz_kom = curr_kalk.procesi[ curr_kalk.procesi.length - 1 ].izlazi[0].kalk_sum_sirovina;

  if ( last_izlaz_kom >= naklada ) {
    is_satis = true;
    break;
  };

*/




}; // kraj  kalk_extra_reverse


