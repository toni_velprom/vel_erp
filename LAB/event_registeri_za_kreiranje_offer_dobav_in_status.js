var html = 
    
`
<div class="row" id="order_sirovina_box" style="display: ${ window.cit_user?.dep?.sifra  == "NAB" ? "flex" : "none" };">

  <div class="col-md-1 col-sm-12">
    ${ cit_comp(rand_id+`_ask_kolicina_1`, valid.ask_kolicina_1, "") }
  </div>

  <div class="col-md-1 col-sm-12">
    ${ cit_comp(rand_id+`_ask_kolicina_2`, valid.ask_kolicina_2, "") }
  </div>

  <div class="col-md-1 col-sm-12">
    ${ cit_comp(rand_id+`_ask_kolicina_3`, valid.ask_kolicina_3, "") }
  </div>

  <div class="col-md-1 col-sm-12">
    ${ cit_comp(rand_id+`_lang_order_dobav`, valid.lang_order_dobav, null,  "") }
  </div>

  <div class="col-md-1 col-sm-12">
    ${ cit_comp(rand_id+`_doc_valuta`, valid.doc_valuta, null,  "") }
  </div>

  <div class="col-md-1 col-sm-12">
    ${ cit_comp(rand_id+`_doc_valuta_manual`, valid.doc_valuta_manual, "") }
  </div>

  <div class="col-md-4 col-sm-12">
    ${ cit_comp(rand_id+`_adresa_order_dobav`, valid.adresa_order_dobav, null,  "" ) }
  </div>

  <div class="col-md-1 col-sm-12">
    <button class="blue_btn btn_small_text" id="status_sirov_ponuda" style="margin: 20px auto 0 0; box-shadow: none;">
      TRAŽI PONUDU
    </button>
  </div>  

  <div class="col-md-1 col-sm-12">
    <button class="blue_btn btn_small_text" id="status_sirov_order" style="margin: 20px 0 0 auto; box-shadow: none;">
      NARUČI
    </button>
  </div>

</div>


`;



$('#'+rand_id+'_ask_kolicina_1').off(`blur.status_ask_kolicina_1`);
$('#'+rand_id+'_ask_kolicina_1').on(`blur.status_ask_kolicina_1`, function() {

  var this_comp_id = $(this).closest('.cit_comp')[0].id;
  var data = this_module.cit_data[this_comp_id];
  var rand_id =  this_module.cit_data[this_comp_id].rand_id;

  var new_value = set_input_data(
    this, // input elem
    data.current_status, // data parent
    null, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
    "dont_update" // određujem ovako jel treba napraviti update dataseta ili samo vratiti value
  );

  if ( !data.current_status ) data.current_status = {};
  data.current_status.ask_kolicina_1 = new_value;

  // console.log( data.current_status );

});


$('#'+rand_id+'_ask_kolicina_2').off(`blur.status_ask_kolicina_2`);
$('#'+rand_id+'_ask_kolicina_2').on(`blur.status_ask_kolicina_2`, function() {

  var this_comp_id = $(this).closest('.cit_comp')[0].id;
  var data = this_module.cit_data[this_comp_id];
  var rand_id =  this_module.cit_data[this_comp_id].rand_id;

  var new_value = set_input_data(
    this, // input elem
    data.current_status, // data parent
    null, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
    "dont_update" // određujem ovako jel treba napraviti update dataseta ili samo vratiti value
  );

  if ( !data.current_status ) data.current_status = {};
  data.current_status.ask_kolicina_2 = new_value;

  // console.log( data.current_status );

});    


$('#'+rand_id+'_ask_kolicina_3').off(`blur.status_ask_kolicina_3`);
$('#'+rand_id+'_ask_kolicina_3').on(`blur.status_ask_kolicina_3`, function() {

  var this_comp_id = $(this).closest('.cit_comp')[0].id;
  var data = this_module.cit_data[this_comp_id];
  var rand_id =  this_module.cit_data[this_comp_id].rand_id;

  var new_value = set_input_data(
    this, // input elem
    data.current_status, // data parent
    null, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
    "dont_update" // određujem ovako jel treba napraviti update dataseta ili samo vratiti value
  );

  if ( !data.current_status ) data.current_status = {};
  data.current_status.ask_kolicina_3 = new_value;

  // console.log( data.current_status );

});    


$('#'+rand_id+'_doc_valuta_manual').off(`blur._doc_valuta_manual`);
$('#'+rand_id+'_doc_valuta_manual').on(`blur._doc_valuta_manual`, function() {

  var this_comp_id = $(this).closest('.cit_comp')[0].id;
  var data = this_module.cit_data[this_comp_id];
  var rand_id =  this_module.cit_data[this_comp_id].rand_id;

  var new_value = set_input_data(
    this, // input elem
    data.current_status, // data parent
    null, // custom rand id ( ako nije standardni kojeg zapišem u cit_data objekt ) 
    "dont_update" // određujem ovako jel treba napraviti update dataseta ili samo vratiti value
  );

  if ( !data.current_status ) data.current_status = {};
  data.current_status.doc_valuta_manual = new_value;

  // console.log( data.current_status );

}); 


$(`#`+rand_id+`_lang_order_dobav`).data('cit_props', {
  desc: 'UNUTAR STATUS MODULA -----> odabir jezika za generiranje dokumenta order dobavljacu ',
  local: true,
  show_cols: ['naziv'],
  return: {},
  show_on_click: true,
  list: 'lang',
  cit_run: function(state) {

    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    if ( !data.current_status )  data.current_status = {};

    if ( state == null ) {
      $('#'+current_input_id).val(``);
      data.current_status.lang_order_dobav = null;
    } else {
      $('#'+current_input_id).val(state.naziv);
      data.current_status.lang_order_dobav = state;
    };

  },
});  


$(`#`+rand_id+`_doc_valuta`).data('cit_props', {
  desc: 'UNUTAR STATUS MODULA - odabir valute u kojoj će e generirati dokument sa cijenama u toj valuti :)',
  local: true,
  show_cols: ['naziv', 'valuta'],
  return: {},
  show_on_click: true,
  list: 'valute',
  cit_run: function(state) {

    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    if ( !data.current_status )  data.current_status = {};

    if ( state == null ) {
      $('#'+current_input_id).val(``);
      data.current_status.doc_valuta = null;
    } else {
      $('#'+current_input_id).val(state.valuta);
      data.current_status.doc_valuta = state;
    };

  },
});


$(`#`+rand_id+`_adresa_order_dobav`).data('cit_props', {
  desc: 'UNUTAR STATUS MODULA ----->  odabir jezika za generiranje dokumenta order dobavljacu',
  local: true,
  show_cols: ['full_adresa'],
  return: {},
  show_on_click: true,
  list:  ( $(`#svi_kontakti_dobavljaca_box`).data(`cit_result`) || [] ),
  cit_run: function(state) {

    var this_comp_id = $('#'+current_input_id).closest('.cit_comp')[0].id;
    var data = this_module.cit_data[this_comp_id];
    var rand_id =  this_module.cit_data[this_comp_id].rand_id;

    if ( !data.current_status )  data.current_status = {};

    if ( state == null ) {
      $('#'+current_input_id).val(``);
      data.current_status.adresa_order_dobav = null;
    } else {
      $('#'+current_input_id).val(state.full_adresa);
      data.current_status.adresa_order_dobav = state;
    };

  },
});  

