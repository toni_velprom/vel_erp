function change_sifre( product_data ) {
    
    $.each( product_data.elements, function(e_ind, elem) {

      var curr_elem_sifra = elem.sifra;
      var new_elem_sifra = `sel`+cit_rand();

      $.each( product_data.elements, function(e_ind_2, elem_2) {
        if ( elem_2.sifra == curr_elem_sifra ) product_data.elements[e_ind_2].sifra = new_elem_sifra;
        if ( elem_2.elem_parent == curr_elem_sifra ) product_data.elements[e_ind_2].elem_parent = new_elem_sifra;
      });

      if ( product_data.kalk ) {

        // obriši ovaj kalk id tako da spremi novi kalk koja je kopija
        delete product_data.kalk._id;

        if ( product_data.kalk.offer_kalk?.length > 0 ) {

          $.each( product_data.kalk.offer_kalk, function(offer_ind, off_kalk) {

            product_data.kalk.offer_kalk[offer_ind].kalk_sifra = `kalk`+cit_rand();

            if ( off_kalk.procesi?.length > 0 )  {
              
              $.each( off_kalk.procesi, function(proces_ind, proc) {
                
                var new_row_sifra = "procesrow" + cit_rand();
                product_data.kalk.offer_kalk[offer_ind].procesi[proces_ind].row_sifra = new_row_sifra;
                
                if ( proc.izlazi?.length > 0 ) {

                  $.each( proc.izlazi, function(iz_ind, izlaz) {


                    var curr_izlaz_sifra = izlaz.sifra;
                    var new_izlaz_sifra  = `izlaz` + cit_rand();
                    
                    product_data.kalk.offer_kalk[offer_ind].procesi[proces_ind].izlazi[iz_ind].sifra = new_izlaz_sifra;


                    if ( proc.ulazi?.length > 0 ) {
                      $.each( proc.ulazi, function(ulaz_ind, ulaz) {
                        if ( ulaz.izlaz_sifra == curr_izlaz_sifra ) {
                          product_data.kalk.offer_kalk[offer_ind].procesi[proces_ind].ulazi[ulaz_ind].izlaz_sifra = new_izlaz_sifra;
                        }
                      });
                    };

                    // samo ako nije procesni element koji nema kalk propery u sebi
                    if ( !izlaz.kalk_element?.elem_tip.kalk ) {

                      if ( izlaz.kalk_element?.sifra == curr_elem_sifra ) 
                        product_data.kalk.offer_kalk[offer_ind].procesi[proces_ind].izlazi[iz_ind].kalk_element.sifra = new_elem_sifra;
                      // samo ako nije procesni element koji nema kalk propery u sebi
                      if ( izlaz.kalk_element?.elem_parent == curr_elem_sifra ) 
                        product_data.kalk.offer_kalk[offer_ind].procesi[proces_ind].izlazi[iz_ind].kalk_element.elem_parent = new_elem_sifra;

                    };
                    
                  }); // loop po izlazima

                }; // kraj da li postoje ulazi


                if ( proc.ulazi?.length > 0 )  {

                  $.each( proc.ulazi, function(ulaz_ind, ulaz) {

                    product_data.kalk.offer_kalk[offer_ind].procesi[proces_ind].ulazi[ulaz_ind].sifra = `ulaz` + cit_rand();

                    
                    // samo ako nije procesni element koji nema kalk propery u sebi
                    if ( !ulaz.kalk_element?.elem_tip.kalk ) {

                      if ( ulaz.kalk_element?.sifra == curr_elem_sifra ) 
                        product_data.kalk.offer_kalk[offer_ind].procesi[proces_ind].ulazi[ulaz_ind].kalk_element.sifra = new_elem_sifra;
                      if ( ulaz.kalk_element?.elem_parent == curr_elem_sifra ) 
                        product_data.kalk.offer_kalk[offer_ind].procesi[proces_ind].ulazi[ulaz_ind].kalk_element.elem_parent = new_elem_sifra;

                    };
                    
                  });
                  
                };

                proc.action.action_sifra = `action`+cit_rand();


              })// kraj looop procesi

            }; // jel postoje procesi

          }); // loop po  kalk offer

        }; //  jel postoji offer kalk

        if ( product_data.kalk.pro_kalk?.length > 0 ) {

          $.each( product_data.kalk.pro_kalk, function(pro_ind, production_kalk) {

            product_data.kalk.pro_kalk[pro_ind].kalk_sifra = `kalk`+cit_rand();

            if ( production_kalk.procesi?.length > 0 )  {

              $.each( production_kalk.procesi, function(proces_ind, proc) {

                if ( proc.izlazi?.length > 0 ) {

                  $.each( proc.izlazi, function(iz_ind, izlaz) {


                    var curr_izlaz_sifra = izlaz.sifra;
                    var new_izlaz_sifra  = `izlaz` + cit_rand();
                    product_data.kalk.pro_kalk[pro_ind].procesi[proces_ind].izlazi[iz_ind].sifra = new_izlaz_sifra;


                    if ( proc.ulazi?.length > 0  ) {
                      $.each( proc.ulazi, function(ulaz_ind, ulaz) {
                        if ( ulaz.izlaz_sifra == curr_izlaz_sifra ) {
                          product_data.kalk.pro_kalk[pro_ind].procesi[proces_ind].ulazi[ulaz_ind].izlaz_sifra = new_izlaz_sifra;
                        }
                      });
                    };

                    if ( !izlaz.kalk_element?.elem_tip.kalk ) {
                      if ( izlaz.kalk_element?.sifra == curr_elem_sifra ) 
                        product_data.kalk.pro_kalk[pro_ind].procesi[proces_ind].izlazi[iz_ind].kalk_element.sifra = new_elem_sifra;
                      if ( izlaz.kalk_element?.elem_parent == curr_elem_sifra ) 
                        product_data.kalk.pro_kalk[pro_ind].procesi[proces_ind].izlazi[iz_ind].kalk_element.elem_parent = new_elem_sifra;
                    };
                    
                  }); // loop po izlazima

                }; // kraj da li postoje ulazi


                if ( proc.ulazi?.length > 0 )  {

                  $.each( proc.ulazi, function(ulaz_ind, ulaz) {

                    product_data.kalk.pro_kalk[pro_ind].procesi[proces_ind].ulazi[ulaz_ind].sifra = `ulaz` + cit_rand();

                    if ( !ulaz.kalk_element?.elem_tip.kalk ) {
                      
                      if ( ulaz.kalk_element?.sifra == curr_elem_sifra ) 
                        product_data.kalk.pro_kalk[pro_ind].procesi[proces_ind].ulazi[ulaz_ind].kalk_element.sifra = new_elem_sifra;
                      if ( ulaz.kalk_element?.elem_parent == curr_elem_sifra ) 
                        product_data.kalk.pro_kalk[pro_ind].procesi[proces_ind].ulazi[ulaz_ind].kalk_element.elem_parent = new_elem_sifra;
                      
                    };
                  });
                };

                proc.action.action_sifra = `action`+cit_rand();


              })// kraj looop procesi

            }; // jel postoje procesi

          }); // loop po  kalk production

        }; //  jel postoji production kalk

        if ( product_data.kalk.post_kalk?.length > 0 ) {

          $.each( product_data.kalk.post_kalk, function(post_ind, kalk_post) {

            product_data.kalk.post_kalk[post_ind].kalk_sifra = `kalk`+cit_rand();

            if ( kalk_post.procesi?.length > 0 )  {

              $.each( kalk_post.procesi, function(proces_ind, proc) {

                if ( proc.izlazi?.length > 0 ) {

                  $.each( proc.izlazi, function(iz_ind, izlaz) {


                    var curr_izlaz_sifra = izlaz.sifra;
                    var new_izlaz_sifra  = `izlaz` + cit_rand();
                    product_data.kalk.post_kalk[post_ind].procesi[proces_ind].izlazi[iz_ind].sifra = new_izlaz_sifra;


                    if ( proc.ulazi?.length > 0  ) {
                      $.each( proc.ulazi, function(ulaz_ind, ulaz) {
                        if ( ulaz.izlaz_sifra == curr_izlaz_sifra ) {
                          product_data.kalk.post_kalk[post_ind].procesi[proces_ind].ulazi[ulaz_ind].izlaz_sifra = new_izlaz_sifra;
                        }
                      });
                    };

                    if ( !izlaz.kalk_element?.elem_tip.kalk ) {
                      
                      if ( izlaz.kalk_element?.sifra == curr_elem_sifra ) 
                        product_data.kalk.post_kalk[post_ind].procesi[proces_ind].izlazi[iz_ind].kalk_element.sifra = new_elem_sifra;
                      if ( izlaz.kalk_element?.elem_parent == curr_elem_sifra ) 
                        product_data.kalk.post_kalk[post_ind].procesi[proces_ind].izlazi[iz_ind].kalk_element.elem_parent = new_elem_sifra;
                      
                    };
                    
                  }); // loop po izlazima

                }; // kraj da li postoje ulazi


                if ( proc.ulazi?.length > 0 )  {

                  $.each( proc.ulazi, function(ulaz_ind, ulaz) {

                    product_data.kalk.post_kalk[post_ind].procesi[proces_ind].ulazi[ulaz_ind].sifra = `ulaz` + cit_rand();

                    if ( !ulaz.kalk_element?.elem_tip.kalk ) {
                      
                      if ( ulaz.kalk_element?.sifra == curr_elem_sifra ) 
                        product_data.kalk.post_kalk[post_ind].procesi[proces_ind].ulazi[ulaz_ind].kalk_element.sifra = new_elem_sifra;
                      if ( ulaz.kalk_element?.elem_parent == curr_elem_sifra ) 
                        product_data.kalk.post_kalk[post_ind].procesi[proces_ind].ulazi[ulaz_ind].kalk_element.elem_parent = new_elem_sifra;
                      
                    };
                    
                  });
                };

                proc.action.action_sifra = `action`+cit_rand();


              })// kraj looop procesi

            }; // jel postoje procesi

          }); // loop po  kalk production

        }; //  jel postoji post kalk

      }; // jel postoji kalk u productu

    }); // loop po svim elementima producta
   
    // ---------------------- SADA MORAM PROMJENITI SVE SIFRE OD PROCESNIH ELEMENATA KOJI NISU INICIJALNO U PRODUCT ELEMENTS
    
    
    
    if ( product_data.kalk ) {

      // obriši ovaj kalk id tako da spremi novi kalk koja je kopija
      delete product_data.kalk._id;

      if ( product_data.kalk.offer_kalk?.length > 0 ) {

        $.each( product_data.kalk.offer_kalk, function(offer_ind, off_kalk) {

          if ( off_kalk.procesi?.length > 0 )  {

            $.each( off_kalk.procesi, function(proces_ind, proc) {

              if ( proc.izlazi?.length > 0 ) {

                $.each( proc.izlazi, function(iz_ind, izlaz) {

                  // samo ako JESTE procesni element koji nema kalk propery u sebi
                  if ( izlaz.kalk_element?.elem_tip.kalk == true ) {
                    
                    var curr_elem_sifra = izlaz.kalk_element.sifra;
                    var new_elem_sifra = `sel` + cit_rand();
                    product_data.kalk.offer_kalk[offer_ind].procesi[proces_ind].izlazi[iz_ind].kalk_element.sifra = new_elem_sifra;
                    
                    
                    if ( proc.ulazi?.length > 0 ) {
                      
                      $.each( proc.ulazi, function(ulaz_ind, ulaz) {

                        if ( ulaz.kalk_element?.sifra == curr_elem_sifra ) 
                          product_data.kalk.offer_kalk[offer_ind].procesi[proces_ind].ulazi[ulaz_ind].kalk_element.sifra = new_elem_sifra;
                        
                        if ( ulaz.kalk_element?.elem_parent == curr_elem_sifra ) 
                          product_data.kalk.offer_kalk[offer_ind].procesi[proces_ind].ulazi[ulaz_ind].kalk_element.elem_parent = new_elem_sifra;

                      }); // loop po ulazima
                    
                    }; // da li postoje ulazi

                  }; // mora biti procesni element

                }); // loop po izlazima

              }; // kraj da li postoje izlazi

            })// kraj looop procesi

          }; // jel postoje procesi

        }); // loop po  kalk offer

      }; //  jel postoji offer kalk

      if ( product_data.kalk.pro_kalk?.length > 0 ) {

        $.each( product_data.kalk.pro_kalk, function(pro_ind, production_kalk) {


          if ( production_kalk.procesi?.length > 0 )  {

            $.each( production_kalk.procesi, function(proces_ind, proc) {

              if ( proc.izlazi?.length > 0 ) {

                $.each( proc.izlazi, function(iz_ind, izlaz) {


                  var curr_izlaz_sifra = izlaz.sifra;
                  var new_izlaz_sifra  = `izlaz` + cit_rand();
                  product_data.kalk.pro_kalk[pro_ind].procesi[proces_ind].izlazi[iz_ind].sifra = new_izlaz_sifra;


                  if ( proc.ulazi?.length > 0  ) {
                    $.each( proc.ulazi, function(ulaz_ind, ulaz) {
                      if ( ulaz.izlaz_sifra == curr_izlaz_sifra ) {
                        product_data.kalk.pro_kalk[pro_ind].procesi[proces_ind].ulazi[ulaz_ind].izlaz_sifra = new_izlaz_sifra;
                      }
                    });
                  };

                  if ( !izlaz.kalk_element?.elem_tip.kalk ) {
                    if ( izlaz.kalk_element?.sifra == curr_elem_sifra ) 
                      product_data.kalk.pro_kalk[pro_ind].procesi[proces_ind].izlazi[iz_ind].kalk_element.sifra = new_elem_sifra;
                    if ( izlaz.kalk_element?.elem_parent == curr_elem_sifra ) 
                      product_data.kalk.pro_kalk[pro_ind].procesi[proces_ind].izlazi[iz_ind].kalk_element.elem_parent = new_elem_sifra;
                  };

                }); // loop po izlazima

              }; // kraj da li postoje ulazi


              if ( proc.ulazi?.length > 0 )  {

                $.each( proc.ulazi, function(ulaz_ind, ulaz) {

                  product_data.kalk.pro_kalk[pro_ind].procesi[proces_ind].ulazi[ulaz_ind].sifra = `ulaz` + cit_rand();

                  if ( !ulaz.kalk_element?.elem_tip.kalk ) {

                    if ( ulaz.kalk_element?.sifra == curr_elem_sifra ) 
                      product_data.kalk.pro_kalk[pro_ind].procesi[proces_ind].ulazi[ulaz_ind].kalk_element.sifra = new_elem_sifra;
                    if ( ulaz.kalk_element?.elem_parent == curr_elem_sifra ) 
                      product_data.kalk.pro_kalk[pro_ind].procesi[proces_ind].ulazi[ulaz_ind].kalk_element.elem_parent = new_elem_sifra;

                  };
                });
              };

              proc.action.action_sifra = `action`+cit_rand();


            })// kraj looop procesi

          }; // jel postoje procesi

        }); // loop po  kalk production

      }; //  jel postoji production kalk

      if ( product_data.kalk.post_kalk?.length > 0 ) {

        $.each( product_data.kalk.post_kalk, function(post_ind, kalk_post) {


          if ( kalk_post.procesi?.length > 0 )  {

            $.each( kalk_post.procesi, function(proces_ind, proc) {

              if ( proc.izlazi?.length > 0 ) {

                $.each( proc.izlazi, function(iz_ind, izlaz) {


                  var curr_izlaz_sifra = izlaz.sifra;
                  var new_izlaz_sifra  = `izlaz` + cit_rand();
                  product_data.kalk.post_kalk[post_ind].procesi[proces_ind].izlazi[iz_ind].sifra = new_izlaz_sifra;


                  if ( proc.ulazi?.length > 0  ) {
                    $.each( proc.ulazi, function(ulaz_ind, ulaz) {
                      if ( ulaz.izlaz_sifra == curr_izlaz_sifra ) {
                        product_data.kalk.post_kalk[post_ind].procesi[proces_ind].ulazi[ulaz_ind].izlaz_sifra = new_izlaz_sifra;
                      }
                    });
                  };

                  if ( !izlaz.kalk_element?.elem_tip.kalk ) {

                    if ( izlaz.kalk_element?.sifra == curr_elem_sifra ) 
                      product_data.kalk.post_kalk[post_ind].procesi[proces_ind].izlazi[iz_ind].kalk_element.sifra = new_elem_sifra;
                    if ( izlaz.kalk_element?.elem_parent == curr_elem_sifra ) 
                      product_data.kalk.post_kalk[post_ind].procesi[proces_ind].izlazi[iz_ind].kalk_element.elem_parent = new_elem_sifra;

                  };

                }); // loop po izlazima

              }; // kraj da li postoje ulazi


              if ( proc.ulazi?.length > 0 )  {

                $.each( proc.ulazi, function(ulaz_ind, ulaz) {

                  product_data.kalk.post_kalk[post_ind].procesi[proces_ind].ulazi[ulaz_ind].sifra = `ulaz` + cit_rand();

                  if ( !ulaz.kalk_element?.elem_tip.kalk ) {

                    if ( ulaz.kalk_element?.sifra == curr_elem_sifra ) 
                      product_data.kalk.post_kalk[post_ind].procesi[proces_ind].ulazi[ulaz_ind].kalk_element.sifra = new_elem_sifra;
                    if ( ulaz.kalk_element?.elem_parent == curr_elem_sifra ) 
                      product_data.kalk.post_kalk[post_ind].procesi[proces_ind].ulazi[ulaz_ind].kalk_element.elem_parent = new_elem_sifra;

                  };

                });
              };

              proc.action.action_sifra = `action`+cit_rand();


            })// kraj looop procesi

          }; // jel postoje procesi

        }); // loop po  kalk production

      }; //  jel postoji post kalk

    }; // jel postoji kalk u productu



    
    
    return product_data;
    
    
  };
  this_module.change_sifre = change_sifre;
  