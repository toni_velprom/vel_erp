    // NAČINI PLAĆANJA
    var multi_widths_pay_type = { naziv: 1, delete_pay_type: 1 };
    var col_widths_sum_pay_type = 0;
    $.each(multi_widths_pay_type, function(width_key, width) {
      col_widths_sum_pay_type += Number(width);
    });
    
    $(`#`+rand_id+`_nacini_placanja`).data('cit_props', {
      
      desc: 'unutar partnera  - odabir nacina placanja drop list',
      local: true,
      /*col_widths: [70, 15, 15],*/
      show_cols: ['naziv' ], // 'button_edit', 'button_delete' ----> ovo moram ukljuciti ako želim da se prikaže !!!!!!
      return: {},
      show_on_click: true,
      list: 'pay_type',
      cit_run: this_module.choose_nacin_placanja,
      
      /* sve ispod su zapravo propsi za listu koja se kreira kad selektiraš red u drop listi */
      multi_list: this_module.cit_data[id].nacini_placanja,
      multi_list_parent: `#nacini_placanja_box`,
      multi_col_order: [ 'naziv', 'delete_pay_type'],
      multi_widths: multi_widths_pay_type,
      add_cols: [
        
        function(row_id) { 
          var prop_name = `delete_pay_type`;
          var header = `<div class="multi_header_cell" style="width: ${multi_widths_pay_type.delete_pay_type/col_widths_sum_pay_type*100}%;">OBRIŠI</div>`;
          var cell = 
            `
            <div class="multi_cell" style="width: ${multi_widths_pay_type.delete_pay_type/col_widths_sum_pay_type*100}%;" data-row_id="${row_id}" >
              <div class="multi_cell_delete_btn multi_list_btn"> <i class="fal fa-times"></i> </div> 
            </div>
            `;

          return {
            prop_name:  prop_name,
            header: header,
            cell: cell
          }
          
        },
      ]
      
    });
    var props = $(`#`+rand_id+`_nacini_placanja`).data('cit_props');
    create_multi_list( rand_id+`_nacini_placanja`, props );
    wait_for(`$('#${rand_id}_nacini_placanja_multi_list').length > 0`, function() {
      this_module.register_nacini_placanja_events(rand_id+`_nacini_placanja`);
    }, 50*1000);
    