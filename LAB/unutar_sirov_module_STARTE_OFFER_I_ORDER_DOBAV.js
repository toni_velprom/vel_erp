 
    
$('#sirov_offer').off("click");
$('#sirov_offer').on("click", this_module.sirov_offer_dobav ); 


$('#sirov_order').off("click");
$('#sirov_order').on("click", this_module.sirov_order_dobav ); 

  


async function sirov_offer_dobav() {


  var this_comp_id = $(this).closest('.cit_comp')[0].id;
  var data = this_module.cit_data[this_comp_id];
  var rand_id =  this_module.cit_data[this_comp_id].rand_id;


  refresh_simple_cal( $('#'+rand_id+`_record_est_time`) );


  if ( !data.ask_kolicina_1 ) {
    popup_warn(`Upišite barem prvu količinu za generiranje dokumenta!`);
    return;
  };

  if ( !data.doc_lang ) {
    data.doc_lang = { sifra: `hr`, naziv: `HRVATSKI` };
  };

  if ( !data.doc_valuta ) {
    data.doc_valuta = { "sifra": "DEFV1", "naziv": "Hrvatska", "valuta": "HRK" };
  };


  if ( !data.dobavljac ) {
    popup_warn(`Potrebno izabrati dobavljača!`);
    return;
  };


  if ( !data.record_est_time ) {
    popup_warn(`Potrebno izabrati procjenu datuma!`);
    return;
  };






  var preview_mod = await get_cit_module('/modules/previews/cit_previews.js', `load_css`);


  var db_partner = null;
  var dobavljac_id = data.dobavljac?._id ? data.dobavljac._id : null;

  if ( dobavljac_id ) {
    db_partner = await this_module.get_dobavljac(dobavljac_id);
  };

  if ( !db_partner ) {
    popup_error(`Greška prilikom pretrage dobavljača u bazi !!!!`);
    return;
  };


  var items = [];

  var order_kolicina = data.ask_kolicina_1 / (data.normativ_omjer || 1); 

  if ( data.ask_kolicina_1 ) {
    items.push({
      naziv: ( (data.dobav_sifra || "") + "--" + (data.dobav_naziv || "") + " (" + data.sirovina_sifra + ")" ),
      jedinica: data.min_order_unit.jedinica,
      count: data.ask_kolicina_1 / (data.normativ_omjer || 1), // npr upisujem 1000 mm ali dobivam 1000 mm / 1 kolut je 100 metara tj 100000 mm = 0,1 kolut 
      decimals: 3,
    });
  };

  if ( data.ask_kolicina_2 ) {
    items.push({
      naziv: ((data.dobav_sifra || "") + "--" + (data.dobav_naziv || "") + " (" + data.sirovina_sifra + ")" ),
      jedinica: data.min_order_unit.jedinica,
      count: data.ask_kolicina_2 / (data.normativ_omjer || 1), // npr upisujem 1000 mm ali dobivam 1000 mmm / 1 kolut je 100 metara tj 100000 mm = 0,1 kolut 
      decimals: 3
    });
  };

  if ( data.ask_kolicina_3 ) {
    items.push({
      naziv: ((data.dobav_sifra || "") + "--" + (data.dobav_naziv || "") + " (" + data.sirovina_sifra + ")" ),
      jedinica: data.min_order_unit.jedinica,
      count: data.ask_kolicina_3 / (data.normativ_omjer || 1), // npr upisujem 1000 mm ali dobivam 1000 mmm / 1 kolut je 100 metara tj 100000 mm = 0,1 kolut ,
      decimals: 3,
    });

  };


  if ( data.ask_kolicina_4 ) {
    items.push({
      naziv: ((data.dobav_sifra || "") + "--" + (data.dobav_naziv || "") + " (" + data.sirovina_sifra + ")" ),
      jedinica: data.min_order_unit.jedinica,
      count: data.ask_kolicina_4 / (data.normativ_omjer || 1), // npr upisujem 1000 mm ali dobivam 1000 mmm / 1 kolut je 100 metara tj 100000 mm = 0,1 kolut ,
      decimals: 3,
    });

  };

  if ( data.ask_kolicina_5 ) {
    items.push({
      naziv: ((data.dobav_sifra || "") + "--" + (data.dobav_naziv || "") + " (" + data.sirovina_sifra + ")" ),
      jedinica: data.min_order_unit.jedinica,
      count: data.ask_kolicina_5 / (data.normativ_omjer || 1), // npr upisujem 1000 mm ali dobivam 1000 mmm / 1 kolut je 100 metara tj 100000 mm = 0,1 kolut ,
      decimals: 3,
    });

  };

  if ( data.ask_kolicina_6 ) {
    items.push({
      naziv: ((data.dobav_sifra || "") + "--" + (data.dobav_naziv || "") + " (" + data.sirovina_sifra + ")" ),
      jedinica: data.min_order_unit.jedinica,
      count: data.ask_kolicina_6 / (data.normativ_omjer || 1), // npr upisujem 1000 mm ali dobivam 1000 mmm / 1 kolut je 100 metara tj 100000 mm = 0,1 kolut ,
      decimals: 3,
    });

  };    

  var doc_sirovs = cit_deep(data);

  doc_sirovs[0].doc_statuses = data.selected_statuses_in_sirov || null;
  doc_sirovs[0].doc_kolicine = [];


  for( ask=1; ask<=6; ask++ ) {
    var kol = data[`ask_kolicina_`+ask];
    kol = cit_number(kol);
    if ( kol !== null ) doc_sirovs.doc_kolicine.push(kol);
  };

  var data_for_doc = {


    sifra: "dfd"+cit_rand(),
    time: Date.now(),

    for_module: `sirov`,

    doc_sirovs: doc_sirovs,

    proj_sifra : null,
    item_sifra : null,
    variant : null,
    status: null,


    sirov_id: data._id || null,
    full_record_name: ( data.sirovina_sifra + "--" + window.concat_naziv_sirovine(data) + "--" + data.dobavljac?.naziv),

    doc_valuta: ( data.doc_valuta?.valuta || "HRK" ), // default je HRK
    doc_valuta_manual: (data.doc_valuta_manual || null ), // manual tečaj
    referent: window.cit_user.full_name,
    place: `Velika Gorica`,

    doc_type: `offer_dobav`,
    doc_num: `---`,


    oib: (db_partner.oib || db_partner.vat_num || ""),
    partner_name: db_partner.naziv,
    partner_adresa: data.doc_adresa.full_adresa,
    partner_id: db_partner._id,

    /*
    partner_place: `67122 ALTRIP`,
    partner_country: `Germany`,
    */

    rok: data.record_est_time,
    dobav_ponuda_sifra: ``,

    items: items,

  };

  // return;

  preview_mod.generate_cit_doc( data_for_doc, data.doc_lang.sifra );

};
this_module.sirov_offer_dobav = sirov_offer_dobav;


async function sirov_order_dobav() {


  var this_comp_id = $(this).closest('.cit_comp')[0].id;
  var data = this_module.cit_data[this_comp_id];
  var rand_id =  this_module.cit_data[this_comp_id].rand_id;


  refresh_simple_cal( $('#'+rand_id+`_record_est_time`) );


  if ( !data.ask_kolicina_1 ) {
    popup_warn(`Upišite količinu za generiranje dokumenta!`);
    return;
  };

  if ( data.ask_kolicina_1 < data.min_order ) {
    popup_warn(`Minimalna količina za naručiti je: ` + cit_format(data.min_order, 2) );
    return;
  };

  if ( !data.dobavljac ) {
    popup_warn(`Potrebno izabrati dobavljača!`);
    return;
  };

  if ( !data.doc_lang ) {
    data.doc_lang = { sifra: `hr`, naziv: `HRVATSKI` };
  };

  if ( !data.doc_valuta ) {
    data.doc_valuta = { "sifra": "DEFV1", "naziv": "Hrvatska", "valuta": "HRK" };
  };


  if ( !data.record_est_time ) {
    popup_warn(`Potrebno izabrati procjenu datuma!`);
    return;
  };

  if ( !data.current_record_parent ) {
    popup_warn(`Potrebno izabrati RFQ na osnovu kojeg radite ovu narudžbu!`);
    return;
  };



  /*
  if ( !data.order_price ) {
    popup_warn(`Upišite dogovorenu cijenu!`);
    return;
  };

  */
  var preview_mod = await get_cit_module('/modules/previews/cit_previews.js', `load_css`);


  var db_partner = null;
  var dobavljac_id = data.dobavljac?._id ? data.dobavljac._id : null;

  if ( dobavljac_id ) {
    db_partner = await this_module.get_dobavljac(dobavljac_id);
  };

  if ( !db_partner ) {
    popup_error(`Greška prilikom pretrage dobavljača u bazi !!!!`);
    return;
  };

  var cijena_za_order = data.alt_cijena;

  var order_kolicina = data.ask_kolicina_1 / data.normativ_omjer || 1; 

  // ako postoji normativ
  if ( data.normativ_omjer ) cijena_za_order =  data.cijena / data.normativ_omjer; 

  var data_for_doc = {

    sifra: "dfd"+cit_rand(),
    time: Date.now(),

    for_module: `sirov`,

    proj_sifra : null,
    item_sifra : null,
    variant : null,
    status: null,
    sirov_id: data._id || null,
    full_record_name: ( data.sirovina_sifra + "--" + window.concat_naziv_sirovine(data) + "--" + data.dobavljac?.naziv ),

    doc_valuta: (data.doc_valuta?.valuta || "HRK" ), // default je HRK
    doc_valuta_manual: ( data.doc_valuta_manual || null ), // manual tečaj
    referent: window.cit_user.full_name,
    place: `Velika Gorica`,

    doc_type: `order_dobav`,
    doc_num: `---`,

    oib: (db_partner.oib || db_partner.vat_num || ""),
    partner_name: db_partner.naziv,
    partner_adresa: data.doc_adresa.full_adresa,
    partner_id: db_partner._id,

    /*
    partner_place: `67122 ALTRIP`,
    partner_country: `Germany`,
    */

    rok: data.record_est_time,
    dobav_ponuda_sifra: data.doc_ponuda_num || "",

    items: [
      { 
        naziv: ((data.dobav_sifra || "") + "--" + (data.dobav_naziv || "") + " (" + data.sirovina_sifra + ")" ),
        jedinica: ( data.min_order_unit.jedinica || data.osnovna_jedinica_out.jedinica ),
        price: (data.order_price || cijena_za_order), // ako je user upisao custom cijenu za order onda uzmi tu cijenu
        count: order_kolicina, 
        decimals: 2,
        /*link:*/
      },
    ],


  };

  preview_mod.generate_cit_doc( data_for_doc, data.doc_lang.sifra );

};
this_module.sirov_order_dobav = sirov_order_dobav;

