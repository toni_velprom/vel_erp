{
    "_id" : ObjectId("616013226c72300eabd131b0"),
    "user_saved" : {
        "user_number" : 10,
        "full_name" : "Toni Kutlić",
        "email" : "toni.kutlic@velprom.hr"
    },
    "saved" : 1628867962873.0,
    "edited" : 1656067567855.0,
    "user_edited" : {
        "_id" : "60a4bc4f8617c34c16d1979f",
        "user_number" : 10,
        "full_name" : "Toni Kutlić",
        "email" : "toni.kutlic@velprom.hr"
    },
      
    "status_conf" : [ 
      
        {
            "sifra" : "ISerror",
            "active" : true,
            "next_status_num" : 1100,
            "naziv" : "GREŠKA UPISA",
            "next_statuses" : [],
            "grana_finish" : [],
            "is_work" : null,
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Općenito",
            "active_status" : true,
            "button_edit" : true,
            "status_num" : null,
            "dep" : {
                "sifra" : "GEN",
                "naziv" : "Općenito"
            },
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "status_num" : 100,
            "naziv" : "UPIT KUPCA",
            "def_deadline" : null,
            "is_work" : false,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "sifra" : "IS1634053892953081",
            "active" : true,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "active_status" : true,
            "button_edit" : true,
            "next_statuses" : [ 
                {
                    "status_num" : 200,
                    "naziv" : "KREIRANJE PONUDE",
                    "def_deadline" : 8,
                    "is_work" : true,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "sifra" : "IS16340539733309028",
                    "active" : true,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "dep_naziv" : "Prodaja",
                    "dep" : {
                        "sifra" : "SAL",
                        "naziv" : "Prodaja"
                    },
                    "next_status_num" : 10,
                    "work_done_for" : null,
                    "work_finished" : null
                }, 
                {
                    "naziv" : "MOLIM PROVJERU I ODOBRENJE PONUDE",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "UPR",
                        "naziv" : "Uprava"
                    },
                    "sifra" : "IS16468367857771187",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 13500,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Uprava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 15
                }, 
                {
                    "def_deadline" : null,
                    "naziv" : "PONUDA POSLANA KUPCU",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SAL",
                        "naziv" : "Prodaja"
                    },
                    "sifra" : "IS16341230539613132",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 240,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Prodaja",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_status_num" : 20,
                    "work_finished" : null,
                    "work_done_for" : "IS16340539733309028",
                    "work_done_for_naziv" : ""
                }, 
                {
                    "naziv" : "KUPAC TRAŽI KOREKCIJE PONUDE",
                    "status_num" : 260,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SAL",
                        "naziv" : "Prodaja"
                    },
                    "sifra" : "IS16341217705872617",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Prodaja",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_status_num" : 40,
                    "work_done_for" : null,
                    "work_finished" : null
                }, 
                {
                    "naziv" : "RADIM KOREKCIJE PONUDE",
                    "status_num" : 270,
                    "def_deadline" : 8,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SAL",
                        "naziv" : "Prodaja"
                    },
                    "sifra" : "IS16341217951720005",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Prodaja",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_status_num" : 60,
                    "work_done_for" : null,
                    "work_finished" : null
                }, 
                {
                    "naziv" : "KOREKCIJE PONUDE GOTOVE",
                    "work_done_for" : "IS16341217951720005",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SAL",
                        "naziv" : "Prodaja"
                    },
                    "sifra" : "IS16418150832797854",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 8900,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Prodaja",
                    "active_status" : true,
                    "work_done_for_naziv" : "RADIM KOREKCIJE PONUDE",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 80
                }
            ],
            "grana_finish" : [ 
                {
                    "naziv" : "KUPAC ODBIO PONUDU !!!",
                    "status_num" : 280,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SAL",
                        "naziv" : "Prodaja"
                    },
                    "sifra" : "IS1634123196597815",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Prodaja",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_status_num" : 10,
                    "work_done_for" : null,
                    "work_finished" : null
                }, 
                {
                    "naziv" : "KUPAC ODOBRIO PONUDU",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SAL",
                        "naziv" : "Prodaja"
                    },
                    "status_num" : 250,
                    "sifra" : "IS16341217492822178",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Prodaja",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_status_num" : 20,
                    "work_finished" : null,
                    "work_done_for" : null,
                    "work_done_for_naziv" : ""
                }, 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : "",
                    "work_done_for" : null,
                    "work_finished" : null
                }
            ],
            "dep_naziv" : "Prodaja",
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "status_num" : 200,
            "naziv" : "KREIRANJE PONUDE",
            "def_deadline" : 8,
            "is_work" : true,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "sifra" : "IS16340539733309028",
            "active" : true,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "active_status" : true,
            "button_edit" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "dep_naziv" : "Prodaja",
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "def_deadline" : null,
            "naziv" : "PONUDA POSLANA KUPCU",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16341230539613132",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 240,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "button_edit" : true,
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : "IS16340539733309028",
            "work_done_for_naziv" : "KREIRANJE PONUDE"
        }, 
        {
            "naziv" : "KUPAC ODOBRIO PONUDU",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "status_num" : 250,
            "sifra" : "IS16341217492822178",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "button_edit" : true,
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "KUPAC TRAŽI KOREKCIJE PONUDE",
            "status_num" : 260,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16341217705872617",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "button_edit" : true,
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "RADIM KOREKCIJE PONUDE",
            "status_num" : 270,
            "def_deadline" : 8,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16341217951720005",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "button_edit" : true,
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "KUPAC ODBIO PONUDU !!!",
            "status_num" : 280,
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS1634123196597815",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "button_edit" : true,
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "status_num" : 300,
            "naziv" : "ZAHTJEV PREMA CAD-u ZA RAZRADU",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "sifra" : "IS16340540259946118",
            "is_work" : null,
            "active" : true,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "active_status" : true,
            "button_edit" : true,
            "next_statuses" : [ 
                {
                    "status_num" : 400,
                    "naziv" : "RAZRADA U TOKU",
                    "def_deadline" : 8,
                    "is_work" : true,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "sifra" : "IS16340540491308528",
                    "active" : true,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "dep_naziv" : "CAD odjel",
                    "dep" : {
                        "sifra" : "CAD",
                        "naziv" : "CAD odjel"
                    },
                    "next_status_num" : 10,
                    "work_done_for" : null,
                    "work_finished" : null
                }, 
                {
                    "naziv" : "PITANJE",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "sifra" : "IS16457184141333354",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 12400,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 30
                }, 
                {
                    "naziv" : "PREUZIMAM ZADATAK",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GRF",
                        "naziv" : "Grafički odjel"
                    },
                    "sifra" : "IS16442324791051643",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 10200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Grafički odjel",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 50
                }, 
                {
                    "naziv" : "ODGOVOR",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "sifra" : "IS1645718439670303",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 12500,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 60
                }
            ],
            "grana_finish" : [ 
                {
                    "status_num" : 550,
                    "naziv" : "RAZRADA GOTOVA",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SAL",
                        "naziv" : "Prodaja"
                    },
                    "sifra" : "IS16341218464135994",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Prodaja",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_status_num" : 10,
                    "work_done_for" : null,
                    "work_finished" : null
                }, 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : "",
                    "work_done_for" : null,
                    "work_finished" : null
                }, 
                {
                    "naziv" : "ODUSTAJEMO OD OVE GRANE",
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "sifra" : "IS16341239747891487",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 1200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_status_num" : 60,
                    "work_finished" : null,
                    "work_done_for" : null,
                    "work_done_for_naziv" : ""
                }
            ],
            "dep_naziv" : "CAD odjel",
            "dep" : {
                "sifra" : "CAD",
                "naziv" : "CAD odjel"
            },
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "status_num" : 400,
            "naziv" : "RAZRADA U TOKU",
            "def_deadline" : 8,
            "is_work" : true,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "sifra" : "IS16340540491308528",
            "active" : true,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "active_status" : true,
            "button_edit" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "dep_naziv" : "CAD odjel",
            "dep" : {
                "sifra" : "CAD",
                "naziv" : "CAD odjel"
            },
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "status_num" : 500,
            "naziv" : "TRAŽIM POJAŠNJENJE ZA RAZRADU",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "sifra" : "IS16340540933159375",
            "is_work" : null,
            "active" : true,
            "def_deadline" : null,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "active_status" : true,
            "button_edit" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "dep_naziv" : "CAD odjel",
            "dep" : {
                "sifra" : "CAD",
                "naziv" : "CAD odjel"
            },
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "status_num" : 550,
            "naziv" : "RAZRADA GOTOVA",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16341218464135994",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "button_edit" : true,
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "status_num" : 600,
            "naziv" : "POSLANI UPITI DOBAV. SIROVINA",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "sifra" : "IS16340541453248845",
            "is_work" : null,
            "active" : true,
            "def_deadline" : null,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "active_status" : true,
            "button_edit" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "dep_naziv" : "Nabava",
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "DOŠLA PONUDA OD DOBAVLJAČA SIROVINE",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "sifra" : "IS16340541627480386",
            "is_work" : null,
            "active" : true,
            "def_deadline" : null,
            "status_num" : 700,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "active_status" : true,
            "button_edit" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "dep_naziv" : "Nabava",
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "status_num" : 750,
            "naziv" : "ZAHTJEV DA SE ODOBRI PONUDA DOBAVLJAČA",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "UPR",
                "naziv" : "Uprava"
            },
            "sifra" : "IS16341219612087546",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Uprava",
            "active_status" : true,
            "button_edit" : true,
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "PONUDA OD DOBAVLJAČA ODOBRENA",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "sifra" : "IS16340541798288716",
            "is_work" : null,
            "active" : true,
            "def_deadline" : null,
            "status_num" : 800,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "active_status" : true,
            "button_edit" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "dep_naziv" : "Nabava",
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "PONUDA OD DOBAVLJAČA ODBIJENA",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "sifra" : "IS1634054191892784",
            "is_work" : null,
            "active" : true,
            "def_deadline" : null,
            "status_num" : 900,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "active_status" : true,
            "button_edit" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "dep_naziv" : "Nabava",
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "ZAHTJEV ZA INTERNU GRAF. PRIPREMU",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "sifra" : "IS1634054309942639",
            "is_work" : null,
            "active" : true,
            "def_deadline" : null,
            "status_num" : 1000,
            "next_statuses" : [ 
                {
                    "status_num" : 1050,
                    "naziv" : "INTERNA GRAF. PRIPREMA U TOKU",
                    "is_work" : true,
                    "def_deadline" : 8,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GRF",
                        "naziv" : "Grafički odjel"
                    },
                    "sifra" : "IS16341246954324487",
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Grafički odjel",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_status_num" : 10,
                    "work_done_for" : null,
                    "work_finished" : null
                }, 
                {
                    "naziv" : "PREUZIMAM ZADATAK",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GRF",
                        "naziv" : "Grafički odjel"
                    },
                    "sifra" : "IS16442324791051643",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 10200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Grafički odjel",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 30
                }, 
                {
                    "naziv" : "PITANJE",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "sifra" : "IS16457184141333354",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 12400,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 50
                }, 
                {
                    "naziv" : "ODGOVOR",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "sifra" : "IS1645718439670303",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 12500,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 70
                }
            ],
            "grana_finish" : [ 
                {
                    "naziv" : "INTERNA GRAF. PRIPREMA GOTOVA",
                    "status_num" : 1080,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GRF",
                        "naziv" : "Grafički odjel"
                    },
                    "sifra" : "IS1634124894341743",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Grafički odjel",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_status_num" : 10,
                    "work_done_for" : "IS16341246954324487",
                    "work_finished" : null
                }, 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : "",
                    "work_done_for" : null,
                    "work_finished" : null
                }, 
                {
                    "naziv" : "ODUSTAJEMO OD OVE GRANE",
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "sifra" : "IS16341239747891487",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 1200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_status_num" : 60,
                    "work_done_for" : null,
                    "work_finished" : null
                }
            ],
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Grafički odjel",
            "active_status" : true,
            "button_edit" : true,
            "dep" : {
                "sifra" : "GRF",
                "naziv" : "Grafički odjel"
            },
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "status_num" : 1050,
            "naziv" : "INTERNA GRAF. PRIPREMA U TOKU",
            "is_work" : true,
            "def_deadline" : 8,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GRF",
                "naziv" : "Grafički odjel"
            },
            "sifra" : "IS16341246954324487",
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Grafički odjel",
            "active_status" : true,
            "button_edit" : true,
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "INTERNO POJAŠNJENJE ZA GRAF. PRIPREMU",
            "def_deadline" : null,
            "status_num" : 1060,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GRF",
                "naziv" : "Grafički odjel"
            },
            "sifra" : "IS1634124770417204",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Grafički odjel",
            "active_status" : true,
            "button_edit" : true,
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "POJAŠNJENJE ZA GRAF. PRIPREMU OD KUPCA",
            "status_num" : 1070,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GRF",
                "naziv" : "Grafički odjel"
            },
            "sifra" : "IS16341248192264958",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Grafički odjel",
            "active_status" : true,
            "button_edit" : true,
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "INTERNA GRAF. PRIPREMA GOTOVA",
            "status_num" : 1080,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GRF",
                "naziv" : "Grafički odjel"
            },
            "sifra" : "IS1634124894341743",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Grafički odjel",
            "active_status" : true,
            "button_edit" : true,
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : "IS16341246954324487",
            "work_done_for_naziv" : "INTERNA GRAF. PRIPREMA U TOKU"
        }, 
        {
            "naziv" : "ZAHTJEV NABAVI ZA SIROVINU",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS1634121913167369",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [ 
                {
                    "naziv" : "ISTRAŽIVANJE TRŽIŠTA SIROVINA U TOKU",
                    "def_deadline" : 8,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "is_work" : true,
                    "sifra" : "IS16348130249015862",
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 1600,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 3,
                    "work_done_for" : null,
                    "work_done_for_naziv" : ""
                }, 
                {
                    "naziv" : " GOTOVO ISTRAŽIVANJE TRŽIŠTA SIROVINA",
                    "work_done_for" : "IS16348130249015862",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS1634917698679862",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 2400,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for_naziv" : "ISTRAŽIVANJE TRŽIŠTA SIROVINA U TOKU",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 4
                }, 
                {
                    "status_num" : 600,
                    "naziv" : "POSLANI UPITI DOBAV. SIROVINA",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "sifra" : "IS16340541453248845",
                    "is_work" : null,
                    "active" : true,
                    "def_deadline" : null,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "dep_naziv" : "Nabava",
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "next_status_num" : 5,
                    "work_finished" : null,
                    "work_done_for" : null
                }, 
                {
                    "naziv" : "DOŠLA PONUDA OD DOBAVLJAČA SIROVINE",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "sifra" : "IS16340541627480386",
                    "is_work" : null,
                    "active" : true,
                    "def_deadline" : null,
                    "status_num" : 700,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "dep_naziv" : "Nabava",
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "next_status_num" : 25,
                    "work_finished" : null,
                    "work_done_for" : null
                }, 
                {
                    "naziv" : "USPOREĐIVANJE PRISTIGLIH PONUDA",
                    "is_work" : true,
                    "def_deadline" : 8,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16348132046361294",
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 1700,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 28,
                    "work_done_for" : null
                }, 
                {
                    "naziv" : "SIROVINA NARUČENA",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16360270099667788",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 3400,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 41
                }, 
                {
                    "naziv" : "UPLATA SIROVINE",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16348149799355674",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 2300,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 45,
                    "work_done_for" : null
                }, 
                {
                    "naziv" : "NAJAVA SKLADIŠTU DA SIROVINA DOLAZI",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SKL",
                        "naziv" : "Skladište"
                    },
                    "sifra" : "IS16348136387145403",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 2200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Skladište",
                    "active_status" : true,
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 50,
                    "work_done_for" : null
                }, 
                {
                    "naziv" : "SIROVINA DJELOMIČNO DOŠLA",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SKL",
                        "naziv" : "Skladište"
                    },
                    "def_deadline" : null,
                    "sifra" : "IS16348135721126677",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 2000,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Skladište",
                    "active_status" : true,
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 60,
                    "work_done_for" : null
                }, 
                {
                    "naziv" : "PROMJENA DATUMA ROBE",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16432826880204646",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 10100,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 65
                }, 
                {
                    "naziv" : "SIROVINA KASNI !!!",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16348133360123557",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 1900,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 70,
                    "work_done_for" : null
                }, 
                {
                    "status_num" : 3800,
                    "naziv" : "OK - STATUS PROČITAN",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "sifra" : "IS1640098251821862",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 80
                }, 
                {
                    "naziv" : "PITANJE",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "sifra" : "IS16457184141333354",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 12400,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 100
                }, 
                {
                    "naziv" : "ODGOVOR",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "sifra" : "IS1645718439670303",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 12500,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 120
                }
            ],
            "grana_finish" : [ 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : "",
                    "work_done_for" : null,
                    "work_finished" : null
                }, 
                {
                    "naziv" : "ODUSTAJEMO OD OVE GRANE",
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "sifra" : "IS16341239747891487",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 1200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_status_num" : 60,
                    "work_done_for" : null,
                    "work_finished" : null
                }, 
                {
                    "naziv" : "SIROVINA DOŠLA",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SKL",
                        "naziv" : "Skladište"
                    },
                    "sifra" : "IS16348135958806685",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 2100,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Skladište",
                    "active_status" : true,
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 80,
                    "work_done_for" : null,
                    "work_done_for_naziv" : ""
                }
            ],
            "status_num" : 1100,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "button_edit" : true,
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "ODUSTAJEMO OD OVE GRANE",
            "dep" : {
                "sifra" : "GEN",
                "naziv" : "Općenito"
            },
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "sifra" : "IS16341239747891487",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 1200,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Općenito",
            "active_status" : true,
            "button_edit" : true,
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "POSLAN UZORAK",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "DOS",
                "naziv" : "Dostava"
            },
            "sifra" : "IS16342948328868508",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 1300,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Dostava",
            "active_status" : true,
            "button_edit" : true,
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "UZORAK PRIHVAĆEN",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16342948570378806",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 1400,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "button_edit" : true,
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "UZORAK ODBIJEN",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS1634294875806574",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 1500,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "button_edit" : true,
            "next_status_num" : null,
            "work_finished" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "ISTRAŽIVANJE TRŽIŠTA SIROVINA U TOKU",
            "def_deadline" : 8,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "is_work" : true,
            "sifra" : "IS16348130249015862",
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 1600,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "USPOREĐIVANJE PRISTIGLIH PONUDA",
            "is_work" : true,
            "def_deadline" : 8,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16348132046361294",
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 1700,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "POSLANA NARUDŽBA SIROVINE",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16348132836369639",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 1800,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "SIROVINA KASNI !!!",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16348133360123557",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 1900,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "SIROVINA DJELOMIČNO DOŠLA",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SKL",
                "naziv" : "Skladište"
            },
            "def_deadline" : null,
            "sifra" : "IS16348135721126677",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 2000,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Skladište",
            "active_status" : true,
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "SIROVINA DOŠLA",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SKL",
                "naziv" : "Skladište"
            },
            "sifra" : "IS16348135958806685",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 2100,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Skladište",
            "active_status" : true,
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "NAJAVA SKLADIŠTU DA SIROVINA DOLAZI",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SKL",
                "naziv" : "Skladište"
            },
            "sifra" : "IS16348136387145403",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 2200,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Skladište",
            "active_status" : true,
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : "UPLATA SIROVINE",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16348149799355674",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 2300,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null,
            "work_done_for" : null,
            "work_done_for_naziv" : ""
        }, 
        {
            "naziv" : " GOTOVO ISTRAŽIVANJE TRŽIŠTA SIROVINA",
            "work_done_for" : "IS16348130249015862",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS1634917698679862",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 2400,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for_naziv" : "ISTRAŽIVANJE TRŽIŠTA SIROVINA U TOKU",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "NABAVA - ZAHTJEV ZA PONUDU DOBAVLJAČA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS1635256284641925",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [ 
                {
                    "naziv" : "NABAVA - PONUDA OD DOBAVLJAČA DOŠLA",
                    "status_num" : 3200,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16352567740304316",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 10
                }, 
                {
                    "naziv" : "NABAVA - MOLIM ODOBRENJE PONUDE DOBAVLJAČA",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16548497358280305",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 13700,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 15
                }, 
                {
                    "naziv" : "NABAVA - PONUDA OD DOBAVLJAČA ODOBRENA",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16352564925889526",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 2600,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 20
                }, 
                {
                    "status_num" : 3300,
                    "naziv" : "NABAVA - SIROVINA NARUČENA",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16360269826066462",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 25
                }, 
                {
                    "naziv" : "NABAVA - UPLATA SIROVINE",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS1635256523573757",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 2700,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 30
                }, 
                {
                    "status_num" : 2800,
                    "naziv" : "NABAVA - NAJAVA SKLADIŠTU DA SIROVINA DOLAZI",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "SKL",
                        "naziv" : "Skladište"
                    },
                    "sifra" : "IS16352565561107395",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Skladište",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 40
                }, 
                {
                    "naziv" : "NABAVA - SIROVINA DJELOMIČNO DOŠLA",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "SKL",
                        "naziv" : "Skladište"
                    },
                    "sifra" : "IS16352565761988662",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 2900,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Skladište",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 50
                }, 
                {
                    "naziv" : "NABAVA - SIROVINA KASNI !!!",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "SKL",
                        "naziv" : "Skladište"
                    },
                    "sifra" : "IS1635256604896468",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 3000,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Skladište",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 60
                }, 
                {
                    "naziv" : "NABAVA - SIROVINA DOŠLA",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "SKL",
                        "naziv" : "Skladište"
                    },
                    "sifra" : "IS16352566219725337",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 3100,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Skladište",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 70
                }, 
                {
                    "naziv" : "NABAVA - POTREBNO PLATITI ROBU",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16557202880964136",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 13800,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 80
                }, 
                {
                    "naziv" : "NABAVA - POTREBNO PLATITI PREDUJAM",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16560675376649348",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 14100,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 90
                }
            ],
            "grana_finish" : [ 
                {
                    "naziv" : "ODUSTAJEMO OD OVE GRANE",
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "sifra" : "IS16341239747891487",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 1200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_status_num" : 20,
                    "work_finished" : null,
                    "work_done_for" : null,
                    "work_done_for_naziv" : ""
                }, 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : ""
                }, 
                {
                    "status_num" : 3900,
                    "naziv" : "NABAVA - PREBAČENO NA PROIZVODNU GRANU",
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS1640268341489634",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 70
                }, 
                {
                    "naziv" : "NABAVA - PRIMKA JE PROKNJIŽENA",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16557203449509055",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 13900,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 80
                }
            ],
            "status_num" : 2500,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "NABAVA - PONUDA OD DOBAVLJAČA ODOBRENA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16352564925889526",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 2600,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "NABAVA - UPLATA SIROVINE",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS1635256523573757",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 2700,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "status_num" : 2800,
            "naziv" : "NABAVA - NAJAVA SKLADIŠTU DA SIROVINA DOLAZI",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "SKL",
                "naziv" : "Skladište"
            },
            "sifra" : "IS16352565561107395",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Skladište",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "NABAVA - SIROVINA DJELOMIČNO DOŠLA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "SKL",
                "naziv" : "Skladište"
            },
            "sifra" : "IS16352565761988662",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 2900,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Skladište",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "NABAVA - SIROVINA KASNI !!!",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "SKL",
                "naziv" : "Skladište"
            },
            "sifra" : "IS1635256604896468",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 3000,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Skladište",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "NABAVA - SIROVINA DOŠLA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "SKL",
                "naziv" : "Skladište"
            },
            "sifra" : "IS16352566219725337",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 3100,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Skladište",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "NABAVA - PONUDA OD DOBAVLJAČA DOŠLA",
            "status_num" : 3200,
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16352567740304316",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "status_num" : 3300,
            "naziv" : "NABAVA - SIROVINA NARUČENA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16360269826066462",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "SIROVINA NARUČENA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16360270099667788",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 3400,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "NABAVA - DOŠLA NEISPRAVNA SIROVINA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16360300569107488",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 3500,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "NABAVA - POSLANA REKLAMACIJA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16360300868075996",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 3600,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "NABAVA - REKLAMACIJA ODOBRENA",
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16360301038632979",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 3700,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "status_num" : 3800,
            "naziv" : "OK - STATUS PROČITAN",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GEN",
                "naziv" : "Općenito"
            },
            "sifra" : "IS1640098251821862",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Općenito",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "status_num" : 3900,
            "naziv" : "NABAVA - PREBAČENO NA PROIZVODNU GRANU",
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS1640268341489634",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "status_num" : 4000,
            "naziv" : "ALAT - U IZRADI",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16408636668597007",
            "is_work" : true,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "ALAT - VRAĆEN KUPCU",
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS164086370575651",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 4200,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "ALAT - UNIŠTEN",
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS1640863723452965",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 4300,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "ALAT - POSUĐEN",
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16408637413651423",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 4400,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "status_num" : 4500,
            "naziv" : "ALAT - NA STANJU",
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16408637769429497",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "ALAT - U RADU PREKO RN",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16408637911745757",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 4600,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "ALAT - U POPRAVKU",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16408638050153875",
            "is_work" : true,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 4700,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "ALAT - POTREBNO DOPREMITI OD DOBAVLJAČA",
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16408638975663738",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 4800,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "ALAT - DOSTAVLJEN OD DOBAVLJAČA",
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16408639465263362",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 4900,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "ZAHTJEV ZA KORIŠTENJE (IZRADU) ALATA",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16408640642602544",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [ 
                {
                    "naziv" : "U TOKU KREIRANJE ILI DOSTAVA ALATA",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SAL",
                        "naziv" : "Prodaja"
                    },
                    "sifra" : "IS16408643295992",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 5200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Prodaja",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 20
                }, 
                {
                    "naziv" : "INFO O ALATU",
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16408660393612527",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 6000,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 30
                }
            ],
            "grana_finish" : [ 
                {
                    "naziv" : "ALAT JE NA STANJU",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SAL",
                        "naziv" : "Prodaja"
                    },
                    "sifra" : "IS16408641608367178",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 5100,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Prodaja",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 10
                }, 
                {
                    "naziv" : "ODUSTAJEMO OD OVE GRANE",
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "sifra" : "IS16341239747891487",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 1200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_status_num" : 30,
                    "work_finished" : null,
                    "work_done_for" : null,
                    "work_done_for_naziv" : ""
                }, 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : ""
                }, 
                {
                    "naziv" : "ALAT TRENUTNO NIJE RASPOLOŽIV",
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS1645796412814405",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 13100,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 60
                }
            ],
            "def_deadline" : null,
            "status_num" : 5000,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "ALAT JE NA STANJU",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16408641608367178",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 5100,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "U TOKU KREIRANJE ILI DOSTAVA ALATA",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16408643295992",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 5200,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "POTVRDA STATUSA ALATA",
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16408646858747212",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 5300,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "KUPAC ZAHTJEVA UZORAK",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16408656614626953",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [ 
                {
                    "naziv" : "INFO O UZORKU",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SAL",
                        "naziv" : "Prodaja"
                    },
                    "sifra" : "IS16408657581602544",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 5600,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Prodaja",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 20
                }, 
                {
                    "naziv" : "UZORAK IZRADA U TOKU",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "CAD",
                        "naziv" : "CAD odjel"
                    },
                    "sifra" : "IS1640865719855465",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 5500,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "CAD odjel",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 30
                }, 
                {
                    "naziv" : "POTREBNO DOSTAVITI UZORAK KUPCU",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SAL",
                        "naziv" : "Prodaja"
                    },
                    "sifra" : "IS16408658529968477",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 5800,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Prodaja",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 40
                }, 
                {
                    "naziv" : "GOTOV UZORAK",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "CAD",
                        "naziv" : "CAD odjel"
                    },
                    "sifra" : "IS1640865823467638",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 5700,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "CAD odjel",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 50
                }, 
                {
                    "naziv" : "POTREBNO VRATITI UZORAK OD KUPCA",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SAL",
                        "naziv" : "Prodaja"
                    },
                    "sifra" : "IS16408658775245452",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 5900,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Prodaja",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 60
                }, 
                {
                    "naziv" : "POSLAN UZORAK",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "DOS",
                        "naziv" : "Dostava"
                    },
                    "sifra" : "IS16342948328868508",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 1300,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Dostava",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_status_num" : 70,
                    "work_finished" : null,
                    "work_done_for" : null,
                    "work_done_for_naziv" : ""
                }
            ],
            "grana_finish" : [ 
                {
                    "naziv" : "UZORAK PRIHVAĆEN",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SAL",
                        "naziv" : "Prodaja"
                    },
                    "sifra" : "IS16342948570378806",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 1400,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Prodaja",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_status_num" : 20,
                    "work_finished" : null,
                    "work_done_for" : null,
                    "work_done_for_naziv" : ""
                }, 
                {
                    "naziv" : "UZORAK ODBIJEN",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SAL",
                        "naziv" : "Prodaja"
                    },
                    "sifra" : "IS1634294875806574",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 1500,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Prodaja",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_status_num" : 30,
                    "work_finished" : null,
                    "work_done_for" : null,
                    "work_done_for_naziv" : ""
                }, 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : ""
                }, 
                {
                    "naziv" : "ODUSTAJEMO OD OVE GRANE",
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "sifra" : "IS16341239747891487",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 1200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_status_num" : 70,
                    "work_finished" : null,
                    "work_done_for" : null,
                    "work_done_for_naziv" : ""
                }
            ],
            "def_deadline" : null,
            "status_num" : 5400,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "UZORAK IZRADA U TOKU",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "CAD",
                "naziv" : "CAD odjel"
            },
            "sifra" : "IS1640865719855465",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 5500,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "CAD odjel",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "INFO O UZORKU",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16408657581602544",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 5600,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "GOTOV UZORAK",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "CAD",
                "naziv" : "CAD odjel"
            },
            "sifra" : "IS1640865823467638",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 5700,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "CAD odjel",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "POTREBNO DOSTAVITI UZORAK KUPCU",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16408658529968477",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 5800,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "POTREBNO VRATITI UZORAK OD KUPCA",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16408658775245452",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 5900,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "INFO O ALATU",
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16408660393612527",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 6000,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "INFO O RAZRADI",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "CAD",
                "naziv" : "CAD odjel"
            },
            "sifra" : "IS16408661352442986",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 6100,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "CAD odjel",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "INFO O GRAF. PRIPREMI",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GRF",
                "naziv" : "Grafički odjel"
            },
            "sifra" : "IS16408661552271416",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 6200,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Grafički odjel",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "ZAHTJEV PREMA CAD-u ZA NACRT ZA PROIZVODNJU",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "CAD",
                "naziv" : "CAD odjel"
            },
            "sifra" : "IS16408750146878123",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [ 
                {
                    "naziv" : "RAD NA NACRTU ZA PROIZVODNJU U TOKU",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "CAD",
                        "naziv" : "CAD odjel"
                    },
                    "is_work" : true,
                    "sifra" : "IS16408750871766353",
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : 8,
                    "status_num" : 6400,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "CAD odjel",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 50
                }
            ],
            "grana_finish" : [ 
                {
                    "naziv" : "GOTOV NACRT ZA PROIZVODNJU",
                    "def_deadline" : null,
                    "work_done_for" : "IS16408750871766353",
                    "dep" : {
                        "sifra" : "CAD",
                        "naziv" : "Razvoj"
                    },
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "sifra" : "IS1644926143514824",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 10800,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Razvoj",
                    "active_status" : true,
                    "work_done_for_naziv" : "RAD NA NACRTU ZA PROIZVODNJU U TOKU",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 30
                }, 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : ""
                }
            ],
            "def_deadline" : null,
            "status_num" : 6300,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "CAD odjel",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "RAD NA NACRTU ZA PROIZVODNJU U TOKU",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "CAD",
                "naziv" : "CAD odjel"
            },
            "is_work" : true,
            "sifra" : "IS16408750871766353",
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : 8,
            "status_num" : 6400,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "CAD odjel",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "VANJSKI TISAK NA PROIZVODU",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16408758411607424",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [ 
                {
                    "naziv" : "VANJSKI TISAK - ODOBRENA PONUDA",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GRF",
                        "naziv" : "Grafički odjel"
                    },
                    "sifra" : "IS16408758954732935",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 6700,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Prodaja",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 30
                }, 
                {
                    "naziv" : "VANJSKI TISAK - POTREBNO POSLATI NAŠ MATERIJAL/PROIZVOD",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GRF",
                        "naziv" : "Grafički odjel"
                    },
                    "sifra" : "IS16408759310586194",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 6800,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Grafički odjel",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 30
                }, 
                {
                    "naziv" : "VANJSKI TISAK - POSLAN ZAHTJEV ZA PONUDU",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GRF",
                        "naziv" : "Grafički odjel"
                    },
                    "sifra" : "IS16408758622800674",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 6600,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Prodaja",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 50
                }, 
                {
                    "naziv" : " VANJSKI TISAK - TISKARA TREBA DOSTAVITI TISAK",
                    "is_work" : false,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SKL",
                        "naziv" : "Skladište"
                    },
                    "sifra" : "IS16457183198999583",
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 12300,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Skladište",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 60
                }, 
                {
                    "naziv" : "VANJSKI TISAK - DOSTAVA NAŠEG MATERIJALA/PROIZVODA PREMA TISKARI U TOKU",
                    "work_done_for" : "IS16408759310586194",
                    "is_work" : true,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "DOS",
                        "naziv" : "Dostava"
                    },
                    "sifra" : "IS1640876067791467",
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : 5,
                    "status_num" : 7000,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Dostava",
                    "active_status" : true,
                    "work_done_for_naziv" : "VANJSKI TISAK - POTREBNO POSLATI NAŠ MATERIJAL/PROIZVOD",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 90
                }, 
                {
                    "naziv" : " VANJSKI TISAK - DOSTAVLJEN NAŠ MATERIJAL/PROIZVOD U TISKARU",
                    "work_done_for" : "IS1640876067791467",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "DOS",
                        "naziv" : "Dostava"
                    },
                    "sifra" : "IS16408761268092954",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 7100,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Dostava",
                    "active_status" : true,
                    "work_done_for_naziv" : "VANJSKI TISAK - DOSTAVA NAŠEG MATERIJALA/PROIZVODA PREMA TISKARI U TOKU",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 110
                }, 
                {
                    "naziv" : "VANJSKI TISAK - POTREBNO PLATITI",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "FIN",
                        "naziv" : "Financije"
                    },
                    "sifra" : "IS16408762469886045",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 7200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Financije",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 130
                }, 
                {
                    "naziv" : "VANJSKI TISAK - PLAĆENO",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "FIN",
                        "naziv" : "Financije"
                    },
                    "sifra" : "IS16408762938534358",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 7300,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Financije",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 150
                }, 
                {
                    "naziv" : "VANJSKI TISAK - GOTOV !!! POTREBNO POKUPITI IZ TISKARE",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "DOS",
                        "naziv" : "Dostava"
                    },
                    "sifra" : "IS16408763431112444",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 7400,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Dostava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 170
                }, 
                {
                    "naziv" : "VANJSKI TISAK - DOSTAVLJEN IZ TISKARE (PRIMKA)",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SAL",
                        "naziv" : "Prodaja"
                    },
                    "sifra" : "IS16408763641705076",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 7500,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Prodaja",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 190
                }
            ],
            "grana_finish" : [ 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : ""
                }, 
                {
                    "naziv" : "VANJSKI TISAK - TISAK JE OVJEREN, DOSTAVLJEN I SPREMAN ZA PROIZVODNJU",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GRF",
                        "naziv" : "Grafički odjel"
                    },
                    "sifra" : "IS16457314629093555",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 12600,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Grafički odjel",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 100
                }
            ],
            "def_deadline" : null,
            "status_num" : 6500,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "VANJSKI TISAK - POSLAN ZAHTJEV ZA PONUDU",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GRF",
                "naziv" : "Grafički odjel"
            },
            "sifra" : "IS16408758622800674",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 6600,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Grafički odjel",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "VANJSKI TISAK - ODOBRENA PONUDA",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GRF",
                "naziv" : "Grafički odjel"
            },
            "sifra" : "IS16408758954732935",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 6700,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Grafički odjel",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "VANJSKI TISAK - POTREBNO POSLATI NAŠ MATERIJAL/PROIZVOD",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GRF",
                "naziv" : "Grafički odjel"
            },
            "sifra" : "IS16408759310586194",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 6800,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Grafički odjel",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "VANJSKI TISAK - DOSTAVA NAŠEG MATERIJALA/PROIZVODA PREMA TISKARI U TOKU",
            "work_done_for" : "IS16408759310586194",
            "is_work" : true,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "DOS",
                "naziv" : "Dostava"
            },
            "sifra" : "IS1640876067791467",
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : 5,
            "status_num" : 7000,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Dostava",
            "active_status" : true,
            "work_done_for_naziv" : "VANJSKI TISAK - POTREBNO POSLATI NAŠ MATERIJAL/PROIZVOD",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : " VANJSKI TISAK - DOSTAVLJEN NAŠ MATERIJAL/PROIZVOD U TISKARU",
            "work_done_for" : "IS1640876067791467",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "DOS",
                "naziv" : "Dostava"
            },
            "sifra" : "IS16408761268092954",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 7100,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Dostava",
            "active_status" : true,
            "work_done_for_naziv" : "VANJSKI TISAK - DOSTAVA NAŠEG MATERIJALA/PROIZVODA PREMA TISKARI U TOKU",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "VANJSKI TISAK - POTREBNO PLATITI",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "FIN",
                "naziv" : "Financije"
            },
            "sifra" : "IS16408762469886045",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 7200,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Financije",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "VANJSKI TISAK - PLAĆENO",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "FIN",
                "naziv" : "Financije"
            },
            "sifra" : "IS16408762938534358",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 7300,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Financije",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "VANJSKI TISAK - GOTOV !!! POTREBNO POKUPITI IZ TISKARE",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "DOS",
                "naziv" : "Dostava"
            },
            "sifra" : "IS16408763431112444",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 7400,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Dostava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "VANJSKI TISAK - DOSTAVLJEN IZ TISKARE (PRIMKA)",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16408763641705076",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 7500,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "VANJSKI TISAK - FOLIO TISAK NAKON PROIZVODNJE",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16408764107750933",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [ 
                {
                    "naziv" : "VANJSKI TISAK - POSLAN ZAHTJEV ZA PONUDU",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GRF",
                        "naziv" : "Grafički odjel"
                    },
                    "sifra" : "IS16408758622800674",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 6600,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Grafički odjel",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 10
                }, 
                {
                    "naziv" : "VANJSKI TISAK - ODOBRENA PONUDA",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GRF",
                        "naziv" : "Grafički odjel"
                    },
                    "sifra" : "IS16408758954732935",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 6700,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Grafički odjel",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 20
                }, 
                {
                    "naziv" : "VANJSKI TISAK - POTREBNO POSLATI NAŠ MATERIJAL/PROIZVOD",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GRF",
                        "naziv" : "Grafički odjel"
                    },
                    "sifra" : "IS16408759310586194",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 6800,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Grafički odjel",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 30
                }
            ],
            "grana_finish" : [ 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : ""
                }
            ],
            "def_deadline" : null,
            "status_num" : 7600,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "POTVRDA NK",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16408765082680852",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 7700,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "KUPAC PLATIO AVANS",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16408765462414514",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 7800,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "REZERVACIJA SIROVINE PO PONUDI",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16409410886890088",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 7900,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "status_num" : 8000,
            "naziv" : "PROBLEM STROJ",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO",
                "naziv" : "Proizvodnja/Work"
            },
            "sifra" : "IS1641804595136842",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Work",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "SERVIS STROJA U TOKU",
            "def_deadline" : null,
            "is_work" : true,
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO",
                "naziv" : "Proizvodnja/Work"
            },
            "sifra" : "IS16418046310852476",
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 8100,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Work",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "SERVIS STROJA GOTOV",
            "work_done_for" : "IS16418046310852476",
            "is_work" : false,
            "def_deadline" : null,
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO",
                "naziv" : "Proizvodnja/Work"
            },
            "sifra" : "IS1641804676056579",
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 8200,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Work",
            "active_status" : true,
            "work_done_for_naziv" : "SERVIS STROJA U TOKU",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "PROBLEM OSOBLJA",
            "status_num" : 8300,
            "def_deadline" : null,
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO",
                "naziv" : "Proizvodnja/Work"
            },
            "sifra" : "IS16418047377375376",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Work",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "PROBLEM OSOBLJA SE RJEŠAVA",
            "def_deadline" : null,
            "is_work" : true,
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO",
                "naziv" : "Proizvodnja/Work"
            },
            "sifra" : "IS16418047846511135",
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 8400,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Work",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "work_done_for" : "IS16418047846511135",
            "naziv" : "PROBLEM OSOBLJA JE RJEŠEN",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO",
                "naziv" : "Proizvodnja/Work"
            },
            "sifra" : "IS16418048176687437",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 8500,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Work",
            "active_status" : true,
            "work_done_for_naziv" : "PROBLEM OSOBLJA SE RJEŠAVA",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "GREŠKA U NALOGU",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO",
                "naziv" : "Proizvodnja/Work"
            },
            "sifra" : "IS1641804940312613",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 8600,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Work",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "GREŠKA U NALOGU SE ISPRAVLJA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "is_work" : true,
            "dep" : {
                "sifra" : "PRO",
                "naziv" : "Proizvodnja/Work"
            },
            "sifra" : "IS16418050178509492",
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 8700,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Work",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "GREŠKA U NALOGU ISPRAVLJENA",
            "work_done_for" : "IS16418050178509492",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO",
                "naziv" : "Proizvodnja/Work"
            },
            "sifra" : "IS16418050525633677",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 8800,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Work",
            "active_status" : true,
            "work_done_for_naziv" : "GREŠKA U NALOGU SE ISPRAVLJA",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "KOREKCIJE PONUDE GOTOVE",
            "work_done_for" : "IS16341217951720005",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16418150832797854",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 8900,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for_naziv" : "RADIM KOREKCIJE PONUDE",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "POSLANA POTVRDA NARUDŽBE KUPCA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS1641815205402297",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 9000,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "REZERVACIJA ROBE PO PONUDI KUPCA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16424230681369185",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [ 
                {
                    "naziv" : "REZERVACIJA ROBE PO NARUDŽBI KUPCA",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SAL",
                        "naziv" : "Prodaja"
                    },
                    "sifra" : "IS16424231019669587",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 9200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Prodaja",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 20
                }, 
                {
                    "naziv" : "ODUSTAJEMO OD OVE PONUDE I NARUDŽBE",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SAL",
                        "naziv" : "Prodaja"
                    },
                    "sifra" : "IS1642423151409516",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 9300,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Prodaja",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 30
                }, 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : ""
                }
            ],
            "status_num" : 9100,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "REZERVACIJA ROBE PO NARUDŽBI KUPCA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16424231019669587",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 9200,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "ODUSTAJMO OD OVE PONUDE I NARUDŽBE",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS1642423151409516",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 9300,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "status_num" : 9400,
            "naziv" : "SPREMNO ZA PROIZVODNJU",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO2",
                "naziv" : "Proizvodnja/Office"
            },
            "sifra" : "IS1643037722002528",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [ 
                {
                    "naziv" : "PROIZVODNJA - RADNI NALOG",
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "PRO2",
                        "naziv" : "Proizvodnja/Office"
                    },
                    "sifra" : "IS16430999439082283",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 10000,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Proizvodnja/Office",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 20
                }
            ],
            "grana_finish" : [ 
                {
                    "naziv" : "PROIZVODNJA GOTOVA",
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "PRO2",
                        "naziv" : "Proizvodnja/Office"
                    },
                    "sifra" : "IS1643037781843246",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 9600,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Proizvodnja/Office",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 20
                }, 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : ""
                }
            ],
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Office",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "PROIZVODNJA POČELA",
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO2",
                "naziv" : "Proizvodnja/Office"
            },
            "sifra" : "IS16430377452435347",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 9500,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Office",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "PROIZVODNJA GOTOVA",
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO2",
                "naziv" : "Proizvodnja/Office"
            },
            "sifra" : "IS1643037781843246",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 9600,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Office",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "PROIZVODNI PROCES - RADNI NALOG",
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO",
                "naziv" : "Proizvodnja/Work"
            },
            "sifra" : "IS16430378755748118",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [ 
                {
                    "naziv" : "PROIZVODNI PROCES - ZAPOČET",
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "PRO",
                        "naziv" : "Proizvodnja/Work"
                    },
                    "sifra" : "IS16430378920956416",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 9800,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Proizvodnja/Work",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 20
                }, 
                {
                    "naziv" : "PROBLEM OSOBLJA",
                    "status_num" : 8300,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "PRO",
                        "naziv" : "Proizvodnja/Work"
                    },
                    "sifra" : "IS16418047377375376",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Proizvodnja/Work",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 40
                }, 
                {
                    "work_done_for" : "IS16418047846511135",
                    "naziv" : "PROBLEM OSOBLJA JE RJEŠEN",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "PRO",
                        "naziv" : "Proizvodnja/Work"
                    },
                    "sifra" : "IS16418048176687437",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 8500,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Proizvodnja/Work",
                    "active_status" : true,
                    "work_done_for_naziv" : "PROBLEM OSOBLJA SE RJEŠAVA",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 60
                }, 
                {
                    "status_num" : 8000,
                    "naziv" : "PROBLEM STROJ",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "PRO",
                        "naziv" : "Proizvodnja/Work"
                    },
                    "sifra" : "IS1641804595136842",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Proizvodnja/Work",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 80
                }, 
                {
                    "naziv" : "SERVIS STROJA U TOKU",
                    "def_deadline" : null,
                    "is_work" : true,
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "PRO",
                        "naziv" : "Proizvodnja/Work"
                    },
                    "sifra" : "IS16418046310852476",
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 8100,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Proizvodnja/Work",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 100
                }, 
                {
                    "naziv" : "SERVIS STROJA GOTOV",
                    "work_done_for" : "IS16418046310852476",
                    "is_work" : false,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "PRO",
                        "naziv" : "Proizvodnja/Work"
                    },
                    "sifra" : "IS1641804676056579",
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 8200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Proizvodnja/Work",
                    "active_status" : true,
                    "work_done_for_naziv" : "SERVIS STROJA U TOKU",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 120
                }, 
                {
                    "naziv" : "PROBLEM OSOBLJA SE RJEŠAVA",
                    "def_deadline" : null,
                    "is_work" : true,
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "PRO",
                        "naziv" : "Proizvodnja/Work"
                    },
                    "sifra" : "IS16418047846511135",
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 8400,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Proizvodnja/Work",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 140
                }, 
                {
                    "naziv" : "GREŠKA U NALOGU",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "PRO",
                        "naziv" : "Proizvodnja/Work"
                    },
                    "sifra" : "IS1641804940312613",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 8600,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Proizvodnja/Work",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 160
                }, 
                {
                    "naziv" : "GREŠKA U NALOGU SE ISPRAVLJA",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "is_work" : true,
                    "dep" : {
                        "sifra" : "PRO",
                        "naziv" : "Proizvodnja/Work"
                    },
                    "sifra" : "IS16418050178509492",
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 8700,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Proizvodnja/Work",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 180
                }, 
                {
                    "naziv" : "GREŠKA U NALOGU ISPRAVLJENA",
                    "work_done_for" : "IS16418050178509492",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "PRO",
                        "naziv" : "Proizvodnja/Work"
                    },
                    "sifra" : "IS16418050525633677",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 8800,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Proizvodnja/Work",
                    "active_status" : true,
                    "work_done_for_naziv" : "GREŠKA U NALOGU SE ISPRAVLJA",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 200
                }, 
                {
                    "naziv" : "DRUGI ZADATAK",
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "PRO",
                        "naziv" : "Proizvodnja/Work"
                    },
                    "sifra" : "IS16450938913273772",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 11200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Proizvodnja/Work",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 230
                }, 
                {
                    "naziv" : "PROBLEM U RADU",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "PRO",
                        "naziv" : "Proizvodnja/Work"
                    },
                    "sifra" : "IS16451097477878032",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 11300,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Proizvodnja/Work",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 250
                }, 
                {
                    "naziv" : "RJEŠENJE RADNOG PROBLEMA U TOKU",
                    "def_deadline" : 8,
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "PRO",
                        "naziv" : "Proizvodnja/Work"
                    },
                    "sifra" : "IS1645109784056882",
                    "is_work" : true,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 11400,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Proizvodnja/Work",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 270
                }, 
                {
                    "naziv" : "PROBLEM U RADU JE OTKLONJEN",
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "PRO",
                        "naziv" : "Proizvodnja/Work"
                    },
                    "sifra" : "IS16451098032009983",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 11500,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Proizvodnja/Work",
                    "active_status" : true,
                    "work_done_for" : "IS1645109784056882",
                    "work_done_for_naziv" : "RJEŠENJE RADNOG PROBLEMA U TOKU",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 290
                }, 
                {
                    "naziv" : "PITANJE",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "sifra" : "IS16457184141333354",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 12400,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 320
                }, 
                {
                    "naziv" : "ODGOVOR",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "sifra" : "IS1645718439670303",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 12500,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 350
                }, 
                {
                    "naziv" : " PROIZVODNI PROCES - GOTOV",
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "PRO",
                        "naziv" : "Proizvodnja/Work"
                    },
                    "sifra" : "IS1643038121299174",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 9900,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Proizvodnja/Work",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 370
                }, 
                {
                    "naziv" : "PROIZVODNI PROCES - DOSTAVITI ROBU ZA PROIZVODNJU",
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "SKL",
                        "naziv" : "Skladište"
                    },
                    "sifra" : "IS1646045305725667",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 13300,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Skladište",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 400
                }
            ],
            "grana_finish" : [ 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : ""
                }, 
                {
                    "naziv" : "ODUSTAJEMO OD OVE GRANE",
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "sifra" : "IS16341239747891487",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 1200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "button_edit" : true,
                    "next_status_num" : 80,
                    "work_finished" : null,
                    "work_done_for" : null,
                    "work_done_for_naziv" : ""
                }, 
                {
                    "naziv" : "PROIZVODNI PROCES - PROVJEREN I POTVRĐEN",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "PRO",
                        "naziv" : "Proizvodnja/Work"
                    },
                    "sifra" : "IS16460382522280327",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 13200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Proizvodnja/Work",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 120
                }
            ],
            "def_deadline" : null,
            "status_num" : 9700,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Work",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "PROIZVODNI PROCES - ZAPOČET",
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO",
                "naziv" : "Proizvodnja/Work"
            },
            "sifra" : "IS16430378920956416",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 9800,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Work",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : " PROIZVODNI PROCES - GOTOV",
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO",
                "naziv" : "Proizvodnja/Work"
            },
            "sifra" : "IS1643038121299174",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 9900,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Work",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "PROIZVODNJA - RADNI NALOG",
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO2",
                "naziv" : "Proizvodnja/Office"
            },
            "sifra" : "IS16430999439082283",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 10000,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Office",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "PROMJENA DATUMA ROBE",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16432826880204646",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 10100,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "PREUZIMAM ZADATAK",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GRF",
                "naziv" : "Grafički odjel"
            },
            "sifra" : "IS16442324791051643",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 10200,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Grafički odjel",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "ZAHTJEV ZA PROVJERU GP (VANJSKA USLUGA)",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GRF",
                "naziv" : "Grafički odjel"
            },
            "sifra" : "IS16445778047049382",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [ 
                {
                    "naziv" : "MOLIM PONUDU ZA GP (VANJSKA USLUGA)",
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GRF",
                        "naziv" : "Grafički odjel"
                    },
                    "sifra" : "IS16445778835512053",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 10400,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Grafički odjel",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 20
                }, 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : ""
                }
            ],
            "status_num" : 10300,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Grafički odjel",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "MOLIM PONUDU ZA GP (VANJSKA USLUGA)",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GRF",
                "naziv" : "Grafički odjel"
            },
            "sifra" : "IS16445778835512053",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 10400,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Grafički odjel",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "MOLIM RENDER",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GRF",
                "naziv" : "Grafički odjel"
            },
            "sifra" : "IS16449257682396394",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [ 
                {
                    "naziv" : "IZRADA RENDERA U TOKU",
                    "def_deadline" : 2,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "is_work" : true,
                    "work_done_for" : null,
                    "dep" : {
                        "sifra" : "GRF",
                        "naziv" : "Grafički odjel"
                    },
                    "sifra" : "IS16449258070026118",
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 10600,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Grafički odjel",
                    "active_status" : true,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 30
                }
            ],
            "grana_finish" : [ 
                {
                    "naziv" : "RENDER GOTOV",
                    "work_done_for" : "IS16449258070026118",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GRF",
                        "naziv" : "Grafički odjel"
                    },
                    "sifra" : "IS1644925857596595",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 10700,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PRED-PROIZVODNJA",
                    "dep_naziv" : "Grafički odjel",
                    "active_status" : true,
                    "work_done_for_naziv" : "IZRADA RENDERA U TOKU",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 30
                }, 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : ""
                }
            ],
            "status_num" : 10500,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Grafički odjel",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "IZRADA RENDERA U TOKU",
            "def_deadline" : 2,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "is_work" : true,
            "work_done_for" : null,
            "dep" : {
                "sifra" : "GRF",
                "naziv" : "Grafički odjel"
            },
            "sifra" : "IS16449258070026118",
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 10600,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Grafički odjel",
            "active_status" : true,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "RENDER GOTOV",
            "work_done_for" : "IS16449258070026118",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GRF",
                "naziv" : "Grafički odjel"
            },
            "sifra" : "IS1644925857596595",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 10700,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Grafički odjel",
            "active_status" : true,
            "work_done_for_naziv" : "IZRADA RENDERA U TOKU",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "GOTOV NACRT ZA PROIZVODNJU",
            "def_deadline" : null,
            "work_done_for" : "IS16408750871766353",
            "dep" : {
                "sifra" : "CAD",
                "naziv" : "Razvoj"
            },
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "sifra" : "IS1644926143514824",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 10800,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Razvoj",
            "active_status" : true,
            "work_done_for_naziv" : "RAD NA NACRTU ZA PROIZVODNJU U TOKU",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "KUPAC MOŽE PLATITI S ODGODOM",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS1644927131777135",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 10900,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "KUPAC PLAĆA PO AVANSU",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS1644927239058997",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 11000,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "KUPAC PLATIO U VALUTI",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SAL",
                "naziv" : "Prodaja"
            },
            "sifra" : "IS16449272784917998",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 11100,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Prodaja",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "DRUGI ZADATAK",
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO",
                "naziv" : "Proizvodnja/Work"
            },
            "sifra" : "IS16450938913273772",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 11200,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Work",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "PROBLEM U RADU",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO",
                "naziv" : "Proizvodnja/Work"
            },
            "sifra" : "IS16451097477878032",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 11300,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Work",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "RJEŠENJE RADNOG PROBLEMA U TOKU",
            "def_deadline" : 8,
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO",
                "naziv" : "Proizvodnja/Work"
            },
            "sifra" : "IS1645109784056882",
            "is_work" : true,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 11400,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Work",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "PROBLEM U RADU JE OTKLONJEN",
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO",
                "naziv" : "Proizvodnja/Work"
            },
            "sifra" : "IS16451098032009983",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 11500,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Work",
            "active_status" : true,
            "work_done_for" : "IS1645109784056882",
            "work_done_for_naziv" : "RJEŠENJE RADNOG PROBLEMA U TOKU",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "GREŠKA RADNIKA (ne koristim za sada)",
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO",
                "naziv" : "Proizvodnja/Work"
            },
            "sifra" : "IS16456241190074204",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 11600,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Work",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "LJUDSKA GREŠKA - POTREBNA DODATNA EDUKACIJA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GEN",
                "naziv" : "Općenito"
            },
            "sifra" : "IS16456245409441008",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [ 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : ""
                }, 
                {
                    "naziv" : "GREŠKA JE EVIDENTIRANA / ISPRAVLJENA",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "sifra" : "IS16457174821005674",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 12200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 100
                }
            ],
            "status_num" : 11700,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Općenito",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "SISTEMSKA GREŠKA",
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GEN",
                "naziv" : "Općenito"
            },
            "sifra" : "IS16456245528614177",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [ 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : ""
                }, 
                {
                    "naziv" : "GREŠKA JE EVIDENTIRANA / ISPRAVLJENA",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "sifra" : "IS16457174821005674",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 12200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 100
                }
            ],
            "def_deadline" : null,
            "status_num" : 11800,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Općenito",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "LJUDSKA GREŠKA KOJA SE NE MOZE RIJEŠITI DODATNOM PROCEDUROM",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GEN",
                "naziv" : "Općenito"
            },
            "sifra" : "IS1645624568705983",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [ 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : ""
                }, 
                {
                    "naziv" : "GREŠKA JE EVIDENTIRANA / ISPRAVLJENA",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "sifra" : "IS16457174821005674",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 12200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 100
                }
            ],
            "status_num" : 11900,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Općenito",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "GREŠKA DOBAVLJAČA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GEN",
                "naziv" : "Općenito"
            },
            "sifra" : "IS1645624588033259",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [ 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : ""
                }, 
                {
                    "naziv" : "GREŠKA JE EVIDENTIRANA / ISPRAVLJENA",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "sifra" : "IS16457174821005674",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 12200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 100
                }
            ],
            "status_num" : 12000,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Općenito",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "GREŠKA KUPCA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GEN",
                "naziv" : "Općenito"
            },
            "sifra" : "IS1645624601434254",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [ 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : ""
                }, 
                {
                    "naziv" : "GREŠKA JE EVIDENTIRANA / ISPRAVLJENA",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "prod",
                        "naziv" : "PROIZVODNJA"
                    },
                    "dep" : {
                        "sifra" : "GEN",
                        "naziv" : "Općenito"
                    },
                    "sifra" : "IS16457174821005674",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 12200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "PROIZVODNJA",
                    "dep_naziv" : "Općenito",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 100
                }
            ],
            "status_num" : 12100,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Općenito",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "GREŠKA JE EVIDENTIRANA / ISPRAVLJENA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GEN",
                "naziv" : "Općenito"
            },
            "sifra" : "IS16457174821005674",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 12200,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Općenito",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : " VANJSKI TISAK - TISKARA TREBA DOSTAVITI TISAK",
            "is_work" : false,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SKL",
                "naziv" : "Skladište"
            },
            "sifra" : "IS16457183198999583",
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 12300,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Skladište",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "PITANJE",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GEN",
                "naziv" : "Općenito"
            },
            "sifra" : "IS16457184141333354",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 12400,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Općenito",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "ODGOVOR",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GEN",
                "naziv" : "Općenito"
            },
            "sifra" : "IS1645718439670303",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 12500,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Općenito",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "VANJSKI TISAK - TISAK JE OVJEREN, DOSTAVLJEN I SPREMAN ZA PROIZVODNJU",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "GRF",
                "naziv" : "Grafički odjel"
            },
            "sifra" : "IS16457314629093555",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 12600,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Grafički odjel",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "ALAT - UPISAN U PROGRAM (BAZU)",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS1645795101306565",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [ 
                {
                    "status_num" : 4000,
                    "naziv" : "ALAT - U IZRADI",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16408636668597007",
                    "is_work" : true,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 10
                }, 
                {
                    "naziv" : " ALAT - IZRADA JE GOTOVA",
                    "work_done_for" : "IS16408636668597007",
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16457955600397222",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 12900,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for_naziv" : "ALAT - U IZRADI",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 20
                }, 
                {
                    "naziv" : "ALAT - POTREBNO DOPREMITI OD DOBAVLJAČA",
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16408638975663738",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 4800,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 30
                }, 
                {
                    "naziv" : "ALAT - DOSTAVLJEN OD DOBAVLJAČA",
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16408639465263362",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 4900,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 40
                }, 
                {
                    "naziv" : "ALAT - IZRADA KOD DOBAVLJAČA",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16457959231872275",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 13000,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 50
                }, 
                {
                    "naziv" : "ALAT - U POPRAVKU",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16408638050153875",
                    "is_work" : true,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 4700,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 60
                }, 
                {
                    "naziv" : "ALAT - POPRAVAK JE GOTOV",
                    "def_deadline" : null,
                    "work_done_for" : "IS16408638050153875",
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16457951564526064",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 12800,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for_naziv" : "ALAT - U POPRAVKU",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 70
                }, 
                {
                    "naziv" : "ALAT - POSUĐEN",
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16408637413651423",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 4400,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 80
                }, 
                {
                    "naziv" : "ALAT - U RADU PREKO RN",
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16408637911745757",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "status_num" : 4600,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 90
                }, 
                {
                    "naziv" : "ALAT - VRAĆEN KUPCU",
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS164086370575651",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 4200,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 100
                }
            ],
            "grana_finish" : [ 
                {
                    "sifra" : "ISerror",
                    "active" : true,
                    "next_status_num" : 50,
                    "naziv" : "GREŠKA UPISA",
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "is_work" : null,
                    "def_deadline" : null,
                    "cat" : {
                        "sifra" : "preprod",
                        "naziv" : "PRED-PROIZVODNJA"
                    },
                    "next_statuses_string" : "",
                    "grana_finish_string" : ""
                }, 
                {
                    "status_num" : 4500,
                    "naziv" : "ALAT - NA STANJU",
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS16408637769429497",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 100
                }, 
                {
                    "naziv" : "ALAT - UNIŠTEN",
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS1640863723452965",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 4300,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 150
                }, 
                {
                    "naziv" : " ALAT - STORNO (odustali od izrade)",
                    "cat" : {
                        "sifra" : "nabava",
                        "naziv" : "REGULARNA NABAVA"
                    },
                    "dep" : {
                        "sifra" : "NAB",
                        "naziv" : "Nabava"
                    },
                    "sifra" : "IS1651746059604391",
                    "is_work" : null,
                    "active" : true,
                    "next_statuses" : [],
                    "grana_finish" : [],
                    "def_deadline" : null,
                    "status_num" : 13600,
                    "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
                    "cat_naziv" : "REGULARNA NABAVA",
                    "dep_naziv" : "Nabava",
                    "active_status" : true,
                    "work_done_for" : null,
                    "work_done_for_naziv" : "",
                    "work_finished" : null,
                    "button_edit" : true,
                    "next_status_num" : 200
                }
            ],
            "status_num" : 12700,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "ALAT - POPRAVAK JE GOTOV",
            "def_deadline" : null,
            "work_done_for" : "IS16408638050153875",
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16457951564526064",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 12800,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for_naziv" : "ALAT - U POPRAVKU",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : " ALAT - IZRADA JE GOTOVA",
            "work_done_for" : "IS16408636668597007",
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16457955600397222",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 12900,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for_naziv" : "ALAT - U IZRADI",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "ALAT - IZRADA KOD DOBAVLJAČA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16457959231872275",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 13000,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "ALAT TRENUTNO NIJE RASPOLOŽIV",
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS1645796412814405",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 13100,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "PROIZVODNI PROCES - PROVJEREN I POTVRĐEN",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "PRO",
                "naziv" : "Proizvodnja/Work"
            },
            "sifra" : "IS16460382522280327",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 13200,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Proizvodnja/Work",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "PROIZVODNI PROCES - DOSTAVITI ROBU ZA PROIZVODNJU",
            "cat" : {
                "sifra" : "prod",
                "naziv" : "PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "SKL",
                "naziv" : "Skladište"
            },
            "sifra" : "IS1646045305725667",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 13300,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PROIZVODNJA",
            "dep_naziv" : "Skladište",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "PREBACUJEM ZADATAK NA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "CAD",
                "naziv" : "Razvoj"
            },
            "sifra" : "IS16464013184163152",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 13400,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Razvoj",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "MOLIM PROVJERU I ODOBRENJE PONUDE",
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "UPR",
                "naziv" : "Uprava"
            },
            "sifra" : "IS16468367857771187",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 13500,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Uprava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : " ALAT - STORNO (odustali od izrade)",
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS1651746059604391",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "def_deadline" : null,
            "status_num" : 13600,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "NABAVA - MOLIM ODOBRENJE PONUDE DOBAVLJAČA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16548497358280305",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 13700,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "NABAVA - POTREBNO PLATITI ROBU",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16557202880964136",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 13800,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "NABAVA - PRIMKA JE PROKNJIŽENA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16557203449509055",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 13900,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "status_num" : 14000,
            "naziv" : "NABAVA - SIROVINA PROKNJIŽENA",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "preprod",
                "naziv" : "PRED-PROIZVODNJA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16559800003961738",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana \" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "PRED-PROIZVODNJA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }, 
        {
            "naziv" : "NABAVA - POTREBNO PLATITI PREDUJAM",
            "def_deadline" : null,
            "cat" : {
                "sifra" : "nabava",
                "naziv" : "REGULARNA NABAVA"
            },
            "dep" : {
                "sifra" : "NAB",
                "naziv" : "Nabava"
            },
            "sifra" : "IS16560675376649348",
            "is_work" : null,
            "active" : true,
            "next_statuses" : [],
            "grana_finish" : [],
            "status_num" : 14100,
            "nova_grana" : "<button class=\"blue_btn btn_small_text add_nova_grana cit_disable\" style=\"margin: 0 auto; padding: 0 20px; height: 22px; width: auto; box-shadow: none;\">\n          <i class=\"fas fa-plus\" style=\"margin-right: 10px;\" ></i> NOVA GRANA\n        </button>",
            "cat_naziv" : "REGULARNA NABAVA",
            "dep_naziv" : "Nabava",
            "active_status" : true,
            "work_done_for" : null,
            "work_done_for_naziv" : "",
            "work_finished" : null,
            "button_edit" : true,
            "next_status_num" : null
        }
    ]
}