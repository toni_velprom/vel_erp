// ==UserScript==
// @name         sud reg
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://sudreg.pravosudje.hr/registar/*
// @icon         https://www.google.com/s2/favicons?domain=google.com
// @grant        GM.setClipboard
// @require      https://code.jquery.com/jquery-2.1.4.min.js
// ==/UserScript==

(function () {
  'use strict';

  var interval_id = null;

  var time = localStorage.getItem('time');
  console.log(time);


  if (!time && window.location.href.indexOf(`_MBS`) == -1 ) {

    localStorage.setItem('time', String(Date.now()));

    setTimeout( async function () {

      var clip_text = await navigator.clipboard.readText();

      $("#P1_NAZIV").val(clip_text);
      $("input[value='Pretraži']").trigger("click");
    }, 1000);

  } else {
    
    
    var time_after = localStorage.getItem('time');
    // ako se u url nalazi _MBS to znači da je to stranica razultata
    // ako nemam time iz local storega to znači da sam direktno došao na ovu stranicu
    // tj nisam došao putem pretrage nego vjerojatno putem linka za MBS
    if ( !time_after && window.location.href.indexOf(`_MBS`) > -1 ) {

      return;
    };

    localStorage.removeItem('time');

    var proteklo_vrijeme = 100;

    interval_id = setInterval(function () {

      proteklo_vrijeme += 100;

      if ($(".report-standard").length > 0) {

        GM.setClipboard(String($(".report-standard")[0].outerHTML));
        // alert(String($(".report-standard")[0].outerHTML));
        clearInterval(interval_id);
        window.opener.focus();

      } else if (proteklo_vrijeme > 3000) {
        clearInterval(interval_id);
        alert("Nisam uspio dobiti podatke unutar 3 sekunde");


      };

    }, 100);

    // clearInterval(interval_id);

  };
})();
