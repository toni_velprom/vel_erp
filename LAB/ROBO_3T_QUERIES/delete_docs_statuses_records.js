db.getCollection('sirovine').updateMany(
{"records.0": { $exists: true }  },
{ $set: { records: [] } } )

db.getCollection('sirovine').updateMany(
{"statuses.0": { $exists: true }  },
{ $set: { statuses: [] } } )

db.getCollection('partners').updateMany(
{"docs.0": { $exists: true }  },
{ $set: { docs: [] } } )

db.getCollection('partners').updateMany(
{"statuses.0": { $exists: true }  },
{ $set: { statuses: [] } } )

db.getCollection('products').updateMany(
{"statuses.0": { $exists: true }  },
{ $set: { statuses: [] } } )

db.getCollection('projects').updateMany(
{"proj_specs.0": { $exists: true }  },
{ $set: { proj_specs: [] } } )
