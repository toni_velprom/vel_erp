db.getCollection('products').updateMany(
{},
{ $set: { statuses: [], prod_specs: [] } }
)

db.getCollection('kalkulacije').updateMany(
{},
{ $set: { pro_kalk: [], meta_pro_kalk: null, post_kalk: [], meta_post_kalk: null } }
)

db.getCollection('sirovine').updateMany(
{},
{ $set: { statuses: [], records: [], specs: [] } }
)


db.getCollection('kalkulacije').updateMany(
{ _id: ObjectId("61a89b3aa4166726ba03a428") },
{ $set: { "offer_kalk.1.kalk_reserv": false, "offer_kalk.1.kalk_for_pro": false } }
)
