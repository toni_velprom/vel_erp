  
radne_stanice: [
  
/*  

  { 
    "sifra": "DUM000", 
    "func": "dummy_work",
    "naziv": "PROMJENI !!! (prazni rad)", 
    "type": "DUMPROC000", 
    "proces_type_naziv": "Dummy proces (prazni rad)",
    "elem_sifra": "DUMELE000",
    "elem_naziv": "Dummy element (prazni rad)",
    "unit": "kom",
    "interval": "h",
    "speed": 0,
    "price": 0
  },
  
*/
  
// VANSJKE SLOTERE OBAVEZNO UBACITI !!!!!
  
  
{ "sifra": "DUM1", "func": "dummy_work", "naziv": "PROMJENI !!! Offset", "type": "DUMPROC1", "proces_type_naziv": "Dummy proces", "elem_sifra": "DUMELE1", "elem_naziv": "Dummy element", "unit": "kom", "interval": "h", "speed": 0, "price": 0 },
 { "sifra": "DUM2", "func": "dummy_work", "naziv": "PROMJENI !!! Folio", "type": "DUMPROC2", "proces_type_naziv": "Dummy proces", "elem_sifra": "DUMELE2", "elem_naziv": "Dummy element", "unit": "kom", "interval": "h", "speed": 0, "price": 0 },
 { "sifra": "DUM3", "func": "dummy_work", "naziv": "PROMJENI !!! DIGITAL Latex", "type": "DUMPROC3", "proces_type_naziv": "Dummy proces", "elem_sifra": "DUMELE3", "elem_naziv": "Dummy element", "unit": "kom", "interval": "h", "speed": 0, "price": 0 },
 { "sifra": "DUM4", "func": "dummy_work", "naziv": "PROMJENI !!! DIGITAL Fuji", "type": "DUMPROC4", "proces_type_naziv": "Dummy proces", "elem_sifra": "DUMELE4", "elem_naziv": "Dummy element", "unit": "kom", "interval": "h", "speed": 0, "price": 0 },

  
 { "sifra": "DUM6", "func": "dummy_work", "naziv": "PROMJENI !!! Digital", "type": "DUMPROC6", "proces_type_naziv": "Dummy proces", "elem_sifra": "DUMELE6", "elem_naziv": "Dummy element", "unit": "kom", "interval": "h", "speed": 0, "price": 0 },
 { "sifra": "DUM7", "func": "dummy_work", "naziv": "PROMJENI !!! Flexo", "type": "DUMPROC7", "proces_type_naziv": "Dummy proces", "elem_sifra": "DUMELE7", "elem_naziv": "Dummy element", "unit": "kom", "interval": "h", "speed": 0, "price": 0 },
 { "sifra": "DUM8", "func": "dummy_work", "naziv": "PROMJENI !!! Blinddruck podignuti", "type": "DUMPROC8", "proces_type_naziv": "Dummy proces", "elem_sifra": "DUMELE8", "elem_naziv": "Dummy element", "unit": "kom", "interval": "h", "speed": 0, "price": 0 },
 { "sifra": "DUM9", "func": "dummy_work", "naziv": "PROMJENI !!! Blinddruck uvučeni", "type": "DUMPROC9", "proces_type_naziv": "Dummy proces", "elem_sifra": "DUMELE9", "elem_naziv": "Dummy element", "unit": "kom", "interval": "h", "speed": 0, "price": 0 },
  
 { "sifra": "DUM10", "func": "dummy_work", "naziv": "PROMJENI !!! Sito", "type": "DUMPROC10", "proces_type_naziv": "Dummy proces", "elem_sifra": "DUMELE10", "elem_naziv": "Dummy element", "unit": "kom", "interval": "h", "speed": 0, "price": 0 },
  
  
  
{ 
  "sifra": "RADS10B", func: "sloter_work", "naziv": "Vanjski Sloter S TISKOM", 
  type: "PRC1SLOTER", "proces_type_naziv": "Vanjski sloter s tiskom", "elem_sifra": "SEL17", "elem_naziv": "Sloter element (vanjski s tiskom)",
  "unit": "kom", "interval": "h", "speed": 1000, price: 250
},
{ 
  "sifra": "RADS10C", func: "sloter_work", "naziv": "Vanjski Sloter BEZ TISKA ",
  type: "PRC17BSLOTER", "proces_type_naziv": "Vanjski sloter bez tiska", "elem_sifra": "SEL17SL", "elem_naziv": "Sloter element (vanjski bez tiska)",
  "unit": "kom", "interval": "h", "speed": 1000, price: 250 
},
  

{
  "sifra": "RADS13", func: "pakir_work", "naziv": "PAKIRANJE KUTIJA - složeno u bunt na paletu",
  type: "PRC13", "proces_type_naziv": "Pakiranje", "elem_sifra": "SEL30", "elem_naziv": "Upakirani proizvodi/elementi",
  "unit": "kom", "interval": "h", "speed": 250000, price: 250 
},  
{
  "sifra": "RADS14", func: "pakir_work", "naziv": "PAKIRANJE KUTIJA - omatanje mini strechom po 1 komad", 
  type: "PRC13", "proces_type_naziv": "Pakiranje", "elem_sifra": "SEL30", "elem_naziv": "Upakirani proizvodi/elementi",
  "unit": "kom", "interval": "h", "speed": 35000, price: 250
},
{
  "sifra": "RADS15", func: "pakir_work",  "naziv": "PAKIRANJE KUTIJA - u PVC crijevo po 50 komada", 
  type: "PRC13", "proces_type_naziv": "Pakiranje", "elem_sifra": "SEL30", "elem_naziv": "Upakirani proizvodi/elementi",
  "unit": "kom", "interval": "h", "speed": 12500, price: 250 
},
{ 
  "sifra": "RADS16", func: "pakir_work", "naziv": "PAKIRANJE KUTIJA - u kutiju",
  type: "PRC13", "proces_type_naziv": "Pakiranje", 
  "elem_sifra": "SEL30", "elem_naziv": "Upakirani proizvodi/elementi",
  "unit": "kom", "interval": "h", "speed": 125000, price: 250 
},
{
  "sifra": "RADS12", func: "sklad_work", "naziv": "SKLADIŠTENJE RUČNO",
  type: "PRC18", "proces_type_naziv": "Skladištenje", 
  "elem_sifra": "SEL32", "elem_naziv": "Skladišten proizvod/element",
  "unit": "kom", "interval": "h", "speed": 4000, price: 250 
},
{ 
  "sifra": "RADS100", func: "dostav_work", "naziv": "DOSTAVA", 
  type: "PRC14", "proces_type_naziv": "Dostava", "elem_sifra": "SEL31", "elem_naziv": "Dostavljen proizvod/element",
  "unit": "kom", "interval": "h", "speed": 1, price: 250 },

  
  /* -------------------------------- SCITEX -------------------------------- */

  {
    "sifra": "RADS2", 
    "naziv": "SCITEX - SAMPLE", 
    "func": "scitex_work",
    "type": "PRC17",
    "proces_type_naziv": "Unutranji Tisak",
    "elem_sifra": "SEL18", 
    "elem_naziv": "Otisnuti element (VELPROM tisak)",
    "unit": "kom", 
    "interval": "h",
    "speed": 32,
    "price": 1250,
  },
 
  {
    "sifra": "RADS3", "naziv": "SCITEX - TEXT", "unit": "kom", "interval": "h", "speed": 58, "price": 1250, 
    "func": "scitex_work", "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", 
    "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)" 
  },
 
  { 
    "sifra": "RADS4", "naziv": "SCITEX - FAST SAMPLE", "unit": "kom", "interval": "h", "speed": 65, "price": 1250, 
    "func": "scitex_work", "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", 
    "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)"
  },
 
  { 
    "sifra": "RADS5", "naziv": "SCITEX - HIGH QUALITY", "unit": "kom", "interval": "h", "speed": 78, "price": 1250, 
    "func": "scitex_work", "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", 
    "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)" 
  },
 
  { 
    "sifra": "RADS6", "naziv": "SCITEX - POP PRODUCTION", "unit": "kom", "interval": "h", "speed": 96, "price": 1250, 
    "func": "scitex_work", "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", 
    "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)" 
  },
 
  { 
    "sifra": "RADS7", "naziv": "SCITEX - PRODUCTION", "unit": "kom", "interval": "h", "speed": 113, "price": 1250,
    "func": "scitex_work", "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", 
    "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)"
  },
 
  { 
    "sifra": "RADS8", "naziv": "SCITEX - FAST PRODUCTION", "unit": "kom", "interval": "h", "speed": 127, "price": 1250, 
    "func": "scitex_work", "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", 
    "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)" 
  },
 
  { 
    "sifra": "RADS8B", "naziv": "SCITEX - MANULNO ULAGANJE", "unit": "kom", "interval": "h", "speed": 30, "price": 1250,
    "func": "scitex_work", "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", 
    "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)"
  },
  
/*  
  { 
    "sifra": "RADS8BDUMMY", "naziv": "PROMIJENI !!! DIGITAL PRINT SCITEX", 
    "unit": "kom", "interval": "h", "speed": 30, "price": 1250,
    "func": "scitex_work", "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", 
    "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)"
  },
  
*/
  
  /* -------------------------------- LATEX -------------------------------- */
  
 
  {
    "sifra": "RAD101", "naziv": "LATEX - MAX SPEED", "unit": "m2", "interval": "h", "speed": 91, "price": 10, 
    "func": "", "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", 
    "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)" 
  },
 
  { 
    "sifra": "RAD102", "naziv": "LATEX - OUTDOOR HIGH SPEED", "unit": "m2", "interval": "h", "speed": 31, "price": 10, 
    "func": "", "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", 
    "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)"
  },
 
  { "sifra": "RAD103", "naziv": "LATEX - OUTDOOR PLUS", "unit": "m2", "interval": "h", "speed": 23, "price": 10, "func": "",
 "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)" },
 
  { "sifra": "RAD104", "naziv": "LATEX - INDOOR QUALITY", "unit": "m2", "interval": "h", "speed": 17, "price": 10, "func": "",
 "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)" },
 
  { "sifra": "RAD105", "naziv": "LATEX - INDOOR HIGH QUALITY", "unit": "m2", "interval": "h", "speed": 14, "price": 10, "func": "",
 "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)" },
 
  { "sifra": "RAD106", "naziv": "LATEX - BACKLITS, TEXTILES AND CANVAS", "unit": "m2", "interval": "h", "speed": 6, "price": 10, "func": "",
 "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)" },
 
  { "sifra": "RAD107", "naziv": "LATEX - HIGH SATURATION TEXTILES", "unit": "m2", "interval": "h", "speed": 5, "price": 10, "func": "",
 "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)" },
 
  
/*  
  { 
    "sifra": "RAD107DUMMY", "naziv": "PROMIJENI !!! DIGITAL PRINT LATEX ", 
    "unit": "m2", "interval": "h", "speed": 5, "price": 10, 
    "func": "", "type": "PRC17", "proces_type_naziv": "Unutranji Tisak",
    "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)" 
  },
*/
  
  /* -------------------------------- FUJI -------------------------------- */
  
  
  
  { "sifra": "RAD108", "naziv": "FUJI - EXPRESS", "unit": "m2", "interval": "h", "speed": 33, "price": 10, "func": "",
 "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)" },
 
  { "sifra": "RAD109", "naziv": "FUJI - PRODUCTION", "unit": "m2", "interval": "h", "speed": 20, "price": 10, "func": "",
 "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)" },
 
  { "sifra": "RAD110", "naziv": "FUJI - STANDARD", "unit": "m2", "interval": "h", "speed": 13, "price": 10, "func": "",
 "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)" },
 
  { "sifra": "RAD111", "naziv": "FUJI - QUALITY", "unit": "m2", "interval": "h", "speed": 8.3, "price": 10, "func": "",
 "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)" },
 
  { "sifra": "RAD112", "naziv": "FUJI - HIGH QUALITY", "unit": "m2", "interval": "h", "speed": 4.2, "price": 10, "func": "",
 "type": "PRC17", "proces_type_naziv": "Unutranji Tisak", "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)" },
  
  
/*  
  {
    "sifra": "RAD112DUMMY", "naziv": "PROMIJENI !!! DIGITAL PRINT FUJI", 
    "unit": "m2", "interval": "h", "speed": 4.2, "price": 10, 
    "func": "", "type": "PRC17", "proces_type_naziv": "Unutranji Tisak",
    "elem_sifra": "SEL18", "elem_naziv": "Otisnuti element (VELPROM tisak)" 
  },
  
*/

 
  { "sifra": "RAD113", "naziv": "LASER - REZANJE", "unit": "m", "interval": "h", "speed": 2400, "price": 10, "func": "",
 "type": "PRC23", "proces_type_naziv": "Rezanje", "elem_sifra": "SEL38", "elem_naziv": "Rezani element" },
 
  { "sifra": "RAD114", "naziv": "LASER - GRAVIRANJE", "unit": "m", "interval": "h", "speed": 2400, "price": 10, "func": "",
 "type": "PRC23", "proces_type_naziv": "Rezanje", "elem_sifra": "SEL38", "elem_naziv": "Rezani element" },
 
  { "sifra": "RAD115", "naziv": "LASER - REZANJE I GRAVIRANJE", "unit": "m", "interval": "h", "speed": 2400, "price": 10, "func": "",
 "type": "PRC23", "proces_type_naziv": "Rezanje", "elem_sifra": "SEL38", "elem_naziv": "Rezani element" },
 
  { "sifra": "RAD116", "naziv": "CNC - REZANJE", "unit": "m", "interval": "h", "speed": 1800, "price": 10, "func": "",
 "type": "PRC23", "proces_type_naziv": "Rezanje", "elem_sifra": "SEL38", "elem_naziv": "Rezani element" },
 
  { "sifra": "RAD117", "naziv": "SAVIJANJE NA ŽICU - SAVIJANJE NA ŽICU", "unit": "kom", "interval": "h", "speed": 60, "price": 10, "func": "",
 "type": "PRC20", "proces_type_naziv": "Savijanje", "elem_sifra": "SEL35", "elem_naziv": "Savijeni element" },
 
  { "sifra": "RAD118", "naziv": "LAMINATOR - LAMINIRANJE", "unit": "kom", "interval": "h", "speed": 0, "price": 10, "func": "",
 "type": "PRC21", "proces_type_naziv": "Laminiranje", "elem_sifra": "SEL36", "elem_naziv": "Laminirani element" },
 
  { "sifra": "RAD119", "naziv": "POTEZNI NOŽ KEENCUT EVOLUTION E2 EV2310\n - REZANJE", "unit": "kom", "interval": "h", "speed": 60, "price": 10, "func": "",
 "type": "PRC23", "proces_type_naziv": "Rezanje", "elem_sifra": "SEL38", "elem_naziv": "Rezani element" },
 
  { "sifra": "RADS9", "naziv": "SLOTER GANDOSSI & FOSSATI GFM 180 - USJECANJE", "unit": "kom", "interval": "h", "speed": 1500, "price": 10, "func": "",
 "type": "PRC31", "proces_type_naziv": "Usjecanje/Foliotisak", "elem_sifra": "SEL46", "elem_naziv": "Sloter element s folio tiskom" },
 
  {
    "sifra": "RADS10", "naziv": "SLOTER GANDOSSI & FOSSATI GFM 180 - USJECANJE + FOLIOTISAK",
    "type": "PRC31", "proces_type_naziv": "Usjecanje/Foliotisak", 
    "elem_sifra": "SEL46", "elem_naziv": "Sloter element s folio tiskom",
    "unit": "kom", "interval": "h", "speed": 1500, "price": 10, "func": "sloter_work",
  
  },
 
  { "sifra": "RAD122", "naziv": "SLOTER GANDOSSI & FOSSATI GFM 180 - FOLIOTISAK", "unit": "kom", "interval": "h", "speed": 1500, "price": 10, "func": "",
 "type": "PRC31", "proces_type_naziv": "Usjecanje/Foliotisak", "elem_sifra": "SEL46", "elem_naziv": "Sloter element s folio tiskom" },
 
  { "sifra": "RAD123", "naziv": "ARISTO - SAMO REZANJE", "unit": "m", "interval": "h", "speed": 4068, "price": 10, "func": "",
 "type": "PRC23", "proces_type_naziv": "Rezanje", "elem_sifra": "SEL38", "elem_naziv": "Rezani element" },
 
  { "sifra": "RAD123B", "naziv": "ARISTO - REZANJE I BIGANJE", "unit": "m", "interval": "h", "speed": 0, "price": 10, "func": "",
 "type": "PRC23", "proces_type_naziv": "Rezanje", "elem_sifra": "SEL38", "elem_naziv": "Rezani element" },
 
{
  "sifra": "RADS1B", "naziv": "ZUND - REZANJE OSCILIRAJUĆIM NOŽEM", 
  "type": "PRC23", "proces_type_naziv": "Rezanje", 
  "elem_sifra": "SEL38", "elem_naziv": "Rezani element",
  "unit": "m", "interval": "h", "speed": 5090, "price": 300, "func": "zund_work",
},
 
{ 
  "sifra": "RADS1C", "naziv": "ZUND - REZANJE BRZOPOTEZNIM NOŽEM", 
  "type": "PRC23", "proces_type_naziv": "Rezanje", 
  "elem_sifra": "SEL38", "elem_naziv": "Rezani element",
  "unit": "m", "interval": "h", "speed": 5090, "price": 300, "func": "zund_work",
},
  
  
   
{ 
  "sifra": "RADS1CLEANZUND", "naziv": "OPTRGAVANJE NAKON ŠTANCE ILI ZUND-a", 
  "type": "PRC33", "proces_type_naziv": "Čiščenje", 
  "elem_sifra": "SEL48", "elem_naziv": "Očišćeni element/proizvod",
  "unit": "m", "interval": "h", "speed": 0, "price": 0, "func": "clean_work",
},
 
  
  
  { "sifra": "RAD124", "naziv": "GRAFICKI NOZ WOHLENBERG - REZANJE", "unit": "kom", "interval": "h", "speed": 1000, "price": 10, "func": "",
 "type": "PRC23", "proces_type_naziv": "Rezanje", "elem_sifra": "SEL38", "elem_naziv": "Rezani element" },
 
  
  
  { "sifra": "RAD125", "naziv": "ZELENI KRAJŠER - REZANJE TISKA", "unit": "kom", "interval": "h", "speed": 200, "price": 10, "func": "",
 "type": "PRC23", "proces_type_naziv": "Rezanje", "elem_sifra": "SEL38", "elem_naziv": "Rezani element" },
 
  { "sifra": "RAD126", "naziv": "ZELENI KRAJŠER - REZANJE", "unit": "kom", "interval": "h", "speed": 600, "price": 10, "func": "",
 "type": "PRC23", "proces_type_naziv": "Rezanje", "elem_sifra": "SEL38", "elem_naziv": "Rezani element" },
 
{ 
  "sifra": "RAD127", "naziv": "ZELENI KRAJŠER - BIGANJE", "func": "",
  "type": "PRC23", "proces_type_naziv": "Rezanje", "elem_sifra": "SEL38", "elem_naziv": "Rezani element",
  "unit": "kom", "interval": "h", "speed": 600, "price": 10,
},
 
{
  "sifra": "RADS1", "naziv": "ZELENI KRAJŠER - REZANJE + BIGANJE", "func": "krajser_work",
  "type": "PRC9", "proces_type_naziv": "Rezanje/biganje", 
  "elem_sifra": "SEL16", "elem_naziv": "Izrezani/bigani element",
  "unit": "m", "interval": "h", "speed": 700, "price": 250,
},
 
  { "sifra": "RAD128", "naziv": "SIVI KRAJŠER - REZANJE TISKA", "unit": "kom", "interval": "h", "speed": 200, "price": 10, "func": "",
 "type": "PRC23", "proces_type_naziv": "Rezanje", "elem_sifra": "SEL38", "elem_naziv": "Rezani element" },
 
  { "sifra": "RAD129", "naziv": "SIVI KRAJŠER - REZANJE", "unit": "kom", "interval": "h", "speed": 600, "price": 10, "func": "",
 "type": "PRC23", "proces_type_naziv": "Rezanje", "elem_sifra": "SEL38", "elem_naziv": "Rezani element" },
 
  { "sifra": "RAD130", "naziv": "SIVI KRAJŠER - BIGANJE", "unit": "kom", "interval": "h", "speed": 600, "price": 10, "func": "",
 "type": "PRC23", "proces_type_naziv": "Rezanje", "elem_sifra": "SEL38", "elem_naziv": "Rezani element" },
 
  { "sifra": "RAD131", "naziv": "SIVI KRAJŠER - REZANJE + BIGANJE", "unit": "kom", "interval": "h", "speed": 600, "price": 10, "func": "",
 "type": "PRC9", "proces_type_naziv": "Rezanje/biganje", "elem_sifra": "SEL16", "elem_naziv": "Izrezani/bigani element" },
 
  { "sifra": "RADS18", "naziv": "USJECALICA - USJECANJE", "unit": "kom", "interval": "h", "speed": 100, "price": 10, "func": "usjek_work",
 "type": "PRC19", "proces_type_naziv": "Usjecanje", "elem_sifra": "SEL33", "elem_naziv": "Usječeni element" },
 
  { "sifra": "RAD133", "naziv": "ŠLIC MAŠINA - SLOTER NOVI - USJECANJE", "unit": "kom", "interval": "h", "speed": 250, "price": 10, "func": "usjek_work",
 "type": "PRC19", "proces_type_naziv": "Usjecanje", "elem_sifra": "SEL33", "elem_naziv": "Usječeni element" },
  
 
{
  "sifra": "RAD134B1B1", "naziv": "KAŠIRKA LAMINA - KAŠIRANJE B1 ARAK NA B1 VAL. LJEPENKU", "unit": "kom", "func": "kasir_work",
  "type": "PRC2", "proces_type_naziv": "Kaširanje", 
  "elem_sifra": "SEL21", "elem_naziv": "Kaširani element",
  "interval": "h", "speed": 300, "price": 300,
},
  
{
  "sifra": "RAD134B0B0", "naziv": "KAŠIRKA LAMINA - KAŠIRANJE B0 ARAK NA B0 VAL. LJEPENKU", "unit": "kom", "func": "kasir_work",
  "type": "PRC2", "proces_type_naziv": "Kaširanje", 
  "elem_sifra": "SEL21", "elem_naziv": "Kaširani element",
  "interval": "h", "speed": 300/2, "price": 300,
},  
  
  
{ 
  "sifra": "RAD135", "naziv": "RUČNO KAŠIRANJE - B1 ARAK NA VAL. LJEPENKU", "unit": "kom", "func": "",
  "type": "PRC2", "proces_type_naziv": "Kaširanje", 
  "elem_sifra": "SEL21", "elem_naziv": "Kaširani element",
  "interval": "h", "speed": 300/4, "price": 300,
},
  
  
{ 
  "sifra": "RAD135B0", "naziv": "RUČNO KAŠIRANJE - B0 ARAK NA VAL. LJEPENKU", "unit": "kom", "func": "",
  "type": "PRC2", "proces_type_naziv": "Kaširanje", 
  "elem_sifra": "SEL21", "elem_naziv": "Kaširani element",
  "interval": "h", "speed": 300/6, "price": 300,
},  
  
 
{ 
  "sifra": "RAD136", "naziv": "CILINDAR ŠTANCA HEIDELBERG HB CYLINDER 54X77 - ŠTANCANJE", 
  "unit": "udarac", "interval": "h", "speed": 2000, "price": 200, "func": "stanca_work", tear_off_time: 0.05/125,
  "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", 
  "elem_naziv": "Odštancani element"
  },
 
  
{
  "sifra": "RAD137", "naziv": "ZAKLOPNA VELIKA ŠTANCA - ŠTANCANJE BEZ TISKA ", "unit": "udarac",
  "interval": "h", "speed": 300, "price": 300, "func": "stanca_work",
  "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element", tear_off_time: 0.4/125,
},
 
  { "sifra": "RAD138", "naziv": "ZAKLOPNA VELIKA ŠTANCA - ŠTANCANJE DIGITALNI TISAK JEDNOSTRANO", "unit": "udarac", "interval": "h", "speed": 250, "price": 300,
   "func": "stanca_work",
 "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element", tear_off_time: 0.4/125  },
 
  { "sifra": "RAD139", "naziv": "ZAKLOPNA VELIKA ŠTANCA - ŠTANCANJE DIGITALNI TISAK OBOSTRANO", "unit": "udarac", "interval": "h", "speed": 200, "price": 300, "func": "stanca_work",
 "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element", tear_off_time:  0.4/125 },
 
  { "sifra": "RAD140", "naziv": "ZAKLOPNA VELIKA ŠTANCA - ŠTANCANJE KAŠIRANI JEDNOSTRANI TISAK", "unit": "udarac", "interval": "h", "speed": 250, "price": 300, "func": "stanca_work",
 "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element", tear_off_time:  0.4/125},
 
  { "sifra": "RAD141", "naziv": "ZAKLOPNA VELIKA ŠTANCA - ŠTANCANJE KAŠIRANI OBOSTRANI TISAK", "unit": "udarac", "interval": "h", "speed": 200, "price": 300, "func": "stanca_work",
 "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element", tear_off_time:  0.4/125 },
 
  
  
  
  { "sifra": "RAD152", "naziv": "ZAKLOPNA ŠTANCA B1 - ŠTANCANJE BEZ TISKA ", "unit": "udarac", "interval": "h", "speed": 600, "price": 200, "func": "stanca_work",
 "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element" , tear_off_time: 0.15/125  },
 
  { "sifra": "RAD153", "naziv": "ZAKLOPNA ŠTANCA B1 - ŠTANCANJE DIGITALNI TISAK JEDNOSTRANO", "unit": "udarac", "interval": "h", "speed": 500, "price": 200, "func": "stanca_work",
 "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element" , tear_off_time:  0.15/125 },
 
  { "sifra": "RAD154", "naziv": "ZAKLOPNA ŠTANCA B1 - ŠTANCANJE DIGITALNI TISAK OBOSTRANO", "unit": "udarac", "interval": "h", "speed": 450, "price": 200, "func": "stanca_work",
 "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element" , tear_off_time:  0.15/125  },
 
  { "sifra": "RAD155", "naziv": "ZAKLOPNA ŠTANCA B1 - ŠTANCANJE KAŠIRANI JEDNOSTRANI TISAK", "unit": "udarac", "interval": "h", "speed": 500, "price": 200, "func": "stanca_work",
 "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element" , tear_off_time:  0.15/125 },
 
  { "sifra": "RAD156", "naziv": "ZAKLOPNA ŠTANCA B1 - ŠTANCANJE KAŠIRANI OBOSTRANI TISAK", "unit": "udarac", "interval": "h", "speed": 450, "price": 200, "func": "stanca_work",
 "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element" , tear_off_time:  0.15/125  },
 
  
  
  
  
  { "sifra": "RAD142", "naziv": "ZAKLOPNA ŠTANCA B0 (ZELENA) - ŠTANCANJE BEZ TISKA ", "unit": "udarac", "interval": "h", "speed": 500, "price": 250, "func": "stanca_work",
 "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element" , tear_off_time:  0.25/125 },
 
  { "sifra": "RAD143", "naziv": "ZAKLOPNA ŠTANCA B0 (ZELENA) - ŠTANCANJE DIGITALNI TISAK JEDNOSTRANO", "unit": "udarac", "interval": "h", "speed": 400, "price": 250, "func": "stanca_work",
 "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element", tear_off_time: 0.25/125  },
 
  { "sifra": "RAD144", "naziv": "ZAKLOPNA ŠTANCA B0 (ZELENA) - ŠTANCANJE DIGITALNI TISAK OBOSTRANO", "unit": "udarac", "interval": "h", "speed": 350, "price": 250, "func": "stanca_work",
 "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element", tear_off_time: 0.25/125  },
 
  { "sifra": "RAD145", "naziv": "ZAKLOPNA ŠTANCA B0 (ZELENA) - ŠTANCANJE KAŠIRANI JEDNOSTRANI TISAK", "unit": "udarac", "interval": "h", "speed": 400, "price": 250, "func": "stanca_work",
 "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element", tear_off_time: 0.25/125  },
 
  { "sifra": "RAD146", "naziv": "ZAKLOPNA ŠTANCA B0 (ZELENA) - ŠTANCANJE KAŠIRANI OBOSTRANI TISAK", "unit": "udarac", "interval": "h", "speed": 350, "price": 250, "func": "stanca_work",
 "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element", tear_off_time: 0.25/125 },
 
  
  
  
  
  
  { "sifra": "RAD147", "naziv": "ZAKLOPNA ŠTANCA B0 (SIVA) - ŠTANCANJE BEZ TISKA ", "unit": "udarac", "interval": "h", "speed": 500, "price": 250, "func": "stanca_work",
 "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element", tear_off_time: 0.25/125 },
 
  { "sifra": "RAD148", "naziv": "ZAKLOPNA ŠTANCA B0 (SIVA) - ŠTANCANJE DIGITALNI TISAK JEDNOSTRANO", "unit": "udarac", "interval": "h", "speed": 400, "price": 250, "func": "stanca_work",
 "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element" , tear_off_time: 0.25/125  },
 
  { "sifra": "RAD149", "naziv": "ZAKLOPNA ŠTANCA B0 (SIVA) - ŠTANCANJE DIGITALNI TISAK OBOSTRANO", "unit": "udarac", "interval": "h", "speed": 350, "price": 250, "func": "stanca_work",
 "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element", tear_off_time: 0.25/125 },
 
  { "sifra": "RAD150", "naziv": "ZAKLOPNA ŠTANCA B0 (SIVA) - ŠTANCANJE KAŠIRANI JEDNOSTRANI TISAK", "unit": "udarac", "interval": "h", "speed": 400, "price": 250, "func": "stanca_work",
 "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element" , tear_off_time: 0.25/125  },
 
  { "sifra": "RAD151", "naziv": "ZAKLOPNA ŠTANCA B0 (SIVA) - ŠTANCANJE KAŠIRANI OBOSTRANI TISAK", "unit": "udarac", "interval": "h", "speed": 350, "price": 250, "func": "stanca_work",
 "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element" , tear_off_time: 0.25/125  },
 
  
  

{ 

  "sifra": "RAD157", "naziv": "FQD X-1400 ŠIVAČICA (STICHING MACHINE) - ŠIVANJE", "unit": "šivanja", "interval": "h", 
  "speed": 37.5, "price": 1, "func": "",
  "type": "PRC4", "proces_type_naziv": "Štancanje", "elem_sifra": "SEL23", "elem_naziv": "Odštancani element" 
},
 
  { 
    "sifra": "RAD158", "naziv": "ZX-1300 VELIKA LJEPILICA (SEMI AUTOMATIC GLUER) - LJEPLJENJE B VAL", "unit": "kom", 
    "interval": "h", "speed": 500, "price": 10, "func": "",
    "type": "PRC32", "proces_type_naziv": "Ljepljenje", "elem_sifra": "SEL47", "elem_naziv": "Zaljepljeni elementi/proizvod" 
  },
 
  { "sifra": "RADS11", "naziv": "ZX-1300 VELIKA LJEPILICA (SEMI AUTOMATIC GLUER) - LJEPLJENJE BC/EB VAL", "unit": "kom", "interval": "h", "speed": 375, "price": 10, "func": "",
 "type": "PRC32", "proces_type_naziv": "Ljepljenje", "elem_sifra": "SEL47", "elem_naziv": "Zaljepljeni elementi/proizvod" },
 
  { "sifra": "RAD159", "naziv": "LJEPILICA - BOX PLUS 2 (APR SOLUTION) - LJEPLJENJE 1 TOČKA PAPIR", "unit": "m", "interval": "h", "speed": 9000, "price": 10, "func": "",
 "type": "PRC32", "proces_type_naziv": "Ljepljenje", "elem_sifra": "SEL47", "elem_naziv": "Zaljepljeni elementi/proizvod" },
 
  { "sifra": "RAD160", "naziv": "LJEPILICA - BOX PLUS 2 (APR SOLUTION) - LJEPLJENJE 1 TOČKA KAŠIRANI MIKROVAL", "unit": "m", "interval": "h", "speed": 9000, "price": 10, "func": "",
 "type": "PRC32", "proces_type_naziv": "Ljepljenje", "elem_sifra": "SEL47", "elem_naziv": "Zaljepljeni elementi/proizvod" },
 
  { "sifra": "RAD161", "naziv": "LJEPILICA - BOX PLUS 2 (APR SOLUTION) - LJEPLJENJE 3 TOČKE", "unit": "m", "interval": "h", "speed": 9000, "price": 10, "func": "",
 "type": "PRC32", "proces_type_naziv": "Ljepljenje", "elem_sifra": "SEL47", "elem_naziv": "Zaljepljeni elementi/proizvod" },
 
  { "sifra": "RAD162", "naziv": "LJEPILICA - BOX PLUS 2 (APR SOLUTION) - NANOŠENJE DUPLOFANA", "unit": "m", "interval": "h", "speed": 9000, "price": 10, "func": "",
 "type": "PRC32", "proces_type_naziv": "Ljepljenje", "elem_sifra": "SEL47", "elem_naziv": "Zaljepljeni elementi/proizvod" },

  
  
  
  
  { "sifra": "RAD164", "naziv": "VAKUUMIRKA - VAKUMIRANJE", "unit": "kom", "interval": "h", "speed": 87.5, "price": 10, "func": "",
 "type": "PRC26", "proces_type_naziv": "Vakumiranje", "elem_sifra": "SEL41", "elem_naziv": "Vakumirani element/proizvod" },
 
  
  
  
  
  { "sifra": "RAD165", "naziv": "ČIŠĆENJE - ČIŠĆENJE", "unit": "kom", "interval": "h", "speed": 0, "price": 10, "func": "",
 "type": "PRC33", "proces_type_naziv": "Čiščenje", "elem_sifra": "SEL48", "elem_naziv": "Očišćeni element/proizvod" },
 
  /*
  
  { "sifra": "RAD166", "naziv": "DORADA - LJEPLJENJE POLICA ZA STALKE - MALENE", "unit": "kom", "interval": "h", "speed": 120, "price": 10, "func": "",
 "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", "elem_sifra": "SEL22", "elem_naziv": "Doradno zaljepljen element" },
 
  { "sifra": "RAD167", "naziv": "DORADA - LJEPLJENJE POLICA ZA STALKE - VELIKE", "unit": "kom", "interval": "h", "speed": 100, "price": 10, "func": "",
 "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", "elem_sifra": "SEL22", "elem_naziv": "Doradno zaljepljen element" },
  
  { "sifra": "RAD168", "naziv": "DORADA - SPAJANJE BOKOVA I LEĐA ZA STALKE - MANJI", "unit": "kom", "interval": "h", "speed": 25, "price": 10, "func": "",
 "type": "PRC27", "proces_type_naziv": "Spajanje", "elem_sifra": "SEL42", "elem_naziv": "Spojeni elementi/proizvod" },
 
  { "sifra": "RAD169", "naziv": "DORADA - SPAJANJE BOKOVA I LEĐA ZA STALKE - SREDNJI", "unit": "kom", "interval": "h", "speed": 18.75, "price": 10, "func": "",
 "type": "PRC27", "proces_type_naziv": "Spajanje", "elem_sifra": "SEL42", "elem_naziv": "Spojeni elementi/proizvod" },
 
  { "sifra": "RAD170", "naziv": "DORADA - SPAJANJE BOKOVA I LEĐA ZA STALKE - VELIKI", "unit": "kom", "interval": "h", "speed": 15, "price": 10, "func": "",
 "type": "PRC27", "proces_type_naziv": "Spajanje", "elem_sifra": "SEL42", "elem_naziv": "Spojeni elementi/proizvod" },
 
  { "sifra": "RAD171", "naziv": "DORADA - SPAJANJE BOKOVA I LEĐA ZA STALKE + ULJEPLJIVANJE POLICA - MANJI", "unit": "kom", "interval": "h", "speed": 21.875, "price": 10, "func": "",
 "type": "PRC27", "proces_type_naziv": "Spajanje", "elem_sifra": "SEL42", "elem_naziv": "Spojeni elementi/proizvod" },
 
  { "sifra": "RAD172", "naziv": "DORADA - SPAJANJE BOKOVA I LEĐA ZA STALKE + ULJEPLJIVANJE POLICA- SREDNJI", "unit": "kom", "interval": "h", "speed": 15, "price": 10, "func": "",
 "type": "PRC27", "proces_type_naziv": "Spajanje", "elem_sifra": "SEL42", "elem_naziv": "Spojeni elementi/proizvod" },
 
  
  { "sifra": "RAD173", "naziv": "DORADA - SPAJANJE BOKOVA I LEĐA ZA STALKE+ ULJEPLJIVANJE POLICA - VELIKI", "unit": "kom", "interval": "h", "speed": 12.5, "price": 10, "func": "",
 "type": "PRC27", "proces_type_naziv": "Spajanje", "elem_sifra": "SEL42", "elem_naziv": "Spojeni elementi/proizvod" },
 
  { "sifra": "RAD174", "naziv": "DORADA - ULJEPLJIVANJE SHELFLOCKOVA U BOKOVE", "unit": "kom", "interval": "h", "speed": 120, "price": 10, "func": "",
 "type": "PRC34", "proces_type_naziv": "Uljepljivanje", "elem_sifra": "SEL49", "elem_naziv": "Uljepljeni elementi/proizvod" },

  */
  

/* ------------START----------------- NE ULJEPLJUJEM POLICE NA LJEĐA ----------------------------- */  
  
/*  
{
  "sifra": "RAD250", "naziv": "DORADA STALAK - ljepjenje bokova i leđa BEZ OJAČANJA",  "func": "glue_back_sides_work",
  "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
  "elem_sifra": "SEL1000", "elem_naziv": "Zaljepljeni bokovi za leđa bez ojačanja",
  "unit": "kom", "interval": "h", "speed": 60/8, "price": 150,
},  

{
   
  "sifra": "RAD251", "naziv": "DORADA STALAK - ljepjenje bokova i leđa SA OJAČANJEM", "func": "glue_back_sides_R_work",
  "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
  "elem_sifra": "SEL1001", "elem_naziv": "Zaljepljeni bokovi za leđa s ojačanjima",
  "unit": "kom", "interval": "h", "speed": 50/8, "price": 150, 
},
  
{
  "sifra": "RAD252", "naziv": "DORADA STALAK - ljepjenje shelflockova i bokova BEZ OJAČANJA",  "func": "glue_shelflock_sides_work",
  "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
  "elem_sifra": "SEL1002", "elem_naziv": "Zaljepljeni shelflockovi i bokovi bez ojačanja",
  "unit": "kom", "interval": "h", "speed": 60*1.2/8, "price": 150,
},
  
{
  "sifra": "RAD253", "naziv": "DORADA STALAK - ljepjenje shelflockova i bokova SA OJAČANJEM",  "func": "glue_shelflock_sides_R_work",
  "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
  "elem_sifra": "SEL1003", "elem_naziv": "Zaljepljeni shelflockovi i bokovi sa ojačanjem",
  "unit": "kom", "interval": "h", "speed": 60*1.2/8, "price": 150,
},

*/
  
/* ------------END----------------- NE ULJEPLJUJEM POLICE NA LJEĐA ----------------------------- */    
  
  
/* ------------START----------------- SA ULJEPLJENIM POLICAMA NA LEĐA ----------------------------- */    
  
  
/*  
  
{
  "sifra": "RAD250GS", "naziv": "DORADA STALAK - ljepljenje bokova i leđa BEZ OJAČANJA (police zaljepljenje na leđa)",  
  "func": "glue_back_sides_GS_work",
  "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
  "elem_sifra": "SEL1000", "elem_naziv": "Zaljepljeni bokovi za leđa bez ojačanja",
  "unit": "kom", "interval": "h", "speed": 60*1.1/8, "price": 150,
},  
  
{
   
  "sifra": "RAD251GS", "naziv": "DORADA STALAK - ljepljenje bokova i leđa SA OJAČANJEM (police zaljepljenje na leđa)", 
  "func": "glue_back_sides_RGS_work",
  "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
  "elem_sifra": "SEL1001", "elem_naziv": "Zaljepljeni bokovi za leđa s ojačanjima",
  "unit": "kom", "interval": "h", "speed": 50*1.1/8, "price": 150, 
},
  
{
  "sifra": "RAD252GS", "naziv": "DORADA STALAK - ljepjenje shelflockova i bokova BEZ OJAČANJA (police zaljepljenje na leđa)",  
  "func": "glue_shelflock_sides_GS_work",
  "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
  "elem_sifra": "SEL1002", "elem_naziv": "Zaljepljeni shelflockovi i bokovi bez ojačanja",
  "unit": "kom", "interval": "h", "speed": 60*1.2*1.1/8, "price": 150,
},
  
{
  "sifra": "RAD253GS", "naziv": "DORADA STALAK - ljepjenje shelflockova i bokova SA OJAČANJEM (police zaljepljenje na leđa)",  
  "func": "glue_shelflock_sides_RGS_work",
  "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
  "elem_sifra": "SEL1003", "elem_naziv": "Zaljepljeni shelflockovi i bokovi sa ojačanjem",
  "unit": "kom", "interval": "h", "speed": 60*1.2*1.1/8, "price": 150,
},  
  
*/  
  
/* ------------END----------------- SA ULJEPLJENIM POLICAMA NA LEĐA ----------------------------- */      
  
/*  
  
{
  "sifra": "RAD253POLICEBACK", "naziv": "DORADA STALAK - ljepljenje polica na leđa",  "func": "glue_polica_back_work",
  "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
  "elem_sifra": "SEL1003POLICE", "elem_naziv": "Zaljepljene police na leđima",
  "unit": "kom", "interval": "h", "speed": 100, "price": 150,
},
  

{
  "sifra": "RAD254", "naziv": "DORADA STALAK - ljepljenje polica s jednim ojačanjem",  "func": "glue_polica_work",
  "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
  "elem_sifra": "SEL1004", "elem_naziv": "Zaljepljena polica",
  "unit": "kom", "interval": "h", "speed": 100, "price": 150,
}, 

{
  "sifra": "RAD255", "naziv": "DORADA STALAK - ljepljenje polica s duplim ojačanjem",  "func": "glue_polica_R_work",
  "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
  "elem_sifra": "SEL1005", "elem_naziv": "Zaljepljena polica s duplim ojačanjem",
  "unit": "kom", "interval": "h", "speed": 50, "price": 150,
},
  
  
  
{
  "sifra": "RAD255FOOTER", "naziv": "DORADA STALAK - ljepljenje footera",  "func": "glue_footer_work",
  "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
  "elem_sifra": "SEL1005FOOTER", "elem_naziv": "Zaljepljen footer",
  "unit": "kom", "interval": "h", "speed": 70, "price": 150,
},

  
{
  "sifra": "RAD256", "naziv": "DORADA STALAK - ljepljenje shelf lockova",  "func": "glue_shelflock_work",
  "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
  "elem_sifra": "SEL1006", "elem_naziv": "Element sa zaljepljenim shelf lockovima",
  "unit": "kom", "interval": "h", "speed": 240, "price": 150,
},  
  
*/

  
{ 
  "sifra": "RADGLUEGUN", "naziv": "LJEPLJENJE - RUČNI PIŠTOLJ", "func": "gun_glue_work",
  "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
  "elem_sifra": "SELDORADA", "elem_naziv": "Zaljepljeni elementi/proizvod",
  "unit": "kom", "interval": "h", "speed": 1, "price": 150,
},

{ 
  "sifra": "RADGLUEPITON", "naziv": "LJEPLJENJE - PITON", "func": "piton_glue_work",
  "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
  "elem_sifra": "SELDORADA", "elem_naziv": "Zaljepljeni elementi/proizvod",
  "unit": "kom", "interval": "h", "speed": 1, "price": 150,
},
  

{ 
  "sifra": "RADGLUEOCTO", "naziv": "LJEPLJENJE - HOBOTNICA", "func": "octo_glue_work",
  "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
  "elem_sifra": "SELDORADA", "elem_naziv": "Zaljepljeni elementi/proizvod",
  "unit": "kom", "interval": "h", "speed": 1, "price": 150,
},


{ 
  "sifra": "RADGLUECNC", "naziv": "LJEPLJENJE - CNC STROJ", "func": "cnc_glue_work",
  "type": "PRC3", "proces_type_naziv": "Doradno Ljepljenje", 
  "elem_sifra": "SELDORADA", "elem_naziv": "Zaljepljeni elementi/proizvod",
  "unit": "kom", "interval": "h", "speed": 1, "price": 150,
},



  
{ 
  "sifra": "RAD178", "naziv": "DORADA - REZANJE ČIČAK TRAKE (muške i ženske)", "func": "cut_cicak_work",
  "type": "PRC36", "proces_type_naziv": "Doradno rezanje", 
  "elem_sifra": "SEL51", "elem_naziv": "Odrezani elementi/proizvod",
  "unit": "kom", "interval": "h", "speed": 2000, "price": 150,
},  

{ 
  "sifra": "RAD179", "naziv": "DORADA - NANOŠENJE ČIČAK TRAKE - MUŠKA", "func": "glue_cicak_M_work",
  "type": "PRC35", "proces_type_naziv": "Nanošenje čičak trake", 
  "elem_sifra": "SEL50", "elem_naziv": "Element/proizvod s čičak trakom", 
  "unit": "m", "interval": "h", "speed": 60, "price": 150,
},

{ 
  "sifra": "RAD179F", "naziv": "DORADA - NANOŠENJE ČIČAK TRAKE - ŽENSKA", "func": "glue_cicak_F_work",
  "type": "PRC35", "proces_type_naziv": "Nanošenje čičak trake", 
  "elem_sifra": "SEL50", "elem_naziv": "Element/proizvod s čičak trakom", 
  "unit": "m", "interval": "h", "speed": 60, "price": 150,
},  

  
{ 
  "sifra": "RAD177", "naziv": "DORADA - REZANJE I NANOŠENJE DUPLOFANA", "func": "duplofan_work",
  "type": "PRC7", "proces_type_naziv": "Ljepljenje duplofana", 
  "elem_sifra": "SEL26", "elem_naziv": "Element s zajepljenom duplofanom",
  "unit": "kom", "interval": "h", "speed": 80, "price": 150,
},
  
 
{ 
  "sifra": "RAD176", "naziv": "DORADA - RUČNO VEZANJE KUTIJA I PAKIRANJE", "func": "box_pakir_work",
  "type": "PRC13", "proces_type_naziv": "Pakiranje", 
  "elem_sifra": "SEL30", "elem_naziv": "Upakirani elementi",
  "unit": "kom", "interval": "h", "speed": 750, "price": 150,
},
  
 
{ 
  // imam dvije opcije kada js stalk s uljepljenim policama i kada nisu uljepljene
  "sifra": "RAD163", "naziv": "OMATALICA GG MACCHINE - OMATANJE STALKA", "func": "stalak_omat_work", 
  "type": "PRC25", "proces_type_naziv": "Omatanje", 
  "elem_sifra": "SEL40", "elem_naziv": "Omotani element/proizvod",
  "unit": "kom", "interval": "h", "speed": 60, "price": 150, /* 60 je s uljepljenim policama ---> 50 kom je ako police nisu uljepljenje  */
},
  
 
{ 
  "sifra": "RAD180", "naziv": "DORADA - FORMIRANJE STALKA", "func": "stalak_form_work",
  "type": "PRC28", "proces_type_naziv": "Formiranje", 
  "elem_sifra": "SEL43", "elem_naziv": "Formirani elementi/proizvod",
  "unit": "kom", "interval": "h", "speed": 60, "price": 10, 
},

  
], 