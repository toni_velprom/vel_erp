#!/bin/bash

#set -o errexit

sleep 720

now=$(date +"%T")
echo "Current time : $now" > /var/www/erp.velprom.hr/bin/cron_vnc_started.txt
sleep 2

vncserver -kill :1
sleep 2
vncserver

