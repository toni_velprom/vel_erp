#!/bin/bash

set -o errexit

rsync -avu --delete /Users/tonikutlic/TONI/2021/VELPROM/VEL_ERP/api/* toni@192.168.88.112:/var/www/erp.velprom.hr/api

cd /Users/tonikutlic/TONI/2021/VELPROM/VEL_ERP/public
rsync -avu --exclude 'docs' --delete /Users/tonikutlic/TONI/2021/VELPROM/VEL_ERP/public/* toni@192.168.88.112:/var/www/erp.velprom.hr/public

rsync -avu --delete /Users/tonikutlic/TONI/2021/VELPROM/VEL_ERP/server.js toni@192.168.88.112:/var/www/erp.velprom.hr
rsync -avu --delete /Users/tonikutlic/TONI/2021/VELPROM/VEL_ERP/auth_rules.js toni@192.168.88.112:/var/www/erp.velprom.hr
rsync -avu --delete /Users/tonikutlic/TONI/2021/VELPROM/VEL_ERP/compile_files.js toni@192.168.88.112:/var/www/erp.velprom.hr