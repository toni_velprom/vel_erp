#!/bin/bash

set -o errexit

ssh toni@192.168.88.112 /home/toni/db_dump.sh $1
scp -r toni@192.168.88.112:/home/toni/mongo_baza_bckp/$1 /Users/tonikutlic/TONI/2021/VELPROM/VEL_ERP_MATERIJALI/mongo_baza_bckp
