#!/bin/bash

#set -o errexit

sleep 360

now=$(date +"%T")
echo "Current time : $now" > /var/www/erp.velprom.hr/bin/cron_server_started.txt
sleep 2

umount /mnt/mrav/
sleep 2
mount -t cifs -o username=MRAV,password=eukaliptus69 //192.168.88.205/mrav /mnt/mrav
sleep 2
systemctl stop mongod
sleep 2
systemctl start mongod
sleep 2
cd /var/www/erp.velprom.hr/
# forever start -o out.log -e err.log server.js
# /usr/local/bin/forever start -o /var/www/erp.velprom.hr/out.log -e /var/www/erp.velprom.hr/err.log /var/www/erp.velprom.hr/server.js

# /usr/local/bin/forever --sourceDir /var/www/erp.velprom.hr/ -c 'node --inspect-brk' server.js -o out.log -e err.log 


/usr/local/bin/forever --sourceDir /var/www/erp.velprom.hr/ -c 'node' server.js -o out.log -e err.log 




