#!/bin/bash

set -o errexit

#cp -R www/css/. platforms/browser/www/css/
#cp -R www/js/. platforms/browser/www/js/
#cp -R www/img/. platforms/browser/www/img/
#cp -R www/index.html platforms/browser/www/index.html

#rsync -avu --delete "www/css/" "platforms/browser/www/css"
#rsync -avu --delete "www/js/" "platforms/browser/www/js"
#rsync -avu --delete "www/img/" "platforms/browser/www/img"
#cp -R www/index.html platforms/browser/www/index.html


#npx babel public --out-file script-compiled.js

npx babel ./public/js/initial_scripts.js  --out-file ./public/js/initial_scripts_babel.js --presets=@babel/preset-env  --source-maps inline
browserify ./public/js/initial_scripts_babel.js -o ./public/js/bro_initial_scripts.js -d
