var mongoose = require('mongoose');
// mongoose.set('useFindAndModify', false);

var User = mongoose.model('User');
var jwt = require('jsonwebtoken');

var jsdom = require("jsdom");
var { JSDOM } = jsdom;
var { window } = new JSDOM(`...`);
var $ = require("jquery")(window);



function protected_read_paths( path ) {

  var protected = [ 
    
  `/modules/info/info.js`,
  `/modules/admin/vel_admin.js`,

  ];

  var is_protected = false;  
  

  $.each(protected, function(ind, protected_path ) {
    if ( path.indexOf(protected_path) > -1 ) is_protected = true;
  });
  
  return is_protected;
  
};

function protected_edit_paths( path ) {

  var protected = [ 
    
  `/modules/info/info.js`,
  `/edit_admin_conf`,

  ];

  var is_protected = false;  
  

  $.each(protected, function(ind, protected_path ) {
    if ( path.indexOf(protected_path) > -1 ) is_protected = true;
  });
  
  return is_protected;
  
};

function protected_delete_paths( path ) {

  var protected = [ 
    
  `/modules/info/info.js`,
  `/delete_admin_conf`,

  ];

  var is_protected = false;  
  

  $.each(protected, function(ind, protected_path ) {
    if ( path.indexOf(protected_path) > -1 ) is_protected = true;
  });
  
  return is_protected;
  
};


function prava_usera(user, path) {
  
  var role = null;
  var mods = { read: false,  write: false,  delete: false };
  
  // neki pathovi korespondiraju poljima u user roles:
  // neki pathovi korespondiraju poljima u user roles:
  // neki pathovi korespondiraju poljima u user roles:
  
  if ( path.indexOf('/modules/info/info.js') > -1 ) role = `info`;
  // if ( path.indexOf('/get_admin_conf') > -1 ) role = `admin_conf`;
  if ( path.indexOf(`/modules/admin/vel_admin.js`) > -1 ) role = `admin_view`;
  
  
  // neki pathovi korespondiraju poljima u user roles:
  // neki pathovi korespondiraju poljima u user roles:
  // neki pathovi korespondiraju poljima u user roles:
  
  
    
  
  // ako postoji role za taj path
  // i ako user ima taj role
  
  if ( role && user.roles[role] ) {
    
    if ( user.roles[role].indexOf('r') > -1 ) mods.read = true;
    if ( user.roles[role].indexOf('w') > -1 ) mods.write = true;
    if ( user.roles[role].indexOf('d') > -1 ) mods.delete = true;
    
  };
  
  return mods;
  
};


function gatekeeper(req, res, next) {

  // we can specify urls for triggering this gatekeeper
  // for example /secure
  // we can include multiple urls with ||
  // also we can use regExp to pin-point specific url


  // in case url contains parameters like /secure?bla=sdasda&....
  var pure_url = req.url;
  
  if ( req.url.indexOf('?') > -1) pure_url = req.url.split('?')[0]; 
  // in case url contains hash like /some_url#bla/dasdad/
  if ( pure_url.indexOf('#') > -1) pure_url = pure_url.split('#')[0];
  
  // 1) token can be part of ajax body
  // 2) or part of URL (as url string parameter) 
  // 3) or ajax call header property
  var token = req.body.token || req.query.token || req.headers['x-auth-token'];

  // ako je path jedan od protected
  if ( protected_read_paths(pure_url) ) {
  
    // ako ima token
    if (token) {
      
      global.check_user_auth(token)
      .then(function(response) {
        
        if ( response.success == true ) {
          
          var prava = prava_usera(response.user, pure_url);
          
          if ( prava.read !== true ) {
            
            res.json({ 
              success: false, 
              msg: 'Nemate ovlasti za pregled ove informacije.<br>ERROR: 002'
            });
            
            return;
            
          } else {
            
            next();
            
          };

        } 
        else {
          
          res.json({ 
            success: false, 
            msg: response.msg
          });
          
          return;
        };
        
      })
      .catch(function(err) {
        res.json({ 
          success: false, 
          msg: 'Došlo je do greške prilikom verifikacije korisnika.<br>ERROR: 001' 
        });
        
        return;
      });

    } 
    else {
      // ako nema token
      res.json({ 
          success: false, 
          msg: 'Nedostaje token ili je token istekao.<br>Molimo vas da se prijavite u aplikaciju!' 
      });

      return;
      // res.redirect('/login');
    };


     // kraj ifa ako je protected path
  } 
  else if ( protected_edit_paths(pure_url) ) {
  
    // ako ima token
    if (token) {
      
      global.check_user_auth(token)
      .then(function(response) {
        
        if ( response.success == true ) {
          
          var prava = prava_usera(response.user, pure_url);
          
          if ( prava.write !== true ) {
            
            res.json({ 
              success: false, 
              msg: 'Nemate ovlasti za editiranje ove informacije.<br>ERROR: 002.1'
            });
            
            return;
            
          } 
          else {
            
            next();
            
          };

        } 
        else {
          
          res.json({ 
            success: false, 
            msg: response.msg
          });
          
          return;
        };
        
      })
      .catch(function(err) {
        res.json({ 
          success: false, 
          msg: 'Došlo je do greške prilikom verifikacije korisnika.<br>ERROR: 001' 
        });
        
        return;
      });

    } 
    else {
      // ako nema token
      res.json({ 
          success: false, 
          msg: 'Nedostaje token ili je token istekao.<br>Molimo vas da se prijavite u aplikaciju!' 
      });

      return;
      // res.redirect('/login');
    };


     // kraj ifa ako je protected path ZA EDIT
  } 
  else {
    // ako nije protected
    next();
  };
    
};
exports.gatekeeper = gatekeeper;

/*

----------------------------------------------------------------------------------------
OVO JE SAMO PRIMJER KAKO VRATITI CUSTOM HTML
----------------------------------------------------------------------------------------


if (req.url == '/login') {


  // this is just exapmle of sending raw html

  res.set('content-type','text/html');

  var page = 
  `<!doctype html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <title>The HTML5 Herald</title>
    <meta name="description" content="Login Page">
    <meta name="author" content="Repuzzlic Ltd">
    <link rel="stylesheet" href="/css/proba.css?v=1.0">
  </head>
  <body>
    THIS IS JUST A TEST <br>
    OF REDIRECTION TO LOGIN PAGE
  </body>
  </html>
  `;

  res.send(page);  

};
----------------------------------------------------------------------------------------
OVO JE SAMO PRIMJER KAKO VRATITI CUSTOM HTML
----------------------------------------------------------------------------------------

*/


function check_user_auth(token) {

  return new Promise( function(resolve, reject) {

    var super_admin = false;
    
    /*
    if ( token == 'free' ) {
      resolve({ success: true, user: 'free' , super_admin: false });
      return;
    };
    */
    
    jwt.verify(token, 'super_tajna', function(err, decoded_user) { 

    if (err) {
      reject({ success: false, msg: 'Greška kod verifikacije usera!', error: err });    
      return;
    };

      User.findOne(
      { _id: decoded_user._id },
      function(err, user) {

        if (err) {

          resolve({ success: false, msg: 'Greška kod pronalaska usera!', error: err });
          return;
          
        };

        if (user) {
          
          var clean_user = global.mng_to_js(user);
          
          if (
            decoded_user._id == '60a29bee859170736fd12e6d' || /* Radmila */
            decoded_user._id == '60a4bc4f8617c34c16d1979f' || /* Toni */
            decoded_user._id == '60cc9208f0b3e71b940aa88d' || /* Bruno */
            decoded_user._id == '60cc9207f0b3e71b940aa888' /* Zoran */
          ) {
            super_admin = true;
          };
          
          resolve({ success: true, user: clean_user, super_admin: super_admin });

        } else {
          // ako nije našao usera !!!
          resolve({ success: false, msg: 'Nije pronađen user!' });
        };


      }); // kraj find user        

    }); // kraj verifikacije tokena  

  }); // kraj return promisa
  
}; // kraj check is super func
global.check_user_auth = check_user_auth;



function handle_auth(req) { 
  
  
  return new Promise( function(resolve, reject) {

    var edit_user_num = req.body.user_number;

    var token = req.headers['x-auth-token'] || null;

    if ( token ) {

      global.check_user_auth(token).then(
      function( result ) {

        if ( result.success == true ) {
          
          resolve(result);

        } else {

          resolve({ success: false, msg: 'Došlo je do greške prilikom provjere korisnika!' }); 
          return;

        };

      })
      .catch(function(err) {

        // ako se dogodi bilo koja greška kod verifikacije tokena ili pronalaska usera
        resolve({ success: false, msg: 'Došlo je do greške prilikom provjere korisnika!', err: err }); 

      });

    } 
    else {
      // vrati da nema ovlasti ako je request bez tokena !!!!
      resolve({ success: false, msg: 'Nedostaje token!' });    

    };
    
    
  }); // kraj promisa

};

global.handle_auth = handle_auth;

  

